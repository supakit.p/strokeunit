package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005PData.java
 * Description: SFTE005PData class implements for handle tuser data base schema.
 * Version: $Revision$
 * Creation date: Wed Jan 03 13:47:17 ICT 2018
 */
/**
 * SFTE005PData class implements for handle tuser data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE005PData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE005PData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tuser");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("username",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("userbranch",java.sql.Types.VARCHAR,true);
	addSchema("usertname",java.sql.Types.VARCHAR,true);
	addSchema("usertsurname",java.sql.Types.VARCHAR,true);
	addSchema("userename",java.sql.Types.VARCHAR,true);
	addSchema("useresurname",java.sql.Types.VARCHAR,true);
	addSchema("email",java.sql.Types.VARCHAR,true);
	addSchema("gender",java.sql.Types.VARCHAR,true);
	addSchema("mobile",java.sql.Types.VARCHAR,true);
	addSchema("lineno",java.sql.Types.VARCHAR,true);
	map("userid","userid");
	map("username","username");
	map("site","site");
	map("userbranch","userbranch");
	map("usertname","usertname");
	map("usertsurname","usertsurname");
	map("userename","userename");
	map("useresurname","useresurname");
	map("email","email");
	map("gender","gender");
	map("mobile","mobile");
	map("lineno","lineno");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE005PData.class+"=$Revision$\n";
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getUsername() {
	return getString("username");
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getUserbranch() {
	return getString("userbranch");
}
public void setUserbranch(String newUserbranch) {
	setMember("userbranch",newUserbranch);
}
public String getUsertname() {
	return getString("usertname");
}
public void setUsertname(String newUsertname) {
	setMember("usertname",newUsertname);
}
public String getUsertsurname() {
	return getString("usertsurname");
}
public void setUsertsurname(String newUsertsurname) {
	setMember("usertsurname",newUsertsurname);
}
public String getUserename() {
	return getString("userename");
}
public void setUserename(String newUserename) {
	setMember("userename",newUserename);
}
public String getUseresurname() {
	return getString("useresurname");
}
public void setUseresurname(String newUseresurname) {
	setMember("useresurname",newUseresurname);
}
public String getEmail() {
	return getString("email");
}
public void setEmail(String newEmail) {
	setMember("email",newEmail);
}
public String getGender() {
	return getString("gender");
}
public void setGender(String newGender) {
	setMember("gender",newGender);
}
public String getMobile() {
	return getString("mobile");
}
public void setMobile(String newMobile) {
	setMember("mobile",newMobile);
}
public String getLineno() {
	return getString("lineno");
}
public void setLineno(String newLineno) {
	setMember("lineno",newLineno);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUsername(bean.getFieldByName(mapper("username")).asString());
	setSite(bean.getFieldByName(mapper("site")).asString());
	setUserbranch(bean.getFieldByName(mapper("userbranch")).asString());
	setUsertname(bean.getFieldByName(mapper("usertname")).asString());
	setUsertsurname(bean.getFieldByName(mapper("usertsurname")).asString());
	setUserename(bean.getFieldByName(mapper("userename")).asString());
	setUseresurname(bean.getFieldByName(mapper("useresurname")).asString());
	setEmail(bean.getFieldByName(mapper("email")).asString());
	setGender(bean.getFieldByName(mapper("gender")).asString());
	setMobile(bean.getFieldByName(mapper("mobile")).asString());
	setLineno(bean.getFieldByName(mapper("lineno")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setUsername(rs.getString("username"));
	setSite(rs.getString("site"));
	setUserbranch(rs.getString("userbranch"));
	setUsertname(rs.getString("usertname"));
	setUsertsurname(rs.getString("usertsurname"));
	setUserename(rs.getString("userename"));
	setUseresurname(rs.getString("useresurname"));
	setEmail(rs.getString("email"));
	setGender(rs.getString("gender"));
	setMobile(rs.getString("mobile"));
	setLineno(rs.getString("lineno"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("username",getUsername());
	sql.setParameter("site",getSite());
	sql.setParameter("userbranch",getUserbranch());
	sql.setParameter("usertname",getUsertname());
	sql.setParameter("usertsurname",getUsertsurname());
	sql.setParameter("userename",getUserename());
	sql.setParameter("useresurname",getUseresurname());
	sql.setParameter("email",getEmail());
	sql.setParameter("gender",getGender());
	sql.setParameter("mobile",getMobile());
	sql.setParameter("lineno",getLineno());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("userid",getUserid());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	KnSQL knsql = new KnSQL(this);
	knsql.append("update tuserinfo set usertname=?usertname, usertsurname=?usertsurname, ");
	knsql.append("userename=?userename, useresurname=?useresurname, ");
	knsql.append("email=?email, mobile=?mobile, lineno=?lineno ");
	knsql.append("where userid=?userid ");
	knsql.setParameter("userid",getUserid());
	knsql.setParameter("usertname",getUsertname());
	knsql.setParameter("usertsurname",getUsertsurname());
	knsql.setParameter("userename",getUserename());
	knsql.setParameter("useresurname",getUseresurname());
	knsql.setParameter("email",getEmail());
	knsql.setParameter("mobile",getMobile());
	knsql.setParameter("lineno",getLineno());
	knsql.executeUpdate(connection);
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	sql.clear();
	sql.append("select tuser.*,");
	sql.append("tuserinfo.userbranch,tuserinfo.usertname,tuserinfo.usertsurname,");
	sql.append("tuserinfo.userename,tuserinfo.useresurname,tuserinfo.supervisor,");
	sql.append("tuserinfo.email,tuserinfo.gender,tuserinfo.mobile,tuserinfo.lineno ");
	sql.append("from tuser,tuserinfo ");
	sql.append("where tuser.userid = tuserinfo.userid ");
	sql.append("and tuser.userid = ?userid ");
	sql.setParameter("userid",getUserid());
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE005PData aSFTE005PData = new SFTE005PData();
		aSFTE005PData.fetchResult(rs);
		add(aSFTE005PData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
