package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE003Bean.java
 * Description: SFTE003Bean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Sun Sep 23 11:31:56 ICT 2018
 */
/**
 * SFTE003Bean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
@SuppressWarnings("serial")
public class SFTE003Bean extends BeanSchema {

	public SFTE003Bean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("product",java.sql.Types.VARCHAR,"product");
		addSchema("nameen",java.sql.Types.VARCHAR,"nameen");
		addSchema("nameth",java.sql.Types.VARCHAR,"nameth");
		addSchema("verified",java.sql.Types.VARCHAR,"verified");
		collectClass(com.fs.bean.SFTE003ABean.class);
	}
	public String getProduct() {
		return getMember("product");
	}
	public void setProduct(String newProduct) {
		setMember("product",newProduct);
	}
	public String getNameen() {
		return getMember("nameen");
	}
	public void setNameen(String newNameen) {
		setMember("nameen",newNameen);
	}
	public String getNameth() {
		return getMember("nameth");
	}
	public void setNameth(String newNameth) {
		setMember("nameth",newNameth);
	}
	public String getVerified() {
		return getMember("verified");
	}
	public void setVerified(String newVerified) {
		setMember("verified",newVerified);
	}
}
