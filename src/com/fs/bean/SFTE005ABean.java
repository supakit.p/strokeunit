package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005ABean.java
 * Description: SFTE005ABean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Wed Jan 03 09:44:25 ICT 2018
 */
/**
 * SFTE005ABean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class SFTE005ABean extends BeanSchema {

	public SFTE005ABean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("userid",java.sql.Types.VARCHAR,"userid");
		addSchema("username",java.sql.Types.VARCHAR,"username");
		addSchema("site",java.sql.Types.VARCHAR,"site");
		addSchema("userbranch",java.sql.Types.VARCHAR,"userbranch");
		addSchema("usertname",java.sql.Types.VARCHAR,"usertname");
		addSchema("usertsurname",java.sql.Types.VARCHAR,"usertsurname");
		addSchema("userename",java.sql.Types.VARCHAR,"userename");
		addSchema("useresurname",java.sql.Types.VARCHAR,"useresurname");
		addSchema("startdate",java.sql.Types.DATE,"startdate");
		addSchema("enddate",java.sql.Types.DATE,"enddate");
		addSchema("status",java.sql.Types.VARCHAR,"status");
		addSchema("userpassword",java.sql.Types.VARCHAR,"userpassword");
		addSchema("passwordexpiredate",java.sql.Types.DATE,"passwordexpiredate");
		addSchema("supervisor",java.sql.Types.VARCHAR,"supervisor");
		addSchema("photoimage",java.sql.Types.VARCHAR,"photoimage");
		addSchema("adminflag",java.sql.Types.VARCHAR,"adminflag");
		addSchema("firstpage",java.sql.Types.VARCHAR,"firstpage");
		addSchema("theme",java.sql.Types.VARCHAR,"theme");
		addSchema("showphoto",java.sql.Types.VARCHAR,"showphoto");
		addSchema("loginfailtimes",java.sql.Types.TINYINT,"loginfailtimes");
		addSchema("lockflag",java.sql.Types.VARCHAR,"lockflag");
		addSchema("usertype",java.sql.Types.VARCHAR,"usertype");
		addSchema("iconfile",java.sql.Types.VARCHAR,"iconfile");
		addSchema("accessdate",java.sql.Types.DATE,"accessdate");
		addSchema("accesstime",java.sql.Types.TIME,"accesstime");
		addSchema("accesshits",java.sql.Types.BIGINT,"accesshits");
		addSchema("failtime",java.sql.Types.BIGINT,"failtime");
		addSchema("editdate",java.sql.Types.DATE,"editdate");
		addSchema("edittime",java.sql.Types.TIME,"edittime");
		addSchema("email",java.sql.Types.VARCHAR,"email");
		addSchema("gender",java.sql.Types.VARCHAR,"gender");
		addSchema("mobile",java.sql.Types.VARCHAR,"mobile");
		addSchema("lineno",java.sql.Types.VARCHAR,"lineno");
		addSchema("sitedesc",java.sql.Types.VARCHAR,"sitedesc");
	}
	public String getUserid() {
		return getMember("userid");
	}
	public void setUserid(String newUserid) {
		setMember("userid",newUserid);
	}
	public String getUsername() {
		return getMember("username");
	}
	public void setUsername(String newUsername) {
		setMember("username",newUsername);
	}
	public String getSite() {
		return getMember("site");
	}
	public void setSite(String newSite) {
		setMember("site",newSite);
	}
	public String getSitedesc() {
		return getMember("sitedesc");
	}
	public void setSitedesc(String newSitedesc) {
		setMember("sitedesc",newSitedesc);
	}
	public String getUserbranch() {
		return getMember("userbranch");
	}
	public void setUserbranch(String newUserbranch) {
		setMember("userbranch",newUserbranch);
	}
	public String getUsertname() {
		return getMember("usertname");
	}
	public void setUsertname(String newUsertname) {
		setMember("usertname",newUsertname);
	}
	public String getUsertsurname() {
		return getMember("usertsurname");
	}
	public void setUsertsurname(String newUsertsurname) {
		setMember("usertsurname",newUsertsurname);
	}
	public String getUserename() {
		return getMember("userename");
	}
	public void setUserename(String newUserename) {
		setMember("userename",newUserename);
	}
	public String getUseresurname() {
		return getMember("useresurname");
	}
	public void setUseresurname(String newUseresurname) {
		setMember("useresurname",newUseresurname);
	}
	public String getStartdate() {
		return getMember("startdate");
	}
	public void setStartdate(String newStartdate) {
		setMember("startdate",newStartdate);
	}
	public String getEnddate() {
		return getMember("enddate");
	}
	public void setEnddate(String newEnddate) {
		setMember("enddate",newEnddate);
	}
	public String getStatus() {
		return getMember("status");
	}
	public void setStatus(String newStatus) {
		setMember("status",newStatus);
	}
	public String getUserpassword() {
		return getMember("userpassword");
	}
	public void setUserpassword(String newUserpassword) {
		setMember("userpassword",newUserpassword);
	}
	public String getPasswordexpiredate() {
		return getMember("passwordexpiredate");
	}
	public void setPasswordexpiredate(String newPasswordexpiredate) {
		setMember("passwordexpiredate",newPasswordexpiredate);
	}
	public String getSupervisor() {
		return getMember("supervisor");
	}
	public void setSupervisor(String newSupervisor) {
		setMember("supervisor",newSupervisor);
	}
	public String getPhotoimage() {
		return getMember("photoimage");
	}
	public void setPhotoimage(String newPhotoimage) {
		setMember("photoimage",newPhotoimage);
	}
	public String getAdminflag() {
		return getMember("adminflag");
	}
	public void setAdminflag(String newAdminflag) {
		setMember("adminflag",newAdminflag);
	}
	public String getFirstpage() {
		return getMember("firstpage");
	}
	public void setFirstpage(String newFirstpage) {
		setMember("firstpage",newFirstpage);
	}
	public String getTheme() {
		return getMember("theme");
	}
	public void setTheme(String newTheme) {
		setMember("theme",newTheme);
	}
	public String getShowphoto() {
		return getMember("showphoto");
	}
	public void setShowphoto(String newShowphoto) {
		setMember("showphoto",newShowphoto);
	}
	public String getLoginfailtimes() {
		return getMember("loginfailtimes");
	}
	public void setLoginfailtimes(String newLoginfailtimes) {
		setMember("loginfailtimes",newLoginfailtimes);
	}
	public String getLockflag() {
		return getMember("lockflag");
	}
	public void setLockflag(String newLockflag) {
		setMember("lockflag",newLockflag);
	}
	public String getUsertype() {
		return getMember("usertype");
	}
	public void setUsertype(String newUsertype) {
		setMember("usertype",newUsertype);
	}
	public String getIconfile() {
		return getMember("iconfile");
	}
	public void setIconfile(String newIconfile) {
		setMember("iconfile",newIconfile);
	}
	public String getAccessdate() {
		return getMember("accessdate");
	}
	public void setAccessdate(String newAccessdate) {
		setMember("accessdate",newAccessdate);
	}
	public String getAccesstime() {
		return getMember("accesstime");
	}
	public void setAccesstime(String newAccesstime) {
		setMember("accesstime",newAccesstime);
	}
	public String getAccesshits() {
		return getMember("accesshits");
	}
	public void setAccesshits(String newAccesshits) {
		setMember("accesshits",newAccesshits);
	}
	public String getFailtime() {
		return getMember("failtime");
	}
	public void setFailtime(String newFailtime) {
		setMember("failtime",newFailtime);
	}
	public String getEditdate() {
		return getMember("editdate");
	}
	public void setEditdate(String newEditdate) {
		setMember("editdate",newEditdate);
	}
	public String getEdittime() {
		return getMember("edittime");
	}
	public void setEdittime(String newEdittime) {
		setMember("edittime",newEdittime);
	}
	public String getEmail() {
		return getMember("email");
	}
	public void setEmail(String newEmail) {
		setMember("email",newEmail);
	}
	public String getGender() {
		return getMember("gender");
	}
	public void setGender(String newGender) {
		setMember("gender",newGender);
	}
	public String getMobile() {
		return getMember("mobile");
	}
	public void setMobile(String newMobile) {
		setMember("mobile",newMobile);
	}
	public String getLineno() {
		return getMember("lineno");
	}
	public void setLineno(String newLineno) {
		setMember("lineno",newLineno);
	}
}
