package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005CData.java
 * Description: SFTE005CData class implements for handle tuser data base schema.
 * Version: $Revision$
 * Creation date: Wed Jan 03 14:38:21 ICT 2018
 */
/**
 * SFTE005CData class implements for handle tuser data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.auth.PasswordLibrary;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused", "unchecked"})
public class SFTE005CData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE005CData() {
	super();
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("username",getUsername());
	sql.setParameter("site",getSite());
	sql.setParameter("userpassword",getUserpassword());
	sql.setParameter("passwordexpiredate",getPasswordexpiredate());
	sql.setParameter("changeflag", getChangeflag());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
protected void initialize() {
	super.initialize();
	setTable("tuser");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("username",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("userpassword",java.sql.Types.VARCHAR);
	addSchema("passwordexpiredate",java.sql.Types.DATE);
	addSchema("changeflag",java.sql.Types.VARCHAR);
	map("site","site");
	map("passwordexpiredate","passwordexpiredate");
	map("userpassword","userpassword");
	map("username","username");
	map("userid","userid");
	map("changeflag","changeflag");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	addSchema("oldpassword",java.sql.Types.VARCHAR,true);
	addSchema("confirmpassword",java.sql.Types.VARCHAR,true);
	addSchema("checkflag",java.sql.Types.VARCHAR,true);
	map("oldpassword","oldpassword");
	map("confirmpassword","confirmpassword");
	map("checkflag","checkflag");
	//#(30000) programmer code end;
}
public int check(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	int result = 0;
	String userpassword = getUserpassword();
	if(userpassword==null || userpassword.trim().length()<=0) {
		throw new BeanException("Password is undefined",-8995,global==null?"EN":global.getFsLanguage());
	}
	setKeyField("userid");
	String usrpwd = null;
	KnSQL knsql = new KnSQL(this);
	knsql.append("select userpassword from tuser where userid=?userid");
	knsql.setParameter("userid",getUserid());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		if(rs.next()) {
			result++;
			usrpwd = rs.getString("userpassword");
		}
	}
	if(result==0) throw new BeanException("User does not existed",-8895,global==null?"EN":global.getFsLanguage());
	PasswordLibrary plib = new PasswordLibrary(this);
	if(!userpassword.equals(usrpwd)) {
		if (userpassword.length() > 8) {
			throw new BeanException("Password mismatch",-8996,global==null?"EN":global.getFsLanguage());
		}
		if(!plib.checkPassword(userpassword, usrpwd)) {
			throw new BeanException("Password mismatch",-8996,global==null?"EN":global.getFsLanguage());
		}
	}
	return result;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE005CData aSFTE005CData = new SFTE005CData();
		aSFTE005CData.fetchResult(rs);
		add(aSFTE005CData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("userid",getUserid());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setUsername(rs.getString("username"));
	setSite(rs.getString("site"));
	setUserpassword(rs.getString("userpassword"));
	setPasswordexpiredate(rs.getDate("passwordexpiredate"));
	setChangeflag(rs.getString("changeflag"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE005CData.class+"=$Revision$\n";
}
public String getChangeflag() {
	return getString("changeflag");
}
public String getCheckflag() {
	return getString("checkflag");
}
public String getConfirmpassword() {
	return getString("confirmpassword");
}
public String getOldpassword() {
	return getString("oldpassword");
}
public Date getPasswordexpiredate() {
	return getDate("passwordexpiredate");
}
public String getSite() {
	return getString("site");
}
public String getUserid() {
	return getString("userid");
}
public String getUsername() {
	return getString("username");
}
public String getUserpassword() {
	return getString("userpassword");
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUsername(bean.getFieldByName(mapper("username")).asString());
	setSite(bean.getFieldByName(mapper("site")).asString());
	setUserpassword(bean.getFieldByName(mapper("userpassword")).asString());
	setPasswordexpiredate(bean.getFieldByName(mapper("passwordexpiredate")).asDate());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	setOldpassword(bean.getFieldByName(mapper("oldpassword")).asString());
	setConfirmpassword(bean.getFieldByName(mapper("confirmpassword")).asString());
	setCheckflag(bean.getFieldByName(mapper("checkflag")).asString());
	if(getCheckflag()==null || getCheckflag().trim().length()<=0) setCheckflag("1");
	//#(40000) programmer code end;
	return super.obtain(bean);
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public int process(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar,String action) throws Exception {
	if("reset".equalsIgnoreCase(action)) {
		return reset(connection,centerConnection,globalConnection,transientVar);
	}
	if("check".equalsIgnoreCase(action)) {
		return check(connection,centerConnection,globalConnection,transientVar);
	}
	return super.process(connection, centerConnection, globalConnection, transientVar, action);
}
public int reset(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("username");
	removeKeyField("userid");
	boolean found = false;
	KnSQL knsql = new KnSQL(this);
	knsql.append("select userid,userpassword from tuser where username = ?username");
	knsql.setParameter("username",getUsername());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		if(rs.next()) {
			found = true;
		}
	}
	if(!found) throw new BeanException("User name does not existed",-8991,global==null?"EN":global.getFsLanguage());
	PasswordLibrary plib = new PasswordLibrary(this);
	String password = getUserpassword();
	if (password.length() > 8) {
		throw new BeanException("Password length not over 8 characters",-8896,global==null?"EN":global.getFsLanguage());
	}
	if(password.equals(getUsername())) {
		throw new BeanException("Not allow password as same as user name",-8994,global==null?"EN":global.getFsLanguage());
	}
	password = plib.encrypt(password);
	setUserpassword(password);
	setChangeflag("0");
	ExecuteStatement sql = createQueryForUpdate(connection);
	assignParameters(sql);
	return sql.executeUpdate(connection);
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public void setChangeflag(String newChangeflag) {
	setMember("changeflag",newChangeflag);
}
public void setCheckflag(String newCheckflag) {
	setMember("checkflag",newCheckflag);
}
public void setConfirmpassword(String newConfirmpassword) {
	setMember("confirmpassword",newConfirmpassword);
}
public void setOldpassword(String newOldpassword) {
	setMember("oldpassword",newOldpassword);
}
public void setPasswordexpiredate(Date newPasswordexpiredate) {
	setMember("passwordexpiredate",newPasswordexpiredate);
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
public void setUserpassword(String newUserpassword) {
	setMember("userpassword",newUserpassword);
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	String username = null;
	String usrpwd = null;
	boolean found = false;
	KnSQL knsql = new KnSQL(this);
	knsql.append("select username,userpassword from tuser where userid=?userid");
	knsql.setParameter("userid",getUserid());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		if(rs.next()) {
			found = true;
			username = rs.getString("username");
			usrpwd = rs.getString("userpassword");
		}
	}
	if(!found) throw new BeanException("User does not existed",-8895,global==null?"EN":global.getFsLanguage());
	
	PasswordLibrary plib = new PasswordLibrary(this);
	if(!"0".equals(getCheckflag())) {
		if(!getOldpassword().equals(usrpwd)) {
			String opassword = getOldpassword();
			if (opassword.length() > 8) {
				opassword = opassword.substring(0,8);
			}			
			if(!plib.checkPassword(opassword, usrpwd)) {
				throw new BeanException("Password mismatch",-8894,global==null?"EN":global.getFsLanguage());
			}
		}	
	}
	String password = getUserpassword();
	if(password.equals(getUserid())) throw new BeanException("Not allow password as same as user",-8899,global==null?"EN":global.getFsLanguage());
	if(password.equals(username)) throw new BeanException("Not allow password as same as user",-8994,global==null?"EN":global.getFsLanguage());
	if (password.length() > 8) {
		throw new BeanException("Password length not over 8 characters",-8896,global==null?"EN":global.getFsLanguage());
	}
	password = plib.encrypt(password);
	setUserpassword(password);
	setChangeflag("0");
	if(getPasswordexpiredate()==null) {
		setPasswordexpiredate(plib.getUserExpireDate(connection, getUserid(), null));
	}
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public BeanException validateDatas(String fs_language) {
	if(getUserid()==null || getUserid().trim().length()<=0) {
		return new BeanException("User is undefined",-8891,fs_language);
	}
	if(!"0".equals(getCheckflag())) {
		if(!getUserpassword().equals(getConfirmpassword())) {
			return new BeanException("Confirm Password Mismatch",-8897,fs_language);
		}
	}
	if(getUserpassword().equals(getUserid())) {
		return new BeanException("Not allow password as same as user",-8899,fs_language);
	}
	if(getUserpassword().length()>8) {
		return new BeanException("Password length not over 8 characters",-8896,fs_language);
	}	
	return null;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public BeanException validateResetDatas(String fs_language) {
	if(getUsername()==null || getUsername().trim().length()<=0) {
		return new BeanException("User is undefined",-8891,fs_language);
	}
	if(getUserpassword().equals(getUsername())) {
		return new BeanException("Not allow password as same as user name",-8994,fs_language);
	}
	if(getUserpassword().length()>8) {
		return new BeanException("Password length not over 8 characters",-8896,fs_language);
	}	
	return null;
}		
//#(100000) programmer code end;		
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
}
