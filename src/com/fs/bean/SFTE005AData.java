package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005AData.java
 * Description: SFTE005AData class implements for handle tuser data base schema.
 * Version: $Revision$
 * Creation date: Wed Jan 03 10:00:14 ICT 2018
 */
/**
 * SFTE005AData class implements for handle tuser data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.auth.PasswordLibrary;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE005AData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE005AData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tuser");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("username",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("startdate",java.sql.Types.DATE);
	addSchema("enddate",java.sql.Types.DATE);
	addSchema("status",java.sql.Types.VARCHAR);
	addSchema("userpassword",java.sql.Types.VARCHAR);
	addSchema("passwordexpiredate",java.sql.Types.DATE);
	addSchema("photoimage",java.sql.Types.VARCHAR,true);
	addSchema("adminflag",java.sql.Types.VARCHAR);
	addSchema("firstpage",java.sql.Types.VARCHAR);
	addSchema("theme",java.sql.Types.VARCHAR);
	addSchema("showphoto",java.sql.Types.VARCHAR);
	addSchema("loginfailtimes",java.sql.Types.TINYINT);
	addSchema("lockflag",java.sql.Types.VARCHAR);
	addSchema("usertype",java.sql.Types.VARCHAR);
	addSchema("iconfile",java.sql.Types.VARCHAR);
	addSchema("accessdate",java.sql.Types.DATE);
	addSchema("accesstime",java.sql.Types.TIME);
	addSchema("accesshits",java.sql.Types.BIGINT);
	addSchema("failtime",java.sql.Types.BIGINT);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("userbranch",java.sql.Types.VARCHAR,true);
	addSchema("usertname",java.sql.Types.VARCHAR,true);
	addSchema("usertsurname",java.sql.Types.VARCHAR,true);
	addSchema("userename",java.sql.Types.VARCHAR,true);
	addSchema("useresurname",java.sql.Types.VARCHAR,true);
	addSchema("supervisor",java.sql.Types.VARCHAR,true);
	addSchema("email",java.sql.Types.VARCHAR,true);
	addSchema("gender",java.sql.Types.VARCHAR,true);
	addSchema("mobile",java.sql.Types.VARCHAR,true);
	addSchema("lineno",java.sql.Types.VARCHAR,true);
	addSchema("sitedesc",java.sql.Types.VARCHAR,true);
	map("showphoto","showphoto");
	map("accesstime","accesstime");
	map("iconfile","iconfile");
	map("accessdate","accessdate");
	map("site","site");
	map("lockflag","lockflag");
	map("adminflag","adminflag");
	map("passwordexpiredate","passwordexpiredate");
	map("startdate","startdate");
	map("usertype","usertype");
	map("loginfailtimes","loginfailtimes");
	map("edittime","edittime");
	map("userpassword","userpassword");
	map("useresurname","useresurname");
	map("email","email");
	map("editdate","editdate");
	map("usertname","usertname");
	map("userbranch","userbranch");
	map("enddate","enddate");
	map("userename","userename");
	map("status","status");
	map("theme","theme");
	map("usertsurname","usertsurname");
	map("mobile","mobile");
	map("username","username");
	map("firstpage","firstpage");
	map("supervisor","supervisor");
	map("accesshits","accesshits");
	map("failtime","failtime");
	map("userid","userid");
	map("photoimage","photoimage");
	map("gender","gender");
	map("lineno","lineno");
	map("sitedesc", "sitedesc");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE005AData.class+"=$Revision$\n";
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getUsername() {
	return getString("username");
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getSitedesc() {
	return getString("sitedesc");
}
public void setSitedesc(String newSitedesc) {
	setMember("sitedesc",newSitedesc);
}
public String getUserbranch() {
	return getString("userbranch");
}
public void setUserbranch(String newUserbranch) {
	setMember("userbranch",newUserbranch);
}
public String getUsertname() {
	return getString("usertname");
}
public void setUsertname(String newUsertname) {
	setMember("usertname",newUsertname);
}
public String getUsertsurname() {
	return getString("usertsurname");
}
public void setUsertsurname(String newUsertsurname) {
	setMember("usertsurname",newUsertsurname);
}
public String getUserename() {
	return getString("userename");
}
public void setUserename(String newUserename) {
	setMember("userename",newUserename);
}
public String getUseresurname() {
	return getString("useresurname");
}
public void setUseresurname(String newUseresurname) {
	setMember("useresurname",newUseresurname);
}
public Date getStartdate() {
	return getDate("startdate");
}
public void setStartdate(Date newStartdate) {
	setMember("startdate",newStartdate);
}
public Date getEnddate() {
	return getDate("enddate");
}
public void setEnddate(Date newEnddate) {
	setMember("enddate",newEnddate);
}
public String getStatus() {
	return getString("status");
}
public void setStatus(String newStatus) {
	setMember("status",newStatus);
}
public String getUserpassword() {
	return getString("userpassword");
}
public void setUserpassword(String newUserpassword) {
	setMember("userpassword",newUserpassword);
}
public Date getPasswordexpiredate() {
	return getDate("passwordexpiredate");
}
public void setPasswordexpiredate(Date newPasswordexpiredate) {
	setMember("passwordexpiredate",newPasswordexpiredate);
}
public String getSupervisor() {
	return getString("supervisor");
}
public void setSupervisor(String newSupervisor) {
	setMember("supervisor",newSupervisor);
}
public String getPhotoimage() {
	return getString("photoimage");
}
public void setPhotoimage(String newPhotoimage) {
	setMember("photoimage",newPhotoimage);
}
public String getAdminflag() {
	return getString("adminflag");
}
public void setAdminflag(String newAdminflag) {
	setMember("adminflag",newAdminflag);
}
public String getFirstpage() {
	return getString("firstpage");
}
public void setFirstpage(String newFirstpage) {
	setMember("firstpage",newFirstpage);
}
public String getTheme() {
	return getString("theme");
}
public void setTheme(String newTheme) {
	setMember("theme",newTheme);
}
public String getShowphoto() {
	return getString("showphoto");
}
public void setShowphoto(String newShowphoto) {
	setMember("showphoto",newShowphoto);
}
public int getLoginfailtimes() {
	return getInt("loginfailtimes");
}
public void setLoginfailtimes(int newLoginfailtimes) {
	setMember("loginfailtimes",newLoginfailtimes);
}
public String getLockflag() {
	return getString("lockflag");
}
public void setLockflag(String newLockflag) {
	setMember("lockflag",newLockflag);
}
public String getUsertype() {
	return getString("usertype");
}
public void setUsertype(String newUsertype) {
	setMember("usertype",newUsertype);
}
public String getIconfile() {
	return getString("iconfile");
}
public void setIconfile(String newIconfile) {
	setMember("iconfile",newIconfile);
}
public Date getAccessdate() {
	return getDate("accessdate");
}
public void setAccessdate(Date newAccessdate) {
	setMember("accessdate",newAccessdate);
}
public Time getAccesstime() {
	return getTime("accesstime");
}
public void setAccesstime(Time newAccesstime) {
	setMember("accesstime",newAccesstime);
}
public long getAccesshits() {
	return getLong("accesshits");
}
public void setAccesshits(long newAccesshits) {
	setMember("accesshits",newAccesshits);
}
public long getFailtime() {
	return getLong("failtime");
}
public void setFailtime(long newFailtime) {
	setMember("failtime",newFailtime);
}
public Date getEditdate() {
	return getDate("editdate");
}
public void setEditdate(Date newEditdate) {
	setMember("editdate",newEditdate);
}
public Time getEdittime() {
	return getTime("edittime");
}
public void setEdittime(Time newEdittime) {
	setMember("edittime",newEdittime);
}
public String getEmail() {
	return getString("email");
}
public void setEmail(String newEmail) {
	setMember("email",newEmail);
}
public String getGender() {
	return getString("gender");
}
public void setGender(String newGender) {
	setMember("gender",newGender);
}
public String getMobile() {
	return getString("mobile");
}
public void setMobile(String newMobile) {
	setMember("mobile",newMobile);
}
public String getLineno() {
	return getString("lineno");
}
public void setLineno(String newLineno) {
	setMember("lineno",newLineno);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUsername(bean.getFieldByName(mapper("username")).asString());
	setSite(bean.getFieldByName(mapper("site")).asString());
	setUserbranch(bean.getFieldByName(mapper("userbranch")).asString());
	setUsertname(bean.getFieldByName(mapper("usertname")).asString());
	setUsertsurname(bean.getFieldByName(mapper("usertsurname")).asString());
	setUserename(bean.getFieldByName(mapper("userename")).asString());
	setUseresurname(bean.getFieldByName(mapper("useresurname")).asString());
	setStartdate(bean.getFieldByName(mapper("startdate")).asDate());
	setEnddate(bean.getFieldByName(mapper("enddate")).asDate());
	setStatus(bean.getFieldByName(mapper("status")).asString());
	setUserpassword(bean.getFieldByName(mapper("userpassword")).asString());
	setPasswordexpiredate(bean.getFieldByName(mapper("passwordexpiredate")).asDate());
	setSupervisor(bean.getFieldByName(mapper("supervisor")).asString());
	setPhotoimage(bean.getFieldByName(mapper("photoimage")).asString());
	setAdminflag(bean.getFieldByName(mapper("adminflag")).asString());
	setFirstpage(bean.getFieldByName(mapper("firstpage")).asString());
	setTheme(bean.getFieldByName(mapper("theme")).asString());
	setShowphoto(bean.getFieldByName(mapper("showphoto")).asString());
	setLoginfailtimes(bean.getFieldByName(mapper("loginfailtimes")).asInt());
	setLockflag(bean.getFieldByName(mapper("lockflag")).asString());
	setUsertype(bean.getFieldByName(mapper("usertype")).asString());
	setIconfile(bean.getFieldByName(mapper("iconfile")).asString());
	setAccessdate(bean.getFieldByName(mapper("accessdate")).asDate());
	setAccesstime(bean.getFieldByName(mapper("accesstime")).asTime());
	setAccesshits(bean.getFieldByName(mapper("accesshits")).asLong());
	setFailtime(bean.getFieldByName(mapper("failtime")).asLong());
	setEditdate(bean.getFieldByName(mapper("editdate")).asDate());
	setEdittime(bean.getFieldByName(mapper("edittime")).asTime());
	setEmail(bean.getFieldByName(mapper("email")).asString());
	setGender(bean.getFieldByName(mapper("gender")).asString());
	setMobile(bean.getFieldByName(mapper("mobile")).asString());
	setLineno(bean.getFieldByName(mapper("lineno")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	if(getSite()==null || getSite().trim().length()<=0) {
		if(global!=null) setSite(global.getFsSite());
	}
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setUsername(rs.getString("username"));
	setSite(rs.getString("site"));
	setStartdate(rs.getDate("startdate"));
	setEnddate(rs.getDate("enddate"));
	setStatus(rs.getString("status"));
	setUserpassword(rs.getString("userpassword"));
	setPasswordexpiredate(rs.getDate("passwordexpiredate"));
	setSupervisor(rs.getString("supervisor"));
	setPhotoimage(rs.getString("photoimage"));
	setAdminflag(rs.getString("adminflag"));
	setFirstpage(rs.getString("firstpage"));
	setTheme(rs.getString("theme"));
	setShowphoto(rs.getString("showphoto"));
	setLoginfailtimes(rs.getInt("loginfailtimes"));
	setLockflag(rs.getString("lockflag"));
	setUsertype(rs.getString("usertype"));
	setIconfile(rs.getString("iconfile"));
	setAccessdate(rs.getDate("accessdate"));
	setAccesstime(rs.getTime("accesstime"));
	setAccesshits(rs.getLong("accesshits"));
	setFailtime(rs.getLong("failtime"));
	setEditdate(rs.getDate("editdate"));
	setEdittime(rs.getTime("edittime"));
	setUserbranch(rs.getString("userbranch"));
	setUsertname(rs.getString("usertname"));
	setUsertsurname(rs.getString("usertsurname"));
	setUserename(rs.getString("userename"));
	setUseresurname(rs.getString("useresurname"));
	setEmail(rs.getString("email"));
	setGender(rs.getString("gender"));
	setMobile(rs.getString("mobile"));
	setLineno(rs.getString("lineno"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("username",getUsername());
	sql.setParameter("site",getSite());
	sql.setParameter("userbranch",getUserbranch());
	sql.setParameter("usertname",getUsertname());
	sql.setParameter("usertsurname",getUsertsurname());
	sql.setParameter("userename",getUserename());
	sql.setParameter("useresurname",getUseresurname());
	sql.setParameter("startdate",getStartdate());
	sql.setParameter("enddate",getEnddate());
	sql.setParameter("status",getStatus());
	sql.setParameter("userpassword",getUserpassword());
	sql.setParameter("passwordexpiredate",getPasswordexpiredate());
	sql.setParameter("supervisor",getSupervisor());
	sql.setParameter("photoimage",getPhotoimage());
	sql.setParameter("adminflag",getAdminflag());
	sql.setParameter("firstpage",getFirstpage());
	sql.setParameter("theme",getTheme());
	sql.setParameter("showphoto",getShowphoto());
	sql.setParameter("loginfailtimes",getLoginfailtimes());
	sql.setParameter("lockflag",getLockflag());
	sql.setParameter("usertype",getUsertype());
	sql.setParameter("iconfile",getIconfile());
	sql.setParameter("accessdate",getAccessdate());
	sql.setParameter("accesstime",getAccesstime());
	sql.setParameter("accesshits",getAccesshits());
	sql.setParameter("failtime",getFailtime());
	sql.setParameter("editdate",getEditdate());
	sql.setParameter("edittime",getEdittime());
	sql.setParameter("email",getEmail());
	sql.setParameter("gender",getGender());
	sql.setParameter("mobile",getMobile());
	sql.setParameter("lineno",getLineno());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	boolean found = false;
	KnSQL knsql = new KnSQL(this);
	knsql.append("select userid from tuser where userid=?userid");
	knsql.setParameter("userid",getUserid());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		if(rs.next()) {
			found = true;
		}
	}
	if(found) throw new BeanException("User ID is already existed",-8898);
	knsql.clear();
	knsql.append("insert into tuserinfo(site,employeeid,userid,userename,useresurname,usertname,usertsurname,email,gender,mobile,lineno) ");
	knsql.append("values(?site,?employeeid,?userid,?userename,?useresurname,?usertname,?usertsurname,?email,?gender,?mobile,?lineno) ");
	knsql.setParameter("site",getSite());
	//knsql.setParameter("employeeid",com.fs.bean.dba.DBAUtility.createID());
	knsql.setParameter("employeeid",getUserid());
	knsql.setParameter("userid",getUserid());
	knsql.setParameter("usertname",getUsertname());
	knsql.setParameter("usertsurname",getUsertsurname());
	knsql.setParameter("userename",getUserename());
	knsql.setParameter("useresurname",getUseresurname());
	knsql.setParameter("email",getEmail());
	knsql.setParameter("gender", getGender());
	knsql.setParameter("mobile",getMobile());
	knsql.setParameter("lineno",getLineno());
	knsql.executeUpdate(connection);
	if(getStartdate()==null) setStartdate(new java.sql.Date(System.currentTimeMillis()));
	setEditdate(new java.sql.Date(System.currentTimeMillis()));
	setEdittime(new java.sql.Time(System.currentTimeMillis()));	
	if(getStatus()==null || getStatus().trim().length()<=0) {
		setStatus("A");
	}
	if(getUserpassword()==null || getUserpassword().trim().length()<=0) {
		setUserpassword("password");
	}
	PasswordLibrary plib = new PasswordLibrary(this);
	String password = getUserpassword();
	if (password.length() > 8) {
		password = password.substring(0,8);
	}
	password = plib.encrypt(password);
	setUserpassword(password);
	setUsername(getUserid());
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	KnSQL knsql = new KnSQL(this);
	knsql.append("insert into tuserinfohistory ");
	knsql.append("select * from tuserinfo where site = ?site and userid = ?userid ");
	knsql.setParameter("site",getSite());
	knsql.setParameter("userid",getUserid());
	knsql.executeUpdate(connection);
	knsql.clear();
	knsql.append("delete from tuserinfo where site = ?site and userid = ?userid ");
	knsql.setParameter("site",getSite());
	knsql.setParameter("userid",getUserid());
	knsql.executeUpdate(connection);	
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("userid",getUserid());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	KnSQL knsql = new KnSQL(this);
	knsql.append("update tuserinfo set usertname=?usertname, usertsurname=?usertsurname, ");
	knsql.append("userename=?userename, useresurname=?useresurname, ");
	knsql.append("email=?email, gender=?gender, mobile=?mobile, lineno=?lineno ");
	knsql.append("where userid=?userid ");
	knsql.setParameter("userid",getUserid());
	knsql.setParameter("usertname",getUsertname());
	knsql.setParameter("usertsurname",getUsertsurname());
	knsql.setParameter("userename",getUserename());
	knsql.setParameter("useresurname",getUseresurname());
	knsql.setParameter("email",getEmail());
	knsql.setParameter("gender", getGender());
	knsql.setParameter("mobile",getMobile());
	knsql.setParameter("lineno",getLineno());
	knsql.executeUpdate(connection);
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE005AData aSFTE005AData = new SFTE005AData();
		aSFTE005AData.fetchResult(rs);
		add(aSFTE005AData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
