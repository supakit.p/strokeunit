package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005CBean.java
 * Description: SFTE005CBean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Wed Jan 03 14:36:29 ICT 2018
 */
/**
 * SFTE005CBean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.TheLibrary;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class SFTE005PinBean extends BeanSchema {

	public SFTE005PinBean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("userid",java.sql.Types.VARCHAR,"userid");
		addSchema("oldpincode",java.sql.Types.VARCHAR,"oldpincode");
		addSchema("pincode",java.sql.Types.VARCHAR,"pincode");
		addSchema("confirmpincode",java.sql.Types.VARCHAR,"confirmpincode");
		addSchema("checkflag",java.sql.Types.VARCHAR,"checkflag");
		addSchema("cardid",java.sql.Types.VARCHAR,"cardid");
		addSchema("birthday",java.sql.Types.DATE,"birthday");
		addSchema("mistakens",java.sql.Types.TINYINT,"mistakens");
		addSchema("mistakentime",java.sql.Types.BIGINT,"mistakentime");
	}
	public String getUserid() {
		return getMember("userid");
	}
	public void setUserid(String newUserid) {
		setMember("userid",newUserid);
	}
	public String getOldpincode() {
		return getMember("oldpincode");
	}
	public void setOldpincode(String newOldpincode) {
		setMember("oldpincode",newOldpincode);
	}
	public String getConfirmpincode() {
		return getMember("confirmpincode");
	}
	public void setConfirmpincode(String newConfirmpincode) {
		setMember("confirmpincode",newConfirmpincode);
	}
	public String getCheckflag() {
		return getMember("checkflag");
	}
	public void setCheckflag(String newCheckflag) {
		setMember("checkflag",newCheckflag);
	}	
	public String getPincode() {
		return getMember("pincode");
	}
	public void setPincode(String newPincode) {
		setMember("pincode",newPincode);
	}
	public String getCardid() {
		return getMember("cardid");
	}
	public void setCardid(String newCardid) {
		setMember("cardid",newCardid);
	}
	public String getBirthday() {
		return getMember("birthday");
	}
	public void setBirthday(String newBirthday) {
		setMember("birthday",newBirthday);
	}
	public String getMistakens() {
		return getMember("mistakens");
	}
	public void setMistakens(String newMistakens) {
		setMember("mistakens",newMistakens);
	}
	public String getMistakentime() {
		return getMember("mistakentime");
	}
	public void setMistakentime(String newMistakentime) {
		setMember("mistakentime",newMistakentime);
	}	
	public BeanException validateDatas(String fs_language) {
		if(!getPincode().equals(getConfirmpincode())) {
			return new BeanException("Confirm PIN Code Mismatch",-13213,fs_language);
		}
		return TheLibrary.validatePincode(getPincode(),getCardid(),getFieldByName("birthday").asDate(),fs_language);
	}	
}
