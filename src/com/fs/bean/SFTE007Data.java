package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: PMTE031Data.java
 * Description: PMTE031Data class implements for handle tuserinfo data base schema.
 * Version: $Revision$
 * Creation date: Thu Oct 18 10:52:44 ICT 2018
 */
/**
 * PMTE031Data class implements for handle tuserinfo data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.TheLibrary;
import com.fs.dev.TheUtility;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE007Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE007Data() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tuserinfo");
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("employeeid",java.sql.Types.VARCHAR);
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("usertname",java.sql.Types.VARCHAR);
	addSchema("usertsurname",java.sql.Types.VARCHAR);
	addSchema("userename",java.sql.Types.VARCHAR);
	addSchema("useresurname",java.sql.Types.VARCHAR);
	addSchema("employeecode",java.sql.Types.VARCHAR);
	map("usertname","usertname");
	map("userid","userid");
	map("site","site");
	map("usertsurname","usertsurname");
	map("employeecode","employeecode");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE007Data.class+"=$Revision$\n";
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getUsertname() {
	return getString("usertname");
}
public void setUsertname(String newUsertname) {
	setMember("usertname",newUsertname);
}
public String getUsertsurname() {
	return getString("usertsurname");
}
public void setUsertsurname(String newUsertsurname) {
	setMember("usertsurname",newUsertsurname);
}
public String getEmployeecode() {
	return getString("employeecode");
}
public void setEmployeecode(String newEmployeecode) {
	setMember("employeecode",newEmployeecode);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setSite(bean.getFieldByName(mapper("site")).asString());
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUsertname(bean.getFieldByName(mapper("usertname")).asString());
	setUsertsurname(bean.getFieldByName(mapper("usertsurname")).asString());
	setEmployeecode(bean.getFieldByName(mapper("employeecode")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setSite(rs.getString("site"));
	setUserid(rs.getString("userid"));
	setUsertname(rs.getString("usertname"));
	setUsertsurname(rs.getString("usertsurname"));
	setEmployeecode(rs.getString("employeecode"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("site",getSite());
	sql.setParameter("userid",getUserid());
	sql.setParameter("usertname",getUsertname());
	sql.setParameter("usertsurname",getUsertsurname());
	sql.setParameter("employeecode",getEmployeecode());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	boolean eng = TheUtility.isEnglish(global);
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	String filter = " where";
	sql.clear();
	sql.append("select tuserinfo.site,tuserinfo.userid,tuserinfo.userbranch,tuserinfo.usertname,");
	sql.append("tuserinfo.usertsurname,tuserinfo.userename,tuserinfo.useresurname,");
	sql.append("tuserinfo.employeecode,tuserinfo.employeeid,tuserinfo.email,tuserinfo.effectdate,");
	if(eng) {
		sql.append("tcomp.nameen as sitedesc ");
	} else {
		sql.append("tcomp.nameth as sitedesc ");
	}
	sql.append("from tuserinfo ");
	sql.append("left join tcomp on tcomp.site = tuserinfo.site ");
	if(getSite()!=null && getSite().trim().length()>0) {
		sql.append(filter+" tuserinfo.site = ?site ");
		sql.setParameter("site", getSite());
		filter = " and";
	}
	if(getEmployeecode()!=null && getEmployeecode().trim().length()>0) {
		sql.append(filter+" tuserinfo.employeecode LIKE ?employeecode ");
		sql.setParameter("employeecode", "%"+getEmployeecode()+"%");
		filter = " and";
	}
	if(getUserid()!=null && getUserid().trim().length()>0) {
		sql.append("and tuserinfo.userid LIKE ?userid ");
		sql.setParameter("userid", "%"+getUserid()+"%");
	}
	if(getUsertname()!=null && getUsertname().trim().length()>0) {
		sql.append("and tuserinfo.usertname LIKE ?usertname ");
		sql.setParameter("usertname","%"+getUsertname()+"%");
	}
	if(getUsertsurname()!=null && getUsertsurname().trim().length()>0) {
		sql.append("and tuserinfo.usertsurname LIKE ?usertsurname ");
		sql.setParameter("usertsurname","%"+getUsertsurname()+"%");
	}
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		com.fs.bean.SFTE007AData aPMTE031AData = new com.fs.bean.SFTE007AData();
		aPMTE031AData.fetchResult(rs);
		add(aPMTE031AData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		aPMTE031AData.setSitedesc(rs.getString("sitedesc"));
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
