package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Thu Sep 14 10:07:24 ICT 2017
 */

import com.fs.bean.misc.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
//#everybody in love
//#(5000) programmer code begin;
//#(5000) programmer code end;
public class SFTE102Bean extends BeanSchema {
//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTE102Bean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("api",java.sql.Types.VARCHAR,"API :");
	addSchema("client_time",java.sql.Types.TIMESTAMP,"client time :");
	addSchema("stroke_mobile_id",java.sql.Types.VARCHAR,"stroke mobile id :");
	addSchema("device_id",java.sql.Types.VARCHAR,"device id :");
	addSchema("sap_no",java.sql.Types.VARCHAR,"sap no :");
	addSchema("flag_data",java.sql.Types.VARCHAR,"Flag data:");
	addSchema("hn",java.sql.Types.VARCHAR,"HN:");
	addSchema("citizen_id",java.sql.Types.VARCHAR,"citizen id:");
	addSchema("name",java.sql.Types.VARCHAR,"name:");
	addSchema("age",java.sql.Types.VARCHAR,"age:");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	addSchema("screen",java.sql.Types.VARCHAR,"screen:");
	addSchema("tab",java.sql.Types.VARCHAR,"tab:");
	addSchema("part_screen",java.sql.Types.VARCHAR,"part_screen:");
	addSchema("ref_id",java.sql.Types.VARCHAR,"ref_id:");
	addSchema("authority_id",java.sql.Types.VARCHAR,"authority_id:");
	addSchema("data",java.sql.Types.VARCHAR,"data:");
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE102Bean.class+"=$Revision$\n";
}
public String getApi() {
	return getMember("api");
}
public void setApi(String newApi) {
	setMember("api",newApi);
}
public String getClient_time() {
	return getMember("client_time");
}
public void setClient_time(String newClient_time) {
	setMember("client_time",newClient_time);
}
public String getStroke_mobile_id() {
	return getMember("stroke_mobile_id");
}
public void setStroke_mobile_id(String newStroke_mobile_id) {
	setMember("stroke_mobile_id",newStroke_mobile_id);
}
public String getDevice_id() {
	return getMember("device_id");
}
public void setDevice_id(String newDevice_id) {
	setMember("device_id",newDevice_id);
}
public String getSap_no() {
	return getMember("sap_no");
}
public void setSap_no(String newSap_no) {
	setMember("sap_no",newSap_no);
}
public String getFlag_data() {
	return getMember("flag_data");
}
public void setFlag_data(String newFlag_data) {
	setMember("flag_data",newFlag_data);
}
public String getHn() {
	return getMember("hn");
}
public void setHn(String newHn) {
	setMember("hn",newHn);
}
public String getCitizen_id() {
	return getMember("citizen_id");
}
public void setCitizen_id(String newCitizen_id) {
	setMember("citizen_id",newCitizen_id);
}
public String getName() {
	return getMember("name");
}
public void setName(String newName) {
	setMember("name",newName);
}
public String getAge() {
	return getMember("age");
}
public void setAge(String newAge) {
	setMember("age",newAge);
}
//#methods defined everything will flow
//#(30000) programmer code begin;

public void setScreen(String newScreen) {
	setMember("screen",newScreen);
}
public String getScreen() {
	return getMember("screen");
}
public void setTab(String newTab) {
	setMember("tab",newTab);
}
public String getTab() {
	return getMember("tab");
}
public void setPart_screen(String newPartscreen) {
	setMember("part_screen",newPartscreen);
}
public String getPart_screen() {
	return getMember("part_screen");
}
public void setRef_id(String newRefid) {
	setMember("ref_id",newRefid);
}
public String getRef_id() {
	return getMember("ref_id");
}
public String getAuthority_id() {
	return getMember("authority_id");
}
public void setAuthority_id(String newAuthority_id) {
	setMember("authority_id",newAuthority_id);
}
public String getData() {
	return getMember("data");
}
public void setData(String newData) {
	setMember("data",newData);
}
//#(30000) programmer code end;
}
