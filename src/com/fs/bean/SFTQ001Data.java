package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTQ001Data.java
 * Description: SFTQ001Data class implements for handle tul data base schema.
 * Version: $Revision$
 * Creation date: Tue Jun 27 09:38:13 ICT 2017
 */
/**
 * SFTQ001Data class implements for handle tul data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTQ001Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
private BeanUtility butil = new BeanUtility();
//#(20000) programmer code end;
public SFTQ001Data() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tul");
	addSchema("seqno",java.sql.Types.BIGINT);
	addSchema("curtime",java.sql.Types.TIMESTAMP);
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("progid",java.sql.Types.VARCHAR);
	addSchema("action",java.sql.Types.VARCHAR);
	addSchema("remark",java.sql.Types.VARCHAR);
	map("userid","userid");
	map("progid","progid");
	addSchema("datefrom",java.sql.Types.DATE,true);
	addSchema("dateto",java.sql.Types.DATE,true);
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	map("datefrom","datefrom");
	map("dateto","dateto");
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTQ001Data.class+"=$Revision$\n";
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getProgid() {
	return getString("progid");
}
public void setProgid(String newProgid) {
	setMember("progid",newProgid);
}
public Date getDatefrom() {
	return getDate("datefrom");
}
public void setDatefrom(Date newDatefrom) {
	setMember("datefrom",newDatefrom);
}
public Date getDateto() {
	return getDate("dateto");
}
public void setDateto(Date newDateto) {
	setMember("dateto",newDateto);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setProgid(bean.getFieldByName(mapper("progid")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	setDatefrom(bean.getFieldByName(mapper("datefrom")).asDate());
	setDateto(bean.getFieldByName(mapper("dateto")).asDate());
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setProgid(rs.getString("progid"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("progid",getProgid());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	java.util.Map<String,String> prgmap = new java.util.HashMap<>();
	KnSQL knsql = new KnSQL(this);
	knsql.append("select programid,progname from tprog");
	try(java.sql.ResultSet prs = knsql.executeQuery(connection)) {
		while(prs.next()) {
			String progid = prs.getString("programid");
			String progname =  prs.getString("progname");
			if(progid!=null && progname!=null) {
				prgmap.put(progid,progname);
			}
		}
	}
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	String filter = " where";
	if((getUserid() != null) && (getUserid().trim().length() > 0)) {
		sql.append(filter+" userid LIKE ?userid ");
		sql.setParameter("userid","%"+getUserid()+"%");
		filter = " and";
	}
	if((getProgid() != null) && (getProgid().trim().length() > 0)) {
		sql.append(filter+" progid LIKE ?progid ");
		sql.setParameter("progid","%"+getProgid()+"%");
		filter = " and";
	}
	if(getDatefrom()!=null) {
		sql.append(filter+" curtime >= ?datefrom ");
		sql.setParameter("datefrom",new java.sql.Timestamp(getDatefrom().getTime()));
		filter = " and";
	}
	if(getDateto()!=null) {
		java.util.Date todate = BeanUtility.rollingDate(getDateto(),0,0,0,23,59,59);
		sql.append(filter+" curtime <= ?dateto ");
		sql.setParameter("dateto",new java.sql.Timestamp(todate.getTime()));
		filter = " and";
	}
	sql.append(" order by curtime desc,userid,progid ");
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		while(rs.next()) {
			result++;
			//#addon statement if you come back
			//#(85000) programmer code begin;
			//#(85000) programmer code end;
			com.fs.bean.SFTQ001AData aSFTQ001AData = new com.fs.bean.SFTQ001AData();
			aSFTQ001AData.fetchResult(rs);
			add(aSFTQ001AData);
			//#addon statement if you come back
			//#(90000) programmer code begin;
			String progname = prgmap.get(aSFTQ001AData.getProgid());
			if(progname!=null) aSFTQ001AData.setProgname(progname);
			//#(90000) programmer code end;
		}
		//#after scraping result set in too deep
		//#(240000) programmer code begin;
		//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
