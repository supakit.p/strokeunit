package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE001Data.java
 * Description: SFTE001Data class implements for handle tprog data base schema.
 * Version: $Revision$
 * Creation date: Mon Jun 26 09:06:08 ICT 2017
 */
/**
 * SFTE001Data class implements for handle tprog data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE001Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE001Data() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tprog");
	addSchema("product",java.sql.Types.VARCHAR);
	addSchema("programid",java.sql.Types.VARCHAR);
	addSchema("progname",java.sql.Types.VARCHAR);
	addSchema("progtype",java.sql.Types.VARCHAR);
	addSchema("description",java.sql.Types.VARCHAR);
	addSchema("parameters",java.sql.Types.VARCHAR);
	addSchema("progsystem",java.sql.Types.VARCHAR);
	addSchema("iconfile",java.sql.Types.VARCHAR);
	addSchema("iconstyle",java.sql.Types.VARCHAR);
	map("progname","progname");
	map("progtype","progtype");
	map("programid","programid");
	map("product","product");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE001Data.class+"=$Revision$\n";
}
public String getProduct() {
	return getString("product");
}
public void setProduct(String newProduct) {
	setMember("product",newProduct);
}
public String getProgramid() {
	return getString("programid");
}
public void setProgramid(String newProgramid) {
	setMember("programid",newProgramid);
}
public String getProgname() {
	return getString("progname");
}
public void setProgname(String newProgname) {
	setMember("progname",newProgname);
}
public String getProgtype() {
	return getString("progtype");
}
public void setProgtype(String newProgtype) {
	setMember("progtype",newProgtype);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setProduct(bean.getFieldByName(mapper("product")).asString());
	setProgramid(bean.getFieldByName(mapper("programid")).asString());
	setProgname(bean.getFieldByName(mapper("progname")).asString());
	setProgtype(bean.getFieldByName(mapper("progtype")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setProduct(rs.getString("product"));
	setProgramid(rs.getString("programid"));
	setProgname(rs.getString("progname"));
	setProgtype(rs.getString("progtype"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("product",getProduct());
	sql.setParameter("programid",getProgramid());
	sql.setParameter("progname",getProgname());
	sql.setParameter("progtype",getProgtype());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	String filter = " where";
	if(getProduct()!=null && getProduct().trim().length()>0) {
		sql.append(filter+" product = ?product ");
		sql.setParameter("product",getProduct());
		filter = " and";
	}
	if(getProgramid()!=null && getProgramid().trim().length()>0) {
		sql.append(filter+" programid LIKE ?programid ");
		sql.setParameter("programid","%"+getProgramid()+"%");
		filter = " and";
	}
	if(getProgname()!=null && getProgname().trim().length()>0) {
		sql.append(filter+" progname LIKE ?progname ");
		sql.setParameter("progname","%"+getProgname()+"%");
		filter = " and";
	}
	if(getProgtype()!=null && getProgtype().trim().length()>0) {
		sql.append(filter+" progtype = ?progtype ");
		sql.setParameter("progtype",getProgtype());
		filter = " and";
	}
	sql.append(" order by programid ");
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		com.fs.bean.SFTE001AData aSFTE001AData = new com.fs.bean.SFTE001AData();
		aSFTE001AData.fetchResult(rs);
		add(aSFTE001AData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
/*
//#(revision) programmer code begin;
$Revision$
//#(revision) programmer code end;
*/
}
