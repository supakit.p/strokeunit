package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005PBean.java
 * Description: SFTE005PBean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Wed Jan 03 13:44:38 ICT 2018
 */
/**
 * SFTE005PBean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class SFTE005PBean extends BeanSchema {

	public SFTE005PBean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("userid",java.sql.Types.VARCHAR,"userid");
		addSchema("username",java.sql.Types.VARCHAR,"username");
		addSchema("site",java.sql.Types.VARCHAR,"site");
		addSchema("userbranch",java.sql.Types.VARCHAR,"userbranch");
		addSchema("usertname",java.sql.Types.VARCHAR,"usertname");
		addSchema("usertsurname",java.sql.Types.VARCHAR,"usertsurname");
		addSchema("userename",java.sql.Types.VARCHAR,"userename");
		addSchema("useresurname",java.sql.Types.VARCHAR,"useresurname");
		addSchema("email",java.sql.Types.VARCHAR,"email");
		addSchema("gender",java.sql.Types.VARCHAR,"gender");
		addSchema("mobile",java.sql.Types.VARCHAR,"mobile");
		addSchema("lineno",java.sql.Types.VARCHAR,"lineno");
	}
	public String getUserid() {
		return getMember("userid");
	}
	public void setUserid(String newUserid) {
		setMember("userid",newUserid);
	}
	public String getUsername() {
		return getMember("username");
	}
	public void setUsername(String newUsername) {
		setMember("username",newUsername);
	}
	public String getSite() {
		return getMember("site");
	}
	public void setSite(String newSite) {
		setMember("site",newSite);
	}
	public String getUserbranch() {
		return getMember("userbranch");
	}
	public void setUserbranch(String newUserbranch) {
		setMember("userbranch",newUserbranch);
	}
	public String getUsertname() {
		return getMember("usertname");
	}
	public void setUsertname(String newUsertname) {
		setMember("usertname",newUsertname);
	}
	public String getUsertsurname() {
		return getMember("usertsurname");
	}
	public void setUsertsurname(String newUsertsurname) {
		setMember("usertsurname",newUsertsurname);
	}
	public String getUserename() {
		return getMember("userename");
	}
	public void setUserename(String newUserename) {
		setMember("userename",newUserename);
	}
	public String getUseresurname() {
		return getMember("useresurname");
	}
	public void setUseresurname(String newUseresurname) {
		setMember("useresurname",newUseresurname);
	}
	public String getEmail() {
		return getMember("email");
	}
	public void setEmail(String newEmail) {
		setMember("email",newEmail);
	}
	public String getGender() {
		return getMember("gender");
	}
	public void setGender(String newGender) {
		setMember("gender",newGender);
	}
	public String getMobile() {
		return getMember("mobile");
	}
	public void setMobile(String newMobile) {
		setMember("mobile",newMobile);
	}
	public String getLineno() {
		return getMember("lineno");
	}
	public void setLineno(String newLineno) {
		setMember("lineno",newLineno);
	}
}
