package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: TConfigBean.java
 * Description: TConfigBean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Tue Jan 22 11:37:10 ICT 2019
 */
/**
 * TConfigBean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class TConfigBean extends BeanSchema {

	public TConfigBean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("category",java.sql.Types.VARCHAR,"category");
		addSchema("colname",java.sql.Types.VARCHAR,"colname");
		addSchema("colvalue",java.sql.Types.VARCHAR,"colvalue");
	}
	public String getCategory() {
		return getMember("category");
	}
	public void setCategory(String newCategory) {
		setMember("category",newCategory);
	}
	public String getColname() {
		return getMember("colname");
	}
	public void setColname(String newColname) {
		setMember("colname",newColname);
	}
	public String getColvalue() {
		return getMember("colvalue");
	}
	public void setColvalue(String newColvalue) {
		setMember("colvalue",newColvalue);
	}
}
