package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Tue Jun 27 09:21:57 ICT 2017
 */

import com.fs.bean.misc.*;
import com.fs.bean.util.*;
import com.fs.bean.gener.*;

@SuppressWarnings({"serial","unused"})
public class TUserGroupBean extends BeanSchema {

//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public TUserGroupBean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("userid",java.sql.Types.VARCHAR,"User ID");
	addSchema("groupname",java.sql.Types.VARCHAR,"Group Name");
	addSchema("rolename",java.sql.Types.VARCHAR,"Role Name");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+TUserGroupBean.class+"=$Revision$\n";
}
public String getUserid() {
	return getMember("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getGroupname() {
	return getMember("groupname");
}
public void setGroupname(String newGroupname) {
	setMember("groupname",newGroupname);
}
public String getRolename() {
	return getMember("rolename");
}
public void setRolename(String newRolename) {
	setMember("rolename",newRolename);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
//#(30000) programmer code end;
}
