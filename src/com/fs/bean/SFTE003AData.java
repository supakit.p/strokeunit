package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE003AData.java
 * Description: SFTE003AData class implements for handle tprod data base schema.
 * Version: $Revision$
 * Creation date: Sun Sep 23 11:34:36 ICT 2018
 */
/**
 * SFTE003AData class implements for handle tprod data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE003AData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE003AData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tprod");
	addSchema("product",java.sql.Types.VARCHAR);
	addSchema("nameen",java.sql.Types.VARCHAR);
	addSchema("nameth",java.sql.Types.VARCHAR);
	addSchema("seqno",java.sql.Types.INTEGER);
	addSchema("serialid",java.sql.Types.VARCHAR);
	addSchema("startdate",java.sql.Types.DATE);
	addSchema("url",java.sql.Types.VARCHAR);
	addSchema("capital",java.sql.Types.VARCHAR);
	addSchema("verified",java.sql.Types.VARCHAR);
	addSchema("centerflag",java.sql.Types.VARCHAR);
	addSchema("iconfile",java.sql.Types.VARCHAR);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("edituser",java.sql.Types.VARCHAR);
	map("nameth","nameth");
	map("verified","verified");
	map("url","url");
	map("startdate","startdate");
	map("serialid","serialid");
	map("product","product");
	map("nameen","nameen");
	map("iconfile","iconfile");
	map("seqno","seqno");
	map("centerflag","centerflag");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE003AData.class+"=$Revision$\n";
}
public String getProduct() {
	return getString("product");
}
public void setProduct(String newProduct) {
	setMember("product",newProduct);
}
public String getNameen() {
	return getString("nameen");
}
public void setNameen(String newNameen) {
	setMember("nameen",newNameen);
}
public String getNameth() {
	return getString("nameth");
}
public void setNameth(String newNameth) {
	setMember("nameth",newNameth);
}
public String getSerialid() {
	return getString("serialid");
}
public void setSerialid(String newSerialid) {
	setMember("serialid",newSerialid);
}
public Date getStartdate() {
	return getDate("startdate");
}
public void setStartdate(Date newStartdate) {
	setMember("startdate",newStartdate);
}
public String getUrl() {
	return getString("url");
}
public void setUrl(String newUrl) {
	setMember("url",newUrl);
}
public String getCapital() {
	return getString("capital");
}
public void setCapital(String newCapital) {
	setMember("capital",newCapital);
}
public String getVerified() {
	return getString("verified");
}
public void setVerified(String newVerified) {
	setMember("verified",newVerified);
}
public String getIconfile() {
	return getString("iconfile");
}
public void setIconfile(String newIconfile) {
	setMember("iconfile",newIconfile);
}
public Date getEditdate() {
	return getDate("editdate");
}
public void setEditdate(Date newEditdate) {
	setMember("editdate",newEditdate);
}
public Time getEdittime() {
	return getTime("edittime");
}
public void setEdittime(Time newEdittime) {
	setMember("edittime",newEdittime);
}
public String getEdituser() {
	return getString("edituser");
}
public void setEdituser(String newEdituser) {
	setMember("edituser",newEdituser);
}
public int getSeqno() {
	return getInt("seqno");
}
public void setSeqno(int newSeqno) {
	setMember("seqno",newSeqno);
}
public String getCenterflag() {
	return getString("centerflag");
}
public void setCenterflag(String newCenterflag) {
	setMember("centerflag",newCenterflag);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setProduct(bean.getFieldByName(mapper("product")).asString());
	setNameen(bean.getFieldByName(mapper("nameen")).asString());
	setNameth(bean.getFieldByName(mapper("nameth")).asString());
	setSeqno(bean.getFieldByName(mapper("seqno")).asInt());
	setSerialid(bean.getFieldByName(mapper("serialid")).asString());
	setStartdate(bean.getFieldByName(mapper("startdate")).asDate());
	setUrl(bean.getFieldByName(mapper("url")).asString());
	setCapital(bean.getFieldByName(mapper("capital")).asString());
	setVerified(bean.getFieldByName(mapper("verified")).asString());
	setCenterflag(bean.getFieldByName(mapper("centerflag")).asString());
	setIconfile(bean.getFieldByName(mapper("iconfile")).asString());
	setEditdate(bean.getFieldByName(mapper("editdate")).asDate());
	setEdittime(bean.getFieldByName(mapper("edittime")).asTime());
	setEdituser(bean.getFieldByName(mapper("edituser")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	setProduct(getProduct().toUpperCase());
	setEdituser(global==null?getCurrentUser():global.getFsUser());
	if(getVerified()==null || getVerified().trim().length()<=0) {
		setVerified("0");
	}	
	if(getCenterflag()==null || getCenterflag().trim().length()<=0) {
		setCenterflag("0");
	}	
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setProduct(rs.getString("product"));
	setNameen(rs.getString("nameen"));
	setNameth(rs.getString("nameth"));
	setSeqno(rs.getInt("seqno"));
	setSerialid(rs.getString("serialid"));
	setStartdate(rs.getDate("startdate"));
	setUrl(rs.getString("url"));
	setCapital(rs.getString("capital"));
	setVerified(rs.getString("verified"));
	setCenterflag(rs.getString("centerflag"));
	setIconfile(rs.getString("iconfile"));
	setEditdate(rs.getDate("editdate"));
	setEdittime(rs.getTime("edittime"));
	setEdituser(rs.getString("edituser"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	if(getEditdate()==null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if(getEdittime()==null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	//#(75000) programmer code end;
	sql.setParameter("product",getProduct());
	sql.setParameter("nameen",getNameen());
	sql.setParameter("nameth",getNameth());
	sql.setParameter("seqno",getSeqno());
	sql.setParameter("serialid",getSerialid());
	sql.setParameter("startdate",getStartdate());
	sql.setParameter("url",getUrl());
	sql.setParameter("capital",getCapital());
	sql.setParameter("verified",getVerified());
	sql.setParameter("centerflag",getCenterflag());
	sql.setParameter("iconfile",getIconfile());
	sql.setParameter("editdate",getEditdate());
	sql.setParameter("edittime",getEdittime());
	sql.setParameter("edituser",getEdituser());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	if(getEditdate()==null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if(getEdittime()==null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("product");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("product",getProduct());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("product");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	if(getEditdate()==null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if(getEdittime()==null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	setNullableFields("url","serialid","startdate");
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("product");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getProduct()==null) || getProduct().trim().equals("")) throw new java.sql.SQLException("Product is unspecified","product",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE003AData aSFTE003AData = new SFTE003AData();
		aSFTE003AData.fetchResult(rs);
		add(aSFTE003AData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
