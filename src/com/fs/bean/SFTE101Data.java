package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE101Data.java
 * Description: SFTE101Data class implements for handle M_Authority data base schema.
 * Version: $Revision$
 * Creation date: Wed Sep 13 15:53:13 ICT 2017
 */
/**
 * SFTE101Data class implements for handle M_Authority data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
import org.json.*;
import com.fs.dev.strok.service.*;
//#(10000) programmer code end;

public class SFTE101Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;

    private SirirajUtility sutil = new SirirajUtility();
    private BeanUtility butil = new BeanUtility();
    private String text = "Success";
    private String status = "Y";
    private boolean mobileFlag = true;
//#(20000) programmer code end;

    public SFTE101Data() {
        super();
    }

    protected void initialize() {
        super.initialize();
        setTable("M_Authority");
        addSchema("AuthorityId", java.sql.Types.INTEGER);
        addSchema("AuthorityTypeId", java.sql.Types.INTEGER);
        addSchema("AttendingDoctorFlag", java.sql.Types.CHAR);
        addSchema("PrefixName", java.sql.Types.VARCHAR);
        addSchema("FirstName", java.sql.Types.VARCHAR);
        addSchema("LastName", java.sql.Types.VARCHAR);
        addSchema("MDNumber", java.sql.Types.VARCHAR);
        addSchema("Barcode", java.sql.Types.VARCHAR);
        addSchema("SAPNumber", java.sql.Types.VARCHAR);
        addSchema("Username", java.sql.Types.VARCHAR);
        addSchema("IsActive", java.sql.Types.CHAR);
        addSchema("UpdateById", java.sql.Types.INTEGER);
        addSchema("UpdateDate", java.sql.Types.TIMESTAMP);
        //#intialize how deep is your love
        //#(30000) programmer code begin;
        addSchema("data", java.sql.Types.VARCHAR);
        map("data", "data");
        //#(30000) programmer code end;
    }

    public String fetchVersion() {
        return super.fetchVersion() + SFTE101Data.class + "=$Revision$\n";
    }

    public int getAuthorityid() {
        return getInt("AuthorityId");
    }

    public void setAuthorityid(int newAuthorityid) {
        setMember("AuthorityId", newAuthorityid);
    }

    public int getAuthoritytypeid() {
        return getInt("AuthorityTypeId");
    }

    public void setAuthoritytypeid(int newAuthoritytypeid) {
        setMember("AuthorityTypeId", newAuthoritytypeid);
    }

    public String getAttendingdoctorflag() {
        return getString("AttendingDoctorFlag");
    }

    public void setAttendingdoctorflag(String newAttendingdoctorflag) {
        setMember("AttendingDoctorFlag", newAttendingdoctorflag);
    }

    public String getPrefixname() {
        return getString("PrefixName");
    }

    public void setPrefixname(String newPrefixname) {
        setMember("PrefixName", newPrefixname);
    }

    public String getFirstname() {
        return getString("FirstName");
    }

    public void setFirstname(String newFirstname) {
        setMember("FirstName", newFirstname);
    }

    public String getLastname() {
        return getString("LastName");
    }

    public void setLastname(String newLastname) {
        setMember("LastName", newLastname);
    }

    public String getMdnumber() {
        return getString("MDNumber");
    }

    public void setMdnumber(String newMdnumber) {
        setMember("MDNumber", newMdnumber);
    }

    public String getBarcode() {
        return getString("Barcode");
    }

    public void setBarcode(String newBarcode) {
        setMember("Barcode", newBarcode);
    }

    public String getSapnumber() {
        return getString("SAPNumber");
    }

    public void setSapnumber(String newSapnumber) {
        setMember("SAPNumber", newSapnumber);
    }

    public String getUsername() {
        return getString("Username");
    }

    public void setUsername(String newUsername) {
        setMember("Username", newUsername);
    }

    public String getIsactive() {
        return getString("IsActive");
    }

    public void setIsactive(String newIsactive) {
        setMember("IsActive", newIsactive);
    }

    public int getUpdatebyid() {
        return getInt("UpdateById");
    }

    public void setUpdatebyid(int newUpdatebyid) {
        setMember("UpdateById", newUpdatebyid);
    }

    public Timestamp getUpdatedate() {
        return getTimestamp("UpdateDate");
    }

    public void setUpdatedate(Timestamp newUpdatedate) {
        setMember("UpdateDate", newUpdatedate);
    }

    public String getData() {
        return getString("data");
    }

    public void setData(String newData) {
        setMember("data", newData);
    }

    public boolean obtain(BeanSchemaInterface bean) throws Exception {
        if (bean == null) {
            return super.obtain(bean);
        }
        setAuthorityid(bean.getFieldByName(mapper("AuthorityId")).asInt());
        setAuthoritytypeid(bean.getFieldByName(mapper("AuthorityTypeId")).asInt());
        setAttendingdoctorflag(bean.getFieldByName(mapper("AttendingDoctorFlag")).asString());
        setPrefixname(bean.getFieldByName(mapper("PrefixName")).asString());
        setFirstname(bean.getFieldByName(mapper("FirstName")).asString());
        setLastname(bean.getFieldByName(mapper("LastName")).asString());
        setMdnumber(bean.getFieldByName(mapper("MDNumber")).asString());
        setBarcode(bean.getFieldByName(mapper("Barcode")).asString());
        setSapnumber(bean.getFieldByName(mapper("SAPNumber")).asString());
        setUsername(bean.getFieldByName(mapper("Username")).asString());
        setIsactive(bean.getFieldByName(mapper("IsActive")).asString());
        setUpdatebyid(bean.getFieldByName(mapper("UpdateById")).asInt());
        setUpdatedate(bean.getFieldByName(mapper("UpdateDate")).asTimestamp());
        //#obtain it perfect moment
        //#(40000) programmer code begin;
        setData(bean.getFieldByName(mapper("data")).asString());
        //#(40000) programmer code end;
        return super.obtain(bean);
    }

    public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
        super.fetchResult(rs);
        setAuthorityid(rs.getInt("AuthorityId"));
        setAuthoritytypeid(rs.getInt("AuthorityTypeId"));
        setAttendingdoctorflag(rs.getString("AttendingDoctorFlag"));
        setPrefixname(rs.getString("PrefixName"));
        setFirstname(rs.getString("FirstName"));
        setLastname(rs.getString("LastName"));
        setMdnumber(rs.getString("MDNumber"));
        setBarcode(rs.getString("Barcode"));
        setSapnumber(rs.getString("SAPNumber"));
        setUsername(rs.getString("Username"));
        setIsactive(rs.getString("IsActive"));
        setUpdatebyid(rs.getInt("UpdateById"));
        setUpdatedate(rs.getTimestamp("UpdateDate"));
        //#fetching other result desire
        //#(60000) programmer code begin;
        //#(60000) programmer code end;
    }

    protected void assignParameters(ExecuteStatement sql) throws Exception {
        if (sql == null) {
            return;
        }
        //#Everything I do, I do it for you
        //#(75000) programmer code begin;
        //#(75000) programmer code end;
        sql.setParameter("AuthorityId", getAuthorityid());
        sql.setParameter("AuthorityTypeId", getAuthoritytypeid());
        sql.setParameter("AttendingDoctorFlag", getAttendingdoctorflag());
        sql.setParameter("PrefixName", getPrefixname());
        sql.setParameter("FirstName", getFirstname());
        sql.setParameter("LastName", getLastname());
        sql.setParameter("MDNumber", getMdnumber());
        sql.setParameter("Barcode", getBarcode());
        sql.setParameter("SAPNumber", getSapnumber());
        sql.setParameter("Username", getUsername());
        sql.setParameter("IsActive", getIsactive());
        sql.setParameter("UpdateById", getUpdatebyid());
        sql.setParameter("UpdateDate", getUpdatedate());
        //#I'm gonna be around you
        //#(77000) programmer code begin;
        //#(77000) programmer code end;
    }

    public int insert(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        //#begin with insert statement going on
        //#(250000) programmer code begin;
        //#(250000) programmer code end;
        ExecuteStatement sql = createQueryForInsert(connection);
        //#another modification keep on loving you
        //#(110000) programmer code begin;
        //#(110000) programmer code end;
        assignParameters(sql);
        //#assigned parameter always on my mind
        //#(120000) programmer code begin;
        //#(120000) programmer code end;
        return sql.executeUpdate(connection);
        //#ending with insert statement here
        //#(260000) programmer code begin;
        //#(260000) programmer code end;
    }

    public int collect(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        removeAlls();
        
        PermissionBean fsPermission = global == null ? null : global.getPermission();
        //#here we go to the collection
        //#(65000) programmer code begin;
        /*
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	java.sql.ResultSet rs = sql.executeQuery(connection);
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE101Data aSFTE101Data = new SFTE101Data();
		aSFTE101Data.fetchResult(rs);
		add(aSFTE101Data);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
         */
        // *------------- Get DATA from JSONString -------------* //
        //System.out.println("==================== collect ===================="+getData());
        int errorCode = 0;
        java.util.Map map = new java.util.Hashtable();
        String error_desc = "";
        JSONObject JObject = null;
        String sapnumber = "";
        String name = "";
        String authorityType = "";
        String submitDate = null;
        String strokeID = "";
        String getSapno = " : ";
        String authorityid = "";
        java.sql.Timestamp sysdate = butil.getCurrentTimestamp();
        String desc = "";
        String statusCode = null;
        String statusDescription = null;
        String strokeMobileId = null;
        String EmpName = null;
        String MDNumber = null;
        String AuthorityType = null;
        boolean sapFlag = true;
        boolean empFlag = true;
        System.out.println("----------------- collect ---------------------");
        
        try {
            if (!sutil.isJSONValid(getData())) { //validate JSON format
                System.out.println("----------------- valid json ---------------------");
                throw new Exception("SERR:12");
            }
            try {
                JObject = new JSONObject(getData());
                
            } catch (Exception ex) {
                throw new Exception("SERR:12");
            }
            try {
                sapnumber = JObject.getString("sapNumber");
            } catch (Exception ex) {
                sapFlag = false;
            }
            try {
                name = JObject.getString("empName");
                EmpName = name;
            } catch (Exception ex) {
                empFlag = false;
            }
            try {
                submitDate = JObject.getString("submitDate");
            } catch (Exception ex) {

            }
            try {
                strokeMobileId = JObject.getString("strokeMobileId");
                if (strokeMobileId.isEmpty()) {
                    mobileFlag = false;
                }
            } catch (Exception ex) {
                mobileFlag = false;
            }
            try {
                strokeID = JObject.getString("strokeId");
            } catch (Exception ex) {
            }

            if (!sapFlag && !empFlag) {
                throw new Exception("SERR:23");
            }

            // *------------- Get DATA from JSONString -------------* //
            java.util.Map mapBody = new java.util.Hashtable();
            //map.put("Name", name);
            //map.put("SAPNumber", sapnumber);
            java.util.Vector vec = new java.util.Vector();
            TheEmployee pt = new TheEmployee();
            if (sapnumber != null && !sapnumber.equalsIgnoreCase("")) {
                System.out.println("========sapnumber============" + sapnumber);
                //System.out.println("call api Siriraj by sapno =========== "+sapnumber);

                Employee emp = pt.getEmployeeBySAP(sapnumber);
                //System.out.println("emp==============="+emp);
                if (emp == null) { //can not connect server
System.out.println("========can not connect server============");
                    map.put("Status", -1);
                    errorCode = 6;
                } else {
                    System.out.println("=======connect siriraj ======"+emp.isSuccess());
                    if (emp.isSuccess()) { // status = 0
                        System.out.println("========status = 0============");
                        EmpName = emp.getEmpName();
                        MDNumber = emp.getMdNumber();
                        AuthorityType = emp.getAuthorityType();
                        //System.out.println("emp ===================" + emp);
                        //System.out.println("=====insertT_Authority======"+sapnumber);
                        authorityid = insertT_Authority(connection, emp.getAuthorityType(), butil.MS874ToUnicode(EmpName), butil.MS874ToUnicode(MDNumber), sapnumber, submitDate);
                        //	6/18/2018
                        //map.put("Status", 0);
                        mapBody.put("EmpName", EmpName);
                        mapBody.put("MDNumber", MDNumber);
                        mapBody.put("SAPNumber", sapnumber);
                        mapBody.put("AuthorityType", AuthorityType);
                        mapBody.put("AuthorityId", authorityid);
                        // insert apiEmployeesLog
                        getSapno += sapnumber;
                        statusCode = Long.toString(emp.getStatus());
                        statusDescription = emp.getStatusDescription();
                        //		}
                        vec.add(mapBody);
                        map.put("body", vec);
                        System.out.println("========vec============" + vec);
                    } else {
                        // insert apiEmployeesLog
                        errorCode = 16;
                    }
                }

            } else {
                // Lookup Process
                System.out.println("========employee============");
                Employee emp[] = pt.getEmployeeByName(name);
                for (int i = 0; i < emp.length; i++) {
                    System.out.println("========employee============" + emp[i].getStatus());
                    System.out.println("========employee============" + emp[i].isSuccess());
                    java.util.Map mapEmp = new java.util.Hashtable();
                    if (emp[i] != null) {
                        if (!emp[i].isSuccess()) {
                            //map.put("Status", -1);
                            errorCode = 16;
                            //insert apiEmployeesLog
                        } else {
                            authorityid = insertT_Authority(connection, emp[i].getAuthorityType(), butil.MS874ToUnicode(emp[i].getEmpName()), butil.MS874ToUnicode(emp[i].getMdNumber()), emp[i].getSapNumber(), submitDate);
                            mapEmp.put("EmpName", emp[i].getEmpName());
                            mapEmp.put("MDNumber", emp[i].getMdNumber());
                            mapEmp.put("SAPNumber", emp[i].getSapNumber());
                            mapEmp.put("AuthorityType", emp[i].getAuthorityType());
                            mapEmp.put("AuthorityId", authorityid);
                            //insert apiEmployeesLog
                            vec.add(mapEmp);
                            if (i > 0) {
                                getSapno += ", ";
                            }
                            getSapno += emp[i].getSapNumber();

                            statusCode = Long.toString(emp[i].getStatus());

                        }
                    } else {
                        errorCode = 6;
                    }

                }
                if (vec.size() > 0) {
                    map.put("body", vec);
                }
            }

            error_desc = sutil.getErrorDesc(connection, errorCode);

            //insertApiEmployeesLog(connection, strokeID, statusCode, statusDescription, EmpName, sapnumber, AuthorityType, error_desc, submitDate, MDNumber );
        } catch (Exception ex) {
            connection.rollback();
            System.out.println("error=========="+ex.getMessage());
            if (ex.getMessage() != null && ex.getMessage().indexOf("SERR") >= 0) {
                String[] err = ex.getMessage().split(":");
                try {
                    errorCode = Integer.parseInt(err[1]);
                } catch (Exception e) {
                    errorCode = 999;
                }
            }
        }
        map.put("server_time", sysdate);
        map.put("error_code", errorCode);
        map.put("error_desc", sutil.chkNotNull(sutil.getErrorDesc(connection, errorCode)));
        map.put("desc", desc);

        try {
            String JSON_str = JSONUtility.parseJSON(map, null, true);
            insertApiEmployeesLog(connection, strokeID, statusCode, statusDescription, butil.MS874ToUnicode(EmpName), sapnumber, AuthorityType, error_desc, submitDate, butil.MS874ToUnicode(MDNumber), JSON_str);
            global.setFsMessage(JSON_str);
            System.out.println("----------------- setFsMessage --------------------- : " + JSON_str);
        } catch (Exception ex) {
            global.setFsMessage("");
            System.out.println("----------------- ex --------------------- : " + ex);
        }

        //#(240000) programmer code end;
        return 0;
    }
//#another methods defined drive you crazy
//#(100000) programmer code begin;

    public String insertT_Authority(java.sql.Connection connection, String AuthorityType, String EmpName, String MDNumber, String sapnumber, String submitDate) throws Exception {
        KnSQL sql = new KnSQL();
        String AuthorityId = checkT_Authority(connection, sapnumber);
        if (AuthorityId == null) {
            boolean getAuthoFlag = false;
            int AuthorityTypeId = 0;
            String Category = null;
            java.sql.ResultSet rs = getAuthorityTypeId(connection, AuthorityType);
            sql.clear();
            sql.append("insert into T_Authority (FirstName,MDNo,SAPNo,submitDate");
            if (rs != null && rs.next()) {
                sql.append(",AuthorityTypeId,Category");
                getAuthoFlag = true;
                AuthorityTypeId = rs.getInt("AuthorityTypeId");
                Category = rs.getString("SRJ_AuthorityType_EN");
            }
            sql.append(") \n");
            sql.append("values(?FirstName,?MDNumber,?SAPNumber,?submitDate");
            if (getAuthoFlag) {
                sql.append(",?AuthorityTypeId,?Category");
                sql.setParameter("AuthorityTypeId", AuthorityTypeId);
                sql.setParameter("Category", Category);
            }
            sql.append(") ");
            sql.setParameter("FirstName", EmpName);
            sql.setParameter("MDNumber", MDNumber);
            sql.setParameter("SAPNumber", sapnumber);
            sql.setParameter("submitDate", submitDate);
            sql.executeUpdate(connection);

            sql.clear();
            sql.append("select max(AuthorityId) authorityId \n");
            sql.append("from T_Authority \n");
            rs = sql.executeQuery(connection);
            if (rs != null && rs.next()) {
                AuthorityId = rs.getString("authorityId");
            }

            String authorityTypeId = Integer.toString(AuthorityTypeId);
            insertT_AuthorityLog(connection, "insert", AuthorityId, authorityTypeId, Category, null, EmpName, null, MDNumber, null, sapnumber, null, null, null, submitDate, null, null);

        } else {
            java.sql.ResultSet rs = getAuthorityTypeId(connection, AuthorityType);
            sql.clear();
            sql.append("update T_Authority \n");
            sql.append(" set FirstName = ?FirstName ,MDNo = ?MDNumber,SAPNo = ?SAPNumber, \n");
            sql.append(" submitDate = ?submitDate ");
            if (rs != null && rs.next()) {
                sql.append(",AuthorityTypeId = ?authorityTypeId , Category = ?category ");
                sql.setParameter("authorityTypeId", rs.getInt("AuthorityTypeId"));
                sql.setParameter("category", rs.getString("SRJ_AuthorityType_EN"));
            }
            sql.append(" where AuthorityId = ?AuthorityId ");
            sql.setParameter("FirstName", EmpName);
            sql.setParameter("MDNumber", MDNumber);
            sql.setParameter("SAPNumber", sapnumber);
            sql.setParameter("submitDate", submitDate);
            sql.setParameter("AuthorityId", AuthorityId);
            sql.executeUpdate(connection);

            String authorityTypeId = Integer.toString(rs.getInt("AuthorityTypeId"));
            insertT_AuthorityLog(connection, "update", AuthorityId, authorityTypeId, rs.getString("SRJ_AuthorityType_EN"), null, EmpName, null, MDNumber, null, sapnumber, null, null, null, submitDate, null, null);

        }

        return AuthorityId;
    }

    public String checkT_Authority(java.sql.Connection connection, String sapNumber) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("select AuthorityId from T_Authority \n");
        sql.append(" where SAPNo = ?sapno \n");
        sql.setParameter("sapno", sapNumber);
        String authorityId = null;
        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) {
            authorityId = rs.getString("AuthorityId");
        }
        return authorityId;
    }

    public void insertM_Authority(java.sql.Connection connection, String AuthorityTypeId, String EmpName, String MDNumber, String sapnumber) throws Exception {
        KnSQL sql = new KnSQL();

        try {
            sql.append("insert into M_Authority (AuthorityTypeId,FirstName,MDNumber,SAPNumber,IsActive,UpdateById,UpdateDate) \n");
            sql.append("values(?AuthorityTypeId,?FirstName,?MDNumber,?SAPNumber,'1',?CreateById,?CreateDate) ");
            sql.setParameter("AuthorityTypeId", AuthorityTypeId);
            sql.setParameter("FirstName", EmpName);
            sql.setParameter("MDNumber", MDNumber);
            sql.setParameter("SAPNumber", sapnumber);
            sql.setParameter("IsActive", 1);
            sql.setParameter("CreateById", -99);
            sql.setParameter("CreateDate", butil.getCurrentTimestamp());
            sql.executeUpdate(connection);

            sql.clear();
            sql.append("insert into t_Authority (AuthorityTypeId,FirstName,MDNumber,SAPNumber,IsActive,UpdateById,UpdateDate) \n");
            sql.append("values(?AuthorityTypeId,?FirstName,?MDNumber,?SAPNumber,'1',?CreateById,?CreateDate) ");
            sql.setParameter("AuthorityTypeId", AuthorityTypeId);
            sql.setParameter("FirstName", EmpName);
            sql.setParameter("MDNumber", MDNumber);
            sql.setParameter("SAPNumber", sapnumber);
            sql.setParameter("IsActive", 1);
            sql.setParameter("CreateById", -99);
            sql.setParameter("CreateDate", butil.getCurrentTimestamp());
            sql.executeUpdate(connection);

        } catch (Exception ex) {

        }

    }

    public String checkM_AuthorityType(java.sql.Connection connection, String authorityType) throws Exception {
        //System.out.println("=================== check M_AuthorityType ====================" + authorityType);
        KnSQL sql = new KnSQL();
        sql.append("select AuthorityTypeId,SRJ_AuthorityCode from M_AuthorityType \n");
        sql.append(" where SRJ_AuthorityCode = ?authoritytype ");
        sql.setParameter("authoritytype", authorityType);
        java.sql.ResultSet rs = sql.executeQuery(connection);
        String id = "";

        if (!rs.next()) {
            /*	java.sql.Timestamp sysdate = butil.getCurrentTimestamp();
		//System.out.println("=================== insert M_AuthorityType ====================");
		sql.clear();
		sql.append("insert into M_AuthorityType(AuthorityTypeId, SRJ_AuthorityCode,CreateDate) \n ");
		sql.append("values((select max(AuthorityTypeId)+1 from M_AuthorityType), ?authoritytype,?createdate )");
		sql.setParameter("authoritytype" , authorityType);
		sql.setParameter("createdate", sysdate);
		sql.executeUpdate(connection);
		id = checkM_AuthorityType(connection,authorityType);
             */
            return null;
        } else {
            id = rs.getString("AuthorityTypeId");
        }
        return id;
    }

    public int checkM_AuthorityType2(java.sql.Connection connection, String authorityType, int AuthorityTypeId) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("select SRJ_AuthorityCode from M_AuthorityType \n");
        sql.append(" where SRJ_AuthorityCode = ?authoritytype ");
        sql.append("  and AuthorityTypeId = ?authoritytypeid ");
        sql.setParameter("authoritytype", authorityType);
        sql.setParameter("authoritytypeid", AuthorityTypeId);
        java.sql.ResultSet rs = sql.executeQuery(connection);

        if (rs.next()) {
            return 0;
        } else {
            return 17;
        }
    }

    public void insertApiEmployeesLog(java.sql.Connection connection, String strokeid, String statusCode, String statusDescription, String empname, String sapno,
            String authoritytype, String error_desc, String submitDate, String MDNumber, String JSON_str) throws Exception {

        //System.out.println(" -------------- insert api log -------------- ");
        KnSQL sql = new KnSQL();
        sql.append(" insert into api_EmployeesLog \n");
        sql.append(" (StrokeId,ActionFrom,StatusCode,StatusDescription,EmpName,SAPNumber,AuthoriTytype,Remark,SubmitDate,MDNumber) \n");
        sql.append(" values(?strokeId,?actionFrom,?statusCode,?statusDescription,?empName,?sapNumber,?authorityType,?remark,?submitDate,?mdNumber)");

        sql.setParameter("strokeId", strokeid);

        if (mobileFlag) {
            sql.setParameter("actionFrom", "mobile");
        } else {
            sql.setParameter("actionFrom", "web");
        }

        sql.setParameter("statusCode", statusCode);
        sql.setParameter("statusDescription", statusDescription);
        sql.setParameter("empName", empname);
        sql.setParameter("sapNumber", sapno);
        sql.setParameter("remark", JSON_str);
        sql.setParameter("submitDate", submitDate);
        sql.setParameter("authorityType", authoritytype);
        sql.setParameter("mdNumber", MDNumber);

        sql.executeUpdate(connection);

    }

    public java.sql.ResultSet getAuthorityTypeId(java.sql.Connection connection, String authorityType) throws Exception {

        KnSQL sql = new KnSQL();
        sql.append("select AuthorityTypeId , SRJ_AuthorityType_EN \n");
        sql.append("from M_AuthorityType \n");
        sql.append(" where SRJ_AuthorityCode = ?authorityType \n");
        sql.setParameter("authorityType", authorityType);
        java.sql.ResultSet rs = sql.executeQuery(connection);

        return rs;
    }

    //create by Maen 31/01/2019 
    public void insertT_AuthorityLog(java.sql.Connection connection, String action, String authorityId, String authorityTypeId,
            String category, String prefixName, String firstName, String lastName, String mdNo, String barcode,
            String sapNo, String username, String password, String isActive, String submitDate, String createDate, String updateDate) throws Exception {

        KnSQL sql = new KnSQL();

        sql.append(" insert into T_AuthorityLog ");
        sql.append(" (Action,ActionFrom,AuthorityId,AuthorityTypeId,Category,PrefixName,FirstName,LastName,MDNo,Barcode,SAPNo,Username,Password,SubmitDate) ");
        sql.append(" values(?action,?actionFrom,?authorityId,?authorityTypeId,?category,?prefixName,?firstName,?lastName,?mdNo,?barcode,?sapNo,?username,?password,?submitDate) ");
        sql.setParameter("action", action);
        if (mobileFlag) {
            sql.setParameter("actionFrom", "mobile");
        } else {
            sql.setParameter("actionFrom", "web");
        }
        sql.setParameter("authorityId", authorityId);
        sql.setParameter("authorityTypeId", authorityTypeId);
        sql.setParameter("category", category);
        sql.setParameter("prefixName", prefixName);
        sql.setParameter("firstName", firstName);
        sql.setParameter("lastName", lastName);
        sql.setParameter("mdNo", mdNo);
        sql.setParameter("barcode", barcode);
        sql.setParameter("sapNo", sapNo);
        sql.setParameter("username", username);
        sql.setParameter("password", password);
        sql.setParameter("submitDate", submitDate);

        sql.executeUpdate(connection);

    }

//#(100000) programmer code end;
}
