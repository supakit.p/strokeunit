package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005CBean.java
 * Description: SFTE005CBean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Wed Jan 03 14:36:29 ICT 2018
 */
/**
 * SFTE005CBean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.TheLibrary;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class SFTE005DBean extends BeanSchema {

	public SFTE005DBean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("userid",java.sql.Types.VARCHAR,"userid");
		addSchema("username",java.sql.Types.VARCHAR,"username");
		addSchema("userpassword",java.sql.Types.VARCHAR,"userpassword");
		addSchema("oldpassword",java.sql.Types.VARCHAR,"oldpassword");
		addSchema("confirmpassword",java.sql.Types.VARCHAR,"confirmpassword");
		addSchema("checkflag",java.sql.Types.VARCHAR,"checkflag");
		addSchema("pincode",java.sql.Types.VARCHAR,"pincode");
		addSchema("cardid",java.sql.Types.VARCHAR,"cardid");
		addSchema("birthday",java.sql.Types.DATE,"birthday");
	}
	public String getUserid() {
		return getMember("userid");
	}
	public void setUserid(String newUserid) {
		setMember("userid",newUserid);
	}
	public String getUsername() {
		return getMember("username");
	}
	public void setUsername(String newUsername) {
		setMember("username",newUsername);
	}
	public String getUserpassword() {
		return getMember("userpassword");
	}
	public void setUserpassword(String newUserpassword) {
		setMember("userpassword",newUserpassword);
	}
	public String getOldpassword() {
		return getMember("oldpassword");
	}
	public void setOldpassword(String newOldpassword) {
		setMember("oldpassword",newOldpassword);
	}
	public String getConfirmpassword() {
		return getMember("confirmpassword");
	}
	public void setConfirmpassword(String newConfirmpassword) {
		setMember("confirmpassword",newConfirmpassword);
	}
	public String getCheckflag() {
		return getMember("checkflag");
	}
	public void setCheckflag(String newCheckflag) {
		setMember("checkflag",newCheckflag);
	}	
	public String getPincode() {
		return getMember("pincode");
	}
	public void setPincode(String newPincode) {
		setMember("pincode",newPincode);
	}
	public String getCardid() {
		return getMember("cardid");
	}
	public void setCardid(String newCardid) {
		setMember("cardid",newCardid);
	}
	public String getBirthday() {
		return getMember("birthday");
	}
	public void setBirthday(String newBirthday) {
		setMember("birthday",newBirthday);
	}
	public BeanException validateDatas(String fs_language) {
		if(getUserid()==null || getUserid().trim().length()<=0) {
			return new BeanException("User is undefined",-8891,fs_language);
		}
		if(!getUserpassword().equals(getConfirmpassword())) {
			return new BeanException("Confirm Password Mismatch",-8897,fs_language);
		}
		if(getUserpassword().equals(getUserid())) {
			return new BeanException("Not allow password as same as user",-8899,fs_language);
		}
		if(getUserpassword().length()>8) {
			return new BeanException("Password length not over 8 characters",-8896,fs_language);
		}	
		return TheLibrary.validatePincode(getPincode(),getCardid(),getFieldByName("birthday").asDate(),fs_language);
	}	
}
