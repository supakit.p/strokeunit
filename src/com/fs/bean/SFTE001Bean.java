package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Mon Jun 26 08:56:34 ICT 2017
 */

import com.fs.bean.gener.*;

@SuppressWarnings("serial")
public class SFTE001Bean extends BeanSchema {

//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTE001Bean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("product",java.sql.Types.VARCHAR,"Product");
	addSchema("programid",java.sql.Types.VARCHAR,"Program ID");
	addSchema("progname",java.sql.Types.VARCHAR,"Program Name");
	addSchema("progtype",java.sql.Types.VARCHAR,"Program Type");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	collectClass(com.fs.bean.SFTE001ABean.class);
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE001Bean.class+"=$Revision$\n";
}
public String getProduct() {
	return getMember("product");
}
public void setProduct(String newProduct) {
	setMember("product",newProduct);
}
public String getProgramid() {
	return getMember("programid");
}
public void setProgramid(String newProgramid) {
	setMember("programid",newProgramid);
}
public String getProgname() {
	return getMember("progname");
}
public void setProgname(String newProgname) {
	setMember("progname",newProgname);
}
public String getProgtype() {
	return getMember("progtype");
}
public void setProgtype(String newProgtype) {
	setMember("progtype",newProgtype);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
//#(30000) programmer code end;
}
