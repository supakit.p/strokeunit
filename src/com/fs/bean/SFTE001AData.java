package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE001AData.java
 * Description: SFTE001AData class implements for handle tprog data base schema.
 * Version: $Revision$
 * Creation date: Mon Jun 26 09:03:33 ICT 2017
 */
/**
 * SFTE001AData class implements for handle tprog data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE001AData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE001AData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tprog");
	addSchema("product",java.sql.Types.VARCHAR);
	addSchema("programid",java.sql.Types.VARCHAR);
	addSchema("progname",java.sql.Types.VARCHAR);
	addSchema("prognameth",java.sql.Types.VARCHAR);
	addSchema("progtype",java.sql.Types.VARCHAR);
	addSchema("description",java.sql.Types.VARCHAR);
	addSchema("parameters",java.sql.Types.VARCHAR);
	addSchema("progsystem",java.sql.Types.VARCHAR);
	addSchema("iconfile",java.sql.Types.VARCHAR);
	addSchema("iconstyle",java.sql.Types.VARCHAR);
	addSchema("shortname",java.sql.Types.VARCHAR);
	addSchema("shortnameth",java.sql.Types.VARCHAR);
	addSchema("progpath",java.sql.Types.VARCHAR);
	map("progsystem","progsystem");
	map("programid","programid");
	map("progtype","progtype");
	map("iconfile","iconfile");
	map("iconstyle","iconstyle");
	map("product","product");
	map("description","description");
	map("parameters","parameters");
	map("progname","progname");
	map("shortname","shortname");
	map("prognameth","prognameth");
	map("shortnameth","shortnameth");
	map("progpath","progpath");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE001AData.class+"=$Revision$\n";
}
public String getProduct() {
	return getString("product");
}
public void setProduct(String newProduct) {
	setMember("product",newProduct);
}
public String getProgramid() {
	return getString("programid");
}
public void setProgramid(String newProgramid) {
	setMember("programid",newProgramid);
}
public String getProgname() {
	return getString("progname");
}
public void setProgname(String newProgname) {
	setMember("progname",newProgname);
}
public String getPrognameth() {
	return getString("prognameth");
}
public void setPrognameth(String newPrognameth) {
	setMember("prognameth",newPrognameth);
}
public String getProgtype() {
	return getString("progtype");
}
public void setProgtype(String newProgtype) {
	setMember("progtype",newProgtype);
}
public String getDescription() {
	return getString("description");
}
public void setDescription(String newDescription) {
	setMember("description",newDescription);
}
public String getParameters() {
	return getString("parameters");
}
public void setParameters(String newParameters) {
	setMember("parameters",newParameters);
}
public String getProgsystem() {
	return getString("progsystem");
}
public void setProgsystem(String newProgsystem) {
	setMember("progsystem",newProgsystem);
}
public String getIconfile() {
	return getString("iconfile");
}
public void setIconfile(String newIconfile) {
	setMember("iconfile",newIconfile);
}
public String getIconstyle() {
	return getString("iconstyle");
}
public void setIconstyle(String newIconstyle) {
	setMember("iconstyle",newIconstyle);
}
public String getShortname() {
	return getString("shortname");
}
public void setShortname(String newShortname) {
	setMember("shortname",newShortname);
}
public String getShortnameth() {
	return getString("shortnameth");
}
public void setShortnameth(String newShortnameth) {
	setMember("shortnameth",newShortnameth);
}
public String getProgpath() {
	return getString("progpath");
}
public void setProgpath(String newProgpath) {
	setMember("progpath",newProgpath);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setProduct(bean.getFieldByName(mapper("product")).asString());
	setProgramid(bean.getFieldByName(mapper("programid")).asString());
	setProgname(bean.getFieldByName(mapper("progname")).asString());
	setPrognameth(bean.getFieldByName(mapper("prognameth")).asString());
	setProgtype(bean.getFieldByName(mapper("progtype")).asString());
	setDescription(bean.getFieldByName(mapper("description")).asString());
	setParameters(bean.getFieldByName(mapper("parameters")).asString());
	setProgsystem(bean.getFieldByName(mapper("progsystem")).asString());
	setIconfile(bean.getFieldByName(mapper("iconfile")).asString());
	setIconstyle(bean.getFieldByName(mapper("iconstyle")).asString());
	setShortname(bean.getFieldByName(mapper("shortname")).asString());
	setShortnameth(bean.getFieldByName(mapper("shortnameth")).asString());
	setProgpath(bean.getFieldByName(mapper("progpath")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	setProgramid(BeanUtility.trimAll(getProgramid()));
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setProduct(rs.getString("product"));
	setProgramid(rs.getString("programid"));
	setProgname(rs.getString("progname"));
	setPrognameth(rs.getString("prognameth"));
	setProgtype(rs.getString("progtype"));
	setDescription(rs.getString("description"));
	setParameters(rs.getString("parameters"));
	setProgsystem(rs.getString("progsystem"));
	setIconfile(rs.getString("iconfile"));
	setIconstyle(rs.getString("iconstyle"));
	setShortname(rs.getString("shortname"));
	setShortnameth(rs.getString("shortnameth"));
	setProgpath(rs.getString("progpath"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("product",getProduct());
	sql.setParameter("programid",getProgramid());
	sql.setParameter("progname",getProgname());
	sql.setParameter("prognameth",getPrognameth());
	sql.setParameter("progtype",getProgtype());
	sql.setParameter("description",getDescription());
	sql.setParameter("parameters",getParameters());
	sql.setParameter("progsystem",getProgsystem());
	sql.setParameter("iconfile",getIconfile());
	sql.setParameter("iconstyle",getIconstyle());
	sql.setParameter("shortname",getShortname());
	sql.setParameter("shortnameth",getShortnameth());
	sql.setParameter("progpath",getProgpath());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("programid");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("programid",getProgramid());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("programid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("programid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getProduct()==null) || getProduct().trim().equals("")) throw new java.sql.SQLException("Product is unspecified","product",-2008);
	if((getProgramid()==null) || getProgramid().trim().equals("")) throw new java.sql.SQLException("Programid is unspecified","programid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE001AData aSFTE001AData = new SFTE001AData();
		aSFTE001AData.fetchResult(rs);
		add(aSFTE001AData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
