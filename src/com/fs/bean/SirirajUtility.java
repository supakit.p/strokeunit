package com.fs.bean;

import com.fs.bean.misc.*;
import com.fs.bean.util.*;
import org.json.*;
import java.util.*;
import org.apache.commons.codec.binary.Base64;
import java.net.*;
import java.io.*;
import com.fs.dev.strok.service.*;

public class SirirajUtility {

    private static BeanUtility butil = new BeanUtility();

    public static void main(String[] args) {
        System.out.println("Hello World!");
        try {
            //   java.sql.Connection dbconn = getSelfConnection("STROKEMOBILE");
            //   dbconn.setAutoCommit(true);
            //      System.out.println("  [ Database [STROKEMOBILE] OK ] ");
            SirirajUtility sutil = new SirirajUtility();
            // sutil. insertT_TransactionLog(dbconn,"m_device","deviceid","3","1",null);
            //sutil. insertT_TransactionLog(dbconn,tablename,primarykey,keyvalue,strokeid,authorityid);
            // sutil. insertT_TransactionLog(dbconn,"t_stroke","strokeid","5","5","1");
            /*java.util.Vector<java.util.Vector> vec = sutil.getOldData(dbconn,"t_stroke","strokeid","5");
			System.out.println("vec field======\n"+vec.elementAt(0));
			System.out.println("vec value======\n"+vec.elementAt(1));
			System.out.println("vec type======\n"+vec.elementAt(2));


			sutil.compareData(dbconn,vec,"t_stroke","strokeid","2","5","1",butil.getCurrentTimestamp());
             */
 /*	java.util.Map map = new java.util.Hashtable();	
			java.util.Vector<java.util.Map> vec = new java.util.Vector<java.util.Map>();
			java.util.Map detail = new java.util.Hashtable();	
			for(int i=0;i<3;i++) {
			    detail = new java.util.Hashtable();	
				detail.put("criteriaid",Integer.toString(i));
				detail.put("result","0");
				vec.addElement(detail);
			}

			map.put("criteria",vec);
			String JSON_str = JSONUtility.parseJSON(map,null,true);
			System.out.println("json_str ==="+JSON_str);
			sutil.isJSONValid(JSON_str);

			JSONObject jObject = new JSONObject(JSON_str);

		    JSONArray paramdata = jObject.getJSONArray("criteria");
		    int n = paramdata.length();
			System.out.println("child length===="+n);
		    for (int i = 0; i < n; ++i) {
				JSONObject paramArray = paramdata.getJSONObject(i);
				System.out.println("criteriaid===="+paramArray.getString("criteriaid"));
				System.out.println("result===="+paramArray.getString("result"));
			};
             */
            //	System.out.println(sutil.convertStrToTimestamp("2017-07-07 21:17:00"));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static java.sql.Connection getSelfConnection(String system) throws Exception {
        if (system.length() > 0) {
            system = system + "_";
        }
        String driver = (String) GlobalVariable.getVariable(system + "DRIVER");
        String url = (String) GlobalVariable.getVariable(system + "URL");
        String user = (String) GlobalVariable.getVariable(system + "USER");
        String pwd = (String) GlobalVariable.getVariable(system + "PASSWORD");
        Class.forName(driver);
        java.sql.Connection conn = java.sql.DriverManager.getConnection(url, user, pwd);
        conn.setTransactionIsolation(java.sql.Connection.TRANSACTION_READ_COMMITTED);
        conn.setAutoCommit(true);
        return conn;
    }

    public boolean insertT_TransactionLog(java.sql.Connection conn, String table, String keyfield, String datavalue, String StrokeID, String AuthorityId, java.sql.Timestamp client_timestamp) throws Exception {
        String[] key = keyfield.split(",");
        String[] value = datavalue.split(",");
        Trace.debug(" INSERT LOG ________________________ keyfield: " + keyfield);
        Trace.debug(" INSERT LOG ________________________ datavalue: " + datavalue);
        KnSQL sql_ins = new KnSQL();
        sql_ins.append("insert into T_TransactionLog (Action,ActionFrom,StrokeId,TableName,ChangedColumn,NewData,KeyName,KeyId,UpdateDate)  \n");
        sql_ins.append(" values ('insert','App',?StrokeId,?TableName,?ChangedColumn,?NewData,?KeyName,?KeyId,?UpdateDate) \n");
        sql_ins.setCompile(true);
        sql_ins.setParameter("StrokeId", StrokeID);
        sql_ins.setParameter("TableName", table);
        sql_ins.setParameter("UpdateDate", client_timestamp);

        KnSQL sql = new KnSQL();
        sql.append("select * from " + table);
        sql.append(" where 1=1 \n");
        for (int i = 0; i < key.length; i++) {
            if (key[i] != null && !"".equalsIgnoreCase(key[i])) {
                sql.append(" and " + key[i] + " = ?" + key[i] + " \n");
                sql.setParameter(key[i], value[i]);
            }
        }
        sql_ins.setParameter("KeyName", key[0]);
        sql_ins.setParameter("KeyId", value[0]);

        java.sql.ResultSet rs = sql.executeQuery(conn);
        if (rs != null && rs.next()) {
            sql.clear();
            sql.append("select distinct s.COLUMN_NAME,s.DATA_TYPE,s.NUMERIC_SCALE  \n");
            sql.append(" from INFORMATION_SCHEMA.COLUMNS s  \n");
            sql.append(" where  s.TABLE_NAME = ?tablename \n");
            sql.setParameter("tablename", table);
            java.sql.ResultSet rs_col = sql.executeQuery(conn);
            while (rs_col.next()) {
                String colname = rs_col.getString("COLUMN_NAME");
                sql_ins.setParameter("ChangedColumn", colname);
                sql_ins.setParameter("NewData", rs.getString(colname));
                sql_ins.executeUpdate(conn);
            }
            return true;
        }
        return false;

    }

    public boolean insertT_TransactionLogTest(java.sql.Connection conn, String table, String keyfield, String datavalue, String StrokeID, String AuthorityId, java.sql.Timestamp client_timestamp) throws Exception {
        System.err.println("insertT_TransactionLogTest --------------");
        String[] key = keyfield.split(",");
        String[] value = datavalue.split(",");
        Trace.debug(" INSERT LOG ________________________ keyfield: " + keyfield);
        Trace.debug(" INSERT LOG ________________________ datavalue: " + datavalue);
        KnSQL sql_ins = new KnSQL();
        sql_ins.append("insert into T_TransactionLog (Action,ActionFrom,StrokeId,TableName,ChangedColumn,NewData,KeyName,KeyId,UpdateDate)  \n");
        sql_ins.append(" values ('insert','App',?StrokeId,?TableName,?ChangedColumn,?NewData,?KeyName,?KeyId,?UpdateDate) \n");
        sql_ins.setCompile(true);
        sql_ins.setParameter("StrokeId", StrokeID);
        sql_ins.setParameter("TableName", table);
        sql_ins.setParameter("UpdateDate", client_timestamp);

        KnSQL sql = new KnSQL();
        sql.append("select * from " + table);
        sql.append(" where 1=1 \n");
        for (int i = 0; i < key.length; i++) {
            if (key[i] != null && !"".equalsIgnoreCase(key[i])) {
                sql.append(" and " + key[i] + " = ?" + key[i] + " \n");
                sql.setParameter(key[i], value[i]);
            }
        }
        sql_ins.setParameter("KeyName", key[0]);
        sql_ins.setParameter("KeyId", value[0]);

        java.sql.ResultSet rs = sql.executeQuery(conn);
        if (rs != null && rs.next()) {
            sql.clear();
            sql.append("select distinct s.COLUMN_NAME,s.DATA_TYPE,s.NUMERIC_SCALE  \n");
            sql.append(" from INFORMATION_SCHEMA.COLUMNS s  \n");
            sql.append(" where  s.TABLE_NAME = ?tablename \n");
            sql.setParameter("tablename", table);
            java.sql.ResultSet rs_col = sql.executeQuery(conn);
            while (rs_col.next()) {
                String colname = rs_col.getString("COLUMN_NAME");
                sql_ins.setParameter("ChangedColumn", colname);
                sql_ins.setParameter("NewData", rs.getString(colname));
                sql_ins.executeUpdate(conn);
            }
            return true;
        }
        return false;

    }

    public java.util.Vector<java.util.Vector> getOldData(java.sql.Connection conn, String table, String keyfield, String datavalue) throws Exception {
        java.util.Vector<java.util.Vector> vecmain = new java.util.Vector<java.util.Vector>();

        java.util.Vector<String> vec = new java.util.Vector<String>();
        java.util.Vector<String> vecdata = new java.util.Vector<String>();
        java.util.Vector<String> vectype = new java.util.Vector<String>();

        KnSQL sql = new KnSQL();
        sql.append("   select distinct s.COLUMN_NAME,s.DATA_TYPE \n");
        sql.append(" from INFORMATION_SCHEMA.COLUMNS s \n");
        sql.append(" where  s.TABLE_NAME = ?tablename \n");
        sql.append(" order by s.COLUMN_NAME \n");
        sql.setParameter("tablename", table);
        java.sql.ResultSet rs = sql.executeQuery(conn);
        while (rs.next()) {
            vec.addElement(rs.getString("COLUMN_NAME"));
            vectype.addElement(rs.getString("DATA_TYPE"));
        }

        sql.clear();
        sql.append("select * from " + table);
        sql.append(" where 1=1 \n");

        String[] key = keyfield.split(",");
        String[] value = datavalue.split(",");

        for (int i = 0; i < key.length; i++) {
            sql.append(" and " + key[i] + " = ?" + key[i] + " \n");
            sql.setParameter(key[i], value[i]);
        }

        rs = sql.executeQuery(conn);
        if (rs.next()) {
            for (int i = 0; i < vec.size(); i++) {
                String fieldname = (String) vec.elementAt(i);
                vecdata.addElement(rs.getString(fieldname));
            }
        }
        vecmain.addElement(vec);
        vecmain.addElement(vecdata);
        vecmain.addElement(vectype);
        return vecmain;
    }

    public void compareData(java.sql.Connection conn, java.util.Vector<java.util.Vector> OldVec, String table, String keyfield, String datavalue, String StrokeID, String AuthorityId, java.sql.Timestamp client_timestamp) throws Exception {
        java.util.Vector<String> vecfield = new java.util.Vector<String>();
        java.util.Vector<String> vecdata = new java.util.Vector<String>();
        java.util.Vector<String> vectype = new java.util.Vector<String>();

        if (OldVec != null && OldVec.size() >= 3) {
            vecfield = (java.util.Vector<String>) OldVec.elementAt(0);
            vecdata = (java.util.Vector<String>) OldVec.elementAt(1);
            vectype = (java.util.Vector<String>) OldVec.elementAt(2);

            KnSQL sql = new KnSQL();
            sql.append("select * from " + table);
            sql.append(" where 1=1 \n");

            String[] key = keyfield.split(",");
            String[] value = datavalue.split(",");

            for (int i = 0; i < key.length; i++) {
                sql.append(" and " + key[i] + " = ?" + key[i] + " \n");
                sql.setParameter(key[i], value[i]);
            }

            java.sql.ResultSet rs = sql.executeQuery(conn);
            if (rs.next()) {
                KnSQL sql_ins = new KnSQL();
                sql_ins.append("insert into T_TransactionLog (Action,ActionFrom,StrokeId,TableName,ChangedColumn,NewData,OldData,KeyName,KeyId,UpdateDate)  \n");
                sql_ins.append(" values ('update','App',?StrokeId,?TableName,?ChangedColumn,?NewData,?OldData,?KeyName,?KeyId,?UpdateDate) \n");
                sql_ins.setCompile(true);
                sql_ins.setParameter("StrokeId", StrokeID);
                sql_ins.setParameter("TableName", table);
                sql_ins.setParameter("KeyName", key[0]);
                sql_ins.setParameter("KeyId", value[0]);
                sql_ins.setParameter("UpdateDate", client_timestamp);
                for (int i = 0; i < vecfield.size(); i++) {
                    String fieldname = (String) vecfield.elementAt(i);
                    String newData = rs.getString(fieldname);
                    String oldData = (String) vecdata.elementAt(i);
                    String type = (String) vectype.elementAt(i);
                    Trace.info("fieldname[" + fieldname + "]  type[" + type + "] new data[" + newData + "]==== old data [" + oldData + "] ");

                    if (newData == null && oldData == null) {
                        continue;
                    } else if ((newData == null && oldData != null) || (newData != null && oldData == null)) {
                        sql_ins.setParameter("NewData", newData);
                        sql_ins.setParameter("OldData", oldData);
                        sql_ins.setParameter("ChangedColumn", fieldname);
                        sql_ins.executeUpdate(conn);
                    } else if (!newData.equals(oldData)) {
                        sql_ins.setParameter("NewData", newData);
                        sql_ins.setParameter("OldData", oldData);
                        sql_ins.setParameter("ChangedColumn", fieldname);
                        sql_ins.executeUpdate(conn);
                    }
                }
            }
        }
    }

    public String getErrorDesc(java.sql.Connection connection, int code) throws Exception {
        KnSQL sql = new KnSQL();
        String msg = "";
        sql.append("  select ErrorMessage from S_ErrorMsg    \n");
        sql.append("  where ErrorCode = ?errorcode    \n");
        sql.setParameter("errorcode", code);

        java.sql.ResultSet rs = sql.executeQuery(connection);
        while (rs.next()) {
            msg = chkNotNull(rs.getString("ErrorMessage"));
        }
        return msg;
    }

    public String chkNotNull(String avalue) {
        String result = (avalue == null) ? "" : avalue;
        return result;
    }

    public boolean isJSONValid(String jsonString) {
        try {
            new JSONObject(jsonString);
        } catch (JSONException ex) {
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(jsonString);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public int getStrokeId(java.sql.Connection connection, String StrokeMobileId) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("select StrokeId from T_Stroke \n");
        sql.append(" where StrokeMobileId = ?strokemobileid \n");
        sql.setParameter("strokemobileid", StrokeMobileId);

        int StrokeId = 0;
        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) {
            StrokeId = rs.getInt("StrokeId");
        } else {
            throw new com.fs.bean.BeanException("Stroke ID not found.", "", 9001);
        }
        Trace.info("StrokeId==============" + StrokeId);
        return StrokeId;
    }

    public int updateT_Stroke(java.sql.Connection connection, int StrokeId, int StepId, int AuthorityId, java.sql.Timestamp ClientTime) throws Exception {
        Trace.info("=======================updateT_Stroke==================");
        // get old data
        java.util.Vector<java.util.Vector> vec = getOldData(connection, "T_Stroke", "StrokeId", Integer.toString(StrokeId));

        int result = 0;
        KnSQL sql = new KnSQL();
        sql.append("update T_Stroke set CurrentStepId=?stepid, LastUpdateById=?lastupdatebyid, LastUpdateDate=?lastupdatedate \n");
        sql.append(" where StrokeId = ?strokeid \n");
        sql.setParameter("strokeid", StrokeId);
        sql.setParameter("stepid", StepId);
        sql.setParameter("lastupdatebyid", AuthorityId);
        sql.setParameter("lastupdatedate", ClientTime);
        result += sql.executeUpdate(connection);

        compareData(connection, vec, "T_Stroke", "StrokeId", Integer.toString(StrokeId), Integer.toString(StrokeId), Integer.toString(AuthorityId), ClientTime);
        return result;
    }

    public boolean doTransaction(java.sql.Connection connection, String apiname, String jsonStr, int stroke_id, boolean updflag) throws Exception {
        Trace.info("=================doTransaction==========apiname[" + apiname + "] =========================");
        Trace.info("=================jsonStr[" + jsonStr + "]");

        boolean result = false;
        switch (apiname) {
            case "patient_info":
                result = patient_info(connection, stroke_id, jsonStr, updflag, "1");
                break;							// update t_stroke
            case "patient_info2":
                result = patient_info(connection, stroke_id, jsonStr, updflag, "2");
                break;							// update t_stroke
            case "patient_pic":
                result = patient_pic(connection, stroke_id, jsonStr, updflag);
                break;									// save picture
            case "symptoms":
                result = symptoms(connection, stroke_id, jsonStr, updflag, "symptoms");
                break;					//insert t_strokesymptom,t_symptomlist
            case "symptoms_time":
                result = symptoms(connection, stroke_id, jsonStr, updflag, "symptoms_time");
                break;			//insert t_strokesymptom,t_symptomlist
            case "symptoms_time_neuro":
                result = symptoms(connection, stroke_id, jsonStr, updflag, "symptoms_time");
                break;			//insert t_strokesymptom,t_symptomlist
            case "symptoms_time_act":
                result = symptoms(connection, stroke_id, jsonStr, updflag, "symptoms_time_act");
                break;	//insert t_strokesymptom,t_symptomlist
            case "blood_drawn_time":
                result = blood_drawn_time(connection, stroke_id, jsonStr, updflag);
                break;						//insert or update t_investigation
            case "glucose":
                result = glucose(connection, stroke_id, jsonStr, updflag);
                break;										//insert or update t_investigation
            case "ekg":
                result = ekg(connection, stroke_id, jsonStr, updflag);
                break;											//insert or update t_investigation
            case "body_weight":
                result = body_weight(connection, stroke_id, jsonStr, updflag);
                break;								//insert or update t_investigation
            case "nihss":
                result = nihss(connection, stroke_id, jsonStr, updflag);
                break;											//insert t_nihss,t_nihssscore,t_nihssscorelog
            case "patient_info_sheet":
                result = patient_info_sheet(connection, stroke_id, jsonStr, updflag);
                break;						//update t_treatment
            case "ct_scan":
                result = ct_scan(connection, stroke_id, jsonStr, updflag);
                break;										//update t_treatment,insert t_criteriaresult,t_criteriaresultlog
            case "stroke_type":
                result = stroke_type(connection, stroke_id, jsonStr, updflag);
                break;								//insert t_stroketype
            case "treatment":
                result = treatment(connection, stroke_id, jsonStr, updflag);
                break;									//update t_treatment
            case "nicardipine_1":
                result = nicardipine(connection, stroke_id, jsonStr, updflag, "1");
                break;								//update tpa
            case "nicardipine_2":
                result = nicardipine(connection, stroke_id, jsonStr, updflag, "2");
                break;								//update tpa
            case "record_nicardipine":
                result = nicardipine(connection, stroke_id, jsonStr, updflag, "3");
                break;								//update tpa
            case "iv_tpa":
                result = nicardipine(connection, stroke_id, jsonStr, updflag, "4");
                break;								//update tpa
            case "tpa_push_time":
                result = tpa_push_time(connection, stroke_id, jsonStr, updflag);
                break;							//update tpa
            case "cta_criteria":
                result = cta_criteria(connection, stroke_id, jsonStr, updflag);
                break;									//update t_treatment
            case "cta_time":
                result = cta_time(connection, stroke_id, jsonStr, updflag);
                break;									//insert/update t_treatment
            case "criteria_for_thrombectomy":
                result = criteria_for_thrombectomy(connection, stroke_id, jsonStr, updflag);
                break;			//insert t_treatment,t_criteriaresult,t_criteriaresultlog
            case "thrombectomy_decision":
                result = thrombectomy_decision(connection, stroke_id, jsonStr, updflag, "1");
                break;			//update t_treatment
            case "angiogram_anesthetic_timestamp":
                result = angiogram_anesthetic_timestamp(connection, stroke_id, jsonStr, updflag);
                break;  //insert/update t_thrombectomy
            case "angiogram_groin_puncture":
                result = angiogram_groin_puncture(connection, stroke_id, jsonStr, updflag, "1");
                break;		//update t_thrombectomy
            case "positive_angiogram_timestamp":
                result = angiogram_groin_puncture(connection, stroke_id, jsonStr, updflag, "2");
                break;		//update t_thrombectomy
            case "angiogram_recanalization_timestamp":
                result = angiogram_groin_puncture(connection, stroke_id, jsonStr, updflag, "3");
                break;		//update t_thrombectomy
            case "angiogram_ia_timestamp":
                result = angiogram_groin_puncture(connection, stroke_id, jsonStr, updflag, "4");
                break;		//update t_thrombectomy
            case "angiogram_end_procedure_timestamp":
                result = angiogram_groin_puncture(connection, stroke_id, jsonStr, updflag, "5");
                break;		//update t_thrombectomy
            case "treatment_summary":
                result = treatment_summary(connection, stroke_id, jsonStr, updflag);
                break;						//update t_stroke
            case "treatment_summary_strokeunit":
                result = treatment_summary(connection, stroke_id, jsonStr, updflag);
                break;						//update t_stroke
            case "document_signed_next":
                result = document_signed_next(connection, stroke_id, jsonStr, updflag);
                break;				//update t_stroke
            case "consent_signed":
                result = consent_signed(connection, stroke_id, jsonStr, updflag);
                break;							//update t_stroke
            case "vital_sign_time":
                result = vital_sign_time(connection, stroke_id, jsonStr, updflag, "vital_sign_time");
                break;		//insert/update t_vitalsign
            case "vital_sign_result":
                result = vital_sign_time(connection, stroke_id, jsonStr, updflag, "vital_sign_result");
                break;	//insert/update t_vitalsign
            case "ct_result":
                result = thrombectomy_decision(connection, stroke_id, jsonStr, updflag, "2");
                break;	        //update t_treatment
            case "lab_result":
                result = lab_result(connection, stroke_id, jsonStr, updflag);
                break;	                                //insert/update t_lab
            case "angiogram_next":
                result = angiogram_next(connection, stroke_id, jsonStr, updflag);
                break;	                        //update t_stroke
            case "tpa_criteria_sys":
                result = tpa_criteria(connection, stroke_id, jsonStr, updflag);
                break;	                                //insert t_criteriaresult
            case "tpa_criteria_neuro":
                result = tpa_criteria(connection, stroke_id, jsonStr, updflag);
                break;	                                //insert t_criteriaresult

            default:
                throw new Exception("SERR:14");
        }
        return result;
    }

    public int genStrokeid(java.sql.Connection connection, String apiname, String jsonStr) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"genstrokeid": {														       */
 /*															 "stroke_mobile_id": 1,													   */
 /*															 "client_time": "20170502 13:20:20",								   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",          								           */
 /*															 "strokeid": ""          								                       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================genStrokeid==========apiname[" + apiname + "] =====================");
        Trace.info("=================jsonStr[" + jsonStr + "]");

        JSONObject jObject = new JSONObject(jsonStr);
        String stroke_mobile_id = jObject.getString("stroke_mobile_id");
        String strokeid_str = jObject.getString("strokeid");

        String client_time = jObject.getString("client_time");
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"));
        int strokeid = 0;
        if (strokeid_str == null || strokeid_str.trim().equals("")) {

            //insert t_stroke for generating stroke_id
            KnSQL sql = new KnSQL();
            sql.append("insert into T_Stroke(StrokeMobileId,OpenStrokeById,OpenStrokeTime,LastUpdateById,LastUpdateDate) \n");
            sql.append("values(?StrokeMobileId,?OpenStrokeById,?OpenStrokeTime,?LastUpdateById,?LastUpdateDate) \n");
            sql.setParameter("StrokeMobileId", stroke_mobile_id);
            sql.setParameter("OpenStrokeById", authority_id);
            sql.setParameter("OpenStrokeTime", client_time);
            sql.setParameter("LastUpdateById", authority_id);
            sql.setParameter("LastUpdateDate", client_time);
            //sql.executeUpdate(connection);
            sql.executeUpdate(connection, java.sql.Statement.RETURN_GENERATED_KEYS);

            //get stroke_id
            //int strokeid =  getStrokeId(connection,authority_id,stroke_mobile_id);
            strokeid = (int) sql.getGeneratedKey();
            Trace.info("============strokeid========" + strokeid);
        } else {
            strokeid = Integer.parseInt(strokeid_str);
        }
        if (checkDulplicateT_AuthorityArrival(connection, authority_id, strokeid)) {
            insertT_AuthorityArrival(connection, authority_id, strokeid, convertStrToTimestamp(jObject.getString("client_time")));
        }
        /*
			sql.clear();
			sql.append("insert t_beacon(Strokeid,Beaconrecordid,beacontime,lastupdatebyid,lastupdatedate) \n");
			sql.append(" values(?Strokeid,?Beaconrecordid,?beacontime,?LastUpdateById,?LastUpdateDate) \n");
			sql.setParameter("Strokeid",strokeid);
			sql.setParameter("Beaconrecordid","1");
			sql.setParameter("beacontime",client_time);
			sql.setParameter("LastUpdateById",authority_id);
			sql.setParameter("LastUpdateDate",client_time);
			sql.executeUpdate(connection);
         */
        insertBeacon(connection, strokeid, authority_id, convertStrToTimestamp(client_time), "1");

        return strokeid;
    }

    /*	public int getStrokeId(java.sql.Connection connection,String authority_id,String stroke_mobile_id)throws Exception{
			java.sql.Timestamp sysdate = butil.getCurrentTimestamp();
			KnSQL sql = new KnSQL();
			sql.append("select StrokeId from T_Stroke \n");
			sql.append(" where OpenStrokeById = ?OpenStrokeById \n");
			sql.append("   and LastUpdateById = ?LastUpdateById \n");
				sql.append(" and StrokeMobileId = ?StrokeMobileId \n");
			sql.append(" order by StrokeId desc \n");

			sql.setParameter("OpenStrokeById",authority_id);
			sql.setParameter("LastUpdateById",authority_id);
			sql.setParameter("StrokeMobileId",stroke_mobile_id);

			java.sql.ResultSet rs = sql.executeQuery(connection);
			if(rs != null && rs.next()){
				return rs.getInt("StrokeId");
			}
			return 0;
		}*/
    public boolean ekg(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"ekg": {         														       */
 /*															 "ekg_flag": 1,												  	               */
 /*															 "ekg_flag_time": "20170502 13:20:20",							   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        int result = 0;
        JSONObject jObject = new JSONObject(jsonStr);
        String ekg_flag = jObject.getString("ekg_flag");
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        java.sql.Timestamp ekg_flag_time = convertStrToTimestamp(jObject.getString("ekg_flag_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);

        String stepid = jObject.getString("stepid");

        Hashtable<String, Object> ht = new Hashtable<String, Object>();
        ht.put("ekg_flag", ekg_flag);
        ht.put("authority_id", authority_id);
        ht.put("client_time", client_time);
        ht.put("ekg_flag_time", ekg_flag_time);
        ht.put("stroke_id", stroke_id);

        KnSQL sql = new KnSQL();
        sql.append("select strokeid from t_investigation \n");
        sql.append("where strokeid = ?strokeid \n");
        sql.setParameter("strokeid", stroke_id);

        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) { //found data
            java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_investigation", "strokeid", Integer.toString(stroke_id));

            result = updateT_Investigation(connection, "ekg", ht);
            compareData(connection, vec, "t_investigation", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);

        } else { //not found data
            result = insertT_Investigation(connection, "ekg", ht);
            insertT_TransactionLog(connection, "t_investigation", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        }
        if (result > 0 && updflag) {
            result = updateT_Stroke(connection, stroke_id, Integer.parseInt(stepid), Integer.parseInt(authority_id), client_time);
        }
        /*	if(result <= 0) {
				throw new Exception("No data to insert/update in t_investigation");
			}*/
        return true;
    }

    public int updateT_Investigation(java.sql.Connection connection, String modulename, Hashtable<String, Object> ht) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("update t_investigation set \n");

        if (!ht.isEmpty()) {
            if (modulename != null && modulename.equalsIgnoreCase("ekg")) {
                sql.append(" ekgflag = ?ekgflag , \n");
                sql.append(" ekgflagbyid = ?ekgflagbyid , \n");
                sql.append(" ekgflagtime = ?ekgflagtime , \n");
                sql.append(" lastupdatebyid = ?lastupdatebyid , \n");
                sql.append(" lastupdatedate = ?lastupdatedate \n");

                sql.setParameter("ekgflag", (String) ht.get("ekg_flag"));
                sql.setParameter("ekgflagbyid", (String) ht.get("authority_id"));
                sql.setParameter("ekgflagtime", (java.sql.Timestamp) ht.get("ekg_flag_time"));
                sql.setParameter("lastupdatedate", (java.sql.Timestamp) ht.get("client_time"));
                sql.setParameter("lastupdatebyid", (String) ht.get("authority_id"));
            } else {
                throw new Exception("Can't prepare SQL for updating t_investigation");
            }

            sql.append(" where strokeid  = ?strokeid \n");
            sql.setParameter("strokeid", (int) ht.get("stroke_id"));
            return sql.executeUpdate(connection);
        }
        return 0;
    }

    public int insertT_Investigation(java.sql.Connection connection, String modulename, Hashtable<String, Object> ht) throws Exception {
        if (!ht.isEmpty()) {
            KnSQL sql = new KnSQL();
            if (modulename != null && modulename.equalsIgnoreCase("ekg")) {
                sql.append("insert into t_investigation(strokeid,ekgflag,ekgflagbyid,ekgflagtime,lastupdatebyid,lastupdatedate)  \n");
                sql.append("values \n");
                sql.append(" (?strokeid,?ekgflag,?ekgflagbyid,?ekgflagtime,?lastupdatebyid,?lastupdatedate) \n");

                sql.setParameter("ekgflag", (String) ht.get("ekg_flag"));
                sql.setParameter("ekgflagbyid", (String) ht.get("authority_id"));
                sql.setParameter("lastupdatedate", (java.sql.Timestamp) ht.get("client_time"));
                sql.setParameter("lastupdatebyid", (String) ht.get("authority_id"));
                sql.setParameter("ekgflagtime", (java.sql.Timestamp) ht.get("ekg_flag_time"));
                sql.setParameter("strokeid", (int) ht.get("stroke_id"));
            } else {
                throw new Exception("Can't prepare SQL for inserting t_investigation");
            }
            return sql.executeUpdate(connection);
        }
        return 0;
    }

    public boolean ct_scan(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"ct_scan": {       														       */
 /*															 "ct_aspect": 1,											  	               */
 /*															 "ct_scan_time": "20170502 13:20:20",							   */
 /*															 "ct_result_time": "20170502 13:20:20",	    				   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "criteria": [                                  								   */
 /*															 {"criteriaid": "1","result":"0"},          								   */
 /*															 {"criteriaid": "2","result":"1"},          								   */
 /*																]							        									       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        int result = 0;
        JSONObject jObject = new JSONObject(jsonStr);
        String ct_aspect = jObject.getString("ct_aspect");
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        java.sql.Timestamp ct_scan_time = convertStrToTimestamp(jObject.getString("ct_scan_time"));
        java.sql.Timestamp ct_result_time = convertStrToTimestamp(jObject.getString("ct_result_time"));
        int stepid = Integer.parseInt(jObject.getString("stepid"));
        JSONArray paramdata = jObject.getJSONArray("criteria");

        int n = paramdata.length();
        /*	if(n <=0 ) {
				throw new Exception("data length is 0");
			}*/

        Trace.info(this, "data length =============" + n);
        Hashtable<String, Object> ht = new Hashtable<String, Object>();
        ht.put("ct_aspect", ct_aspect);
        ht.put("authority_id", authority_id);
        ht.put("client_time", client_time);
        ht.put("ct_scan_time", ct_scan_time);
        ht.put("ct_result_time", ct_result_time);
        ht.put("stroke_id", stroke_id);

        //update t_treatment
        result = updateT_treatment(connection, ht);

        /*if(result <= 0) {
				 throw new Exception("Not Found Data in t_treatment");
			}*/
        //move data to t_criteriaresultlog
        //moveDataToLog(connection,ht);
        KnSQL sql = new KnSQL();
        sql.append("insert into t_criteriaresult(criteriaid,strokeid,result,resultbyid,resulttime,lastupdatebyid,lastupdatedate) \n");
        sql.append(" values (?criteriaid,?strokeid,?result,?resultbyid,?resulttime,?lastupdatebyid,?lastupdatedate) \n");
        sql.setCompile(true);
        String d_criteriaid = null;
        String d_result = null;
        for (int i = 0; i < n; ++i) {
            JSONObject paramArray = paramdata.getJSONObject(i);
            d_criteriaid = paramArray.getString("criteriaid");
            d_result = paramArray.getString("result");

            Trace.info(this, "criteriaid====" + paramArray.getString("criteriaid"));
            Trace.info(this, "result====" + paramArray.getString("result"));

            if (d_criteriaid == null || d_criteriaid.equals("")) {
                throw new Exception("criteriaid is undefined");
            }
            /*	if(d_result == null || d_result.equals("") || !butil.isInset(d_result,"0,1")) {
							throw new Exception("result is undefined or result is out of set[0,1] ");
						}*/
            if (isDuplicateT_CriteriaResult(connection, stroke_id, d_criteriaid)) {
                throw new Exception("Duplicate criteriaid[" + d_criteriaid + "],strokeid[" + stroke_id + "]");
            }
            sql.clearParameters();
            sql.setParameter("strokeid", stroke_id);
            sql.setParameter("criteriaid", d_criteriaid);
            sql.setParameter("result", d_result);
            sql.setParameter("resultbyid", authority_id);
            sql.setParameter("lastupdatebyid", authority_id);
            sql.setParameter("resulttime", ct_result_time);
            sql.setParameter("lastupdatedate", client_time);
            result = sql.executeUpdate(connection);
            insertT_TransactionLog(connection, "t_criteriaresult", "criteriaid,strokeid", d_criteriaid + "," + Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);

            if (result > 0 && updflag) {
                result = updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
                /*if(result <= 0) {
								throw new Exception("Not Found Data in t_stroke");
							}*/
            }
            /*else {
							throw new Exception("No data to insert/update in t_criteriaresult");
						}*/
        }
        insertBeaconTemp(connection, stroke_id, authority_id, "3", jObject.getString("ct_scan_time"), -1);

        return true;
    }

    public int updateT_treatment(java.sql.Connection connection, Hashtable<String, Object> ht) throws Exception {
        int stroke_id = (int) ht.get("stroke_id");
        java.sql.Timestamp client_time = (java.sql.Timestamp) ht.get("client_time");
        String authority_id = (String) ht.get("authority_id");
        java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_treatment", "strokeid", Integer.toString(stroke_id));

        //update t_treatment
        KnSQL sql = new KnSQL();
        sql.append("update t_treatment \n");
        sql.append(" set ctscantime = ?ctscantime , \n");
        sql.append("      ctscanbyid = ?ctscanbyid , \n");
        sql.append("      ctaspect = ?ctaspect , \n");
        sql.append("      ctbyid = ?ctbyid , \n");
        sql.append("      ctresulttime = ?ctresulttime, \n");
        sql.append("     lastupdatebyid =?lastupdatebyid ,\n");
        sql.append("     lastupdatedate =?lastupdatedate  \n");
        sql.append("where strokeid = ?strokeid \n");
        sql.setParameter("strokeid", (int) ht.get("stroke_id"));
        sql.setParameter("ctscantime", (java.sql.Timestamp) ht.get("ct_scan_time"));
        sql.setParameter("ctscanbyid", authority_id);
        sql.setParameter("ctaspect", (String) ht.get("ct_aspect"));
        sql.setParameter("ctbyid", authority_id);
        sql.setParameter("ctresulttime", (java.sql.Timestamp) ht.get("ct_result_time"));
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        int result = sql.executeUpdate(connection);
        Trace.info("result from  update t_treatement ct_scan ===================" + result);
        //update T_TransactionLog
        compareData(connection, vec, "t_treatment", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);

        return result;
    }

    public void moveDataToLog(java.sql.Connection connection, Hashtable<String, Object> ht) throws Exception {
        int stroke_id = (int) ht.get("stroke_id");
        int authority_id = Integer.parseInt((String) ht.get("authority_id"));
        java.sql.Timestamp client_time = (java.sql.Timestamp) ht.get("client_time");

        KnSQL sql1 = new KnSQL();
        sql1.append("select criteriaid,result,resultbyid,resulttime from t_criteriaresult \n");
        sql1.append("where strokeid = ?strokeid \n");
        sql1.setParameter("strokeid", stroke_id);

        java.sql.ResultSet rs = sql1.executeQuery(connection);

        KnSQL sql = new KnSQL();
        sql.append("select criteriaid from t_criteriaresultlog \n");
        sql.append("where strokeid = ?strokeid \n");
        sql.append("and criteriaid = ?criteriaid \n");
        sql.setCompile(true);

        while (rs.next()) {
            //check data in t_criteriaresultlog
            sql.clearParameters();
            sql.setParameter("strokeid", stroke_id);
            sql.setParameter("criteriaid", rs.getInt("criteriaid"));
            java.sql.ResultSet rslog = sql.executeQuery(connection);
            if (rslog != null && rslog.next()) { //found data
                //update t_criteriaresultlog
                updateT_CriteriaresultLog(connection, stroke_id, authority_id, client_time, rs.getInt("criteriaid"), rs.getString("result"), rs.getInt("resultbyid"), rs.getTimestamp("resulttime"));
            } else { //not found data
                //insert t_criteriaresultlog
                insertT_CriteriaresultLog(connection, stroke_id, authority_id, client_time, rs.getInt("criteriaid"), rs.getString("result"), rs.getInt("resultbyid"), rs.getTimestamp("resulttime"));
            }
        }
        deleteT_Criteriaresult(connection, stroke_id);
    }

    public void insertT_CriteriaresultLog(java.sql.Connection connection, int stroke_id, int authority_id, java.sql.Timestamp client_time, int criteriaid, String result, int resultbyid, java.sql.Timestamp resulttime) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("insert  t_criteriaresultlog(criteriaid,strokeid,result,resultbyid,resulttime,lastupdatebyid,lastupdatedate) \n");
        sql.append(" values (?criteriaid,?strokeid,?result,?resultbyid,?resulttime,?lastupdatebyid,?lastupdatedate)  \n");
        sql.setParameter("strokeid", stroke_id);
        sql.setParameter("criteriaid", criteriaid);
        sql.setParameter("result", result);
        sql.setParameter("resultbyid", resultbyid);
        sql.setParameter("resulttime", resulttime);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        sql.executeUpdate(connection);
    }

    public void updateT_CriteriaresultLog(java.sql.Connection connection, int stroke_id, int authority_id, java.sql.Timestamp client_time, int criteriaid, String result, int resultbyid, java.sql.Timestamp resulttime) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("update t_criteriaresultlog \n");
        sql.append("set result= ?result ,\n");
        sql.append("     resultbyid= ?resultbyid ,\n");
        sql.append("     resulttime = ?resulttime ,\n ");
        sql.append("     lastupdatebyid =?lastupdatebyid ,\n");
        sql.append("     lastupdatedate =?lastupdatedate  \n");
        sql.append("where strokeid = ?strokeid \n");
        sql.append("and criteriaid = ?criteriaid \n");
        sql.setParameter("strokeid", stroke_id);
        sql.setParameter("criteriaid", criteriaid);
        sql.setParameter("result", result);
        sql.setParameter("resultbyid", resultbyid);
        sql.setParameter("resulttime", resulttime);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        sql.executeUpdate(connection);
    }

    public void deleteT_Criteriaresult(java.sql.Connection connection, int stroke_id) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("delete from t_criteriaresult \n");
        sql.append("where strokeid = ?strokeid \n");
        sql.setParameter("strokeid", stroke_id);
        sql.executeUpdate(connection);
    }

    private boolean isDuplicateT_CriteriaResult(java.sql.Connection connection, int stroke_id, String criteriaid) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("select criteriaid from t_criteriaresult \n");
        sql.append("where criteriaid = ?criteriaid \n");
        sql.append("and strokeid = ?strokeid \n");
        sql.setParameter("strokeid", stroke_id);
        sql.setParameter("criteriaid", criteriaid);

        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) {
            return true;
        }
        return false;
    }

    public boolean symptoms(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag, String modulename) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"symptoms": {       														   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",          								           */
 /*															 "stepid": "1",                              								   */
 /*															 "symptoms": "1"                            								   */
 /*															 "other_symptom": "1"                    								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        /**
         * *******************************************************************************
         */
        /*															"symptoms_time": {       												   */
 /*															 "last_seen_normal": 1,									  	               */
 /*															 "onset": 1,	               								  	               */
 /*															 "diff_time": "20170502 13:20:20",				  	               */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",          								           */
 /*															 "stepid": "1",                              								   */
 /*															 "symptoms": "1"                            								   */
 /*															 "other_symptom": "1"                    								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        /**
         * *******************************************************************************
         */
        /*															"symptoms_time_act": {  												   */
 /*															 "last_seen_normal":  "20170502 13:20:20",	  	               */
 /*															 "onset": 1,	               								  	               */
 /*															 "diff_time": "20170502 13:20:20",				  	               */
 /*															 "activate_fast_track_flag": 1,						  	               */
 /*															 "mimics_id": 1,             								  	               */
 /*															 "sap_id": "19002345",          								           */
 /*															 "reason": 1,               								  	               */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "symptoms": "1"                            								   */
 /*															 "other_symptom": "1"                    								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr " + jsonStr);
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = jObject.getInt("stepid");
        String symptoms = jObject.getString("symptoms");
        String other_symptom = jObject.getString("other_symptom");

        Hashtable<String, Object> ht = new Hashtable<String, Object>();
        ht.put("authority_id", authority_id);
        ht.put("client_time", client_time);
        ht.put("stroke_id", stroke_id);

        if (modulename != null && modulename.equalsIgnoreCase("symptoms_time")) {
            ht.put("last_seen_normal", jObject.getString("last_seen_normal"));
            ht.put("onset", jObject.getString("onset"));
            ht.put("diff_time", jObject.getString("diff_time"));
        } else if (modulename != null && modulename.equalsIgnoreCase("symptoms_time_act")) {
            ht.put("last_seen_normal", jObject.getString("last_seen_normal"));
            ht.put("onset", jObject.getString("onset"));
            ht.put("diff_time", jObject.getString("diff_time"));
            ht.put("activate_fast_track_flag", jObject.getString("activate_fast_track_flag"));
            ht.put("reason", jObject.getString("reason"));
            ht.put("mimics_id", jObject.getString("mimics_id"));
        }

        int result = 0;
        // insert T_StrokeSymptom
        long StrokeSymptomId = insertT_StrokeSymptom(connection, modulename, ht);

        //	int StrokeSymptomId = getStrokeSymptomId(connection);
        insertT_TransactionLog(connection, "T_StrokeSymptom", "StrokeSymptomId", Long.toString(StrokeSymptomId), Integer.toString(stroke_id), authority_id, client_time);

        // insert T_SymptomList
        String[] sList = symptoms.trim().split("\\|");
        for (int i = 0; i < sList.length; i++) {
            //if(sList[i]!=null && sList[i].equals("1")) {
            Trace.info(modulename + "============" + sList[i] + "== StrokeSymptomId ==" + StrokeSymptomId);
            result += insertT_SymptomList(connection, StrokeSymptomId, sList[i], stroke_id, other_symptom);
            //}
        }

        if (modulename != null && modulename.equalsIgnoreCase("symptoms") && updflag) {
            // update T_Stroke
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }
        return true;
    }

    public long insertT_StrokeSymptom(java.sql.Connection connection, String modulename, Hashtable<String, Object> ht) throws Exception {
        Trace.info("=====================insertT_StrokeSymptom====================");

        int result = 0;
        java.sql.Timestamp client_time = (java.sql.Timestamp) ht.get("client_time");
        //	String client_time = (String)ht.get("client_time");
        int authority_id = Integer.parseInt((String) ht.get("authority_id"));

        String columnStr = "";
        String valueStr = "";
        if (modulename != null && modulename.equalsIgnoreCase("symptoms_time")) {
            columnStr = ",LastSeenNormal,Onset,DiffTime";
            valueStr = ",?lastseennormal,?onset,?difftime";
        } else if (modulename != null && modulename.equalsIgnoreCase("symptoms_time_act")) {
            columnStr = ",LastSeenNormal,Onset,DiffTime,ActivateFastTrackFlag,Reason,MIMICSId";
            valueStr = ",?lastseennormal,?onset,?difftime,?activatefasttrackflag,?reason,?mimicsid";
        }

        KnSQL sql = new KnSQL();
        sql.append("insert into T_StrokeSymptom (StrokeId" + columnStr + ",SymptomById,SymptomTime,LastUpdateById,LastUpdateDate) \n");
        sql.append("values(?strokeid" + valueStr + ",?symptombyid,?symptomtime,?lastupdatebyid,?lastupdatedate) \n");
        sql.setParameter("strokeid", (int) ht.get("stroke_id"));
        if (modulename != null && modulename.equalsIgnoreCase("symptoms_time")) {
            sql.setParameter("lastseennormal", (String) ht.get("last_seen_normal"));
            sql.setParameter("onset", (String) ht.get("onset"));
            sql.setParameter("difftime", (String) ht.get("diff_time"));
        } else if (modulename != null && modulename.equalsIgnoreCase("symptoms_time_act")) {
            sql.setParameter("lastseennormal", (String) ht.get("last_seen_normal"));
            sql.setParameter("onset", (String) ht.get("onset"));
            sql.setParameter("difftime", (String) ht.get("diff_time"));
            sql.setParameter("activatefasttrackflag", (String) ht.get("activate_fast_track_flag"));
            sql.setParameter("reason", (String) ht.get("reason"));
            sql.setParameter("mimicsid", (String) ht.get("mimics_id"));
        }
        sql.setParameter("symptombyid", authority_id);
        sql.setParameter("symptomtime", client_time);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        //result += sql.executeUpdate(connection);
        result += sql.executeUpdate(connection, java.sql.Statement.RETURN_GENERATED_KEYS);

        return sql.getGeneratedKey();
    }

    /*	public int getStrokeSymptomId(java.sql.Connection connection) throws Exception {
				KnSQL sql = new KnSQL();
				sql.append("select max(StrokeSymptomId) as StrokeSymptomId from T_StrokeSymptom \n");

				java.sql.ResultSet rs = sql.executeQuery(connection);
				int StrokeSymptomId = 0;
				if(rs!=null && rs.next()) {
					StrokeSymptomId = rs.getInt("StrokeSymptomId");
				}
				return StrokeSymptomId;
			}*/
    public int insertT_SymptomList(java.sql.Connection connection, long StrokeSymptomId, String ordinal, int stroke_id, String other_symptom) throws Exception {
        Trace.info("=====================insertT_SymptomList====================");
        Trace.info("StrokeSymptomId=================" + StrokeSymptomId);
        Trace.info("ordinal=========================" + ordinal);

        int result = 0;
        KnSQL sql = new KnSQL();
        sql.append("select SymptomId from M_Symptom \n");
        sql.append(" where Ordinal = ?ordinal \n");
        sql.append("   and IsActive = '1' \n");
        sql.setParameter("ordinal", ordinal);

        java.sql.ResultSet rs = sql.executeQuery(connection);
        int SymptomId = 0;
        if (rs != null && rs.next()) {
            SymptomId = rs.getInt("SymptomId");
            if (SymptomId != 10) {
                other_symptom = "";
            }
            sql.clear();
            sql.append("insert into T_SymptomList (StrokeSymptomId,SymptomId,OtherSymptom) \n");
            sql.append("values(?strokesymptomid,?symptomid,?othersymptom) \n");
            sql.setParameter("strokesymptomid", StrokeSymptomId);
            sql.setParameter("symptomid", SymptomId);
            sql.setParameter("othersymptom", other_symptom);
            result += sql.executeUpdate(connection);
        }
        return result;
    }

    public boolean nicardipine(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag, String modulename) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"nicardipine_1": {       													   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "lower_bp1_push_flag": "1"            								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        /**
         * *******************************************************************************
         */
        /*															"nicardipine_2": {       													   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "lower_bp2_push_flag": "1"            								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        /**
         * *******************************************************************************
         */
        /*															"record_nicardipine": {       		    								   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "lowerbpdripflag": "1"                  								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        /**
         * *******************************************************************************
         */
        /*															"iv_tpa": {       					        								   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "rtpa_push_amt": "1"            								           */
 /*															 "rtpa_total": "1"            								                   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = Integer.parseInt(jObject.getString("stepid"));

        Hashtable<String, Object> ht = new Hashtable<String, Object>();
        ht.put("authority_id", authority_id);
        ht.put("client_time", client_time);
        ht.put("stroke_id", stroke_id);

        if (modulename != null && modulename.equals("1")) {
            ht.put("lower_bp1_push_flag", jObject.getString("lower_bp1_push_flag"));
        } else if (modulename != null && modulename.equals("2")) {
            ht.put("lower_bp2_push_flag", jObject.getString("lower_bp2_push_flag"));
        } else if (modulename != null && modulename.equals("3")) {
            ht.put("lowerbpdripflag", jObject.getString("lowerbpdripflag"));
        } else if (modulename != null && modulename.equals("4")) {
            ht.put("rtpa_push_amt", jObject.getString("rtpa_push_amt"));
            ht.put("rtpa_total", jObject.getString("rtpa_total"));
        }

        int result = 0;
        // insert/update T_TPA
        result += updateT_TPA(connection, ht, modulename);
        if (updflag) {
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public int updateT_TPA(java.sql.Connection connection, Hashtable<String, Object> ht, String modulename) throws Exception {
        Trace.info("=======================updateT_TPA==================");
        // get old data
        int stroke_id = (int) ht.get("stroke_id");
        java.sql.Timestamp client_time = (java.sql.Timestamp) ht.get("client_time");
        String authority_id = (String) ht.get("authority_id");

        java.util.Vector<java.util.Vector> vec = null;

        String columnStr = "";
        String insCol = "";
        String param = "";
        if (modulename != null && modulename.equals("1")) {
            columnStr = "LowerBP1PushFlag=?lowerbp1pushflag,LowerBP1PushTime=?lowerbp1pushtime,";
            insCol = "LowerBP1PushFlag,LowerBP1PushTime,";
            param = "?lowerbp1pushflag,?lowerbp1pushtime,";
        } else if (modulename != null && modulename.equals("2")) {
            columnStr = "LowerBP2PushFlag=?lowerbp2pushflag,LowerBP2PushTime=?lowerbp2pushtime,";
            insCol = "LowerBP2PushFlag,LowerBP2PushTime,";
            param = "?lowerbp2pushflag,?lowerbp2pushtime,";
        } else if (modulename != null && modulename.equals("3")) {
            columnStr = "LowerBPDripFlag=?lowerbpdripflag,LowerBPDripTime=?lowerbpdriptime,lowerbpbyid=?lowerbpbyid,lowerbptime=?lowerbptime,";
            insCol = "LowerBPDripFlag,LowerBPDripTime,lowerbpbyid,lowerbptime,";
            param = "?lowerbpdripflag,?lowerbpdriptime,?lowerbpbyid,?lowerbptime,";
        } else if (modulename != null && modulename.equals("4")) {
            columnStr = "rtPATotal=?rtPATotal,rtPAPushAmt=?rtPAPushAmt,rtPAPushById=?rtPAPushById,rtPAPushTime=?rtPAPushTime,";
            insCol = "rtPATotal,rtPAPushAmt,rtPAPushById,rtPAPushTime,";
            param = "?rtPATotal,?rtPAPushAmt,?rtPAPushById,?rtPAPushTime,";
        }

        KnSQL sql = new KnSQL();
        int result = 0;
        int getstroke = getValueStrokeId(connection, stroke_id, "T_TPA");
        Trace.info("+++check t_tpa exist++++++++++++++++" + getstroke);
        if (getstroke <= 0) {
            sql.append("insert into t_tpa(" + insCol + "LastUpdateById,LastUpdateDate,strokeid) \n");
            sql.append("values (" + param + "?LastUpdateById,?LastUpdateDate,?strokeid) \n");
        } else {
            vec = getOldData(connection, "T_TPA", "StrokeId", Integer.toString(stroke_id));
            sql.append("update T_TPA set " + columnStr + "LastUpdateById=?lastupdatebyid,LastUpdateDate=?lastupdatedate \n");
            sql.append(" where StrokeId = ?strokeid \n");
        }
        sql.setParameter("strokeid", stroke_id);
        if (modulename != null && modulename.equals("1")) {
            sql.setParameter("lowerbp1pushflag", (String) ht.get("lower_bp1_push_flag"));
            sql.setParameter("lowerbp1pushtime", client_time);
        } else if (modulename != null && modulename.equals("2")) {
            sql.setParameter("lowerbp2pushflag", (String) ht.get("lower_bp2_push_flag"));
            sql.setParameter("lowerbp2pushtime", client_time);
        } else if (modulename != null && modulename.equals("3")) {
            sql.setParameter("lowerbpdripflag", (String) ht.get("lowerbpdripflag"));
            sql.setParameter("lowerbpdriptime", client_time);
            sql.setParameter("lowerbpbyid", authority_id);
            sql.setParameter("lowerbptime", client_time);
        } else if (modulename != null && modulename.equals("4")) {
            sql.setParameter("rtPAPushAmt", (String) ht.get("rtpa_push_amt"));
            sql.setParameter("rtPATotal", (String) ht.get("rtpa_total"));
            sql.setParameter("rtPAPushById", authority_id);
            sql.setParameter("rtPAPushTime", client_time);
        }
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        result += sql.executeUpdate(connection);
        if (getstroke > 0) {
            compareData(connection, vec, "T_TPA", "StrokeId", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        } else {
            insertT_TransactionLog(connection, "T_TPA", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        }

        return result;
    }

    public boolean stroke_type(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"stroke_type": {        													   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "stroke_type_ids": "1"                 								   */
 /*															 "attending_id": "1"                 								       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = Integer.parseInt(jObject.getString("stepid"));
        String stoketypeids = jObject.getString("stroke_type_ids");

        String[] stroketypeid = stoketypeids.split(",");

        int result = 0;
        KnSQL sql = new KnSQL();
        sql.append("insert into T_StrokeType (StrokeId,StrokeTypeId,StrokeTypeById,StrokeTypeTime,LastUpdateById,LastUpdateDate) \n");
        sql.append("values(?StrokeId,?StrokeTypeId,?StrokeTypeById,?StrokeTypeTime,?LastUpdateById,?LastUpdateDate) \n");
        sql.setCompile(true);
        for (int i = 0; i < stroketypeid.length; i++) {
            Trace.info("=========stroketypeid=============" + stroketypeid[i]);
            sql.clearParameters();
            sql.setParameter("StrokeId", stroke_id);
            sql.setParameter("StrokeTypeId", stroketypeid[i]);
            sql.setParameter("StrokeTypeById", authority_id);
            sql.setParameter("StrokeTypeTime", client_time);
            sql.setParameter("LastUpdateById", authority_id);
            sql.setParameter("LastUpdateDate", client_time);
            result += sql.executeUpdate(connection);
        }
        //update t_treatment
        sql.clear();
        sql.append(" update T_Treatment set  \n");
        sql.append(" AttendingDoctorId = ?AttendingDoctorId,  \n");
        sql.append(" LastUpdateById = ?lastupdatebyid,  \n");
        sql.append(" LastUpdateDate = ?lastupdatedate  \n");
        sql.append(" where StrokeId = ?strokeid  \n");
        sql.setParameter("strokeid", stroke_id);
        sql.setParameter("AttendingDoctorId", jObject.getString("attending_id"));
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        result += sql.executeUpdate(connection);
        if (updflag) {
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public boolean treatment(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"treatment": {        													   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "tpa_flag": "1",                           								   */
 /*															 "tpa_flag_time": "20170502 13:20:20",							   */
 /*															 "tpa_flag_reason": "1",                 								   */
 /*															 "tpa_flag_by_id": "1"                    								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = Integer.parseInt(jObject.getString("stepid"));

        int result = 0;
        KnSQL sql = new KnSQL();

        java.util.Vector<java.util.Vector> oldvec = null;

        int getstroke = getValueStrokeId(connection, stroke_id, "T_Treatment");
        Trace.info("+++treatment  getstroke++++++++++++++++" + getstroke);
        if (getstroke > 0) { //found data
            sql.append("update T_Treatment set tPAFlag =?tPAFlag,tPAFlagReason =?tPAFlagReason, tPAFlagById = ?tPAFlagById,tPAFlagTime =?tPAFlagTime, \n");
            sql.append("LastUpdateById=?LastUpdateById,LastUpdateDate=?LastUpdateDate \n");
            sql.append("where StrokeId=?StrokeId \n");

            oldvec = getOldData(connection, "t_treatment", "strokeid", Integer.toString(stroke_id));

        } else {
            //insert t_treatment
            sql.append("insert into T_Treatment(strokeid,tpaflag,tPAFlagReason,tPAFlagById,tPAFlagTime,LastUpdateById,LastUpdateDate)  \n");
            sql.append(" values(?StrokeId,?tPAFlag,?tPAFlagReason,?tPAFlagById,?tPAFlagTime,?LastUpdateById,?LastUpdateDate) ");
        }

        sql.setParameter("tPAFlag", jObject.getString("tpa_flag"));
        sql.setParameter("tPAFlagReason", jObject.getString("tpa_flag_reason"));
        sql.setParameter("tPAFlagById", jObject.getString("tpa_flag_by_id"));
        sql.setParameter("tPAFlagTime", convertStrToTimestamp(jObject.getString("tpa_flag_time")));
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        sql.setParameter("StrokeId", stroke_id);

        result = sql.executeUpdate(connection);
        if (getstroke > 0) {
            compareData(connection, oldvec, "t_treatment", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        } else {
            insertT_TransactionLog(connection, "t_treatment", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        }

        if (updflag) {
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public boolean cta_criteria(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"cta_criteria": {        													   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "cta_flag": "1",                           								   */
 /*															 "cta_flag_reason": "1",                 								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = Integer.parseInt(jObject.getString("stepid"));

        int result = 0;
        KnSQL sql = new KnSQL();
        java.util.Vector<java.util.Vector> oldvec = getOldData(connection, "t_treatment", "strokeid", Integer.toString(stroke_id));
        sql.append("update T_Treatment set CTAFlag =?CTAFlag,CTAFlagReason =?CTAFlagReason, CTAFlagById = ?CTAFlagById,CTAFlagTime =?CTAFlagTime, \n");
        sql.append("LastUpdateById=?LastUpdateById,LastUpdateDate=?LastUpdateDate \n");
        sql.append("where StrokeId=?StrokeId \n");

        sql.setParameter("CTAFlag", jObject.getString("cta_flag"));
        sql.setParameter("CTAFlagReason", jObject.getString("cta_flag_reason"));
        sql.setParameter("CTAFlagById", authority_id);
        sql.setParameter("CTAFlagTime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        sql.setParameter("StrokeId", stroke_id);

        result = sql.executeUpdate(connection);

        compareData(connection, oldvec, "t_treatment", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);

        if (updflag) {
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public boolean angiogram_anesthetic_timestamp(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"angiogram_anesthetic_timestamp": {     						   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "anesthetic_time": "20170502 13:20:20"						   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        java.sql.Timestamp anesthetic_time = convertStrToTimestamp(jObject.getString("anesthetic_time"));
        int stepid = Integer.parseInt(jObject.getString("stepid"));

        int result = 0;

        KnSQL sql = new KnSQL();
        sql.append("select strokeid from T_Thrombectomy \n");
        sql.append("where StrokeId =?StrokeId");
        sql.setParameter("StrokeId", stroke_id);
        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) {
            updateT_Trombectomy(connection, stroke_id, authority_id, client_time, anesthetic_time);
        } else {
            insertT_Trombectomy(connection, stroke_id, authority_id, client_time, anesthetic_time);
        }
        insertBeaconTemp(connection, stroke_id, authority_id, "5", jObject.getString("anesthetic_time"), -1);
        if (updflag) {
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public void updateT_Trombectomy(java.sql.Connection connection, int stroke_id, String authority_id, java.sql.Timestamp client_time, java.sql.Timestamp anesthetictime) throws Exception {
        Trace.debug("====================updateT_Trombectomy=================");
        int rs = 0;
        java.util.Vector<java.util.Vector> oldvec = getOldData(connection, "t_thrombectomy", "strokeid", Integer.toString(stroke_id));
        KnSQL sql = new KnSQL();

        sql.append("update T_thrombectomy set AnestheticById =?anestheticbyid,AnestheticTime=?anesthetictime, \n");
        sql.append("LastUpdateById =?lastupdatebyid,LastUpdateDate=?lastupdatedate \n");
        sql.append("where StrokeId = ?strokeid \n");
        sql.setParameter("anesthetictime", anesthetictime);
        sql.setParameter("anestheticbyid", authority_id);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        sql.setParameter("strokeid", stroke_id);
        rs = sql.executeUpdate(connection);
        if (rs > 0) {
            compareData(connection, oldvec, "T_Thrombectomy", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        }
    }

    public void insertT_Trombectomy(java.sql.Connection connection, int stroke_id, String authority_id, java.sql.Timestamp client_time, java.sql.Timestamp anesthetictime) throws Exception {
        Trace.debug("====================insertT_Trombectomy=================");
        int rs = 0;
        KnSQL sql = new KnSQL();
        sql.append("insert into T_thrombectomy (StrokeId,AnestheticById,AnestheticTime,LastUpdateById,LastUpdateDate) \n");
        sql.append("values(?strokeid,?anestheticbyid,?anesthetictime,?lastupdatebyid,?lastupdatedate) \n");
        sql.setParameter("anesthetictime", anesthetictime);
        sql.setParameter("anestheticbyid", authority_id);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        sql.setParameter("strokeid", stroke_id);
        rs = sql.executeUpdate(connection);
        if (rs > 0) {
            insertT_TransactionLog(connection, "T_Thrombectomy", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        }

    }

    public boolean patient_info(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag, String modulename) throws Exception {
        /**
         * ***********************************module 1*************************************
         */
        /*															"patient_info": {        													   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": 19002345,            								   */
 /*															 "stepid": 1,                                								   */
 /*															 "passport_number": ""  ,                            					   */
 /*															 "citizen_id": 1234567890123,  							        	   */
 /*															 "hn": "",                               							        	   */
 /*															 "sap_id": 1234567890123 							        	       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        /**
         * ********************************module 2****************************************
         */
        /*															"patient_info2": {        												   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "age": "1",                                    					               */
 /*															 "name": "1",                 							        	           */
 /*															 "sex": "1",                           							        	   */
 /*															 "sap_id": 1234567890123 							        	       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = jObject.getInt("stepid");
        String citizenid = null;
        String hn = null;
        String passport_number = null;
        String prefixid = null;
        int s_age = 0;
        String age = null;
        int result = 0;
        String firstname = null;
        String lastname = null;
        String gender = null;
        //	java.sql.Date birthday = null;
        String birthday = null;

        KnSQL sql = new KnSQL();

        ThePatient pt = new ThePatient();
        Patient p = null;

        if (modulename != null && modulename.equals("1")) { // patient_info
            citizenid = jObject.getString("citizen_id");
            hn = jObject.getString("hn");
            passport_number = jObject.getString("passport_number");
            if (citizenid != null && !citizenid.equals("")) {
                p = pt.getPatientByID(citizenid);
                Trace.info("connecting siriraj server by citizenid : " + p);
                if (p.isSuccess()) {
                    prefixid = p.getPrefixName();
                    hn = p.getHN();
                    passport_number = p.getPassportNo();
                    firstname = butil.MS874ToUnicode(p.getFirstName());
                    lastname = butil.MS874ToUnicode(p.getLastName());
                    birthday = p.getDateOfBirth();
                    gender = butil.MS874ToUnicode(p.getGender());

                    java.util.Date bdate = p.getBirthDay();
                    java.sql.Date bddate = new java.sql.Date(bdate.getTime());
                    int getYearBdate = butil.getYearFromYMD(bddate.toString());
                    String currentdate = butil.formatDateTime(butil.getCurrentDate(), "yyyyMMdd");
                    int getYearCurdate = butil.getYearFromYMD(currentdate);
                    s_age = getYearCurdate - getYearBdate;

                }
            } else if (hn != null && !hn.equals("")) {
                p = pt.getPatientByHN(hn);
                Trace.info("connecting siriraj server by hn : " + p);
                if (p.isSuccess()) {
                    prefixid = p.getPrefixName();
                    hn = p.getHN();
                    citizenid = p.getCitizenId();
                    passport_number = p.getPassportNo();
                    firstname = butil.MS874ToUnicode(p.getFirstName());
                    lastname = butil.MS874ToUnicode(p.getLastName());
                    birthday = p.getDateOfBirth();
                    gender = butil.MS874ToUnicode(p.getGender());

                    java.util.Date bdate = p.getBirthDay();
                    java.sql.Date bddate = new java.sql.Date(bdate.getTime());
                    int getYearBdate = butil.getYearFromYMD(bddate.toString());
                    String currentdate = butil.formatDateTime(butil.getCurrentDate(), "yyyyMMdd");
                    int getYearCurdate = butil.getYearFromYMD(currentdate);
                    s_age = getYearCurdate - getYearBdate;

                }

            }/*else if(passport_number != null &&  passport_number != ""){

						}*/

            sql.clear();
            sql.append("update T_Stroke set CurrentStepId =?CurrentStepId,HN =?hn, \n");
            sql.append(" CitizenId = ?CitizenId,PassportNumber =?PassportNumber, \n");
            sql.append(" PrefixName =?PrefixId,FirstName=?FirstName, \n");
            sql.append(" LastName=?LastName,Gender=?Gender, \n");
            sql.append(" DateOfBirth = ?DateOfBirth, \n");
            if (s_age > 0) {
                sql.append(" age = ?age , \n");
                sql.setParameter("age", s_age);
            }
            sql.append(" HNInputById=?HNInputById,HNInputTime=?HNInputTime,LastUpdateById=?LastUpdateById, \n");
            sql.append(" LastUpdateDate=?LastUpdateDate where StrokeId=?StrokeId \n");

        } else if (modulename != null && modulename.equals("2")) {
            String name = jObject.getString("name");
            String[] parts = name.split(" ");
            firstname = parts[0];
            lastname = parts[1];
            age = jObject.getString("age");
            gender = jObject.getString("sex");

            sql.clear();
            sql.append("update T_Stroke set CurrentStepId =?CurrentStepId \n");
            sql.append(" ,FirstName=?FirstName, \n");
            sql.append(" LastName=?LastName,Gender=?Gender, \n");
            if (age != null) {
                sql.append(" Age=?Age, \n");
                sql.setParameter("Age", age);
            }
            sql.append(" HNInputById=?HNInputById,HNInputTime=?HNInputTime,LastUpdateById=?LastUpdateById, \n");
            sql.append(" LastUpdateDate=?LastUpdateDate where StrokeId=?StrokeId \n");

        }

        /*
				sql.append("select * from T_Patient_temp \n");
				if(modulename != null && modulename.equals("1")) { // patient_info
					citizenid = jObject.getString("citizen_id");
					hn = jObject.getString("hn");
					passport_number = jObject.getString("passport_number");
					
					if(citizenid != null && !citizenid.equals("")){
						sql.append("where CitizenId = ?citizen_id ");
						sql.setParameter("citizen_id",citizenid);
					}else if(hn != null &&  hn != ""){
						sql.append("where HN = ?HN ");
						sql.setParameter("HN",hn);
					}else if(passport_number != null &&  passport_number != ""){
						sql.append("where PassportNumber = ?PassportNumber ");
						sql.setParameter("PassportNumber",passport_number);
					}
				} else if(modulename != null && modulename.equals("2")) {
						sql.append("where Firstname = ?firstname ");
						sql.append("and LastName = ?lastname ");
						sql.append("and Age = ?age ");
						sql.append("and Gender = ?gender");

						String name = jObject.getString("name");
						String[] parts = name.split(" ");
						firstname=parts[0];
						lastname = parts[1];
						age = Integer.parseInt(jObject.getString("age"));
						gender = jObject.getString("sex");

						sql.setParameter("firstname",firstname);		
						sql.setParameter("lastname",lastname);
						sql.setParameter("age",age);
						sql.setParameter("gender",gender);
				}

				java.sql.ResultSet rs = sql.executeQuery(connection);
				if(rs != null && rs.next()){
					hn = rs.getString("HN");
					citizenid = rs.getString("CitizenId");
					passport_number = rs.getString("PassportNumber");
					prefixid = rs.getString("PrefixId");
					firstname = rs.getString("FirstName");
					lastname = rs.getString("LastName");
					gender = rs.getString("Gender");
					birthday = rs.getDate("DateOfBirth");
					age = rs.getInt("Age");
				}
         */
        java.util.Vector<java.util.Vector> oldvec = getOldData(connection, "t_stroke", "strokeid", Integer.toString(stroke_id));

        sql.setParameter("CurrentStepId", stepid);
        sql.setParameter("hn", hn);
        sql.setParameter("CitizenId", citizenid);
        sql.setParameter("PassportNumber", passport_number);
        sql.setParameter("PrefixId", prefixid);
        sql.setParameter("FirstName", firstname);
        sql.setParameter("LastName", lastname);
        sql.setParameter("Gender", gender);
        sql.setParameter("DateOfBirth", birthday);
        sql.setParameter("HNInputById", authority_id);
        sql.setParameter("HNInputTime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        sql.setParameter("StrokeId", stroke_id);

        result = sql.executeUpdate(connection);

        if (result > 0) {
            compareData(connection, oldvec, "t_stroke", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        }
        return true;
    }

    public boolean patient_pic(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"patient_pic": {        													   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "pic_base64": "1",                                   					   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = Integer.parseInt(jObject.getString("stepid"));
        String picBase64 = jObject.getString("pic_base64");
        Trace.info("====================pic==============" + picBase64);
        String imgpath = (String) GlobalVariable.getVariable("IMAGE_PATH");
        Trace.info("====================imgpath==============" + imgpath);

        try {
            //java.io.File file = new java.io.File(imgpath+picBase64);
            java.io.File file = new java.io.File(imgpath + Integer.toString(stroke_id) + ".jpg");
            if (file.exists() && !file.isDirectory()) {
                Trace.info("==================file  exist==============");
                java.io.PrintWriter writer = new java.io.PrintWriter(file);
                writer.print("");
                writer.close();
            } else {
                Trace.info("==================file not exist==============");
                file = new java.io.File(imgpath + Integer.toString(stroke_id) + ".jpg");
            }
            // Converting a Base64 String into Image byte array
            byte[] imageByteArray = Base64.decodeBase64(picBase64);
            Trace.debug("imageByteArray===========" + imageByteArray);

            // Write a image byte array into file system
            Trace.debug("write image============  " + imgpath + Integer.toString(stroke_id) + ".jpg");
            java.io.FileOutputStream imageOutFile = new java.io.FileOutputStream(file);
            imageOutFile.write(imageByteArray);

            imageOutFile.close();

            Trace.debug("==============Image Successfully Manipulated!");
        } catch (java.io.FileNotFoundException e) {
            Trace.debug("================Image not found" + e);
            throw new Exception("SERR:7");
        } catch (java.io.IOException ioe) {
            Trace.debug("==================Exception while reading the Image " + ioe);
            throw new Exception("SERR:8");
        }

        return true;
    }

    public boolean angiogram_groin_puncture(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag, String modulename) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"angiogram_groin_puncture": {     						               */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "groin_puncture_timestamp": "20170502 13:20:20"	       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        /**
         * *******************************************************************************
         */
        /*													"positive_angiogram_timestamp": {     						               */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*														 "positive_angiogram_timestamp": "20170502 13:20:20"	       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        /**
         * *******************************************************************************
         */
        /*													"angiogram_recanalization_timestamp": {     						       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*														     "recanalization_timestamp": "20170502 13:20:20"	           */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        /**
         * *******************************************************************************
         */
        /*													   "angiogram_ia_timestamp": {                  						       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*														     "ia_time": "20170502 13:20:20"	                                   */
 /*															 "iaid": "1",                                  								   */
 /*															 "iaamt": "1",                              								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        /**
         * *******************************************************************************
         */
        /*													"angiogram_end_procedure_timestamp": {     						       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        java.sql.Timestamp _timestamp = null;
        int stepid = Integer.parseInt(jObject.getString("stepid"));

        int result = 0;

        KnSQL sql = new KnSQL();
        java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_thrombectomy", "strokeid", Integer.toString(stroke_id));
        sql.append(" update T_Thrombectomy  set  \n");
        if (modulename != null && modulename.equals("1")) {
            sql.append(" PunctureTime = ?l_timestamp,  \n");
            _timestamp = convertStrToTimestamp(jObject.getString("groin_puncture_timestamp"));
        } else if (modulename != null && modulename.equals("2")) {
            sql.append(" PosAngiogramTime = ?l_timestamp,  \n");
            _timestamp = convertStrToTimestamp(jObject.getString("positive_angiogram_timestamp"));
        } else if (modulename != null && modulename.equals("3")) {
            sql.append(" recanaltime = ?l_timestamp,  \n");
            _timestamp = convertStrToTimestamp(jObject.getString("recanalization_timestamp"));
        } else if (modulename != null && modulename.equals("4")) {
            sql.append(" IAId = ?iaid,  \n");
            sql.append(" IAAmt = ?iaamt,  \n");
            sql.append(" IATime = ?l_timestamp,  \n");
            _timestamp = convertStrToTimestamp(jObject.getString("ia_time"));
            sql.setParameter("iaid", jObject.getString("iaid"));
            sql.setParameter("iaamt", jObject.getString("iaamt"));
        } else if (modulename != null && modulename.equals("5")) {
            sql.append(" EndProcedureTime = ?l_timestamp,  \n");
            _timestamp = client_time;
        }
        sql.append(" ThrombectomyById = ?thrombectomybyid,  \n");
        sql.append(" ThrombectomyTime = ?thrombectomytime, \n");
        sql.append(" LastUpdateById = ?lastupdatebyid, \n");
        sql.append(" LastUpdateDate = ?lastupdatedate \n");
        sql.append(" where StrokeId = ?strokeid  \n");
        sql.setParameter("strokeid", stroke_id);
        sql.setParameter("l_timestamp", _timestamp);
        sql.setParameter("thrombectomybyid", authority_id);
        sql.setParameter("thrombectomytime", client_time);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        result = sql.executeUpdate(connection);
        compareData(connection, vec, "t_thrombectomy", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);

        if (updflag) {
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public boolean criteria_for_thrombectomy(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*													"criteria_for_thrombectomy": {     						                       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "cta_collateral": "1"	                                                       */
 /*															 "criteria": [                                  								   */
 /*															 {"CriteriaId": "1","Result":"0"},          							   */
 /*															 {"CriteriaId": "2","Result":"1"},          							   */
 /*																]							        									       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int cta_collateral = Integer.parseInt(jObject.getString("cta_collateral"));
        int stepid = Integer.parseInt(jObject.getString("stepid"));

        JSONArray paramdata = jObject.getJSONArray("criteria");
        int n = paramdata.length();
        if (n <= 0) {
            throw new Exception("criteria length is 0");
        } else {
            int getstroke = getValueStrokeId(connection, stroke_id, "t_treatment");
            Trace.info("+++getstroke++++++++++++++++" + getstroke);
            if (getstroke > 0) {
                KnSQL sql = new KnSQL();
                sql.append("update T_Treatment \n");
                sql.append("set CTACollateral = ?CTACollateral, \n");
                sql.append("     CTAById = ?CTAById, \n");
                sql.append("     CTAResultTime = ?CTAResultTime, \n");
                sql.append("     LastUpdateById = ?LastUpdateById, \n");
                sql.append("     LastUpdateDate = ?LastUpdateDate  \n");
                sql.append(" where strokeid = ?StrokeId \n");
                sql.setParameter("StrokeId", stroke_id);
                sql.setParameter("CTACollateral", cta_collateral);
                sql.setParameter("CTAById", authority_id);
                sql.setParameter("CTAResultTime", client_time);
                sql.setParameter("LastUpdateById", authority_id);
                sql.setParameter("LastUpdateDate", client_time);
                sql.executeUpdate(connection);
            } else {
                insertT_Treament(connection, stroke_id, authority_id, client_time, cta_collateral);
            }
            for (int i = 0; i < n; i++) {
                JSONObject ansArray = paramdata.getJSONObject(i);
                int id = ansArray.getInt("CriteriaId");
                int result = ansArray.getInt("Result");
                insertT_CriteriaResult(id, result, connection, stroke_id, authority_id, client_time);
                insertT_CriteriaResultLog(id, result, connection, stroke_id, authority_id, client_time);
            }
        }

        if (updflag) {
            updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public void insertT_Treament(java.sql.Connection connection, int stroke_id, String authority_id, java.sql.Timestamp client_time, int CTACollateral) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("insert into T_Treatment \n");
        sql.append("(StrokeId,CTACollateral,CTAById,CTAResultTime,LastUpdateById,LastUpdateDate) \n");
        sql.append(" values(?StrokeId,?CTACollateral,?CTAById,?CTAResultTime,?LastUpdateById,?LastUpdateDate) ");
        sql.setParameter("StrokeId", stroke_id);
        sql.setParameter("CTACollateral", CTACollateral);
        sql.setParameter("CTAById", authority_id);
        sql.setParameter("CTAResultTime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        sql.executeUpdate(connection);
        insertT_TransactionLog(connection, "T_Treatment", "StrokeId", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
    }

    public void insertT_CriteriaResult(int id, int result, java.sql.Connection connection, int stroke_id, String authority_id, java.sql.Timestamp client_time) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("insert into T_CriteriaResult \n");
        sql.append(" (CriteriaId,StrokeId,Result,ResultById,ResultTime,LastUpdateById,LastUpdateDate) \n");
        sql.append(" values(?CriteriaId,?StrokeId,?Result,?ResultById,?ResultTime,?LastUpdateById,?LastUpdateDate) \n");
        sql.setParameter("CriteriaId", id);
        sql.setParameter("StrokeId", stroke_id);
        sql.setParameter("Result", result);
        sql.setParameter("ResultById", authority_id);
        sql.setParameter("ResultTime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        sql.executeUpdate(connection);
    }

    public void insertT_CriteriaResultLog(int id, int result, java.sql.Connection connection, int stroke_id, String authority_id, java.sql.Timestamp client_time) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("insert into T_CriteriaResultLog \n");
        sql.append(" (CriteriaId,StrokeId,Result,ResultById,ResultTime,LastUpdateById,LastUpdateDate) \n");
        sql.append(" values(?CriteriaId,?StrokeId,?Result,?ResultById,?ResultTime,?LastUpdateById,?LastUpdateDate) \n");
        sql.setParameter("CriteriaId", id);
        sql.setParameter("StrokeId", stroke_id);
        sql.setParameter("Result", result);
        sql.setParameter("ResultById", authority_id);
        sql.setParameter("ResultTime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        sql.executeUpdate(connection);
    }

    public boolean thrombectomy_decision(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag, String apiflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"thrombectomy_decision": {        					    			   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "sap_id": "19002345",          								           */
 /*															 "neuro_reason": "1"                 								       */
 /*															 "intervention_flag": "1"                 								   */
 /*															 "intervention_id": "1"                 								       */
 /*															 "intervention_reason": "1"                 							   */
 /*															 "neuro_flag": "1"                 								           */
 /*															 "neuro_id": "1"                 								               */
 /*															 "thrombec_flag": "1"                 								       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        /**
         * *******************************************************************************
         */
        /*															"ct_result": {        					    			                       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",          								           */
 /*															 "authority_name": "Miss A B",        								   */
 /*															 "stepid": "1",                              								   */
 /*															 "ct_result_time": "20170919 22:01:00",					       */
 /*															 "ct_aspect": "1"                  								           */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = Integer.parseInt(jObject.getString("stepid"));
        int result = 0;

        java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_treatment", "strokeid", Integer.toString(stroke_id));
        KnSQL sql = new KnSQL();
        sql.append(" update T_Treatment  set  \n");
        if (apiflag != null && apiflag.equals("1")) {//thrombectomy_decision
            sql.append(" InterventionFlag = ?interventionflag,  \n");
            sql.append(" InterventionReason = ?intervention_reason,  \n");
            sql.append(" InterventionId = ?interventionid,  \n");
            sql.append(" NeuroFlag = ?neuro_flag,  \n");
            sql.append(" NeuroReason = ?neuro_reason, \n");
            sql.append(" NeuroId = ?neuro_id, \n");
            sql.append(" ThrombecFlag = ?thrombec_flag, \n");
            sql.append(" ThrombecById = ?thrombecbyid, \n");
            sql.append(" ThrombecTime = ?thrombectime, \n");
            sql.setParameter("interventionflag", jObject.getString("intervention_flag"));
            sql.setParameter("intervention_reason", jObject.getString("intervention_reason"));
            sql.setParameter("interventionid", jObject.getString("intervention_id"));
            sql.setParameter("neuro_flag", jObject.getString("neuro_flag"));
            sql.setParameter("neuro_reason", jObject.getString("neuro_reason"));
            sql.setParameter("neuro_id", jObject.getString("neuro_id"));
            sql.setParameter("thrombec_flag", jObject.getString("thrombec_flag"));
            sql.setParameter("thrombecbyid", authority_id);
            sql.setParameter("thrombectime", client_time);
        } else if (apiflag != null && apiflag.equals("2")) {//ct_result
            sql.append("  ctaspect = ?ctaspect, \n");
            sql.append("  ctbyid = ?ctbyid, \n");
            sql.append("  ctresulttime = ?ctresulttime, \n");
            sql.setParameter("ctaspect", jObject.getString("ct_aspect"));
            sql.setParameter("ctbyid", authority_id);
            sql.setParameter("ctresulttime", jObject.getString("ct_result_time"));

        }
        sql.append(" LastUpdateById = ?lastupdatebyid, \n");
        sql.append(" LastUpdateDate = ?lastupdatedate \n");
        sql.append(" where StrokeId = ?strokeid  \n");
        sql.setParameter("strokeid", stroke_id);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        result = sql.executeUpdate(connection);

        compareData(connection, vec, "t_treatment", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);

        if (apiflag != null && apiflag.equals("1")) {//thrombectomy_decision
            insertBeaconTemp(connection, stroke_id, authority_id, "4", jObject.getString("client_time"), 1);
        }
        if (updflag) {
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public boolean patient_info_sheet(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"patient_info_sheet": {        					    			       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "patient_infosheet_flagTime": "20170502 13:20:20",	       */
 /*							                                 "consent_signed_flagtime": "20170502 13:20:20",	    	   */
 /*															 "consent_signed_flag": "1"                 							   */
 /*															 "patient_infosheet_flag": "1"                 				           */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = Integer.parseInt(jObject.getString("stepid"));
        int result = 0;

        java.util.Vector<java.util.Vector> vec = null;
        KnSQL sql = new KnSQL();
        int getstroke = getValueStrokeId(connection, stroke_id, "T_Treatment");
        Trace.info("+++getstroke++++++++++++++++" + getstroke);
        if (getstroke > 0) {
            sql.append(" update T_Treatment  set  \n");
            sql.append(" PatientInfoSheetFlagTime = ?PatientInfoSheetFlagTime,  \n");
            sql.append(" ConsentSignedFlagById = ?ConsentSignedFlagById,  \n");
            sql.append(" ConsentSignedFlagTime = ?ConsentSignedFlagTime,  \n");
            sql.append(" ConsentSignedFlag = ?ConsentSignedFlag,  \n");
            sql.append(" PatientInfoSheetFlag = ?PatientInfoSheetFlag, \n");
            sql.append(" PatientInfoSheetFlagById = ?PatientInfoSheetFlagById, \n");
            sql.append(" LastUpdateById = ?lastupdatebyid, \n");
            sql.append(" LastUpdateDate = ?lastupdatedate \n");
            sql.append(" where StrokeId = ?strokeid  \n");
            vec = getOldData(connection, "t_treatment", "strokeid", Integer.toString(stroke_id));
        } else {
            sql.append("insert into t_treatment(strokeid,PatientInfoSheetFlagTime,ConsentSignedFlagById,\n");
            sql.append(" ConsentSignedFlagTime,ConsentSignedFlag,PatientInfoSheetFlag,PatientInfoSheetFlagById,\n");
            sql.append(" LastUpdateById,LastUpdateDate) \n");
            sql.append(" values \n");
            sql.append(" (?strokeid,?PatientInfoSheetFlagTime,?ConsentSignedFlagById,\n");
            sql.append(" ?ConsentSignedFlagTime,?ConsentSignedFlag,?PatientInfoSheetFlag,?PatientInfoSheetFlagById,\n");
            sql.append(" ?LastUpdateById,?LastUpdateDate) \n");
        }
        sql.setParameter("strokeid", stroke_id);
        sql.setParameter("PatientInfoSheetFlagTime", jObject.getString("patient_infosheet_flagTime"));
        sql.setParameter("ConsentSignedFlagById", authority_id);
        sql.setParameter("ConsentSignedFlagTime", jObject.getString("consent_signed_flagtime"));
        sql.setParameter("ConsentSignedFlag", jObject.getString("consent_signed_flag"));
        sql.setParameter("PatientInfoSheetFlag", jObject.getString("patient_infosheet_flag"));
        sql.setParameter("PatientInfoSheetFlagById", authority_id);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        result = sql.executeUpdate(connection);

        if (getstroke > 0) {
            compareData(connection, vec, "t_treatment", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        } else {
            insertT_TransactionLog(connection, "t_treatment", "strokeid", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);
        }
        if (updflag) {
            result += updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public boolean nihss(java.sql.Connection connection, int stroke_id, String jsonStr, boolean updflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*												             "nihss": {     						                                           */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "action": "insert",                           								   */
 /*															 "score": 20,                                								   */
 /*															 "total_score": 40,                          								   */
 /*															 "nihss_scores": [                            								   */
 /*									{"not_answer": "1","Score":"0","answerId":"1","QuestionId":"2"},     			   */
 /*									{"not_answer": "2","Score":"1","answerId":"2","QuestionId":"3"},     			   */
 /*																}							        									       */
 /*																]							        									       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + jsonStr + "]");
        JSONObject jObject = new JSONObject(jsonStr);
        java.sql.Timestamp client_time = convertStrToTimestamp(jObject.getString("client_time"));
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = Integer.parseInt(jObject.getString("stepid"));

        JSONArray paramdata = jObject.getJSONArray("nihss_scores");
        int n = paramdata.length();
        if (n <= 0) {
            throw new Exception("criteria length is 0");
        } else {
            int score = jObject.getInt("score");
            int totalScore = jObject.getInt("total_score");
            String action = jObject.getString("action");
            String scores = null;
            int NIHSSId = insertT_NIHSS(score, totalScore, connection, stroke_id, authority_id, client_time);
            for (int i = 0; i < n; i++) {
                JSONObject ansArray = paramdata.getJSONObject(i);
                int scoreFlag = ansArray.getInt("not_answer");
                if (scoreFlag == 0) {
                    scores = ansArray.getString("Score");
                }

                int NIHSSScoreId = insertT_NIHSSScore(NIHSSId, connection, ansArray.getInt("answerId"), ansArray.getInt("QuestionId"), scores, authority_id, client_time);
                insertT_NIHSSScoreLog(NIHSSScoreId, connection, authority_id, client_time, ansArray.getInt("answerId"), ansArray.getInt("QuestionId"), scores, action);
            }
            if (updflag) {
                updateT_Stroke(connection, stroke_id, stepid, Integer.parseInt(authority_id), client_time);
            }

        }

        return true;
    }

    public int insertT_NIHSSScore(int NIHSSId, java.sql.Connection connection, int answerid, int questionid, String scores, String authority_id, java.sql.Timestamp client_time) throws Exception {
        KnSQL sql = new KnSQL();

        sql.append(" insert into T_NIHSSScore(NIHSSId,AnswerId,QuestionId,Score,NIHSSScoreById,NIHSSScoreTime,LastUpdateById,LastUpdateDate) \n");
        sql.append(" values(?NIHSSId,?AnswerId,?QuestionId,?Score,?NIHSSScoreById,?NIHSSScoreTime,?LastUpdateById,?LastUpdateDate) \n");
        sql.setParameter("NIHSSId", NIHSSId);
        sql.setParameter("AnswerId", answerid);
        sql.setParameter("QuestionId", questionid);
        sql.setParameter("Score", scores);
        sql.setParameter("NIHSSScoreById", authority_id);
        sql.setParameter("NIHSSScoreTime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);

        //sql.executeUpdate(connection);
        sql.executeUpdate(connection, java.sql.Statement.RETURN_GENERATED_KEYS);

        /*	sql.clear();
				sql.append("select max(NIHSSScoreId) as nid from T_NIHSSScore \n");
				sql.append(" where NIHSSId = ?NIHSSId \n");
				sql.setParameter("NIHSSId",NIHSSId);
				java.sql.ResultSet rs = sql.executeQuery(connection);
				int result = 0;
				if(rs.next()){
					result = rs.getInt("nid");
				}
				return result;
         */
        return (int) sql.getGeneratedKey();
    }

    public int insertT_NIHSS(int score, int totalScore, java.sql.Connection connection, int stroke_id, String authority_id, java.sql.Timestamp client_time) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("insert into T_NIHSS(StrokeId,Score,TotalScore,NIHSSById,NIHSSTime,LastUpdateById,LastUpdateDate) \n");
        sql.append("values(?StrokeId,?Score,?TotalScore,?NIHSSById,?NIHSSTime,?LastUpdateById,?LastUpdateDate) \n");
        sql.setParameter("StrokeId", stroke_id);
        sql.setParameter("Score", score);
        sql.setParameter("TotalScore", totalScore);
        sql.setParameter("NIHSSById", authority_id);
        sql.setParameter("NIHSSTime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);

        //sql.executeUpdate(connection);
        sql.executeUpdate(connection, java.sql.Statement.RETURN_GENERATED_KEYS);
        insertT_TransactionLog(connection, "T_NIHSS", "StrokeId", Integer.toString(stroke_id), Integer.toString(stroke_id), authority_id, client_time);

        /*sql.clear();
				sql.append("select max(NIHSSId) as nid from T_NIHSS \n");
				sql.append(" where StrokeId = ?strokeid \n");
				sql.setParameter("strokeid",stroke_id);
				java.sql.ResultSet rs = sql.executeQuery(connection);
				int result = 0;
				if(rs.next()){
					result = rs.getInt("nid");
				}
				return result;
         */
        return (int) sql.getGeneratedKey();
    }

    public void insertT_NIHSSScoreLog(int NIHSSScoreId, java.sql.Connection connection, String authority_id, java.sql.Timestamp client_time,
            int answerid, int questionid, String scores, String action) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append(" insert into T_NIHSSScoreLog(NIHSSScoreId,AnswerId,QuestionId,Score,NIHSSScoreById,NIHSSScoreTime,LastUpdateById,LastUpdateDate,Action) \n");
        sql.append(" values(?NIHSSScoreId,?AnswerId,?QuestionId,?Score,?NIHSSScoreById,?NIHSSScoreTime,?LastUpdateById,?LastUpdateDate,?Action) \n");
        sql.setParameter("NIHSSScoreId", NIHSSScoreId);
        sql.setParameter("AnswerId", answerid);
        sql.setParameter("QuestionId", questionid);
        sql.setParameter("Score", scores);
        sql.setParameter("NIHSSScoreById", authority_id);
        sql.setParameter("NIHSSScoreTime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        sql.setParameter("Action", action);
        sql.executeUpdate(connection);
    }

    public boolean glucose(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"glucose": {        					    		                	       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "poct_glucosetime": "20170502 13:20:20",	                   */
 /*															 "poct_glucose": "25"                      							       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }
        try {
            Trace.debug("========== Get JsonString ========== ");
            JSONObject jObject = new JSONObject(data);
            String client_time0 = jObject.getString("client_time");
            java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
            String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
            int stepid = jObject.getInt("stepid");
            String POCTglucose = jObject.getString("poct_glucose");
            String POCTglucoseTime = jObject.getString("poct_glucosetime");

            getstroke = getValueStrokeId(connection, StrokeId, "T_Investigation");
            Trace.info("+++getstroke++++++++++++++++" + getstroke);
            if (getstroke > 0) {
                java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_investigation", "strokeid", Integer.toString(StrokeId));
                updateT_Investigation(connection, StrokeId, POCTglucose, POCTglucoseTime, Integer.parseInt(authority_id), client_time, 1);
                compareData(connection, vec, "t_investigation", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            } else {
                insertT_Investigation(connection, StrokeId, POCTglucose, POCTglucoseTime, Integer.parseInt(authority_id), client_time, 1);
                insertT_TransactionLog(connection, "t_investigation", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            }
            if (flag) {
                updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);
            }
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    public void insertT_Investigation(java.sql.Connection connection, int StrokeId, String _value, String _time,
            int AuthorityId, java.sql.Timestamp ClientTime, int checkflag) throws Exception {
        int result = 0;
        String str = "";
        String param = "";
        if (checkflag == 1) {
            str = "POCTglucose,POCTglucoseTime,POCTglucoseById,";
            param = "?lvalue,?ltime,?authorityid,";
        } else if (checkflag == 2) {
            str = "BodyWeight,WeightTime,WeightById,";
            param = "?lvalue,?ltime,?authorityid,";

        } else if (checkflag == 3) {
            str = "BloodDrawnTime,BloodDrawnById,";
            param = "?ltime,?authorityid,";

        }
        KnSQL sql = new KnSQL();
        sql.append("insert into T_Investigation(StrokeId," + str + "LastUpdateById,LastUpdateDate) \n");
        sql.append("values(?strokeid," + param + "?lastupdatebyid,?lastupdatedate) \n");
        sql.setParameter("strokeid", StrokeId);
        sql.setParameter("lvalue", _value);
        sql.setParameter("ltime", _time);
        sql.setParameter("authorityid", AuthorityId);
        sql.setParameter("lastupdatebyid", AuthorityId);
        sql.setParameter("lastupdatedate", ClientTime);
        result = sql.executeUpdate(connection);
    }

    public void updateT_Investigation(java.sql.Connection connection, int StrokeId, String _value, String _time,
            int AuthorityId, java.sql.Timestamp ClientTime, int checkflag) throws Exception {
        Trace.info("++++++++++++++++++++++ updateT_Investigation ++++++++++++++++++++++++++++");
        int result = 0;
        KnSQL sql = new KnSQL();
        sql.append(" update T_Investigation set  \n");
        if (checkflag == 1) {
            sql.append(" POCTglucose = ?lvalue,  \n");
            sql.append(" POCTglucoseTime = ?time,  \n");
            sql.append(" POCTglucoseById = ?authorityid,  \n");
        }
        if (checkflag == 2) {
            sql.append(" BodyWeight = ?lvalue,  \n");
            sql.append(" WeightTime = ?time,  \n");
            sql.append(" WeightById = ?authorityid,  \n");
        }
        if (checkflag == 3) {
            sql.append(" BloodDrawnTime = ?time,  \n");
            sql.append(" BloodDrawnById = ?authorityid,  \n");
        }

        sql.append(" LastUpdateById = ?lastupdatebyid,  \n");
        sql.append(" LastUpdateDate = ?lastupdatedate  \n");
        sql.append(" where StrokeId = ?strokeid  \n");
        sql.setParameter("strokeid", StrokeId);
        sql.setParameter("lvalue", _value);
        sql.setParameter("time", _time);
        sql.setParameter("authorityid", AuthorityId);
        sql.setParameter("lastupdatebyid", AuthorityId);
        sql.setParameter("lastupdatedate", ClientTime);
        result = sql.executeUpdate(connection);
    }

    public int getValueStrokeId(java.sql.Connection connection, int StrokeId, String tabname) throws Exception {
        KnSQL sql = new KnSQL();
        int num = 0;
        sql.append("  select StrokeId from     \n");
        sql.append(tabname);
        sql.append("  where StrokeId = ?strokeid    \n");
        sql.setParameter("strokeid", StrokeId);
        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) {
            num = rs.getInt("StrokeId");
        }
        return num;
    }

    public boolean body_weight(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"body_weight": {            		    		                	       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "body_weight_time": "20170502 13:20:20",	                   */
 /*															 "body_weight": "25"                      							       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }
        try {
            Trace.debug("========== Get JsonString ========== ");
            JSONObject jObject = new JSONObject(data);
            String client_time0 = jObject.getString("client_time");
            java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
            String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
            int stepid = jObject.getInt("stepid");
            String body_weight = jObject.getString("body_weight");
            String body_weight_time = jObject.getString("body_weight_time");

            getstroke = getValueStrokeId(connection, StrokeId, "T_Investigation");
            Trace.info("+++getstroke++++++++++++++++" + getstroke);
            if (getstroke > 0) {
                java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_investigation", "strokeid", Integer.toString(StrokeId));
                updateT_Investigation(connection, StrokeId, body_weight, body_weight_time, Integer.parseInt(authority_id), client_time, 2);
                compareData(connection, vec, "t_investigation", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            } else {
                insertT_Investigation(connection, StrokeId, body_weight, body_weight_time, Integer.parseInt(authority_id), client_time, 2);
                insertT_TransactionLog(connection, "t_investigation", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            }
            /**
             * **** this part will delete in the future ****
             */
            //body_weight_time- 1

            /*	java.sql.Timestamp bw_time = convertStrToTimestamp(body_weight_time);

				java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd HH:mm:ss");
				if(body_weight_time.indexOf("-") >= 0) {
					dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				}				

				Date parsedDate = dateFormat.parse(body_weight_time);
				Calendar cal = Calendar.getInstance();
				 cal.setTime(parsedDate);
				 cal.add(Calendar.MINUTE, -1);
				 String newTime = dateFormat.format(cal.getTime());

				Trace.info("New Time ===="+newTime);
             */
            insertBeaconTemp(connection, StrokeId, authority_id, "2", body_weight_time, -1);

            /**
             * ***********************************
             */
            if (flag) {
                updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);
            }
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    public boolean cta_time(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"cta_time": {            		          		                	           */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",                 								   */
 /*															 "stepid": "1",                              								   */
 /*															 "ctatime": "20170502 13:20:20",	                                   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }
        try {
            Trace.debug("========== Get JsonString ========== ");
            JSONObject jObject = new JSONObject(data);
            String client_time0 = jObject.getString("client_time");
            java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
            String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
            int stepid = jObject.getInt("stepid");
            String ctatime = jObject.getString("ctatime");

            getstroke = getValueStrokeId(connection, StrokeId, "t_treatment");
            Trace.info("+++getstroke++++++++++++++++" + getstroke);
            if (getstroke > 0) {
                java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_treatment", "strokeid", Integer.toString(StrokeId));
                updateT_treatment_cta_time(connection, StrokeId, ctatime, Integer.parseInt(authority_id), client_time);
                compareData(connection, vec, "t_treatment", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            } else {
                insertT_treatment_cta_time(connection, StrokeId, ctatime, Integer.parseInt(authority_id), client_time);
                insertT_TransactionLog(connection, "t_treatment", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            }
            if (flag) {
                updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);
            }
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    public void insertT_treatment_cta_time(java.sql.Connection connection, int StrokeId, String ctatime, int AuthorityId, java.sql.Timestamp ClientTime) throws Exception {
        Trace.info("++++++++++++++++++++++ insertT_treatment ++++++++++++++++++++++++++++");
        int result = 0;
        KnSQL sql = new KnSQL();
        sql.append("insert into T_Treatment(StrokeId,CTATime,LastUpdateById,LastUpdateDate) \n");
        sql.append("values(?strokeid,?ctatime,?lastupdatebyid,?lastupdatedate,?ctatimebyid) \n");
        sql.setParameter("strokeid", StrokeId);
        sql.setParameter("ctatime", ctatime);
        sql.setParameter("lastupdatebyid", AuthorityId);
        sql.setParameter("ctatimebyid", AuthorityId);
        sql.setParameter("lastupdatedate", ClientTime);
        result = sql.executeUpdate(connection);
    }

    public void updateT_treatment_cta_time(java.sql.Connection connection, int StrokeId, String ctatime, int AuthorityId, java.sql.Timestamp ClientTime) throws Exception {
        Trace.info("++++++++++++++++++++++ updateT_treatment ++++++++++++++++++++++++++++");
        int result = 0;
        KnSQL sql = new KnSQL();

        sql.append(" update T_Treatment set  \n");
        sql.append(" CTATime = ?ctatime,  \n");
        sql.append(" ctatimebyid = ?ctatimebyid, \n");
        sql.append(" LastUpdateById = ?lastupdatebyid,  \n");
        sql.append(" LastUpdateDate = ?lastupdatedate  \n");
        sql.append(" where StrokeId = ?strokeid  \n");
        sql.setParameter("strokeid", StrokeId);
        sql.setParameter("ctatime", ctatime);
        sql.setParameter("lastupdatebyid", AuthorityId);
        sql.setParameter("lastupdatedate", ClientTime);
        sql.setParameter("ctatimebyid", AuthorityId);
        result = sql.executeUpdate(connection);
    }

    public boolean tpa_push_time(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"tpa_push_time": {            		    		                	       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "stepid": "1",                              								   */
 /*															 "rtpa_drip_byid": 12,       											   */
 /*															 "rtpa_drip_amt": 1200,       											   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }
        try {
            Trace.debug("========== Get JsonString ========== ");
            JSONObject jObject = new JSONObject(data);
            String client_time0 = jObject.getString("client_time");
            java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
            String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
            int stepid = jObject.getInt("stepid");
            String ref_id = jObject.getString("rtpa_drip_byid");
            String drip_amt = jObject.getString("rtpa_drip_amt");

            java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_tpa", "strokeid", Integer.toString(StrokeId));
            updateT_tpa(connection, StrokeId, ref_id, drip_amt, Integer.parseInt(authority_id), client_time);
            compareData(connection, vec, "t_tpa", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);

            if (flag) {
                updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);
            }
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    public void updateT_tpa(java.sql.Connection connection, int StrokeId, String rtpa_drip_byid, String rtpa_drip_amt,
            int AuthorityId, java.sql.Timestamp ClientTime) throws Exception {
        Trace.info("++++++++++++++++++++++ T_TPA ++++++++++++++++++++++++++++");
        int result = 0;
        KnSQL sql = new KnSQL();

        java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_tpa", "strokeid", Integer.toString(StrokeId));
        sql.append(" update T_tPA set  \n");
        sql.append(" rtPADripAmt = ?rtpadripamt,  \n");
        sql.append(" rtPADripById = ?rtpadripbyid,  \n");
        sql.append(" rtPADripTime = ?rtpadriptime,  \n");
        sql.append(" rtPAById = ?rtpabyid,  \n");
        sql.append(" rtPATime = ?rtpatime,  \n");
        sql.append(" lastupdatebyid = ?lastupdatebyid, \n");
        sql.append(" lastupdatedate = ?lastupdatedate \n");
        sql.append(" where StrokeId = ?strokeid  \n");
        sql.setParameter("strokeid", StrokeId);
        sql.setParameter("rtpadripamt", rtpa_drip_amt);
        sql.setParameter("rtpadripbyid", rtpa_drip_byid);
        sql.setParameter("rtpadriptime", ClientTime);
        sql.setParameter("rtpabyid", AuthorityId);
        sql.setParameter("rtpatime", ClientTime);
        sql.setParameter("lastupdatebyid", AuthorityId);
        sql.setParameter("lastupdatedate", ClientTime);
        result = sql.executeUpdate(connection);
        compareData(connection, vec, "t_tpa", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), Integer.toString(AuthorityId), ClientTime);
    }

    public boolean blood_drawn_time(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*													         "blood_drawn_time": {     	                					       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",          								           */
 /*															 "stepid": "1",                              								   */
 /*															 "blood_drawn_time": "20170502 13:20:20"					   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }
        try {
            Trace.debug("========== Get JsonString ========== ");
            JSONObject jObject = new JSONObject(data);
            String client_time0 = jObject.getString("client_time");
            java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
            String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
            int stepid = jObject.getInt("stepid");
            String blood_drawn_time = jObject.getString("blood_drawn_time");

            getstroke = getValueStrokeId(connection, StrokeId, "T_Investigation");
            Trace.info("+++check data T_Investigation : getstroke++++++++++++++++" + getstroke);
            if (getstroke > 0) { // found data
                java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_investigation", "strokeid", Integer.toString(StrokeId));
                updateT_Investigation(connection, StrokeId, authority_id, blood_drawn_time, Integer.parseInt(authority_id), client_time, 3);
                compareData(connection, vec, "t_investigation", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            } else { //not found data
                insertT_Investigation(connection, StrokeId, authority_id, blood_drawn_time, Integer.parseInt(authority_id), client_time, 3);
                insertT_TransactionLog(connection, "t_investigation", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            }
            if (flag) {
                updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);
            }
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    public java.sql.Timestamp convertStrToTimestamp(String timeStr) throws Exception {
        if (timeStr == null || timeStr.equals("")) {
            return null;
        }
        java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd HH:mm:ss");
        if (timeStr.indexOf("-") >= 0) {
            dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
        Date parsedDate = dateFormat.parse(timeStr);
        java.sql.Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
        return timestamp;
    }

    public static String executePost(String targetURL, String urlParameters) {
        HttpURLConnection connection = null;
        if (urlParameters != null && !urlParameters.equals("")) {
            targetURL = urlParameters + "?" + urlParameters;
        }
        try {
            //Create connection
            URL url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length",
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Get Response  
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public boolean treatment_summary(java.sql.Connection connection, int stroke_id, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*													         "treatment_summary ": {     	               					       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",          								           */
 /*															 "stepid": "1",                              								   */
 /*															 "finish_flag": "0"                                     					   */
 /*															 "ward_id": 15                                      					       */
 /*															 "authority_name": "Miss A B"                   					       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        Trace.info("=================jsonStr[" + data + "]");

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }

        JSONObject jObject = new JSONObject(data);
        String client_time0 = jObject.getString("client_time");
        java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), stroke_id, client_time);
        int stepid = jObject.getInt("stepid");
        String finish_flag = jObject.getString("finish_flag");
        String ward_id = jObject.getString("ward_id");
        String authority_name = jObject.getString("authority_name");
        int result = 0;

        //	java.util.Vector<java.util.Vector> vec = getOldData(connection,"t_stroke","strokeid",Integer.toString(stroke_id));
        //update t_stroke
        KnSQL sql = new KnSQL();
        sql.append("update t_stroke set currentstepid=?currentstepid,\n");
        if (finish_flag != null && !finish_flag.equals("")) {
            sql.append(" finishedflag = ?finishflag , \n");
            sql.setParameter("finishflag", finish_flag);
        }
        if (stepid == 480 || stepid == 490) { //pressed ward 
            sql.append(" wardbyid = ?wardbyid , \n");
            if (ward_id != null && !ward_id.equals("")) {
                sql.append(" wardid = ?wardid ,\n");
                sql.setParameter("wardid", ward_id);
            }

            sql.append(" wardtime = ?wardtime , \n");
            sql.setParameter("wardbyid", authority_id);
            sql.setParameter("wardtime", client_time);

            insertBeaconTemp(connection, stroke_id, authority_id, "4", client_time0, -1);
        }
        if (stepid == 510) { //pressed admit stroke unit 
            if (ward_id != null && !ward_id.equals("")) {
                sql.append(" wardid = ?wardid ,\n");
                sql.setParameter("wardid", ward_id);
            }
            sql.append(" wardbyid = '1' , \n");
            sql.append(" wardtime = ?wardtime , \n");
            sql.append(" admitwardtime = ?admitwardtime , \n");
            sql.append(" admitwardbyid = ?admitwardbyid , \n");
            sql.setParameter("wardtime", client_time);
            sql.setParameter("admitwardtime", client_time);
            sql.setParameter("admitwardbyid", authority_id);

            insertBeaconTemp(connection, stroke_id, authority_id, "7", client_time0, -1);
        }
        sql.append(" lastupdatebyid = ?lastupdatebyid , \n");
        sql.append(" lastupdatedate = ?lastupdatedate  \n");
        sql.append(" where strokeid = ?strokeid \n");
        sql.setParameter("currentstepid", stepid);
        sql.setParameter("lastupdatedate", client_time);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("strokeid", stroke_id);

        result = sql.executeUpdate(connection);

        //update t_displaypanel
        if (result > 0 && stepid == 510) {//pressed admit stroke unit 
            sql.clear();
            sql.append("insert into t_displaypanel(strokeid,displayid,displaydatetime,performbyid,performdate) \n");
            sql.append(" values(?strokeid,?displayid,?displaydatetime,?performbyid,?performdate) \n");
            sql.setParameter("performdate", client_time);
            sql.setParameter("performbyid", authority_id);
            sql.setParameter("strokeid", stroke_id);
            sql.setParameter("displayid", 41);
            sql.setParameter("displaydatetime", client_time);
            result += sql.executeUpdate(connection);
        }
        //insert t_transactionlog
        //	compareData(connection,vec,"t_stroke","strokeid",Integer.toString(stroke_id),Integer.toString(stroke_id),Integer.toString(authority_id),client_time);

        return true;
    }

    public boolean document_signed_next(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*													      "document_signed_next": {     	               					       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "authority_name": "Miss A B",                  					       */
 /*															 "stepid": 1,                              								       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        Trace.info("=================jsonStr[" + data + "]");

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }

        JSONObject jObject = new JSONObject(data);
        String client_time0 = jObject.getString("client_time");
        java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
        int stepid = jObject.getInt("stepid");

        updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);

        return true;
    }

    public boolean consent_signed(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*													         "consent_signed": {     	                					           */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",          								           */
 /*															 "authority_name": "Mister A B",       								   */
 /*															 "stepid": "1",                              								   */
 /*															 "consent_signed_flag": "0"                          					   */
 /*															 "consent_signed_flagtime": "20170502 13:20:20"			   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        Trace.info("=================jsonStr[" + data + "]");

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }

        JSONObject jObject = new JSONObject(data);
        String client_time0 = jObject.getString("client_time");
        java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
        int stepid = jObject.getInt("stepid");

        int result = 0;
        KnSQL sql = new KnSQL();
        sql.append(" update T_Treatment set  \n");
        sql.append(" consentsignedflag = ?consentsignedflag,  \n");
        sql.append(" consentsignedflagbyid = ?consentsignedflagbyid,  \n");
        sql.append(" consentsignedflagtime = ?consentsignedflagtime,  \n");
        sql.append(" LastUpdateById = ?lastupdatebyid,  \n");
        sql.append(" LastUpdateDate = ?lastupdatedate  \n");
        sql.append(" where StrokeId = ?strokeid  \n");
        sql.setParameter("strokeid", StrokeId);
        sql.setParameter("consentsignedflag", jObject.getString("consent_signed_flag"));
        sql.setParameter("consentsignedflagbyid", authority_id);
        sql.setParameter("consentsignedflagtime", jObject.getString("consent_signed_flagtime"));
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        result = sql.executeUpdate(connection);
        //update t_displaypanel
        if (result > 0) {//pressed admit stroke unit 
            sql.clear();
            sql.append("insert into t_displaypanel(strokeid,displayid,displaydatetime,performbyid,performdate) \n");
            sql.append(" values(?strokeid,?displayid,?displaydatetime,?performbyid,?performdate) \n");
            sql.setParameter("performdate", client_time);
            sql.setParameter("performbyid", authority_id);
            sql.setParameter("strokeid", StrokeId);
            if (jObject.getString("consent_signed_flag") != null && jObject.getString("consent_signed_flag").equals("1")) {
                sql.setParameter("displayid", 15);
            } else {
                sql.setParameter("displayid", 16);
            }
            sql.setParameter("displaydatetime", client_time);
            result += sql.executeUpdate(connection);
        }
        if (flag) {
            updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);
        }

        return true;
    }

    public boolean vital_sign_time(java.sql.Connection connection, int StrokeId, String data, boolean flag, String apiflag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*													         "vital_sign_time": {     	                					           */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "authority_name": "Mister A B",       								   */
 /*															 "stepid": "1",                              								   */
 /*															 "record_time": "20170502 13:20:20"					           */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */

        /**
         * *******************************************************************************
         */
        /*													         "vital_sign_result": {     	                					           */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "authority_name": "Mister A B",       								   */
 /*															 "stepid": "1",                              								   */
 /*															 "vital_sign_time": "20170502 13:20:20"	,				           */
 /*															 "value_hr": "1",                              							   */
 /*															 "value_rr": "1",                               							   */
 /*															 "value_sbp": "1",                              							   */
 /*															 "value_dbp": "1",                              							   */
 /*															 "value_o2": "1",                              							   */
 /*															 "value_temp": "1",                             							   */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        Trace.info("=================jsonStr[" + data + "]");

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }

        JSONObject jObject = new JSONObject(data);
        String client_time0 = jObject.getString("client_time");
        java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
        int stepid = jObject.getInt("stepid");

        int result = 0;
        KnSQL sql = new KnSQL();
        getstroke = getValueStrokeId(connection, StrokeId, "t_vitalsign");
        Trace.info("+++getstroke++++++++++++++++" + getstroke);
        if (getstroke > 0) {
            java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_vitalsign", "strokeid", Integer.toString(StrokeId));
            sql.clear();
            //update t_vitalsign
            sql.append("update t_vitalsign set \n");
            if (apiflag != null && apiflag.equals("vital_sign_time")) {
                sql.append(" recordtime = ?recordtime , \n");
                sql.setParameter("recordtime", jObject.getString("record_time"));
            } else if (apiflag != null && apiflag.equals("vital_sign_result")) {
                sql.append(" heartrate = ?heartrate , \n");
                sql.append(" RespiratoryRate  = ?RespiratoryRate  , \n");
                sql.append(" sbp  = ?sbp  , \n");
                sql.append(" dbp  = ?dbp  , \n");
                sql.append(" OxygenSaturation   = ?OxygenSaturation   , \n");
                sql.append(" Temperature  = ?Temperature  , \n");
                sql.append(" VitalSignTime  = ?VitalSignTime  , \n");
                sql.setParameter("heartrate", jObject.getString("value_hr"));
                sql.setParameter("RespiratoryRate", jObject.getString("value_rr"));
                sql.setParameter("sbp", jObject.getString("value_sbp"));
                sql.setParameter("dbp", jObject.getString("value_dbp"));
                sql.setParameter("OxygenSaturation", jObject.getString("value_o2"));
                sql.setParameter("Temperature", jObject.getString("value_temp"));
                sql.setParameter("VitalSignTime", jObject.getString("vital_sign_time"));

            }
            sql.append(" vitalsignbyid = ?vitalsignbyid , \n");
            sql.append(" LastUpdateById = ?lastupdatebyid,  \n");
            sql.append(" LastUpdateDate = ?lastupdatedate  \n");
            sql.append(" where StrokeId = ?strokeid  \n");
            sql.setParameter("strokeid", StrokeId);
            sql.setParameter("vitalsignbyid", authority_id);
            sql.setParameter("lastupdatebyid", authority_id);
            sql.setParameter("lastupdatedate", client_time);
            result = sql.executeUpdate(connection);

            //insert t_transactionlog
            compareData(connection, vec, "t_vitalsign", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);

        } else {
            String column = "";
            String param = "";
            sql.clear();
            if (apiflag != null && apiflag.equals("vital_sign_time")) {
                column = ",recordtime";
                param = ",?recordtime";
                sql.setParameter("recordtime", jObject.getString("record_time"));
            } else if (apiflag != null && apiflag.equals("vital_sign_result")) {
                column = ",heartrate,RespiratoryRate,sdp,dbp,OxygenSaturation,Temperature,VitalSignTime";
                param = ",?heartrate,?RespiratoryRate,?sdp,?dbp,?OxygenSaturation,?Temperature,?VitalSignTime";
                sql.setParameter("heartrate", jObject.getString("value_hr"));
                sql.setParameter("RespiratoryRate", jObject.getString("value_rr"));
                sql.setParameter("sbp", jObject.getString("value_sbp"));
                sql.setParameter("dbp", jObject.getString("value_dbp"));
                sql.setParameter("OxygenSaturation", jObject.getString("value_o2"));
                sql.setParameter("Temperature", jObject.getString("value_temp"));
                sql.setParameter("VitalSignTime", jObject.getString("vital_sign_time"));
            }
            sql.append("insert into t_vitalsign(strokeid,vitalsignbyid" + column + ",lastupdatebyid,lastupdatedate) \n");
            sql.append(" values(?strokeid,?vitalsignbyid" + param + ",?lastupdatebyid,?lastupdatedate)\n");
            sql.setParameter("strokeid", StrokeId);
            sql.setParameter("vitalsignbyid", authority_id);
            sql.setParameter("lastupdatebyid", authority_id);
            sql.setParameter("lastupdatedate", client_time);
            result = sql.executeUpdate(connection);
        }

        return true;
    }

    public boolean checkDulplicateT_AuthorityArrival(java.sql.Connection connection, String authorityid, int strokeid) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("select AuthorityId from t_authorityarrival \n");
        sql.append(" where AuthorityId = ?authorityId \n");
        sql.append(" and StrokeId = ?strokeid");
        sql.setParameter("authorityid", authorityid);
        sql.setParameter("strokeid", strokeid);
        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) {
            return true;
        }
        return false;
    }

    public void insertT_AuthorityArrival(java.sql.Connection connection, String authorityid, int strokeid, java.sql.Timestamp clienttime) throws Exception {

        KnSQL sql = new KnSQL();
        sql.append("insert into t_authorityarrival \n");
        sql.append("values(?authorityid, ?strokeid, ?arrivaltime, ?lastupdatebyid, ?lastupdateDate)");
        sql.setParameter("authorityid", authorityid);
        sql.setParameter("strokeid", strokeid);
        sql.setParameter("arrivaltime", clienttime);
        sql.setParameter("lastupdatebyid", authorityid);
        sql.setParameter("lastupdateDate", clienttime);
        sql.executeUpdate(connection);
    }

    public String getRealAuthorityId(java.sql.Connection connection, String sapid, String authorityid, int strokeid, java.sql.Timestamp clienttime) throws Exception {
        if (authorityid == null || authorityid.equals("")) {
            //check M_authority
            KnSQL sql = new KnSQL();
            sql.append("select IsActive,AuthorityId \n");
            sql.append("from M_Authority \n");
            sql.append("where SAPNumber = ?sapnumber ");
            sql.setParameter("sapnumber", sapid);
            java.sql.ResultSet rs = sql.executeQuery(connection);
            if (rs.next()) {
                if (rs.getString("IsActive").equals("0")) {
                    throw new Exception("SERR:4");
                }
                if (!checkDulplicateT_AuthorityArrival(connection, rs.getString("AuthorityId"), strokeid)) {
                    insertT_AuthorityArrival(connection, rs.getString("AuthorityId"), strokeid, clienttime);
                }
                return rs.getString("AuthorityId");
            } else {
                Trace.info("sending to siriraj server");
                //send to siriraj server
                TheEmployee pt = new TheEmployee();
                Employee emp = pt.getEmployeeBySAP(sapid);
                if (emp == null) { //can not connect server
                    //return sapid;
                    return insertM_Authority(connection, "", "", "", sapid, strokeid, clienttime);
                } else {
                    Trace.info("sent to siriraj status : " + emp.isSuccess());
                    if (emp.isSuccess()) { // status = 0
                        String EmpName = emp.getEmpName();
                        String MDNumber = emp.getMdNumber();
                        String authorityType = emp.getAuthorityType();
                        String authorityTypeID = checkM_AuthorityType(connection, authorityType);
                        return insertM_Authority(connection, authorityTypeID, butil.MS874ToUnicode(EmpName), butil.MS874ToUnicode(MDNumber), sapid, strokeid, clienttime);
                    } else {
                        //throw new Exception("SERR:16");
                        Trace.info(sapid + " === not found data at siriraj server ====");
                        return insertM_Authority(connection, "", "", "", sapid, strokeid, clienttime);
                    }
                }
            }
        } else {
            if (!checkDulplicateT_AuthorityArrival(connection, authorityid, strokeid)) {
                insertT_AuthorityArrival(connection, authorityid, strokeid, clienttime);
            }
            return authorityid;
        }
    }

    public String getRealAuthorityId(java.sql.Connection connection, String sapid, String authorityid) throws Exception {
        if (authorityid == null || authorityid.equals("")) {
            //check M_authority
            KnSQL sql = new KnSQL();
            sql.append("select IsActive,AuthorityId \n");
            sql.append("from M_Authority \n");
            sql.append("where SAPNumber = ?sapnumber ");
            sql.setParameter("sapnumber", sapid);
            java.sql.ResultSet rs = sql.executeQuery(connection);
            if (rs.next()) {
                if (rs.getString("IsActive").equals("0")) {
                    throw new Exception("SERR:4");
                }
                return rs.getString("AuthorityId");
            } else {
                Trace.info("sending to siriraj server");
                //send to siriraj server
                TheEmployee pt = new TheEmployee();
                Employee emp = pt.getEmployeeBySAP(sapid);
                if (emp == null) { //can not connect server
                    //return sapid;
                    return insertM_Authority(connection, "", "", "", sapid);
                } else {
                    Trace.info("sent to siriraj status : " + emp.isSuccess());
                    if (emp.isSuccess()) { // status = 0
                        String EmpName = emp.getEmpName();
                        String MDNumber = emp.getMdNumber();
                        String authorityType = emp.getAuthorityType();
                        String authorityTypeID = checkM_AuthorityType(connection, authorityType);
                        return insertM_Authority(connection, authorityTypeID, butil.MS874ToUnicode(EmpName), butil.MS874ToUnicode(MDNumber), sapid);
                    } else {
                        //throw new Exception("SERR:16");
                        Trace.info(sapid + " === not found data at siriraj server ====");
                        return insertM_Authority(connection, "", "", "", sapid);
                    }
                }
            }
        } else {
            return authorityid;
        }
    }

    public String checkM_AuthorityType(java.sql.Connection connection, String authorityType) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("select AuthorityTypeId,SRJ_AuthorityCode from M_AuthorityType \n");
        sql.append(" where SRJ_AuthorityCode = ?authoritytype ");
        sql.setParameter("authoritytype", authorityType);
        java.sql.ResultSet rs = sql.executeQuery(connection);
        String id = "";

        if (rs != null && rs.next()) {
            id = rs.getString("AuthorityTypeId");
        } else {
            /*	sql.clear();
						sql.append("insert into M_AuthorityType(SRJ_AuthorityCode,CreateDate) \n ");
						sql.append("values(?authoritytype,now()) ");
						sql.setParameter("authoritytype" , authorityType);
						sql.executeUpdate(connection);
						id = checkM_AuthorityType(connection,authorityType);
             */
            return null;
        }
        return id;
    }

    public String insertM_Authority(java.sql.Connection connection, String AuthorityTypeId, String EmpName,
            String MDNumber, String sapnumber, int strokeid, java.sql.Timestamp clienttime) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("insert into M_Authority (AuthorityTypeId,FirstName,MDNumber,SAPNumber,IsActive,UpdateById,UpdateDate) \n");
        sql.append("values(?AuthorityTypeId,?FirstName,?MDNumber,?SAPNumber,'1',?CreateById,?Createdate) ");
        sql.setParameter("AuthorityTypeId", AuthorityTypeId);
        sql.setParameter("FirstName", EmpName);
        sql.setParameter("MDNumber", MDNumber);
        sql.setParameter("SAPNumber", sapnumber);
        sql.setParameter("IsActive", 1);
        sql.setParameter("CreateById", -99);
        sql.setParameter("Createdate", butil.getCurrentTimestamp());
        int result = sql.executeUpdate(connection);

        sql.clear();
        sql.append("insert into t_Authority (AuthorityTypeId,FirstName,MDNumber,SAPNumber,IsActive,UpdateById,UpdateDate) \n");
        sql.append("values(?AuthorityTypeId,?FirstName,?MDNumber,?SAPNumber,'1',?CreateById,?CreateDate) ");
        sql.setParameter("AuthorityTypeId", AuthorityTypeId);
        sql.setParameter("FirstName", EmpName);
        sql.setParameter("MDNumber", MDNumber);
        sql.setParameter("SAPNumber", sapnumber);
        sql.setParameter("IsActive", 1);
        sql.setParameter("CreateById", -99);
        sql.setParameter("CreateDate", butil.getCurrentTimestamp());
        //		sql.executeUpdate(connection);
        sql.executeUpdate(connection, java.sql.Statement.RETURN_GENERATED_KEYS);

        /*	sql.clear();
					sql.append("select authorityid from M_Authority \n");
					sql.append(" where AuthorityTypeId = ?AuthorityTypeId \n");
					sql.append("and SAPNumber = ?SAPNumber \n");
					sql.append("and IsActive = ?IsActive \n");
					sql.setParameter("AuthorityTypeId", AuthorityTypeId);
					sql.setParameter("SAPNumber", sapnumber);
					sql.setParameter("IsActive", 1);
					java.sql.ResultSet rs = sql.executeQuery(connection);
					if(rs !=null && rs.next()) {
						return rs.getString("authorityid");
					}
					return sapnumber;
         */
        long authority = sql.getGeneratedKey();

        if (!checkDulplicateT_AuthorityArrival(connection, Long.toString(authority), strokeid)) {
            insertT_AuthorityArrival(connection, Long.toString(authority), strokeid, clienttime);
        }
        return Long.toString(authority);

    }

    public String insertM_Authority(java.sql.Connection connection, String AuthorityTypeId, String EmpName,
            String MDNumber, String sapnumber) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("insert into M_Authority (AuthorityTypeId,FirstName,MDNumber,SAPNumber,IsActive,UpdateById,UpdateDate) \n");
        sql.append("values(?AuthorityTypeId,?FirstName,?MDNumber,?SAPNumber,'1',?CreateById,?Createdate) ");
        sql.setParameter("AuthorityTypeId", AuthorityTypeId);
        sql.setParameter("FirstName", EmpName);
        sql.setParameter("MDNumber", MDNumber);
        sql.setParameter("SAPNumber", sapnumber);
        sql.setParameter("IsActive", 1);
        sql.setParameter("CreateById", -99);
        sql.setParameter("Createdate", butil.getCurrentTimestamp());
        int result = sql.executeUpdate(connection);

        sql.clear();
        sql.append("insert into t_Authority (AuthorityTypeId,FirstName,MDNumber,SAPNumber,IsActive,UpdateById,UpdateDate) \n");
        sql.append("values(?AuthorityTypeId,?FirstName,?MDNumber,?SAPNumber,'1',?CreateById,?CreateDate) ");
        sql.setParameter("AuthorityTypeId", AuthorityTypeId);
        sql.setParameter("FirstName", EmpName);
        sql.setParameter("MDNumber", MDNumber);
        sql.setParameter("SAPNumber", sapnumber);
        sql.setParameter("IsActive", 1);
        sql.setParameter("CreateById", -99);
        sql.setParameter("CreateDate", butil.getCurrentTimestamp());
        sql.executeUpdate(connection, java.sql.Statement.RETURN_GENERATED_KEYS);

        long authority = sql.getGeneratedKey();
        return Long.toString(authority);

    }

    public boolean lab_result(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"lab_result": {            		          		                	           */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",                 								   */
 /*															 "stepid": "1",                              								   */
 /*															 "lab_time": "13:20:20",	                                               */
 /*															 "lab": [	                                                                       */
 /*															 {"type":"inr","flag": -1,"value": 30},	                               */
 /*															 {"type":"pt","flag": -1,"value": 200},                                */
 /*															 {"type":"aptt","flag": -1,"value": 20},                               */
 /*															 {"type":"platelet","flag": -1,"value": 20}                           */
 /*															 ]                                                                                */
 /*															 }                                                                                */
        /**
         * *******************************************************************************
         */

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }
        try {
            Trace.debug("========== Get JsonString ========== ");
            JSONObject jObject = new JSONObject(data);
            String client_time0 = jObject.getString("client_time");
            java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
            String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
            int stepid = jObject.getInt("stepid");
            String lab_time = jObject.getString("lab_time");
            JSONArray paramdata = jObject.getJSONArray("lab");
            int result = 0;

            getstroke = getValueStrokeId(connection, StrokeId, "t_lab");
            Trace.info("+++getstroke++++++++++++++++" + getstroke);
            if (getstroke > 0) {
                java.util.Vector<java.util.Vector> vec = getOldData(connection, "t_lab", "strokeid", Integer.toString(StrokeId));
                result = updateT_LAB(connection, paramdata, "update", StrokeId, authority_id, client_time, lab_time);
                compareData(connection, vec, "t_lab", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            } else {
                result = updateT_LAB(connection, paramdata, "insert", StrokeId, authority_id, client_time, lab_time);
                insertT_TransactionLog(connection, "t_lab", "strokeid", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            }
            if (flag) {
                updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);
            }
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    public int updateT_LAB(java.sql.Connection connection, JSONArray jsondata, String mode, int stroke_id, String authority_id, java.sql.Timestamp client_time, String lab_time) throws Exception {
        int result = 0;

        int n = jsondata.length();
        String colins = "";
        String parains = "";
        String colupd = "";

        KnSQL sql = new KnSQL();

        String inr = null;
        String pt = null;
        String aptt = null;
        String platelet = null;
        for (int i = 0; i < n; i++) {
            JSONObject paramArray = jsondata.getJSONObject(i);
            String type = paramArray.getString("type");
            if ("inr".equalsIgnoreCase(type)) {
                inr = paramArray.getString("value");
                colupd += " INR=?inr ,\n";
                colins += "INR,";
                parains += "?inr , ";
            } else if ("pt".equalsIgnoreCase(type)) {
                pt = paramArray.getString("value");
                colupd += " PT=?pt ,\n";
                colins += "PT,";
                parains += "?pt ,";
            } else if ("aptt".equalsIgnoreCase(type)) {
                aptt = paramArray.getString("value");
                colupd += " aPTT = ?aptt , \n";
                colins += "aPTT,";
                parains += "?aptt ,";
            } else if ("platelet".equalsIgnoreCase(type)) {
                platelet = paramArray.getString("value");
                colupd += " Platelet = ?platelet , \n";
                colins += "Platelet,";
                parains += "?platelet,";
            }

        }
        if ("update".equals(mode)) {
            sql.append("update t_lab set \n");
            //INR=?inr, PT=?pt, aPTT=?aptt, Platelet=?platelet, \n");
            sql.append(colupd);
            sql.append(" LabById=?labbyid, LabTime=?labtime, LastUpdateById=?lastupdatebyid, \n");
            sql.append("LastUpdateDate=?lastupdatedate where StrokeId=?strokeid \n");

        } else if ("insert".equals(mode)) {
            //INR,PT,aPTT,Platelet,Lab
            sql.append("insert into t_lab (");
            sql.append(colins);
            sql.append(" StrokeId,LabById,LabTime,LastUpdateById,LastUpdateDate) ");
            sql.append("values(");
            sql.append(parains);
            sql.append("?StrokeId,?LabById,?LabTime,?LastUpdateById,?LastUpdateDate)");
        }
        sql.setParameter("inr", inr);
        sql.setParameter("pt", chkNotNull(pt));
        sql.setParameter("aptt", aptt);
        sql.setParameter("platelet", platelet);
        sql.setParameter("labbyid", authority_id);
        sql.setParameter("labtime", lab_time);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        sql.setParameter("strokeid", stroke_id);
        result = sql.executeUpdate(connection);
        return result;
    }

    public boolean angiogram_next(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*													         "angiogram_next": {     	               					               */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "authority_name": "Miss A B",                  					       */
 /*															 "stepid": 1,                              								       */
 /*															 }								        									       */
        /**
         * *******************************************************************************
         */
        Trace.info("=================jsonStr[" + data + "]");

        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }

        JSONObject jObject = new JSONObject(data);
        String client_time0 = jObject.getString("client_time");
        java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
        String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
        int stepid = jObject.getInt("stepid");

        java.util.Vector<java.util.Vector> vec = getOldData(connection, "T_Stroke", "StrokeId", Integer.toString(StrokeId));

        int result = 0;
        KnSQL sql = new KnSQL();
        sql.append("update T_Stroke set CurrentStepId=?stepid, LastUpdateById=?lastupdatebyid, LastUpdateDate=?lastupdatedate \n");
        sql.append(" where StrokeId = ?strokeid \n");
        sql.setParameter("strokeid", StrokeId);
        sql.setParameter("stepid", stepid);
        sql.setParameter("lastupdatebyid", authority_id);
        sql.setParameter("lastupdatedate", client_time);
        //	sql.setParameter("wardtime",client_time);
        result += sql.executeUpdate(connection);

        compareData(connection, vec, "T_Stroke", "StrokeId", Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
        insertBeaconTemp(connection, StrokeId, authority_id, "6", client_time0, 30);

        return true;
    }

    public boolean tpa_criteria(java.sql.Connection connection, int StrokeId, String data, boolean flag) throws Exception {
        /**
         * *******************************************************************************
         */
        /*															"tpa_criteria_sys": {            		          		                       */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",                 								   */
 /*															 "stepid": "1",                              								   */
 /*															 "autority_name": "Miss,Tom Jonh",                                  */
 /*															 "criteria": [	                                                                   */
 /*															 {"criteriaid":"5","result": "0"},	                                       */
 /*															 {"criteriaid":"6","result": "0"},	                                       */
 /*															 {"criteriaid":"7","result": "0"},	                                       */
 /*															 {"criteriaid":"8","result": "1"},	                                       */
 /*															 ]                                                                                */
 /*															 }                                                                                */
        /**
         * *******************************************************************************
         */

        /**
         * *******************************************************************************
         */
        /*															"tpa_criteria_neuro": {            		          		                   */
 /*															 "client_time": "20170502 13:20:20",			    				   */
 /*															 "authority_id": "19002345",          								   */
 /*															 "sap_id": "19002345",                 								   */
 /*															 "stepid": "1",                              								   */
 /*															 "autority_name": "Miss,Tom Jonh",                                  */
 /*															 "criteria": [	                                                                   */
 /*															 {"criteriaid":"5","result": "0"},	                                       */
 /*															 {"criteriaid":"6","result": "0"},	                                       */
 /*															 {"criteriaid":"7","result": "0"},	                                       */
 /*															 {"criteriaid":"8","result": "1"},	                                       */
 /*															 ]                                                                                */
 /*															 }                                                                                */
        /**
         * *******************************************************************************
         */
        int getstroke = 0;
        if (data == null || data.equals("")) { //validate data
            throw new Exception("data is undefined");
        }
        if (!isJSONValid(data)) { //validate JSON format
            throw new Exception("data is incorrect format(JSON)");
        }
        try {
            Trace.debug("========== Get JsonString ========== ");
            JSONObject jObject = new JSONObject(data);
            String client_time0 = jObject.getString("client_time");
            java.sql.Timestamp client_time = convertStrToTimestamp(client_time0);
            String authority_id = getRealAuthorityId(connection, jObject.getString("sap_id"), jObject.getString("authority_id"), StrokeId, client_time);
            int stepid = jObject.getInt("stepid");

            JSONArray paramdata = jObject.getJSONArray("criteria");
            int n = paramdata.length();
            if (n <= 0) {
                throw new Exception("data length is 0");
            }

            int result = 0;
            KnSQL sql = new KnSQL();
            sql.append("insert into t_criteriaresult(criteriaid,strokeid,result,resultbyid,resulttime,lastupdatebyid,lastupdatedate) \n");
            sql.append(" values (?criteriaid,?strokeid,?result,?resultbyid,?resulttime,?lastupdatebyid,?lastupdatedate) \n");
            sql.setCompile(true);
            String d_criteriaid = null;
            String d_result = null;
            for (int i = 0; i < n; ++i) {
                JSONObject paramArray = paramdata.getJSONObject(i);
                d_criteriaid = paramArray.getString("criteriaid");
                d_result = paramArray.getString("result");

                Trace.info(this, "criteriaid====" + paramArray.getString("criteriaid"));
                Trace.info(this, "result====" + paramArray.getString("result"));

                if (d_criteriaid == null || d_criteriaid.equals("")) {
                    throw new Exception("criteriaid is undefined");
                }
                /*if(d_result == null || d_result.equals("") || !butil.isInset(d_result,"0,1")) {
							throw new Exception("result is undefined or result is out of set[0,1] ");
						} */
                if (isDuplicateCriteriaresult(connection, d_criteriaid, StrokeId)) {
                    throw new Exception("Duplicate criteriaid[" + d_criteriaid + "],strokeid[" + StrokeId + "]");
                }
                sql.clearParameters();
                sql.setParameter("strokeid", StrokeId);
                sql.setParameter("criteriaid", d_criteriaid);
                sql.setParameter("result", d_result);
                sql.setParameter("resultbyid", authority_id);
                sql.setParameter("resulttime", client_time);
                sql.setParameter("lastupdatebyid", authority_id);
                sql.setParameter("lastupdatedate", client_time);
                result = sql.executeUpdate(connection);
                insertT_TransactionLog(connection, "t_criteriaresult", "criteriaid,strokeid", d_criteriaid + "," + Integer.toString(StrokeId), Integer.toString(StrokeId), authority_id, client_time);
            };

            if (flag) {
                updateT_Stroke(connection, StrokeId, stepid, Integer.parseInt(authority_id), client_time);
            }
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    public boolean isDuplicateCriteriaresult(java.sql.Connection connection, String criteriaid, int strokeid) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("select criteriaid from t_criteriaresult \n");
        sql.append("where criteriaid = ?criteriaid \n");
        sql.append("and strokeid = ?strokeid \n");
        sql.setParameter("strokeid", strokeid);
        sql.setParameter("criteriaid", criteriaid);

        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) {
            return true;
        }
        return false;
    }

    public int insertBeacon(java.sql.Connection connection, int strokeid, String authority_id, java.sql.Timestamp client_time, String Beaconrecordid) throws Exception {
        KnSQL sql = new KnSQL();
        sql.append("insert t_beacon(Strokeid,Beaconrecordid,beacontime,lastupdatebyid,lastupdatedate) \n");
        sql.append(" values(?Strokeid,?Beaconrecordid,?beacontime,?LastUpdateById,?LastUpdateDate) \n");
        sql.setParameter("Strokeid", strokeid);
        sql.setParameter("Beaconrecordid", Beaconrecordid);
        sql.setParameter("beacontime", client_time);
        sql.setParameter("LastUpdateById", authority_id);
        sql.setParameter("LastUpdateDate", client_time);
        return sql.executeUpdate(connection);
    }

    public boolean insertBeaconTemp(java.sql.Connection connection, int strokeid, String authority_id,
            String Beaconrecordid, String timeStr, int interval) throws Exception {
        /**
         * **** this part will delete in the future ****
         */

        java.sql.Timestamp bw_time = convertStrToTimestamp(timeStr);

        java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd HH:mm:ss");
        if (timeStr.indexOf("-") >= 0) {
            dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }

        Date parsedDate = dateFormat.parse(timeStr);
        Calendar cal = Calendar.getInstance();
        cal.setTime(parsedDate);
        cal.add(Calendar.MINUTE, interval);
        String newTime = dateFormat.format(cal.getTime());

        Trace.info("New Time ====" + newTime);
        if (Beaconrecordid != null && Beaconrecordid.equals("4")) {
            //check exist Beaconrecordid 4
            KnSQL sql = new KnSQL();
            sql.append("select Beaconrecordid from t_beacon \n");
            sql.append("where strokeid = ?strokeid \n");
            sql.append("and Beaconrecordid = '4' \n");
            sql.setParameter("strokeid", strokeid);
            java.sql.ResultSet rs = sql.executeQuery(connection);
            if (rs != null && rs.next()) {
                return true;
            }
        }
        insertBeacon(connection, strokeid, authority_id, convertStrToTimestamp(newTime), Beaconrecordid);

        /**
         * ***********************************
         */
        return true;
    }

    public java.util.Vector<java.util.Vector> getArrayOldData(java.sql.Connection conn, String table, String keyfield, String datavalue) throws Exception {
        java.util.Vector<java.util.Vector> vecAll = new java.util.Vector<java.util.Vector>();

        java.util.Vector<String> vec = new java.util.Vector<String>();
        java.util.Vector<String> vectype = new java.util.Vector<String>();

        KnSQL sql = new KnSQL();
        sql.append("   select distinct s.COLUMN_NAME,s.DATA_TYPE \n");
        sql.append(" from INFORMATION_SCHEMA.COLUMNS s \n");
        sql.append(" where  s.TABLE_NAME = ?tablename \n");
        sql.append(" order by s.COLUMN_NAME \n");
        sql.setParameter("tablename", table);
        java.sql.ResultSet rs = sql.executeQuery(conn);
        while (rs.next()) {
            vec.addElement(rs.getString("COLUMN_NAME"));
            vectype.addElement(rs.getString("DATA_TYPE"));
        }

        sql.clear();
        sql.append("select * from " + table);
        sql.append(" where 1=1 \n");

        String[] key = keyfield.split(",");
        String[] value = datavalue.split(",");

        for (int i = 0; i < key.length; i++) {
            sql.append(" and " + key[i] + " = ?" + key[i] + " \n");
            sql.setParameter(key[i], value[i]);
        }

        rs = sql.executeQuery(conn);
        while (rs != null && rs.next()) {
            java.util.Vector<java.util.Vector> vecmain = new java.util.Vector<java.util.Vector>();
            java.util.Vector<String> vecdata = new java.util.Vector<String>();
            for (int i = 0; i < vec.size(); i++) {
                String fieldname = (String) vec.elementAt(i);
                vecdata.addElement(rs.getString(fieldname));
            }
            Trace.debug("vec size -------------------> " + vec.size());
            Trace.debug("vecdata size ---------------> " + vecdata.size());
            vecmain.addElement(vec);
            vecmain.addElement(vecdata);
            vecmain.addElement(vectype);
            vecAll.addElement(vecmain);
        }
        Trace.debug("vecAll size ---------------> " + vecAll.size());
        return vecAll;
    }

    public void compareArrayData(java.sql.Connection conn, java.util.Vector<java.util.Vector> OldVec, String table, String keyfield, String datavalue) throws Exception {
        /*
			java.util.Vector<String> vecfield = new java.util.Vector<String>();
			java.util.Vector<String> vecdata = new java.util.Vector<String>();
			java.util.Vector<String> vectype = new java.util.Vector<String>();

			if(OldVec != null && OldVec.size() >= 3) {
				vecfield = (java.util.Vector<String>)OldVec.elementAt(0);
				vecdata = (java.util.Vector<String>)OldVec.elementAt(1);
				vectype = (java.util.Vector<String>)OldVec.elementAt(2);

				KnSQL sql = new KnSQL();
				sql.append("select * from "+table) ;
				sql.append(" where 1=1 \n");

				String[] key = keyfield.split(",");
				String[] value = datavalue.split(",");

				for(int i=0;i < key.length;i++) {
					sql.append(" and "+key[i]+" = ?"+key[i]+" \n");
					sql.setParameter(key[i],value[i]);
				}

				java.sql.ResultSet rs = sql.executeQuery(conn);
				if(rs.next()) {
					KnSQL sql_ins = new KnSQL();
					sql_ins.append("insert into T_TransactionLog (Action,ActionFrom,StrokeId,TableName,ChangedColumn,NewData,OldData,KeyName,KeyId,PerformById,PerformDate)  \n");
					sql_ins.append(" values ('update','App',?TableName,?ChangedColumn,?NewData,?OldData,?KeyName,?KeyId) \n");
					sql_ins.setCompile(true);
					sql_ins.setParameter("TableName",table);
					sql_ins.setParameter("KeyName",key[0]);
					sql_ins.setParameter("KeyId",value[0]);
					for(int i=0;i< vecfield.size();i++) {
						String fieldname = (String)vecfield.elementAt(i);
						String newData = rs.getString(fieldname);
						String oldData = (String)vecdata.elementAt(i);
						String type = (String)vectype.elementAt(i);
						Trace.info("fieldname["+fieldname+"]  type["+type+"] new data["+newData+"]==== old data ["+oldData+"] ");
						
						if(newData == null && oldData==null) {
							continue;
						} else if((newData == null && oldData != null) ||(newData != null && oldData == null)) {
							sql_ins.setParameter("NewData",newData);
							sql_ins.setParameter("OldData",oldData);
							sql_ins.setParameter("ChangedColumn",fieldname);
							sql_ins.executeUpdate(conn);
						} else if(!newData.equals(oldData)) {
							sql_ins.setParameter("NewData",newData);
							sql_ins.setParameter("OldData",oldData);
							sql_ins.setParameter("ChangedColumn",fieldname);
							sql_ins.executeUpdate(conn);
						}
					}
				}
			}
         */
        Trace.debug("---------------------- compare Array ----------------------------");
    }

    public String getSystemConfig(java.sql.Connection connection, String category, String configName) throws Exception {
        String value = null;
        KnSQL sql = new KnSQL();
        sql.append("SELECT value FROM S_SystemConfig WHERE category = ?category AND configName = ?configName");
        sql.setParameter("category", category);
        sql.setParameter("configName", configName);
        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs != null && rs.next()) {
            value = rs.getString("value");
        }
        return value;
    }
}
