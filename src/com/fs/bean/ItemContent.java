package com.fs.bean;

/**
 * @author tassun_oro
 *
 */
public class ItemContent {
	private String userid;
	private String employeeid;
	private String employeecode;
	private String filename;
	private String message;
	private String remarks;
	
	public ItemContent() {
		super();
	}
	public ItemContent(String filename) {
		this(filename,null,null);
	}
	public ItemContent(String filename,String employeecode) {
		this(filename,employeecode,null);
	}
	public ItemContent(String filename,String employeecode,String remarks) {
		this.filename = filename;
		this.employeecode = employeecode;
		this.remarks = remarks;
	}
	public String getEmployeecode() {
		return employeecode;
	}
	public String getEmployeeid() {
		return employeeid;
	}
	public String getFilename() {
		return filename;
	}
	public String getMessage() {
		return message;
	}
	public String getRemarks() {
		return remarks;
	}
	public String getUserid() {
		return userid;
	}
	public void setEmployeecode(String employeecode) {
		this.employeecode = employeecode;
	}
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
}
