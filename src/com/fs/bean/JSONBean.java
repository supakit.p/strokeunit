package com.fs.bean;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;

public class JSONBean {
	public JSONBean() {
		super();
	}
	protected String getJSON(JSONObject json,String fieldName) {
		Object result = json.get(fieldName);
		if(result!=null) {
			return result.toString();
		}
		return null;
	}
	public void obtain(JSONArray json){
		//for override
	}	
	public void obtain(JSONObject json){
		//for override
	}	
	public void obtain(String jsonString) throws Exception{
		if (jsonString == null || jsonString.trim().length()<=0) {
			return;
		}
		Object jsonObject = new JSONParser().parse(jsonString);
		if(jsonObject instanceof JSONObject) {
			obtain((JSONObject) jsonObject);
		} else if(jsonObject instanceof JSONArray) {
			obtain((JSONArray) jsonObject);
		}
	}	
	public JSONObject toJSONObject(){
		return new JSONObject();
	}	
	public String toJSONString(){
		return toJSONObject().toJSONString();
	}
	public String toString() {
		return toJSONString();
	}
}
