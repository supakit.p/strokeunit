package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Tue Jun 27 09:21:57 ICT 2017
 */

import com.fs.bean.misc.*;
import com.fs.bean.util.*;
import com.fs.bean.gener.*;

@SuppressWarnings({"serial","unused"})
public class SFTQ001Bean extends BeanSchema {

//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTQ001Bean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("userid",java.sql.Types.VARCHAR,"User ID");
	addSchema("progid",java.sql.Types.VARCHAR,"Program ID");
	addSchema("datefrom",java.sql.Types.VARCHAR,"From Date");
	addSchema("dateto",java.sql.Types.VARCHAR,"To Date");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	collectClass(com.fs.bean.SFTQ001ABean.class);
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTQ001Bean.class+"=$Revision$\n";
}
public String getUserid() {
	return getMember("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getProgid() {
	return getMember("progid");
}
public void setProgid(String newProgid) {
	setMember("progid",newProgid);
}
public String getDatefrom() {
	return getMember("datefrom");
}
public void setDatefrom(String newDatefrom) {
	setMember("datefrom",newDatefrom);
}
public String getDateto() {
	return getMember("dateto");
}
public void setDateto(String newDateto) {
	setMember("dateto",newDateto);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
//#(30000) programmer code end;
}
