package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Mon Jun 26 09:12:53 ICT 2017
 */

import com.fs.bean.gener.*;

@SuppressWarnings("serial")
public class SFTE002ABean extends BeanSchema {

//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTE002ABean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("groupname",java.sql.Types.VARCHAR,"Group");
	addSchema("supergroup",java.sql.Types.VARCHAR,"Super Group");
	addSchema("nameen",java.sql.Types.VARCHAR,"Name English");
	addSchema("nameth",java.sql.Types.VARCHAR,"Name Thai");
	addSchema("privateflag",java.sql.Types.VARCHAR,"privateflag");
	addSchema("usertype",java.sql.Types.VARCHAR,"usertype");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	addSchema("userids",java.sql.Types.VARCHAR,"Users");
	addSchema("progids",java.sql.Types.VARCHAR,"Programs");
	addSchema("iconstyle",java.sql.Types.VARCHAR,"iconstyle");
	addSchema("seqno",java.sql.Types.VARCHAR,"Sequence");
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE002ABean.class+"=$Revision$\n";
}
public String getGroupname() {
	return getMember("groupname");
}
public String getIconstyle() {
	return getMember("iconstyle");
}
public String getNameen() {
	return getMember("nameen");
}
public String getNameth() {
	return getMember("nameth");
}
public String getPrivateflag() {
	return getMember("privateflag");
}
public String getProgids() {
	return getMember("progids");
}
public String getSupergroup() {
	return getMember("supergroup");
}
//#methods defined everything will flow
//#(30000) programmer code begin;
public String getUserids() {
	return getMember("userids");
}
public String getUsertype() {
	return getMember("usertype");
}
public void setGroupname(String newGroupname) {
	setMember("groupname",newGroupname);
}
public void setIconstyle(String newIconstyle) {
	setMember("iconstyle",newIconstyle);
}
public void setNameen(String newNameen) {
	setMember("nameen",newNameen);
}
public void setNameth(String newNameth) {
	setMember("nameth",newNameth);
}
public void setPrivateflag(String newPrivateflag) {
	setMember("privateflag",newPrivateflag);
}
public void setProgids(String newProgids) {
	setMember("progids",newProgids);
}
//#(30000) programmer code end;
public void setSupergroup(String newSupergroup) {
	setMember("supergroup",newSupergroup);
}
public void setUserids(String newUserids) {
	setMember("userids",newUserids);
}
public void setUsertype(String newUsertype) {
	setMember("usertype",newUsertype);
}
public String getSeqno() {
	return getMember("seqno");
}
public void setSeqno(String newSeqno) {
	setMember("seqno",newSeqno);
}
}
