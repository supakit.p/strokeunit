package com.fs.bean;

@SuppressWarnings("serial")
public class Columnar implements java.io.Serializable {
	private int seqno;
	private String columnname;
	private String columndesc;
	private boolean calfield;
	public Columnar() {
		super();
	}
	public Columnar(int seqno,String columnname,String columndesc) {
		this.seqno = seqno; 
		this.columnname = columnname; 
		this.columndesc = columndesc;
	}
	public Columnar(int seqno,String columnname,String columndesc,boolean calfield) {
		this.seqno = seqno; 
		this.columnname = columnname; 
		this.columndesc = columndesc;
		this.calfield = calfield;
	}
	public void fetchResult(java.sql.ResultSet rs) throws Exception {
		this.seqno = rs.getInt("seqno");
		this.columnname = rs.getString("columnname");
		this.columndesc = rs.getString("columndesc");
		if(this.columnname!=null) {
			this.columnname = this.columnname.toLowerCase();
		}
	}
	public String getColumnDesc() {
		return columndesc;
	}
	public String getColumnName() {
		return columnname;
	}
	public int getSeqno() {
		return seqno;
	}
	public boolean isCalField() {
		return calfield;
	}
	public void setColumnDesc(String columndesc) {
		this.columndesc = columndesc;
	}
	public void setColumnName(String columnname) {
		this.columnname = columnname;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String toString() {
		return columndesc;
	}
}
