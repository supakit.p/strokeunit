package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE006Bean.java
 * Description: SFTE006Bean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Mon Apr 01 14:17:12 ICT 2019
 */
/**
 * SFTE006Bean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class SFTE006Bean extends BeanSchema {
	public SFTE006Bean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("site",java.sql.Types.VARCHAR,"site");
		addSchema("shortname",java.sql.Types.VARCHAR,"shortname");
		addSchema("nameen",java.sql.Types.VARCHAR,"nameen");
		addSchema("nameth",java.sql.Types.VARCHAR,"nameth");
		addSchema("sitedesc",java.sql.Types.VARCHAR,"sitedesc");
		addSchema("products",java.sql.Types.VARCHAR,"products");
		addSchema("productcontent",java.sql.Types.VARCHAR,"productcontent");
	}
	public String getSite() {
		return getMember("site");
	}
	public void setSite(String newSite) {
		setMember("site",newSite);
	}
	public String getShortname() {
		return getMember("shortname");
	}
	public void setShortname(String newShortname) {
		setMember("shortname",newShortname);
	}
	public String getNameen() {
		return getMember("nameen");
	}
	public void setNameen(String newNameen) {
		setMember("nameen",newNameen);
	}
	public String getNameth() {
		return getMember("nameth");
	}
	public void setNameth(String newNameth) {
		setMember("nameth",newNameth);
	}
	public String getSitedesc() {
		return getMember("sitedesc");
	}
	public void setSitedesc(String newSitedesc) {
		setMember("sitedesc",newSitedesc);
	}
	public String getProducts() {
		return getMember("products");
	}
	public void setProducts(String newProducts) {
		setMember("products",newProducts);
	}
	public String getProductcontent() {
		return getMember("productcontent");
	}
	public void setProductcontent(String newProductcontent) {
		setMember("productcontent",newProductcontent);
	}
}
