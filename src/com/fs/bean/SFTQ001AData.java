package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTQ001AData.java
 * Description: SFTQ001AData class implements for handle tul data base schema.
 * Version: $Revision$
 * Creation date: Tue Jun 27 09:37:23 ICT 2017
 */
/**
 * SFTQ001AData class implements for handle tul data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTQ001AData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTQ001AData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tul");
	addSchema("seqno",java.sql.Types.BIGINT);
	addSchema("curtime",java.sql.Types.TIMESTAMP);
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("progid",java.sql.Types.VARCHAR);
	addSchema("action",java.sql.Types.VARCHAR);
	addSchema("remark",java.sql.Types.VARCHAR);
	map("userid","userid");
	map("progid","progid");
	map("action","action");
	map("remark","remark");
	map("curtime","curtime");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	addSchema("progname",java.sql.Types.VARCHAR,true);
	map("progname","progname");
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTQ001AData.class+"=$Revision$\n";
}
public long getSeqno() {
	return getLong("seqno");
}
public void setSeqno(long newSeqno) {
	setMember("seqno",newSeqno);
}
public Timestamp getCurtime() {
	return getTimestamp("curtime");
}
public void setCurtime(Timestamp newCurtime) {
	setMember("curtime",newCurtime);
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getProgid() {
	return getString("progid");
}
public void setProgid(String newProgid) {
	setMember("progid",newProgid);
}
public String getAction() {
	return getString("action");
}
public void setAction(String newAction) {
	setMember("action",newAction);
}
public String getRemark() {
	return getString("remark");
}
public void setRemark(String newRemark) {
	setMember("remark",newRemark);
}
public String getProgname() {
	return getString("progname");
}
public void setProgname(String newProgname) {
	setMember("progname",newProgname);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setSeqno(bean.getFieldByName(mapper("seqno")).asLong());
	setCurtime(bean.getFieldByName(mapper("curtime")).asTimestamp());
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setSite(bean.getFieldByName(mapper("site")).asString());
	setProgid(bean.getFieldByName(mapper("progid")).asString());
	setAction(bean.getFieldByName(mapper("action")).asString());
	setRemark(bean.getFieldByName(mapper("remark")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setSeqno(rs.getLong("seqno"));
	setCurtime(rs.getTimestamp("curtime"));
	setUserid(rs.getString("userid"));
	setSite(rs.getString("site"));
	setProgid(rs.getString("progid"));
	setAction(rs.getString("action"));
	setRemark(rs.getString("remark"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("seqno",getSeqno());
	sql.setParameter("curtime",getCurtime());
	sql.setParameter("userid",getUserid());
	sql.setParameter("site",getSite());
	sql.setParameter("progid",getProgid());
	sql.setParameter("action",getAction());
	sql.setParameter("remark",getRemark());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		while(rs.next()) {
			result++;
			//#addon statement if you come back
			//#(85000) programmer code begin;
			//#(85000) programmer code end;
			SFTQ001AData aSFTQ001AData = new SFTQ001AData();
			aSFTQ001AData.fetchResult(rs);
			add(aSFTQ001AData);
			//#addon statement if you come back
			//#(90000) programmer code begin;
			//#(90000) programmer code end;
		}
		//#after scraping result set in too deep
		//#(240000) programmer code begin;
		//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
