package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Tue Jun 27 09:21:57 ICT 2017
 */

import com.fs.bean.misc.*;
import com.fs.bean.util.*;
import com.fs.bean.gener.*;

@SuppressWarnings({"serial","unused"})
public class TProgGroupBean extends BeanSchema {

//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public TProgGroupBean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("groupname",java.sql.Types.VARCHAR,"Group Name");
	addSchema("programid",java.sql.Types.VARCHAR,"Program ID");
	addSchema("parameters",java.sql.Types.VARCHAR,"Parameters");
	addSchema("seqno",java.sql.Types.VARCHAR,"Sequence");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+TProgGroupBean.class+"=$Revision$\n";
}
public String getGroupname() {
	return getMember("groupname");
}
public void setGroupname(String newGroupname) {
	setMember("groupname",newGroupname);
}
public String getProgramid() {
	return getMember("programid");
}
public void setProgramid(String newProgramid) {
	setMember("programid",newProgramid);
}
public String getParameters() {
	return getMember("parameters");
}
public void setParameters(String newParameters) {
	setMember("parameters",newParameters);
}
public String getSeqno() {
	return getMember("seqno");
}
public void setSeqno(String newSeqno) {
	setMember("seqno",newSeqno);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
//#(30000) programmer code end;
}
