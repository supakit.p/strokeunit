package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005CData.java
 * Description: SFTE005CData class implements for handle tuser data base schema.
 * Version: $Revision$
 * Creation date: Wed Jan 03 14:38:21 ICT 2018
 */
/**
 * SFTE005CData class implements for handle tuser data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.TheLibrary;
import com.fs.dev.auth.PasswordLibrary;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused","unchecked"})
public class SFTE005PinData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE005PinData() {
	super();
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("site",getSite());
	sql.setParameter("changeflag", getChangeflag());
	sql.setParameter("pincode", getPincode());
	sql.setParameter("mistakens",getMistakens());
	sql.setParameter("mistakentime",getMistakentime());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
protected void initialize() {
	super.initialize();
	setTable("tuser");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("changeflag",java.sql.Types.VARCHAR);
	addSchema("pincode",java.sql.Types.VARCHAR);
	map("site","site");
	map("userid","userid");
	map("changeflag","changeflag");
	map("pincode","pincode");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	addSchema("oldpincode",java.sql.Types.VARCHAR,true);
	addSchema("confirmpincode",java.sql.Types.VARCHAR,true);
	addSchema("checkflag",java.sql.Types.VARCHAR,true);
	addSchema("cardid",java.sql.Types.VARCHAR,true);
	addSchema("birthday",java.sql.Types.DATE,true);
	map("oldpincode","oldpincode");
	map("confirmpincode","confirmpincode");
	map("checkflag","checkflag");
	map("cardid","cardid");
	map("birthday","birthday");
	addSchema("mistakens",java.sql.Types.TINYINT);
	addSchema("mistakentime",java.sql.Types.BIGINT);
	addSchema("takencount",java.sql.Types.TINYINT,true);
	map("mistakens","mistakens");
	map("mistakentime","mistakentime");
	map("takencount","takencount");
	//#(30000) programmer code end;
}
public int check(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	String pincode = getPincode();
	if(pincode==null || pincode.trim().length()<=0) {
		throw new BeanException("PIN Code is undefined",-13233,global==null?"EN":global.getFsLanguage());
	}	
	int counter = 0;
	String usrpin = null;
	boolean found = false;
	KnSQL knsql = new KnSQL(this);
	if((getCardid()!=null && getCardid().trim().length()>0) && getBirthday()!=null) {
		knsql.append("select site,userid,birthday ");
		knsql.append("from tuserinfo ");
		knsql.append("where cardid = ?cardid ");
		knsql.append("and birthday = ?birthday ");
		knsql.setParameter("birthday", getBirthday());
		knsql.setParameter("cardid", getCardid());
		try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
			if(rs.next()) {
				counter++;
				if(getUserid()==null || getUserid().trim().length()<=0) {
					setUserid(rs.getString("userid"));
				}
			}
		}
	} else {
		knsql.append("select cardid,birthday ");
		knsql.append("from tuserinfo ");
		knsql.append("where userid = ?userid ");
		knsql.setParameter("userid", getUserid());
		try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
			if(rs.next()) {
				counter++;
				setCardid(rs.getString("cardid"));
				setBirthday(rs.getDate("birthday"));
			}
		}		
	}
	if(counter==0) throw new java.sql.SQLException("User profile not found","userid",-13211);
	validateDatas(global==null?"EN":global.getFsLanguage());
	int mistakens = 0;
	
	knsql.clear();
	knsql.append("select pincode,mistakens,mistakentime from tuser where userid=?userid");
	knsql.setParameter("userid",getUserid());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		if(rs.next()) {
			found = true;
			usrpin = rs.getString("pincode");
			mistakens = rs.getInt("mistakens");
		}
	}
	if(!found) throw new BeanException("User does not existed",-8895,global==null?"EN":global.getFsLanguage());
	int takencount = getTakencount();
	if(takencount<=0) takencount = 5;
	if((mistakens+1)>takencount) {
		throw new BeanException("Over Mistaken PIN Code Verify",-13240,global==null?"EN":global.getFsLanguage());
	}
	PasswordLibrary plib = new PasswordLibrary(this);
	if(!pincode.equals(usrpin)) {
		if (pincode.length() > 8) {
			pincode = pincode.substring(0,8);
		}
		if(!plib.checkPassword(pincode, usrpin)) {
			knsql.clear();
			knsql.append("update tuser set mistakens = mistakens + 1, mistakentime = "+System.currentTimeMillis()+" ");
			knsql.append("where userid = ?userid ");
			knsql.setParameter("userid",getUserid());
			knsql.executeUpdate(connection);
			throw new AbandonException("PIN Code Mismatch",-13234,global==null?"EN":global.getFsLanguage());
		}
	}
	return counter;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE005PinData aSFTE005PinData = new SFTE005PinData();
		aSFTE005PinData.fetchResult(rs);
		add(aSFTE005PinData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setSite(rs.getString("site"));
	setChangeflag(rs.getString("changeflag"));
	setPincode(rs.getString("pincode"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	setMistakens(rs.getInt("mistakens"));
	setMistakentime(rs.getLong("mistakentime"));
	//#(60000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE005PinData.class+"=$Revision$\n";
}
public Date getBirthday() {
	return getDate("birthday");
}
public String getCardid() {
	return getString("cardid");
}
public String getChangeflag() {
	return getString("changeflag");
}
public String getCheckflag() {
	return getString("checkflag");
}
public String getConfirmpincode() {
	return getString("confirmpincode");
}
public int getMistakens() {
	return getInt("mistakens");
}
public long getMistakentime() {
	return getLong("mistakentime");
}
public String getOldpincode() {
	return getString("oldpincode");
}
public String getPincode() {
	return getString("pincode");
}
public String getSite() {
	return getString("site");
}
public int getTakencount() {
	return getInt("takencount");
}
public String getUserid() {
	return getString("userid");
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setSite(bean.getFieldByName(mapper("site")).asString());
	setPincode(bean.getFieldByName(mapper("pincode")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	setOldpincode(bean.getFieldByName(mapper("oldpincode")).asString());
	setConfirmpincode(bean.getFieldByName(mapper("confirmpincode")).asString());
	setCheckflag(bean.getFieldByName(mapper("checkflag")).asString());
	setCardid(bean.getFieldByName(mapper("cardid")).asString());
	setBirthday(bean.getFieldByName(mapper("birthday")).asDate());
	if(getCheckflag()==null || getCheckflag().trim().length()<=0) setCheckflag("1");
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public int process(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar,String action) throws Exception {
	if("check".equalsIgnoreCase(action)) {
		return check(connection,centerConnection,globalConnection,transientVar);
	}
	return super.process(connection, centerConnection, globalConnection, transientVar, action);
}
//#(100000) programmer code end;
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public void setBirthday(Date newBirthday) {
	setMember("birthday",newBirthday);
}
public void setCardid(String newCardid) {
	setMember("cardid",newCardid);
}
public void setChangeflag(String newChangeflag) {
	setMember("changeflag",newChangeflag);
}
public void setCheckflag(String newCheckflag) {
	setMember("checkflag",newCheckflag);
}
public void setConfirmpincode(String newConfirmpincode) {
	setMember("confirmpincode",newConfirmpincode);
}
public void setMistakens(int newMistakens) {
	setMember("mistakens",newMistakens);
}
public void setMistakentime(long newMistakentime) {
	setMember("mistakentime",newMistakentime);
}
public void setOldpincode(String newOldpincode) {
	setMember("oldpincode",newOldpincode);
}
public void setPincode(String newPincode) {
	setMember("pincode",newPincode);
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public void setTakencount(int newTakencount) {
	setMember("takencount",newTakencount);
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	int counter = 0;
	String usrpin = null;
	boolean found = false;
	KnSQL knsql = new KnSQL(this);
	if((getCardid()!=null && getCardid().trim().length()>0) && getBirthday()!=null) {
		knsql.append("select site,userid,birthday ");
		knsql.append("from tuserinfo ");
		knsql.append("where cardid = ?cardid ");
		knsql.append("and birthday = ?birthday ");
		knsql.setParameter("birthday", getBirthday());
		knsql.setParameter("cardid", getCardid());
		try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
			if(rs.next()) {
				counter++;
				if(getUserid()==null || getUserid().trim().length()<=0) {
					setUserid(rs.getString("userid"));
				}
			}
		}
	} else {
		knsql.append("select cardid,birthday ");
		knsql.append("from tuserinfo ");
		knsql.append("where userid = ?userid ");
		knsql.setParameter("userid", getUserid());
		try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
			if(rs.next()) {
				counter++;
				setCardid(rs.getString("cardid"));
				setBirthday(rs.getDate("birthday"));
			}
		}		
	}
	if(counter==0) throw new java.sql.SQLException("User profile not found","userid",-13211);
	validateDatas(global==null?"EN":global.getFsLanguage());
	knsql.clear();
	knsql.append("select pincode,mistakens,mistakentime from tuser where userid=?userid");
	knsql.setParameter("userid",getUserid());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		if(rs.next()) {
			found = true;
			usrpin = rs.getString("pincode");
		}
	}
	if(!found) throw new BeanException("User does not existed",-8895,global==null?"EN":global.getFsLanguage());
	PasswordLibrary plib = new PasswordLibrary(this);
	if(!"0".equals(getCheckflag())) {
		if(!getOldpincode().equals(usrpin)) {
			String opincode = getOldpincode();
			if (opincode.length() > 8) {
				opincode = opincode.substring(0,8);
			}
			if(!plib.checkPassword(opincode, usrpin)) {				
				throw new BeanException("PIN Code Mismatch",-13214,global==null?"EN":global.getFsLanguage());
			}
		}	
	}
	String pincode = getPincode();
	if(pincode!=null && pincode.trim().length()>0) {
		if (pincode.length() > 8) {
			pincode = pincode.substring(0,8);
		}
		pincode = plib.encrypt(pincode);
		setPincode(pincode);
	}
	setMistakens(0);
	setMistakentime(0);
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public BeanException validateDatas(String fs_language) {
	if(!"0".equals(getCheckflag())) {
		if(!getPincode().equals(getConfirmpincode())) {
			return new BeanException("Confirm PIN Code Mismatch",-13213,fs_language);
		}
	}
	return TheLibrary.validatePincode(getPincode(),getCardid(),getBirthday(),fs_language);
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
}
