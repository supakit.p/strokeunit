package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE102Data.java
 * Description: SFTE102Data class implements for handle T_Stroke data base schema.
 * Version: $Revision$
 * Creation date: Thu Sep 14 10:22:04 ICT 2017
 */
/**
 * SFTE102Data class implements for handle T_Stroke data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
import org.json.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import com.fs.dev.strok.service.*;
import java.text.DateFormat;
//#(10000) programmer code end;

public class SFTE102Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;

    private int errorCode = 0;
    private String text = "";
    private String stroke_id = "";
    private SirirajUtility sutil = new SirirajUtility();
    private BeanUtility butil = new BeanUtility();
    private String JSON_result = "";
    private ThePatient pt = new ThePatient();
    private Patient p = null;
    private boolean mobileFlag = true;
//#(20000) programmer code end;

    public SFTE102Data() {
        super();
    }

    protected void initialize() {
        super.initialize();
        setTable("T_Stroke");
        addSchema("StrokeId", java.sql.Types.INTEGER);
        addSchema("StrokeMobileId", java.sql.Types.VARCHAR);
        addSchema("CurrentStepId", java.sql.Types.INTEGER);
        addSchema("FinishedFlag", java.sql.Types.CHAR);
        addSchema("HN", java.sql.Types.VARCHAR);
        addSchema("CitizenId", java.sql.Types.BIGINT);
        addSchema("PassportNumber", java.sql.Types.VARCHAR);
        addSchema("PrefixName", java.sql.Types.VARCHAR);
        addSchema("FirstName", java.sql.Types.VARCHAR);
        addSchema("LastName", java.sql.Types.VARCHAR);
        addSchema("Gender", java.sql.Types.VARCHAR);
        addSchema("Age", java.sql.Types.INTEGER);
        addSchema("DateOfBirth", java.sql.Types.DATE);
        addSchema("Nationality", java.sql.Types.VARCHAR);
        addSchema("OpenStrokeById", java.sql.Types.INTEGER);
        addSchema("OpenStrokeTime", java.sql.Types.TIMESTAMP);
        addSchema("HNInputById", java.sql.Types.INTEGER);
        addSchema("HNInputTime", java.sql.Types.TIMESTAMP);
        addSchema("WardId", java.sql.Types.INTEGER);
        addSchema("WardById", java.sql.Types.INTEGER);
        addSchema("WardTime", java.sql.Types.TIMESTAMP);
        addSchema("AdmitWardById", java.sql.Types.INTEGER);
        addSchema("AdmitWardTime", java.sql.Types.TIMESTAMP);
        addSchema("CompletedFlag", java.sql.Types.CHAR);
        addSchema("CompletedById", java.sql.Types.INTEGER);
        addSchema("CompletedTime", java.sql.Types.TIMESTAMP);
        addSchema("Remark", java.sql.Types.VARCHAR);
        addSchema("LastUpdateById", java.sql.Types.INTEGER);
        addSchema("LastUpdateDate", java.sql.Types.TIMESTAMP);
        map("StrokeMobileId", "stroke_mobile_id");
        map("CitizenId", "citizen_id");
        map("Age", "age");
        map("HN", "hn");
        //#intialize how deep is your love
        //#(30000) programmer code begin;
        /*
	addSchema("api",java.sql.Types.VARCHAR,true);
	addSchema("client_time",java.sql.Types.TIMESTAMP,true);
	addSchema("device_id",java.sql.Types.VARCHAR,true);
	addSchema("sap_no",java.sql.Types.VARCHAR,true);
	addSchema("flag_data",java.sql.Types.VARCHAR,true);
	addSchema("part_screen",java.sql.Types.VARCHAR,true);
	addSchema("screen",java.sql.Types.VARCHAR,true);
	addSchema("tab",java.sql.Types.VARCHAR,true);
	addSchema("ref_id",java.sql.Types.VARCHAR,true);
	addSchema("authority_id",java.sql.Types.VARCHAR,true);

	map("Firstname","name");
	map("api","api");
	map("client_time","client_time");
	map("device_id","device_id");
	map("sap_no","sap_no");
	map("flag_data","flag_data");
	map("screen","screen");
	map("tab","tab");
	map("part_screen","part_screen");
	map("ref_id","ref_id");
	map("authority_id","authority_id");
         */
        addSchema("data", java.sql.Types.VARCHAR, true);
        map("data", "data");
        //#(30000) programmer code end;
    }

    public String fetchVersion() {
        return super.fetchVersion() + SFTE102Data.class + "=$Revision$\n";
    }

    public int getStrokeid() {
        return getInt("StrokeId");
    }

    public void setStrokeid(int newStrokeid) {
        setMember("StrokeId", newStrokeid);
    }

    public String getStrokemobileid() {
        return getString("StrokeMobileId");
    }

    public void setStrokemobileid(String newStrokemobileid) {
        setMember("StrokeMobileId", newStrokemobileid);
    }

    public int getCurrentstepid() {
        return getInt("CurrentStepId");
    }

    public void setCurrentstepid(int newCurrentstepid) {
        setMember("CurrentStepId", newCurrentstepid);
    }

    public String getFinishedflag() {
        return getString("FinishedFlag");
    }

    public void setFinishedflag(String newFinishedflag) {
        setMember("FinishedFlag", newFinishedflag);
    }

    public String getHn() {
        return getString("HN");
    }

    public void setHn(String newHn) {
        setMember("HN", newHn);
    }

    public long getCitizenid() {
        return getLong("CitizenId");
    }

    public void setCitizenid(long newCitizenid) {
        setMember("CitizenId", newCitizenid);
    }

    public String getPassportnumber() {
        return getString("PassportNumber");
    }

    public void setPassportnumber(String newPassportnumber) {
        setMember("PassportNumber", newPassportnumber);
    }

    public String getPrefixname() {
        return getString("PrefixName");
    }

    public void setPrefixname(String newPrefixname) {
        setMember("PrefixName", newPrefixname);
    }

    public String getFirstname() {
        return getString("FirstName");
    }

    public void setFirstname(String newFirstname) {
        setMember("FirstName", newFirstname);
    }

    public String getLastname() {
        return getString("LastName");
    }

    public void setLastname(String newLastname) {
        setMember("LastName", newLastname);
    }

    public String getGender() {
        return getString("Gender");
    }

    public void setGender(String newGender) {
        setMember("Gender", newGender);
    }

    public int getAge() {
        return getInt("Age");
    }

    public void setAge(int newAge) {
        setMember("Age", newAge);
    }

    public Date getDateofbirth() {
        return getDate("DateOfBirth");
    }

    public void setDateofbirth(Date newDateofbirth) {
        setMember("DateOfBirth", newDateofbirth);
    }

    public String getNationality() {
        return getString("Nationality");
    }

    public void setNationality(String newNationality) {
        setMember("Nationality", newNationality);
    }

    public int getOpenstrokebyid() {
        return getInt("OpenStrokeById");
    }

    public void setOpenstrokebyid(int newOpenstrokebyid) {
        setMember("OpenStrokeById", newOpenstrokebyid);
    }

    public Timestamp getOpenstroketime() {
        return getTimestamp("OpenStrokeTime");
    }

    public void setOpenstroketime(Timestamp newOpenstroketime) {
        setMember("OpenStrokeTime", newOpenstroketime);
    }

    public int getHninputbyid() {
        return getInt("HNInputById");
    }

    public void setHninputbyid(int newHninputbyid) {
        setMember("HNInputById", newHninputbyid);
    }

    public Timestamp getHninputtime() {
        return getTimestamp("HNInputTime");
    }

    public void setHninputtime(Timestamp newHninputtime) {
        setMember("HNInputTime", newHninputtime);
    }

    public int getWardid() {
        return getInt("WardId");
    }

    public void setWardid(int newWardid) {
        setMember("WardId", newWardid);
    }

    public int getWardbyid() {
        return getInt("WardById");
    }

    public void setWardbyid(int newWardbyid) {
        setMember("WardById", newWardbyid);
    }

    public Timestamp getWardtime() {
        return getTimestamp("WardTime");
    }

    public void setWardtime(Timestamp newWardtime) {
        setMember("WardTime", newWardtime);
    }

    public int getAdmitwardbyid() {
        return getInt("AdmitWardById");
    }

    public void setAdmitwardbyid(int newAdmitwardbyid) {
        setMember("AdmitWardById", newAdmitwardbyid);
    }

    public Timestamp getAdmitwardtime() {
        return getTimestamp("AdmitWardTime");
    }

    public void setAdmitwardtime(Timestamp newAdmitwardtime) {
        setMember("AdmitWardTime", newAdmitwardtime);
    }

    public String getCompletedflag() {
        return getString("CompletedFlag");
    }

    public void setCompletedflag(String newCompletedflag) {
        setMember("CompletedFlag", newCompletedflag);
    }

    public int getCompletedbyid() {
        return getInt("CompletedById");
    }

    public void setCompletedbyid(int newCompletedbyid) {
        setMember("CompletedById", newCompletedbyid);
    }

    public Timestamp getCompletedtime() {
        return getTimestamp("CompletedTime");
    }

    public void setCompletedtime(Timestamp newCompletedtime) {
        setMember("CompletedTime", newCompletedtime);
    }

    public String getRemark() {
        return getString("Remark");
    }

    public void setRemark(String newRemark) {
        setMember("Remark", newRemark);
    }

    public int getLastupdatebyid() {
        return getInt("LastUpdateById");
    }

    public void setLastupdatebyid(int newLastupdatebyid) {
        setMember("LastUpdateById", newLastupdatebyid);
    }

    public Timestamp getLastupdatedate() {
        return getTimestamp("LastUpdateDate");
    }

    public void setLastupdatedate(Timestamp newLastupdatedate) {
        setMember("LastUpdateDate", newLastupdatedate);
    }

    public boolean obtain(BeanSchemaInterface bean) throws Exception {
        if (bean == null) {
            return super.obtain(bean);
        }
        setStrokeid(bean.getFieldByName(mapper("StrokeId")).asInt());
        setStrokemobileid(bean.getFieldByName(mapper("StrokeMobileId")).asString());
        setCurrentstepid(bean.getFieldByName(mapper("CurrentStepId")).asInt());
        setFinishedflag(bean.getFieldByName(mapper("FinishedFlag")).asString());
        setHn(bean.getFieldByName(mapper("HN")).asString());
        setCitizenid(bean.getFieldByName(mapper("CitizenId")).asLong());
        setPassportnumber(bean.getFieldByName(mapper("PassportNumber")).asString());
        setPrefixname(bean.getFieldByName(mapper("PrefixName")).asString());
        setFirstname(bean.getFieldByName(mapper("FirstName")).asString());
        setLastname(bean.getFieldByName(mapper("LastName")).asString());
        setGender(bean.getFieldByName(mapper("Gender")).asString());
        setAge(bean.getFieldByName(mapper("Age")).asInt());
        setDateofbirth(bean.getFieldByName(mapper("DateOfBirth")).asDate());
        setNationality(bean.getFieldByName(mapper("Nationality")).asString());
        setOpenstrokebyid(bean.getFieldByName(mapper("OpenStrokeById")).asInt());
        setOpenstroketime(bean.getFieldByName(mapper("OpenStrokeTime")).asTimestamp());
        setHninputbyid(bean.getFieldByName(mapper("HNInputById")).asInt());
        setHninputtime(bean.getFieldByName(mapper("HNInputTime")).asTimestamp());
        setWardid(bean.getFieldByName(mapper("WardId")).asInt());
        setWardbyid(bean.getFieldByName(mapper("WardById")).asInt());
        setWardtime(bean.getFieldByName(mapper("WardTime")).asTimestamp());
        setAdmitwardbyid(bean.getFieldByName(mapper("AdmitWardById")).asInt());
        setAdmitwardtime(bean.getFieldByName(mapper("AdmitWardTime")).asTimestamp());
        setCompletedflag(bean.getFieldByName(mapper("CompletedFlag")).asString());
        setCompletedbyid(bean.getFieldByName(mapper("CompletedById")).asInt());
        setCompletedtime(bean.getFieldByName(mapper("CompletedTime")).asTimestamp());
        setRemark(bean.getFieldByName(mapper("Remark")).asString());
        setLastupdatebyid(bean.getFieldByName(mapper("LastUpdateById")).asInt());
        setLastupdatedate(bean.getFieldByName(mapper("LastUpdateDate")).asTimestamp());
        //#obtain it perfect moment
        //#(40000) programmer code begin;
        setApi(bean.getFieldByName(mapper("api")).asString());
        setClient_time(bean.getFieldByName(mapper("client_time")).asTimestamp());
        setDevice_id(bean.getFieldByName(mapper("device_id")).asString());
        setFlag_data(bean.getFieldByName(mapper("flag_data")).asString());
        setSap_no(bean.getFieldByName(mapper("sap_no")).asString());
        setScreen(bean.getFieldByName(mapper("screen")).asString());
        setTab(bean.getFieldByName(mapper("tab")).asString());
        setPart_screen(bean.getFieldByName(mapper("part_screen")).asString());
        setRef_id(bean.getFieldByName(mapper("ref_id")).asString());
        setAuthority_id(bean.getFieldByName(mapper("authority_id")).asString());
        setData(bean.getFieldByName(mapper("data")).asString());
        //#(40000) programmer code end;
        return super.obtain(bean);
    }

    public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
        super.fetchResult(rs);
        setStrokeid(rs.getInt("StrokeId"));
        setStrokemobileid(rs.getString("StrokeMobileId"));
        setCurrentstepid(rs.getInt("CurrentStepId"));
        setFinishedflag(rs.getString("FinishedFlag"));
        setHn(rs.getString("HN"));
        setCitizenid(rs.getLong("CitizenId"));
        setPassportnumber(rs.getString("PassportNumber"));
        setPrefixname(rs.getString("PrefixName"));
        setFirstname(rs.getString("FirstName"));
        setLastname(rs.getString("LastName"));
        setGender(rs.getString("Gender"));
        setAge(rs.getInt("Age"));
        setDateofbirth(rs.getDate("DateOfBirth"));
        setNationality(rs.getString("Nationality"));
        setOpenstrokebyid(rs.getInt("OpenStrokeById"));
        setOpenstroketime(rs.getTimestamp("OpenStrokeTime"));
        setHninputbyid(rs.getInt("HNInputById"));
        setHninputtime(rs.getTimestamp("HNInputTime"));
        setWardid(rs.getInt("WardId"));
        setWardbyid(rs.getInt("WardById"));
        setWardtime(rs.getTimestamp("WardTime"));
        setAdmitwardbyid(rs.getInt("AdmitWardById"));
        setAdmitwardtime(rs.getTimestamp("AdmitWardTime"));
        setCompletedflag(rs.getString("CompletedFlag"));
        setCompletedbyid(rs.getInt("CompletedById"));
        setCompletedtime(rs.getTimestamp("CompletedTime"));
        setRemark(rs.getString("Remark"));
        setLastupdatebyid(rs.getInt("LastUpdateById"));
        setLastupdatedate(rs.getTimestamp("LastUpdateDate"));
        //#fetching other result desire
        //#(60000) programmer code begin;
        //#(60000) programmer code end;
    }

    protected void assignParameters(ExecuteStatement sql) throws Exception {
        if (sql == null) {
            return;
        }
        //#Everything I do, I do it for you
        //#(75000) programmer code begin;
        //#(75000) programmer code end;
        sql.setParameter("StrokeId", getStrokeid());
        sql.setParameter("StrokeMobileId", getStrokemobileid());
        sql.setParameter("CurrentStepId", getCurrentstepid());
        sql.setParameter("FinishedFlag", getFinishedflag());
        sql.setParameter("HN", getHn());
        sql.setParameter("CitizenId", getCitizenid());
        sql.setParameter("PassportNumber", getPassportnumber());
        sql.setParameter("PrefixName", getPrefixname());
        sql.setParameter("FirstName", getFirstname());
        sql.setParameter("LastName", getLastname());
        sql.setParameter("Gender", getGender());
        sql.setParameter("Age", getAge());
        sql.setParameter("DateOfBirth", getDateofbirth());
        sql.setParameter("Nationality", getNationality());
        sql.setParameter("OpenStrokeById", getOpenstrokebyid());
        sql.setParameter("OpenStrokeTime", getOpenstroketime());
        sql.setParameter("HNInputById", getHninputbyid());
        sql.setParameter("HNInputTime", getHninputtime());
        sql.setParameter("WardId", getWardid());
        sql.setParameter("WardById", getWardbyid());
        sql.setParameter("WardTime", getWardtime());
        sql.setParameter("AdmitWardById", getAdmitwardbyid());
        sql.setParameter("AdmitWardTime", getAdmitwardtime());
        sql.setParameter("CompletedFlag", getCompletedflag());
        sql.setParameter("CompletedById", getCompletedbyid());
        sql.setParameter("CompletedTime", getCompletedtime());
        sql.setParameter("Remark", getRemark());
        sql.setParameter("LastUpdateById", getLastupdatebyid());
        sql.setParameter("LastUpdateDate", getLastupdatedate());
        //#I'm gonna be around you
        //#(77000) programmer code begin;
        //#(77000) programmer code end;
    }

    public int insert(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        //#begin with insert statement going on
        //#(250000) programmer code begin;
        //#(250000) programmer code end;
        ExecuteStatement sql = createQueryForInsert(connection);
        //#another modification keep on loving you
        //#(110000) programmer code begin;
        //#(110000) programmer code end;
        assignParameters(sql);
        //#assigned parameter always on my mind
        //#(120000) programmer code begin;
        //#(120000) programmer code end;
        return sql.executeUpdate(connection);
        //#ending with insert statement here
        //#(260000) programmer code begin;
        //#(260000) programmer code end;
    }

    public int delete(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        setKeyField("StrokeId");
        //#begin with delete statement going on
        //#(270000) programmer code begin;
        //#(270000) programmer code end;
        ExecuteStatement sql = createQueryForDelete(connection);
        //#any delete statement more than that
        //#(130000) programmer code begin;
        //#(130000) programmer code end;
        sql.setParameter("StrokeId", getStrokeid());
        //#assigned parameters time after time
        //#(140000) programmer code begin;
        //#(140000) programmer code end;
        return sql.executeUpdate(connection);
        //#ending with delete statement here
        //#(280000) programmer code begin;
        //#(280000) programmer code end;
    }

    public int update(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        setKeyField("StrokeId");
        //#begin with update statement going on
        //#(290000) programmer code begin;
        /*
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
         */

        String strokeid = null;
        String hn = null;
        String citizenid = null;
        String patientsRes = "";
        boolean errorfg = false;
        String submitDate = null;
        String strokeMobileId = null;
        java.util.Map map = new java.util.Hashtable();
        java.util.Vector ivec = new java.util.Vector();
        String desc = "";
        java.sql.Timestamp sysdate = butil.getCurrentTimestamp();
        JSONObject JObject = null;
        try {
            if (getData() == null || getData().equalsIgnoreCase("")) {
                errorCode = 12;
                throw new Exception("body is undefined");
            }
            if (!sutil.isJSONValid(getData())) { //validate JSON format
                throw new Exception("SERR:12");
            }
            try {
                JObject = new JSONObject(getData());
            } catch (Exception ex) {
                throw new Exception("SERR:12");
            }
            try {
                strokeid = JObject.getString("strokeId");
            } catch (Exception ex) {
            }
            try {
                hn = JObject.getString("hn");
            } catch (Exception ex) {
            }
            try {
                citizenid = JObject.getString("citizenId");
            } catch (Exception ex) {
            }
            try {
                submitDate = JObject.getString("submitDate");
            } catch (Exception ex) {
            }
            try {
                strokeMobileId = JObject.getString("strokeMobileId");
                if (strokeMobileId.isEmpty()) {
                    mobileFlag = false;
                }
            } catch (Exception ex) {
                mobileFlag = false;
            }
            String error_desc = "";

            /*try {
			// pass StrokeMobileId for get StrokeId
			setStrokeid(sutil.getStrokeId(connection,getStrokemobileid()));
		}catch (Exception ex) {
			errorfg = true;
			text = ex.getMessage();
			stroke_id = getStrokemobileid();
			errorCode = 10;
		}*/
            //if(!errorfg){
            //System.out.println("HN ************** "+hn);
            //System.out.println("citizenId ******* "+citizenid);
            try {
                String flagdata = getFlag_data();
                String param = "";

                if (hn != null && !"".equalsIgnoreCase(hn)) {   //  call api to siriraj server
                    p = pt.getPatientByHN(hn);
                    if (p == null) {
                        // return null if it can't connect api
                        errorfg = true;
                        text = "unsuccess";
                        errorCode = 6;
                    }
                } else if (citizenid != null && !"".equalsIgnoreCase(citizenid)) {
                    p = pt.getPatientByID(citizenid);
                    if (p == null) {
                        // return null if it can't connect api
                        errorfg = true;
                        text = "unsuccess";
                        errorCode = 6;
                    }
                }
                /*else if(flagdata !=null && "2".equals(flagdata)){
					result =updataT_Stroke(connection,"");
				}


			if("1".equals(flagdata) || "0".equals(flagdata)){
				String sirirajstatus = Long.toString(p.getStatus());

				if("0".equals(sirirajstatus)){
					result =updataT_Stroke(connection,p);

				}else if("1".equals(sirirajstatus)){
					errorfg = true;
					text = "unsuccess";
					errorCode = 16;
				}
			}
			/*	if(result > 0){
					map.put("status","Y");
					map.put("api",sutil.chkNotNull(getApi()));
					map.put("screen",sutil.chkNotNull(getScreen()));
					map.put("tab",sutil.chkNotNull(getTab()));
					map.put("part_screen",sutil.chkNotNull(getPart_screen()));
					map.put("error_flag","N");
					map.put("error_code",0);
					map.put("error_desc",sutil.getErrorDesc(connection,0));
					map.put("server_time",getClient_time());
					map.put("device_id",sutil.chkNotNull(getDevice_id()));
					map.put("strokeid",getStrokeid());
					map.put("ref_id",sutil.chkNotNull(getRef_id()));
					map.put("desc","");
					java.util.Vector ivec = new java.util.Vector();
					java.util.Map imap = new java.util.Hashtable();
					imap.put("name",sutil.chkNotNull(getFirstname()));
					imap.put("surname",sutil.chkNotNull(getLastname()));
					imap.put("age",getAge());
					imap.put("sex",sutil.chkNotNull(getGender()));
					ivec.addElement(imap);
					map.put("body",ivec);

					map.put("status","Y");
					map.put("api",sutil.chkNotNull(getApi()));
					map.put("screen",sutil.chkNotNull(getScreen()));
					map.put("tab",sutil.chkNotNull(getTab()));
					map.put("part_screen",sutil.chkNotNull(getPart_screen()));
					map.put("error_flag","N");
					map.put("error_code",0);
					map.put("error_desc",sutil.getErrorDesc(connection,0));
					map.put("server_time",getClient_time());
					map.put("device_id",sutil.chkNotNull(getDevice_id()));
					map.put("strokeid",sutil.chkNotNull(getStrokemobileid()));
					map.put("ref_id",sutil.chkNotNull(getRef_id()));
					map.put("desc","");
                 */
                //System.out.println("P ====================== "+p);
                //System.out.println("P.getStatus() ========== "+p.getStatus());
                //System.out.println("P.isSuccess() ========== "+p.isSuccess());

                java.util.Map imap = new java.util.Hashtable();
                if (p != null) {
                    if (p.isSuccess()) {

                        String birthdate = p.getDateOfBirth();

                        java.util.Date bdate = p.getBirthDay();
                        java.sql.Date bddate = new java.sql.Date(bdate.getTime());

                        int getYearBdate = butil.getYearFromYMD(bddate.toString());
                        String currentdate = butil.formatDateTime(butil.getCurrentDate(), "yyyyMMdd");
                        int getYearCurdate = butil.getYearFromYMD(currentdate);
//                        int age = getYearCurdate - getYearBdate;

                        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                        //getBirthDay return month +1 ******************
                        bddate.setMonth(bddate.getMonth() - 1);
                        int birthDate = Integer.parseInt(formatter.format(bddate));
                        int currentDate = Integer.parseInt(formatter.format(butil.getCurrentDate()));
                        int age = (currentDate - birthDate) / 10000;

                        setAge(age);
                        imap.put("hn", sutil.chkNotNull(p.getHN()));
                        imap.put("citizenId", sutil.chkNotNull(p.getCitizenId()));
                        imap.put("passportNo", sutil.chkNotNull(p.getPassportNo()));
                        imap.put("prefixName", sutil.chkNotNull(p.getPrefixName()));
                        imap.put("firstName", sutil.chkNotNull(p.getFirstName()));
                        imap.put("lastName", sutil.chkNotNull(p.getLastName()));
                        imap.put("gender", sutil.chkNotNull(p.getGender()));
                        imap.put("age", age);
                        imap.put("dateOfBirth", sutil.chkNotNull(birthdate));
                        imap.put("nationality", sutil.chkNotNull(p.getNationality()));
                        ivec.addElement(imap);
                        patientsRes = p.toJSONString();
                    } else {
                        errorCode = 16;
                    }
                } else {
                    errorCode = 6;
                }

                error_desc = sutil.getErrorDesc(connection, errorCode);
                insertApiPatientsLog(connection, strokeid, hn, citizenid, error_desc + " : " + patientsRes, submitDate);
                if (ivec.size() > 0) {
                    map.put("body", ivec);
                }
                map.put("error_code", errorCode);
                map.put("error_desc", error_desc);
                map.put("desc", text);
            } catch (Exception ex) {
                connection.rollback();
                errorfg = true;
                text = ex.getMessage();
                errorCode = 999;
                error_desc = sutil.getErrorDesc(connection, errorCode);
                map.put("body", ivec);
                map.put("error_code", errorCode);
                map.put("error_desc", error_desc);
                map.put("desc", text);
            }

            //}
        } catch (Exception ex) {
            connection.rollback();
            //System.out.println("error=========="+ex.getMessage());
            if (ex.getMessage() != null && ex.getMessage().indexOf("SERR") >= 0) {
                String[] err = ex.getMessage().split(":");
                try {
                    errorCode = Integer.parseInt(err[1]);
                } catch (Exception e) {
                    errorCode = 999;
                }
            }
        }

        map.put("server_time", sysdate);
        map.put("error_code", errorCode);
        map.put("error_desc", sutil.chkNotNull(sutil.getErrorDesc(connection, errorCode)));
        map.put("desc", desc);

        String JSON_str = JSONUtility.parseJSON(map, null, true);
        global.setFsMessage(JSON_str);
        //System.out.println("JSON_Str============="+JSON_str);

        //#(160000) programmer code end;
        return 0;
        //#ending with delete statement going on
        //#(300000) programmer code begin;
        //#(300000) programmer code end;
    }

    public int retrieve(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        setKeyField("StrokeId");
        //#statement shake it up retrieving
        //#(165000) programmer code begin;
        //#(165000) programmer code end;
        ExecuteStatement sql = createQueryForRetrieve(connection);
        //#any retrieve statement too close
        //#(170000) programmer code begin;
        //#(170000) programmer code end;
        //#assigned parameters make it happen
        //#(180000) programmer code begin;
        //#(180000) programmer code end;
        int result = 0;
        java.sql.ResultSet rs = sql.executeQuery(connection);
        if (rs.next()) {
            result++;
            fetchResult(rs);
            //#any result fetching fool again
            //#(220000) programmer code begin;
            //#(220000) programmer code end;
        }
        //#after fetching result set lovin each day
        //#(230000) programmer code begin;
        //#(230000) programmer code end;
        close(rs);
        return result;
    }

    public boolean validateInsert(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        //#say you say me
        //#(195000) programmer code begin;
        //#(195000) programmer code end;
        if (getStrokeid() < 0) {
            throw new java.sql.SQLException("Strokeid is invalid", "StrokeId", -2008);
        }
        //#valid statement best in me
        //#(190000) programmer code begin;
        //#(190000) programmer code end;
        return true;
    }

    public boolean validateUpdate(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        //#valid update statement to be with you
        //#(200000) programmer code begin;
        //#(200000) programmer code end;
        return true;
    }

    public boolean validateDelete(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        //#valid delete statement here for you
        //#(210000) programmer code begin;
        //#(210000) programmer code end;
        return true;
    }

    public int collect(java.sql.Connection connection, java.sql.Connection centerConnection, java.sql.Connection globalConnection, java.util.Map transientVar) throws Exception {
        removeAlls();
        PermissionBean fsPermission = global == null ? null : global.getPermission();
        //#here we go to the collection
        //#(65000) programmer code begin;
        //#(65000) programmer code end;
        ExecuteStatement sql = createQueryForCollect(connection);
        //#any collect statement would you be happier
        //#(70000) programmer code begin;
        //#(70000) programmer code end;
        //#assigned parameters this temptation
        //#(80000) programmer code begin;
        //#(80000) programmer code end;
        int result = 0;
        java.sql.ResultSet rs = sql.executeQuery(connection);
        while (rs.next()) {
            result++;
            //#addon statement if you come back
            //#(85000) programmer code begin;
            //#(85000) programmer code end;
            SFTE102Data aSFTE102Data = new SFTE102Data();
            aSFTE102Data.fetchResult(rs);
            add(aSFTE102Data);
            //#addon statement if you come back
            //#(90000) programmer code begin;
            //#(90000) programmer code end;
        }
        //#after scraping result set in too deep
        //#(240000) programmer code begin;
        //#(240000) programmer code end;
        close(rs);
        return result;
    }
//#another methods defined drive you crazy
//#(100000) programmer code begin;

    public Timestamp getClient_time() {
        return getTimestamp("client_time");
    }

    public void setClient_time(Timestamp newClient_time) {
        setMember("client_time", newClient_time);
    }

    public void setApi(String newApi) {
        setMember("api", newApi);
    }

    public String getApi() {
        return getString("api");
    }

    public void setDevice_id(String newDeviceid) {
        setMember("device_id", newDeviceid);
    }

    public String getDevice_id() {
        return getString("device_id");
    }

    public void setSap_no(String newSapno) {
        setMember("sap_no", newSapno);
    }

    public String getSap_no() {
        return getString("sap_no");
    }

    public void setFlag_data(String newflagdata) {
        setMember("flag_data", newflagdata);
    }

    public String getFlag_data() {
        return getString("flag_data");
    }

    public String getScreen() {
        return getString("screen");
    }

    public void setScreen(String newScreen) {
        setMember("screen", newScreen);
    }

    public String getTab() {
        return getString("tab");
    }

    public void setTab(String newTab) {
        setMember("tab", newTab);
    }

    public String getPart_screen() {
        return getString("part_screen");
    }

    public void setPart_screen(String newPart_screen) {
        setMember("part_screen", newPart_screen);
    }

    public String getRef_id() {
        return getString("ref_id");
    }

    public void setRef_id(String newRef_id) {
        setMember("ref_id", newRef_id);
    }

    public String getAuthority_id() {
        return getString("authority_Id");
    }

    public void setAuthority_id(String newAuthority_id) {
        setMember("authority_id", newAuthority_id);
    }

    public int updataT_Stroke(java.sql.Connection connection, String JSON_result) throws Exception {
        int result = 0;
        if (JSON_result.length() > 0) {
            JSONObject jsondata = new JSONObject(JSON_result);
            String birthdate = jsondata.getString("DateOfBirth");
            int getYearBdate = butil.getYearFromYMD(birthdate);
            String currentdate = butil.formatDateTime(butil.getCurrentDate(), "yyyyMMdd");
            int getYearCurdate = butil.getYearFromYMD(currentdate);
            int age = (getYearCurdate + 543) - getYearBdate;

            //System.out.println("=========getYearCurdate+543============="+getYearCurdate+543);
            //System.out.println("========getYearBdate=================="+getYearBdate);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            java.util.Date bDate = sdf.parse(birthdate);
            java.sql.Date birthday = new java.sql.Date(bDate.getTime());
            setDateofbirth(birthday);
            setHn(jsondata.getString("HN"));
            setCitizenid(jsondata.getLong("CitizenId"));
            setPrefixname(jsondata.getString("PrefixName"));
            setFirstname(jsondata.getString("FirstName"));
            setLastname(jsondata.getString("LastName"));
            setGender(jsondata.getString("Gender"));
            setAge(getAge());

            //System.out.println("age============"+getAge());
        }
        java.util.Vector<java.util.Vector> vec = sutil.getOldData(connection, "t_stroke", "strokeid", getStrokemobileid());

        KnSQL sql = new KnSQL();
        sql.append(" update T_stroke set  StrokeMobileId =?strokemobileid, \n");
        sql.append(" dateOfBirth=?dateofbirth, HN=?hn, \n");
        sql.append(" CitizenId = ?citizenid, PrefixName=?prefixname, \n");
        sql.append(" FirstName = ?firstname, LastName=?lastname, \n");
        sql.append(" Gender =?gender, Age=?age,\n");
        sql.append(" HNInputById='-99', HNInputTime=?hninputtime, \n");
        sql.append(" LastUpdateById ='-99', LastUpdateDate=?lastupdatedate \n");
        sql.append(" where StrokeId = ?strokeid  \n");

        sql.setParameter("strokemobileid", sutil.chkNotNull(getStrokemobileid()));
        sql.setParameter("dateofbirth", getDateofbirth());
        sql.setParameter("hn", sutil.chkNotNull(getHn()));
        sql.setParameter("citizenid", getCitizenid());
        sql.setParameter("prefixname", sutil.chkNotNull(getPrefixname()));
        sql.setParameter("firstname", sutil.chkNotNull(getFirstname()));
        sql.setParameter("lastname", sutil.chkNotNull(getLastname()));
        sql.setParameter("gender", sutil.chkNotNull(getGender()));
        sql.setParameter("age", getAge());
        sql.setParameter("hninputtime", getClient_time());
        sql.setParameter("lastupdatedate", getClient_time());
        sql.setParameter("strokeid", sutil.chkNotNull(getStrokemobileid()));
        result = sql.executeUpdate(connection);

        if (result > 0) {
            sutil.compareData(connection, vec, "t_stroke", "strokeid", getStrokemobileid(), getStrokemobileid(), getAuthority_id(), getClient_time());
        }
        return result;

    }

    public int updataT_Stroke(java.sql.Connection connection, Patient p) throws Exception {
        int result = 0;
        String birthdate = p.getDateOfBirth();
        java.util.Date bdate = p.getBirthDay();
        java.sql.Date bddate = new java.sql.Date(bdate.getTime());
        int getYearBdate = butil.getYearFromYMD(bddate.toString());
        String currentdate = butil.formatDateTime(butil.getCurrentDate(), "yyyyMMdd");
        int getYearCurdate = butil.getYearFromYMD(currentdate);
        int age = getYearCurdate - getYearBdate;

        //System.out.println("=========getYearCurdate============="+getYearCurdate);
        //System.out.println("========getYearBdate=================="+getYearBdate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        java.util.Date bDate = sdf.parse(birthdate);
        java.sql.Date birthday = new java.sql.Date(bDate.getTime());
        setDateofbirth(birthday);
        setHn(p.getHN());
        setCitizenid(Long.valueOf(p.getCitizenId()));
        setPrefixname(p.getPrefixName());
        setFirstname(p.getFirstName());
        setLastname(p.getLastName());
        setGender(p.getGender());
        setNationality(p.getNationality());
        //setAge(getAge());
        setAge(age);

        //System.out.println("age============"+getAge());
        java.util.Vector<java.util.Vector> vec = sutil.getOldData(connection, "t_stroke", "strokeid", getStrokemobileid());

        KnSQL sql = new KnSQL();
        sql.append(" update T_stroke set  StrokeMobileId =?strokemobileid, \n");
        sql.append(" dateOfBirth=?dateofbirth, HN=?hn, \n");
        sql.append(" CitizenId = ?citizenid, PrefixName=?prefixname, \n");
        sql.append(" FirstName = ?firstname, LastName=?lastname, \n");
        sql.append(" Gender =?gender, Age=?age,\n");
        sql.append(" HNInputById='-99', HNInputTime=?hninputtime, \n");
        sql.append(" LastUpdateById ='-99', LastUpdateDate=?lastupdatedate \n");
        sql.append(" ,nationality = ?nationality \n");
        sql.append(" where StrokeId = ?strokeid  \n");

        sql.setParameter("strokemobileid", sutil.chkNotNull(getStrokemobileid()));
        sql.setParameter("dateofbirth", getDateofbirth());
        sql.setParameter("hn", sutil.chkNotNull(getHn()));
        sql.setParameter("citizenid", getCitizenid());
        sql.setParameter("prefixname", butil.MS874ToUnicode(sutil.chkNotNull(getPrefixname())));
        sql.setParameter("firstname", butil.MS874ToUnicode(sutil.chkNotNull(getFirstname())));
        sql.setParameter("lastname", butil.MS874ToUnicode(sutil.chkNotNull(getLastname())));
        sql.setParameter("gender", butil.MS874ToUnicode(sutil.chkNotNull(getGender())));
        sql.setParameter("nationality", butil.MS874ToUnicode(sutil.chkNotNull(p.getNationality())));
        sql.setParameter("age", getAge());
        sql.setParameter("hninputtime", getClient_time());
        sql.setParameter("lastupdatedate", getClient_time());
        sql.setParameter("strokeid", sutil.chkNotNull(getStrokemobileid()));
        result = sql.executeUpdate(connection);

        if (result > 0) {
            sutil.compareData(connection, vec, "t_stroke", "strokeid", getStrokemobileid(), getStrokemobileid(), getAuthority_id(), getClient_time());
        }
        return result;

    }

    public String getData() {
        return getString("data");
    }

    public void setData(String newData) {
        setMember("data", newData);
    }

    public void insertApiPatientsLog(java.sql.Connection connection, String strokeid, String hn, String citizenid, String remark, String submitDate) throws Exception {
        //System.out.println(" -------------- insertApiPatientsLog -------------- ");
        //System.out.println(" -------------- hn "+hn);
        //System.out.println(" ------- citizenid "+citizenid);
        if (citizenid != null && !"".equalsIgnoreCase(citizenid)) {
            p = pt.getPatientByID(citizenid);
        } else if (hn != null && !"".equalsIgnoreCase(hn)) {
            p = pt.getPatientByHN(hn);
        }
        KnSQL sql = new KnSQL();
        sql.append(" insert into api_PatientsLog \n");
        //System.out.println("p.isSuccess() --------------" +p.isSuccess());
        if (p.isSuccess()) {
            sql.append(" (StrokeId,ActionFrom,StatusCode,StatusDescription,HN,CitizenId,PassportNo,PrefixName,FirstName,LastName,Age,Gender,DateOfBirth,Nationality,Remark,SubmitDate) \n");
            sql.append(" values(?strokeId,?actionFrom,?statusCode,?statusDescription,?hn,?citizenId,?passportNo,?prefixName,?firstName,?lastName,?age,?gender,?dateOfBirth,?nationality,?remark,?submitDate)");
            sql.setParameter("strokeId", strokeid);
            if (mobileFlag) {
                sql.setParameter("actionFrom", "mobile");
            } else {
                sql.setParameter("actionFrom", "web");
            }
            sql.setParameter("statusCode", p.getStatus());
            sql.setParameter("statusDescription", p.getStatusDescription());
            sql.setParameter("hn", p.getHN());
            sql.setParameter("citizenId", p.getCitizenId());
            sql.setParameter("passportNo", p.getPassportNo());
            sql.setParameter("prefixName", butil.MS874ToUnicode(p.getPrefixName()));
            sql.setParameter("firstName", butil.MS874ToUnicode(p.getFirstName()));
            sql.setParameter("lastName", butil.MS874ToUnicode(p.getLastName()));
            sql.setParameter("age", getAge());
            sql.setParameter("gender", butil.MS874ToUnicode(p.getGender()));
            sql.setParameter("dateOfBirth", p.getDateOfBirth());
            sql.setParameter("nationality", butil.MS874ToUnicode(p.getNationality()));
            sql.setParameter("remark", butil.MS874ToUnicode(remark));
            sql.setParameter("submitDate", submitDate);

        } else {
            sql.append(" (StrokeId,ActionFrom,StatusCode,StatusDescription,HN,CitizenId,Remark,SubmitDate) ");
            sql.append(" values(?strokeId,?actionFrom,?statusCode,?statusDescription,?hn,?citizenId,?remark,?submitDate)");

            if (mobileFlag) {
                sql.setParameter("actionFrom", "mobile");
            } else {
                sql.setParameter("actionFrom", "web");
            }

            // api p.getStatus() error if notfond *HardCode
            sql.setParameter("statusCode", 1);

            sql.setParameter("statusDescription", butil.MS874ToUnicode(remark));

            sql.setParameter("strokeId", strokeid);
            sql.setParameter("citizenId", citizenid);
            sql.setParameter("hn", hn);
            sql.setParameter("remark", butil.MS874ToUnicode(remark));
            sql.setParameter("submitDate", submitDate);
        }
        sql.executeUpdate(connection);

    }

//#(100000) programmer code end;
}
