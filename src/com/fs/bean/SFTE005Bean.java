package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005Bean.java
 * Description: SFTE005Bean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Wed Jan 03 09:44:55 ICT 2018
 */
/**
 * SFTE005Bean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class SFTE005Bean extends BeanSchema {

	public SFTE005Bean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("userid",java.sql.Types.CHAR,"userid");
		addSchema("usertname",java.sql.Types.CHAR,"usertname");
		addSchema("usertsurname",java.sql.Types.CHAR,"usertsurname");
		addSchema("userename",java.sql.Types.CHAR,"userename");
		addSchema("useresurname",java.sql.Types.CHAR,"useresurname");
		addSchema("email",java.sql.Types.VARCHAR,"email");
		addSchema("gender",java.sql.Types.CHAR,"gender");
		addSchema("mobile",java.sql.Types.VARCHAR,"mobile");
		collectClass(com.fs.bean.SFTE005ABean.class);
	}
	public String getUserid() {
		return getMember("userid");
	}
	public void setUserid(String newUserid) {
		setMember("userid",newUserid);
	}
	public String getUsertname() {
		return getMember("usertname");
	}
	public void setUsertname(String newUsertname) {
		setMember("usertname",newUsertname);
	}
	public String getUsertsurname() {
		return getMember("usertsurname");
	}
	public void setUsertsurname(String newUsertsurname) {
		setMember("usertsurname",newUsertsurname);
	}
	public String getUserename() {
		return getMember("userename");
	}
	public void setUserename(String newUserename) {
		setMember("userename",newUserename);
	}
	public String getUseresurname() {
		return getMember("useresurname");
	}
	public void setUseresurname(String newUseresurname) {
		setMember("useresurname",newUseresurname);
	}
	public String getEmail() {
		return getMember("email");
	}
	public void setEmail(String newEmail) {
		setMember("email",newEmail);
	}
	public String getGender() {
		return getMember("gender");
	}
	public void setGender(String newGender) {
		setMember("gender",newGender);
	}
	public String getMobile() {
		return getMember("mobile");
	}
	public void setMobile(String newMobile) {
		setMember("mobile",newMobile);
	}
	public String getSite() {
		return getMember("site");
	}
	public void setSite(String newSite) {
		setMember("site",newSite);
	}
}
