package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Thu Oct 18 10:49:25 ICT 2018
 */

import com.fs.bean.dba.*;
import com.fs.bean.misc.*;
import com.fs.bean.util.*;
//#everybody in love
//#(5000) programmer code begin;
//#(5000) programmer code end;
@SuppressWarnings({"serial","unused"})
public class SFTE007Bean extends CustomBean {
//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTE007Bean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("site",java.sql.Types.VARCHAR,"site");
	addSchema("userid",java.sql.Types.VARCHAR,"userid");
	addSchema("usertname",java.sql.Types.VARCHAR,"usertname");
	addSchema("usertsurname",java.sql.Types.VARCHAR,"usertsurname");
	addSchema("employeecode",java.sql.Types.VARCHAR,"employeecode");
	collectClass(com.fs.bean.SFTE007ABean.class);
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE007Bean.class+"=$Revision$\n";
}
public String getSite() {
	return getMember("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getEmployeecode() {
	return getMember("employeecode");
}
public void setEmployeecode(String newEmployeecode) {
	setMember("employeecode",newEmployeecode);
}
public String getUserid() {
	return getMember("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getUsertname() {
	return getMember("usertname");
}
public void setUsertname(String newUsertname) {
	setMember("usertname",newUsertname);
}
public String getUsertsurname() {
	return getMember("usertsurname");
}
public void setUsertsurname(String newUsertsurname) {
	setMember("usertsurname",newUsertsurname);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
//#(30000) programmer code end;
}
