package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE002AData.java
 * Description: SFTE002AData class implements for handle tgroup data base schema.
 * Version: $Revision$
 * Creation date: Mon Jun 26 09:15:14 ICT 2017
 */
/**
 * SFTE002AData class implements for handle tgroup data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE002AData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE002AData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tgroup");
	addSchema("groupname",java.sql.Types.VARCHAR);
	addSchema("supergroup",java.sql.Types.VARCHAR);
	addSchema("nameen",java.sql.Types.VARCHAR);
	addSchema("nameth",java.sql.Types.VARCHAR);
	addSchema("iconstyle",java.sql.Types.VARCHAR);
	addSchema("privateflag",java.sql.Types.VARCHAR);
	addSchema("usertype",java.sql.Types.VARCHAR);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("edituser",java.sql.Types.VARCHAR);
	addSchema("seqno",java.sql.Types.INTEGER);
	map("supergroup","supergroup");
	map("groupname","groupname");
	map("nameen","nameen");
	map("nameth","nameth");
	map("iconstyle","iconstyle");
	map("privateflag","privateflag");
	map("usertype","usertype");
	map("seqno","seqno");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	addSchema("userids",java.sql.Types.VARCHAR,true);
	addSchema("progids",java.sql.Types.VARCHAR,true);
	map("userids","userids");
	map("progids","progids");
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE002AData.class+"=$Revision$\n";
}
public String getGroupname() {
	return getString("groupname");
}
public void setGroupname(String newGroupname) {
	setMember("groupname",newGroupname);
}
public String getSupergroup() {
	return getString("supergroup");
}
public void setSupergroup(String newSupergroup) {
	setMember("supergroup",newSupergroup);
}
public String getNameen() {
	return getString("nameen");
}
public void setNameen(String newNameen) {
	setMember("nameen",newNameen);
}
public String getNameth() {
	return getString("nameth");
}
public void setNameth(String newNameth) {
	setMember("nameth",newNameth);
}
public String getIconstyle() {
	return getString("iconstyle");
}
public void setIconstyle(String newIconstyle) {
	setMember("iconstyle",newIconstyle);
}
public String getPrivateflag() {
	return getString("privateflag");
}
public void setPrivateflag(String newPrivateflag) {
	setMember("privateflag",newPrivateflag);
}
public String getUsertype() {
	return getString("usertype");
}
public void setUsertype(String newUsertype) {
	setMember("usertype",newUsertype);
}
public Date getEditdate() {
	return getDate("editdate");
}
public void setEditdate(Date newEditdate) {
	setMember("editdate",newEditdate);
}
public Time getEdittime() {
	return getTime("edittime");
}
public void setEdittime(Time newEdittime) {
	setMember("edittime",newEdittime);
}
public String getEdituser() {
	return getString("edituser");
}
public void setEdituser(String newEdituser) {
	setMember("edituser",newEdituser);
}
public int getSeqno() {
	return getInt("seqno");
}
public void setSeqno(int newSeqno) {
	setMember("seqno",newSeqno);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setGroupname(bean.getFieldByName(mapper("groupname")).asString());
	setSupergroup(bean.getFieldByName(mapper("supergroup")).asString());
	setNameen(bean.getFieldByName(mapper("nameen")).asString());
	setNameth(bean.getFieldByName(mapper("nameth")).asString());
	setIconstyle(bean.getFieldByName(mapper("iconstyle")).asString());
	setPrivateflag(bean.getFieldByName(mapper("privateflag")).asString());
	setUsertype(bean.getFieldByName(mapper("usertype")).asString());
	setSeqno(bean.getFieldByName(mapper("seqno")).asInt());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	if(global!=null) setEdituser(global.getFsUser());
	setUserids(bean.getFieldByName(mapper("userids")).asString());
	setProgids(bean.getFieldByName(mapper("progids")).asString());
	if(getPrivateflag()==null || getPrivateflag().trim().length()<=0) {
		setPrivateflag("0");
	}
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setGroupname(rs.getString("groupname"));
	setSupergroup(rs.getString("supergroup"));
	setNameen(rs.getString("nameen"));
	setNameth(rs.getString("nameth"));
	setIconstyle(rs.getString("iconstyle"));
	setPrivateflag(rs.getString("privateflag"));
	setUsertype(rs.getString("usertype"));
	setSeqno(rs.getInt("seqno"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	if(getEditdate()==null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if(getEdittime()==null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	//#(75000) programmer code end;
	sql.setParameter("groupname",getGroupname());
	sql.setParameter("supergroup",getSupergroup());
	sql.setParameter("nameen",getNameen());
	sql.setParameter("nameth",getNameth());
	sql.setParameter("iconstyle",getIconstyle());
	sql.setParameter("privateflag",getPrivateflag());
	sql.setParameter("usertype",getUsertype());
	sql.setParameter("editdate",getEditdate());
	sql.setParameter("edittime",getEdittime());
	sql.setParameter("edituser",getEdituser());
	sql.setParameter("seqno",getSeqno());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//insertGroupData(connection,centerConnection,globalConnection,transientVar);
	updateProgramGroup(connection);
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("groupname");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//deleteGroupData(connection,centerConnection,globalConnection,transientVar);
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("groupname",getGroupname());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("groupname");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	//deleteGroupData(connection,centerConnection,globalConnection,transientVar);
	//insertGroupData(connection,centerConnection,globalConnection,transientVar);
	updateProgramGroup(connection);
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("groupname");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getGroupname()==null) || getGroupname().trim().equals("")) throw new java.sql.SQLException("Groupname is unspecified","groupname",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE002AData aSFTE002AData = new SFTE002AData();
		aSFTE002AData.fetchResult(rs);
		add(aSFTE002AData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public String getUserids() {
	return getString("userids");
}
public void setUserids(String newUserids) {
	setMember("userids",newUserids);
}
public String getProgids() {
	return getString("progids");
}
public void setProgids(String newProgids) {
	setMember("progids",newProgids);
}
public int insertGroupData(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	int result = 0;
	TUserGroupData usergrp = new TUserGroupData();
	usergrp.setRelatable(this);
	usergrp.init(global);
	usergrp.setGroupname(getGroupname());
	String[] users = BeanUtility.split(getUserids());
	if(users!=null) {
		for(String uid : users) {
			usergrp.setUserid(uid);	
			result += usergrp.insert(connection,centerConnection,globalConnection,transientVar);
		}
	}
	TProgGroupData proggrp = new TProgGroupData();
	proggrp.setRelatable(this);
	proggrp.init(global);
	proggrp.setGroupname(getGroupname());
	String[] progs = BeanUtility.split(getProgids());
	if(progs!=null) {
		for(String pid : progs) {
			proggrp.setProgramid(pid);	
			result += proggrp.insert(connection,centerConnection,globalConnection,transientVar);
		}
	}
	return result;
}
public int deleteGroupData(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	int result = 0;	
	TUserGroupData usergrp = new TUserGroupData();
	usergrp.setRelatable(this);
	usergrp.init(global);
	usergrp.setGroupname(getGroupname());
	result += usergrp.deleteGroup(connection,centerConnection,globalConnection,transientVar);
	TProgGroupData proggrp = new TProgGroupData();
	proggrp.setRelatable(this);
	proggrp.init(global);
	proggrp.setGroupname(getGroupname());
	result += proggrp.deleteGroup(connection,centerConnection,globalConnection,transientVar);
	return result;
}
public int updateProgramGroup(java.sql.Connection connection) throws Exception {
	int result = 0;
	String[] progs = BeanUtility.split(getProgids());
	if(progs!=null) {
		int index = 0;
		KnSQL knsql = new KnSQL(this);
		knsql.append("update tproggrp set seqno = ?seqno ");
		knsql.append("where groupname = ?groupname and programid = ?programid ");
		for(String pid : progs) {
			if(pid!=null && pid.trim().length()>0) {
				index++;
				knsql.clearParameters();
				knsql.setParameter("groupname", getGroupname());
				knsql.setParameter("programid", pid);
				knsql.setParameter("seqno", index);
				result += knsql.executeUpdate(connection);
			}
		}
	}
	return result;
}
//#(100000) programmer code end;
}
