package com.fs.bean;

import com.fs.bean.gener.*;
@SuppressWarnings("serial")
public class HolidayBean extends BeanSchema {

	public HolidayBean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("site",java.sql.Types.VARCHAR,"site");
		addSchema("holidayid",java.sql.Types.VARCHAR,"holidayid");
		addSchema("holidaydate",java.sql.Types.DATE,"holidaydate");
		addSchema("nameen",java.sql.Types.VARCHAR,"nameen");
		addSchema("nameth",java.sql.Types.VARCHAR,"nameth");
		addSchema("annually",java.sql.Types.VARCHAR,"annually");
		addSchema("editdate",java.sql.Types.DATE,"editdate");
		addSchema("edittime",java.sql.Types.TIME,"edittime");
		addSchema("edituser",java.sql.Types.VARCHAR,"edituser");
		
	}
	public String getSite() {
		return getMember("site");
	}
	public void setSite(String newSite) {
		setMember("site",newSite);
	}
	public String getHolidayid() {
		return getMember("holidayid");
	}
	public void setHolidayid(String newHolidayid) {
		setMember("holidayid",newHolidayid);
	}
	public String getHolidaydate() {
		return getMember("holidaydate");
	}
	public void setHolidaydate(String newHolidaydate) {
		setMember("holidaydate",newHolidaydate);
	}
	public String getNameen() {
		return getMember("nameen");
	}
	public void setNameen(String newNameen) {
		setMember("nameen",newNameen);
	}
	public String getNameth() {
		return getMember("nameth");
	}
	public void setNameth(String newNameth) {
		setMember("nameth",newNameth);
	}
	public String getAnnually() {
		return getMember("annually");
	}
	public void setAnnually(String newAnnually) {
		setMember("annually",newAnnually);
	}
	public String getEditdate() {
		return getMember("editdate");
	}
	public void setEditdate(String newEditdate) {
		setMember("editdate",newEditdate);
	}
	public String getEdittime() {
		return getMember("edittime");
	}
	public void setEdittime(String newEdittime) {
		setMember("edittime",newEdittime);
	}
	public String getEdituser() {
		return getMember("edituser");
	}
	public void setEdituser(String newEdituser) {
		setMember("edituser",newEdituser);
	}
}
