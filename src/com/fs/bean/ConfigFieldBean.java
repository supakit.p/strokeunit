package com.fs.bean;
import com.fs.bean.misc.*;
import com.fs.bean.util.*;
import com.fs.bean.gener.*;
public class ConfigFieldBean extends BeanSchema {
public ConfigFieldBean() {
	super();
}
protected void initialize() {
	super.initialize();
 //------------------
 //Create initialize  method here...
addSchema("configid",java.sql.Types.INTEGER,"configid");
addSchema("configname",java.sql.Types.NVARCHAR,"configname");
addSchema("paramname",java.sql.Types.NVARCHAR,"paramname");
addSchema("tablename",java.sql.Types.NVARCHAR,"tablename");
addSchema("fieldname",java.sql.Types.NVARCHAR,"fieldname");
addSchema("fieldtype",java.sql.Types.NVARCHAR,"fieldtype");
addSchema("iskey",java.sql.Types.BOOLEAN,"iskey");
addSchema("isautokey",java.sql.Types.BOOLEAN,"isautokey");
addSchema("isarray",java.sql.Types.BOOLEAN,"isarray");
addSchema("isactive",java.sql.Types.BOOLEAN,"isactive");
 //-------------------
}
public String fetchVersion() {
	return super.fetchVersion()+ConfigFieldBean.class+"=$Revision$\n";
}
 //------------------
 //create get - set  method here...
 public String getConfigid() {
	return getMember("configid");
 }
 public void setConfigid(String configid) {
	setMember("configid",configid);
 }
 public String getConfigname() {
	return getMember("configname");
 }
 public void setConfigname(String configname) {
	setMember("configname",configname);
 }
 public String getParamname() {
	return getMember("paramname");
 }
 public void setParamname(String paramname) {
	setMember("paramname",paramname);
 }
 public String getTablename() {
	return getMember("tablename");
 }
 public void setTablename(String tablename) {
	setMember("tablename",tablename);
 }
 public String getFieldname() {
	return getMember("fieldname");
 }
 public void setFieldname(String fieldname) {
	setMember("fieldname",fieldname);
 }
 public String getFieldtype() {
	return getMember("fieldtype");
 }
 public void setFieldtype(String fieldtype) {
	setMember("fieldtype",fieldtype);
 }
 public String getIskey() {
	return getMember("iskey");
 }
 public void setIskey(String iskey) {
	setMember("iskey",iskey);
 }
 public String getIsautokey() {
	return getMember("isautokey");
 }
 public void setIsautokey(String isautokey) {
	setMember("isautokey",isautokey);
 }
 public String getIsarray() {
	return getMember("isarray");
 }
 public void setIsarray(String isarray) {
	setMember("isarray",isarray);
 }
 public String getIsactive() {
	return getMember("isactive");
 }
 public void setIsactive(String isactive) {
	setMember("isactive",isactive);
 }
 public String getCreatedate() {
	return getMember("createdate");
 }
 public void setCreatedate(String createdate) {
	setMember("createdate",createdate);
 }
 public String getUpdatedate() {
	return getMember("updatedate");
 }
 public void setUpdatedate(String updatedate) {
	setMember("updatedate",updatedate);
 }
 //-------------------
}
