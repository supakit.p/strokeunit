package com.fs.bean;

public class TheCounter {
	private int records;
	private int rows;
	private String remarks;
	private boolean done;
	public TheCounter() {
		super();
	}
	public TheCounter(int rows,int records) {
		this.rows = rows;
		this.records = records;
	}
	public int getErrors() {
		return rows - records;
	}
	public int getRecords() {
		return records;
	}
	public String getRemarks() {
		return remarks;
	}
	public int getRows() {
		return rows;		
	}
	public boolean isDone() {
		return done;
	}
	public void obtain(TheCounter count) {
		if(count==null) { return; }
		this.records = count.records;
		this.rows = count.rows;
	}
	public void setDone(boolean done) {
		this.done = done;
	}
	public void setRecords(int records) {
		this.records = records;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String toString() {
		return super.toString()+"{rows="+rows+",records="+records+",remarks="+remarks+",done="+done+"}";
	}
}
