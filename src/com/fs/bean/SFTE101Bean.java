package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Wed Sep 13 15:50:51 ICT 2017
 */

import com.fs.bean.misc.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
//#everybody in love
//#(5000) programmer code begin;
//#(5000) programmer code end;
public class SFTE101Bean extends BeanSchema {
//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTE101Bean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("data",java.sql.Types.VARCHAR,"data :");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE101Bean.class+"=$Revision$\n";
}
public String getData() {
	return getMember("data");
}
public void setData(String newData) {
	setMember("data",newData);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
//#(30000) programmer code end;
/*
$Revision$
*/
}
