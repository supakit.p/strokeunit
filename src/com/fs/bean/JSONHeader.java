package com.fs.bean;

import org.json.simple.*;

import com.fs.bean.util.GlobalBean;
import com.fs.bean.util.LabelConfig;

@SuppressWarnings("unchecked")
public class JSONHeader extends JSONBean {
	private String model;
	private String method;
	private String errorcode;
	private String errorflag;
	private String errordesc;
	
	public JSONHeader() {
		super();
	}

	public void composeError(GlobalBean fsGlobal) {
		if(fsGlobal==null) return;
		int errorCode = fsGlobal.parseFsErrorcode();
		composeError("Y", Integer.toString(errorCode==0?1:errorCode), fsGlobal.getFsMessage());		
	}
	
	public void composeError(String errorflag,String errorcode,String errordesc) {
		setErrorflag(errorflag);
		setErrorcode(errorcode);
		setErrordesc(errordesc);
	}

	public void composeErrorNotFound(LabelConfig fsLabel) {
		composeError("Y", "2", fsLabel==null?"Not Found":fsLabel.getText("notfound","Not Found"));
	}
	
	public void composeNoError() {
		composeError("N", "0", "");
	}
		
	public String getErrorcode() {
		return errorcode;
	}

	public String getErrordesc() {
		return errordesc;
	}

	public String getErrorflag() {
		return errorflag;
	}

	public String getMethod() {
		return method;
	}

	public String getModel() {
		return model;
	}

	public void obtain(JSONObject json){
		if(json==null) { return; }
		setModel(getJSON(json,"model"));
		setMethod(getJSON(json,"method"));
		setErrorflag(getJSON(json,"errorflag"));
		setErrorcode(getJSON(json,"errorcode"));
		setErrordesc(getJSON(json,"errordesc"));
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public void setErrordesc(String errordesc) {
		this.errordesc = errordesc;
	}

	public void setErrorflag(String errorflag) {
		this.errorflag = errorflag;
	}
	
	public void setMethod(String method) {
		this.method = method;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public JSONObject toJSONObject() {
		JSONObject json = new JSONObject();
		json.put("model",getModel()==null?"":getModel());
		json.put("method",getMethod()==null?"":getMethod());
		json.put("errorflag",getErrorflag()==null?"":getErrorflag());
		json.put("errorcode",getErrorcode()==null?"":getErrorcode());
		json.put("errordesc",getErrordesc()==null?"":getErrordesc());
		return json;
	}
}
