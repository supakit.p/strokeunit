package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Mon Jun 26 08:56:34 ICT 2017
 */

import com.fs.bean.gener.*;

@SuppressWarnings("serial")
public class SFTE001ABean extends BeanSchema {

//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTE001ABean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("product",java.sql.Types.VARCHAR,"Product");
	addSchema("programid",java.sql.Types.VARCHAR,"Program ID");
	addSchema("progname",java.sql.Types.VARCHAR,"Program Name");
	addSchema("prognameth",java.sql.Types.VARCHAR,"Program Name");
	addSchema("progtype",java.sql.Types.VARCHAR,"Program Type");
	addSchema("description",java.sql.Types.VARCHAR,"Description");
	addSchema("parameters",java.sql.Types.VARCHAR,"Parameters");
	addSchema("progsystem",java.sql.Types.VARCHAR,"Program System");
	addSchema("iconfile",java.sql.Types.VARCHAR,"Icon File");
	addSchema("iconstyle",java.sql.Types.VARCHAR,"Styles");
	addSchema("shortname",java.sql.Types.VARCHAR,"Short Name");	
	addSchema("shortnameth",java.sql.Types.VARCHAR,"Short Name");	
	addSchema("progpath",java.sql.Types.VARCHAR,"Program Path");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	addSchema("progtypedesc",java.sql.Types.VARCHAR,"Program Type Description");
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE001ABean.class+"=$Revision$\n";
}
public String getProduct() {
	return getMember("product");
}
public void setProduct(String newProduct) {
	setMember("product",newProduct);
}
public String getProgramid() {
	return getMember("programid");
}
public void setProgramid(String newProgramid) {
	setMember("programid",newProgramid);
}
public String getProgname() {
	return getMember("progname");
}
public void setProgname(String newProgname) {
	setMember("progname",newProgname);
}
public String getPrognameth() {
	return getMember("prognameth");
}
public void setPrognameth(String newPrognameth) {
	setMember("prognameth",newPrognameth);
}
public String getProgtype() {
	return getMember("progtype");
}
public void setProgtype(String newProgtype) {
	setMember("progtype",newProgtype);
}
public String getDescription() {
	return getMember("description");
}
public void setDescription(String newDescription) {
	setMember("description",newDescription);
}
public String getParameters() {
	return getMember("parameters");
}
public void setParameters(String newParameters) {
	setMember("parameters",newParameters);
}
public String getProgsystem() {
	return getMember("progsystem");
}
public void setProgsystem(String newProgsystem) {
	setMember("progsystem",newProgsystem);
}
public String getIconfile() {
	return getMember("iconfile");
}
public void setIconfile(String newIconfile) {
	setMember("iconfile",newIconfile);
}
public String getIconstyle() {
	return getMember("iconstyle");
}
public void setIconstyle(String newIconstyle) {
	setMember("iconstyle",newIconstyle);
}
public String getShortname() {
	return getMember("shortname");
}
public void setShortname(String newShortname) {
	setMember("shortname",newShortname);
}
public String getShortnameth() {
	return getMember("shortnameth");
}
public void setShortnameth(String newShortnameth) {
	setMember("shortnameth",newShortnameth);
}
public String getProgpath() {
	return getMember("progpath");
}
public void setProgpath(String newProgpath) {
	setMember("progpath",newProgpath);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
public String getProgtypedesc() {
	return getMember("progtypedesc");
}
public void setProgtypedesc(String newProgtypedesc) {
	setMember("progtypedesc",newProgtypedesc);
}
//#(30000) programmer code end;
}
