package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: TUserData.java
 * Description: TUserData class implements for handle tuser data base schema.
 * Version: $Revision$
 * Creation date: Sat Oct 20 11:54:52 ICT 2018
 */
/**
 * TUserData class implements for handle tuser data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.Arguments;
import com.fs.dev.Console;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class TUserData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public TUserData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tuser");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("username",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("startdate",java.sql.Types.DATE);
	addSchema("enddate",java.sql.Types.DATE);
	addSchema("status",java.sql.Types.VARCHAR);
	addSchema("userpassword",java.sql.Types.VARCHAR);
	addSchema("passwordexpiredate",java.sql.Types.DATE);
	addSchema("showphoto",java.sql.Types.VARCHAR);
	addSchema("adminflag",java.sql.Types.VARCHAR);
	addSchema("theme",java.sql.Types.VARCHAR);
	addSchema("firstpage",java.sql.Types.VARCHAR);
	addSchema("loginfailtimes",java.sql.Types.TINYINT);
	addSchema("lockflag",java.sql.Types.VARCHAR);
	addSchema("usertype",java.sql.Types.VARCHAR);
	addSchema("iconfile",java.sql.Types.VARCHAR);
	addSchema("accessdate",java.sql.Types.DATE);
	addSchema("accesstime",java.sql.Types.TIME);
	addSchema("accesshits",java.sql.Types.BIGINT);
	addSchema("failtime",java.sql.Types.BIGINT);
	addSchema("siteflag",java.sql.Types.VARCHAR);
	addSchema("branchflag",java.sql.Types.VARCHAR);
	addSchema("approveflag",java.sql.Types.VARCHAR);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("edituser",java.sql.Types.VARCHAR);
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+TUserData.class+"=$Revision$\n";
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getUsername() {
	return getString("username");
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public Date getStartdate() {
	return getDate("startdate");
}
public void setStartdate(Date newStartdate) {
	setMember("startdate",newStartdate);
}
public Date getEnddate() {
	return getDate("enddate");
}
public void setEnddate(Date newEnddate) {
	setMember("enddate",newEnddate);
}
public String getStatus() {
	return getString("status");
}
public void setStatus(String newStatus) {
	setMember("status",newStatus);
}
public String getUserpassword() {
	return getString("userpassword");
}
public void setUserpassword(String newUserpassword) {
	setMember("userpassword",newUserpassword);
}
public Date getPasswordexpiredate() {
	return getDate("passwordexpiredate");
}
public void setPasswordexpiredate(Date newPasswordexpiredate) {
	setMember("passwordexpiredate",newPasswordexpiredate);
}
public String getShowphoto() {
	return getString("showphoto");
}
public void setShowphoto(String newShowphoto) {
	setMember("showphoto",newShowphoto);
}
public String getAdminflag() {
	return getString("adminflag");
}
public void setAdminflag(String newAdminflag) {
	setMember("adminflag",newAdminflag);
}
public String getTheme() {
	return getString("theme");
}
public void setTheme(String newTheme) {
	setMember("theme",newTheme);
}
public String getFirstpage() {
	return getString("firstpage");
}
public void setFirstpage(String newFirstpage) {
	setMember("firstpage",newFirstpage);
}
public int getLoginfailtimes() {
	return getInt("loginfailtimes");
}
public void setLoginfailtimes(int newLoginfailtimes) {
	setMember("loginfailtimes",newLoginfailtimes);
}
public String getLockflag() {
	return getString("lockflag");
}
public void setLockflag(String newLockflag) {
	setMember("lockflag",newLockflag);
}
public String getUsertype() {
	return getString("usertype");
}
public void setUsertype(String newUsertype) {
	setMember("usertype",newUsertype);
}
public String getIconfile() {
	return getString("iconfile");
}
public void setIconfile(String newIconfile) {
	setMember("iconfile",newIconfile);
}
public Date getAccessdate() {
	return getDate("accessdate");
}
public void setAccessdate(Date newAccessdate) {
	setMember("accessdate",newAccessdate);
}
public Time getAccesstime() {
	return getTime("accesstime");
}
public void setAccesstime(Time newAccesstime) {
	setMember("accesstime",newAccesstime);
}
public long getAccesshits() {
	return getLong("accesshits");
}
public void setAccesshits(long newAccesshits) {
	setMember("accesshits",newAccesshits);
}
public long getFailtime() {
	return getLong("failtime");
}
public void setFailtime(long newFailtime) {
	setMember("failtime",newFailtime);
}
public String getSiteflag() {
	return getString("siteflag");
}
public void setSiteflag(String newSiteflag) {
	setMember("siteflag",newSiteflag);
}
public String getBranchflag() {
	return getString("branchflag");
}
public void setBranchflag(String newBranchflag) {
	setMember("branchflag",newBranchflag);
}
public String getApproveflag() {
	return getString("approveflag");
}
public void setApproveflag(String newApproveflag) {
	setMember("approveflag",newApproveflag);
}
public Date getEditdate() {
	return getDate("editdate");
}
public void setEditdate(Date newEditdate) {
	setMember("editdate",newEditdate);
}
public Time getEdittime() {
	return getTime("edittime");
}
public void setEdittime(Time newEdittime) {
	setMember("edittime",newEdittime);
}
public String getEdituser() {
	return getString("edituser");
}
public void setEdituser(String newEdituser) {
	setMember("edituser",newEdituser);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUsername(bean.getFieldByName(mapper("username")).asString());
	setSite(bean.getFieldByName(mapper("site")).asString());
	setStartdate(bean.getFieldByName(mapper("startdate")).asDate());
	setEnddate(bean.getFieldByName(mapper("enddate")).asDate());
	setStatus(bean.getFieldByName(mapper("status")).asString());
	setUserpassword(bean.getFieldByName(mapper("userpassword")).asString());
	setPasswordexpiredate(bean.getFieldByName(mapper("passwordexpiredate")).asDate());
	setShowphoto(bean.getFieldByName(mapper("showphoto")).asString());
	setAdminflag(bean.getFieldByName(mapper("adminflag")).asString());
	setTheme(bean.getFieldByName(mapper("theme")).asString());
	setFirstpage(bean.getFieldByName(mapper("firstpage")).asString());
	setLoginfailtimes(bean.getFieldByName(mapper("loginfailtimes")).asInt());
	setLockflag(bean.getFieldByName(mapper("lockflag")).asString());
	setUsertype(bean.getFieldByName(mapper("usertype")).asString());
	setIconfile(bean.getFieldByName(mapper("iconfile")).asString());
	setAccessdate(bean.getFieldByName(mapper("accessdate")).asDate());
	setAccesstime(bean.getFieldByName(mapper("accesstime")).asTime());
	setAccesshits(bean.getFieldByName(mapper("accesshits")).asLong());
	setFailtime(bean.getFieldByName(mapper("failtime")).asLong());
	setSiteflag(bean.getFieldByName(mapper("siteflag")).asString());
	setBranchflag(bean.getFieldByName(mapper("branchflag")).asString());
	setApproveflag(bean.getFieldByName(mapper("approveflag")).asString());
	setEditdate(bean.getFieldByName(mapper("editdate")).asDate());
	setEdittime(bean.getFieldByName(mapper("edittime")).asTime());
	setEdituser(bean.getFieldByName(mapper("edituser")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setUsername(rs.getString("username"));
	setSite(rs.getString("site"));
	setStartdate(rs.getDate("startdate"));
	setEnddate(rs.getDate("enddate"));
	setStatus(rs.getString("status"));
	setUserpassword(rs.getString("userpassword"));
	setPasswordexpiredate(rs.getDate("passwordexpiredate"));
	setShowphoto(rs.getString("showphoto"));
	setAdminflag(rs.getString("adminflag"));
	setTheme(rs.getString("theme"));
	setFirstpage(rs.getString("firstpage"));
	setLoginfailtimes(rs.getInt("loginfailtimes"));
	setLockflag(rs.getString("lockflag"));
	setUsertype(rs.getString("usertype"));
	setIconfile(rs.getString("iconfile"));
	setAccessdate(rs.getDate("accessdate"));
	setAccesstime(rs.getTime("accesstime"));
	setAccesshits(rs.getLong("accesshits"));
	setFailtime(rs.getLong("failtime"));
	setSiteflag(rs.getString("siteflag"));
	setBranchflag(rs.getString("branchflag"));
	setApproveflag(rs.getString("approveflag"));
	setEditdate(rs.getDate("editdate"));
	setEdittime(rs.getTime("edittime"));
	setEdituser(rs.getString("edituser"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("username",getUsername());
	sql.setParameter("site",getSite());
	sql.setParameter("startdate",getStartdate());
	sql.setParameter("enddate",getEnddate());
	sql.setParameter("status",getStatus());
	sql.setParameter("userpassword",getUserpassword());
	sql.setParameter("passwordexpiredate",getPasswordexpiredate());
	sql.setParameter("showphoto",getShowphoto());
	sql.setParameter("adminflag",getAdminflag());
	sql.setParameter("theme",getTheme());
	sql.setParameter("firstpage",getFirstpage());
	sql.setParameter("loginfailtimes",getLoginfailtimes());
	sql.setParameter("lockflag",getLockflag());
	sql.setParameter("usertype",getUsertype());
	sql.setParameter("iconfile",getIconfile());
	sql.setParameter("accessdate",getAccessdate());
	sql.setParameter("accesstime",getAccesstime());
	sql.setParameter("accesshits",getAccesshits());
	sql.setParameter("failtime",getFailtime());
	sql.setParameter("siteflag",getSiteflag());
	sql.setParameter("branchflag",getBranchflag());
	sql.setParameter("approveflag",getApproveflag());
	sql.setParameter("editdate",getEditdate());
	sql.setParameter("edittime",getEdittime());
	sql.setParameter("edituser",getEdituser());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("userid",getUserid());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		if(rs.next()) {
			result++;
			fetchResult(rs);
			//#any result fetching fool again
			//#(220000) programmer code begin;
			//#(220000) programmer code end;
		}
		//#after fetching result set lovin each day
		//#(230000) programmer code begin;
		//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		while(rs.next()) {
			result++;
			//#addon statement if you come back
			//#(85000) programmer code begin;
			//#(85000) programmer code end;
			TUserData aTUserData = new TUserData();
			aTUserData.fetchResult(rs);
			add(aTUserData);
			//#addon statement if you come back
			//#(90000) programmer code begin;
			//#(90000) programmer code end;
		}
		//#after scraping result set in too deep
		//#(240000) programmer code begin;
		//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public static void main(String[] args) {
	try {
		if(args.length>0) {
			String section = Arguments.getString(args, "PROMPT", "-ms");
			try(java.sql.Connection connection = TUserData.getNewConnection(section,false)) {
				KnSQL knsql = new KnSQL();
				knsql.append("select cardid,usertname,usertsurname from tuserinfo");
				try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
					while(rs.next()) {
						String cardid = rs.getString("cardid");
						String usertname = rs.getString("usertname");
						String usertsurname = rs.getString("usertsurname");
						if(cardid!=null) {
							Console.out.println("cardid = "+cardid+" : "+cardid.hashCode()+" : "+Integer.toHexString(cardid.hashCode()));
						}
						if(usertname!=null) {
							Console.out.println("usertname = "+usertname+" : "+usertname.hashCode()+" : "+Integer.toHexString(usertname.hashCode()));					
						}
						if(usertsurname!=null) {
							Console.out.println("usertsurname = "+usertsurname+" : "+usertsurname.hashCode()+" : "+Integer.toHexString(usertsurname.hashCode()));										
						}
						String uname = usertname+usertsurname;
						Console.out.println("uname = "+uname+" : "+uname.hashCode()+" : "+Integer.toHexString(uname.hashCode()));										
					}
				}
			}
		} else {
			Console.out.println("USAGE : "+TUserData.class);
			Console.out.println("\t-ms	private db section");
		}
	} catch(java.sql.SQLException ex) {
		Console.out.print(ex);
		Console.out.println("ERROR CODE : "+ex.getErrorCode());
	} catch(Exception ex) {
		Console.out.print(ex);
	}	
}
//#(100000) programmer code end;
}
