package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005CData.java
 * Description: SFTE005CData class implements for handle tuser data base schema.
 * Version: $Revision$
 * Creation date: Wed Jan 03 14:38:21 ICT 2018
 */
/**
 * SFTE005CData class implements for handle tuser data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.TheLibrary;
import com.fs.dev.auth.PasswordLibrary;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE005DData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE005DData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tuser");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("username",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("userpassword",java.sql.Types.VARCHAR);
	addSchema("passwordexpiredate",java.sql.Types.DATE);
	addSchema("changeflag",java.sql.Types.VARCHAR);
	addSchema("pincode",java.sql.Types.VARCHAR);
	map("site","site");
	map("passwordexpiredate","passwordexpiredate");
	map("userpassword","userpassword");
	map("username","username");
	map("userid","userid");
	map("changeflag","changeflag");
	map("pincode","pincode");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	addSchema("oldpassword",java.sql.Types.VARCHAR,true);
	addSchema("confirmpassword",java.sql.Types.VARCHAR,true);
	addSchema("checkflag",java.sql.Types.VARCHAR,true);
	addSchema("cardid",java.sql.Types.VARCHAR,true);
	addSchema("birthday",java.sql.Types.DATE,true);
	map("oldpassword","oldpassword");
	map("confirmpassword","confirmpassword");
	map("checkflag","checkflag");
	map("cardid","cardid");
	map("birthday","birthday");
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE005DData.class+"=$Revision$\n";
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getUsername() {
	return getString("username");
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getUserpassword() {
	return getString("userpassword");
}
public void setUserpassword(String newUserpassword) {
	setMember("userpassword",newUserpassword);
}
public Date getPasswordexpiredate() {
	return getDate("passwordexpiredate");
}
public void setPasswordexpiredate(Date newPasswordexpiredate) {
	setMember("passwordexpiredate",newPasswordexpiredate);
}
public String getOldpassword() {
	return getString("oldpassword");
}
public void setOldpassword(String newOldpassword) {
	setMember("oldpassword",newOldpassword);
}
public String getConfirmpassword() {
	return getString("confirmpassword");
}
public void setConfirmpassword(String newConfirmpassword) {
	setMember("confirmpassword",newConfirmpassword);
}
public String getChangeflag() {
	return getString("changeflag");
}
public void setChangeflag(String newChangeflag) {
	setMember("changeflag",newChangeflag);
}
public String getCheckflag() {
	return getString("checkflag");
}
public void setCheckflag(String newCheckflag) {
	setMember("checkflag",newCheckflag);
}
public String getPincode() {
	return getString("pincode");
}
public void setPincode(String newPincode) {
	setMember("pincode",newPincode);
}
public String getCardid() {
	return getString("cardid");
}
public void setCardid(String newCardid) {
	setMember("cardid",newCardid);
}
public Date getBirthday() {
	return getDate("birthday");
}
public void setBirthday(Date newBirthday) {
	setMember("birthday",newBirthday);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUsername(bean.getFieldByName(mapper("username")).asString());
	setSite(bean.getFieldByName(mapper("site")).asString());
	setUserpassword(bean.getFieldByName(mapper("userpassword")).asString());
	setPasswordexpiredate(bean.getFieldByName(mapper("passwordexpiredate")).asDate());
	setPincode(bean.getFieldByName(mapper("pincode")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	setOldpassword(bean.getFieldByName(mapper("oldpassword")).asString());
	setConfirmpassword(bean.getFieldByName(mapper("confirmpassword")).asString());
	setCheckflag(bean.getFieldByName(mapper("checkflag")).asString());
	setCardid(bean.getFieldByName(mapper("cardid")).asString());
	setBirthday(bean.getFieldByName(mapper("birthday")).asDate());
	if(getCheckflag()==null || getCheckflag().trim().length()<=0) setCheckflag("1");
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setUsername(rs.getString("username"));
	setSite(rs.getString("site"));
	setUserpassword(rs.getString("userpassword"));
	setPasswordexpiredate(rs.getDate("passwordexpiredate"));
	setChangeflag(rs.getString("changeflag"));
	setPincode(rs.getString("pincode"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("username",getUsername());
	sql.setParameter("site",getSite());
	sql.setParameter("userpassword",getUserpassword());
	sql.setParameter("passwordexpiredate",getPasswordexpiredate());
	sql.setParameter("changeflag", getChangeflag());
	sql.setParameter("pincode", getPincode());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	boolean found = false;
	String username = null;
	KnSQL knsql = new KnSQL(this);
	knsql.append("select username from tuser where userid=?userid");
	knsql.setParameter("userid",getUserid());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		if(rs.next()) {
			found = true;
			username = rs.getString("username");
		}
	}
	if(!found) throw new BeanException("User does not existed",-8895,global==null?"EN":global.getFsLanguage());
	PasswordLibrary plib = new PasswordLibrary(this);
	String password = getUserpassword();
	if(password.trim().length()>0) {
		if(password.equals(getUserid())) throw new BeanException("Not allow password as same as user",-8899,global==null?"EN":global.getFsLanguage());
		if(password.equals(username)) throw new BeanException("Not allow password as same as user",-8994,global==null?"EN":global.getFsLanguage());
		if (password.length() > 8) {
			throw new BeanException("Password length not over 8 characters",-8896,global==null?"EN":global.getFsLanguage());
			//password = password.substring(0,8);
		}
		password = plib.encrypt(password);
		setUserpassword(password);	
		setChangeflag("0");
	}
	String pincode = getPincode();
	if(pincode!=null && pincode.trim().length()>0) {
		if (pincode.length() > 8) {
			pincode = pincode.substring(0,8);
		}
		pincode = plib.encrypt(pincode);
		setPincode(pincode);
	}
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE005DData aSFTE005CData = new SFTE005DData();
		aSFTE005CData.fetchResult(rs);
		add(aSFTE005CData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public BeanException validateDatas(String fs_language) {
	if(getUserid()==null || getUserid().trim().length()<=0) {
		return new BeanException("User is undefined",-8891,fs_language);
	}
	if(!getUserpassword().equals(getConfirmpassword())) {
		return new BeanException("Confirm Password Mismatch",-8897,fs_language);
	}
	if(getUserpassword().equals(getUserid())) {
		return new BeanException("Not allow password as same as user",-8899,fs_language);
	}
	if(getUserpassword().equals(getUsername())) {
		return new BeanException("Not allow password as same as user name",-8994,fs_language);
	}
	if(getUserpassword().length()>8) {
		return new BeanException("Password length not over 8 characters",-8896,fs_language);
	}
	return TheLibrary.validatePincode(getPincode(),getCardid(),getBirthday(),fs_language);
}
//#(100000) programmer code end;
}
