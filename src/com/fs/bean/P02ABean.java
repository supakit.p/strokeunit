package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Mon Mar 01 16:19:48 ICT 2021
 */

import com.fs.bean.dba.*;
import com.fs.bean.misc.*;
import com.fs.bean.util.*;
//#everybody in love
//#(5000) programmer code begin;
//#(5000) programmer code end;
@SuppressWarnings({"serial","unused"})
public class P02ABean extends CustomBean {
//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public P02ABean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("focusid",java.sql.Types.INTEGER,"focusid");
	addSchema("focusname",java.sql.Types.VARCHAR,"focusname");
	addSchema("isactive",java.sql.Types.VARCHAR,"isactive");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+P02ABean.class+"=$Revision$\n";
}
public String getFocusid() {
	return getMember("focusid");
}
public void setFocusid(String newFocusid) {
	setMember("focusid",newFocusid);
}
public String getFocusname() {
	return getMember("focusname");
}
public void setFocusname(String newFocusname) {
	setMember("focusname",newFocusname);
}
public String getIsactive() {
	return getMember("isactive");
}
public void setIsactive(String newIsactive) {
	setMember("isactive",newIsactive);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
//#(30000) programmer code end;
}
