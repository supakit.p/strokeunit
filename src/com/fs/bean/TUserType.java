package com.fs.bean;

public class TUserType {
	private String groupname;
	private String usertype;
	private int level;
	private String approveflag;
	public TUserType() {
		super();
	}
	public void fetchResult(java.sql.ResultSet rs) throws Exception {
		this.groupname = rs.getString("groupname");
		this.usertype = rs.getString("usertype");
		this.level = rs.getInt("level");
	}
	public String getApproveflag() {
		return approveflag;
	}
	public String getGroupname() {
		return groupname;
	}
	public int getLevel() {
		return level;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setApproveflag(String approveflag) {
		this.approveflag = approveflag;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
}
