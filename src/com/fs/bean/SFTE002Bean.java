package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Mon Jun 26 09:12:53 ICT 2017
 */

import com.fs.bean.gener.*;

@SuppressWarnings("serial")
public class SFTE002Bean extends BeanSchema {

//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTE002Bean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("groupname",java.sql.Types.VARCHAR,"Group");
	addSchema("supergroup",java.sql.Types.VARCHAR,"Super Group");
	addSchema("nameen",java.sql.Types.VARCHAR,"Name English");
	addSchema("nameth",java.sql.Types.VARCHAR,"Name Thai");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	collectClass(com.fs.bean.SFTE002ABean.class);
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE002Bean.class+"=$Revision$\n";
}
public String getGroupname() {
	return getMember("groupname");
}
public void setGroupname(String newGroupname) {
	setMember("groupname",newGroupname);
}
public String getSupergroup() {
	return getMember("supergroup");
}
public void setSupergroup(String newSupergroup) {
	setMember("supergroup",newSupergroup);
}
public String getNameen() {
	return getMember("nameen");
}
public void setNameen(String newNameen) {
	setMember("nameen",newNameen);
}
public String getNameth() {
	return getMember("nameth");
}
public void setNameth(String newNameth) {
	setMember("nameth",newNameth);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
//#(30000) programmer code end;
}
