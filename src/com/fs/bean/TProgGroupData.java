package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: TProgGroupData.java
 * Description: TProgGroupData class implements for handle tproggrp data base schema.
 * Version: $Revision$
 * Creation date: Mon Jun 26 09:18:12 ICT 2017
 */
/**
 * TProgGroupData class implements for handle tproggrp data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class TProgGroupData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public TProgGroupData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tproggrp");
	addSchema("groupname",java.sql.Types.VARCHAR);
	addSchema("programid",java.sql.Types.VARCHAR);
	addSchema("seqno",java.sql.Types.VARCHAR);
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	map("groupname","groupname");
	map("programid","programid");
	map("seqno","seqno");
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+TProgGroupData.class+"=$Revision$\n";
}
public String getGroupname() {
	return getString("groupname");
}
public void setGroupname(String newGroupname) {
	setMember("groupname",newGroupname);
}
public String getProgramid() {
	return getString("programid");
}
public void setProgramid(String newProgramid) {
	setMember("programid",newProgramid);
}
public int getSeqno() {
	return getInt("seqno");
}
public void setSeqno(int newSeqno) {
	setMember("seqno",newSeqno);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setGroupname(bean.getFieldByName(mapper("groupname")).asString());
	setProgramid(bean.getFieldByName(mapper("programid")).asString());
	setSeqno(bean.getFieldByName(mapper("seqno")).asInt());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setGroupname(rs.getString("groupname"));
	setProgramid(rs.getString("programid"));
	setSeqno(rs.getInt("seqno"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("groupname",getGroupname());
	sql.setParameter("programid",getProgramid());
	sql.setParameter("seqno",getSeqno());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("groupname");
	setKeyField("programid");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	KnSQL knsql = new KnSQL(this);
	knsql.append("delete from tfavor where programid = ?programid ");
	knsql.setParameter("programid", getProgramid());
	knsql.executeUpdate(connection);
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("groupname",getGroupname());
	sql.setParameter("programid",getProgramid());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("groupname");
	setKeyField("programid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("groupname");
	setKeyField("programid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		if(rs.next()) {
			result++;
			fetchResult(rs);
			//#any result fetching fool again
			//#(220000) programmer code begin;
			//#(220000) programmer code end;
		}
		//#after fetching result set lovin each day
		//#(230000) programmer code begin;
		//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getGroupname()==null) || getGroupname().trim().equals("")) throw new java.sql.SQLException("Groupname is unspecified","groupname",-2008);
	if((getProgramid()==null) || getProgramid().trim().equals("")) throw new java.sql.SQLException("Programid is unspecified","programid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		while(rs.next()) {
			result++;
			//#addon statement if you come back
			//#(85000) programmer code begin;
			//#(85000) programmer code end;
			TProgGroupData aTProgGroupData = new TProgGroupData();
			aTProgGroupData.fetchResult(rs);
			add(aTProgGroupData);
			//#addon statement if you come back
			//#(90000) programmer code begin;
			//#(90000) programmer code end;
		}
		//#after scraping result set in too deep
		//#(240000) programmer code begin;
		//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public int deleteGroup(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	deleteFavor(connection,centerConnection,globalConnection,transientVar);
	setKeyField("groupname");
	ExecuteStatement sql = createQueryForDelete(connection);
	sql.setParameter("groupname",getGroupname());
	return sql.executeUpdate(connection);
}
public int deleteFavor(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	int result = 0;
	KnSQL knsql = new KnSQL(this);
	knsql.append("delete from tfavor where programid = ?programid ");
	KnSQL sql = new KnSQL(this);
	sql.append("select programid from tproggrp where groupname = ?groupname ");
	sql.setParameter("groupname",getGroupname());
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		while(rs.next()) {
			String programid = rs.getString("programid");
			knsql.clearParameters();
			knsql.setParameter("programid", programid);
			result += knsql.executeUpdate(connection);		
		}
	}
	return result;
}
//#(100000) programmer code end;
}
