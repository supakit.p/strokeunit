package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: TUSERDEVICEINFOContent.java
 * Description: TUSERDEVICEINFOContent class implements for handle tuserdeviceinfo data base schema.
 * Version: $Revision$
 * Creation date: Thu Aug 08 13:48:50 ICT 2019
 */
/**
 * TUSERDEVICEINFOContent class implements for handle tuserdeviceinfo data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;

@SuppressWarnings({"serial","unused"})
public class TUserDeviceInfoData extends BeanTable {

public TUserDeviceInfoData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tuserdeviceinfo");
	addSchema("id",java.sql.Types.INTEGER,true);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("user_id",java.sql.Types.VARCHAR);
	addSchema("device_id",java.sql.Types.VARCHAR);
	addSchema("os",java.sql.Types.VARCHAR);
	addSchema("device_token",java.sql.Types.VARCHAR);
	addSchema("active",java.sql.Types.TINYINT);
	addSchema("created_date",java.sql.Types.DATE);
	addSchema("created_time",java.sql.Types.TIME);
	applyDefault();
}
public int getId() {
	return getInt("id");
}
public void setId(int newId) {
	setMember("id",newId);
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getUserid() {
	return getString("user_id");
}
public void setUserid(String newUserid) {
	setMember("user_id",newUserid);
}
public String getDeviceid() {
	return getString("device_id");
}
public void setDeviceid(String newDeviceid) {
	setMember("device_id",newDeviceid);
}
public String getOs() {
	return getString("os");
}
public void setOs(String newOs) {
	setMember("os",newOs);
}
public String getDevicetoken() {
	return getString("device_token");
}
public void setDevicetoken(String newDevicetoken) {
	setMember("device_token",newDevicetoken);
}
public int getActive() {
	return getInt("active");
}
public void setActive(int newActive) {
	setMember("active",newActive);
}
public Date getCreateddate() {
	return getDate("created_date");
}
public void setCreateddate(Date newCreateddate) {
	setMember("created_date",newCreateddate);
}
public Time getCreatedtime() {
	return getTime("created_time");
}
public void setCreatedtime(Time newCreatedtime) {
	setMember("created_time",newCreatedtime);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setId(rs.getInt("id"));
	setSite(rs.getString("site"));
	setUserid(rs.getString("user_id"));
	setDeviceid(rs.getString("device_id"));
	setOs(rs.getString("os"));
	setDevicetoken(rs.getString("device_token"));
	setActive(rs.getInt("active"));
	setCreateddate(rs.getDate("created_date"));
	setCreatedtime(rs.getTime("created_time"));
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	sql.setParameter("id",getId());
	sql.setParameter("site",getSite());
	sql.setParameter("user_id",getUserid());
	sql.setParameter("device_id",getDeviceid());
	sql.setParameter("os",getOs());
	sql.setParameter("device_token",getDevicetoken());
	sql.setParameter("active",getActive());
	sql.setParameter("created_date",getCreateddate());
	sql.setParameter("created_time",getCreatedtime());
}
public int insert(java.sql.Connection connection) throws Exception {
	ExecuteStatement sql = createQueryForInsert(connection);
	assignParameters(sql);
	return sql.executeUpdate(connection);
}
public int update(java.sql.Connection connection) throws Exception {
	setKeyField("id");
	ExecuteStatement sql = createQueryForUpdate(connection);
	assignParameters(sql);
	return sql.executeUpdate(connection);
}
public int delete(java.sql.Connection connection) throws Exception {
	setKeyField("id");
	ExecuteStatement sql = createQueryForDelete(connection);
	sql.setParameter("id",getId());
	return sql.executeUpdate(connection);
}
public int retrieve(java.sql.Connection connection) throws Exception {
	setKeyField("id");
	ExecuteStatement sql = createQueryForRetrieve(connection);
	sql.setParameter("id",getId());
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		if(rs.next()) {
			result++;
			fetchResult(rs);
		}
		return result;
	}
}
}
