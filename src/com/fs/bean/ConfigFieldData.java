package com.fs.bean;
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
public class ConfigFieldData extends BeanData {
public ConfigFieldData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("m_ConfigField");
	//==============================
	//#Create initialize here....
addSchema("configid",java.sql.Types.INTEGER);
addSchema("configname",java.sql.Types.NVARCHAR);
addSchema("paramname",java.sql.Types.NVARCHAR);
addSchema("tablename",java.sql.Types.NVARCHAR);
addSchema("fieldname",java.sql.Types.NVARCHAR);
addSchema("fieldtype",java.sql.Types.NVARCHAR);
addSchema("iskey",java.sql.Types.BOOLEAN);
addSchema("isautokey",java.sql.Types.BOOLEAN);
addSchema("isarray",java.sql.Types.BOOLEAN);
addSchema("isactive",java.sql.Types.BOOLEAN);

map("configid","configid");
map("configname","configname");
map("paramname","paramname");
map("tablename","tablename");
map("fieldname","fieldname");
map("fieldtype","fieldtype");
map("iskey","iskey");
map("isautokey","isautokey");
map("isarray","isarray");
map("isactive","isactive");
	//===============================
}
public String fetchVersion() {
	return super.fetchVersion()+ConfigFieldData.class+"=$Revision$\n";
}
 	 //==============================
	//#Create get-set method here....
 public int getConfigid() {
	return getInt("configid");
 }
 public void setConfigid(int  configid) {
	setMember("configid",configid);
 }
 public String getConfigname() {
	return getString("configname");
 }
 public void setConfigname(String  configname) {
	setMember("configname",configname);
 }
 public String getParamname() {
	return getString("paramname");
 }
 public void setParamname(String  paramname) {
	setMember("paramname",paramname);
 }
 public String getTablename() {
	return getString("tablename");
 }
 public void setTablename(String  tablename) {
	setMember("tablename",tablename);
 }
 public String getFieldname() {
	return getString("fieldname");
 }
 public void setFieldname(String  fieldname) {
	setMember("fieldname",fieldname);
 }
 public String getFieldtype() {
	return getString("fieldtype");
 }
 public void setFieldtype(String  fieldtype) {
	setMember("fieldtype",fieldtype);
 }
 public boolean getIskey() {
	return getBoolean("iskey");
 }
 public void setIskey(boolean  iskey) {
	setMember("iskey",iskey);
 }
 public boolean getIsautokey() {
	return getBoolean("isautokey");
 }
 public void setIsautokey(boolean  isautokey) {
	setMember("isautokey",isautokey);
 }
 public boolean getIsarray() {
	return getBoolean("isarray");
 }
 public void setIsarray(boolean  isarray) {
	setMember("isarray",isarray);
 }
 public boolean getIsactive() {
	return getBoolean("isactive");
 }
 public void setIsactive(boolean  isactive) {
	setMember("isactive",isactive);
 }
 public Timestamp getCreatedate() {
	return getTimestamp("createdate");
 }
 public void setCreatedate(Timestamp  createdate) {
	setMember("createdate",createdate);
 }
 public Timestamp getUpdatedate() {
	return getTimestamp("updatedate");
 }
 public void setUpdatedate(Timestamp  updatedate) {
	setMember("updatedate",updatedate);
 }
	//===============================
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	 //==============================
	//#Create obtain here....
setConfigid(bean.getFieldByName(mapper("configid")).asInt());
setConfigname(bean.getFieldByName(mapper("configname")).asString());
setParamname(bean.getFieldByName(mapper("paramname")).asString());
setTablename(bean.getFieldByName(mapper("tablename")).asString());
setFieldname(bean.getFieldByName(mapper("fieldname")).asString());
setFieldtype(bean.getFieldByName(mapper("fieldtype")).asString());
setIskey(bean.getFieldByName(mapper("iskey")).asBoolean());
setIsautokey(bean.getFieldByName(mapper("isautokey")).asBoolean());
setIsarray(bean.getFieldByName(mapper("isarray")).asBoolean());
setIsactive(bean.getFieldByName(mapper("isactive")).asBoolean());
setCreatedate(bean.getFieldByName(mapper("createdate")).asTimestamp());
setUpdatedate(bean.getFieldByName(mapper("updatedate")).asTimestamp());
	//===============================
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	//==============================
	//#Create fetchResult here....
setConfigid(rs.getInt("ConfigId"));
setConfigname(rs.getString("ConfigName"));
setParamname(rs.getString("ParamName"));
setTablename(rs.getString("TableName"));
setFieldname(rs.getString("FieldName"));
setFieldtype(rs.getString("FieldType"));
setIskey(rs.getBoolean("IsKey"));
setIsautokey(rs.getBoolean("IsAutoKey"));
setIsarray(rs.getBoolean("IsArray"));
setIsactive(rs.getBoolean("IsActive"));
setCreatedate(rs.getTimestamp("CreateDate"));
setUpdatedate(rs.getTimestamp("UpdateDate"));
	//===============================
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//==============================
		//#Create Param here....
sql.setParameter("configid",getConfigid());
sql.setParameter("configname",getConfigname());
	//===============================
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	ExecuteStatement sql = createQueryForInsert(connection);
 sql.clear();	//#another modification keep on loving you
	assignParameters(sql);
	//#assigned parameter always on my mind
	return sql.executeUpdate(connection);
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	ExecuteStatement sql = createQueryForCollect(connection);
        sql.append(" ORDER BY configName");
//        sql.clear();	//#any collect statement would you be happier
	//#assigned parameters this temptation
        assignParameters(sql);
	int result = 0;
	java.sql.ResultSet rs = sql.executeQuery(connection);
	while(rs.next()) {
		result++;
		ConfigFieldData aConfigFieldData = new ConfigFieldData();
		aConfigFieldData.fetchResult(rs);
		add(aConfigFieldData);
	}
	close(rs);
	return result;
}

public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
   	setKeyField("RoleID");
 
   	ExecuteStatement sql = createQueryForRetrieve(connection);
 
	sql.clear();
//sql.append("");
	 
	
	
	int result = 0;
//	java.sql.ResultSet rs = sql.executeQuery(connection);
//if(rs.next()) {
//	result++;
//	fetchResult(rs);
	 
//	}
	 
//close(rs);
	return result;
   }public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("id");
	ExecuteStatement sql = createQueryForUpdate(connection);
 sql.clear();	
	//#assigned parameters all rise
	assignParameters(sql);
	//#assigned parameters all rise
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
}
}