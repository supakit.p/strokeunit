package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: PMTE031AData.java
 * Description: PMTE031AData class implements for handle tuser data base schema.
 * Version: $Revision$
 * Creation date: Thu Oct 18 10:52:15 ICT 2018
 */
/**
 * PMTE031AData class implements for handle tuser data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.auth.PasswordLibrary;
import com.fs.dev.auth.ResetPasswordData;
import com.fs.dev.mail.ScriptParser;
import com.fs.dev.mail.SendMail;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE007AData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
private String password = null;
//#(20000) programmer code end;
public SFTE007AData() {
	super();
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	if(getEditdate()==null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if(getEdittime()==null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	//#(75000) programmer code end;
	sql.setParameter("site",getSite());
	sql.setParameter("employeeid",getEmployeeid());
	sql.setParameter("userid",getUserid());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
protected void initialize() {
	super.initialize();
	setTable("tuserinfo");
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("employeeid",java.sql.Types.VARCHAR);
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("employeecode",java.sql.Types.VARCHAR);
	addSchema("usertname",java.sql.Types.VARCHAR);
	addSchema("usertsurname",java.sql.Types.VARCHAR);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("edituser",java.sql.Types.VARCHAR);
	addSchema("userbranch",java.sql.Types.VARCHAR);
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	map("site","site");
	map("userid","userid");
	map("employeeid","employeeid");
	map("employeecode","employeecode");
	map("usertname","usertname");
	map("usertsurname","usertsurname");
	map("userbranch","userbranch");
	addSchema("status",java.sql.Types.VARCHAR,true);
	addSchema("usertype",java.sql.Types.VARCHAR,true);
	addSchema("siteflag",java.sql.Types.VARCHAR,true);
	addSchema("branchflag",java.sql.Types.VARCHAR,true);
	addSchema("approveflag",java.sql.Types.VARCHAR,true);
	addSchema("usersites",java.sql.Types.VARCHAR,true);
	addSchema("userroles",java.sql.Types.VARCHAR,true);
	addSchema("usergroups",java.sql.Types.VARCHAR,true);
	addSchema("userbranches",java.sql.Types.VARCHAR,true);
	addSchema("email",java.sql.Types.VARCHAR,true);
	addSchema("username",java.sql.Types.VARCHAR,true);
	addSchema("userpassword",java.sql.Types.VARCHAR,true);
	addSchema("effectdate",java.sql.Types.DATE,true);
	addSchema("sitedesc",java.sql.Types.VARCHAR,true);
	map("status","status");
	map("siteflag","siteflag");
	map("branchflag","branchflag");
	map("usersites","usersites");
	map("userroles","userroles");
	map("usergroups","usergroups");
	map("userbranches","userbranches");
	map("email","email");
	map("username","username");
	map("userpassword","userpassword");
	map("effectdate","effectdate");
	map("sitedesc","sitedesc");
	//#(30000) programmer code end;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		com.fs.bean.CustomData aCustomData = new com.fs.bean.CustomData();
		aCustomData.fetchResult(rs);
		add(aCustomData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
public void composePassword() {
	String pwd = createPassword();
	setPassword(pwd);
	PasswordLibrary plib = new PasswordLibrary(this);
	pwd = plib.encrypt(pwd);
	setUserpassword(pwd);
}
public String createPassword() {
	String pass = "password";
	Object initpwd = GlobalVariable.getVariable("INITIAL_PASSWORD");
	if(initpwd!=null && initpwd.toString().trim().length()>0) {
		pass = initpwd.toString();
	}
	Object dynamic = GlobalVariable.getVariable("DYNAMIC_INITIAL_PASSWORD");
	if(dynamic!=null && "true".equalsIgnoreCase(dynamic.toString())) {
		pass = ResetPasswordData.createPassword();
	}
	return pass;
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setSite(rs.getString("site"));
	setUserid(rs.getString("userid"));
	setEmployeeid(rs.getString("employeeid"));
	setEmployeecode(rs.getString("employeecode"));
	setUsertname(rs.getString("usertname"));
	setUsertsurname(rs.getString("usertsurname"));
	setEmail(rs.getString("email"));
	setEffectdate(rs.getDate("effectdate"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	setUserbranch(rs.getString("userbranch"));
	//#(60000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE007AData.class+"=$Revision$\n";
}
public Date getEditdate() {
	return getDate("editdate");
}
public Time getEdittime() {
	return getTime("edittime");
}
public String getEdituser() {
	return getString("edituser");
}
public String getEmail() {
	return getString("email");
}
public String getEmployeecode() {
	return getString("employeecode");
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public String getEmployeeid() {
	return getString("employeeid");
}
public String getSite() {
	return getString("site");
}
public String getSitedesc() {
	return getString("sitedesc");
}
public String getSiteflag() {
	return getString("siteflag");
}
public String getBranchflag() {
	return getString("branchflag");
}
public String getStatus() {
	return getString("status");
}
public String getUsergroups() {
	return getString("usergroups");
}
public String getUserid() {
	return getString("userid");
}
public String getUsername() {
	return getString("username");
}
public String getUserpassword() {
	return getString("userpassword");
}
public String getUserroles() {
	return getString("userroles");
}
public String getUsersites() {
	return getString("usersites");
}
public String getUsertname() {
	return getString("usertname");
}
public String getUsertsurname() {
	return getString("usertsurname");
}
public String getUserbranches() {
	return getString("userbranches");
}
public Date getEffectdate() {
	return getDate("effectdate");
}
public String getUserbranch() {
	return getString("userbranch");
}
public void setUserbranch(String newUserbranch) {
	setMember("userbranch",newUserbranch);
}
public String findUserid(String email,java.sql.Connection connection) throws Exception {
	if(email==null) throw new BeanException("Email user not found",-19910);
	String suffix = null;
	String userid = email;
	int index = email.indexOf('@');
	if(index>0) {
		suffix = email.substring(index);
		userid = email.substring(0, index); 
	}	
	index = 0;
	boolean found = false;
	KnSQL knsql = new KnSQL(this);
	knsql.append("select userid from tuser where userid=?userid ");
	do {
		found = false;
		userid = userid+(index>0?""+index:"")+(suffix==null?"":suffix);
		knsql.clearParameters();
		knsql.setParameter("userid", userid);
		try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
			if(rs.next()) {
				found = true;
			}
		}
		index++;
	}while(found);
	return userid;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	int result = 0;
	if(getEditdate()==null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if(getEdittime()==null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	String email = getEmail();
	if(email==null) throw new BeanException("Email user not found",-19910);
	String userid = findUserid(email,connection);
	setUserid(userid);
	setUsername(userid);
	composePassword();
	TUserData user = new TUserData();
	user.init(global);
	user.setRelatable(this);
	user.retain(this);
	result = user.insert(connection, centerConnection, globalConnection, transientVar);
	result += updateUser(connection,centerConnection,globalConnection,transientVar);
	KnSQL knsql = new KnSQL(this); 
	knsql.append("update tuserinfo set userid = ?userid, editdate = ?editdate, ");
	knsql.append("edittime = ?edittime, edituser = ?edituser, effectdate = ?effectdate, remarks = ?remarks ");
	knsql.append("where site = ?site and employeeid = ?employeeid ");
	knsql.setParameter("site", getSite());
	knsql.setParameter("employeeid", getEmployeeid());
	knsql.setParameter("userid", getUserid());
	knsql.setParameter("editdate", getEditdate());
	knsql.setParameter("edittime", getEdittime());
	knsql.setParameter("edituser", getEdituser());
	knsql.setParameter("effectdate", getEffectdate());
	knsql.setParameter("remarks", global==null?"pmte031":global.getFsProg());
	result += knsql.executeUpdate(connection);
	knsql.clear();
	knsql.append("insert into tuserinfohistory ");
	knsql.append("select * from tuserinfo where site = ?site and employeeid = ?employeeid ");
	knsql.setParameter("site", getSite());
	knsql.setParameter("employeeid", getEmployeeid());
	knsql.executeUpdate(connection);		
	setUserpassword(getPassword());
	return result;
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setSite(bean.getFieldByName(mapper("site")).asString());
	setEmployeeid(bean.getFieldByName(mapper("employeeid")).asString());
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUsername(bean.getFieldByName(mapper("username")).asString());
	setStatus(bean.getFieldByName(mapper("status")).asString());
	setEmail(bean.getFieldByName(mapper("email")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	setUsersites(bean.getFieldByName(mapper("usersites")).asString());
	setUserroles(bean.getFieldByName(mapper("userroles")).asString());
	setUsergroups(bean.getFieldByName(mapper("usergroups")).asString());
	setUserbranches(bean.getFieldByName(mapper("userbranches")).asString());
	setEdituser(global==null?getCurrentUser():global.getFsUser());
	setSiteflag(bean.getFieldByName(mapper("siteflag")).asString());
	setBranchflag(bean.getFieldByName(mapper("branchflag")).asString());
	setEffectdate(bean.getFieldByName(mapper("effectdate")).asDate());
	if(getSiteflag()==null || getSiteflag().trim().length()<=0) {
		setSiteflag("0");
	}
	if(getBranchflag()==null || getBranchflag().trim().length()<=0) {
		setBranchflag("0");
	}
	if(getStatus()==null || getStatus().trim().length()<=0) setStatus("A");
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("site");
	setKeyField("employeeid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	sql.clear();
	sql.append("select tuserinfo.site,tuserinfo.employeeid,tuserinfo.userid,tuserinfo.userbranch,");
	sql.append("tuserinfo.usertname,tuserinfo.usertsurname,tuserinfo.userename,tuserinfo.useresurname,");
	sql.append("tuserinfo.employeecode,tuserinfo.email,tuserinfo.effectdate,");
	sql.append("tuser.status,tuser.siteflag,tuser.branchflag,tuser.usertype,tuser.username ");
	sql.append("from tuserinfo ");
	sql.append("left join tuser on tuser.userid = tuserinfo.userid ");
	sql.append("where tuserinfo.site = ?site ");
	sql.append("and tuserinfo.employeeid = ?employeeid ");
	sql.setParameter("site", getSite());
	sql.setParameter("employeeid", getEmployeeid());
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		setStatus(rs.getString("status"));
		setSiteflag(rs.getString("siteflag"));
		setBranchflag(rs.getString("branchflag"));
		setUsername(rs.getString("username"));
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	if(result>0) {
		if(getUserid()!=null && getUserid().trim().length()>0) {
			StringBuilder buf = new StringBuilder();
			KnSQL knsql = new KnSQL(this);
			knsql.append("select * from tusercomp where userid = ?userid ");
			knsql.setParameter("userid", getUserid());
			try(java.sql.ResultSet urs = knsql.executeQuery(connection)) {
				while(urs.next()) {
					String site = urs.getString("site");
					if(buf.length()>0) buf.append(",");
					buf.append(site);
				}
			}
			setUsersites(buf.toString());
			buf.setLength(0);
			knsql.clear();
			knsql.append("select * from tuserbranch where site = ?site and userid = ?userid ");
			knsql.setParameter("site", getSite());
			knsql.setParameter("userid", getUserid());
			try(java.sql.ResultSet urs = knsql.executeQuery(connection)) {
				while(urs.next()) {
					String branch = urs.getString("branch");
					if(buf.length()>0) buf.append(",");
					buf.append(branch);
				}
			}
			setUserbranches(buf.toString());			
			buf.setLength(0);
			knsql.clear();
			knsql.append("select * from tuserrole where userid = ?userid ");
			knsql.setParameter("userid", getUserid());
			try(java.sql.ResultSet urs = knsql.executeQuery(connection)) {
				while(urs.next()) {
					String role = urs.getString("roleid");
					if(buf.length()>0) buf.append(",");
					buf.append(role);
				}
			}
			setUserroles(buf.toString());
			buf.setLength(0);
			knsql.clear();
			knsql.append("select * from tusergrp where userid = ?userid ");
			knsql.setParameter("userid", getUserid());
			try(java.sql.ResultSet urs = knsql.executeQuery(connection)) {
				while(urs.next()) {
					String group = urs.getString("groupname");
					if(buf.length()>0) buf.append(",");
					buf.append(group);
				}
			}
			setUsergroups(buf.toString());
		}
	}
	//#(230000) programmer code end;
	}
	return result;
}
public void setEditdate(Date newEditdate) {
	setMember("editdate",newEditdate);
}
public void setEdittime(Time newEdittime) {
	setMember("edittime",newEdittime);
}
public void setEdituser(String newEdituser) {
	setMember("edituser",newEdituser);
}
public void setEmail(String newEmail) {
	setMember("email",newEmail);
}
public void setEmployeecode(String newEmployeecode) {
	setMember("employeecode",newEmployeecode);
}
public void setEmployeeid(String newEmployeeid) {
	setMember("employeeid",newEmployeeid);
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public void setSitedesc(String newSitedesc) {
	setMember("sitedesc",newSitedesc);
}
public void setSiteflag(String newSiteflag) {
	setMember("siteflag",newSiteflag);
}
public void setBranchflag(String newBranchflag) {
	setMember("branchflag",newBranchflag);
}
public void setStatus(String newStatus) {
	setMember("status",newStatus);
}
public void setUsergroups(String newUsergroups) {
	setMember("usergroups",newUsergroups);
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
public void setUserpassword(String newUserpassword) {
	setMember("userpassword",newUserpassword);
}
public void setUserroles(String newUserroles) {
	setMember("userroles",newUserroles);
}
public void setUsersites(String newUsersites) {
	setMember("usersites",newUsersites);
}
public void setUserbranches(String newUserbranches) {
	setMember("userbranches",newUserbranches);
}
public void setUsertname(String newUsertname) {
	setMember("usertname",newUsertname);
}
public void setUsertsurname(String newUsertsurname) {
	setMember("usertsurname",newUsertsurname);
}
public void setEffectdate(Date newEffectdate) {
	setMember("effectdate",newEffectdate);
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("site");
	setKeyField("employeeid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	if(getEditdate()==null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if(getEdittime()==null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	updateUser(connection,centerConnection,globalConnection,transientVar);
	KnSQL knsql = new KnSQL(this);
	knsql.append("update tuserinfo set effectdate = ?effectdate, editdate = ?editdate, edittime = ?edittime, edituser = ?edituser, remarks = ?remarks ");
	knsql.append("where site = ?site and userid = ?userid ");
	knsql.setParameter("site", getSite());
	knsql.setParameter("userid", getUserid());
	knsql.setParameter("effectdate", getEffectdate());
	knsql.setParameter("editdate", getEditdate());
	knsql.setParameter("edittime", getEdittime());
	knsql.setParameter("edituser", getEdituser());
	knsql.setParameter("remarks", global==null?"pmte031":global.getFsProg());
	knsql.executeUpdate(connection);	
	knsql.clear();
	knsql.append("insert into tuserinfohistory ");
	knsql.append("select * from tuserinfo where site = ?site and userid = ?userid ");
	knsql.setParameter("site", getSite());
	knsql.setParameter("userid", getUserid());
	knsql.executeUpdate(connection);	
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public void setPassword(String password) {
	this.password = password;
}
public String getPassword() {
	return password;
}
public int updateUser(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	int result = 0;
	TUserType usertype = updateUserInfo(connection,centerConnection,globalConnection,transientVar);
	if(usertype!=null) {
		String fs_user_email = "";
		String fs_user_nameth = "";
		String fs_user_nameen = "";
		String oldusername = null;
		boolean updateflag = false;
		if(getUsername()!=null && getUsername().trim().length()>0) {
			KnSQL sql = new KnSQL(this);
			sql.append("select username from tuser where userid = ?userid ");
			sql.setParameter("userid", getUserid());
			try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
				if(rs.next()) {
					oldusername = rs.getString("username");
				}
			}
			updateflag = oldusername!=null && !oldusername.equalsIgnoreCase(getUsername());
			if(updateflag) {
				sql.clear();
				sql.append("select email,usertname,usertsurname,userename,useresurname from tuserinfo where userid=?userid ");
				sql.setParameter("userid", getUserid());
				try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
					if(rs.next()) {
						fs_user_email = rs.getString("email");
						String name = rs.getString("usertname");
						String surname = rs.getString("usertsurname");
						fs_user_nameth = (name==null?"":name)+" "+(surname==null?"":surname);
						name = rs.getString("userename");
						surname = rs.getString("useresurname");
						fs_user_nameen = (name==null?"":name)+" "+(surname==null?"":surname);
					}
				}
			}
		}
		result = 1;
		KnSQL knsql = new KnSQL(this); 
		knsql.append("update tuser set usertype = ?usertype , approveflag = ?approveflag, ");
		knsql.append("status = ?status, siteflag = ?siteflag, branchflag = ?branchflag, ");
		knsql.append("editdate = ?editdate, edittime = ?edittime, edituser = ?edituser ");
		if(updateflag) knsql.append(", username = ?username ");
		knsql.append("where userid = ?userid ");
		knsql.setParameter("userid", getUserid());
		knsql.setParameter("approveflag",usertype.getApproveflag());
		knsql.setParameter("usertype", usertype==null?null:usertype.getUsertype());
		knsql.setParameter("status", getStatus());
		knsql.setParameter("siteflag", getSiteflag());
		knsql.setParameter("branchflag", getBranchflag());
		knsql.setParameter("editdate", getEditdate());
		knsql.setParameter("edittime", getEdittime());
		knsql.setParameter("edituser", getEdituser());
		knsql.setParameter("username", getUsername());
		result = knsql.executeUpdate(connection);
		if(updateflag && (fs_user_email!=null && fs_user_email.trim().length()>0)) {
			java.util.Map map = (java.util.Map)XMLConfig.getConfig("USERIDMAIL",connection);
			Trace.info("USERIDMAIL = "+map);
			if(map!=null) {
				String contents = null;
				knsql.clear();
				knsql.append("select * from ttemplate where template=?template and templatetype=?templatetype ");
				knsql.setParameter("template", "USERID");
				knsql.setParameter("templatetype", "CONFIRM");
				try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
					if(rs.next()) {
						contents = rs.getString("contents");
					}
				}
				StringBuilder msg = new StringBuilder();
				StringBuilder buf = new StringBuilder();
				if(contents!=null && contents.length()>0) {
					String[] texts = contents.split("\n");
					for(String s : texts) buf.append(s).append("<br/>");
				} else {
					buf.append("Dear, ${usertname}<br/>");
					buf.append("<br/>");
					buf.append("Your access user id was changed<br/>");
					buf.append("From : ${fromusername}<br/>");
					buf.append("To : ${tousername}<br/>");
					buf.append("You can try out with the new access into the system.<br/>");
					buf.append("<br/>");
					buf.append("Should you encounter any difficulties, please contact system administrator.<br/>");
					buf.append("<br/>");
					buf.append("Regards,<br/>");
					buf.append("HR. Administrator");
					buf.append("<br/>"); 
				}
				java.util.Map<String,String> transientMap = new java.util.HashMap<>();
				transientMap.put("usertname", fs_user_nameth);
				transientMap.put("userename",fs_user_nameen);
				transientMap.put("fromusername",oldusername);
				transientMap.put("tousername",getUsername());
				msg.append(ScriptParser.translateVariables(buf.toString(), transientMap));
				Object userMail = GlobalVariable.getVariable("USERID_MAIL_CONTROL");
				if(!(userMail!=null && "false".equalsIgnoreCase(userMail.toString()))) { 
					SendMail mailer = new SendMail();
					mailer.obtain(map);
					mailer.setTo(fs_user_email);
					mailer.setMessage(msg.toString());
					if(!(System.getProperty("test.mail.control")!=null)) {
						mailer.start();
						Trace.debug(this,">>> sending mail from : "+mailer.getFrom()+", to : "+mailer.getTo());
					}
				}				
			}
		}
	}
	return result;
}
public TUserType updateUserInfo(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	java.util.Map<String,TUserType> utmap = new java.util.HashMap<>();
	java.util.Map<String,String> rolemap = new java.util.HashMap<>();
	KnSQL knsql = new KnSQL(this);
	knsql.append("select tgroup.groupname,tgroup.usertype,tusertype.level from tgroup ");
	knsql.append("left join tusertype on tusertype.usertype = tgroup.usertype ");
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		while(rs.next()) {
			TUserType ut = new TUserType();
			ut.fetchResult(rs);
			utmap.put(ut.getGroupname(), ut);
		}
	}
	knsql.clear();
	knsql.append("select roleid,approveflag from trole ");
	knsql.append("where site = ?site ");
	knsql.setParameter("site", getSite());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		while(rs.next()) {
			String roleid = rs.getString("roleid");
			String flag = rs.getString("approveflag");
			rolemap.put(roleid, flag==null || flag.trim().length()<=0?"0":flag);
		}
	}
	knsql.clear();
	knsql.append("delete from tusercomp where userid = ?userid ");
	knsql.setParameter("userid", getUserid());
	knsql.executeUpdate(connection);
	knsql.clear();
	knsql.append("insert into tusercomp(userid,site) values(?userid,?site)");
	String[] sites = BeanUtility.split(getUsersites());
	if(sites!=null) {
		for(String id : sites) {
			knsql.clearParameters();
			knsql.setParameter("userid", getUserid());
			knsql.setParameter("site", id);
			knsql.executeUpdate(connection);
		}
	}	
	knsql.clear();
	knsql.append("delete from tuserbranch where site = ?site and userid = ?userid ");
	knsql.setParameter("site", getSite());
	knsql.setParameter("userid", getUserid());
	knsql.executeUpdate(connection);
	knsql.clear();
	knsql.append("insert into tuserbranch(site,branch,userid) values(?site,?branch,?userid)");
	String[] branches = BeanUtility.split(getUserbranches());
	if(branches!=null) {
		for(String id : branches) {
			knsql.clearParameters();
			knsql.setParameter("site", getSite());
			knsql.setParameter("branch", id);
			knsql.setParameter("userid", getUserid());
			knsql.executeUpdate(connection);
		}
	}	
	TUserType usertype = new TUserType();
	String approveflag = "0";
	knsql.clear();
	knsql.append("delete from tuserrole where userid = ?userid ");
	knsql.setParameter("userid", getUserid());
	knsql.executeUpdate(connection);
	knsql.clear();
	knsql.append("insert into tuserrole(userid,roleid) values(?userid,?roleid)");
	String[] roles = BeanUtility.split(getUserroles());
	if(roles!=null) {
		for(String id : roles) {
			knsql.clearParameters();
			knsql.setParameter("userid", getUserid());
			knsql.setParameter("roleid", id);
			knsql.executeUpdate(connection);
			String flag = rolemap.get(id);
			if("1".equals(flag)) {
				approveflag = flag;
			}
		}
	}
	knsql.clear();
	knsql.append("delete from tusergrp where userid = ?userid ");
	knsql.setParameter("userid", getUserid());
	knsql.executeUpdate(connection);
	knsql.clear();
	knsql.append("insert into tusergrp(userid,groupname) values(?userid,?groupname)");
	String[] groups = BeanUtility.split(getUsergroups());
	if(groups!=null) {
		for(String id : groups) {
			knsql.clearParameters();
			knsql.setParameter("userid", getUserid());
			knsql.setParameter("groupname", id);
			knsql.executeUpdate(connection);
			TUserType ut = utmap.get(id);
			if(ut!=null) {
				if(usertype==null) usertype = ut;
				if(usertype.getLevel() < ut.getLevel()) usertype = ut;
			}
		}
	}	
	usertype.setApproveflag(approveflag);
	return usertype;
}
//#(100000) programmer code end;
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
}
