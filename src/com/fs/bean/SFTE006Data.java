package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE006Data.java
 * Description: SFTE006Data class implements for handle tcomp data base schema.
 * Version: $Revision$
 * Creation date: Mon Apr 01 14:23:18 ICT 2019
 */
/**
 * SFTE006Data class implements for handle tcomp data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.TheUtility;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE006Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE006Data() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tcomp");
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("shortname",java.sql.Types.VARCHAR);
	addSchema("nameen",java.sql.Types.VARCHAR);
	addSchema("nameth",java.sql.Types.VARCHAR);
	addSchema("sitedesc",java.sql.Types.VARCHAR,true);
	addSchema("products",java.sql.Types.VARCHAR,true);
	addSchema("productcontent",java.sql.Types.VARCHAR,true);
	map("nameth","nameth");
	map("shortname","shortname");
	map("site","site");
	map("sitedesc","sitedesc");
	map("nameen","nameen");
	map("products","products");
	map("productcontent","productcontent");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE006Data.class+"=$Revision$\n";
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getShortname() {
	return getString("shortname");
}
public void setShortname(String newShortname) {
	setMember("shortname",newShortname);
}
public String getNameen() {
	return getString("nameen");
}
public void setNameen(String newNameen) {
	setMember("nameen",newNameen);
}
public String getNameth() {
	return getString("nameth");
}
public void setNameth(String newNameth) {
	setMember("nameth",newNameth);
}
public String getProducts() {
	return getString("products");
}
public void setProducts(String newProducts) {
	setMember("products",newProducts);
}
public String getProductcontent() {
	return getString("productcontent");
}
public void setProductcontent(String newProductcontent) {
	setMember("productcontent",newProductcontent);
}
public String getSitedesc() {
	return getString("sitedesc");
}
public void setSitedesc(String newSitedesc) {
	setMember("sitedesc",newSitedesc);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setSite(bean.getFieldByName(mapper("site")).asString());
	setShortname(bean.getFieldByName(mapper("shortname")).asString());
	setNameen(bean.getFieldByName(mapper("nameen")).asString());
	setNameth(bean.getFieldByName(mapper("nameth")).asString());
	setProducts(bean.getFieldByName(mapper("products")).asString());
	setProductcontent(bean.getFieldByName(mapper("productcontent")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	if(getSite()==null || getSite().trim().length()<0) {
		if(global!=null) setSite(global.getFsSite());
	}
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setSite(rs.getString("site"));
	setShortname(rs.getString("shortname"));
	setNameen(rs.getString("nameen"));
	setNameth(rs.getString("nameth"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("site",getSite());
	sql.setParameter("shortname",getShortname());
	sql.setParameter("nameen",getNameen());
	sql.setParameter("nameth",getNameth());
	sql.setParameter("products",getProducts());
	sql.setParameter("productcontent",getProductcontent());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("site");
	int rows = 0;
	KnSQL delsql = new KnSQL(this);
	delsql.append("delete from tcompprod where site = ?site ");
	delsql.setParameter("site", getSite());
	rows = delsql.executeUpdate(connection);
	String[] products = BeanUtility.split(getProducts());
	if (products != null) {
		KnSQL knsql = new KnSQL(this);
		knsql.append("insert into tcompprod (site,product) values (?site,?product) ");
		for (String product : products) {
			if(product!=null && product.trim().length()>0) {
				knsql.clearParameters();
				knsql.setParameter("site", getSite());
				knsql.setParameter("product", product);
				rows += knsql.executeUpdate(connection);
			}
		}
	}
	return rows;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("site");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	if(result>0) {
		StringBuilder buf = new StringBuilder();
		StringBuilder sb = new StringBuilder();
		boolean eng = TheUtility.isEnglish(global);
		KnSQL knsql = new KnSQL(this);
		knsql.append("select tcompprod.product,");
		if(eng) {
			knsql.append("tprod.nameen as productname ");
		} else {
			knsql.append("tprod.nameth as productname ");
		}
		knsql.append("from tcompprod ");
		knsql.append("left join tprod on tprod.product = tcompprod.product ");
		knsql.append("where tcompprod.site = ?site ");
		knsql.setParameter("site", getSite());
		try(java.sql.ResultSet ars = knsql.executeQuery(connection)) {
		while(ars.next()) {
			if(buf.length()>0) buf.append(",");
			buf.append(ars.getString("product"));
			if(sb.length()>0) sb.append(",");
			sb.append(ars.getString("productname"));
		}
		}
		setProducts(buf.toString());
		setProductcontent(sb.toString());
		searchProducts(connection);
	}
	//#(230000) programmer code end;
	}
	return result;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	boolean eng = TheUtility.isEnglish(global);
	KnSQL knsql = new KnSQL(this);
	knsql.append("select tcompprod.product,");
	if(eng) {
		knsql.append("tprod.nameen as productname ");
	} else {
		knsql.append("tprod.nameth as productname ");
	}
	knsql.append("from tcompprod ");
	knsql.append("left join tprod on tprod.product = tcompprod.product ");
	knsql.append("where tcompprod.site = ?site ");
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	sql.clear();
	sql.append("select site,shortname,nameen,nameth from tcomp ");
	sql.append("where ( site = headsite or headsite is null ) ");
	if(getShortname()!=null && getShortname().trim().length()>0) {
		sql.append("and shortname like ?shortname ");
		sql.setParameter("shortname", "%"+getShortname()+"%");
	}
	if(getNameen()!=null && getNameen().trim().length()>0) {
		sql.append("and nameen like ?nameen ");
		sql.setParameter("nameen", "%"+getNameen()+"%");
	}
	if(getNameth()!=null && getNameth().trim().length()>0) {
		sql.append("and nameth like ?nameth ");
		sql.setParameter("nameth", "%"+getNameth()+"%");
	}
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE006Data aSFTE006Data = new SFTE006Data();
		aSFTE006Data.fetchResult(rs);
		add(aSFTE006Data);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		String site = rs.getString("site");
		StringBuilder buf = new StringBuilder();
		StringBuilder sb = new StringBuilder();
		knsql.clearParameters();
		knsql.setParameter("site", site);
		java.sql.ResultSet ars = knsql.executeQuery(connection);
		while(ars.next()) {
			if(buf.length()>0) buf.append(",");
			buf.append(ars.getString("product"));
			if(sb.length()>0) sb.append(",");
			String productname = ars.getString("productname");
			if(productname!=null && productname.trim().length()>0) {
				sb.append(productname);
			}
		}
		close(ars);
		aSFTE006Data.setProducts(buf.toString());
		aSFTE006Data.setProductcontent(sb.toString());		
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public int searchProducts(java.sql.Connection connection) throws Exception {
	int result = 0;
	this.list().clear();
	KnSQL knsql = new KnSQL(this);
	knsql.append("select product,nameen,nameth,verified from tprod order by verified ");
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		while(rs.next()) {
			com.fs.bean.ExecuteData aData = new com.fs.bean.ExecuteData();
			aData.fetchResult(rs);
			this.list().add(aData);		
		}
	}
	return result;
}
//#(100000) programmer code end;
/*
//#(revision) programmer code begin;
$Revision$
//#(revision) programmer code end;
*/
}
