package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: TUserBankData.java
 * Description: TUserBankData class implements for handle tuserbank data base schema.
 * Version: $Revision$
 * Creation date: Thu Jun 13 13:29:50 ICT 2019
 */
/**
 * TUserBankData class implements for handle tuserbank data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class TUserBankData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public TUserBankData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tuserbank");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("accountno",java.sql.Types.VARCHAR);
	addSchema("accounttype",java.sql.Types.VARCHAR);
	addSchema("accountname",java.sql.Types.VARCHAR);
	addSchema("bankid",java.sql.Types.VARCHAR);
	addSchema("bankbranchid",java.sql.Types.VARCHAR);
	addSchema("bankbranchname",java.sql.Types.VARCHAR);
	addSchema("remarks",java.sql.Types.VARCHAR);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("edituser",java.sql.Types.VARCHAR);
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+TUserBankData.class+"=$Revision$\n";
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getAccountno() {
	return getString("accountno");
}
public void setAccountno(String newAccountno) {
	setMember("accountno",newAccountno);
}
public String getAccounttype() {
	return getString("accounttype");
}
public void setAccounttype(String newAccounttype) {
	setMember("accounttype",newAccounttype);
}
public String getAccountname() {
	return getString("accountname");
}
public void setAccountname(String newAccountname) {
	setMember("accountname",newAccountname);
}
public String getBankid() {
	return getString("bankid");
}
public void setBankid(String newBankid) {
	setMember("bankid",newBankid);
}
public String getBankbranchid() {
	return getString("bankbranchid");
}
public void setBankbranchid(String newBankbranchid) {
	setMember("bankbranchid",newBankbranchid);
}
public String getBankbranchname() {
	return getString("bankbranchname");
}
public void setBankbranchname(String newBankbranchname) {
	setMember("bankbranchname",newBankbranchname);
}
public String getRemarks() {
	return getString("remarks");
}
public void setRemarks(String newRemarks) {
	setMember("remarks",newRemarks);
}
public Date getEditdate() {
	return getDate("editdate");
}
public void setEditdate(Date newEditdate) {
	setMember("editdate",newEditdate);
}
public Time getEdittime() {
	return getTime("edittime");
}
public void setEdittime(Time newEdittime) {
	setMember("edittime",newEdittime);
}
public String getEdituser() {
	return getString("edituser");
}
public void setEdituser(String newEdituser) {
	setMember("edituser",newEdituser);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setAccountno(bean.getFieldByName(mapper("accountno")).asString());
	setAccounttype(bean.getFieldByName(mapper("accounttype")).asString());
	setAccountname(bean.getFieldByName(mapper("accountname")).asString());
	setBankid(bean.getFieldByName(mapper("bankid")).asString());
	setBankbranchid(bean.getFieldByName(mapper("bankbranchid")).asString());
	setBankbranchname(bean.getFieldByName(mapper("bankbranchname")).asString());
	setRemarks(bean.getFieldByName(mapper("remarks")).asString());
	setEditdate(bean.getFieldByName(mapper("editdate")).asDate());
	setEdittime(bean.getFieldByName(mapper("edittime")).asTime());
	setEdituser(bean.getFieldByName(mapper("edituser")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setAccountno(rs.getString("accountno"));
	setAccounttype(rs.getString("accounttype"));
	setAccountname(rs.getString("accountname"));
	setBankid(rs.getString("bankid"));
	setBankbranchid(rs.getString("bankbranchid"));
	setBankbranchname(rs.getString("bankbranchname"));
	setRemarks(rs.getString("remarks"));
	setEditdate(rs.getDate("editdate"));
	setEdittime(rs.getTime("edittime"));
	setEdituser(rs.getString("edituser"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("accountno",getAccountno());
	sql.setParameter("accounttype",getAccounttype());
	sql.setParameter("accountname",getAccountname());
	sql.setParameter("bankid",getBankid());
	sql.setParameter("bankbranchid",getBankbranchid());
	sql.setParameter("bankbranchname",getBankbranchname());
	sql.setParameter("remarks",getRemarks());
	sql.setParameter("editdate",getEditdate());
	sql.setParameter("edittime",getEdittime());
	sql.setParameter("edituser",getEdituser());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	setKeyField("accountno");
	setKeyField("accounttype");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("accountno",getAccountno());
	sql.setParameter("accounttype",getAccounttype());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//setKeyField("accountno");
	setKeyField("accounttype");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	if(getBankbranchname()==null || getBankbranchname().trim().length()<1) setBankbranchname("");
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	if(getBankbranchname().trim().length()<1) setBankbranchname(null);
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//setKeyField("accountno");
	setKeyField("accounttype");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		if(rs.next()) {
			result++;
			fetchResult(rs);
			//#any result fetching fool again
			//#(220000) programmer code begin;
			//#(220000) programmer code end;
		}
		//#after fetching result set lovin each day
		//#(230000) programmer code begin;
		//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	if((getAccountno()==null) || getAccountno().trim().equals("")) throw new java.sql.SQLException("Accountno is unspecified","accountno",-2008);
	if((getAccounttype()==null) || getAccounttype().trim().equals("")) throw new java.sql.SQLException("Accounttype is unspecified","accounttype",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		while(rs.next()) {
			result++;
			//#addon statement if you come back
			//#(85000) programmer code begin;
			//#(85000) programmer code end;
			TUserBankData aTUserBankData = new TUserBankData();
			aTUserBankData.fetchResult(rs);
			add(aTUserBankData);
			//#addon statement if you come back
			//#(90000) programmer code begin;
			//#(90000) programmer code end;
		}
		//#after scraping result set in too deep
		//#(240000) programmer code begin;
		//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
