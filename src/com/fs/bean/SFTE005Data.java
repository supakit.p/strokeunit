package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005Data.java
 * Description: SFTE005Data class implements for handle tuser data base schema.
 * Version: $Revision$
 * Creation date: Wed Jan 03 10:00:58 ICT 2018
 */
/**
 * SFTE005Data class implements for handle tuser data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.TheUtility;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE005Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE005Data() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tuser");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("username",java.sql.Types.VARCHAR);
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("userbranch",java.sql.Types.VARCHAR);
	addSchema("usertname",java.sql.Types.VARCHAR);
	addSchema("usertsurname",java.sql.Types.VARCHAR);
	addSchema("userename",java.sql.Types.VARCHAR);
	addSchema("useresurname",java.sql.Types.VARCHAR);
	addSchema("startdate",java.sql.Types.DATE);
	addSchema("enddate",java.sql.Types.DATE);
	addSchema("status",java.sql.Types.VARCHAR);
	addSchema("userpassword",java.sql.Types.VARCHAR);
	addSchema("passwordexpiredate",java.sql.Types.DATE);
	addSchema("supervisor",java.sql.Types.VARCHAR);
	addSchema("photoimage",java.sql.Types.VARCHAR);
	addSchema("adminflag",java.sql.Types.VARCHAR);
	addSchema("firstpage",java.sql.Types.VARCHAR);
	addSchema("theme",java.sql.Types.VARCHAR);
	addSchema("showphoto",java.sql.Types.VARCHAR);
	addSchema("loginfailtimes",java.sql.Types.TINYINT);
	addSchema("lockflag",java.sql.Types.VARCHAR);
	addSchema("usertype",java.sql.Types.VARCHAR);
	addSchema("iconfile",java.sql.Types.VARCHAR);
	addSchema("accessdate",java.sql.Types.DATE);
	addSchema("accesstime",java.sql.Types.TIME);
	addSchema("accesshits",java.sql.Types.BIGINT);
	addSchema("failtime",java.sql.Types.BIGINT);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("email",java.sql.Types.VARCHAR);
	addSchema("gender",java.sql.Types.VARCHAR);
	addSchema("mobile",java.sql.Types.VARCHAR);
	map("mobile","mobile");
	map("userename","userename");
	map("userid","userid");
	map("useresurname","useresurname");
	map("email","email");
	map("gender","gender");
	map("usertsurname","usertsurname");
	map("usertname","usertname");
	map("site","site");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE005Data.class+"=$Revision$\n";
}
public String getUserid() {
	return getString("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getUsertname() {
	return getString("usertname");
}
public void setUsertname(String newUsertname) {
	setMember("usertname",newUsertname);
}
public String getUsertsurname() {
	return getString("usertsurname");
}
public void setUsertsurname(String newUsertsurname) {
	setMember("usertsurname",newUsertsurname);
}
public String getUserename() {
	return getString("userename");
}
public void setUserename(String newUserename) {
	setMember("userename",newUserename);
}
public String getUseresurname() {
	return getString("useresurname");
}
public void setUseresurname(String newUseresurname) {
	setMember("useresurname",newUseresurname);
}
public String getEmail() {
	return getString("email");
}
public void setEmail(String newEmail) {
	setMember("email",newEmail);
}
public String getGender() {
	return getString("gender");
}
public void setGender(String newGender) {
	setMember("gender",newGender);
}
public String getMobile() {
	return getString("mobile");
}
public void setMobile(String newMobile) {
	setMember("mobile",newMobile);
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setSite(bean.getFieldByName(mapper("site")).asString());
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUsertname(bean.getFieldByName(mapper("usertname")).asString());
	setUsertsurname(bean.getFieldByName(mapper("usertsurname")).asString());
	setUserename(bean.getFieldByName(mapper("userename")).asString());
	setUseresurname(bean.getFieldByName(mapper("useresurname")).asString());
	setEmail(bean.getFieldByName(mapper("email")).asString());
	setGender(bean.getFieldByName(mapper("gender")).asString());
	setMobile(bean.getFieldByName(mapper("mobile")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	setUsertname(rs.getString("usertname"));
	setUsertsurname(rs.getString("usertsurname"));
	setUserename(rs.getString("userename"));
	setUseresurname(rs.getString("useresurname"));
	setEmail(rs.getString("email"));
	setGender(rs.getString("gender"));
	setMobile(rs.getString("mobile"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("userid",getUserid());
	sql.setParameter("usertname",getUsertname());
	sql.setParameter("usertsurname",getUsertsurname());
	sql.setParameter("userename",getUserename());
	sql.setParameter("useresurname",getUseresurname());
	sql.setParameter("email",getEmail());
	sql.setParameter("gender",getGender());
	sql.setParameter("mobile",getMobile());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("userid",getUserid());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("userid");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getUserid()==null) || getUserid().trim().equals("")) throw new java.sql.SQLException("Userid is unspecified","userid",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	boolean eng = TheUtility.isEnglish(global);
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	sql.clear();
	sql.append("select tuser.*,");
	sql.append("tuserinfo.userbranch,tuserinfo.usertname,tuserinfo.usertsurname,");
	sql.append("tuserinfo.userename,tuserinfo.useresurname,tuserinfo.supervisor,");
	sql.append("tuserinfo.email,tuserinfo.gender,tuserinfo.mobile,tuserinfo.lineno,tuserinfo.photoimage, ");
	if(eng) {
		sql.append("tcomp.nameen as sitedesc ");
	} else {
		sql.append("tcomp.nameth as sitedesc ");
	}
	sql.append("from tuser, tuserinfo, tcomp ");
	sql.append("where tuser.userid = tuserinfo.userid ");
	sql.append("and tuser.site = tcomp.site ");
	String filter = " and";
	if(getSite()!=null && getSite().trim().length()>0) {
		sql.append(filter+" tuser.site = ?site ");
		sql.setParameter("site",getSite());
		filter = " and";
	}
	if(getUserid()!=null && getUserid().trim().length()>0) {
		sql.append(filter+" tuser.userid LIKE ?userid ");
		sql.setParameter("userid","%"+getUserid()+"%");
		filter = " and";
	}
	if(getUsertname()!=null && getUsertname().trim().length()>0) {
		sql.append(filter+" tuserinfo.usertname LIKE ?usertname ");
		sql.setParameter("usertname","%"+getUsertname()+"%");
		filter = " and";
	}
	if(getUsertsurname()!=null && getUsertsurname().trim().length()>0) {
		sql.append(filter+" tuserinfo.usertsurname LIKE ?usertsurname ");
		sql.setParameter("usertsurname","%"+getUsertsurname()+"%");
		filter = " and";
	}
	if(getEmail()!=null && getEmail().trim().length()>0) {
		sql.append(filter+" tuserinfo.email LIKE ?email ");
		sql.setParameter("email","%"+getEmail()+"%");
		filter = " and";
	}
	if(getMobile()!=null && getMobile().trim().length()>0) {
		sql.append(filter+" tuserinfo.mobile LIKE ?mobile ");
		sql.setParameter("mobile","%"+getMobile()+"%");
		filter = " and";
	}
	if(getGender()!=null && getGender().trim().length()>0) {
		sql.append(filter+" tuserinfo.gender = ?gender ");
		sql.setParameter("gender",getGender());
		filter = " and";
	}
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE005AData aSFTE005AData = new SFTE005AData();
		aSFTE005AData.fetchResult(rs);
		add(aSFTE005AData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		aSFTE005AData.setSitedesc(rs.getString("sitedesc"));
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
