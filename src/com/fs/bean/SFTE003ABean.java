package com.fs.bean;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE003ABean.java
 * Description: SFTE003ABean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Sun Sep 23 11:31:49 ICT 2018
 */
/**
 * SFTE003ABean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class SFTE003ABean extends BeanSchema {

	public SFTE003ABean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("product",java.sql.Types.VARCHAR,"product");
		addSchema("nameen",java.sql.Types.VARCHAR,"nameen");
		addSchema("nameth",java.sql.Types.VARCHAR,"nameth");
		addSchema("seqno",java.sql.Types.VARCHAR,"Sequence");
		addSchema("serialid",java.sql.Types.VARCHAR,"serialid");
		addSchema("startdate",java.sql.Types.DATE,"startdate");
		addSchema("url",java.sql.Types.VARCHAR,"url");
		addSchema("verified",java.sql.Types.VARCHAR,"verified");
		addSchema("iconfile",java.sql.Types.VARCHAR,"Icon File");
		addSchema("centerflag",java.sql.Types.VARCHAR,"centerflag");
	}
	public String getProduct() {
		return getMember("product");
	}
	public void setProduct(String newProduct) {
		setMember("product",newProduct);
	}
	public String getNameen() {
		return getMember("nameen");
	}
	public void setNameen(String newNameen) {
		setMember("nameen",newNameen);
	}
	public String getNameth() {
		return getMember("nameth");
	}
	public void setNameth(String newNameth) {
		setMember("nameth",newNameth);
	}
	public String getSerialid() {
		return getMember("serialid");
	}
	public void setSerialid(String newSerialid) {
		setMember("serialid",newSerialid);
	}
	public String getStartdate() {
		return getMember("startdate");
	}
	public void setStartdate(String newStartdate) {
		setMember("startdate",newStartdate);
	}
	public String getUrl() {
		return getMember("url");
	}
	public void setUrl(String newUrl) {
		setMember("url",newUrl);
	}
	public String getVerified() {
		return getMember("verified");
	}
	public void setVerified(String newVerified) {
		setMember("verified",newVerified);
	}
	public String getIconfile() {
		return getMember("iconfile");
	}
	public void setIconfile(String newIconfile) {
		setMember("iconfile",newIconfile);
	}
	public String getSeqno() {
		return getMember("seqno");
	}
	public void setSeqno(String newSeqno) {
		setMember("seqno",newSeqno);
	}
	public String getCenterflag() {
		return getMember("centerflag");
	}
	public void setCenterflag(String newCenterflag) {
		setMember("centerflag",newCenterflag);
	}
}
