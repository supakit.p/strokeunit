package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Thu Oct 18 16:43:43 ICT 2018
 */

import com.fs.bean.dba.*;
import com.fs.bean.misc.*;
import com.fs.bean.util.*;
//#everybody in love
//#(5000) programmer code begin;
//#(5000) programmer code end;
@SuppressWarnings({"serial","unused"})
public class SFTE007ABean extends CustomBean {
//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTE007ABean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("site",java.sql.Types.VARCHAR,"site");
	addSchema("employeeid",java.sql.Types.VARCHAR,"employeeid");
	addSchema("sitedesc",java.sql.Types.VARCHAR,"sitedesc");
	addSchema("userid",java.sql.Types.VARCHAR,"userid");
	addSchema("userpassword",java.sql.Types.VARCHAR,"userpassword");
	addSchema("status",java.sql.Types.VARCHAR,"status");
	addSchema("employeecode",java.sql.Types.VARCHAR,"employeecode");
	addSchema("usertname",java.sql.Types.VARCHAR,"usertname");
	addSchema("usertsurname",java.sql.Types.VARCHAR,"usertsurname");
	addSchema("siteflag",java.sql.Types.VARCHAR,"siteflag");
	addSchema("branchflag",java.sql.Types.VARCHAR,"branchflag");
	addSchema("email",java.sql.Types.VARCHAR,"email");
	addSchema("username",java.sql.Types.VARCHAR,"username");
	addSchema("userbranch",java.sql.Types.VARCHAR,"userbranch");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	addSchema("usersites",java.sql.Types.VARCHAR,"usersites");
	addSchema("userroles",java.sql.Types.VARCHAR,"userroles");
	addSchema("usergroups",java.sql.Types.VARCHAR,"usergroups");
	addSchema("userbranches",java.sql.Types.VARCHAR,"userbranches");
	addSchema("effectdate",java.sql.Types.DATE,"effectdate");
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE007ABean.class+"=$Revision$\n";
}
public String getSite() {
	return getMember("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getEmployeeid() {
	return getMember("employeeid");
}
public void setEmployeeid(String newEmployeeid) {
	setMember("employeeid",newEmployeeid);
}
public String getSitedesc() {
	return getMember("sitedesc");
}
public void setSitedesc(String newSitedesc) {
	setMember("sitedesc",newSitedesc);
}
public String getUserid() {
	return getMember("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getUserpassword() {
	return getMember("userpassword");
}
public void setUserpassword(String newUserpassword) {
	setMember("userpassword",newUserpassword);
}
public String getStatus() {
	return getMember("status");
}
public void setStatus(String newStatus) {
	setMember("status",newStatus);
}
public String getEmployeecode() {
	return getMember("employeecode");
}
public void setEmployeecode(String newEmployeecode) {
	setMember("employeecode",newEmployeecode);
}
public String getUsertname() {
	return getMember("usertname");
}
public void setUsertname(String newUsertname) {
	setMember("usertname",newUsertname);
}
public String getUsertsurname() {
	return getMember("usertsurname");
}
public void setUsertsurname(String newUsertsurname) {
	setMember("usertsurname",newUsertsurname);
}
public String getSiteflag() {
	return getMember("siteflag");
}
public void setSiteflag(String newSiteflag) {
	setMember("siteflag",newSiteflag);
}
public String getBranchflag() {
	return getMember("branchflag");
}
public void setBranchflag(String newBranchflag) {
	setMember("branchflag",newBranchflag);
}
public String getEmail() {
	return getMember("email");
}
public void setEmail(String newEmail) {
	setMember("email",newEmail);
}
public String getUsername() {
	return getMember("username");
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
public String getUsersites() {
	return getMember("usersites");
}
public void setUsersites(String newUsersites) {
	setMember("usersites",newUsersites);
}
public String getUserroles() {
	return getMember("userroles");
}
public void setUserroles(String newUserroles) {
	setMember("userroles",newUserroles);
}
public String getUsergroups() {
	return getMember("usergroups");
}
public void setUsergroups(String newUsergroups) {
	setMember("usergroups",newUsergroups);
}
public String getUserbranches() {
	return getMember("userbranches");
}
public void setUserbranches(String newUserbranches) {
	setMember("userbranches",newUserbranches);
}
public String getEffectdate() {
	return getMember("effectdate");
}
public void setEffectdate(String newEffectdate) {
	setMember("effectdate",newEffectdate);
}
public String getUserbranch() {
	return getMember("userbranch");
}
public void setUserbranch(String newUserbranch) {
	setMember("userbranch",newUserbranch);
}
//#(30000) programmer code end;
/*
$Revision$
*/
}
