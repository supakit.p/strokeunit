package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: P02Data.java
 * Description: P02Data class implements for handle Mw_Focus data base schema.
 * Version: $Revision$
 * Creation date: Wed Mar 03 14:34:57 ICT 2021
 */
/**
 * P02Data class implements for handle Mw_Focus data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class P02Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public P02Data() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("Mw_Focus");
	addSchema("FocusId",java.sql.Types.VARCHAR);
	addSchema("FocusName",java.sql.Types.VARCHAR);
	addSchema("IsActive",java.sql.Types.BIT);
	addSchema("CreateDate",java.sql.Types.TIMESTAMP);
	addSchema("UpdateDate",java.sql.Types.TIMESTAMP);
	map("FocusName","focusname");
	map("FocusId","focusid");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+P02Data.class+"=$Revision$\n";
}
public String getFocusid() {
	return getString("FocusId");
}
public void setFocusid(String newFocusid) {
	setMember("FocusId",newFocusid);
}
public String getFocusname() {
	return getString("FocusName");
}
public void setFocusname(String newFocusname) {
	setMember("FocusName",newFocusname);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setFocusid(bean.getFieldByName(mapper("FocusId")).asString());
	setFocusname(bean.getFieldByName(mapper("FocusName")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setFocusid(rs.getString("FocusId"));
	setFocusname(rs.getString("FocusName"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	//sql.setParameter("FocusId",getFocusid());
	//sql.setParameter("FocusName",getFocusname());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	sql.clear();
	sql.append("SELECT  ");
	sql.append(" FocusId, FocusName, IsActive ");
	sql.append(" from Mw_Focus ");
	String sFocusid =String.valueOf(getFocusid());
	if(getFocusid() != null && !getFocusid().isEmpty()) {
		
		sql.append("where  FocusId LIKE ?focusid ");
		if(getFocusname() != null && !getFocusname().isEmpty()) {
			sql.append("AND  Focusname LIKE ?focusname ");
		}
	}else if(getFocusname() != null && !getFocusname().isEmpty()) {
		sql.append("where Focusname LIKE ?focusname ");
	}
	
	sql.setParameter("focusid", "%" + getFocusid() + "%");
	sql.setParameter("focusname", "%" + getFocusname() + "%");
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
		while(rs.next()) {
			result++;
			//#addon statement if you come back
			//#(85000) programmer code begin;
			//#(85000) programmer code end;
			com.fs.bean.P02AData aP02AData = new com.fs.bean.P02AData();
			aP02AData.fetchResult(rs);
			add(aP02AData);
			aP02AData.setFocusid(Integer.parseInt(rs.getString("FocusId")));
			aP02AData.setFocusname(rs.getString("FocusName"));
			aP02AData.setIsactive(rs.getBoolean("IsActive"));
			//#addon statement if you come back
			//#(90000) programmer code begin;
			//#(90000) programmer code end;
		}
		//#after scraping result set in too deep
		//#(240000) programmer code begin;
		//#(240000) programmer code end;
	}
	//#before talk love
	//#(245000) programmer code begin;
	//#(245000) programmer code end;
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
