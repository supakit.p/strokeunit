package com.fs.bean;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE003Data.java
 * Description: SFTE003Data class implements for handle tprod data base schema.
 * Version: $Revision$
 * Creation date: Sun Sep 23 11:34:57 ICT 2018
 */
/**
 * SFTE003Data class implements for handle tprod data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.math.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class SFTE003Data extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public SFTE003Data() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tprod");
	addSchema("product",java.sql.Types.VARCHAR);
	addSchema("nameen",java.sql.Types.VARCHAR);
	addSchema("nameth",java.sql.Types.VARCHAR);
	addSchema("serialid",java.sql.Types.VARCHAR);
	addSchema("startdate",java.sql.Types.DATE);
	addSchema("url",java.sql.Types.VARCHAR);
	addSchema("capital",java.sql.Types.VARCHAR);
	addSchema("verified",java.sql.Types.VARCHAR);
	addSchema("iconfile",java.sql.Types.VARCHAR);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("edituser",java.sql.Types.VARCHAR);
	map("nameth","nameth");
	map("verified","verified");
	map("product","product");
	map("nameen","nameen");
	map("iconfile","iconfile");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTE003Data.class+"=$Revision$\n";
}
public String getProduct() {
	return getString("product");
}
public void setProduct(String newProduct) {
	setMember("product",newProduct);
}
public String getNameen() {
	return getString("nameen");
}
public void setNameen(String newNameen) {
	setMember("nameen",newNameen);
}
public String getNameth() {
	return getString("nameth");
}
public void setNameth(String newNameth) {
	setMember("nameth",newNameth);
}
public String getVerified() {
	return getString("verified");
}
public void setVerified(String newVerified) {
	setMember("verified",newVerified);
}
public String getIconfile() {
	return getString("iconfile");
}
public void setIconfile(String newIconfile) {
	setMember("iconfile",newIconfile);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setProduct(bean.getFieldByName(mapper("product")).asString());
	setNameen(bean.getFieldByName(mapper("nameen")).asString());
	setNameth(bean.getFieldByName(mapper("nameth")).asString());
	setVerified(bean.getFieldByName(mapper("verified")).asString());
	setIconfile(bean.getFieldByName(mapper("iconfile")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setProduct(rs.getString("product"));
	setNameen(rs.getString("nameen"));
	setNameth(rs.getString("nameth"));
	setVerified(rs.getString("verified"));
	setIconfile(rs.getString("iconfile"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("product",getProduct());
	sql.setParameter("nameen",getNameen());
	sql.setParameter("nameth",getNameth());
	sql.setParameter("verified",getVerified());
	sql.setParameter("iconfile",getIconfile());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("product");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("product",getProduct());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("product");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("product");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;
	//#(195000) programmer code end;
	if((getProduct()==null) || getProduct().trim().equals("")) throw new java.sql.SQLException("Product is unspecified","product",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	String filter = " where";
	if(getNameen()!=null && getNameen().trim().length()>0) {
		sql.append(filter+" nameen LIKE ?nameen ");
		sql.setParameter("nameen", "%"+getNameen()+"%");
		filter = " and";
	}
	if(getNameth()!=null && getNameth().trim().length()>0) {
		sql.append(filter+" nameth LIKE ?nameth ");
		sql.setParameter("nameth", "%"+getNameth()+"%");
		filter = " and";
	}
	if(getVerified()!=null && getVerified().trim().length()>0) {
		sql.append(filter+" verified = ?verified ");
		sql.setParameter("verified", getVerified());
		filter = " and";		
	}	
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		SFTE003AData aSFTE003AData = new SFTE003AData();
		aSFTE003AData.fetchResult(rs);
		add(aSFTE003AData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
//#(100000) programmer code end;
}
