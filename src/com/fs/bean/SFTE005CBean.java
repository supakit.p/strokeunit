package com.fs.bean;

import java.sql.Date;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: SFTE005CBean.java
 * Description: SFTE005CBean class implements for handle  schema bean.
 * Version: $Revision$
 * Creation date: Wed Jan 03 14:36:29 ICT 2018
 */
/**
 * SFTE005CBean class implements for handle schema bean.
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.TheLibrary;
import com.fs.bean.misc.*;
@SuppressWarnings({"serial","unused"})
public class SFTE005CBean extends BeanSchema {

	public SFTE005CBean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("userid",java.sql.Types.CHAR,"userid");
		addSchema("username",java.sql.Types.CHAR,"username");
		addSchema("userpassword",java.sql.Types.CHAR,"userpassword");
		addSchema("oldpassword",java.sql.Types.CHAR,"oldpassword");
		addSchema("confirmpassword",java.sql.Types.CHAR,"confirmpassword");
		addSchema("checkflag",java.sql.Types.VARCHAR,"checkflag");
	}
	public String getUserid() {
		return getMember("userid");
	}
	public void setUserid(String newUserid) {
		setMember("userid",newUserid);
	}
	public String getUsername() {
		return getMember("username");
	}
	public void setUsername(String newUsername) {
		setMember("username",newUsername);
	}
	public String getUserpassword() {
		return getMember("userpassword");
	}
	public void setUserpassword(String newUserpassword) {
		setMember("userpassword",newUserpassword);
	}
	public String getOldpassword() {
		return getMember("oldpassword");
	}
	public void setOldpassword(String newOldpassword) {
		setMember("oldpassword",newOldpassword);
	}
	public String getConfirmpassword() {
		return getMember("confirmpassword");
	}
	public void setConfirmpassword(String newConfirmpassword) {
		setMember("confirmpassword",newConfirmpassword);
	}
	public String getCheckflag() {
		return getMember("checkflag");
	}
	public void setCheckflag(String newCheckflag) {
		setMember("checkflag",newCheckflag);
	}	
	public String getPasswordexpiredate() {
		return getMember("passwordexpiredate");
	}	
	public void setPasswordexpiredate(String newPasswordexpiredate) {
		setMember("passwordexpiredate",newPasswordexpiredate);
	}
	public BeanException validateDatas(String fs_language) {
		if(getUserid()==null || getUserid().trim().length()<=0) {
			return new BeanException("User is undefined",-8891,fs_language);
		}
		if(!"0".equals(getCheckflag())) {
			if(!getUserpassword().equals(getConfirmpassword())) {
				return new BeanException("Confirm Password Mismatch",-8897,fs_language);
			}
		}
		if(getUserpassword().equals(getUserid())) {
			return new BeanException("Not allow password as same as user",-8899,fs_language);
		}
		if(getUserpassword().length()>8) {
			return new BeanException("Password length not over 8 characters",-8896,fs_language);
		}	
		return null;
	}		
	public BeanException validateResetDatas(String fs_language) {
		if(getUsername()==null || getUsername().trim().length()<=0) {
			return new BeanException("User is undefined",-8891,fs_language);
		}
		if(!getUserpassword().equals(getConfirmpassword())) {
			return new BeanException("Confirm Password Mismatch",-8897,fs_language);
		}		
		if(getUserpassword().equals(getUsername())) {
			return new BeanException("Not allow password as same as user",-8994,fs_language);
		}
		if(getUserpassword().length()>8) {
			return new BeanException("Password length not over 8 characters",-8896,fs_language);
		}	
		return null;
	}		
}
