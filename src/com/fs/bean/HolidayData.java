package com.fs.bean;
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.bean.misc.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes","unused"})
public class HolidayData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
//#(20000) programmer code end;
public HolidayData() {
	super();
}
protected void initialize() {
	super.initialize();
	setTable("tholiday");
	addSchema("site",java.sql.Types.VARCHAR);
	addSchema("holidayid",java.sql.Types.VARCHAR);
	addSchema("holidaydate",java.sql.Types.DATE);
	addSchema("nameen",java.sql.Types.VARCHAR);
	addSchema("nameth",java.sql.Types.VARCHAR);
	addSchema("annually",java.sql.Types.VARCHAR);
	addSchema("editdate",java.sql.Types.DATE);
	addSchema("edittime",java.sql.Types.TIME);
	addSchema("edituser",java.sql.Types.VARCHAR);
	//#intialize how deep is your love 
	//#(30000) programmer code begin;	
	//#(30000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+HolidayData.class+"=$Revision$\n";
}
public String getSite() {
	return getString("site");
}
public void setSite(String newSite) {
	setMember("site",newSite);
}
public String getHolidayid() {
	return getString("holidayid");
}
public void setHolidayid(String newHolidayid) {
	setMember("holidayid",newHolidayid);
}
public Date getHolidaydate() {
	return getDate("holidaydate");
}
public void setHolidaydate(Date newHolidaydate) {
	setMember("holidaydate",newHolidaydate);
}
public String getNameen() {
	return getString("nameen");
}
public void setNameen(String newNameen) {
	setMember("nameen",newNameen);
}
public String getNameth() {
	return getString("nameth");
}
public void setNameth(String newNameth) {
	setMember("nameth",newNameth);
}
public String getAnnually() {
	return getString("annually");
}
public void setAnnually(String newAnnually) {
	setMember("annually",newAnnually);
}
public Date getEditdate() {
	return getDate("editdate");
}
public void setEditdate(Date newEditdate) {
	setMember("editdate",newEditdate);
}
public Time getEdittime() {
	return getTime("edittime");
}
public void setEdittime(Time newEdittime) {
	setMember("edittime",newEdittime);
}
public String getEdituser() {
	return getString("edituser");
}
public void setEdituser(String newEdituser) {
	setMember("edituser",newEdituser);
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setSite(bean.getFieldByName(mapper("site")).asString());
	setHolidayid(bean.getFieldByName(mapper("holidayid")).asString());
	setHolidaydate(bean.getFieldByName(mapper("holidaydate")).asDate());
	setNameen(bean.getFieldByName(mapper("nameen")).asString());
	setNameth(bean.getFieldByName(mapper("nameth")).asString());
	setAnnually(bean.getFieldByName(mapper("annually")).asString());
	setEditdate(bean.getFieldByName(mapper("editdate")).asDate());
	setEdittime(bean.getFieldByName(mapper("edittime")).asTime());
	setEdituser(bean.getFieldByName(mapper("edituser")).asString());
	//#obtain it perfect moment
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setSite(rs.getString("site"));
	setHolidayid(rs.getString("holidayid"));
	setHolidaydate(rs.getDate("holidaydate"));
	setNameen(rs.getString("nameen"));
	setNameth(rs.getString("nameth"));
	setAnnually(rs.getString("annually"));
	setEditdate(rs.getDate("editdate"));
	setEdittime(rs.getTime("edittime"));
	setEdituser(rs.getString("edituser"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
protected void assignParameters(ExecuteStatement sql) throws Exception {
	if(sql==null) return;
	//#Everything I do, I do it for you
	//#(75000) programmer code begin;
	//#(75000) programmer code end;
	sql.setParameter("site",getSite());
	sql.setParameter("holidayid",getHolidayid());
	sql.setParameter("holidaydate",getHolidaydate());
	sql.setParameter("nameen",getNameen());
	sql.setParameter("nameth",getNameth());
	sql.setParameter("annually",getAnnually());
	sql.setParameter("editdate",getEditdate());
	sql.setParameter("edittime",getEdittime());
	sql.setParameter("edituser",getEdituser());
	//#I'm gonna be around you
	//#(77000) programmer code begin;
	//#(77000) programmer code end;
}
public int insert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#begin with insert statement going on
	//#(250000) programmer code begin;
	if (getEditdate() == null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if (getEdittime() == null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	//#(250000) programmer code end;
	ExecuteStatement sql = createQueryForInsert(connection);
	//#another modification keep on loving you
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	assignParameters(sql);
	//#assigned parameter always on my mind
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with insert statement here
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
public int delete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("site");
	setKeyField("holidayid");
	setKeyField("holidaydate");
	//#begin with delete statement going on
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	ExecuteStatement sql = createQueryForDelete(connection);
	//#any delete statement more than that
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	sql.setParameter("site",getSite());
	sql.setParameter("holidayid",getHolidayid());
	sql.setParameter("holidaydate",getHolidaydate());
	//#assigned parameters time after time
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement here
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("site");
	setKeyField("holidayid");
	setKeyField("holidaydate");
	//#begin with update statement going on
	//#(290000) programmer code begin;
	if (getEditdate() == null) setEditdate(new java.sql.Date(System.currentTimeMillis()));
	if (getEdittime() == null) setEdittime(new java.sql.Time(System.currentTimeMillis()));
	//#(290000) programmer code end;
	ExecuteStatement sql = createQueryForUpdate(connection);
	//#any update statement over protected
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	assignParameters(sql);
	//#assigned parameters all rise
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
	return sql.executeUpdate(connection);
	//#ending with delete statement going on
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
public int retrieve(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("site");
	setKeyField("holidayid");
	setKeyField("holidaydate");
	//#statement shake it up retrieving
	//#(165000) programmer code begin;
	//#(165000) programmer code end;
	ExecuteStatement sql = createQueryForRetrieve(connection);
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	}
	return result;
}
public boolean validateInsert(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#say you say me
	//#(195000) programmer code begin;	
	//#(195000) programmer code end;
	if((getSite()==null) || getSite().trim().equals("")) throw new java.sql.SQLException("Site is unspecified","site",-2008);
	if((getHolidayid()==null) || getHolidayid().trim().equals("")) throw new java.sql.SQLException("Holidayid is unspecified","holidayid",-2008);
	if(getHolidaydate()==null) throw new java.sql.SQLException("Holidaydate is undefined","holidaydate",-2008);
	//#valid statement best in me
	//#(190000) programmer code begin;
	int counter = 0;
	KnSQL knsql = new KnSQL(this);
	knsql.append("select count(*) as counter from "+getTableName()+" ");
	knsql.append("where site = ?site and holidayid = ?holidayid and holidaydate = ?holidaydate ");
	knsql.setParameter("site", getSite());
	knsql.setParameter("holidayid", getHolidayid());
	knsql.setParameter("holidaydate", getHolidaydate());
	try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
		if(rs.next()) {
			counter = rs.getInt("counter");
		}
	}
	if(counter>0) {
		throw new BeanException("Duplicate holiday date",-18902);
	}
	//#(190000) programmer code end;
	return true;
}
public boolean validateUpdate(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid update statement to be with you
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
	return true;
}
public boolean validateDelete(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	//#valid delete statement here for you
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
}
public int collect(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	removeAlls();
	PermissionBean fsPermission = global==null?null:global.getPermission();
	//#here we go to the collection
	//#(65000) programmer code begin;
	//#(65000) programmer code end;
	ExecuteStatement sql = createQueryForCollect(connection);
	//#any collect statement would you be happier
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	//#assigned parameters this temptation
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
	int result = 0;
	try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
	while(rs.next()) {
		result++;
		//#addon statement if you come back
		//#(85000) programmer code begin;
		//#(85000) programmer code end;
		HolidayData aHolidayData = new HolidayData();
		aHolidayData.fetchResult(rs);
		add(aHolidayData);
		//#addon statement if you come back
		//#(90000) programmer code begin;
		//#(90000) programmer code end;
	}
	//#after scraping result set in too deep
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
	}
	return result;
}
//#another methods defined drive you crazy
//#(100000) programmer code begin;
public int deleteGroup(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
	setKeyField("site");
	setKeyField("holidayid");
	ExecuteStatement sql = createQueryForDelete(connection);
	sql.setParameter("site",getSite());
	sql.setParameter("holidayid",getHolidayid());
	return sql.executeUpdate(connection);
}
public void setTable(String newTableName) {
	super.setTable(newTableName);
}
//#(100000) programmer code end;
}
