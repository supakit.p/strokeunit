package com.fs.bean;

import com.fs.bean.gener.*;

public class ConfigFieldBeanMapper extends BeanMap {
public ConfigFieldBeanMapper() {
	super();
}
protected void initialize() {
	super.initialize();
	mapClass("com.fs.bean.ConfigFieldData");
}
public String fetchVersion() {
	return super.fetchVersion()+ConfigFieldBeanMapper.class+"=$Revision$\n";
}
}