package com.fs.bean;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Tue Jun 27 09:21:57 ICT 2017
 */

import com.fs.bean.misc.*;
import com.fs.bean.util.*;
import com.fs.bean.gener.*;

@SuppressWarnings({"serial","unused"})
public class SFTQ001ABean extends BeanSchema {

//#member & declaration remember me this way
//#(10000) programmer code begin;
//#(10000) programmer code end;
public SFTQ001ABean() {
	super();
}
protected void initialize() {
	super.initialize();
	addSchema("curtime",java.sql.Types.VARCHAR,"Date Time");
	addSchema("userid",java.sql.Types.VARCHAR,"User ID");
	addSchema("progid",java.sql.Types.VARCHAR,"Program ID");
	addSchema("action",java.sql.Types.VARCHAR,"Action");
	addSchema("remark",java.sql.Types.VARCHAR,"Remark");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	addSchema("progname",java.sql.Types.VARCHAR,"Program Name");
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+SFTQ001ABean.class+"=$Revision$\n";
}
public String getCurtime() {
	return getMember("curtime");
}
public void setCurtime(String newCurtime) {
	setMember("curtime",newCurtime);
}
public String getUserid() {
	return getMember("userid");
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public String getProgid() {
	return getMember("progid");
}
public void setProgid(String newProgid) {
	setMember("progid",newProgid);
}
public String getAction() {
	return getMember("action");
}
public void setAction(String newAction) {
	setMember("action",newAction);
}
public String getRemark() {
	return getMember("remark");
}
public void setRemark(String newRemark) {
	setMember("remark",newRemark);
}
//#methods defined everything will flow
//#(30000) programmer code begin;
public String getProgname() {
	return getMember("progname");
}
public void setProgname(String newProgname) {
	setMember("progname",newProgname);
}
//#(30000) programmer code end;
}
