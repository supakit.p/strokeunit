package com.fs.dev;

import com.fs.bean.misc.KnSQL;
import com.fs.bean.util.GlobalBean;

/**
 * @author tassun_oro
 *
 */
public class TheUser {
	private GlobalBean global;
	private String employeeid;
	private String userid;
	private String usertname;
	private String usertsurname;
	private String userename;
	private String useresurname;
	private boolean isexisted = false;
	
	public TheUser(String userid) {
		this(userid,null,null);
	}
	public TheUser(String userid,GlobalBean global) {
		this(userid,null,global);
	}
	public TheUser(String userid,String employeeid) {
		this(userid,employeeid,null);
	}
	public TheUser(String userid,String employeeid,GlobalBean global) {
		this.userid = userid;
		this.employeeid = employeeid;
		this.global = global;
	}
	public void fetchResult(java.sql.ResultSet rs) throws Exception {
		setEmployeeid(rs.getString("employeeid"));
		setUserid(rs.getString("userid"));
		setUsertname(rs.getString("usertname"));
		setUsertsurname(rs.getString("usertsurname"));
		setUserename(rs.getString("userename"));
		setUseresurname(rs.getString("useresurname"));
	}
	public String getEmployeeid() {
		return employeeid;
	}
	public String getFullEName() {
		return (getUserename()==null?"":getUserename())+" "+(getUseresurname()==null?"":getUseresurname());
	}
	public String getFullTName() {
		return (getUsertname()==null?"":getUsertname())+" "+(getUsertsurname()==null?"":getUsertsurname());
	}
	public GlobalBean getGlobal() {
		return global;
	}
	public String getUserename() {
		return userename;
	}
	public String getUseresurname() {
		return useresurname;
	}
	public String getUserid() {
		return userid;		
	}
	public String getUserName() {
		if(!TheUtility.isEnglish(global)) {
			return getFullTName();
		}
		return getFullEName();
	}
	public String getUserName(java.sql.Connection connection) throws Exception {
		isexisted = false;
		KnSQL knsql = new KnSQL(this);
		knsql.append("select employeeid,userid,usertname,usertsurname,userename,useresurname ");
		knsql.append("from tuserinfo ");
		if(getEmployeeid()!=null && getEmployeeid().trim().length()>0) {
			knsql.append("where employeeid = ?employeeid ");
			knsql.setParameter("employeeid", getEmployeeid());			
		} else {
			knsql.append("where userid = ?userid ");
			knsql.setParameter("userid", getUserid());
		}
		try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
			if(rs.next()) {
				isexisted = true;
				fetchResult(rs);
			}
		}
		return getUserName();
	}
	public String getUsertname() {
		return usertname;
	}
	public String getUsertsurname() {
		return usertsurname;
	}
	public boolean isExisting() {
		return isexisted;
	}
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
	public void setGlobal(GlobalBean global) {
		this.global = global;
	}
	public void setUserename(String userename) {
		this.userename = userename;
	}
	public void setUseresurname(String useresurname) {
		this.useresurname = useresurname;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public void setUsertname(String usertname) {
		this.usertname = usertname;
	}
	public void setUsertsurname(String usertsurname) {
		this.usertsurname = usertsurname;
	}
}
