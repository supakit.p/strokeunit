package com.fs.dev.notify;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fs.dev.Arguments;
import com.fs.dev.Console;
import com.google.gson.Gson;

/**
 * @author tassun_oro
 *
 */
public class FirebaseNotificationSender {
    protected String url = "https://fcm.googleapis.com/fcm/send";
    protected String key = "AIzaSyDb0-L2rUGshUOCPN30wZF6yjN8Nl-sUSU";

    public FirebaseNotificationSender() { 
    	super();
    }

    public FirebaseNotificationSender(String url, String key) {
        this.url = url;
        this.key = key;
    }
    
    public static void main(String[] args) {
    	//java com/fs/dev/notify/FirebaseNotificationSender -action build -token 12345 -data type=Approval -click TODOLIST
    	try {
	        if (args.length > 0) {
	        	String action = Arguments.getString(args, "build", "-action");
	        	String title = Arguments.getString(args, "Test", "-title");
	        	String message = Arguments.getString(args, "Hello", "-msg");
	        	String click = Arguments.getString(args, null, "-click");
	        	java.util.List<String> tokens = null;
	        	java.util.Map<String,Object> data = null;
	        	FirebaseNotificationSender sender = new FirebaseNotificationSender();
            	sender.setUrl(Arguments.getString(args, sender.getUrl(), "-url"));
            	sender.setKey(Arguments.getString(args, sender.getKey(), "-key"));
                for (int i = 0, isz = args.length; i < isz; i++) {
                    String para = args[i];
                    if (para.equalsIgnoreCase("-token")) {
                        if (args.length > (i + 1)) {
                        	if(tokens==null) tokens = new java.util.ArrayList<>();
                        	tokens.add(args[i + 1]);
                        }
                    } else if (para.equalsIgnoreCase("-data")) {
                        if (args.length > (i + 1)) {
                        	String text = args[i + 1];
							int idx = text.indexOf('=');
							if(idx>0) {
								String name = text.substring(0,idx);
								String value = text.substring(idx+1);
								if(data==null) data = new java.util.HashMap<>();
								data.put(name,value);
							}
                        	
                        }
                    }
                }
                if("build".equalsIgnoreCase(action)) {
                	java.util.List<String> payloads = sender.buildPayload(tokens, title, message, click, data);
                	Console.out.println("payloads = "+payloads);                	
                } else {
	                if(tokens!=null && !tokens.isEmpty()) {
	                	sender.send(tokens, title, message, data);
	                } else {
	                	java.util.List<String> payloads = sender.buildPayload(tokens, title, message, click, data);
	                	Console.out.println("payloads = "+payloads);
	                }
                }
	        }
    	} catch(Exception ex) {
    		Console.out.print(ex);
    	}
    }

    protected List<String> buildPayload(List<String> deviceTokens, PayloadModel payload, java.util.Map<String,Object> data) {
        List<String> payloads = new ArrayList<>();        
        // Limit 1000 tokens per request
        for(int i=0; i<deviceTokens.size(); i+=1000) {
            FirebaseModel firebaseModel = new FirebaseModel();
            int from = i;
            int end = i + 999;
            if(end > deviceTokens.size()) {
                end = deviceTokens.size() - 1;
            }
            List<String> tokens = deviceTokens.subList(from, end + 1);
            firebaseModel.setRegistration_ids(tokens.toArray(new String[0]));
            firebaseModel.setNotification(payload);  
            firebaseModel.setData(data);
            Gson gson = new Gson();
            String js = gson.toJson(firebaseModel);
            payloads.add(js);
            i++;
        }        
        return payloads;
    }
    
    protected List<String> buildPayload(List<String> deviceTokens, String title, String message, String action, java.util.Map<String,Object> data) {
        PayloadModel payload = new PayloadModel();
        payload.setTitle(title);
        payload.setBody(message);
        payload.setClickAction(action);
        return buildPayload(deviceTokens,payload,data);
    }
    
    public String getKey() {
    	return key;
    }
    
    public String getUrl() {
    	return url;
    }
    
    public Object send(List<String> tokens, PayloadModel payload, java.util.Map<String,Object> data) {
        StringBuilder reply = new StringBuilder();        
        List<String> payloads = buildPayload(tokens, payload, data);
        Iterator<String> iterator = payloads.iterator();
        while (iterator.hasNext()) {
            HttpURLConnection httpcon = null;
            try {
                httpcon = (HttpURLConnection) ((new URL(url).openConnection()));
                httpcon.setDoOutput(true);
                httpcon.setRequestProperty("Content-Type", "application/json");
                httpcon.setRequestProperty("Authorization", "key="+key);
                httpcon.setRequestMethod("POST");
                httpcon.connect();
                byte[] outputBytes = iterator.next().getBytes("UTF-8");
                try (OutputStream os = httpcon.getOutputStream()) {
                    os.write(outputBytes);
                    try (BufferedReader br = new BufferedReader(new InputStreamReader((httpcon.getInputStream())))) {
                        String item;
                        while ((item = br.readLine()) != null) {
                            reply.append(item);
                        }
                    }
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            } finally {
                if(httpcon != null) {
                    httpcon.disconnect();
                }
            }
        }        
        return reply.toString();
    }
    
    public Object send(List<String> tokens, String title, String message) {
    	return send(tokens,title,message,null,null);
    }

    public Object send(List<String> tokens, String title, String message, java.util.Map<String,Object> data) {
        PayloadModel payload = new PayloadModel();
        payload.setTitle(title);
        payload.setBody(message);
        return send(tokens,payload,data);
    }

    public Object send(List<String> tokens, String title, String message, String action) {
    	return send(tokens,title,message,action,null);
    }
    
    public Object send(List<String> tokens, String title, String message, String action, java.util.Map<String,Object> data) {
        PayloadModel payload = new PayloadModel();
        payload.setTitle(title);
        payload.setBody(message);
        payload.setClickAction(action);
        return send(tokens,payload,data);
    }
    
    public void setKey(String key) {
    	this.key = key;
    }
    
    public void setUrl(String url) {
    	this.url = url;
    }
}
