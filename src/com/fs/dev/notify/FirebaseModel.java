package com.fs.dev.notify;

/**
 * @author tassun_oro
 *
 */
public class FirebaseModel {
    private String[] registration_ids;
    private PayloadModel notification;
    private java.util.Map<String,Object> data;
    
    public java.util.Map<String,Object> getData() {
    	return data;
    }

    public PayloadModel getNotification() {
        return notification;
    }

    public String[] getRegistration_ids() {
        return registration_ids;
    }

    public void setData(java.util.Map<String,Object> data) {
    	this.data = data;
    }
    
    public void setNotification(PayloadModel notification) {
        this.notification = notification;
    }
    
    public void setRegistration_ids(String[] registration_ids) {
        this.registration_ids = registration_ids;
    }
}
