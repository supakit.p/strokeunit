package com.fs.dev.notify;

/**
 * @author tassun_oro
 *
 */
public class PayloadModel {
    private String title;
    private String body;
    private Integer badge;
    private String subtitle;
    private String click_action;

    public Integer getBadge() {
        return badge;
    }
    
    public String getBody() {
        return body;
    }
    
    public String getClickAction() {
    	return click_action;
    }
    
    public String getSubtitle() {
    	return subtitle;
    }
    
    public String getTitle() {
        return title;
    }

    public void setBadge(Integer badge) {
        this.badge = badge;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setClickAction(String action) {
    	this.click_action = action;
    }

    public void setSubtitle(String subtitle) {
    	this.subtitle = subtitle;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
