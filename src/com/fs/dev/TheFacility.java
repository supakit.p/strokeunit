package com.fs.dev;

import javax.servlet.http.HttpServletRequest;

import com.fs.bean.EntityBean;
import com.fs.bean.ExecuteBean;
import com.fs.bean.misc.KnSQL;
import com.fs.bean.util.AccessorBean;
import com.fs.bean.util.BeanUtility;
import com.fs.bean.util.GlobalBean;
import com.fs.interfaces.RelatableInterface;

@SuppressWarnings({"serial","rawtypes"})
public class TheFacility extends TheUtility {
	public TheFacility() {
		super();
	}
	public TheFacility(GlobalBean global) {
		super(global);
	}
	public TheFacility(RelatableInterface parent) {
		super(parent);
	}
	public TheFacility(String section) {
		super(section);
	}
	public TheFacility(String section, String progid) {
		super(section, progid);
	}
	public static void main(String[] args) {
		//java com/fs/dev/TheFacility -ms PROMPT -act Bank
		//java com/fs/dev/TheFacility -ms PROMPT -act PartyCompany -site FWG -party FC,FW
		try {
			TheFacility util = new TheFacility();
			if(args.length>0) {	
				String action = Arguments.getString(args, null, "-act");		
				AccessorBean accessor = new AccessorBean();
				GlobalBean global = new GlobalBean();
				global.setFsAction(GlobalBean.COLLECT_MODE);
				accessor.setFsUser(Arguments.getString(args, accessor.getFsUser(), "-u"));
				global.setFsProg(Arguments.getString(args, global.getFsProg(), "-pid"));
				global.setFsSection(Arguments.getString(args, global.getFsSection(), "-ms"));
				accessor.setFsSite(Arguments.getString(args, accessor.getFsSite(), "-site"));
				accessor.setFsSites(Arguments.getString(args, accessor.getFsSites(), "-sites"));
				accessor.setFsParty(Arguments.getString(args, accessor.getFsParty(), "-party"));
				if(action!=null && action.trim().length()>0) {
					EntityBean bean = null;
					java.util.Map map = null;
					String getMethod = "get"+action;
					String createMethod = "create"+action;					
					java.lang.reflect.Method gmet = util.getClass().getMethod(getMethod,AccessorBean.class,GlobalBean.class);
					java.lang.reflect.Method cmet = util.getClass().getMethod(createMethod,EntityBean.class);
					Object[] params = new Object[] { accessor, global };
					bean = (EntityBean)gmet.invoke(util, params);
					map = (java.util.Map)cmet.invoke(util, new Object[] { bean } );					
					Console.out.println("map : "+map.getClass()+", "+map);
					if(bean!=null) {
						java.util.Enumeration ens = bean.elements();
						if(ens!=null) {
							while(ens.hasMoreElements()) {
								Console.out.println(ens.nextElement());
							}
						}
						java.util.List list = util.createListEntities(bean,null,null);
						Console.out.println("create list entities : "+list);
						list = util.createEntitiesAsList(bean,null,null);
						Console.out.println("create as list : "+list);
					}
				}	
			} else {
				Console.out.println("USAGE : "+TheFacility.class);
				Console.out.println("\t-act action");
				Console.out.println("\t-ms maing db section");
				Console.out.println("\t-u user");
				Console.out.println("\t-pid program id");
				Console.out.println("\t-dir directory");
				Console.out.println("\t-site site");
				Console.out.println("\t-sites sites(ex. 01,02)");
				Console.out.println("\t-party sites partner(ex. 01,02)");
			}
		} catch(Exception ex) {
			Console.out.print(ex);
		}
		System.exit(0);
	}	
	public java.util.Map createBank(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"bankid","nameen");
	}
	public void createBanks(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createBanks(accessor,pid,request,category,null);
	}	
	public void createBanks(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createBanks(null,accessor,pid,request,category,content);
	}
	public void createBanks(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getBank(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"bankid",(String)null,request,category,content);
			}
		}
	}	
	public void createCompanyInParties(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createCompanyInParties(accessor,pid,request,category,null);
	}		
	public void createCompanyInParties(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createCompanyInParties(null,accessor,pid,request,category,content);
	}
	public void createCompanyInParties(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getCompanyInParty(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"site",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createCompanyInParty(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"site","nameen");
	}
	public java.util.Map createConfirmStatus(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"confirmid","nameen");
	}
	public void createConfirmStatuses(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createConfirmStatuses(accessor,pid,request,category,null);
	}	
	public void createConfirmStatuses(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createConfirmStatuses(null,accessor,pid,request,category,content);
	}	
	public void createConfirmStatuses(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getConfirmStatus(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"confirmid",(String)null,request,category,content);
			}
		}
	}	
	public java.util.Map createExportStatus(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"exportid","nameen");
	}
	public void createExportStatuses(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createExportStatuses(accessor,pid,request,category,null);
	}	
	public void createExportStatuses(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createExportStatuses(null,accessor,pid,request,category,content);
	}	
	public void createExportStatuses(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getExportStatus(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"exportid",(String)null,request,category,content);
			}
		}
	}	
	public void createPartyCompanies(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createPartyCompanies(accessor,pid,request,category,null);
	}
	public void createPartyCompanies(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createPartyCompanies(null,accessor,pid,request,category,content);
	}	
	public void createPartyCompanies(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getPartyCompany(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"site",(String)null,request,category,content);
			}
		}
	}	
	public java.util.Map createPartyCompany(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"site","nameen");
	}			
	public java.util.Map createPartyHolidayCalendar(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"holidayid","nameen");
	}
	public void createPartyHolidayCalendars(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createPartyHolidayCalendars(accessor,pid,request,category,null);
	}
	public void createPartyHolidayCalendars(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createPartyHolidayCalendars(null,accessor,pid,request,category,content);
	}
	public void createPartyHolidayCalendars(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getPartyHolidayCalendar(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"holidayid",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createTransferStatus(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"transferid","nameen");
	}
	public void createTransferStatuses(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createTransferStatuses(accessor,pid,request,category,null);
	}
	public void createTransferStatuses(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createTransferStatuses(null,accessor,pid,request,category,content);
	}
	public void createTransferStatuses(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getTransferStatus(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"transferid",(String)null,request,category,content);
			}
		}
	}	
	public EntityBean getBank(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tbank","bankid",null,"nameen,bankid",false,false);
	}
	public EntityBean getCompanyInParty(AccessorBean accessor,GlobalBean global) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select site,shortname,nameen,nameth ");
		knsql.append("from tcomp ");
		if(accessor.getFsParty()!=null && accessor.getFsParty().length()>0) {
			String sites = BeanUtility.getFilterQoute(accessor.getFsParty());
			knsql.append("where ( site = '"+accessor.getFsSite()+"' ");
			knsql.append("or site in ("+sites+") ) ");
		} else {
			knsql.append("where site = '"+accessor.getFsSite()+"' ");			
		}
		knsql.append("and (inactive is null or inactive != '1') ");
		knsql.append("order by nameen, nameth ");
		return getEntities(entity,accessor,global);
	}
	public EntityBean getConfirmStatus(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tconfirmstatus","confirmid",null,null,false,false);
	}
	public EntityBean getExportStatus(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"texportstatus","exportid",null,null,false,false);
	}
	public EntityBean getPartyCompany(AccessorBean accessor,GlobalBean global) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select site,shortname,nameen,nameth ");
		knsql.append("from tcomp ");
		knsql.append("where site = '"+accessor.getFsSite()+"' ");
		knsql.append("or site in (select site from tcompparty where partysite = '"+accessor.getFsSite()+"') ");
		knsql.append("and (inactive is null or inactive != '1') ");		
		knsql.append("order by nameen, nameth ");
		return getEntities(entity,accessor,global);
	}
	public EntityBean getPartyHolidayCalendar(AccessorBean accessor,GlobalBean global) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select site,holidayid,nameen,nameth ");
		knsql.append("from tcompholidaytable ");
		knsql.append("where site = '"+accessor.getFsSite()+"' ");
		knsql.append("or site in (select site from tcompparty where partysite = '"+accessor.getFsSite()+"') ");
		knsql.append("and (inactive is null or inactive != '1') ");		
		knsql.append("order by nameen, nameth ");
		return getEntities(entity,accessor,global);
	}
	public EntityBean getTransferStatus(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"ttransferstatus","transferid",null,null,false,false);
	}	
}
