package com.fs.dev.tree;

import org.w3c.dom.*;
import com.fs.bean.gener.BeanSchema;
import com.fs.bean.util.BeanUtility;
import org.json.simple.*;

/**
 * @author tassun_oro
 *
 */

@SuppressWarnings({"serial","unchecked"})
public class TUser implements UserInterface, java.io.Serializable {
	private String userID;
	private String userNameth;
	private String userNameen;
	private String userType;
	private String email;
	private String supervisor;
	private String employeeid;
	private String site;
	private String inactive;
	private UserInterface parent;
	private java.util.List<UserInterface> usersList;
	private java.util.List<String> groups;
	private java.util.List<RoleInterface> roles;
	private String levelid;
	private int level;
	
	public TUser() {
		super();		
	}
	public TUser(Element root) {
		init(root);
	}
	public void addGroup(String groupname) {
		if(groupname==null) { return; }
		getGroups().add(groupname);
	}
	public void addRole(RoleInterface role) {
		if(role==null) { return; }
		if(getRoles().contains(role)) { return; }
		getRoles().add(role);
		role.addUser(this);
	}
	public UserInterface addUser(UserInterface user) {
		if(user==null) {
			return user;
		}
		if(getUsers().contains(user)) {
			return user;
		}
		getUsers().add(user);
		user.setParent(this);
		return user;
	}
	public void fetchResult(BeanSchema bean) {
		if(bean==null) { return; }
		setSite(bean.getString("site"));
		setUser(bean.getString("userid"));
		String usrname = bean.getString("usertname");
		String usrsurname = bean.getString("usertsurname");
		setUserNameth((usrname==null?"":usrname)+" "+(usrsurname==null?"":usrsurname));
		usrname = bean.getString("userename");
		usrsurname = bean.getString("useresurname");
		setUserNameen((usrname==null?"":usrname)+" "+(usrsurname==null?"":usrsurname));
		setUserType(bean.getString("usertype"));
		setEmail(bean.getString("email"));
		setSupervisor(bean.getString("supervisor"));
		setEmployeeid(bean.getString("employeeid"));
		setLevelid(bean.getString("levelid"));
		setInactive(bean.getString("inactive"));
	}
	public void fetchResult(java.sql.ResultSet rs) throws Exception {
		setSite(rs.getString("site"));
		setUser(rs.getString("userid"));
		String usrname = rs.getString("usertname");
		String usrsurname = rs.getString("usertsurname");
		setUserNameth((usrname==null?"":usrname)+" "+(usrsurname==null?"":usrsurname));
		usrname = rs.getString("userename");
		usrsurname = rs.getString("useresurname");
		setUserNameen((usrname==null?"":usrname)+" "+(usrsurname==null?"":usrsurname));
		setUserType(rs.getString("usertype"));
		setEmail(rs.getString("email"));
		setSupervisor(rs.getString("supervisor"));
		setEmployeeid(rs.getString("employeeid"));
		setLevelid(rs.getString("levelid"));
		setInactive(rs.getString("inactive"));
	}
	public RoleInterface findRole(String roleId) {
		if(roleId==null) {
			return null;
		}
		if(roles!=null) {
			for(RoleInterface role : roles) {
				if(role!=null && roleId.equals(role.getRoleId())) {
					return role;
				}
			}
		}
		return null;
	}
	public void findUsers(java.util.List<UserInterface> users) {
		findUsers(getUser(),users);
	}
	public void findUsers(String userid,java.util.List<UserInterface> users) {
		if(users==null) { return; }
		if(usersList!=null) {
			for(int i=0,isz=usersList.size();i<isz;i++) {
				UserInterface usr = usersList.get(i);
				if(usr!=null && (usr.getUser()!=null && !usr.getUser().equalsIgnoreCase(userid))) {
					users.add(usr);
					usr.findUsers(userid,users);
				}
			}
		}						
	}
	public String getEmail() {
		return email;
	}
	public String getEmployeeid() {
		return employeeid;
	}
	public java.util.List<String> getGroups() {
		if(groups==null) groups = new java.util.ArrayList<>();
		return groups;
	}
	public String getInactive() {
		return inactive;
	}
	public int getLevel() {
		return level;
	}
	public String getLevelid() {
		return levelid;
	}
	public java.util.Map<String,UserInterface> getMembers() {
		java.util.Map<String,UserInterface> members = new java.util.HashMap<>();
		java.util.List<UserInterface> users = getUnderLines();
		if(users!=null) {
			for(UserInterface usr : users) {
				if(usr!=null) {
					members.put(usr.getUser(), usr);
				}
			}
		}
		return members;
	}
	public UserInterface getParent() {
		return parent;
	}
	public java.util.List<RoleInterface> getRoles() {
		if(roles==null) roles = new java.util.ArrayList<>();
		return roles;
	}
	public String getSite() {
		return site;
	}
	public String getSupervisor() {
		return supervisor;
	}
	public java.util.List<UserInterface> getUnderLines() {
		java.util.List<UserInterface> users = new java.util.ArrayList<>();
		findUsers(users);
		return users;
	}
	public String getUser() {
		return userID;
	}
	public String getUserName() {
		return getUserNameth();
	}
	public String getUserNameen() {
		return userNameen;
	}
	public String getUserNameth() {
		return userNameth;
	}
	public java.util.List<UserInterface> getUsers() {
		if(usersList==null) usersList = new java.util.ArrayList<>();
		return usersList;
	}
	public String getUserType() {
		return userType;
	}
	public void init(Element root) {
		String attr = root.getAttribute("id");
		if(attr!=null && attr.trim().length()>0) userID = attr;
		attr = root.getAttribute("name");
		if(attr!=null && attr.trim().length()>0) userNameth = attr;
		attr = root.getAttribute("nameth");
		if(attr!=null && attr.trim().length()>0) userNameth = attr;
		attr = root.getAttribute("nameen");
		if(attr!=null && attr.trim().length()>0) userNameen = attr;
		attr = root.getAttribute("email");
		if(attr!=null && attr.trim().length()>0) email = attr;
		attr = root.getAttribute("type");
		if(attr!=null && attr.trim().length()>0) userType = attr;
		attr = root.getAttribute("supervisor");
		if(attr!=null && attr.trim().length()>0) supervisor = attr;
		attr = root.getAttribute("employeeid");
		if(attr!=null && attr.trim().length()>0) employeeid = attr;
		attr = root.getAttribute("levelid");
		if(attr!=null && attr.trim().length()>0) levelid = attr;
		attr = root.getAttribute("level");
		if(attr!=null && attr.trim().length()>0) {
			level = Integer.parseInt(attr);
		}
		attr = root.getAttribute("inactive");
		if(attr!=null && attr.trim().length()>0) inactive = attr;
	}
	public boolean isGroup(String groupname) {
		if(groups!=null) {
			for(String gid : groups) {
				if(gid!=null && gid.equals(groupname)) return true;
			}
		}
		return false;
	}
	public boolean isInactive() {
		return "1".equals(getInactive());
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
	public void setInactive(String inactive) {
		this.inactive = inactive;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public void setLevelid(String levelid) {
		this.levelid = levelid;
	}
	public void setParent(UserInterface parent) {
		this.parent = parent;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}
	public void setUser(String user) {
		userID = user;
	}
	public void setUserName(String newName) {
		setUserNameth(newName);
	}
	public void setUserNameen(String nameen) {
		this.userNameen = nameen;
	}
	public void setUserNameth(String nameth) {
		this.userNameth = nameth;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String toJSON() {
		return toJSONObjects().toJSONString();
	}
	public JSONObject toJSONObject() {
		JSONObject json = new JSONObject();
		json.put("id", userID==null?"":userID);
		json.put("type", userType==null?"":userType);
		json.put("nameth", userNameth==null?"":userNameth);
		json.put("nameen", userNameen==null?"":userNameen);
		json.put("email", email==null?"":email);
		json.put("supervisor", supervisor==null?"":supervisor);
		json.put("employeeid", employeeid==null?"":employeeid);
		json.put("levelid", levelid==null?"":levelid);
		json.put("level", Integer.toString(level));
		json.put("inactive", inactive==null?"":inactive);
		return json;
	}
	public JSONObject toJSONObjects() {
		JSONObject json = toJSONObject();
		if(usersList!=null && !usersList.isEmpty()) {
			JSONArray array = new JSONArray();
			json.put("users",array);
			for(int i=0,isz=usersList.size();i<isz;i++) {
				UserInterface usr = usersList.get(i);
				if(usr!=null) {
					array.add(usr.toJSONObjects());
				}
			}
		}		
		return json;
	}
	public String toJSONString() {
		return toJSONObject().toJSONString();
	}
	public String toString() {
		return toJSONString();
	}
	public String toXML() {
		return toXML(1);
	}
	public String toXML(int level) {
		StringBuilder buf = new StringBuilder();
		for(int i=0;i<level;i++) buf.append("\t");
		buf.append("<user id=\""+userID+"\"");
		if(userType!=null) buf.append(" type=\""+userType+"\"");
		if(userNameth!=null) buf.append(" nameth=\""+BeanUtility.preserveXML(userNameth)+"\"");
		if(userNameen!=null) buf.append(" nameen=\""+BeanUtility.preserveXML(userNameen)+"\"");
		if(email!=null) buf.append(" email=\""+BeanUtility.preserveXML(email)+"\"");
		if(supervisor!=null) buf.append(" supervisor=\""+supervisor+"\"");
		if(employeeid!=null) buf.append(" employeeid=\""+employeeid+"\"");
		if(levelid!=null) buf.append(" levelid=\""+levelid+"\"");
		if(level>0) buf.append(" level=\""+level+"\"");
		if(inactive!=null) buf.append(" inactive=\""+(inactive==null||inactive.trim().length()<=0?"0":inactive)+"\"");
		buf.append(">\n");
		if(usersList!=null) {
			for(int i=0,isz=usersList.size();i<isz;i++) {
				UserInterface usr = usersList.get(i);
				if(usr!=null) {
					buf.append(usr.toXML(level+1));
				}
			}
		}		
		for(int i=0;i<level;i++) buf.append("\t");	
		buf.append("</user>\n");
		return buf.toString();
	}	
}
