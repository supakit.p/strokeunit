package com.fs.dev.tree;

/**
 * @author tassun_oro
 *
 */
public interface RoleInterface {
	public void addUser(UserInterface user);
	public UserInterface findUser(String userId);
	public String getRoleId();
	public String getRoleName();
	public java.util.List<UserInterface> getUsers();
}
