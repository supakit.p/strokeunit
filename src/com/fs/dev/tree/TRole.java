package com.fs.dev.tree;

/**
 * @author tassun_oro
 *
 */
public class TRole implements RoleInterface {
    private String roleId;
    private String roleName;	
	private java.util.List<UserInterface> usersList;
	public TRole() {
		super();
	}
	public void addUser(UserInterface user) {
		if (user == null) { return; }	      
		if (getUsers().contains(user)) {
			return;
		}
		getUsers().add(user);
		user.addRole(this);
	}
	public void fetchResult(java.sql.ResultSet rs) throws Exception {
		setRoleId(rs.getString("roleid"));
		setRoleName(rs.getString("nameth"));
	}
	public UserInterface findUser(String userId) {
        if (userId == null) {
        	return null;        
        }
        if(usersList!=null) {
        	for (UserInterface user : usersList) {
        		if (user != null && userId.equals(user.getUser())) {
        			return user;
        		}
        	}
        }
        return null;
    }
	public String getRoleId() {
		return roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public java.util.List<UserInterface> getUsers() {
		if (usersList == null) {
			usersList = new java.util.ArrayList<>();
		}
		return usersList;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}	
    public void setRoleName(String roleName) {
		this.roleName = roleName;
	}	
}
