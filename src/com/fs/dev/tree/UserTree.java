package com.fs.dev.tree;

import com.fs.bean.ExecuteBean;
import com.fs.bean.ExecuteData;
import com.fs.bean.gener.BeanSchema;
import com.fs.bean.misc.KnSQL;
import com.fs.bean.misc.Trace;
import com.fs.bean.util.GlobalBean;
import com.fs.bean.util.GlobalVariable;
import com.fs.dev.Arguments;
import com.fs.dev.Console;
import com.fs.dev.VerifyException;

/**
 * @author tassun_oro
 *
 */
@SuppressWarnings("rawtypes")
public class UserTree {
	private String site;
	private java.util.Map<String,UserInterface> users;
	private java.util.Map<String,RoleInterface> roles;
	private java.util.Map<String,UserInterface> leavers;
	private boolean english = true;
	public UserTree() {	
		super();
	}
	public UserTree(boolean eng) {
		this.english = eng;
	}
	public UserTree(String site) {
		this.site = site;
	}
	public UserTree(String site,boolean eng) {
		this.site = site;
		this.english = eng;
	}
	public static void main(String[] args) {
    	//java com/fs/dev/tree/UserTree -ms PROMPT -site FWG -user ttso
    	try {
    		if(args.length>0) {
        		boolean transport = Arguments.getBoolean(args, false, "-trans");
        		String section = Arguments.getString(args, "PROMPT", "-ms");
        		String userid = Arguments.getString(args, null, "-user");
    			UserTree ut = new UserTree("FWG");
    			ut.setSite(Arguments.getString(args, ut.getSite(), "-site"));
    			if(transport) {
	    			GlobalBean fsGlobal = new GlobalBean();
	    			fsGlobal.setFsSection(section); 
	    			fsGlobal.setFsProg("usertree");
	    			fsGlobal.setFsVar("fsUser","test");
	    			fsGlobal.setFsAction(GlobalBean.COLLECT_MODE);    			
	    			ExecuteBean bean = new ExecuteBean();
	    			KnSQL knsql = bean.getKnSQL();
	    	        knsql.append("select tuserinfo.site,tuserinfo.userid,tuserinfo.usertname,tuserinfo.usertsurname,tuserinfo.supervisor,tuserinfo.employeeid,tuserinfo.email,tuserinfo.levelid,tuserinfo.inactive,tuser.usertype ");
	    	        knsql.append("from tuserinfo ");
	    	        knsql.append("left join tuser on tuser.site = tuserinfo.site and tuser.userid = tuserinfo.userid ");
	    	        knsql.append("where tuserinfo.site = '"+ut.getSite()+"' ");	    		
	    			bean.transport(fsGlobal);
	    			ut.load(bean);
    			} else {
	    			try(java.sql.Connection connection = ExecuteData.getNewConnection(section,false)) {
	    				ut.load(connection);
	    			}
    			}
    			if(userid!=null) {
    				UserInterface usr = ut.findUser(userid);
    				Console.out.println(usr);
    				if(usr!=null) {
    					Console.out.println(usr.getUserNameen()+" : "+usr.getUserNameth());
    					Console.out.println("===============================================");
    					Console.out.println("users = "+usr.getUsers());
    					Console.out.println("===============================================");
    					Console.out.println("supervisor : "+usr.getSupervisor()+", "+ut.findUser(usr.getSupervisor()));
    					Console.out.println("===============================================");
    					Console.out.println("under lines = "+usr.getUnderLines());
    				}
    			}
				Console.out.println("===============================================");
    			Console.out.println(ut.getUsers());
				Console.out.println("===============================================");
    			Console.out.println("users = "+(ut.getUsers()==null?0:ut.getUsers().size()));
    		} else {
    			Console.out.println("USAGE : "+UserTree.class);
    			Console.out.println("\t-ms	private db section");
    		}
    	} catch(java.sql.SQLException ex) {
    		Console.out.print(ex);
    		Console.out.println("ERROR CODE : "+ex.getErrorCode());
    	} catch(Exception ex) {
    		Console.out.print(ex);
    	}		
	}
	protected void clearUsers() {
        if(users!=null) {
        	users.clear();    	
        }
    } 
	public void build() throws Exception {
    	if(users!=null) {    		
    		for(java.util.Iterator<String> it = users.keySet().iterator();it.hasNext();) {
    			String userid = it.next();
    			UserInterface usr = users.get(userid);
    			String supervisor = usr.getSupervisor();
    			if(supervisor!=null && !supervisor.equals(userid)) {
    				UserInterface sup = findUser(supervisor);
    				if(sup!=null) {
    					sup.addUser(usr);
    				}
    			}
    		}
    	}
    }
	public RoleInterface findRole(String roleId) {
    	if(roleId==null || roleId.trim().length()<=0) {
    		return null;
    	}
    	return getRoles().get(roleId);
    }
    public UserInterface findUser(String userId) {
        if (userId==null || userId.trim().length()<=0) {
        	return null;
        }
        return getUsers().get(userId);
    }
    public UserInterface getFirstManager(String userid) {
		if(userid==null) {
			return null;
		}
		UserInterface usr = findUser(userid);
    	if(usr!=null) {
			String supid = usr.getSupervisor();
			if(supid!=null) {				
				return findUser(supid);
			}
    	}
		return null;
	}  
    public java.util.Map<String,UserInterface> getLeavers() {
        if (leavers == null) {
            leavers = new java.util.HashMap<>();
        }
        return leavers;
    }
    public java.util.Map<String,RoleInterface> getRoles() {
        if (roles == null) {
            roles = new java.util.HashMap<>();
        }
        return roles;
    }
    public UserInterface getSecondManager(String userid) {
		if(userid==null) {
			return null;
		}
		UserInterface usr = findUser(userid);
    	if(usr!=null) {
			String supid = usr.getSupervisor();
			if(supid!=null) {				
				UserInterface sup = findUser(supid);
				if(sup!=null) {
					supid = sup.getSupervisor();
					if(supid!=null) {
						return findUser(supid);
					}
				}
			}
    	}
		return null;
	}
    public String getSite() {
		return site;
	}
    public UserInterface getSupercoach(String userid) {
		if(userid==null) {
			return null;
		}
		UserInterface usr = findUser(userid);
    	if(usr!=null) {
			String supid = usr.getSupervisor();
			if(supid!=null) {				
				UserInterface sup = findUser(supid);
				while(sup!=null) {
					if(sup.isGroup("SUPERCOACH")) {
						return sup;
					}
					sup = sup.getParent();
				}
			}
    	}
		return null;
	}
    public java.util.Map<String,UserInterface> getUsers() {
        if (users == null) {
            users = new java.util.LinkedHashMap<>();
        }
        return users;
    }    
    public boolean isEnglish() {
		return english;
	}
    public boolean isLeave(String userid) {
    	if(userid==null) {
    		return false;
    	}
    	Object checkLeave = GlobalVariable.getVariable("WORKFLOW_CHECK_LEAVE");
    	if(!(checkLeave!=null && "true".equalsIgnoreCase(checkLeave.toString()))) {
    		return false;
    	}
    	UserInterface user = getLeavers().get(userid);
    	if(user!=null) {
    		return true;
    	}
    	return false;
    }
	public boolean isLeave(UserInterface user) {
    	if(user==null) {
    		return false;
    	}
    	return isLeave(user.getUser());
    }
	public void load(BeanSchema bean) throws Exception {
    	if(bean==null) { return; }
        clearUsers();		
    	for(java.util.Enumeration ens = bean.elements();ens.hasMoreElements();) {
    		BeanSchema bs = (BeanSchema)ens.nextElement();
    		TUser user = new TUser();
    		user.fetchResult(bs);
            if(user.getUser()!=null) {
            	getUsers().put(user.getUser(), user);
            }    		
    	}
    	build();
    }
	public void load(java.sql.Connection connection) throws Exception {
        if (connection == null) {
            throw new VerifyException("Connection is undefined");
        }
        clearUsers();
        java.util.Map<String,Integer> lvmap = new java.util.HashMap<>();
        KnSQL knsql = new KnSQL(this);
        knsql.append("select levelid,level from tlevel");
        try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
	        while (rs.next()) {
	        	String levelid = rs.getString("levelid");
	        	int level = rs.getInt("level");
	        	lvmap.put(levelid, Integer.valueOf(level));
	        }        
        }
        knsql.clear();
        knsql.append("select tuserinfo.site,tuserinfo.userid,tuserinfo.userename,tuserinfo.useresurname,tuserinfo.usertname,tuserinfo.usertsurname,tuserinfo.supervisor,tuserinfo.employeeid,tuserinfo.email,tuserinfo.levelid,tuserinfo.inactive,tuser.usertype ");
        knsql.append("from tuserinfo ");
        knsql.append("left join tuser on tuser.site = tuserinfo.site and tuser.userid = tuserinfo.userid ");
        knsql.append("where tuserinfo.site = ?site ");
        if(isEnglish()) {
        	knsql.append("order by userename,useresurname ");
        } else {
        	knsql.append("order by usertname,usertsurname ");
        }
        knsql.setParameter("site",getSite());
        try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
	        while (rs.next()) {
	            TUser user = new TUser();
	            user.fetchResult(rs);
	            Integer level = lvmap.get(user.getLevelid());
	            if(level!=null) user.setLevel(level.intValue());
	            if(user.getUser()!=null) {
	            	getUsers().put(user.getUser(), user);
	            }
	        }   
        }
        knsql.clear();
        knsql.append("select tusergrp.userid,tusergrp.groupname from tusergrp,tuserinfo ");
        knsql.append("where tusergrp.userid = tuserinfo.userid ");
        knsql.append("and tuserinfo.site = ?site ");
        knsql.setParameter("site",getSite());
        try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
	        while (rs.next()) {
	        	String uid = rs.getString("userid");
	        	String gid = rs.getString("groupname");
	            if(uid!=null && gid!=null) {
	            	UserInterface usr = findUser(uid);
	            	if(usr!=null) {
	            		usr.addGroup(gid);
	            	}
	            }
	        }    
        }
        knsql.clear();
        knsql.append("select roleid,nameth from trole where site = ?site ");
        knsql.setParameter("site",getSite());
        try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
	        while (rs.next()) {
	            TRole role = new TRole();
	            role.fetchResult(rs);
	            getRoles().put(role.getRoleId(), role);	
	        }    
        }
        knsql.clear();
        knsql.append("select tuserrole.userid,tuserrole.roleid from tuserrole,tuserinfo ");
        knsql.append("where tuserrole.userid = tuserinfo.userid ");
        knsql.append("and tuserinfo.site = ?site ");
        knsql.setParameter("site",getSite());
        try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
	        while (rs.next()) {
	        	String uid = rs.getString("userid");
	        	String rid = rs.getString("roleid");
	            if(uid!=null && rid!=null) {
	            	RoleInterface role = getRoles().get(rid);
	            	UserInterface usr = findUser(uid);
	            	if(usr!=null && role!=null) {
	            		usr.addRole(role);
	            	}
	            }
	        }  
        }
        leavers = new java.util.HashMap<>();
        java.sql.Date curdate = new java.sql.Date(System.currentTimeMillis());
        Trace.debug("current date " + curdate);
        knsql.clear();
        knsql.append("select distinct userid from tuserleave ");
        knsql.append("where site = ?site and ?leavedate between startdate and enddate ");
        knsql.setParameter("site",getSite());
        knsql.setParameter("leavedate", curdate);
        try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
	        while (rs.next()) {
	            String uid = rs.getString("userid");
	            UserInterface usr = findUser(uid);
	            if (usr != null) {
	                leavers.put(usr.getUser(), usr);
	            }
	        }
        }
        Trace.debug("leavers " + leavers);        
        build();
    }
    public void setEnglish(boolean eng) {
		this.english = eng;
	}
    public void setSite(String site) {
		this.site = site;
	}
}
