package com.fs.dev.tree;

import org.json.simple.JSONObject;

/**
 * @author tassun_oro
 *
 */
public interface UserInterface {
	public void addGroup(String groupname);
	public void addRole(RoleInterface role);
	public UserInterface addUser(UserInterface user);
	public RoleInterface findRole(String roleId);
	public void findUsers(java.util.List<UserInterface> users);
	public void findUsers(String userid,java.util.List<UserInterface> users);
	public String getEmail();
	public String getEmployeeid();
	public java.util.List<String> getGroups();
	public int getLevel();
	public java.util.Map<String,UserInterface> getMembers();
	public UserInterface getParent();
	public java.util.List<RoleInterface> getRoles();
	public String getSite();
	public String getSupervisor();
	public java.util.List<UserInterface> getUnderLines();
	public String getUser();
	public String getUserName();
	public String getUserNameen();
	public String getUserNameth();
	public java.util.List<UserInterface> getUsers();
	public String getUserType();
	public boolean isGroup(String groupname);
	public boolean isInactive();
	public void setParent(UserInterface parent);
	public JSONObject toJSONObject();
	public JSONObject toJSONObjects();
	public String toXML(int level);
}
