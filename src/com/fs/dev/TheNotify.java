package com.fs.dev;

import com.fs.bean.ExecuteData;
import com.fs.bean.ctrl.BeanData;
import com.fs.bean.misc.KnSQL;
import com.fs.bean.misc.Trace;
import com.fs.dev.version.VersionAdapter;
import com.fs.dev.notify.FirebaseNotificationSender;
import com.fs.interfaces.RelatableInterface;

/**
 * @author tassun_oro
 *
 */
@SuppressWarnings("serial")
public class TheNotify extends VersionAdapter implements java.io.Serializable {
	private String notifyCategory = "notification";
	private String firebaseUrl;
	private String firebaseKey;
	private String notifyTitle;
	private String notifyContents;
	private String notifyFlag;
	private String notifyType;
	private String notifyId;
	private String notifySites;
	private String notifySite;
	private String notifyDepartments;
	private String notifyUsers;
	private String notifyAction;
	private java.util.Map<String,Object> notifyData;
	
	public TheNotify() {
		super();
	}
	public TheNotify(RelatableInterface parent) {
		super(parent);
	}
	public TheNotify(RelatableInterface parent,String category) {
		super(parent);
		this.notifyCategory = category;
	}
	public TheNotify(String category) {
		this.notifyCategory = category;
	}
	public static void main(String[] args) {
		//java com/fs/dev/TheNotify -ms PROMPT -site FWG -title Test -text "Hello Notification 1" -flag 1
		//java com/fs/dev/TheNotify -ms PROMPT -site FWG -title Test -text "Hello Notification 2" -flag 2 -sites FWG
		//java com/fs/dev/TheNotify -ms PROMPT -site FWG -title Test -text "Hello Notification 3" -flag 3 -sites FWG -depts 001,002,003,004
		//java com/fs/dev/TheNotify -ms PROMPT -site FWG -title Test -text "Hello Notification 5" -flag 4 -sites FWG -users tso,tas,smt
		//java com/fs/dev/TheNotify -ms PROMPT -site FWG -title Test -text "Hello Notification 6" -token fu8ic4T_y6s:APA91bHAGf2l9zlsMUmdbcsWiLjVf4MH0xe-Aa_GzkQjK3Qaf-4CIS--d1fJ2c4XRkwf0HygSbviJOK6RwQjRXmagUvpjMB1UTLQqBfv7lqTi8waw4NriiYuqSeeFndHHM6IUwywhoRM
		try {
			if(args.length>0) {	
				TheNotify noti = new TheNotify();
				String section = Arguments.getString(args, "", "-ms");
            	noti.setNotifyType(Arguments.getString(args, noti.getNotifyType(), "-type"));
            	noti.setNotifyFlag(Arguments.getString(args, noti.getNotifyFlag(), "-flag"));
                noti.setNotifySite(Arguments.getString(args, noti.getNotifySite(), "-site"));
            	noti.setNotifySites(Arguments.getString(args, noti.getNotifySites(), "-sites"));
            	noti.setNotifyDepartments(Arguments.getString(args, noti.getNotifyDepartments(), "-depts"));
            	noti.setNotifyUsers(Arguments.getString(args, noti.getNotifyUsers(), "-users"));
                noti.setNotifyTitle(Arguments.getString(args, noti.getNotifyTitle(), "-title"));
                noti.setNotifyContents(Arguments.getString(args, noti.getNotifyContents(), "-text"));
                noti.setFirebaseUrl(Arguments.getString(args, noti.getFirebaseUrl(), "-url"));
                noti.setFirebaseKey(Arguments.getString(args, noti.getFirebaseKey(), "-key"));
				java.util.List<String> tokens = null;
                for (int i = 0, isz = args.length; i < isz; i++) {
                    String para = args[i];
                    if (para.equalsIgnoreCase("-token")) {
                        if (args.length > (i + 1)) {
                        	if(tokens==null) tokens = new java.util.ArrayList<>();
                        	tokens.add(args[i + 1]);
                        }
                    }
                }		
                if(noti.getNotifyTitle()!=null && noti.getNotifyContents()!=null) {
                	try(java.sql.Connection connection = ExecuteData.getNewConnection(section,false)) {
                		noti.sendNotify(connection, tokens);
                	}
                }
			} else {
				usage();
			}
		} catch(java.sql.SQLException ex) {
			Console.out.print(ex);
			Console.err.println("ERROR CODE : "+ex.getErrorCode());
		} catch(Exception ex) {
			Console.out.print(ex);
		}
	}
	public static void usage() {
		Console.out.println("USAGE : "+TheNotify.class);
		Console.out.println("\t-ms main db section (via global_config.xml)");
		Console.out.println("\t-flag 1=All,2=Companies,3=Departments,4=Users");
		Console.out.println("\t-type FEED or CUSTOM");
		Console.out.println("\t-site current site");
		Console.out.println("\t-sites companies code ex. FWG,COMP,COMS");
		Console.out.println("\t-depts departments codes ex. 001,002,003");
		Console.out.println("\t-title notify title");
		Console.out.println("\t-texts notify contents");
		Console.out.println("\t-url fire base url");
		Console.out.println("\t-key fire base key");
		Console.out.println("\t-token device token (optional)");
	}
	protected void ensureFireNotify(java.sql.Connection connection) throws Exception {
		if((getFirebaseUrl()!=null && getFirebaseUrl().trim().length()>0) && ((getFirebaseKey()!=null && getFirebaseKey().trim().length()>0))) { return; }
		KnSQL knsql = new KnSQL(this);
		knsql.append("select * from tconfig where category='"+getNotifyCategory()+"' ");
		try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
			while(rs.next()) {
				String colname = rs.getString("colname");
				if("firebase_url".equalsIgnoreCase(colname)) {
					setFirebaseUrl(rs.getString("colvalue"));
				}
				if("firebase_key".equalsIgnoreCase(colname)) {
					setFirebaseKey(rs.getString("colvalue"));
				}
			}
		}
	}
	protected java.util.List<String> ensureTokenList(java.sql.Connection connection) throws Exception {
		java.util.List<String> result = new java.util.ArrayList<>();
		if("1".equals(getNotifyFlag())) { 
			//all (all user's device under current site and head site = current site)
			java.util.ArrayList<String> sitelist = new java.util.ArrayList<>();
			sitelist.add(getNotifySite());
			KnSQL knsql = new KnSQL(this);
			knsql.append("select site from tcomp where headsite = ?site ");
			knsql.setParameter("site", getNotifySite());
			try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
				while(rs.next()) {
					sitelist.add(rs.getString("site"));
				}
			}
			String[] sites = new String[sitelist.size()];
			sitelist.toArray(sites);
			knsql.clear();
			knsql.append("SELECT device_token FROM tuserdeviceinfo ");
			knsql.append("where active=1 and site in (?sites)" );
			knsql.setParameter("sites", sites);
			try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
				while(rs.next()) {
					String device_token = rs.getString("device_token");
					if(device_token!=null && device_token.length()>0) {
						result.add(device_token);
					}
				}
			}
		} else if("2".equals(getNotifyFlag())) {
			//companies
			if(getNotifySites()!=null && getNotifySites().trim().length()>0) {
				String[] sites = getNotifySites().split(",");
				KnSQL knsql = new KnSQL(this);
				knsql.append("SELECT device_token FROM tuserdeviceinfo ");
				knsql.append("where active=1 and site in (?sites)" );
				knsql.setParameter("sites", sites);
				try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
					while(rs.next()) {
						String device_token = rs.getString("device_token");
						if(device_token!=null && device_token.length()>0) {
							result.add(device_token);
						}
					}
				}
			}
		} else if("3".equals(getNotifyFlag())) {
			//departments
			if(getNotifyDepartments()!=null && getNotifyDepartments().trim().length()>0) {
				String[] departments = getNotifyDepartments().split(",");
				KnSQL knsql = new KnSQL(this);
				knsql.append("SELECT tuserdeviceinfo.device_token FROM tuserdeviceinfo, tuserinfo ");
				knsql.append("where tuserdeviceinfo.active=1 and tuserdeviceinfo.site = ?site ");
				knsql.append("and tuserdeviceinfo.user_id = tuserinfo.userid ");
				knsql.append("and tuserinfo.deptcode in (?deptcodes) ");
				knsql.setParameter("site", getNotifySites());
				knsql.setParameter("deptcodes", departments);
				try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
					while(rs.next()) {
						String device_token = rs.getString("device_token");
						if(device_token!=null && device_token.length()>0) {
							result.add(device_token);
						}
					}
				}
			}
		} else if("4".equals(getNotifyFlag())) {
			//specified users
			if(getNotifyUsers()!=null && getNotifyUsers().trim().length()>0) {
				String[] users = getNotifyUsers().split(",");
				KnSQL knsql = new KnSQL(this);
				knsql.append("SELECT device_token FROM tuserdeviceinfo ");
				knsql.append("where active=1 and site = ?site ");
				knsql.append("and user_id in (?users) ");
				knsql.setParameter("site", getNotifySites());
				knsql.setParameter("users", users);
				try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
					while(rs.next()) {
						String device_token = rs.getString("device_token");
						if(device_token!=null && device_token.length()>0) {
							result.add(device_token);
						}
					}
				}
			}
		}
		return result;
	}
	public String getFirebaseKey() {
		return firebaseKey;
	}
	public String getFirebaseUrl() {
		return firebaseUrl;
	}
	public String getNotifyAction() {
		return notifyAction;
	}
	public String getNotifyCategory() {
		return notifyCategory;
	}
	public String getNotifyContents() {
		return notifyContents;
	}
	public java.util.Map<String,Object> getNotifyData() {
		return notifyData;
	}
	public String getNotifyDepartments() {
		return notifyDepartments;
	}
	public String getNotifyFlag() {
		return notifyFlag;
	}
	public String getNotifyId() {
		return notifyId;
	}
	public String getNotifySite() {
		return notifySite;
	}
	public String getNotifySites() {
		return notifySites;
	}
	public String getNotifyTitle() {
		return notifyTitle;
	}
	public String getNotifyType() {
		return notifyType;
	}
	public String getNotifyUsers() {
		return notifyUsers;
	}
	public void obtain(BeanData data) {
		if(data==null) return;
		setNotifyId(data.getString("notifyid"));
		setNotifyFlag(data.getString("notifyflag"));
		setNotifyType(data.getString("notifytype"));
		setNotifyTitle(data.getString("notifytitle"));
		setNotifyContents(data.getString("notifycontents"));
		setNotifySite(data.getString("site"));
		setNotifySites(data.getString("notifysites"));
		setNotifyDepartments(data.getString("notifydepts"));
		setNotifyAction(data.getString("notifyaction"));
	}
	public void sendNotify(java.sql.Connection connection) throws Exception {
		sendNotify(connection,null);
	}
	public void sendNotify(java.sql.Connection connection,java.util.List<String> tokens) throws Exception {
		ensureFireNotify(connection);
		if((getFirebaseUrl()==null || getFirebaseUrl().trim().length()<=0) && ((getFirebaseKey()==null || getFirebaseKey().trim().length()<=0))) {
			Trace.info(this,"not found notify fire url & key ...");
			return;
		}
		if(tokens==null) {
			tokens = ensureTokenList(connection);
		}
		if(tokens!=null && !tokens.isEmpty()) {
			Trace.debug(this,"notify = "+this);
			Trace.debug(this,"notify tokens = "+tokens);
			String contents = getNotifyContents();
			if(contents.indexOf('<')>=0 || contents.indexOf('>')>=0) {
				try {
					contents = com.fs.dev.HtmlToText.htmlToText(contents);
				}catch(Exception ex) { }
			}
            FirebaseNotificationSender notificationSender = new FirebaseNotificationSender(getFirebaseUrl(), getFirebaseKey());
            Object reply = notificationSender.send(tokens, getNotifyTitle(), contents, getNotifyAction(), getNotifyData());
			Trace.info(this,"notify reply = "+reply);
		}
	}
	public void setFirebaseKey(String firebaseKey) {
		this.firebaseKey = firebaseKey;
	}
	public void setFirebaseUrl(String firebaseUrl) {
		this.firebaseUrl = firebaseUrl;
	}
	public void setNotifyAction(String notifyAction) {
		this.notifyAction = notifyAction;
	}
	public void setNotifyCategory(String notifyCategory) {
		this.notifyCategory = notifyCategory;
	}
	public void setNotifyContents(String notifyContents) {
		this.notifyContents = notifyContents;
	}
	public void setNotifyData(java.util.Map<String,Object> notifyData) {
		this.notifyData = notifyData;
	}
	public void setNotifyDepartments(String notifyDepartments) {
		this.notifyDepartments = notifyDepartments;
	}
	public void setNotifyFlag(String notifyFlag) {
		this.notifyFlag = notifyFlag;
	}
	public void setNotifyId(String notifyId) {
		this.notifyId = notifyId;
	}
	public void setNotifySite(String notifySite) {
		this.notifySite = notifySite;
	}
	public void setNotifySites(String notifySites) {
		this.notifySites = notifySites;
	}
	public void setNotifyTitle(String notifyTitle) {
		this.notifyTitle = notifyTitle;
	}
	public void setNotifyType(String notifyType) {
		this.notifyType = notifyType;
	}
	public void setNotifyUsers(String notifyUsers) {
		this.notifyUsers = notifyUsers;
	}	
	public String toString() {
		return super.toString()+"{id="+notifyId+",flag="+notifyFlag+",type="+notifyType+",site="+notifySite+",title="+notifyTitle+",contents="+notifyContents+",sites="+notifySites+",departments="+notifyDepartments+",users="+notifyUsers+"}";
	}
}
