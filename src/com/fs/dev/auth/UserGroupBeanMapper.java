package com.fs.dev.auth;

/*
 * SCCS id: $Id: UserGroupBeanMapper.java,v 1.0 2008-12-03 14:03:56+07 apipong_kan Exp $
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile: UserGroupBeanMapper.java,v $
 * Description: UserGroupBeanMapper class implements for handle mapping class
 * Version: $Revision: 1.0 $
 * Programmer: $Author: apipong_kan $
 * Creation date: (03/11/2008 16:21:56)
 */
/**
 * UserGroupBeanMapper class implements for handle mapping class.
 */
import com.fs.bean.gener.*;

//#import anything the right time
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings("serial")
public class UserGroupBeanMapper extends BeanMap {
	//#another methods defined irresistible
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	public UserGroupBeanMapper() {
		super();
	}
	public String fetchVersion() {
		return super.fetchVersion()+UserGroupBeanMapper.class+"=$Revision: 1.0 $\n";
	}
	protected void initialize() {
		super.initialize();
		mapClass("com.fs.dev.auth.UserGroupData");
		//#other initial give me a reason
		//#(20000) programmer code begin;
		//#(20000) programmer code end;
	}
}
