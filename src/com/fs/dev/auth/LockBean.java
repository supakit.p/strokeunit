package com.fs.dev.auth;

import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.Console;
import com.fs.bean.ctrl.*;

@SuppressWarnings({ "serial", "unchecked" })
public class LockBean extends BeanSchema implements Runnable {
	private Thread reaper = null;
	private GlobalBean global = null;
	//#methods defined everything will flow
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	//#member & declaration remember me this way
	//#(10000) programmer code begin;
	//#(10000) programmer code end;
	public LockBean() {
		super();
	}
	public void execute(GlobalBean global) {
		this.global = global;
		reaper = new Thread(this,"LockBean-Thread");
		reaper.start();
	}
	public String fetchVersion() {
		return super.fetchVersion()+LockData.class+"=$Revision: 1.1 $\n";
	}
	public String getLock() {
		return getMember("lockflag");
	}
	public String getUser() {
		return getMember("user");
	}
	protected void initialize() {
		super.initialize();
		addSchema("user",java.sql.Types.VARCHAR,"User");
		addSchema("lockflag",java.sql.Types.VARCHAR,"Lock");
		//#initialize & assigned always somewhere
		//#(20000) programmer code begin;
		//#(20000) programmer code end;
	}
	@SuppressWarnings("deprecation")
	public void run() {
		try {
			TheTransportor.transport(global,this);
		} catch(Exception ex) {
			Console.out.print(ex);
		}
		global = null;
		if(reaper!=null) reaper.stop();
		reaper = null;
	}
	public void setLock(String newLock) {
		setMember("lockflag",newLock);
	}
	public void setUser(String newUser) {
		setMember("user",newUser);
	}
}
