package com.fs.dev.auth;

import org.w3c.dom.Element;

import com.fs.bean.util.BeanUtility;

@SuppressWarnings({"serial"})
public class GroupEntity implements java.io.Serializable {
	private String groupname = null;
	private String supergroup = null;
	private String description = null;
	private java.util.HashMap<String,GroupEntity> childs = null;	
	private UserList users = null;
	
	public GroupEntity() {
		super();
	}
	public GroupEntity(Element root) {
		groupname = root.getAttribute("name");
		if(groupname==null) {
			groupname = root.getAttribute("groupname");
		}
		supergroup = root.getAttribute("supergroup");
		description = root.getAttribute("description");
		users = new UserList(root,this);
	}
	public void addGroup(GroupEntity group) {
		if(group==null) { return; }
		if(childs==null) {
			childs = new java.util.HashMap<>();
		}
		childs.put(group.groupname,group);
	}
	public void addUser(UserEntity user,String role) {
		if(user==null) { return; }
		if(users==null) {
			users = new UserList();
		}
		users.addUser(user);
		user.addGroup(groupname,role);
	}
	public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
		groupname = rs.getString("groupname");
		supergroup = rs.getString("supergroup");
		description = rs.getString("description");
	}
	public java.util.Map<String, GroupEntity> getChilds() {
		if(childs==null) {
			childs = new java.util.HashMap<>();
		}
		return childs;
	}
	public String getDescription() {
		return description;
	}
	public String getGroupname() {
		return groupname;
	}
	public String getSupergroup() {
		return supergroup;
	}
	public UserList getUserList() {
		return users;
	}	
	public void setDescription(String description) {
		this.description = description;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}		
	public void setSupergroup(String supergroup) {
		this.supergroup = supergroup;
	}
	public String toString() {
		return super.toString()+"{name="+groupname+",supergroup="+supergroup+",description="+description+"}";
	}
	public String toXML() {
		return toXML(0);
	}
	public String toXML(int level) {
		StringBuilder buf = new StringBuilder();
		for(int i=0;i<level;i++) buf.append("\t");
		buf.append("<item");
		if(groupname!=null) buf.append(" name=\""+groupname+"\"");
		if(supergroup!=null && !supergroup.trim().equals("")) buf.append(" supergroup=\""+supergroup+"\"");			
		if(description!=null) buf.append(" description=\""+BeanUtility.preserveXML(description)+"\"");
		if(childs!=null && !childs.isEmpty()) {
			buf.append(">\n");
			if(users!=null && !users.isEmpty()) {
				buf.append(users.xmlUser(level+1));
			} 				
			for(GroupEntity ge : childs.values()) {
				if(ge!=null) {
					buf.append(ge.toXML(level+1));
				}
			}
			for(int i=0;i<level;i++) buf.append("\t");
		} else {
			if(users!=null && !users.isEmpty()) {
				buf.append(">\n");
				buf.append(users.xmlUser(level+1));
				for(int i=0;i<level;i++) buf.append("\t");
			}  else {				
				buf.append(">");
			}
		}
		buf.append("</item>\n");
		return buf.toString();
	}
}
