package com.fs.dev.auth;

import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.Console;
import com.fs.bean.ctrl.*;

@SuppressWarnings({"serial","rawtypes"})
public class UserGroupBean extends BeanSchema {
	//#methods defined everything will flow
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	//#member & declaration remember me this way
	//#(10000) programmer code begin;
	//#(10000) programmer code end;
	public UserGroupBean() {
		super();
	}
	public String fetchVersion() {
		return super.fetchVersion()+UserGroupBean.class+"=$Revision: 1.0 $\n";
	}
	public String getGroups() {
		return getMember("groups");
	}
	public String getUsers() {
		return getMember("users");
	}
	protected void initialize() {
		super.initialize();
		addSchema("groups",java.sql.Types.VARCHAR,"Groups");
		addSchema("users",java.sql.Types.VARCHAR,"Users");
		//#initialize & assigned always somewhere
		//#(20000) programmer code begin;
		//#(20000) programmer code end;
	}
	public static void main(String[] args) {
		try {
			StringBuilder gbuf = new StringBuilder();
			gbuf.append("<item groupname=\"MD\" description=\"Management Director\">");
	        gbuf.append("	<item groupname=\"ADMIN\" supergroup=\"MD\" description=\"Administrator\">");
			gbuf.append("		<item groupname=\"OPERATOR\" supergroup=\"ADMIN\" description=\"Operator\"></item>");
			gbuf.append("		<item groupname=\"TESTER\" supergroup=\"ADMIN\" description=\"Testing\"></item>");
			gbuf.append("		<item groupname=\"TSOGRP\" supergroup=\"ADMIN\" description=\"Testing\"></item>");
			gbuf.append("		<item groupname=\"QA\" supergroup=\"ADMIN\" description=\"Quality Assurance\"></item>");
	        gbuf.append("	</item>");
			gbuf.append("</item>");
	
			StringBuilder ubuf = new StringBuilder();
			ubuf.append("<users>");
	        ubuf.append("	<user>");
			ubuf.append("		<uid>admin</uid>");
			ubuf.append("		<name>Adminstrator </name>");
	        ubuf.append("	</user>");
	        ubuf.append("	<user>");
			ubuf.append("		<uid>tas</uid>");
			ubuf.append("		<name>Tassun Oros</name>");
			ubuf.append("		<groups>");
			ubuf.append("			<group>");
			ubuf.append("				<gid>ADMIN</gid>");
			ubuf.append("				<role>ADMIN</role>");
			ubuf.append("			</group>");
			ubuf.append("		</groups>");
	        ubuf.append("	</user>");
	        ubuf.append("	<user>");
			ubuf.append("		<uid>tso</uid>");
			ubuf.append("		<name>Tassun Oros</name>");
			ubuf.append("		<groups>");
			ubuf.append("			<group>");
			ubuf.append("				<gid>ADMIN</gid>");
			ubuf.append("				<role>ADMIN</role>");
			ubuf.append("			</group>");
			ubuf.append("		</groups>");
	        ubuf.append("	</user>");
	        ubuf.append("	<user>");
			ubuf.append("		<uid>tch</uid>");
			ubuf.append("		<name>Touchy</name>");
			ubuf.append("		<groups>");
			ubuf.append("			<group>");
			ubuf.append("				<gid>OPERATOR</gid>");
			ubuf.append("				<role>OPERATOR</role>");
			ubuf.append("			</group>");
			ubuf.append("		</groups>");
	        ubuf.append("	</user>");
			ubuf.append("</users>");
	
			UserGroupBean fsBean = new UserGroupBean();
			fsBean.setGroups(gbuf.toString());
			fsBean.setUsers(ubuf.toString());
			GlobalBean fsGlobalBean = new GlobalBean();
			fsGlobalBean.setFsProg("tso001");
			fsGlobalBean.setFsVar("fsUser","tso");
	
			fsGlobalBean.setFsAction(GlobalBean.UPDATE_MODE);
			
			TheTransportor.transport(fsGlobalBean,fsBean);
			
			int rows = fsBean.effectedTransactions();
			Console.out.println("effected "+rows+" rows.");
			Console.out.println("this : "+fsBean);
			java.util.Enumeration fsElements = fsBean.elements();
			if(fsElements!=null) {
				while(fsElements.hasMoreElements()) {
					Console.out.println(fsElements.nextElement());
				}
			}		
		} catch(Exception ex) {
			Console.out.print(ex);
		}
		System.exit(0);
	}
	public void setGroups(String newGroups) {
		setMember("groups",newGroups);
	}
	public void setUsers(String newUsers) {
		setMember("users",newUsers);
	}
}
