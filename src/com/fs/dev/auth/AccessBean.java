package com.fs.dev.auth;

/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile$
 * Description: Bean generate class
 * Version: $Revision$
 * Programmer: $Author$
 * Creation date: Tue Aug 20 12:00:21 ICT 2013
 */

import com.fs.bean.gener.*;
import com.fs.bean.util.*;
//#everybody in love
//#(5000) programmer code begin;
import com.fs.bean.ctrl.*;
//#(5000) programmer code end;
@SuppressWarnings("serial")
public class AccessBean extends BeanSchema {
	//#member & declaration remember me this way
	//#(10000) programmer code begin;
	//#(10000) programmer code end;
	public AccessBean() {
		super();
	}
	protected void initialize() {
		super.initialize();
		addSchema("accessor",java.sql.Types.VARCHAR,"Accessor");
		addSchema("accessorname",java.sql.Types.VARCHAR,"Accessor Name");
		//#initialize & assigned always somewhere
		//#(20000) programmer code begin;
		//#(20000) programmer code end;
	}
	public String fetchVersion() {
		return super.fetchVersion()+AccessBean.class+"=$Revision$\n";
	}
	public String getAccessor() {
		return getMember("accessor");
	}
	public void setAccessor(String newAccessor) {
		setMember("accessor",newAccessor);
	}
	public String getAccessorname() {
		return getMember("accessorname");
	}
	public void setAccessorname(String newAccessorname) {
		setMember("accessorname",newAccessorname);
	}
	//#methods defined everything will flow
	//#(30000) programmer code begin;
	public void startUpdate(final GlobalBean fsGlobalBean) {
		new Thread() {
			public void run() {
				try { update(fsGlobalBean); }catch(Exception ex) { }
				this.interrupt();
			}
		}.start();		
	}
	public int update(GlobalBean fsGlobalBean) throws Exception {
		fsGlobalBean.setFsAction(GlobalBean.UPDATE_MODE);
		TheTransportor.transport(fsGlobalBean,this);		
		return effectedTransactions();
	}
	//#(30000) programmer code end;
	/*
	$Revision$
	*/
}
