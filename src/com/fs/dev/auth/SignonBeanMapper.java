package com.fs.dev.auth;

/*
 * SCCS id: $Id: SignonBeanMapper.java,v 1.1 2008-02-08 14:57:45+07 tassun_o Exp $
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile: SignonBeanMapper.java,v $
 * Description: LogonBeanMapper class implements for handle mapping class
 * Version: $Revision: 1.1 $
 * Programmer: $Author: tassun_o $
 * Creation date: Fri Sep 30 13:43:10 GMT+07:00 2005
 */
/**
 * LogonBeanMapper class implements for handle mapping class.
 */
import com.fs.bean.gener.*;

//#import anything the right time
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings("serial")
public class SignonBeanMapper extends BeanMap {
	//#another methods defined irresistible
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	public SignonBeanMapper() {
		super();
	}
	public String fetchVersion() {
		return super.fetchVersion()+SignonBeanMapper.class+"=$Revision: FS-20170623-103000 $\n";
	}
	protected void initialize() {
		super.initialize();
		mapClass("com.fs.dev.auth.SignonData");
		//#other initial give me a reason
		//#(20000) programmer code begin;
		//#(20000) programmer code end;
	}
}
