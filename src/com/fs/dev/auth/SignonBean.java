package com.fs.dev.auth;

import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.Arguments;
import com.fs.dev.Console;

@SuppressWarnings("serial")
public class SignonBean extends BeanSchema {
	//#methods defined everything will flow
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	//#member & declaration remember me this way
	//#(10000) programmer code begin;
	//#(10000) programmer code end;
	public SignonBean() {
		super();
	}
	public static void main(String[] args) {
		//java com/fs/dev/strok/SignonBean -ms STROK -user tso
		try {
			if(args.length>0) {
				GlobalBean fsGlobal = new GlobalBean();
				fsGlobal.setFsSection(null); //point to default db section in global_config.xml
				fsGlobal.setFsProg("logon");
				fsGlobal.setFsVar("fsUser","tso");
				fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
				SignonBean fsBean = new SignonBean();
				fsGlobal.setFsSection(Arguments.getString(args, fsGlobal.getFsSection(), "-ms"));
				fsBean.setUser(Arguments.getString(args, fsBean.getUser(), "-user"));
				fsBean.setPassword(Arguments.getString(args, fsBean.getPassword(), "-pwd"));
				long st1 = System.currentTimeMillis();	
				fsBean.transport(fsGlobal);
				long st2 = System.currentTimeMillis();	
				int rows = fsBean.effectedTransactions();
				Console.out.println("effected "+rows+" rows. response time "+(st2-st1)+" msecs.");			
				Console.out.println(fsBean);				
			} else {
				Console.out.println("USAGE : "+SignonBean.class);
				Console.out.println("\t-ms main db section");
				Console.out.println("\t-user user id");
				Console.out.println("\t-pwd user password");
			}
		} catch(Exception ex) {
			Console.out.print(ex);
		}
		System.exit(0);
	}
	protected void initialize() {
		super.initialize();
		addSchema("user",java.sql.Types.VARCHAR,"User");
		addSchema("branch",java.sql.Types.VARCHAR,"Branch");
		addSchema("password",java.sql.Types.VARCHAR,"Password");
		addSchema("logondate",java.sql.Types.DATE,"Date");
		//#initialize & assigned always somewhere
		//#(20000) programmer code begin;
		addSchema("email",java.sql.Types.VARCHAR,"Email");
		addSchema("name",java.sql.Types.VARCHAR,"Name");
		addSchema("groups",java.sql.Types.VARCHAR,"Groups");
		addSchema("authlevel",java.sql.Types.VARCHAR);
		addSchema("adminflag",java.sql.Types.VARCHAR);
		addSchema("activeflag",java.sql.Types.VARCHAR);
		addSchema("lockflag",java.sql.Types.VARCHAR);
		addSchema("products",java.sql.Types.VARCHAR,"Products");
		addSchema("projects",java.sql.Types.VARCHAR,"Projects");
		addSchema("deptcode",java.sql.Types.VARCHAR,"Department");
		addSchema("divcode",java.sql.Types.VARCHAR,"Division");
		addSchema("photoimage",java.sql.Types.VARCHAR,"Image");
		addSchema("rolename",java.sql.Types.VARCHAR,"Role");
		addSchema("passwordexpiredate",java.sql.Types.DATE,"ExpireDate");
		addSchema("supervisor",java.sql.Types.VARCHAR,"Supervisor");
		addSchema("status",java.sql.Types.VARCHAR,"Status");
		addSchema("loginfailtimes",java.sql.Types.VARCHAR,"FailTimes");
		addSchema("theme",java.sql.Types.VARCHAR,"Theme");
		addSchema("messages",java.sql.Types.VARCHAR,"Messages");
		addSchema("roles",java.sql.Types.VARCHAR,"Roles");
		addSchema("systemdate",java.sql.Types.DATE,"System Date");
		addSchema("usertype",java.sql.Types.VARCHAR,"User Type");
		addSchema("site",java.sql.Types.VARCHAR,"Site");
		addSchema("iconfile",java.sql.Types.VARCHAR,"Icon");
		addSchema("accessdate",java.sql.Types.DATE,"Access Date");
		addSchema("accesstime",java.sql.Types.TIME,"Access Time");
		addSchema("accesshits",java.sql.Types.TIME,"Access Hits");
		addSchema("failtime",java.sql.Types.BIGINT,"Fail Time");
		addSchema("mobile",java.sql.Types.VARCHAR,"Mobile");
		addSchema("cardid",java.sql.Types.VARCHAR,"Card ID");
		addSchema("username",java.sql.Types.VARCHAR,"User Name");
		addSchema("background",java.sql.Types.VARCHAR,"Background");
		addSchema("birthday",java.sql.Types.DATE,"Birthday");
		addSchema("nationcode",java.sql.Types.VARCHAR,"Nation Code");
		addSchema("titlecode",java.sql.Types.VARCHAR,"Title Code");
		addSchema("employeeid",java.sql.Types.VARCHAR,"Employee ID");
		addSchema("positionid",java.sql.Types.VARCHAR,"Position ID");
		addSchema("positionlevel",java.sql.Types.INTEGER,"Position Level");
		addSchema("siteflag",java.sql.Types.VARCHAR,"siteflag");
		addSchema("branchflag",java.sql.Types.VARCHAR,"branchflag");
		addSchema("branches",java.sql.Types.VARCHAR,"branches");
		addSchema("sites",java.sql.Types.VARCHAR,"sites");
		addSchema("headsite",java.sql.Types.VARCHAR,"headSite");
		addSchema("headbranch",java.sql.Types.VARCHAR,"headbranch");
		addSchema("ename",java.sql.Types.VARCHAR,"ename");
		addSchema("esurname",java.sql.Types.VARCHAR,"esurname");
		addSchema("tname",java.sql.Types.VARCHAR,"tname");
		addSchema("tsurname",java.sql.Types.VARCHAR,"tsurname");
		addSchema("compename",java.sql.Types.VARCHAR,"compename");
		addSchema("comptname",java.sql.Types.VARCHAR,"comptname");
		addSchema("branchename",java.sql.Types.VARCHAR,"branchename");
		addSchema("branchtname",java.sql.Types.VARCHAR,"branchtname");
		addSchema("language",java.sql.Types.VARCHAR,"language");
		addSchema("effectdate",java.sql.Types.DATE,"effectdate");
		addSchema("changeflag",java.sql.Types.VARCHAR,"changeflag");
		addSchema("gpsflag",java.sql.Types.VARCHAR,"gpsflag");
		addSchema("partysite",java.sql.Types.VARCHAR,"partysite");
		//#(20000) programmer code end;
	}
	public void assign(AccessBean bean) {
		if(bean==null) return;
		bean.setAccessor(getUser());
		bean.setAccessorname(getName());
	}
	public void assign(AccessorBean bean) {
		if(bean==null) return;
		bean.setFsBranch(getBranch());
		bean.setFsDate(getLogondate());
		bean.setFsUser(getUser());
		bean.setFsSystemDate(getSystemdate());	
		bean.setFsSite(getSite());
		bean.setFsVar("fsHeadSite",getHeadsite());
		bean.setFsVar("fsHeadBranch", getHeadbranch());	
		bean.setFsVar("fsUserName",getName());
		bean.setFsVar("fsUserNaming", getUsername());
		bean.setFsVar("fsPassword",getPassword());
		bean.setFsVar("fsUserGroup",getGroups());
		bean.setFsVar("fsUserProduct",getProducts());
		bean.setFsVar("fsUserProject",getProjects());
		bean.setFsVar("fsUserStatus",getStatus());
		bean.setFsVar("fsEmail",getEmail());
		bean.setFsVar("fsDepartment",getDeptcode());
		bean.setFsVar("fsDivision",getDivcode());
		bean.setFsVar("fsImage",getPhotoimage());
		bean.setFsVar("fsSupervisor",getSupervisor());
		bean.setFsVar("fsRoles",getRolename());
		bean.setFsVar("fsStyles",getTheme());
		bean.setFsVar("fsUsertype",getUsertype());
		bean.setFsVar("fsIconfile",getIconfile());
		bean.setFsVar("fsAccessdate",getAccessdate());
		bean.setFsVar("fsAccesstime",getAccesstime());
		bean.setFsVar("fsAccesshits",getAccesshits());
		bean.setFsVar("fsMobile",getMobile());
		bean.setFsVar("fsCardid",getCardid());
		bean.setFsVar("fsBackground",getBackground());
		bean.setFsVar("fsBirthday",getBirthday());
		bean.setFsVar("fsNationcode",getNationcode());
		bean.setFsVar("fsTitlecode",getTitlecode());
		bean.setFsVar("fsEmployeeid",getEmployeeid());
		bean.setFsVar("fsPositionid",getPositionid());
		bean.setFsVar("fsPositionlevel",getPositionlevel());
		bean.setFsVar("fsSites", getSites());
		bean.setFsVar("fsBranches", getBranches());
		bean.setFsVar("fsSiteflag", getSiteflag());
		bean.setFsVar("fsBranchflag", getBranchflag());
		bean.setFsVar("fsUserename", getEname());
		bean.setFsVar("fsUseresurname", getEsurname());
		bean.setFsVar("fsUsertname", getTname());
		bean.setFsVar("fsUsertsurname", getTsurname());
		bean.setFsVar("fsCompename", getCompename());
		bean.setFsVar("fsComptname", getComptname());
		bean.setFsVar("fsBranchename", getBranchename());
		bean.setFsVar("fsBranchtname", getBranchtname());
		bean.setFsVar("fsEffectdate", getEffectdate());
		bean.setFsVar("fsGpsflag", getGpsflag());
		bean.setFsVar("fsParty", getPartysite());
		bean.setFsVar("fsAdminflag", getAdminflag());
		bean.setFsVar("fsPasswordexpire", getPasswordexpiredate());
	}
	public String fetchVersion() {
		return super.fetchVersion()+SignonBean.class+"=$Revision: FS-20170623-095500 $\n";
	}
	public String getAccessdate() {
		return getMember("accessdate");
	}
	public String getAccesshits() {
		return getMember("accesshits");
	}
	public String getAccesstime() {
		return getMember("accesstime");
	}
	public String getActiveflag() {
		return getMember("activeflag");
	}
	public String getAdminflag() {
		return getMember("adminflag");
	}
	public String getBackground() {
		return getMember("background");
	}	
	public String getBirthday() {
		return getMember("birthday");
	}	
	public String getBranch() {
		return getMember("branch");
	}
	public String getBranchename() {
		return getMember("branchename");
	}
	public String getBranches() {
		return getMember("branches");
	}
	public String getBranchflag() {
		return getMember("branchflag");
	}
	public String getBranchtname() {
		return getMember("branchtname");
	}
	public String getCardid() {
		return getMember("cardid");
	}
	public String getChangeflag() {
		return getMember("changeflag");
	}
	public String getCompename() {
		return getMember("compename");
	}
	public String getComptname() {
		return getMember("comptname");
	}
	public String getDeptcode() {
		return getMember("deptcode");
	}
	public String getDivcode() {
		return getMember("divcode");
	}
	public String getEffectdate() {
		return getMember("effectdate");
	}
	public String getEmail() {
		return getMember("email");
	}
	public String getEmployeeid() {
		return getMember("employeeid");
	}
	public String getEname() {
		return getMember("ename");
	}
	public String getEsurname() {
		return getMember("esurname");
	}
	public String getFailtime() {
		return getMember("failtime");
	}
	public String getGpsflag() {
		return getMember("gpsflag");
	}
	public String getGroups() {
		return getMember("groups");
	}
	public String getHeadbranch() {
		return getMember("headbranch");
	}
	public String getHeadsite() {
		return getMember("headsite");
	}
	public String getIconfile() {
		return getMember("iconfile");
	}
	public String getLanguage() {
		return getMember("language");
	}
	public String getLockflag() {
		return getMember("lockflag");
	}
	public String getLoginfailtimes() {
		return getMember("loginfailtimes");
	}
	public String getLogondate() {
		return getMember("logondate");
	}
	public String getMessages() {
		return getMember("messages");
	}
	public String getMobile() {
		return getMember("mobile");
	}
	public String getName() {
		return getMember("name");
	}
	public String getNationcode() {
		return getMember("nationcode");
	}
	public String getPartysite() {
		return getMember("partysite");
	}
	public String getPassword() {
		return getMember("password");
	}
	public String getPasswordexpiredate() {
		return getMember("passwordexpiredate");
	}
	public String getPhotoimage() {
		return getMember("photoimage");
	}
	public String getPositionid() {
		return getMember("positionid");
	}
	public String getPositionlevel() {
		return getMember("positionlevel");
	}
	public String getProducts() {
		return getMember("products");
	}
	public String getProjects() {
		return getMember("projects");
	}
	public String getRolename() {
		return getMember("rolename");
	}
	public String getRoles() {
		return getMember("roles");
	}
	public String getSite() {
		return getMember("site");
	}
	public String getSiteflag() {
		return getMember("siteflag");
	}
	public String getSites() {
		return getMember("sites");
	}
	public String getStatus() {
		return getMember("status");
	}
	public String getSupervisor() {
		return getMember("supervisor");
	}
	public String getSystemdate() {
		return getMember("systemdate");
	}
	public String getTheme() {
		return getMember("theme");
	}
	public String getTitlecode() {
		return getMember("titlecode");
	}
	public String getTname() {
		return getMember("tname");
	}
	public String getTsurname() {
		return getMember("tsurname");
	}
	public String getUser() {
		return getMember("user");
	}
	public String getUsername() {
		return getMember("username");
	}
	public String getUsertype() {
		return getMember("usertype");
	}
	public void setAccessdate(String newAccessdate) {
		setMember("accessdate",newAccessdate);
	}
	public void setAccesshits(String newAccesshits) {
		setMember("accesshits",newAccesshits);
	}
	public void setAccesstime(String newAccesstime) {
		setMember("accesstime",newAccesstime);
	}
	public void setActiveflag(String newActiveflag) {
		setMember("activeflag",newActiveflag);
	}
	public void setAdminflag(String newAdminflag) {
		setMember("adminflag",newAdminflag);
	}
	public void setBackground(String newBackground) {
		setMember("background", newBackground);
	}
	public void setBirthday(String newBirthday) {
		setMember("birthday",newBirthday);
	}
	public void setBranch(String newBranch) {
		setMember("branch",newBranch);
	}
	public void setBranchename(String newBranchename) {
		setMember("branchename",newBranchename);
	}
	public void setBranches(String newBranches) {
		setMember("branches",newBranches);
	}
	public void setBranchflag(String newBranchflag) {
		setMember("branchflag",newBranchflag);
	}
	public void setBranchtname(String newBranchtname) {
		setMember("branchtname",newBranchtname);
	}
	public void setCardid(String newCardid) {
		setMember("cardid", newCardid);
	}
	public void setChangeflag(String newChangeflag) {
		setMember("changeflag",newChangeflag);
	}
	public void setCompename(String newCompename) {
		setMember("compename",newCompename);
	}
	public void setComptname(String newComptname) {
		setMember("comptname",newComptname);
	}
	public void setDeptcode(String newDeptcode) {
		setMember("deptcode",newDeptcode);
	}
	public void setDivcode(String newDivcode) {
		setMember("divcode",newDivcode);
	}
	public void setEffectdate(String newEffectdate) {
		setMember("effectdate",newEffectdate);
	}	
	public void setEmail(String newEmail) {
		setMember("email",newEmail);
	}
	public void setEmployeeid(String newEmployeeid) {
		setMember("employeeid",newEmployeeid);
	}
	public void setEname(String newEname) {
		setMember("ename",newEname);
	}
	public void setEsurname(String newEsurname) {
		setMember("esurname",newEsurname);
	}
	public void setFailtime(String newFailtime) {
		setMember("failtime",newFailtime);
	}
	public void setGpsflag(String newGpsflag) {
		setMember("gpsflag",newGpsflag);
	}
	public void setGroups(String newGroups) {
		setMember("groups",newGroups);
	}
	public void setHeadbranch(String newHeadBranch) {
		setMember("headbranch",newHeadBranch);
	}
	public void setHeadsite(String newHeadSite) {
		setMember("headsite",newHeadSite);
	}
	public void setIconfile(String newIconfile) {
		setMember("iconfile",newIconfile);
	}
	public void setLanguage(String newLanguage) {
		setMember("language",newLanguage);
	}
	public void setLockflag(String newLockflag) {
		setMember("lockflag",newLockflag);
	}
	public void setLoginfailtimes(String newLoginfailtimes) {
		setMember("loginfailtimes",newLoginfailtimes);
	}
	public void setLogondate(String newLogondate) {
		setMember("logondate",newLogondate);
	}
	public void setMessages(String newMessages) {
		setMember("messages",newMessages);
	}
	public void setMobile(String newMobile) {
		setMember("mobile",newMobile);
	}
	public void setName(String newName) {
		setMember("name",newName);
	}
	public void setNationcode(String newNationcode) {
		setMember("nationcode",newNationcode);
	}
	public void setNewwindowflag(String newWindowflag) {
		setMember("newwindowflag",newWindowflag);
	}
	public void setPartysite(String newPartysite) {
		setMember("partysite",newPartysite);
	}
	public void setPassword(String newPassword) {
		setMember("password",newPassword);
	}
	public void setPasswordexpiredate(String newPasswordexpiredate) {
		setMember("passwordexpiredate",newPasswordexpiredate);
	}	
	public void setPhotoimage(String newPhotoimage) {
		setMember("photoimage",newPhotoimage);
	}
	public void setPositionid(String newPositionid) {
		setMember("positionid",newPositionid);
	}
	public void setPositionlevel(String newPositionlevel) {
		setMember("positionlevel",newPositionlevel);
	}
	public void setProducts(String newProducts) {
		setMember("products",newProducts);
	}
	public void setProjects(String newProjects) {
		setMember("projects",newProjects);
	}
	public void setRolename(String newRolename) {
		setMember("rolename",newRolename);
	}
	public void setRoles(String newRoles) {
		setMember("roles",newRoles);
	}	
	public void setSite(String newSite) {
		setMember("site",newSite);
	}	
	public void setSiteflag(String newSiteflag) {
		setMember("siteflag",newSiteflag);
	}
	public void setSites(String newSites) {
		setMember("sites",newSites);
	}
	public void setStatus(String newStatus) {
		setMember("status",newStatus);
	}
	public void setSupervisor(String newSupervisor) {
		setMember("supervisor",newSupervisor);
	}
	public void setSystemdate(String newSystemdate) {
		setMember("systemdate",newSystemdate);
	}
	public void setTheme(String newTheme) {
		setMember("theme",newTheme);
	}	
	public void setTitlecode(String newTitlecode) {
		setMember("titlecode",newTitlecode);
	}
	public void setTname(String newTname) {
		setMember("tname",newTname);
	}
	public void setTsurname(String newTsurname) {
		setMember("tsurname",newTsurname);
	}
	public void setUser(String newUser) {
		setMember("user",newUser);
	}
	public void setUsername(String newUsername) {
		setMember("username",newUsername);
	}	
	public void setUsertype(String newUsertype) {
		setMember("usertype",newUsertype);
	}
}
