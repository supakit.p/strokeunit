package com.fs.dev.auth;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fs.bean.util.BeanUtility;

@SuppressWarnings({"serial"})
public class UserEntity implements java.io.Serializable {
	private String userid = null;
	private String username = null;
	private java.util.HashMap<String,String> groups = null;
	
	public UserEntity() {
		super();
	}
	public UserEntity(Element root, GroupEntity group) {
		NodeList children = root.getChildNodes();
		for (int i = 0,isz=children.getLength(); i<isz; i++) {
    		Node child = children.item(i);
    		if(child.getNodeType()==Element.ELEMENT_NODE) {
    			if(child.getNodeName().equals("uid") && child.getFirstChild()!=null) {
	        		userid = child.getFirstChild().getNodeValue();	        			
    			} else if(child.getNodeName().equals("role") && child.getFirstChild()!=null) {
        			if(group!=null) {
        				group.addUser(this,child.getFirstChild().getNodeValue());	        			
        			}
    			} else if(child.getNodeName().equals("name") && child.getFirstChild()!=null) {
        			username = child.getFirstChild().getNodeValue();	        			
    			} else if(child.getNodeName().equals("groups")) {
        			Element element = (Element) child;
        			initGroup(element,this);
    			}        	
    		}
		}
		String attr = root.getAttribute("role");
		if(attr!=null && !attr.trim().equals("")) {
			if(group!=null) {
				group.addUser(this,attr);
			}
		}
	}
	protected void initGroup(Element root,UserEntity user) {
		NodeList children = root.getChildNodes();
		for (int i = 0,isz=children.getLength();i<isz;i++) {
    		Node child = children.item(i);
    		if(child.getNodeType()==Element.ELEMENT_NODE) {
    			if(child.getNodeName().equals("group")) {
        			String gid = null;
        			String role = null;
        			NodeList gchild = child.getChildNodes();
        			for(int j=0,jsz=gchild.getLength();j<jsz;j++) {
	        			Node gnode = gchild.item(j);
	        			if(gnode.getNodeType()==Element.ELEMENT_NODE) {
		        			if(gnode.getNodeName().equals("gid") && gnode.getFirstChild()!=null) {
				        		gid = gnode.getFirstChild().getNodeValue();				        			
		        			} else if(gnode.getNodeName().equals("role") && gnode.getFirstChild()!=null) {
				        		role = gnode.getFirstChild().getNodeValue();				        			
		        			}
	        			}
        			}
        			if(gid!=null) {
	        			user.addGroup(gid,role);
        			}
    			}
    		}
		}
	}
	public void addGroup(GroupEntity group,String role) {
		if(group==null) { return; }
		addGroup(group.getGroupname(),role);
		group.addUser(this,role);
	}
	public void addGroup(String groupname,String role) {
		if(groupname==null) { return; }
		if(groups==null) {
			groups = new java.util.HashMap<>();
		}
		groups.remove(groupname);
		groups.put(groupname,role==null?groupname:role);
	}
	public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
		userid = rs.getString("userid");
		String name = rs.getString("userename");
		String surname = rs.getString("useresurname");
		username = (name==null?"":name)+" "+(surname==null?"":surname);
	}
	public java.util.Map<String, String> getGroups() {
		if(groups==null) {
			groups = new java.util.HashMap<>();
		}
		return groups;
	}	
	public String getRole(String groupname) {
		if(groupname==null) { return null; }
		if(groups==null) { return null; }
		return groups.get(groupname);
	}			
	public String getUserid() {
		return userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String toString() {
		return super.toString()+"{uid="+userid+",name="+username+"}";
	}
	public String toXML() {
		return toXML(0);
	}
	public String toXML(int level) {
		StringBuilder buf = new StringBuilder();
		for(int i=0;i<level;i++) buf.append("\t");
		buf.append("<user>\n");
		for(int i=0;i<(level+1);i++) buf.append("\t");
		buf.append("<uid>"+userid+"</uid>\n");
		for(int i=0;i<(level+1);i++) buf.append("\t");
		buf.append("<name>"+BeanUtility.preserveXML(username==null?"":username)+"</name>\n");			
		if(groups!=null && !groups.isEmpty()) {
			for(int i=0;i<(level+1);i++) buf.append("\t");
			buf.append("<groups>\n");				
			for(java.util.Iterator<String> it = groups.keySet().iterator();it.hasNext();) {
				String gid = it.next();
				if(gid!=null) {
					String role = groups.get(gid);
					for(int i=0;i<(level+2);i++) buf.append("\t");
					buf.append("<group>\n");
					for(int i=0;i<(level+3);i++) buf.append("\t");
					buf.append("<gid>"+gid+"</gid>\n");
					for(int i=0;i<(level+3);i++) buf.append("\t");
					buf.append("<role>"+(role==null?gid:role)+"</role>\n");
					for(int i=0;i<(level+2);i++) buf.append("\t");
					buf.append("</group>\n");
				}
			}
			for(int i=0;i<(level+1);i++) buf.append("\t");
			buf.append("</groups>\n");
		}
		for(int i=0;i<level;i++) buf.append("\t");				
		buf.append("</user>\n");			
		return buf.toString();
	}
}
