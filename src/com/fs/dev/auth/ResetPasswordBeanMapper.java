package com.fs.dev.auth;

/*
 * SCCS id: $Id: ResetPasswordBeanMapper.java,v 1.0 2010-02-15 21:57:23+07 parichat_kas Exp $
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile: ResetPasswordBeanMapper.java,v $
 * Description: 
 * Version: $Revision: 1.0 $
 * Programmer: $Author: parichat_kas $
 * Creation date: (24/10/2009 22:31:16)
 * Update log: $Log: ResetPasswordBeanMapper.java,v $
 * Update log: Revision 1.0  2010-02-15 21:57:23+07  parichat_kas
 * Update log: Initial revision
 * Update log: *
 */
/**
 * ResetPasswordBeanMapper class implements for handle mapping class.
 */
import com.fs.bean.gener.*;

//#import anything the right time
//#(10000) programmer code begin;
//#(10000) programmer code end; 
@SuppressWarnings("serial")
public class ResetPasswordBeanMapper extends BeanMap {
//#another methods defined irresistible
//#(30000) programmer code begin;
//#(30000) programmer code end;
//#another methods defined irresistible
//#(30000) programmer code begin;
//#(30000) programmer code end;
public ResetPasswordBeanMapper() {
	super();
}
public String fetchVersion() {
	return super.fetchVersion()+ResetPasswordBeanMapper.class+"=$Revision: 1.0 $\n";
}
protected void initialize() {
	super.initialize();
	mapClass("com.fs.dev.auth.ResetPasswordData");
	//#other initial give me a reason
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
}
}
