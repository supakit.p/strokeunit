package com.fs.dev.auth;

import com.fs.interfaces.VersionDefinitionInterface;
import com.fs.dev.Console;
public class Version implements VersionDefinitionInterface {
	public Version() {
		super();
	}
	public static void main(String[] args) {
		Version v = new Version();
		Console.out.println(v.fetchDefinition());
		Console.out.println(v.fetchVersion());
	}	
	public String fetchDefinition() {
		StringBuilder buffer = new StringBuilder();
		buffer.append(Version.class+"\n");
		buffer.append("FS-20170521-142900 : Initial version definition.\n");
		buffer.append("FS-20190211-101500 : SignonBean & SignonData provide more user information.\n");
		return buffer.toString();
	}
	public String fetchVersion() {
		return Version.class+"=$Revision: FS-20190211-101500 $\n";
	}
}
