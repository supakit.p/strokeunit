package com.fs.dev.auth;

/*
 * SCCS id: $Id: ResetPasswordBean.java,v 1.0 2010-02-15 21:57:24+07 parichat_kas Exp $
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile: ResetPasswordBean.java,v $
 * Description: 
 * Version: $Revision: 1.0 $
 * Programmer: $Author: parichat_kas $
 * Creation date: (24/10/2009 22:35:42)
 * Update log: $Log: ResetPasswordBean.java,v $
 * Update log: Revision 1.0  2010-02-15 21:57:24+07  parichat_kas
 * Update log: Initial revision
 * Update log: *
 */
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.Arguments;
import com.fs.dev.Console;
import com.fs.bean.ctrl.*;

@SuppressWarnings("serial")
public class ResetPasswordBean extends BeanSchema {
/**
 * ResetPasswordBean constructor comment.
 */
public ResetPasswordBean() {
	super();
}
public static void main(String[] args) {
	//java com/fs/bean/ResetPasswordBean -s MYREFDB -e tassun_oro@hotmail.com
	try {
		if(args.length>0) {
			ResetPasswordBean bean = new ResetPasswordBean();
			bean.setEmail(Arguments.getString(args, bean.getEmail(), "-e", "-email"));
			GlobalBean fsGlobal = new GlobalBean();
			fsGlobal.setFsProg(Arguments.getString(args, "resetpassword", "-p"));
			fsGlobal.setFsSection(Arguments.getString(args, fsGlobal.getFsSection(), "-s", "-ms"));
			fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
			BeanTransport fsTransport = TheTransportor.transport(fsGlobal,bean);
			if(fsTransport!=null) fsGlobal.setSuccess(fsTransport.isSuccess());
			Console.out.println("effected transaction : "+bean.effectedTransactions());
			Console.out.println(bean);
		} else {
			Console.out.println("USAGE : "+ResetPasswordBean.class);
			Console.out.println("\t-p	program id.");
			Console.out.println("\t-e	email address");
			Console.out.println("\t-s	data base section");
		}
	} catch(Exception ex) {
		Console.out.print(ex);
	}
	System.exit(0);
}
protected void initialize() {
	super.initialize();
	addSchema("userid",java.sql.Types.VARCHAR,"User");
	addSchema("email", java.sql.Types.VARCHAR,"email");
	addSchema("password",java.sql.Types.VARCHAR,"New Password");
	addSchema("securecode", java.sql.Types.VARCHAR, "Secure Code");
	addSchema("username",java.sql.Types.VARCHAR,"User Name");
	//#initialize & assigned always somewhere
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+ResetPasswordBean.class+"=$Revision: FS-20180528-175500 $\n";
}
public String getEmail() {
	return getMember("email");
}
public String getPassword() {
	return getMember("password");
}
public String getSecurecode() {
	return getMember("securecode");
}
public String getUserid() {
	return getMember("userid");
}
public String getUsername() {
	return getMember("username");
}
public void setEmail(String newEmail) {
	setMember("email",newEmail);
}
public void setPassword(String newPassword) {
	setMember("password",newPassword);
}
public void setSecurecode(String newSecurecode) {
	setMember("securecode",newSecurecode);
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
}
