package com.fs.dev.auth;

/*
 * SCCS id: $Id: LockBeanMapper.java,v 1.1 2008-02-08 14:57:50+07 tassun_o Exp $
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile: LockBeanMapper.java,v $
 * Description: LockBeanMapper class implements for handle mapping class
 * Version: $Revision: 1.1 $
 * Programmer: $Author: tassun_o $
 * Creation date: Fri Sep 30 13:43:10 GMT+07:00 2005
 */
/**
 * LockBeanMapper class implements for handle mapping class.
 */
import com.fs.bean.gener.*;

//#import anything the right time
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings("serial")
public class LockBeanMapper extends BeanMap {
	//#another methods defined irresistible
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	public LockBeanMapper() {
		super();
	}
	public String fetchVersion() {
		return super.fetchVersion()+LockBeanMapper.class+"=$Revision: 1.1 $\n";
	}
	protected void initialize() {
		super.initialize();
		mapClass("com.fs.dev.auth.LockData");
		//#other initial give me a reason
		//#(20000) programmer code begin;
		//#(20000) programmer code end;
	}
}
