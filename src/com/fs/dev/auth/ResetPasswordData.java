package com.fs.dev.auth;

/*
 * SCCS id: $Id: ResetPasswordData.java,v 1.1 2010-09-23 12:08:57+07 tassun_o Exp $
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile: ResetPasswordData.java,v $
 * Description: ResetPasswordData class implements for handle tus data base schema.
 * Version: $Revision: 1.1 $
 * Programmer: $Author: tassun_o $
 * Creation date: (24/10/2009 22:31:16)
 * Update log: $Log: ResetPasswordData.java,v $
 * Update log: Revision 1.1  2010-09-23 12:08:57+07  tassun_o
 * Update log: support relatable interface to  take owner logger
 * Update log: *
 */
/**
 * ResetPasswordData class implements for handle tus data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.Arguments;
import com.fs.dev.Console;
import com.fs.bean.misc.*;

//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings("serial")
public class ResetPasswordData extends BeanData {
//#defined & declaration the real thing
//#(20000) programmer code begin;
public static final java.util.Random random = new java.util.Random(System.currentTimeMillis());
//#(100000) programmer code end;
public ResetPasswordData() {
	super();
}
public static String createPassword() {
	String time = Long.toHexString(System.currentTimeMillis()).toUpperCase();
	time = time.substring(time.length()-4);
	long l = random.nextLong();
	if(l<0) l = Math.abs(l);
	String code = Long.toHexString(l).toUpperCase();
	code = code.substring(0,4);
	return time+code;
}
public static void main(String[] args) {
	try {
		if(args.length>0) {
			String section = Arguments.getString(args, null, "-s", "-ms");
			ResetPasswordData rp = new ResetPasswordData();
			rp.setUserid(Arguments.getString(args, rp.getUserid(), "-u"));
			rp.setUserpassword(Arguments.getString(args, rp.getUserpassword(), "-p"));
			rp.setEmail(Arguments.getString(args, rp.getEmail(), "-e", "-email"));
			rp.update(DBConnection.getConnection(section));			
		} else {
			Console.out.println("USAGE : "+ResetPasswordData.class);
			Console.out.println("\t-u	user id");
			Console.out.println("\t-p	password");
			Console.out.println("\t-e	email address");
			Console.out.println("\t-s	data base section");
		}
	} catch(Exception ex) {
		Console.out.print(ex);
	}	
}
protected void initialize() {
	super.initialize();
	setTable("tuser");
	addSchema("userid",java.sql.Types.VARCHAR);
	addSchema("userpassword",java.sql.Types.VARCHAR);
	addSchema("email",java.sql.Types.VARCHAR);
	addSchema("securecode",java.sql.Types.VARCHAR);
	addSchema("username",java.sql.Types.VARCHAR);
	addSchema("userlogin",java.sql.Types.VARCHAR);
	map("userid","userid");
	map("userpassword","password");
	map("email","email");
	map("securecode","securecode");
	map("username","username");
	//#intialize how deep is your love 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
}
public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
	super.fetchResult(rs);
	setUserid(rs.getString("userid"));
	//#fetching other result desire
	//#(60000) programmer code begin;
	String name = rs.getString("usertname");
	String surname = rs.getString("usertsurname");
	setUsername((name==null?"":name)+" "+(surname==null?"":surname));
	//#(60000) programmer code end;
}
public String fetchVersion() {
	return super.fetchVersion()+ResetPasswordData.class+"=$Revision: FS-20180616-113000 $\n";
}
public String getEmail() {
	return getString("email");
}
public String getSecurecode() {
	return getString("securecode");
}
public String getUserid() {
	return getString("userid");
}
public String getUserlogin() {
	return getString("userlogin");
}
public String getUsername() {
	return getString("username");
}
public String getUserpassword() {
	return getString("userpassword");
}
public boolean obtain(BeanSchemaInterface bean) throws Exception {
	if(bean==null) return super.obtain(bean);
	setUserid(bean.getFieldByName(mapper("userid")).asString());
	setUserpassword(bean.getFieldByName(mapper("userpassword")).asString());
	setEmail(bean.getFieldByName(mapper("email")).asString());
	//#(40000) programmer code end;
	return super.obtain(bean);
}
public int retrieve(java.sql.Connection connection) throws Exception {
	boolean found = false;
	String filter = "and";
	KnSQL sql = new KnSQL(this);
	sql.append("select tuser.username,tuserinfo.userid,tuserinfo.email,tuserinfo.usertname,tuserinfo.usertsurname,tuser.username ");
	sql.append("from tuser,tuserinfo ");
	sql.append("where tuser.userid = tuserinfo.userid ");
	if(getUserid()!=null && getUserid().trim().length()>0) {
		sql.append(filter+" tuserinfo.userid = ?userid ");
		sql.setParameter("userid",getUserid());
		filter = "and";
		found = true;
	} 
	if(getEmail()!=null && getEmail().trim().length()>0) {
		sql.append(filter+" tuserinfo.email = ?email ");;
		sql.setParameter("email",getEmail());
		filter = "and";
		found = true;
	}
	if(!found) return 0;
	//#any retrieve statement too close
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	//#assigned parameters make it happen
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
	int result = 0;
	java.sql.ResultSet rs = sql.executeQuery(connection);
	if(rs.next()) {
		result++;
		fetchResult(rs);
		//#any result fetching fool again
		//#(220000) programmer code begin;
		setUserlogin(rs.getString("username"));
		//#(220000) programmer code end;
	}
	//#after fetching result set lovin each day
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	return result;
}
public void setEmail(String newEmail) {
	setMember("email",newEmail);
}
public void setSecurecode(String newSecurecode) {
	setMember("securecode",newSecurecode);
}
public void setUserid(String newUserid) {
	setMember("userid",newUserid);
}
public void setUserlogin(String newUserlogin) {
	setMember("userlogin",newUserlogin);
}
public void setUsername(String newUsername) {
	setMember("username",newUsername);
}
public void setUserpassword(String newUserpassword) {
	setMember("userpassword",newUserpassword);
}
public int update(java.sql.Connection connection) throws Exception {
	//#begin with update statement going on
	//#(290000) programmer code begin;	
	int result = retrieve(connection);
	if(result>0) {
		setUserpassword(createPassword());
		BeanUtility util = new BeanUtility(this);		
		PasswordLibrary plib = new PasswordLibrary(this);
		java.sql.Date logondate = new java.sql.Date(System.currentTimeMillis());
		if(global!=null && global.getFsDate()!=null) {
			logondate = util.parseSQLDate(global.getFsDate());
		} 
		Trace.info(this,"reset password : "+getUserpassword());
		boolean reply = plib.updatePassword(connection,getUserid(),getUserpassword(),logondate,false);
		if(reply) {
			result++;
		}
		setUserid(getUserlogin());
	} else throw new com.fs.bean.BeanException("ID or Email address does not exist",-8888);
	//#(290000) programmer code end;
	return result;
	//#(300000) programmer code end;
}
}
