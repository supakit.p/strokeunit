package com.fs.dev.auth;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressWarnings({"serial","rawtypes"})
public class UserList implements java.io.Serializable {
	private GroupEntity group = null;
	private java.util.TreeMap<String,UserEntity> users = new java.util.TreeMap<>();
	
	public UserList() {
		super();
	}
	public UserList(Element root,GroupEntity group) {
		this.group = group;
		NodeList children = root.getChildNodes();
		for (int i = 0,isz=children.getLength(); i<isz; i++) {
    		Node child = children.item(i);
    		if(child.getNodeType()==Element.ELEMENT_NODE && child.getNodeName().equals("user")) {
				addUser(new UserEntity((Element)child,group));        			
    		}
		}
	}
	public void addUser(UserEntity user) {
		if(user==null) { return; }
		users.put(user.getUserid(),user);
	}
	public GroupEntity getGroup() {
		return group;
	}		
	public UserEntity getUser(String uid) {
		if(uid==null) { return null; }
		return (UserEntity)users.get(uid);
	}
	public java.util.Map<String,UserEntity> getUsers() {
		return users;
	}
	public boolean isEmpty() {
		return users.isEmpty();
	}
	public String toString() {
		return super.toString()+users;
	}
	public String toXML() {
		return toXML(0);
	}
	public String toXML(int level) {
		StringBuilder buf = new StringBuilder();
		for(int i=0;i<level;i++) buf.append("\t");				
		buf.append("<users>\n");
		if(users!=null && !users.isEmpty()) {				
			for(java.util.Iterator it = users.values().iterator();it.hasNext();) {
				UserEntity ue = (UserEntity)it.next();
				if(ue!=null) buf.append(ue.toXML(level+1));
			}
		}
		for(int i=0;i<level;i++) buf.append("\t");				
		buf.append("</users>\n");						
		return buf.toString();
	}
	public String xmlUser() {
		return xmlUser(0);
	}
	public String xmlUser(int level) {
		StringBuilder buf = new StringBuilder();
		for(int i=0;i<level;i++) buf.append("\t");
		buf.append("<user>\n");			
		for(java.util.Iterator it = users.values().iterator();it.hasNext();) {
			UserEntity ue = (UserEntity)it.next();
			if(ue!=null) {
				String role = null;
				if(group!=null) role = ue.getRole(group.getGroupname());
				for(int i=0;i<(level+1);i++) buf.append("\t");
				buf.append("<uid");
				if(role!=null) buf.append(" role=\""+role+"\"");
				buf.append(">"+ue.getUserid()+"</uid>\n");
			}
		}
		for(int i=0;i<level;i++) buf.append("\t");
		buf.append("</user>\n");
		return buf.toString();
	}
}
