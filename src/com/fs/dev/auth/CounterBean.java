package com.fs.dev.auth;

import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.Console;
import com.fs.bean.ctrl.*;

@SuppressWarnings({ "serial", "unchecked" })
public class CounterBean extends BeanSchema implements Runnable {
	private Thread reaper = null;
	private GlobalBean global = null;
	//#methods defined everything will flow
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	//#member & declaration remember me this way
	//#(10000) programmer code begin;
	//#(10000) programmer code end;
	public CounterBean() {
		super();
	}
	public void execute(GlobalBean global) {
		this.global = global;
		reaper = new Thread(this,"CounterBean-Thread");
		reaper.start();
	}
	public String fetchVersion() {
		return super.fetchVersion()+CounterBean.class+"=$Revision: 1.1 $\n";
	}
	public String getCounter() {
		return getMember("counter");
	}
	public String getLock() {
		return getMember("lockflag");
	}
	public String getUser() {
		return getMember("user");
	}
	protected void initialize() {
		super.initialize();
		addSchema("user",java.sql.Types.VARCHAR,"User");
		addSchema("counter",java.sql.Types.INTEGER,"Counter");
		//#initialize & assigned always somewhere
		//#(20000) programmer code begin;
		addSchema("lockflag",java.sql.Types.VARCHAR,"Lock");
		//#(20000) programmer code end;
	}
	public static void main(String[] args) {
		try {
			String user = "tso";
			if(args.length>0) user = args[0];
			CounterBean fsCounterBean = new CounterBean();
			fsCounterBean.setUser(user);
			fsCounterBean.setCounter("1");
			GlobalBean fsHandler = new GlobalBean();
			fsHandler.setFsProg("counter");
			fsHandler.setFsAction(GlobalBean.UPDATE_MODE);
			fsCounterBean.setGlobalBean(fsHandler);
			fsCounterBean.run();
			Console.out.println("main end.");
		} catch(Exception ex) {
			Console.out.print(ex);
		}
		System.exit(0);
	}
	@SuppressWarnings("deprecation")
	public void run() {
		try {
			TheTransportor.transport(global,this);
		} catch(Exception ex) {
			Console.out.print(ex);
		}
		global = null;
		if(reaper!=null) reaper.stop();
		reaper = null;
	}
	public void setCounter(String newCounter) {
		setMember("counter",newCounter);
	}
	public void setGlobalBean(GlobalBean global) {
		this.global = global;
	}
	public void setLock(String newLock) {
		setMember("lockflag",newLock);
	}
	public void setUser(String newUser) {
		setMember("user",newUser);
	}
}
