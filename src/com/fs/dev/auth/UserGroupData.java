package com.fs.dev.auth;

/*
 * SCCS id: $Id: UserGroupData.java,v 1.1 2010-09-23 12:09:08+07 tassun_o Exp $
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile: UserGroupData.java,v $
 * Description: UserGroupData class implements for handle user group
 * Version: $Revision: 1.1 $
 * Programmer: $Author: tassun_o $
 * Creation date: (03/11/2008 16:21:56)
 * Update log: $Log: UserGroupData.java,v $
 * Update log: Revision 1.1  2010-09-23 12:09:08+07  tassun_o
 * Update log: support relatable interface to  take owner logger
 * Update log:
 * Update log: Revision 1.0  2008-12-03 14:04:01+07  apipong_kan
 * Update log: Initial revision
 * Update log: *
 */
/**
 * UserGroupData class implements for handler user group
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.misc.*;
import com.fs.bean.util.*;
import com.fs.dom.*;
import org.w3c.dom.*;

import com.fs.dev.Arguments;
import com.fs.dev.Console;
@SuppressWarnings({"serial"})
public class UserGroupData extends BeanData {
	/**
	 * UserGroupData constructor comment.
	 */
	public UserGroupData() {
		super();
	}
	public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
		super.fetchResult(rs);
		//#fetching other result desire
		//#(60000) programmer code begin;
		//#(60000) programmer code end;
	}
	public String fetchVersion() {
		return super.fetchVersion()+UserGroupData.class+"=$Revision: 1.1 $\n";
	}
	protected java.util.Map<String,GroupEntity> getGroup(Element root) {	
	 	java.util.Map<String,GroupEntity> result = new java.util.HashMap<>();
	 	if(root==null) {
	 		return result;
	 	}
	 	String attr = root.getAttribute("name");
		if(attr==null) {
			root.getAttribute("groupname");
		}
	 	if(attr!=null && !attr.trim().equals("")) {
		 	GroupEntity ge = new GroupEntity(root);
		 	result.remove(attr);
		 	result.put(attr,ge);
	 	}
		NodeList children = root.getChildNodes();
	    for (int i = 0,isz=children.getLength(); i<isz; i++) {
	        Node child = children.item(i);
	        if(child.getNodeType()==Element.ELEMENT_NODE && child.getNodeName().equals("item")) {
				Element element = (Element)child;
				initGroup(element,result,root);	        	
	        }
	    }
	    return result;
	}
	public String getGroups() {
		return getString("groups");
	}
	public String getUsers() {
		return getString("users");
	}
	protected void initGroup(Element root,java.util.Map<String,GroupEntity> map,Element parent) {
	 	String attr = root.getAttribute("name");
		if(attr==null) {
			root.getAttribute("groupname");
		}
	 	if(attr!=null && !attr.trim().equals("")) {
		 	GroupEntity ge = new GroupEntity(root);
		 	map.remove(attr);
		 	map.put(attr,ge);
			if(parent!=null) {
				attr = parent.getAttribute("name");
				if(attr==null) {
					attr = parent.getAttribute("groupname");
				}
				ge.setSupergroup(attr);
			}
	 	}
	    NodeList children = root.getChildNodes();
	    for (int i = 0,isz=children.getLength();i<isz; i++) {
	        Node child = children.item(i);
	        if(child.getNodeType()==Element.ELEMENT_NODE && child.getNodeName().equals("item")) {
		        Element element = (Element) child;
		        initGroup(element,map,root);	        	
	        }
	    }
	}
	protected void initialize() {
		super.initialize();
		setTable("tgroup");
		addSchema("users",java.sql.Types.VARCHAR);
		addSchema("groups",java.sql.Types.VARCHAR);
		map("users","users");
		map("groups","groups");
		//#intialize how deep is your love 
		//#(30000) programmer code begin;
		//#(30000) programmer code end;
	}
	public static void main(String[] args) {
		try {
			//java com/fs/dev/strok/UserGroupData -u tso -a retrieve
			if(args.length>0) {
				String user = Arguments.getString(args, "tso", "-u");
				String progid = Arguments.getString(args, "tso001", "-p");
				String action = Arguments.getString(args, "retrieve", "-a", "-act");
				
				StringBuilder gbuf = new StringBuilder();
				gbuf.append("<item name=\"MD\" description=\"Management Director\">");
	        	gbuf.append("	<item name=\"ADMIN\" description=\"Administrator\">");
	        	gbuf.append("		<user><uid>tso</uid></user>");
				gbuf.append("		<item name=\"OPERATOR\"  description=\"Operator\">");
				gbuf.append("			<user><uid>tch</uid><uid>tas</uid></user>");
				gbuf.append("		</item>");
				gbuf.append("		<item name=\"TESTER\" description=\"Testing\">");
				gbuf.append("			<user><uid>tos</uid></user>");
				gbuf.append("		</item>");
				gbuf.append("		<item name=\"TSOGRP\"  description=\"Testing\"></item>");
				gbuf.append("		<item name=\"QA\"  description=\"Quality Assurance\"></item>");
	        	gbuf.append("	</item>");
				gbuf.append("</item>");
	
				StringBuilder ubuf = new StringBuilder();
				ubuf.append("<users>");
	        	ubuf.append("	<user>");
				ubuf.append("		<uid>admin</uid>");
				ubuf.append("		<name>Adminstrator </name>");
	        	ubuf.append("	</user>");
	        	ubuf.append("	<user>");
				ubuf.append("		<uid>tas</uid>");
				ubuf.append("		<name>Tassun Oros</name>");
				ubuf.append("		<groups>");
				ubuf.append("			<group>");
				ubuf.append("				<gid>ADMIN</gid>");
				ubuf.append("				<role>ADMIN</role>");
				ubuf.append("			</group>");
				ubuf.append("		</groups>");
	        	ubuf.append("	</user>");
	        	ubuf.append("	<user>");
				ubuf.append("		<uid>tso</uid>");
				ubuf.append("		<name>Tassun Oros</name>");
				ubuf.append("		<groups>");
				ubuf.append("			<group>");
				ubuf.append("				<gid>ADMIN</gid>");
				ubuf.append("				<role>ADMIN</role>");
				ubuf.append("			</group>");
				ubuf.append("		</groups>");
	        	ubuf.append("	</user>");
	        	ubuf.append("	<user>");
				ubuf.append("		<uid>tch</uid>");
				ubuf.append("		<name>Touchy</name>");
				ubuf.append("		<groups>");
				ubuf.append("			<group>");
				ubuf.append("				<gid>OPERATOR</gid>");
				ubuf.append("				<role>OPERATOR</role>");
				ubuf.append("			</group>");
				ubuf.append("		</groups>");
	        	ubuf.append("	</user>");
				ubuf.append("</users>");
				
				GlobalBean gb = new GlobalBean();
				gb.setFsProg(progid);
				gb.setFsVar("fsUser",user);
			
				UserGroupData gd = new UserGroupData();		
				gd.setGroups(gbuf.toString());
				gd.setUsers(ubuf.toString());
				gd.init(gb);
				try(java.sql.Connection conn = DBConnection.getConnection()) {
					int rows = 0;
					if(action.equals("retrieve")) {
						rows = gd.retrieve(conn);
					} else {
						rows = gd.update(conn);
					}
					Console.out.println("effected "+rows+" rows.");
				}
				Console.out.println(gd.getGroups());
				Console.out.println(gd.getUsers());
			} else {
				Console.out.println("USAGE : "+UserGroupData.class);
				Console.out.println("\t-u user id");
				Console.out.println("\t-p program id");
			}
		} catch(Exception ex) {
			Console.out.print(ex);
		}
	}
	public boolean obtain(BeanSchemaInterface bean) throws Exception {
		if(bean==null) {
			return super.obtain(bean);
		}
		setGroups(bean.getFieldByName(mapper("groups")).asString());
		setUsers(bean.getFieldByName(mapper("users")).asString());
		//#obtain it perfect moment
		//#(40000) programmer code begin;
		//#(40000) programmer code end;
		return super.obtain(bean);
	}
	public int retrieve(java.sql.Connection connection) throws Exception {
		KnSQL sql = new KnSQL(this);
		sql.append("select * from "+getTable());
		//#any retrieve statement too close
		//#(170000) programmer code begin;
		//#(170000) programmer code end;
		//#assigned parameters make it happen
		//#(180000) programmer code begin;
		GroupEntity root = null;
		java.util.HashMap<String,GroupEntity> map = new java.util.HashMap<>();
		//#(180000) programmer code end;
		int result = 0;
		try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
			while(rs.next()) {
				result++;
				//#any result fetching fool again
				//#(220000) programmer code begin;
				GroupEntity ge = new GroupEntity();
				ge.fetchResult(rs);		
				map.put(ge.getGroupname(),ge);
				if(ge.getSupergroup()==null || ge.getSupergroup().trim().equals("")) {
					root = ge;
				}
				//#(220000) programmer code end;
			}
		}
		//#after fetching result set lovin each day
		//#(230000) programmer code begin;			
		for(GroupEntity ge : map.values()) {
			if(ge.getSupergroup()!=null && !ge.getSupergroup().trim().equals("")) {
				GroupEntity sg = (GroupEntity)map.get(ge.getSupergroup());
				if(sg!=null) {
					sg.addGroup(ge);
				}
			}
		}
		if(root==null) {
			boolean found = true;
			while(found) {
				found = false;				
				for(GroupEntity ge : map.values()) {
					if(ge.getSupergroup()!=null && !ge.getSupergroup().trim().equals("")) {
						GroupEntity sg = (GroupEntity)map.get(ge.getSupergroup());
						if(sg!=null) {
							map.remove(ge.getGroupname());
							found = true;
							break;
						}
					}
				}
			}
			if(!map.isEmpty()) {
				java.util.Iterator<GroupEntity> it = map.values().iterator();
				if(it.hasNext()) {
					root = it.next();
				}
			}
		}
		if(root!=null) {
			setGroups(root.toXML());
		}
		UserList users = new UserList();
		sql.clear();
		sql.append("select userid,userename,useresurname from tuser");
		try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
			while(rs.next()) {
				UserEntity ue = new UserEntity();
				ue.fetchResult(rs);
				users.addUser(ue);
			}
		}
		sql.clear();
		sql.append("select * from tusergrp");
		try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
			while(rs.next()) {
				String uid = rs.getString("userid");
				String gid = rs.getString("groupname");
				String role = rs.getString("rolename");
				if(uid!=null && gid!=null) {
					UserEntity ue = users.getUser(uid);
					if(ue!=null) {
						GroupEntity ge = (GroupEntity)map.get(gid);
						if(ge!=null) ge.addUser(ue,role);
						ue.addGroup(gid,role);
					}
				}
			}
		}
		setUsers(users.toXML());
		//#(230000) programmer code end;
		return result;
	}
	public void setGroups(String newGroups) {
		setMember("groups",newGroups);
	}
	public void setUsers(String newUsers) {
		setMember("users",newUsers);
	}
	public int update(java.sql.Connection connection) throws Exception {
		//#begin with update statement going on
		//#(290000) programmer code begin;
		boolean testing = false;
		int result = 0;
		KnSQL sql = new KnSQL(this);
		sql.append("delete from "+getTable());
		if(!testing) result += sql.executeUpdate(connection);
		sql.clear();
		sql.append("insert into "+getTable());
		sql.append(" (groupname,supergroup,description)");
		sql.append(" values(?groupname,?supergroup,?description)");
		//#(290000) programmer code end;
		//#any update statement over protected
		//#(150000) programmer code begin;
		if(getGroups()!=null && !getGroups().trim().equals("")) {
	    	StringBuilder buf = new StringBuilder();
	    	buf.append("<?xml version=\"1.0\" encoding=\"TIS-620\"?>");
	    	buf.append(getGroups());
	    	DOMReader reader = new DOMReader();    	
	    	Document doc = reader.parseXML(buf.toString());
	    	Element root = doc.getDocumentElement();
	    	java.util.Map<String,GroupEntity> map = getGroup(root);
			if(!map.isEmpty()) {			
				KnSQL sql1 = new KnSQL(this);
				sql1.append("delete from tusergrp where userid = ?userid ");	
				KnSQL sql2 = new KnSQL(this);
				sql2.append("insert into tusergrp(userid,groupname,rolename)");
				sql2.append(" values(?userid,?groupname,?rolename) ");				
				for(GroupEntity ge : map.values()) {
					if(ge!=null) {
						sql.setParameter("groupname",ge.getGroupname());
						sql.setParameter("supergroup",ge.getSupergroup()==null?"":ge.getSupergroup());
						sql.setParameter("description",ge.getDescription());
						if(!testing) result += sql.executeUpdate(connection,false);
					}
				}				
				for(GroupEntity ge : map.values()) {
					if(ge!=null) {
						if(ge.getUserList()!=null) {
							java.util.Map<String,UserEntity> users = ge.getUserList().getUsers();
							if(users!=null && !users.isEmpty()) {								
								for(UserEntity ue : users.values()) {
									if(ue!=null) {
										sql1.setParameter("userid",ue.getUserid());
										if(!testing) {
											result += sql1.executeUpdate(connection);
										}
	
									}
								}
							}					
						}
					}
				}				
				for(GroupEntity ge : map.values()) {
					if(ge!=null) {
						if(ge.getUserList()!=null) {
							java.util.Map<String,UserEntity> users = ge.getUserList().getUsers();
							if(users!=null && !users.isEmpty()) {								
								for(UserEntity ue : users.values()) {
									if(ue!=null) {
										String role = ue.getRole(ge.getGroupname());
										sql2.setParameter("userid",ue.getUserid());
										sql2.setParameter("groupname",ge.getGroupname());
										sql2.setParameter("rolename",role==null?ge.getGroupname():role);
										if(!testing) {
											result += sql2.executeUpdate(connection);
										}
									}
								}
							}					
						}
					}
				}
			}
		}
		if(getUsers()!=null && !getUsers().trim().equals("")) {
			StringBuilder buf = new StringBuilder();  	
			buf.append("<?xml version=\"1.0\" encoding=\"TIS-620\"?>");
			buf.append(getUsers());
			DOMReader reader = new DOMReader();
			Document doc = reader.parseXML(buf.toString());
			Element root = doc.getDocumentElement();
			UserList list = new UserList(root,null);
			KnSQL sql1 = new KnSQL(this);
			sql1.append("delete from tusergrp where userid = ?userid ");
			
			KnSQL sql2 = new KnSQL(this);
			sql2.append("insert into tusergrp(userid,groupname,rolename)");
			sql2.append(" values(?userid,?groupname,?rolename) ");
			java.util.Map<String,UserEntity> users = list.getUsers();
			if(users!=null && !users.isEmpty()) {				
				for(UserEntity ue : users.values()) {
					if(ue!=null) {
						sql1.setParameter("userid",ue.getUserid());
						if(!testing) {
							result += sql1.executeUpdate(connection);
						}	
						java.util.Map<String,String> groups = ue.getGroups();
						if(groups!=null && !groups.isEmpty()) {							
							for(java.util.Iterator<String> git = groups.keySet().iterator();git.hasNext();) {
								String gid = git.next();
								if(gid!=null) {
									String role = (String)groups.get(gid);
									sql2.setParameter("userid",ue.getUserid());
									sql2.setParameter("groupname",gid);
									sql2.setParameter("rolename",role==null?gid:role);
									if(!testing) {
										result += sql2.executeUpdate(connection);
									}
								}
							}
						}
					}
				}
			}
		}
		//#(150000) programmer code end;
		//#assigned parameters all rise
		//#(160000) programmer code begin;
		//#(160000) programmer code end;
		return result;
		//#ending with delete statement going on
		//#(300000) programmer code begin;
		//#(300000) programmer code end;
	}
}
