package com.fs.dev.auth;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: LogonData.java
 * Description: LogonData class implements for handle tus data base schema.
 * Version: $Revision: 1.6 $
 * Creation date: Fri Sep 30 13:43:06 GMT+07:00 2005
 */
/**
 * LogonData class implements for handle tus data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.util.*;
import com.fs.dev.Arguments;
import com.fs.dev.Console;
import com.fs.bean.misc.*;
import java.sql.*;

//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings("serial")
public class SignonData extends BeanData {
	//#another methods defined drive you crazy
	//#(100000) programmer code begin;
	//#(100000) programmer code end;
	//#defined & declaration the real thing
	//#(20000) programmer code begin;
	//#(20000) programmer code end;	
	public SignonData() {
		super();
	}
	public static void main(String[] args) {
		//java com/fs/dev/strok/SignonData -ms STROK -user tso
		try {
			String section = Arguments.getString(args, null, "-ms");
			String user = Arguments.getString(args, null, "-user");
			if(args.length>0) {
				if(user!=null && user.trim().length()>0) {
					try(java.sql.Connection conn = SignonData.getNewConnection(section)) {
						java.text.SimpleDateFormat f = new java.text.SimpleDateFormat("dd/MM/yyyy",java.util.Locale.US);
						GlobalBean g = new GlobalBean();
						g.setFsVar("fsUser","tso");
						g.setFsVar("fsDate",f.format(new java.util.Date()));
						SignonData d = new SignonData();
						d.setUserid(user);
						d.init(g);
						int rows = d.retrieve(conn);
						Console.out.println("effected "+rows+" records.");
						Console.out.println(d);
					}
				}
			} else {
				Console.out.println("USAGE : "+SignonData.class);
				Console.out.println("\t-ms main db section");
				Console.out.println("\t-user user id");
				Console.out.println("\t-pwd user password");
				Console.out.println("Please specified db section in global_config.xml with option -ms");
			}
		} catch(Exception ex) {
			Console.out.print(ex);
		}
	}
	protected void initialize() {
		super.initialize();
		setTable("tuser");
		addSchema("userid",java.sql.Types.VARCHAR);
		addSchema("username",java.sql.Types.VARCHAR);
		addSchema("userename",java.sql.Types.VARCHAR,true);
		addSchema("usertname",java.sql.Types.VARCHAR,true);
		addSchema("userpassword",java.sql.Types.VARCHAR,true);
		addSchema("userbranch",java.sql.Types.VARCHAR,true);
		map("usertname","name");
		map("userid","user");
		map("username","username");
		map("userpassword","password");
		map("userbranch","branch");
		//#intialize how deep is your love 
		//#(30000) programmer code begin;
		map("logondate","logondate");
		addSchema("adminflag",java.sql.Types.VARCHAR);
		addSchema("activeflag",java.sql.Types.VARCHAR);
		addSchema("lockflag",java.sql.Types.VARCHAR);
		addSchema("products",java.sql.Types.VARCHAR);
		addSchema("projects",java.sql.Types.VARCHAR);
		addSchema("photoimage",java.sql.Types.VARCHAR);
		addSchema("passwordexpiredate",java.sql.Types.DATE);
		addSchema("status",java.sql.Types.VARCHAR);
		addSchema("loginfailtimes",java.sql.Types.VARCHAR);
		addSchema("firstpage",java.sql.Types.VARCHAR);
		addSchema("theme",java.sql.Types.VARCHAR);
		addSchema("systemdate",java.sql.Types.DATE);
		addSchema("usertype",java.sql.Types.VARCHAR);
		addSchema("site",java.sql.Types.VARCHAR);
		addSchema("iconfile",java.sql.Types.VARCHAR);
		addSchema("accessdate",java.sql.Types.DATE);
		addSchema("accesstime",java.sql.Types.TIME);
		addSchema("accesshits",java.sql.Types.BIGINT);
		addSchema("failtime",java.sql.Types.BIGINT);
		addSchema("groups",java.sql.Types.VARCHAR,true);
		addSchema("email",java.sql.Types.VARCHAR,true);
		addSchema("deptcode",java.sql.Types.VARCHAR,true);
		addSchema("divcode",java.sql.Types.VARCHAR,true);
		addSchema("rolename",java.sql.Types.VARCHAR,true);
		addSchema("supervisor",java.sql.Types.VARCHAR,true);
		addSchema("messages",java.sql.Types.VARCHAR,true);
		addSchema("roles",java.sql.Types.VARCHAR,true);
		addSchema("mobile",java.sql.Types.VARCHAR,true);
		addSchema("cardid",java.sql.Types.VARCHAR,true);
		map("groups","groups");
		map("adminflag","adminflag");
		map("activeflag","activeflag");
		map("lockflag","lockflag");
		map("products","products");
		map("projects","projects");
		map("email","email");
		map("deptcode","deptcode");
		map("divcode","divcode");
		map("photoimage","photoimage");
		map("rolename","rolename");
		map("passwordexpiredate","passwordexpiredate");
		map("supervisor","supervisor");
		map("status","status");
		map("loginfailtimes","loginfailtimes");
		map("firstpage","firstpage");
		map("theme","theme");
		map("messages","messages");
		map("roles","roles");	
		map("systemdate","systemdate");
		map("usertype","usertype");
		map("site","site");
		map("iconfile","iconfile");
		map("accessdate","accessdate");
		map("accesstime","accesstime");
		map("accesshits","accesshits");
		map("failtime","failtime");
		map("mobile","mobile");
		map("cardid","cardid");
		addSchema("background",java.sql.Types.VARCHAR,true);
		addSchema("birthday",java.sql.Types.DATE,true);
		addSchema("nationcode",java.sql.Types.VARCHAR,true);
		addSchema("titlecode",java.sql.Types.VARCHAR,true);
		addSchema("employeeid",java.sql.Types.VARCHAR,true);
		addSchema("positionid",java.sql.Types.VARCHAR,true);
		addSchema("positionlevel",java.sql.Types.INTEGER,true);
		addSchema("siteflag",java.sql.Types.VARCHAR,true);
		addSchema("branchflag",java.sql.Types.VARCHAR,true);
		addSchema("branches",java.sql.Types.VARCHAR,true);
		addSchema("sites",java.sql.Types.VARCHAR,true);		
		addSchema("headsite",java.sql.Types.VARCHAR,true);
		addSchema("headbranch",java.sql.Types.VARCHAR,true);
		map("background","background");
		map("birthday","birthday");
		map("nationcode","nationcode");
		map("titlecode","titlecode");
		map("employeeid","employeeid");
		map("positionid","positionid");
		map("positionlevel","positionlevel");
		map("siteflag","siteflag");
		map("branchflag","branchflag");
		map("branches","branches");
		map("sites","sites");
		map("headsite","headsite");
		map("headbranch","headbranch");
		addSchema("ename",java.sql.Types.VARCHAR,true);
		addSchema("esurname",java.sql.Types.VARCHAR,true);
		addSchema("tname",java.sql.Types.VARCHAR,true);
		addSchema("tsurname",java.sql.Types.VARCHAR,true);
		addSchema("compename",java.sql.Types.VARCHAR,true);
		addSchema("comptname",java.sql.Types.VARCHAR,true);
		addSchema("branchename",java.sql.Types.VARCHAR,true);
		addSchema("branchtname",java.sql.Types.VARCHAR,true);
		addSchema("effectdate",java.sql.Types.DATE,true);
		map("ename","ename");
		map("esurname","esurname");
		map("tname","tname");
		map("tsurname","tsurname");
		map("compename","compename");
		map("comptname","comptname");
		map("branchename","branchename");
		map("branchtname","branchtname");
		map("effectdate","effectdate");
		addSchema("changeflag",java.sql.Types.VARCHAR,true);
		addSchema("gpsflag",java.sql.Types.VARCHAR,true);
		addSchema("partysite",java.sql.Types.VARCHAR,true);
		map("changeflag","changeflag");
		map("gpsflag","gpsflag");
		map("partysite","partysite");
		//#(30000) programmer code end;
	}
	public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
		super.fetchResult(rs);
		setUserid(rs.getString("userid"));
		setUsername(rs.getString("username"));
		setUserpassword(rs.getString("userpassword"));
		setAdminflag(rs.getString("adminflag"));
		String status = rs.getString("status");
		if(status==null) status = "";
		setStatus(status);
		setActiveflag(status.equals("A")?"1":"0");
		setLockflag(rs.getString("lockflag"));
		setLoginfailtimes(rs.getInt("loginfailtimes"));
		setPasswordexpiredate(rs.getDate("passwordexpiredate"));
		setTheme(rs.getString("theme"));
		String showphoto = rs.getString("showphoto");
		if(showphoto!=null && showphoto.equals("0")) {
			setPhotoimage(null);
		}
		setUsertype(rs.getString("usertype"));
		setSite(rs.getString("site"));
		setIconfile(rs.getString("iconfile"));
		setAccessdate(rs.getDate("accessdate"));
		setAccesstime(rs.getTime("accesstime"));
		setAccesshits(rs.getLong("accesshits"));
		setFailtime(rs.getLong("failtime"));
		setSiteflag(rs.getString("siteflag"));
		setBranchflag(rs.getString("branchflag"));
		setHeadsite(getSite());
		setChangeflag(rs.getString("changeflag"));
		//#(60000) programmer code end;
	}
	public String fetchVersion() {
		return super.fetchVersion()+SignonData.class+"=$Revision: FS-20170623-091500 $\n";
	}
	public Date getAccessdate() {
		return getDate("accessdate");
	}
	public long getAccesshits() {
		return getLong("accesshits");
	}
	public Time getAccesstime() {
		return getTime("accesstime");
	}
	public String getActiveflag() {
		return getString("activeflag");
	}
	public String getAdminflag() {
		return getString("adminflag");
	}
	public String getBackground() {
		return getString("background");
	}
	public Date getBirthday() {
		return getDate("birthday");
	}
	public String getBranchename() {
		return getString("branchename");
	}
	public String getBranches() {
		return getString("branches");
	}
	public String getBranchflag() {
		return getString("branchflag");
	}
	public String getBranchtname() {
		return getString("branchtname");
	}
	public String getCardid() {
		return getString("cardid");
	}
	public String getChangeflag() {
		return getString("changeflag");
	}
	public String getCompename() {
		return getString("compename");
	}
	public String getComptname() {
		return getString("comptname");
	}
	public String getDeptcode() {
		return getString("deptcode");
	}
	public String getDivcode() {
		return getString("divcode");
	}
	public Date getEffectdate() {
		return getDate("effectdate");
	}
	public String getEmail() {
		return getString("email");
	}
	public String getEmployeeid() {
		return getString("employeeid");
	}
	public String getEname() {
		return getString("ename");
	}
	public String getEsurname() {
		return getString("esurname");
	}
	public long getFailtime() {
		return getLong("failtime");
	}
	public String getGpsflag() {
		return getString("gpsflag");
	}
	public String getGroups() {
		return getString("groups");
	}
	public String getHeadbranch() {
		return getString("headbranch");
	}
	public String getHeadsite() {
		return getString("headsite");
	}
	public String getIconfile() {
		return getString("iconfile");
	}
	public String getLockflag() {
		return getString("lockflag");
	}	
	public int getLoginfailtimes() {
		return getInt("loginfailtimes");
	}
	public Date getLogondate() {
		return getDate("logondate");
	}
	public String getMessages() {
		return getString("messages");
	}
	public String getMobile() {
		return getString("mobile");
	}
	public String getNationcode() {
		return getString("nationcode");
	}
	public String getPartysite() {
		return getString("partysite");
	}
	public Date getPasswordexpiredate() {
		return getDate("passwordexpiredate");
	}	
	public String getPhotoimage() {
		return getString("photoimage");
	}
	public String getPositionid() {
		return getString("positionid");
	}
	public int getPositionlevel() {
		return getInt("positionlevel");
	}
	public String getProducts() {
		return getString("products");
	}
	public String getProjects() {
		return getString("projects");
	}
	public String getRolename() {
		return getString("rolename");
	}
	public String getRoles() {
		return getString("roles");
	}
	public String getSite() {
		return getString("site");
	}
	public String getSiteflag() {
		return getString("siteflag");
	}
	public String getSites() {
		return getString("sites");
	}
	public String getStatus() {
		return getString("status");
	}
	public String getSupervisor() {
		return getString("supervisor");
	}
	public Date getSystemdate() {
		return getDate("systemdate");
	}
	public String getTheme() {
		return getString("theme");
	}
	public String getTitlecode() {
		return getString("titlecode");
	}
	public String getTname() {
		return getString("tname");
	}
	public String getTsurname() {
		return getString("tsurname");
	}
	public String getUserbranch() {
		return getString("userbranch");
	}
	public String getUserename() {
		return getString("userename");
	}
	public String getUserid() {
		return getString("userid");
	}
	public String getUsername() {
		return getString("username");
	}
	public String getUserpassword() {
		return getString("userpassword");
	}
	public String getUsertname() {
		return getString("usertname");
	}
	public String getUsertype() {
		return getString("usertype");
	}	
	public boolean obtain(BeanSchemaInterface bean) throws Exception {
		if(bean==null) return super.obtain(bean);
		setUserid(bean.getFieldByName(mapper("userid")).asString());
		setUsername(bean.getFieldByName(mapper("username")).asString());
		setUserename(bean.getFieldByName(mapper("userename")).asString());
		setUsertname(bean.getFieldByName(mapper("usertname")).asString());
		setUserpassword(bean.getFieldByName(mapper("userpassword")).asString());
		setUserbranch(bean.getFieldByName(mapper("userbranch")).asString());
		//#obtain it perfect moment
		//#(40000) programmer code begin;
		setEmail(bean.getFieldByName(mapper("email")).asString());
		setLogondate(bean.getFieldByName(mapper("logondate")).asDate());
		setPasswordexpiredate(bean.getFieldByName(mapper("passwordexpiredate")).asDate());
		setDeptcode(bean.getFieldByName(mapper("deptcode")).asString());
		setDivcode(bean.getFieldByName(mapper("divcode")).asString());
		setPhotoimage(bean.getFieldByName(mapper("photoimage")).asString());
		setRolename(bean.getFieldByName(mapper("rolename")).asString());
		setSupervisor(bean.getFieldByName(mapper("supervisor")).asString());
		setStatus(bean.getFieldByName(mapper("status")).asString());
		setTheme(bean.getFieldByName(mapper("theme")).asString());
		setLoginfailtimes(bean.getFieldByName(mapper("loginfailtimes")).asInt());
		setMessages(bean.getFieldByName(mapper("messages")).asString());
		setRoles(bean.getFieldByName(mapper("roles")).asString());
		setUsertype(bean.getFieldByName(mapper("usertype")).asString());
		setSite(bean.getFieldByName(mapper("site")).asString());
		setIconfile(bean.getFieldByName(mapper("iconfile")).asString());
		setMobile(bean.getFieldByName(mapper("mobile")).asString());
		setCardid(bean.getFieldByName(mapper("cardid")).asString());
		//#(40000) programmer code end;
		return super.obtain(bean);
	}
	public int retrieve(java.sql.Connection connection) throws Exception {
		KnSQL sql = new KnSQL(this);
		sql.append("select * from ");
		sql.append(getTable());
		sql.append(" where username=?username ");
		//#any retrieve statement too close
		//#(170000) programmer code begin;
		//#(170000) programmer code end;
		sql.setParameter("username",getUsername());
		//#assigned parameters make it happen
		//#(180000) programmer code begin;
		//String uid = getUserid();
		//String pwd = getUserpassword();
		//#(180000) programmer code end;
		int result = 0;
		try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
			if(rs.next()) {
				result++;
				fetchResult(rs);
				//#any result fetching fool again
				//#(220000) programmer code begin;
				//#(220000) programmer code end;
			}
		}
		//#after fetching result set lovin each day
		//#(230000) programmer code begin;
		setUserename(getUsername());
		setUsertname(getUsername());
		if(result>0) {
			sql.clear();
			sql.append("select * from tuserinfo ");
			sql.append("where userid = ?userid ");
			sql.setParameter("userid",getUserid());
			try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
				while(rs.next()) {
					//#fetching other result desire
					//#(60000) programmer code begin;	
					setUserename(rs.getString("userename"));
					setUsertname(rs.getString("usertname"));
					setUserbranch(rs.getString("userbranch"));
					String ename = rs.getString("userename");
					if(ename==null) ename = "";
					String esurname = rs.getString("useresurname");
					if(esurname==null) esurname = "";
					String tname = rs.getString("usertname");
					if(tname==null) tname = "";
					String tsurname = rs.getString("usertsurname");
					if(tsurname==null) tsurname = "";
					setUserename(ename+"  "+esurname);
					setUsertname(tname+"  "+tsurname);
					setEname(ename);
					setTname(tname);
					setEsurname(esurname);
					setTsurname(tsurname);				
					setHeadbranch(getUserbranch());
					setPhotoimage(rs.getString("photoimage"));
					setSupervisor(rs.getString("supervisor"));
					setMobile(rs.getString("mobile"));
					setEmail(rs.getString("email"));
					setCardid(rs.getString("cardid"));
					setDeptcode(rs.getString("deptcode"));
					setDivcode(rs.getString("divcode"));
					setNationcode(rs.getString("nationcode"));
					setTitlecode(rs.getString("titlecode"));
					setBirthday(rs.getDate("birthday"));
					setEmployeeid(rs.getString("employeeid"));
					setPositionid(rs.getString("positionid"));
					setPositionlevel(rs.getInt("positionlevel"));
					setEffectdate(rs.getDate("effectdate"));
				}
			}
			//find out all groups under setting to be access programs
			StringBuilder gbuf = new StringBuilder();
			StringBuilder rbuf = new StringBuilder();
			sql.clear();
			sql.append("select * from tusergrp ");
			sql.append("where userid = ?userid ");
			sql.setParameter("userid",getUserid());
			try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
				while(rs.next()) {
					String gid = rs.getString("groupname");
					if(gid!=null && !gid.trim().equals("")) {
						if(gbuf.toString().length()>0) gbuf.append(",");
						gbuf.append(gid);
					}
					String role = rs.getString("rolename");
					if(role!=null && !role.trim().equals("")) {
						if(rbuf.toString().length()>0) rbuf.append(",");
						rbuf.append(role);
						setRolename(role);
					}
				}
			}
			setGroups(gbuf.toString());
			setRoles(rbuf.toString());
			setSystemdate(new java.sql.Date(System.currentTimeMillis()));
			//find out sites under setting by this user (grant by specified site access setting)
			StringBuilder buf = new StringBuilder();
			sql.clear();
			sql.append("select site from tusercomp ");
			sql.append("where userid = ?userid ");
			sql.setParameter("userid",getUserid());
			try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
				while(rs.next()) {
					String sid = rs.getString("site");
					if(sid!=null && !sid.trim().equals("")) {
						if(buf.toString().length()>0) buf.append(",");
						buf.append(sid);
					}
				}
			}
			setSites(buf.toString());
			if(getSite()!=null && getSite().trim().length()>0) {				
				sql.clear();
				sql.append("select bgimage,headsite,nameen,nameth from tcomp ");
				sql.append("where site = ?site ");
				sql.setParameter("site",getSite());
				try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
					if(rs.next()) {
						setBackground(rs.getString("bgimage"));
						setHeadsite(rs.getString("headsite"));
						setCompename(rs.getString("nameen"));
						setComptname(rs.getString("nameth"));
					}
				}			
				//find out all sites under headsite when access all site flag = 1
				if("1".equals(getSiteflag())) {
					buf.setLength(0);
					sql.clear();
					sql.append("select site from tcompgrp ");
					sql.append("where headsite = ?headsite ");
					sql.setParameter("headsite",getSite());
					try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
						while(rs.next()) {
							String sid = rs.getString("site");
							if(sid!=null && !sid.trim().equals("")) {
								if(buf.toString().length()>0) buf.append(",");
								buf.append(sid);
							}
						}
					}
					if(buf.length()>0) setSites(buf.toString());
				}
				//find out all branch under setting by this user when access all branch flag = 1
				if("1".equals(getBranchflag())) {
					buf.setLength(0);
					sql.clear();
					sql.append("select branch from tuserbranch ");
					sql.append("where site = ?site ");
					sql.append("and userid = ?userid ");
					sql.setParameter("site",getSite());
					sql.setParameter("userid",getUserid());
					try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
						while(rs.next()) {
							String bid = rs.getString("branch");
							if(bid!=null && !bid.trim().equals("")) {
								if(buf.toString().length()>0) buf.append(",");
								buf.append(bid);
							}
						}
					}
					if(buf.length()>0) setBranches(buf.toString());
				}
				if(getUserbranch()!=null && getUserbranch().trim().length()>0) {
					sql.clear();
					sql.append("select gpsflag,nameen,nameth from tcompbranch ");
					sql.append("where site = ?site ");
					sql.append("and branch = ?branch ");
					sql.setParameter("site",getSite());
					sql.setParameter("branch",getUserbranch());
					try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
						if(rs.next()) {
							setGpsflag(rs.getString("gpsflag"));
							setBranchename(rs.getString("nameen"));
							setBranchtname(rs.getString("nameth"));
						}
					}
				}
				//find out party site (partner ship) of this user	
				buf.setLength(0);
				sql.clear();
				sql.append("select site from tcompparty ");
				sql.append("where partysite = ?partysite ");
				sql.setParameter("partysite",getSite());
				try(java.sql.ResultSet rs = sql.executeQuery(connection)) {
					while(rs.next()) {
						String sid = rs.getString("site");
						if(sid!=null && !sid.trim().equals("")) {
							if(buf.toString().length()>0) buf.append(",");
							buf.append(sid);
						}
					}
				}
				if(buf.length()>0) setPartysite(buf.toString());
			}
		}
		//#(230000) programmer code end;
		return result;
	}
	public void setAccessdate(Date newAccessdate) {
		setMember("accessdate",newAccessdate);
	}
	public void setAccesshits(long newAccesshits) {
		setMember("accesshits",newAccesshits);
	}
	public void setAccesstime(Time newAccesstime) {
		setMember("accesstime",newAccesstime);
	}
	public void setActiveflag(String newActiveflag) {
		setMember("activeflag",newActiveflag);
	}
	public void setAdminflag(String newAdminflag) {
		setMember("adminflag",newAdminflag);
	}
	public void setBackground(String newBackground) {
		setMember("background",newBackground);
	}
	public void setBirthday(Date newBirthday) {
		setMember("birthday",newBirthday);
	}
	public void setBranchename(String newBranchename) {
		setMember("branchename",newBranchename);
	}
	public void setBranches(String newBranches) {
		setMember("branches",newBranches);
	}
	public void setBranchflag(String newBranchflag) {
		setMember("branchflag",newBranchflag);
	}
	public void setBranchtname(String newBranchtname) {
		setMember("branchtname",newBranchtname);
	}
	public void setCardid(String newCardid) {
		setMember("cardid",newCardid);
	}
	public void setChangeflag(String newChangeflag) {
		setMember("changeflag",newChangeflag);
	}
	public void setCompename(String newCompename) {
		setMember("compename",newCompename);
	}
	public void setComptname(String newComptname) {
		setMember("comptname",newComptname);
	}
	public void setDeptcode(String newDeptcode) {
		setMember("deptcode",newDeptcode);
	}
	public void setDivcode(String newDivcode) {
		setMember("divcode",newDivcode);
	}
	public void setEffectdate(Date newEffectdate) {
		setMember("effectdate",newEffectdate);
	}
	public void setEmail(String newEmail) {
		setMember("email",newEmail);
	}
	public void setEmployeeid(String newEmployeeid) {
		setMember("employeeid",newEmployeeid);
	}
	public void setEname(String newEname) {
		setMember("ename",newEname);
	}
	public void setEsurname(String newEsurname) {
		setMember("esurname",newEsurname);
	}
	public void setFailtime(long newFailtime) {
		setMember("failtime",newFailtime);
	}
	public void setGpsflag(String newGpsflag) {
		setMember("gpsflag",newGpsflag);
	}
	public void setGroups(String newGroups) {
		setMember("groups",newGroups);
	}
	public void setHeadbranch(String newHeadbranch) {
		setMember("headbranch",newHeadbranch);
	}
	public void setHeadsite(String newHeadsite) {
		setMember("headsite",newHeadsite);
	}
	public void setIconfile(String newIconfile) {
		setMember("iconfile",newIconfile);
	}
	public void setLockflag(String newLockflag) {
		setMember("lockflag",newLockflag);
	}
	public void setLoginfailtimes(int newLoginfailtimes) {
		setMember("loginfailtimes",newLoginfailtimes);
	}
	public void setLogondate(Date newLogondate) {
		setMember("logondate",newLogondate);
	}
	public void setMessages(String newMessages) {
		setMember("messages",newMessages);
	}
	public void setMobile(String newMobile) {
		setMember("mobile",newMobile);
	}
	public void setNationcode(String newNationcode) {
		setMember("nationcode",newNationcode);
	}
	public void setNewwindowflag(String newWindowflag) {
		setMember("newwindowflag",newWindowflag);
	}
	public void setPartysite(String newPartysite) {
		setMember("partysite",newPartysite);
	}
	public void setPasswordexpiredate(Date newPasswordexpiredate) {
		setMember("passwordexpiredate",newPasswordexpiredate);
	}
	public void setPhotoimage(String newPhotoimage) {
		setMember("photoimage",newPhotoimage);
	}
	public void setPositionid(String newPositionid) {
		setMember("positionid",newPositionid);
	}
	public void setPositionlevel(int newPositionlevel) {
		setMember("positionlevel",newPositionlevel);
	}
	public void setProducts(String newProducts) {
		setMember("products",newProducts);
	}
	public void setProjects(String newProjects) {
		setMember("projects",newProjects);
	}
	public void setRolename(String newRolename) {
		setMember("rolename",newRolename);
	}
	public void setRoles(String newRoles) {
		setMember("roles",newRoles);
	}
	public void setSite(String newSite) {
		setMember("site",newSite);
	}
	public void setSiteflag(String newSiteflag) {
		setMember("siteflag",newSiteflag);
	}
	public void setSites(String newSites) {
		setMember("sites",newSites);
	}
	public void setStatus(String newStatus) {
		setMember("status",newStatus);
	}
	public void setSupervisor(String newSupervisor) {
		setMember("supervisor",newSupervisor);
	}
	public void setSystemdate(Date newSystemdate) {
		setMember("systemdate",newSystemdate);
	}
	public void setTheme(String newTheme) {
		setMember("theme",newTheme);
	}
	public void setTitlecode(String newTitlecode) {
		setMember("titlecode",newTitlecode);
	}
	public void setTname(String newTname) {
		setMember("tname",newTname);
	}
	public void setTsurname(String newTsurname) {
		setMember("tsurname",newTsurname);
	}
	public void setUserbranch(String newUserbranch) {
		setMember("userbranch",newUserbranch);
	}
	public void setUserename(String newUserename) {
		setMember("userename",newUserename);
	}
	public void setUserid(String newUserid) {
		setMember("userid",newUserid);
	}
	public void setUsername(String newUsername) {
		setMember("username",newUsername);
	}
	public void setUserpassword(String newUserpassword) {
		setMember("userpassword",newUserpassword);
	}
	public void setUsertname(String newUsertname) {
		setMember("usertname",newUsertname);
	}
	public void setUsertype(String newUsertype) {
		setMember("usertype",newUsertype);
	}	
}
