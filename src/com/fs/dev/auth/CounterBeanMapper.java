package com.fs.dev.auth;

/*
 * SCCS id: $Id: CounterBeanMapper.java,v 1.1 2008-02-08 14:57:52+07 tassun_o Exp $
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: $RCSfile: CounterBeanMapper.java,v $
 * Description: CounterBeanMapper class implements for handle mapping class
 * Version: $Revision: 1.1 $
 * Programmer: $Author: tassun_o $
 * Creation date: Fri Sep 30 13:43:10 GMT+07:00 2005
 */
/**
 * CounterBeanMapper class implements for handle mapping class.
 */
import com.fs.bean.gener.*;

//#import anything the right time
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings("serial")
public class CounterBeanMapper extends BeanMap {
	//#another methods defined irresistible
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	public CounterBeanMapper() {
		super();
	}
	public String fetchVersion() {
		return super.fetchVersion()+CounterBeanMapper.class+"=$Revision: 1.1 $\n";
	}
	protected void initialize() {
		super.initialize();
		mapClass("com.fs.dev.auth.CounterData");
		//#other initial give me a reason
		//#(20000) programmer code begin;
		//#(20000) programmer code end;
	}
}
