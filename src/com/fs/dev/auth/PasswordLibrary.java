package com.fs.dev.auth;

import java.sql.*;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.fs.bean.misc.*;
import com.fs.bean.util.*;
import com.fs.dev.Console;
import com.fs.dev.version.*;
import com.fs.interfaces.*;

@SuppressWarnings({"serial","unchecked", "rawtypes"})
public class PasswordLibrary extends VersionAdapter implements java.io.Serializable {
	//private java.sql.Connection conn = null;
	private java.util.HashMap ht = null;
	private java.sql.Date logondate = null;
	private String changedate = "";
	public PasswordLibrary() {
		super();
	}
	public PasswordLibrary(RelatableInterface relatable) {
		super(relatable);
	}
	public static void main(String[] args) {
		if(args.length>0) {
			BeanUtility util = new BeanUtility();
			String user = args[0];
			String usrpass = "";
			String newusrpass = "password";
			if(args.length>1) usrpass = args[1];
			if(args.length>2) newusrpass = args[2];
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
			if(args.length>3) {
				date = util.parseSQLDate(args[3]);
			}
			try {
				PasswordLibrary lib = new PasswordLibrary();
				try(java.sql.Connection conn = DBConnection.getConnection()) {
					java.util.Map hat = lib.changePassword(conn,user,usrpass,newusrpass,date);
					Console.out.println(hat);
				}
			} catch(Exception ex) {
				Console.out.print(ex);
			}
		}
	}
	private int parseInt(String val) {
		if(val!=null && !val.equals("")) {
			try {
				return Integer.parseInt(val);
			} catch(Exception ex) { }
		}
		return 0;
	}
	public java.util.Map changePassword(java.sql.Connection connection,String userid, String oldpwd, String newpwd, java.sql.Date logdate) {
		return changePassword(connection,null,userid,oldpwd,newpwd,logdate,true);
	}
	public java.util.Map changePassword(java.sql.Connection connection,String site,String userid, String oldpwd, String newpwd, java.sql.Date logdate) {
		return changePassword(connection,site,userid,oldpwd,newpwd,logdate,true);
	}
	public java.util.Map changePassword(java.sql.Connection connection,String site,String userid, String oldpwd, String newpwd, java.sql.Date logdate, boolean history) {
	    java.util.HashMap h = new java.util.HashMap();
	    logondate = logdate;
	    try {	    
	        Trace.debug(this,"Change Password");
	        if (!checkUser(connection,site,userid)) {
		        String errcode = "-3001";
		        String msg = "Invalid User";
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            return h;
	        }
	        if (!checkPassword(connection,site,userid, oldpwd)) {
		        String errcode = "-3002";
		        String msg = "Invalid Password";
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            return h;
	        }
	        if (!getUserInformation(connection,userid)) {
		        String errcode = "-3003";
		        String msg = "Wrong data in table tppwd. Please Contact Admin";
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            return h;
	        }
	        if (checkNotchgpwduntilday(connection,userid)) {
		        String errcode = "-3004";
		        String msg = "You can change Password after " + changedate;
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            h.put("args",changedate);
	            h.put("args0",ht.get("notchgpwduntilday"));
	            return h;
	        }
	        if (checkMinpwdlength(newpwd)) {
		        String errcode = "-3005";
		        String msg = "Please entry length of New Password at least " + (String) ht.get("minpwdlength") + " character.";
	            h.put("result", "false");
	            h.put("msg",msg);
	            h.put("errno",errcode);
	            h.put("args",ht.get("minpwdlength"));
	            return h;
	        }
	        if (checkAlphainpwd(newpwd)) {
		        String errcode = "-3006";
		        String msg = "Please input character at least " + (String) ht.get("alphainpwd") + " character.";
	            h.put("result", "false");
	            h.put("msg",msg);
	            h.put("errno",errcode);
	            h.put("args",ht.get("alphainpwd"));
	            return h;
	        }
	        if (checkOtherinpwd(newpwd)) {
		        String errcode = "-3007";
		        String msg = "Please input special character at least " + (String) ht.get("otherinpwd") + " character.";
	            h.put("result", "false");
	            h.put("msg",msg);
	            h.put("errno",errcode);
	            h.put("args",ht.get("otherinpwd"));
	            return h;
	        }
	        if (checkMaxsamechar(newpwd)) {
		        String errcode = "-3008";
		        String msg = "Please input the same character not more than " + (String) ht.get("maxsamechar") + " character.";
	            h.put("result", "false");
	            h.put("msg",msg);
	            h.put("errno",errcode);
	            h.put("args",ht.get("maxsamechar"));
	            return h;
	        }
	        if (checkMindiffchar(newpwd)) {
		        String errcode = "-3009";
		        String msg = "Please input different character at least " + (String) ht.get("mindiffchar") + " character.";
	            h.put("result", "false");
	            h.put("msg",msg);
	            h.put("errno",errcode);
	            h.put("args",ht.get("mindiffchar"));
	            return h;
	        }
	
	        if (checkMaxarrangechar(newpwd)) {
		        String errcode = "-3010";
		        String msg = "Please input sequential character at least " + (String) ht.get("maxarrangechar") + " character.";
	            h.put("result", "false");
	            h.put("msg",msg);
	            h.put("errno",errcode);
	            h.put("args",ht.get("maxarrangechar"));
	            return h;
	        }
	
	        if (checkReserveword(connection,newpwd)) {
		        String errcode = "-3011";
		        String msg = "New Password cannot be reserve word. Please change New Password";
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            return h;
	        }
	
	        if (checkTimenotusedoldpwd(connection,userid, newpwd)) {	        
		        String errcode = "-3012";
		        String msg = "New Password cannot be the same as Old Password for "+ ht.get("timenotusedoldpwd");
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            h.put("args",ht.get("timenotusedoldpwd"));
	            return h;
	        }
	        updatePassword(connection,site,userid, newpwd, logondate, history);
	        h.put("result", "true");
	    } catch (java.sql.SQLException ex) {
	        Trace.error(this,ex);
	        String msg = ex.getMessage();
	        if(msg==null) msg = ex.getClass().getName();
	        h.put("result", "false");
	        h.put("msg", msg);
	        h.put("errno",""+ex.getErrorCode());
	    } catch (Exception ex) {
	        Trace.error(this,ex);
	        String msg = ex.getMessage();
	        if(msg==null) msg = ex.getClass().getName();
	        h.put("result", "false");
	        h.put("msg", msg);
	    }
	    return h;
	}
	public boolean checkAlphainpwd(String password) {
		if(password==null) {
			return false;
		}
	    String val = (String) ht.get("alphainpwd");
	    int num = parseInt(val);
	    if (num == 0) {
	        return false;
	    }
	    int alpha = 0;
	    for (int i = 0; i < password.length(); i++) {
	        if (Character.isLetter(password.charAt(i))) {
	            alpha++;
	        }
	    }
	    if (alpha >= num) {
	        return false;
	    }
	    return true;
	}
	public boolean checkMaxarrangechar(String password) {
		if(password==null) {
			return false;
		}
	    String val = (String) ht.get("maxarrangechar");
	    int maxarrange = parseInt(val);
	    if (maxarrange == 0) {
	        return false;
	    }
	    int max = 1;
	    int guess = 0;
	    for (int i = 0; i < password.length(); i++) {
	        int c = password.charAt(i);
	        if (c == guess) {
	            max++;
	            guess = c + 1;
	            if (max > maxarrange) {
	                return true;
	            }
	        } else {
	            guess = c + 1;
	            max = 1;
	        }
	    }
	    max = 1;
	    guess = 0;
	    for (int i = 0; i < password.length(); i++) {
	        int c = password.charAt(i);
	        if (c == guess) {
	            max++;
	            guess = c - 1;
	            if (max > maxarrange) {
	                return true;
	            }
	        } else {
	            guess = c - 1;
	            max = 1;
	        }
	    }
	    return false;
	}
	public boolean checkMaxsamechar(String password) {
		if(password==null) {
			return false;
		}
	    String val = (String) ht.get("maxsamechar");
	    int maxchar = parseInt(val);
	    if (maxchar == 0) {
	        return false;
	    }
	    for (int i = 0; i < password.length(); i++) {
	        char c = password.charAt(i);
	        int same = 0;
	        for (int j = 0; j < password.length(); j++) {
	            if (c == password.charAt(j)) {
	                same++;
	            }
	        }
	        if (same > maxchar) {
	            return true;
	        }
	    }
	    return false;
	}
	public boolean checkMindiffchar(String password) {
		if(password==null) {
			return false;
		}
	    String val = (String) ht.get("mindiffchar");
	    int mindiff = parseInt(val);
	    if (mindiff == 0) {
	        return false;
	    }
	    java.util.List v = new java.util.ArrayList();
	    for (int i = 0; i < password.length(); i++) {
	        char c = password.charAt(i);
	        boolean notfound = true;
	        for (int j = 0;(j < v.size() && (notfound)); j++) {
	            Character ch = (Character) v.get(j);
	            if (c == ch.charValue()) {
	                notfound = false;
	            }
	        }
	        if (notfound) {
	            v.add(new Character(c));
	        }
	    }
	    int diff = v.size();
	    if (diff >= mindiff) {
	        return false;
	    }
	    return true;
	}
	public boolean checkMinpwdlength(String password) {
		if(password==null) {
			return false;
		}
	    String val = (String) ht.get("minpwdlength");
	    int len = parseInt(val);
	    if (len == 0) {
	        return false;
	    }
	    int pwdlen = password.length();
	    if (pwdlen < len) {
	        return true;
	    }
	    return false;
	}
	public boolean checkNotchgpwduntilday(java.sql.Connection conn,String userid) throws Exception {
		if(userid==null) {
			return false;
		}
	    String val = (String) ht.get("notchgpwduntilday");
	    int notchg = parseInt(val);
	    if (notchg == 0) {
	        return false;
	    }
	    KnSQL SQL = new KnSQL(this);
	    SQL.append("select max(systemdate) systemdate from tupwd ");
	    SQL.append("where userid = ?userid ");
	    SQL.setParameter("userid",userid);
	    ResultSet rs = SQL.executeQuery(conn);
	    java.sql.Date systemdate = null;
	    if (rs.next()) {
	        systemdate = rs.getDate("systemdate");
	    }
	    if (systemdate == null) {
	        return false;
	    }
	    if(logondate==null) logondate = new java.sql.Date(System.currentTimeMillis());
	    BeanUtility util = new BeanUtility(this);
	    java.util.Date chgdate = util.plusDate(systemdate, notchg);
	    changedate = util.formatDateTime(chgdate, "dd/MM/yyyy");
	    if (util.compareDate(chgdate, logondate) < 0) {
	        return false;
	    }
	    return true;
	}
	public boolean checkOtherinpwd(String password) {
		if(password==null) {
			return false;
		}
	    String val = (String) ht.get("otherinpwd");
	    int num = parseInt(val);
	    if (num == 0) {
	        return false;
	    }
	    int other = 0;
	    for (int i = 0; i < password.length(); i++) {
	        if (!Character.isLetter(password.charAt(i)))
	            other++;
	    }
	    if (other >= num) {
	        return false;
	    }
	    return true;
	}
	public java.util.Map checkPassword(java.sql.Connection c, String userid, String oldpwd, java.sql.Date logon) {
		return checkPassword(c,null,userid,oldpwd,logon);
	}
	public boolean checkPassword(java.sql.Connection conn,String site,String userid, String pwd) throws Exception {
	    KnSQL SQL = new KnSQL(this);
	    SQL.append("select userpassword from tuser ");
	    SQL.append("where userid = ?userid ");
	    if(site!=null && site.length()>0) SQL.append("and site = '"+site+"' ");
	    SQL.setParameter("userid",userid);
	    try(ResultSet rs = SQL.executeQuery(conn)) {
		    if (rs.next()) {
		        String dbpwd = rs.getString("userpassword");
		        Console.out.println("db password = "+dbpwd+" == "+encryptPassword(pwd));
		        if(dbpwd!=null) {	      
		        	if(dbpwd.equals(pwd) || dbpwd.equals(encryptPassword(pwd))) {
		            	return true;
		        	}
		        } else {
			    	if(pwd==null || pwd.equals("")) {
			    		return true;
			    	}
			    	return false;
		        }
		    }
	    }
	    return false;
	}
	public java.util.Map checkPassword(java.sql.Connection conn, String site,String userid, String oldpwd, java.sql.Date logon) {
	    java.util.HashMap h = new java.util.HashMap();
	    logondate = logon;
	    try {
	        Trace.debug(this,"Change Password");
	        if (!checkUser(conn,site,userid)) {
		        String errcode = "-3001";
		        String msg = "Invalid user";
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            return h;
	        }
	        if (!checkPassword(conn,site,userid, oldpwd)) {
		        String errcode = "-3002";
		        String msg = "Invalid password";
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            return h;
	        }
	        if (!getUserInformation(conn,userid)) {
		        String errcode = "-3003";
		        String msg = "Wrong data in table tppwd. Please Contact Admin";
	            h.put("result", "false");
	            h.put("msg", msg);
	            h.put("errno",errcode);
	            return h;
	        }
	        h.put("result", "true");
	    } catch (java.sql.SQLException ex) {
	        Trace.error(this,ex);
	        String msg = ex.getMessage();
	        if(msg==null) msg = ex.getClass().getName();
	        h.put("result", "false");
	        h.put("msg", msg);
	        h.put("errno",""+ex.getErrorCode());
	    } catch (Exception ex) {
	        Trace.error(this,ex);
	        String msg = ex.getMessage();
	        if(msg==null) msg = ex.getClass().getName();
	        h.put("result", "false");
	        h.put("msg", msg);
	    }
	    return h;
	}
	public boolean checkPassword(String pwd,String storedPwd) {
		return BCrypt.checkpw(pwd, storedPwd);
	}
	public boolean checkReserveword(java.sql.Connection conn,String password) throws Exception {
		if(password==null) { return false; }
	    String val = (String) ht.get("checkreservepwd");
	    if (val!=null && val.charAt(0) == '0') {
	        return false;
	    }
	    KnSQL SQL = new KnSQL(this);
	    SQL.append("select * from trpwd ");
	    SQL.append("where reservepwd = ?password ");
	    SQL.setParameter("password",password);
	    try(ResultSet rs = SQL.executeQuery(conn)) {
	    	if (rs.next()) {
	    		return true;
	    	}
	    }
	    return false;
	}
	public boolean checkTimenotusedoldpwd(java.sql.Connection conn,String userid, String password) throws Exception {
		if(password==null) {
			return false;
		}
		if(userid==null) {
			return false;
		}
	    String val = (String) ht.get("timenotusedoldpwd");
	    int notused = parseInt(val);
	    if (notused == 0) {
	        return false;
	    }
	    boolean duplicate = false;
	    int time = 0;
	    KnSQL SQL = new KnSQL(this);
	    SQL.append("select * from tupwd ");
	    SQL.append("where userid = ?userid ");
	    SQL.setParameter("userid",userid);
	    SQL.append("order by serverdatetime desc ");
	    try(ResultSet rs = SQL.executeQuery(conn)) {
		    while (rs.next() && !duplicate) {
			    String dbpwd = rs.getString("userpassword");
			    if(dbpwd!=null) {
		        	if (encryptPassword(password).equals(dbpwd)) {
		            	duplicate = true;
		        	} else {
		            	time++;
		        	}
			    }
		    }
	    }
	    if (duplicate && (time < notused)) {
	        return true;
	    }
	    return false;
	}
	public boolean checkUser(java.sql.Connection conn,String site,String userid) throws Exception {
		boolean result = false;
		if(userid==null) {
			return result;	
		}
	    KnSQL SQL = new KnSQL(this);
	    SQL.append("select userid from tuser ");
	    SQL.append("where userid = ?userid ");
	    if(site!=null && site.length()>0) SQL.append("and site = '"+site+"' ");
	    SQL.setParameter("userid",userid);
	    try(ResultSet rs = SQL.executeQuery(conn)) {
		    if(rs.next()) {
			    result = true;
		    }
	    }
	    return result;
	}
	public String encrypt(String pwd) {
		return encrypt(pwd,null);
	}
	public String encrypt(String pwd,String salt) {
		if(pwd==null || pwd.trim().length()==0) {
			return pwd;
		}
		if(salt==null || salt.trim().length()==0) {
			salt = BCrypt.gensalt();
		}
		return BCrypt.hashpw(pwd, salt);
	}
	public String encryptPassword(String pwd) {
		return encrypt(pwd);
	}
	public String fetchVersion() {
		return super.fetchVersion()+PasswordLibrary.class+"=$Revision: 1.6 $\n";
	}
	public java.util.Map getHashtable() {
		return ht;
	}
	public java.sql.Date getUserExpireDate(java.sql.Connection conn,String userid,java.sql.Date expiredate) throws Exception {
		if(ht==null || ht.isEmpty()) {
			getUserInformation(conn,userid);
		}
	    BeanUtility util = new BeanUtility(this);
	    java.sql.Date systemdate = expiredate;
	    if(systemdate==null) {
	    	systemdate = BeanUtility.getCurrentDate();
	    }
	    String val = ht==null?"120":(String) ht.get("pwdexpireday");
	    java.util.Date edate = util.plusDate(systemdate, val==null?120:Integer.parseInt(val));
	    return new java.sql.Date(edate.getTime());		
	}
	public boolean getUserInformation(java.sql.Connection conn,String userid) throws Exception {
		if(userid==null) {
			return false;
		}
	    ht = new java.util.HashMap();
	    KnSQL SQL = new KnSQL(this);
	    SQL.append("select * from tppwd ");
	    SQL.append("where userid = ?userid ");
	    SQL.setParameter("userid",userid);
	    try(ResultSet rs = SQL.executeQuery(conn)) {
		    if (rs.next()) {
		        ht.put("checkreservepwd", rs.getString("checkreservepwd"));
		        ht.put("timenotusedoldpwd", rs.getString("timenotusedoldpwd"));
		        ht.put("notchgpwduntilday", rs.getString("notchgpwduntilday"));
		        ht.put("minpwdlength", rs.getString("minpwdlength"));
		        ht.put("alphainpwd", rs.getString("alphainpwd"));
		        ht.put("otherinpwd", rs.getString("otherinpwd"));
		        ht.put("maxsamechar", rs.getString("maxsamechar"));
		        ht.put("mindiffchar", rs.getString("mindiffchar"));
		        ht.put("maxarrangechar", rs.getString("maxarrangechar"));
		        ht.put("pwdexpireday", Integer.toString(rs.getInt("pwdexpireday")));
		        return true;
		    } 
	    }
	    SQL.clear();
	    SQL.append("select * from tppwd ");
	    SQL.append("where userid = 'DEFAULT' ");
	    try(java.sql.ResultSet rs = SQL.executeQuery(conn)) {
	        if (rs.next()) {
	            ht.put("checkreservepwd", rs.getString("checkreservepwd"));
	            ht.put("timenotusedoldpwd", rs.getString("timenotusedoldpwd"));
	            ht.put("notchgpwduntilday", rs.getString("notchgpwduntilday"));
	            ht.put("minpwdlength", rs.getString("minpwdlength"));
	            ht.put("alphainpwd", rs.getString("alphainpwd"));
	            ht.put("otherinpwd", rs.getString("otherinpwd"));
	            ht.put("maxsamechar", rs.getString("maxsamechar"));
	            ht.put("mindiffchar", rs.getString("mindiffchar"));
	            ht.put("maxarrangechar", rs.getString("maxarrangechar"));
	            ht.put("pwdexpireday", Integer.toString(rs.getInt("pwdexpireday")));
	            return true;
	        }
	    }
	    return false;
	}
	public boolean insertHistory(java.sql.Connection connection,String userid, String password,java.sql.Timestamp serverdate,java.sql.Date systemdate,String editor) throws Exception {
		if(connection==null) {
			return false;
		}
		if(userid==null) {
			return false;
		}
		KnSQL SQL = new KnSQL(this);
	    SQL.append("insert into tupwd (serverdatetime,systemdate,userid,userpassword,edituserid) ");
	    SQL.append("values(?serverdatetime,?systemdate,?userid,?userpassword,?edituserid) ");
	   	if(serverdate==null) serverdate = new java.sql.Timestamp(System.currentTimeMillis());
	   	if(systemdate==null) systemdate = new java.sql.Date(System.currentTimeMillis());
	    SQL.setParameter("serverdatetime",serverdate);
	    SQL.setParameter("systemdate",systemdate);
	    SQL.setParameter("userid",userid);
	    SQL.setParameter("userpassword",password);
	    SQL.setParameter("edituserid",editor);
	    SQL.executeUpdate(connection);
	    return true;
	}
	public boolean updatePassword(java.sql.Connection conn,String userid, String password) throws Exception {
		return updatePassword(conn,null,userid,password,null);
	}
	public boolean updatePassword(java.sql.Connection conn,String userid, String password,java.sql.Date expiredate) throws Exception {
		return updatePassword(conn,null,userid,password,expiredate,true);
	}
	public boolean updatePassword(java.sql.Connection conn,String userid, String password,java.sql.Date expiredate,boolean history) throws Exception {
		return updatePassword(conn,null,userid,password,expiredate,history);
	}
	public boolean updatePassword(java.sql.Connection conn,String site,String userid, String password) throws Exception {
		return updatePassword(conn,site,userid,password,null,true);
	}
	public boolean updatePassword(java.sql.Connection conn,String site,String userid, String password,java.sql.Date expiredate) throws Exception {
		return updatePassword(conn,site,userid,password,expiredate,true);
	}
	public boolean updatePassword(java.sql.Connection conn,String site,String userid, String password,java.sql.Date expiredate,boolean history) throws Exception {
		if(userid==null) {
			throw new SQLException("User is undefined","userid",-4008);
		}
		if(password==null) {
			throw new SQLException("Password is undefined","password",-4009);
		}
	    java.sql.Date systemdate = expiredate;
	    if(systemdate==null) {
	    	systemdate = BeanUtility.getCurrentDate();
	    }
	    java.sql.Date expdate = getUserExpireDate(conn,userid,expiredate);
		String encpwd = encryptPassword(password);
	    KnSQL SQL = new KnSQL(this);
	    SQL.append("update tuser set userpassword = '" + encpwd + "',");
	    SQL.append("passwordexpiredate = ?expiredate ");
	    SQL.append(", changeflag = '1' ");
	    SQL.append("where userid = ?userid ");
	    if(site!=null && site.length()>0) {
	    	SQL.append("and site = '"+site+"' ");
	    }
	    SQL.setParameter("userid",userid);
	    SQL.setParameter("expiredate",expdate);
	    SQL.executeUpdate(conn);
	    if(!history) {
	    	return true;
	    }
	    return insertHistory(conn,userid,encpwd,new java.sql.Timestamp(System.currentTimeMillis()),systemdate,userid);
	}
}
