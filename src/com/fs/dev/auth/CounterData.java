package com.fs.dev.auth;

/*
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: CounterData.java
 * Description: CounterData class implements for handle tus data base schema.
 * Version: $Revision: 1.2 $
 * Creation date: Fri Sep 30 13:43:06 GMT+07:00 2005
 */
/**
 * CounterData class implements for handle tus data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.misc.*;

//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings("serial")
public class CounterData extends BeanData {
	//#another methods defined drive you crazy
	//#(100000) programmer code begin;
	//#(100000) programmer code end;
	//#defined & declaration the real thing
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
	
	public CounterData() {
		super();
	}
	public void fetchResult(java.sql.ResultSet rs) throws java.sql.SQLException {
		super.fetchResult(rs);
		setUserid(rs.getString("userid"));
		setCounter(rs.getInt("loginfailtimes"));
		//#fetching other result desire
		//#(60000) programmer code begin;
		//#(60000) programmer code end;
	}
	public String fetchVersion() {
		return super.fetchVersion()+CounterData.class+"=$Revision: 1.2 $\n";
	}
	public int getCounter() {
		return getInt("counter");
	}
	public String getLockflag() {
		return getString("lockflag");
	}
	public String getUserid() {
		return getString("userid");
	}
	protected void initialize() {
		super.initialize();
		setTable("tuser");
		addSchema("userid",java.sql.Types.VARCHAR);
		addSchema("counter",java.sql.Types.INTEGER);
		map("userid","user");
		map("counter","counter");
		//#intialize how deep is your love 
		//#(30000) programmer code begin;
		addSchema("lockflag",java.sql.Types.VARCHAR);
		map("lockflag","lockflag");
		//#(30000) programmer code end;
	}
	public boolean obtain(BeanSchemaInterface bean) throws Exception {
		if(bean==null) return super.obtain(bean);
		setUserid(bean.getFieldByName(mapper("userid")).asString());
		setCounter(bean.getFieldByName(mapper("counter")).asInt());
		//#obtain it perfect moment
		//#(40000) programmer code begin;
		setLockflag(bean.getFieldByName(mapper("lockflag")).asString());
		//#(40000) programmer code end;
		return super.obtain(bean);
	}
	public void setCounter(int newCounter) {
		setMember("counter",newCounter);
	}
	public void setLockflag(String newLockflag) {
		setMember("lockflag",newLockflag);
	}
	public void setUserid(String newUserid) {
		setMember("userid",newUserid);
	}
	public int update(java.sql.Connection connection) throws Exception {
		//#begin with update statement going on
		//#(290000) programmer code begin;
		if(getLockflag()!=null && !getLockflag().equals("")) {
			LockData lock = new LockData();
			lock.setRelatable(this);
			lock.setUserid(getUserid());
			lock.setLockflag(getLockflag());
			lock.update(connection);
		}
		//#(290000) programmer code end;
		KnSQL sql = new KnSQL(this);
		sql.append("update tuser set ");
		sql.append("loginfailtimes = loginfailtimes + ?loginfailtimes ");	sql.append("where ");
		sql.append("username=?username ");
		//#any update statement over protected
		//#(150000) programmer code begin;
		//#(150000) programmer code end;
		sql.setParameter("loginfailtimes",getCounter());
		sql.setParameter("username",getUserid());
		//#assigned parameters all rise
		//#(160000) programmer code begin;
		//#(160000) programmer code end;
		return sql.executeUpdate(connection);
		//#ending with delete statement going on
		//#(300000) programmer code begin;
		//#(300000) programmer code end;
	}
}
