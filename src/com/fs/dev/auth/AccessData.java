package com.fs.dev.auth;
/*
 * SCCS id: $Id$
 * System Name: Internet Securities Backoffice System
 * Copyright: Freewill Solutions Co., Ltd.
 * Program id: ActivateData.java
 * Description: ActivateData class implements for handle tactivate data base schema.
 * Version: $Revision$
 * Creation date: Mon Oct 21 12:29:54 ICT 2013
 */
/**
 * ActivateData class implements for handle tactivate data base schema.
 */
import com.fs.bean.ctrl.*;
import com.fs.bean.gener.*;
import com.fs.bean.misc.*;
import java.sql.*;
//#import anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
@SuppressWarnings({"serial","rawtypes"})
public class AccessData extends BeanData {
	//#defined & declaration the real thing
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
	public AccessData() {
		super();
	}
	protected void initialize() {
		super.initialize();
		setTable("tuser");
		addSchema("accessor",java.sql.Types.VARCHAR);
		addSchema("accessdate",java.sql.Types.DATE);
		addSchema("accesstime",java.sql.Types.TIME);
		addSchema("accesshits",java.sql.Types.BIGINT);
		//#intialize how deep is your love 
		//#(30000) programmer code begin;
		map("accessor","accessor");
		map("accessdate","accessdate");
		map("accesstime","accesstime");
		map("accesshits","accesshits");
		//#(30000) programmer code end;
	}
	public String fetchVersion() {
		return super.fetchVersion()+AccessData.class+"=$Revision$\n";
	}
	public String getAccessor() {
		return getString("accessor");
	}
	public void setAccessor(String newAccessor) {
		setMember("accessor",newAccessor);
	}
	public Date getAccessdate() {
		return getDate("accessdate");
	}
	public void setAccessdate(Date newAccessdate) {
		setMember("accessdate",newAccessdate);
	}
	public Time getAccesstime() {
		return getTime("accesstime");
	}
	public void setAccesstime(Time newAccesstime) {
		setMember("accesstime",newAccesstime);
	}
	public long getAccesshits() {
		return getLong("accesshits");
	}
	public void setAccesshits(long newAccesshits) {
		setMember("accesshits",newAccesshits);
	}
	public boolean obtain(BeanSchemaInterface bean) throws Exception {
		if(bean==null) return super.obtain(bean);
		setAccessor(bean.getFieldByName(mapper("accessor")).asString());
		setAccessdate(bean.getFieldByName(mapper("accessdate")).asDate());
		setAccesstime(bean.getFieldByName(mapper("accesstime")).asTime());
		setAccesshits(bean.getFieldByName(mapper("accesshits")).asLong());
		//#obtain it perfect moment
		//#(40000) programmer code begin;
		//#(40000) programmer code end;
		return super.obtain(bean);
	}
	protected void assignParameters(ExecuteStatement sql) throws Exception {
		if(sql==null) return;
		//#Everything I do, I do it for you
		//#(75000) programmer code begin;
		if(getAccessdate()==null) setAccessdate(new java.sql.Date(System.currentTimeMillis()));
		if(getAccesstime()==null) setAccesstime(new java.sql.Time(System.currentTimeMillis()));
		if(getAccesshits()<=0) setAccesshits(1);
		//#(75000) programmer code end;
		sql.setParameter("userid",getAccessor());
		sql.setParameter("accessdate",getAccessdate());
		sql.setParameter("accesstime",getAccesstime());
		sql.setParameter("accesshits",getAccesshits());
		//#I'm gonna be around you
		//#(77000) programmer code begin;
		//#(77000) programmer code end;
	}
	public int update(java.sql.Connection connection,java.sql.Connection centerConnection,java.sql.Connection globalConnection,java.util.Map transientVar) throws Exception {
		//#begin with update statement going on
		//#(290000) programmer code begin;	
		//#(290000) programmer code end;
		ExecuteStatement sql = new KnSQL(this);
		sql.append("update tuser set accessdate=?accessdate, accesstime=?accesstime, accesshits = accesshits + ?accesshits, mistakens = 0, mistakentime = 0 ");
		sql.append("where userid=?userid ");
		//#any update statement over protected
		//#(150000) programmer code begin;
		//#(150000) programmer code end;
		assignParameters(sql);
		//#assigned parameters all rise
		//#(160000) programmer code begin;
		//#(160000) programmer code end;
		return sql.executeUpdate(connection);
		//#ending with delete statement going on
		//#(300000) programmer code begin;
		//#(300000) programmer code end;
	}
	//#another methods defined drive you crazy
	//#(100000) programmer code begin;
	//#(100000) programmer code end;
}
