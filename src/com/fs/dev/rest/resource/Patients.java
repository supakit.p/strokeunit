package com.fs.dev.rest.resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.fs.dev.strok.service.Patient;

@Path("Patients")
public class Patients extends RestManager {
	java.util.List<Patient> patlist = new java.util.ArrayList<Patient>();
	{
		patlist.add(new Patient("00000001","1234567890123","1111111111","นาย","สมชาย","นะครับ","ชาย","25320101","ไทย"));
		patlist.add(new Patient("00000002","1234567890000","1111111112","นาย","สมชาย","นะฮ้า","ไม่ระบุ","25020101","ไทย"));
		patlist.add(new Patient("00000003","0123123456789","1111111113","นาย","บุตร","เข้มแข็ง","ชาย","25430101","ไทย"));
		patlist.add(new Patient("00000004","0000000444444","1111111114","นางสาว","ราตรี","สีสว่าง","หญิง","24750101","ไทย"));
		patlist.add(new Patient("00000005","0000000555555","1111111115","นาง","มาดี","มาแล้ว","หญิง","25120101","ไทย"));
		patlist.add(new Patient("11223344","1111222233333","1111111116","นางสาว","ดอกไม้","สดใส","หญิง","25301101","ไทย"));
		patlist.add(new Patient("12345678","1234567812345","1111111117","เด็กหญิง","มะลิ","หอมกรุ่น","หญิง","25590101","ไทย"));
		patlist.add(new Patient("22334455","2233445522334","1111111118","นาง","กำไร","เงินมาก","หญิง","24820101","ไทย"));
		patlist.add(new Patient("77112233","1123445567889","1111111119","Mr.","Testor","Test","ชาย","25190101","ไทย"));

		patlist.add(new Patient("33445566","3344556611223","3344556611","นาง","มะกรูด","ลำไย","หญิง","25070402","ไทย"));
		patlist.add(new Patient("44556677","4455667711223","4455667711","นางสาว","แก้ว","มังกร","หญิง","25450301","ไทย"));
		patlist.add(new Patient("55667788","5566778811223","5566778811","นาย","ภูผา","มหาสมุทร","ชาย","25440401","ไทย"));
		patlist.add(new Patient("66778899","6677889911223","6677889911","Mr.","Testor 01","Test1","ชาย","25290101","ไทย"));
		patlist.add(new Patient("77889900","7788990011223","7788990011","Mrs.","Testor 02","Test2","ชาย","25100101","ไทย"));
				
		patlist.add(new Patient("50322265","2223445567889","2223445567","Mrs.","Suvapan","Tunsakul","หญิง","25190101","ไทย"));	
		patlist.add(new Patient("47501384","3323445567889","3323445567","นาย","สุริยะ","ชื่นมะนา","ชาย","25110101","ไทย"));		
		patlist.add(new Patient("45501424","4423445567889","4423445567","นาย","ลำพูน","ศรีโปดก","ชาย","24800101","ไทย"));		
		patlist.add(new Patient("44074303","0123445567889","1023445567","นางสาว.","วิยะดา","เกิดเกตุปิ่น","หญิง","25020101","ไทย"));		
		patlist.add(new Patient("53603086","5432154321000","5432154321","นาย","จู","ศิริขวัญชัย(คูหาสันติสุข)","ไม่ระบุ","25600101","ไทย"));
		
	}
	public Patients() {
		// TODO Auto-generated constructor stub
	}
    @GET
	@Path("/hn/{pid}/{subpid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String doGetHN(@PathParam("pid") String pid,@PathParam("subpid") String subpid,@Context HttpServletRequest request, @Context HttpServletResponse response) {
		allowCrossOriginResourceSharing(response);
		response.setContentType("text/html; charset=UTF-8");
		for(Patient p : patlist) {
			if(p.getHN().equals(pid)) {
				return p.toJSONString();
			}
		}
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 1,");
    	buf.append("\"StatusDescription\": \"Record not found\"");
    	buf.append("}");    	
		return buf.toString();
		/*
    	long cid = System.currentTimeMillis();
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 0,");
    	buf.append("\"StatusDescription\": \"Success\",");
    	buf.append("\"HN\": \""+pid+"\",");
    	buf.append("\"CitizenId\": \""+cid+"\",");
    	buf.append("\"PassportNo\": \"\",");
    	buf.append("\"PrefixName\": \"Mr.\",");
    	buf.append("\"FirstName\": \"Test\",");
    	buf.append("\"LastName\": \"Test\",");
    	buf.append("\"Gender\": \"Male\",");
    	buf.append("\"DateOfBirth\": \"25200801\",");
    	buf.append("\"Nationality\": \"Thai\"");
    	buf.append("}");    	
    	return buf.toString();
    	*/
    }
    @GET
	@Path("/pid/{pid}/{subpid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String doGetPid(@PathParam("pid") String pid,@PathParam("subpid") String subpid,@Context HttpServletRequest request, @Context HttpServletResponse response) {
		allowCrossOriginResourceSharing(response);
		response.setContentType("text/html; charset=UTF-8");
		for(Patient p : patlist) {
			if(p.getCitizenId().equals(pid)) {
				return p.toJSONString();
			}
		}
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 1,");
    	buf.append("\"StatusDescription\": \"Record not found\"");
    	buf.append("}");    	
		return buf.toString();
		/*
    	long cid = System.currentTimeMillis();
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 0,");
    	buf.append("\"StatusDescription\": \"Success\",");
    	buf.append("\"HN\": \""+cid+"\",");
    	buf.append("\"CitizenId\": \""+pid+"\",");
    	buf.append("\"PassportNo\": \"\",");
    	buf.append("\"PrefixName\": \"Mr.\",");
    	buf.append("\"FirstName\": \"Test\",");
    	buf.append("\"LastName\": \"Test\",");
    	buf.append("\"Gender\": \"Male\",");
    	buf.append("\"DateOfBirth\": \"25200801\",");
    	buf.append("\"Nationality\": \"Thai\"");
    	buf.append("}");    	
    	return buf.toString();
    	*/
    }

}
