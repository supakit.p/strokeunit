package com.fs.dev.rest.resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import com.fs.bean.util.*;
import com.fs.dev.strok.service.Employee;
import java.net.URLDecoder;

@Path("RegEmp")
public class Employees extends RestManager {
	java.util.List<Employee> emplist = new java.util.ArrayList<Employee>();
	{/*
		emplist.add(new Employee("ER Doctor","ว.11000001","11000001","D001"));
		emplist.add(new Employee("Neurologist","ว.12000001","12000001","D002"));
		emplist.add(new Employee("Interventionist","ว.13000001","13000001","D003"));
		emplist.add(new Employee("Anesthesiologist","ว.14000001","14000001","D004"));
		emplist.add(new Employee("ER Nurse","ว.21000001","21000001","N001"));
		emplist.add(new Employee("Stroke Unit Nurse","ว.22000001","22000001","7"));
		
		emplist.add(new Employee("ER Doctor","ว.11000001","11000000","D001"));
		emplist.add(new Employee("Neurologist","ว.12000001","12000000","D002"));
		emplist.add(new Employee("Interventionist","ว.13000001","13000000","D003"));
		emplist.add(new Employee("Anesthesiologist","ว.14000001","14000000","D004"));
		emplist.add(new Employee("ER Nurse","ว.21000001","21000000","N001"));
		emplist.add(new Employee("Stroke Unit Nurse","ว.22000001","22000000","N002"));
		emplist.add(new Employee("สมชาย รักษาดี","ว.123456","33333333","D002"));
		emplist.add(new Employee("สมหญิง สุดสวย","ว.222000","33330000","N001"));
		emplist.add(new Employee("Neurologist","ว.456789","44444444","D002"));
		
		emplist.add(new Employee("ER Doctor","ว.011111","11111111","D001"));
		emplist.add(new Employee("Neurologist","ว.022222","22222222","D002"));
		emplist.add(new Employee("Interventionist","ว.033333","33333333","D003"));
		emplist.add(new Employee("Anesthesiologist","ว.044444","44444444","D004"));
		emplist.add(new Employee("ER Nurse","ว.055555","55555555","N001"));
		emplist.add(new Employee("Stroke Unit Nurse","ว.066666","66666666","N002"));
		emplist.add(new Employee("Interventionist2","ว.030303","03333333","D003"));
		emplist.add(new Employee("Anesthesiologist2","ว.040404","04444444","D004"));
		emplist.add(new Employee("Interventionist3","ว.0133333","13333333","D003"));
		emplist.add(new Employee("Anesthesiologist3","ว.014444","14444444","D004"));
	*/
		emplist.add(new Employee("ER Doctor","ว.11000001","11000001","D001"));
		emplist.add(new Employee("Neurologist","ว.12000001","12000001","D002"));
		emplist.add(new Employee("Interventionist","ว.13000001","13000001","D003"));
		emplist.add(new Employee("Anesthesiologist","ว.14000001","14000001","D004"));
		emplist.add(new Employee("ER Nurse","ว.21000001","21000001","N001"));
		emplist.add(new Employee("สมชาย  สบายดี","ว.123456","33333333","D002"));
		emplist.add(new Employee("ER Doctor","ว.11000001","11000000","D001"));
		emplist.add(new Employee("Neurologist","ว.12000001","12000000","D002"));
		emplist.add(new Employee("Interventionist","ว.13000001","13000000","D003"));
		emplist.add(new Employee("Anesthesiologist","ว.14000001","14000000","D004"));
		emplist.add(new Employee("ER Nurse","ว.21000001","21000000","N001"));
		emplist.add(new Employee("Stroke Unit Nurse","ว.22000001","22000000","N002"));
		emplist.add(new Employee("สมชาย  รักษาดี","ว.123456","33333333","D002"));
		emplist.add(new Employee("สมหญิง  สุดสวย","ว.222000","33330000","N001"));
		emplist.add(new Employee("Neurologist","ว.456789","44444444","D002"));
		emplist.add(new Employee("เด่นชัย โด่งดัง","ว.011111","11111111","D001"));
		emplist.add(new Employee("Neurologist","ว.022222","22222222","D002"));
		emplist.add(new Employee("สมหมาย สุขใจ","ว.033333","33333333","D003"));
		emplist.add(new Employee("Anesthesiologist","ว.044444","44444444","D004"));
		emplist.add(new Employee("หญิงเล็ก จิ๋วหวิ๋ว","ว.055555","55555555","N001"));
		emplist.add(new Employee("ชายกลาง สว่างวงศ์","ว.066666","66666666","N002"));
		emplist.add(new Employee("สมพร ชัยโย","ว.030303","03333333","D003"));
		emplist.add(new Employee("สมหวัง ดั่งใจปอง","ว.040404","04444444","D004"));
		emplist.add(new Employee("สมปรารถนา รักจัง","ว.0133333","13333333","D003"));
		emplist.add(new Employee("ใจดี ดีใจ","ว.014444","14444444","D004"));
		
		emplist.add(new Employee("รศ. นพ. ทวีศักดิ์ เอื้อบุญญาวัฒน์","22886","10011347","D003"));
		emplist.add(new Employee("รรศ. นพ. ยงชัย นิละนนท์","18511","10005269","D002"));
		emplist.add(new Employee("รนพ. สุทธิชัย แซ่เฮ้ง","","10023170","D003"));
		emplist.add(new Employee("รอ. นพ. กิตติพงษ์ สุจิรัตนวิมล","","10003607","D004"));
		emplist.add(new Employee("รน.ส. มนันชยา กองเมืองปัก","","10010816","N002"));
	}
	public Employees() {
		// TODO Auto-generated constructor stub
	}
    @GET
	@Path("/MDNumber/{pid}/{subpid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String doGetMD(@PathParam("pid") String pid,@PathParam("subpid") String subpid,@Context HttpServletRequest request, @Context HttpServletResponse response) {
		allowCrossOriginResourceSharing(response);
		response.setContentType("text/html; charset=UTF-8");
		for(Employee e : emplist) {
			if(e.getMdNumber().equals(pid)) {
				return e.toJSONString();
			}
		}
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 1,");
    	buf.append("\"StatusDescription\": \"Record not found\"");
    	buf.append("}");    	
		return buf.toString();
		/*
    	long cid = System.currentTimeMillis();
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 0,");
    	buf.append("\"StatusDescription\": \"Success\",");
    	buf.append("\"EmpName\": \"Mr. Tester Test\",");
    	buf.append("\"MDNumber\": "+pid+",");
    	buf.append("\"SAPNumber\": \""+cid+"\",");
    	buf.append("\"AuthorityType\": \"D001\"");
    	buf.append("}");    	
    	return buf.toString();
    	*/
    }
    @GET
	@Path("/SAPID/{pid}/{subpid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String doGetSAP(@PathParam("pid") String pid,@PathParam("subpid") String subpid,@Context HttpServletRequest request, @Context HttpServletResponse response) {
		allowCrossOriginResourceSharing(response);
		response.setContentType("text/html; charset=UTF-8");
		for(Employee e : emplist) {
			if(e.getSapNumber().equals(pid)) {
				return e.toJSONString();
			}
		}
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 1,");
    	buf.append("\"StatusDescription\": \"Record not found\"");
    	buf.append("}");    	
		return buf.toString();
		/*
    	long cid = System.currentTimeMillis();
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 0,");
    	buf.append("\"StatusDescription\": \"Success\",");
    	buf.append("\"EmpName\": \"Mr. Testing Test\",");
    	buf.append("\"MDNumber\": "+cid+",");
    	buf.append("\"SAPNumber\": \""+pid+"\",");
    	buf.append("\"AuthorityType\": \"D001\"");
    	buf.append("}");    	
    	return buf.toString();
    	*/
    }
    @GET
	@Path("/EmpName/{pid}/{subpid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String doGetName(@PathParam("pid") String pid,@PathParam("subpid") String subpid,@Context HttpServletRequest request, @Context HttpServletResponse response) throws Exception {
		BeanUtility butil = new BeanUtility();
		allowCrossOriginResourceSharing(response);
		response.setContentType("text/html; charset=UTF-8");
		java.util.List<Employee> results = new java.util.ArrayList<Employee>(); 
		System.out.println("raw data = "+pid);
		//pid = URLDecoder.decode(pid,"UTF-8");
		for(Employee e : emplist) {
			System.out.println("getEmpName = "+e.getEmpName());
			
			if(e.getEmpName().indexOf(pid)>=0) {
				results.add(e);
			}
		}
		if(results.size()>0) {
	    	StringBuilder buf = new StringBuilder("[");
			for(int i=0,isz=results.size();i<isz;i++) {
				Employee e = results.get(i);
				if(i>0) buf.append(",");
				buf.append(e.toJSONString());
			}
	    	buf.append("]");
	    	return buf.toString();
		}
    	StringBuilder buf = new StringBuilder();
    	buf.append("{\"Status\": 1,");
    	buf.append("\"StatusDescription\": \"Record not found\"");
    	buf.append("}");    	
		return buf.toString();
		/*
		long cid = System.currentTimeMillis();
    	StringBuilder buf = new StringBuilder("[");
    	buf.append("{\"Status\": 0,");
    	buf.append("\"StatusDescription\": \"Success\",");
    	buf.append("\"EmpName\": \"Mr."+pid+" Test\",");
    	buf.append("\"MDNumber\": "+cid+",");
    	buf.append("\"SAPNumber\": \"10099999\",");
    	buf.append("\"AuthorityType\": \"D001\"");
    	buf.append("}");   
    	buf.append("]");
    	return buf.toString();
    	*/
    }
}
