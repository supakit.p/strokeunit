package com.fs.dev;

/**
 * @author tassun_oro
 *
 */
public class Content {
	private String enKey;
	private String enValue;
	private String thKey;
	private String thValue;
	public Content() {
		super();
	}
	public Content(String enKey,String enValue,String thKey,String thValue) {
		this.enKey = enKey;
		this.enValue = enValue;
		this.thKey = thKey;
		this.thValue = thValue;
	}
	public static Content emptyContent() {
		return new Content("","","","");
	}
	public String getEnKey() {
		return enKey;
	}
	public String getEnValue() {
		return enValue;
	}
	public String getThKey() {
		return thKey;
	}
	public String getThValue() {
		return thValue;
	}
	public void setEnKey(String enKey) {
		this.enKey = enKey;
	}
	public void setEnValue(String enValue) {
		this.enValue = enValue;
	}
	public void setThKey(String thKey) {
		this.thKey = thKey;
	}
	public void setThValue(String thValue) {
		this.thValue = thValue;
	}
}
