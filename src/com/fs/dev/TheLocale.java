package com.fs.dev;

import com.fs.bean.util.GlobalVariable;

/**
 * @author tassun_oro
 *
 */
public class TheLocale {	
	public TheLocale() {
		super();
	}
	
	public static java.text.SimpleDateFormat getDateFormat(String language) {
		return new java.text.SimpleDateFormat(getDatePicture(),getLocale(language));
	}

	public static String getDatePicture() {
		Object picture = GlobalVariable.getVariable("API_DATE_PICTURE");
		if(picture!=null && picture.toString().trim().length()>0) {
			return picture.toString();
		}
		return "d MMM yyyy";
	}
	
	public static java.util.Locale getLocale(String language) {
		if("EN".equalsIgnoreCase(language)) return java.util.Locale.US;
		if("TH".equalsIgnoreCase(language)) return new java.util.Locale("th","TH");
		if("VN".equalsIgnoreCase(language)) return new java.util.Locale("vi","VN");
		if("CN".equalsIgnoreCase(language)) return java.util.Locale.CHINA;
		if("JP".equalsIgnoreCase(language)) return java.util.Locale.JAPAN;
		if("JA".equalsIgnoreCase(language)) return java.util.Locale.JAPANESE;
		return java.util.Locale.US;
	}
	
	public static java.text.SimpleDateFormat getMonthFormat(String language) {
		return new java.text.SimpleDateFormat(getMonthPicture(),getLocale(language));
	}
	
	public static String getMonthPicture() {
		Object picture = GlobalVariable.getVariable("API_MONTH_PICTURE");
		if(picture!=null && picture.toString().trim().length()>0) {
			return picture.toString();
		}
		return "MMMM yyyy";
	}
	
	public static java.text.SimpleDateFormat getTimeFormat(String language) {
		return new java.text.SimpleDateFormat(getTimePicture(),getLocale(language));
	}
	
	public static String getTimePicture() {
		Object picture = GlobalVariable.getVariable("API_TIME_PICTURE");
		if(picture!=null && picture.toString().trim().length()>0) {
			return picture.toString();
		}
		return "HH:mm";
	}
	
	public static java.text.SimpleDateFormat getTimeStampFormat(String language) {
		return new java.text.SimpleDateFormat(getTimeStampPicture(),getLocale(language));
	}
	
	public static String getTimeStampPicture() {
		Object picture = GlobalVariable.getVariable("API_TIMESTAMP_PICTURE");
		if(picture!=null && picture.toString().trim().length()>0) {
			return picture.toString();
		}
		return "d MMM yyyy HH:mm";
	}
}
