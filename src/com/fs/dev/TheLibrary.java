package com.fs.dev;

import com.fs.bean.BeanException;
import com.fs.bean.misc.ExecuteStatement;
import com.fs.bean.misc.KnSQL;
import com.fs.bean.util.GlobalBean;
import com.fs.dev.library.SmartLibrary;

/**
 * @author tassun_oro
 *
 */
@SuppressWarnings({"serial","rawtypes"})
public class TheLibrary extends SmartLibrary {
	public static final java.util.Locale LOCALE_TH = new java.util.Locale("th","TH");
	protected static final java.text.SimpleDateFormat[] formaters = { 
			new java.text.SimpleDateFormat("ddMMyy",java.util.Locale.US),
			new java.text.SimpleDateFormat("ddyyMM",java.util.Locale.US),
			new java.text.SimpleDateFormat("MMddyy",java.util.Locale.US),
			new java.text.SimpleDateFormat("MMyydd",java.util.Locale.US),
			new java.text.SimpleDateFormat("yyMMdd",java.util.Locale.US),	
			new java.text.SimpleDateFormat("yyddMM",java.util.Locale.US),
			
			new java.text.SimpleDateFormat("ddMMyy",LOCALE_TH),
			new java.text.SimpleDateFormat("ddyyMM",LOCALE_TH),
			new java.text.SimpleDateFormat("MMddyy",LOCALE_TH),
			new java.text.SimpleDateFormat("MMyydd",LOCALE_TH),
			new java.text.SimpleDateFormat("yyMMdd",LOCALE_TH),	
			new java.text.SimpleDateFormat("yyddMM",LOCALE_TH)	
	};
	private GlobalBean global;
	
	public TheLibrary() {
		super();
	}
	public TheLibrary(GlobalBean global) {
		this.global = global;
	}
	public static void closing(java.sql.ResultSet rs) {
		if (rs != null) {
			try {
				java.sql.Statement stm = rs.getStatement();
				if (stm != null)
					stm.close();
			} catch (Exception ex) {
			} finally {
				try {
					rs.close();
				} catch (Exception ex) { }
			}
		}
	}	
	public static String composeQueryInParty(java.sql.Connection connection,ExecuteStatement sql,String filter,GlobalBean global) throws Exception {
		return composeQueryInParty(connection,sql,filter,global,null);
	}
	public static String composeQueryInParty(java.sql.Connection connection,ExecuteStatement sql,String filter,GlobalBean global,String site) throws Exception {
		return composeQueryInParty(connection,sql,filter,global,site,null);
	}
	public static String composeQueryInParty(java.sql.Connection connection,ExecuteStatement sql,String filter,GlobalBean global,String site,String table) throws Exception {
		if(site!=null && site.trim().length()>0) {
			if(!isInParty(global,site)) {
				throw new BeanException("No permission to access site",-19902);
			}
			sql.append(filter+" "+(table==null?"":table+".")+"site = ?site ");
			sql.setParameter("site",site);			
			filter = " and";
			return filter;
		}
		//global.getFsSite = user site, global.getFsParty = sites under party site (partner ship that come from accessor log on)
		String fs_site = global==null?null:global.getFsSite();
		String fs_sites = global==null?null:global.getFsParty();
		if(fs_sites!=null) {
			sql.append(filter+" ( "+(table==null?"":table+".")+"site = ?site or "+(table==null?"":table+".")+"site in (?sites) ) ");
			sql.setParameter("site",fs_site);			
			sql.setParameter("sites",fs_sites.split(","));
			filter = " and";
		} else {
			sql.append(filter+" "+(table==null?"":table+".")+"site = ?site ");
			sql.setParameter("site",fs_site);			
			filter = " and";
		}
		return filter;
	}
	public static String composeQueryInSites(java.sql.Connection connection,ExecuteStatement sql,String filter,GlobalBean global) throws Exception {
		return composeQueryInSites(connection,sql,filter,global,null);
	}
	public static String composeQueryInSites(java.sql.Connection connection,ExecuteStatement sql,String filter,GlobalBean global,String site) throws Exception {
		return composeQueryInSites(connection,sql,filter,global,site,null);
	}
	public static String composeQueryInSites(java.sql.Connection connection,ExecuteStatement sql,String filter,GlobalBean global,String site,String table) throws Exception {
		if(site!=null && site.trim().length()>0) {
			if(!isInSite(global,site)) {
				throw new BeanException("No permission to access site",-19902);
			}
			sql.append(filter+" "+(table==null?"":table+".")+"site = ?site ");
			sql.setParameter("site",site);			
			filter = " and";
			return filter;
		}
		//global.getFsSite = user site, global.getFsSites = sites under head site (that come from accessor log on)
		String fs_site = global==null?null:global.getFsSite();
		String fs_sites = global==null?null:global.getFsSites();
		if(fs_sites!=null) {
			sql.append(filter+" ( "+(table==null?"":table+".")+"site = ?site or "+(table==null?"":table+".")+"site in (?sites) ) ");
			sql.setParameter("site",fs_site);			
			sql.setParameter("sites",fs_sites.split(","));
			filter = " and";
		} else {
			sql.append(filter+" "+(table==null?"":table+".")+"site = ?site ");
			sql.setParameter("site",fs_site);			
			filter = " and";
		}
		return filter;
	}
	public static boolean isInParty(GlobalBean global,String site) {
		String fs_site = global==null?null:global.getFsSite();
		String fs_sites = global==null?null:global.getFsParty();
		if(site.equals(fs_site)) {
			return true;
		}
		if(fs_sites!=null) {
			String[] sites = fs_sites.split(",");
			if(sites!=null) {
				for(String s : sites) {
					if(site.equals(s)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	public static boolean isInSite(GlobalBean global,String site) {
		String fs_site = global==null?null:global.getFsSite();
		String fs_sites = global==null?null:global.getFsSites();
		if(site.equals(fs_site)) {
			return true;
		}
		if(fs_sites!=null) {
			String[] sites = fs_sites.split(",");
			if(sites!=null) {
				for(String s : sites) {
					if(site.equals(s)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	public static boolean isOnlyAlphabet(String str) { 
		return ((str != null)  && (str.trim().length()>0)  && (str.matches("^[a-zA-Z ]*$"))); 
	}
	public static void main(String[] args) {
		if(args.length>0) {	
			String action = Arguments.getString(args, null, "-act");	
			String text = Arguments.getString(args, null, "-txt");
			try {
				if(text!=null) {
					if("card".equals(action)) {
						Console.out.println(validCardID(text));
					} else if("alpha".equals(action)) {
						Console.out.println(isOnlyAlphabet(text));
					}
				}
			}catch(Exception ex) {
				Console.out.print(ex);
			}
		} else {
			Console.out.println("USAGE : "+TheLibrary.class);
			Console.out.println("-act action ex. card = validate card id, alpha = validate english text");
			Console.out.println("-txt text");
		}
	}
	public static BeanException validatePincode(String pincode,String cardid,java.sql.Date birthday, String fs_language) {
		if(pincode!=null && pincode.trim().length()>0) {
			if(cardid!=null && cardid.trim().length()>0) {
				int idx = cardid.indexOf(pincode);
				if(idx>=0) return new BeanException("Not allow pin code as card id or birth day",-13212,fs_language);
			}
			if(birthday!=null) {
				for(java.text.SimpleDateFormat form : formaters) {
					String text = form.format(birthday);
					int idx = text.indexOf(pincode);
					if(idx>=0) return new BeanException("Not allow pin code as card id or birth day",-13212,fs_language);
				}
			}
		}
		return null;
	}
	public static boolean validCardID(String cardid) throws Exception {
		if (cardid != null) {
			if (cardid.length() != 13) {
				throw new BeanException("Card ID must has 13 character", -18909);
			}
			int sum = 0;
			for (int i = 0; i < 12; i++) {
				sum += Character.getNumericValue(cardid.charAt(i)) * (13 - i);
			}
			if ((11 - sum % 11) % 10 != Character.getNumericValue(cardid.charAt(12))) {
				throw new BeanException("Card ID is Invalid", -18910);
			}
		}
		return true;
	}
	public java.util.List<String> getAllParty(java.sql.Connection connection,GlobalBean global) throws Exception {
		return getAllParty(connection,global==null?null:global.getFsSite());
	}
	public java.util.List<String> getAllParty(java.sql.Connection connection,String partysite) throws Exception {
		java.util.List<String> results = new java.util.ArrayList<>();
		if(connection!=null && (partysite!=null && partysite.trim().length()>0)) {
			KnSQL knsql = new KnSQL(this);
			knsql.append("select site from tcompparty where partysite = ?partysite ");
			knsql.setParameter("partysite",partysite);
			try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
				while(rs.next()) {
					results.add(rs.getString("site"));
				}
				close(rs);
			}
		}
		return results;
	}
	public java.util.List<String> getAllSites(java.sql.Connection connection,GlobalBean global) throws Exception {
		return getAllSites(connection,global==null?null:global.getFsSite());
	}
	public java.util.List<String> getAllSites(java.sql.Connection connection,String headsite) throws Exception {
		java.util.List<String> results = new java.util.ArrayList<>();
		if(connection!=null && (headsite!=null && headsite.trim().length()>0)) {
			KnSQL knsql = new KnSQL(this);
			knsql.append("select site from tcompgrp where headsite = ?headsite ");
			knsql.setParameter("headsite",headsite);
			try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
				while(rs.next()) {
					results.add(rs.getString("site"));
				}
				close(rs);
			}
		}
		return results;
	}
	public GlobalBean getGlobal() {
		return global;
	}	
	public java.util.List<java.util.Map<Object,Object>> mapToList(java.util.Map map) {
		return mapToList(map,"id","name");
	}
	public java.util.List<java.util.Map<Object,Object>> mapToList(java.util.Map map,String id,String name) {
		java.util.ArrayList<java.util.Map<Object,Object>> result = new java.util.ArrayList<>();
		if(map!=null && id!=null && name!=null) {
			for(java.util.Iterator it = map.keySet().iterator();it.hasNext();) {
				Object key = it.next();
				Object value = map.get(key);
				if(key!=null && value!=null) {
					java.util.Map<Object,Object> emap = new java.util.HashMap<>();
					emap.put(id, key);
					emap.put(name, value);
					result.add(emap);
				}
			}
		}
		return result;
	}		
}
