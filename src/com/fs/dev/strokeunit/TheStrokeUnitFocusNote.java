/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fs.dev.strokeunit;

import com.fs.bean.misc.KnSQL;
import com.fs.bean.util.GlobalBean;
import java.sql.Connection;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Supakit_Pud
 */
public class TheStrokeUnitFocusNote {

    final String fs_actionIns = GlobalBean.INSERT_MODE;
    TheStrokeUnit su = new TheStrokeUnit();
    JSONArray jsonArrayP03ProgressNote;
    JSONArray jsonArrayP03ProgressMore;
    JSONArray jsonArrayP03StrokeType;
    JSONArray jsonArrayP03NeuroSign;
    JSONArray jsonArrayP03AcuteIschemic;
    JSONArray jsonArrayP03vitalSign;

    public TheStrokeUnitFocusNote(Connection connection) throws Exception {
        //Get Json For Insert 
        JSONArray jsonP03ProgressNote = su.getConfigfield(connection, "P03ProgressNote", "Tw_ProgressNote");
        jsonArrayP03ProgressNote = su.handleConfigAllData(jsonP03ProgressNote);
        JSONArray jsonP03ProgressMore = su.getConfigfield(connection, "P03ProgressMore", "Tw_ProgressMore");
        jsonArrayP03ProgressMore = su.handleConfigAllData(jsonP03ProgressMore);
        JSONArray jsonP03StrokeType = su.getConfigfield(connection, "P03StrokeType", "Tw_FocusStrokeType");
        jsonArrayP03StrokeType = su.handleConfigAllData(jsonP03StrokeType);
        JSONArray jsonP03NeuroSign = su.getConfigfield(connection, "P03NeuroSign", "Tw_NeuroSign");
        jsonArrayP03NeuroSign = su.handleConfigAllData(jsonP03NeuroSign);
        JSONArray jsonP03AcuteIschemic = su.getConfigfield(connection, "P03AcuteIschemic", "Tw_AcuteIschemic");
        jsonArrayP03AcuteIschemic = su.handleConfigAllData(jsonP03AcuteIschemic);
        JSONArray jsonP03vitalSign = su.getConfigfield(connection, "P03vitalSign", "Tw_VitalSign");
        jsonArrayP03vitalSign = su.handleConfigAllData(jsonP03vitalSign);

    }

    public void deleteFocusNote(Connection connection, String focusNoteId) throws Exception {
        //Delete Data After Insert
        KnSQL knsql = new KnSQL(this);
        knsql.append("DELETE FROM Tw_ProgressNote WHERE FocusNoteId =?focusNoteId ");
        knsql.setParameter("focusNoteId", focusNoteId);
        knsql.executeUpdate(connection);
        knsql = new KnSQL(this);
        knsql.append("DELETE FROM Tw_FocusStrokeType WHERE FocusNoteId =?focusNoteId ");
        knsql.setParameter("focusNoteId", focusNoteId);
        knsql.executeUpdate(connection);
        knsql = new KnSQL(this);
        knsql.append("DELETE FROM Tw_ProgressMore WHERE FocusNoteId =?focusNoteId ");
        knsql.setParameter("focusNoteId", focusNoteId);
        knsql.executeUpdate(connection);
        knsql = new KnSQL(this);
        knsql.append("DELETE FROM Tw_NeuroSign WHERE FocusNoteId =?focusNoteId ");
        knsql.setParameter("focusNoteId", focusNoteId);
        knsql.executeUpdate(connection);
        knsql = new KnSQL(this);
        knsql.append("DELETE FROM Tw_AcuteIschemic WHERE FocusNoteId =?focusNoteId ");
        knsql.setParameter("focusNoteId", focusNoteId);
        knsql.executeUpdate(connection);
        knsql = new KnSQL(this);
        knsql.append("DELETE FROM Tw_VitalSign WHERE PatientId =?focusNoteId ");
        knsql.setParameter("focusNoteId", focusNoteId);
        knsql.executeUpdate(connection);
    }

    public int insertFocusNote(Connection connection, String focusNoteId, JSONArray jsonArrayList, String progressType) throws Exception {

        int result = 0;
        for (int i = 0; i < jsonArrayList.length(); i++) {
            JSONObject jsonObjectData = jsonArrayList.getJSONObject(i);
            String focusTime = jsonObjectData.getString("time");
            JSONArray jsonFocusDetail = null;
            System.err.println("progressType : " + progressType);
            switch (progressType) {
                case "A":
                    if (jsonObjectData.has("assessmentDetail")) {
                        jsonFocusDetail = new JSONArray(jsonObjectData.getString("assessmentDetail"));
                    } else {
                        jsonFocusDetail = new JSONArray();
                    }
                    break;
                case "I":
                    if (jsonObjectData.has("interventionDetail")) {
                        jsonFocusDetail = new JSONArray(jsonObjectData.getString("interventionDetail"));
                    } else {
                        jsonFocusDetail = new JSONArray();
                    }
                    break;
                case "E":
                    if (jsonObjectData.has("evaluationDetail")) {
                        jsonFocusDetail = new JSONArray(jsonObjectData.getString("evaluationDetail"));
                    } else {
                        jsonFocusDetail = new JSONArray();
                    }
                    break;
            }
            System.err.println("jsonFocusDetail : " + jsonFocusDetail);
            if (jsonFocusDetail.length() <= 0) {
                JSONObject jsonDetail = new JSONObject();
                jsonDetail.put("focusNoteId", focusNoteId);
                jsonDetail.put("focusTime", focusTime);
                jsonDetail.put("progressType", progressType);
                jsonDetail.put("ordinal",i + 1);
                org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, "P03ProgressNote");
                org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(jsonDetail, jsonArrayConfigfield);
                result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfig, jsonDetail).getEffectedTransactions();
            }

            for (int j = 0; j < jsonFocusDetail.length(); j++) {
                JSONObject jsonDetail = jsonFocusDetail.getJSONObject(j);
                String progressNoteId = jsonDetail.getString("progressNoteId");
                String additional = "";
                JSONArray progressDetail = new JSONArray(jsonDetail.getString("progressDetail"));
                for (int k = 0; k < progressDetail.length(); k++) {
                    JSONObject jsonProgressDetail = progressDetail.getJSONObject(k);
                    additional = jsonProgressDetail.getString("additional");
                    String seq = jsonProgressDetail.getString("seq");
                    System.err.println("additional : " + jsonProgressDetail);
                    if (additional.length() > 0) {
                        if (additional.contains("S")) {
                            JSONArray jsonStrokeType = new JSONArray(jsonProgressDetail.getString("strokeType"));
                            System.err.println("jsonStrokeType : " + jsonStrokeType);
                            for (int l = 0; l < jsonStrokeType.length(); l++) {
                                JSONObject strokeType = jsonStrokeType.getJSONObject(l);
                                strokeType.put("focusNoteId", focusNoteId);
                                strokeType.put("focusTime", focusTime);
                                strokeType.put("progressNoteId", progressNoteId);
                                strokeType.put("seq", seq);
                                result += su.handleParams(connection, fs_actionIns, jsonArrayP03StrokeType, strokeType).getEffectedTransactions();
                            }
                        }
                        if (additional.contains("N")) {
                            JSONObject jsonNeuroSign = new JSONObject(jsonProgressDetail.getString("neuroSign"));
                            jsonNeuroSign.put("focusNoteId", focusNoteId);
                            jsonNeuroSign.put("focusTime", focusTime);
                            jsonNeuroSign.put("progressNoteId", progressNoteId);
                            jsonNeuroSign.put("seq", seq);
                            result += su.handleParams(connection, fs_actionIns, jsonArrayP03NeuroSign, jsonNeuroSign).getEffectedTransactions();
                        }
                        if (additional.contains("A")) {
                            JSONObject jsonAcuteIschemic = new JSONObject(jsonProgressDetail.getString("acuteIschemic"));
                            jsonAcuteIschemic.put("focusNoteId", focusNoteId);
                            jsonAcuteIschemic.put("focusTime", focusTime);
                            jsonAcuteIschemic.put("progressNoteId", progressNoteId);
                            jsonAcuteIschemic.put("seq", seq);
                            result += su.handleParams(connection, fs_actionIns, jsonArrayP03AcuteIschemic, jsonAcuteIschemic).getEffectedTransactions();
                        }
                        if (additional.contains("V")) {
                            JSONObject jsonVitalSign = new JSONObject(jsonProgressDetail.getString("vitalSign"));
                            jsonVitalSign.put("focusNoteId", focusNoteId);
                            jsonVitalSign.put("focusTime", focusTime);
                            jsonVitalSign.put("progressNoteId", progressNoteId);
                            jsonVitalSign.put("seq", seq);
                            jsonVitalSign.put("vitalSignId", TheStrokeUnit.Generate.createUUID());

                            result += su.handleParams(connection, fs_actionIns, jsonArrayP03vitalSign, jsonVitalSign).getEffectedTransactions();
                        }
                    } else {
                        System.err.println("else : jsonArrayP03ProgressMore ");
                        jsonProgressDetail.put("focusNoteId", focusNoteId);
                        jsonProgressDetail.put("focusTime", focusTime);
                        jsonProgressDetail.put("progressNoteId", progressNoteId);
                        jsonProgressDetail.put("seq", seq);
                        result += su.handleParams(connection, fs_actionIns, jsonArrayP03ProgressMore, jsonProgressDetail).getEffectedTransactions();
                    }
                }
                jsonDetail.put("focusNoteId", focusNoteId);
                jsonDetail.put("focusTime", focusTime);
                jsonDetail.put("progressType", progressType);
                jsonDetail.put("additional", additional);
                jsonDetail.put("ordinal", i + 1);
                result += su.handleParams(connection, fs_actionIns, jsonArrayP03ProgressNote, jsonDetail).getEffectedTransactions();
            }
        }
        return result;
    }

}
