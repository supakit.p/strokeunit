/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fs.dev.strokeunit;

import com.fs.bean.misc.KnSQL;
import com.fs.bean.misc.Trace;
import com.fs.bean.util.GlobalBean;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Supakit_Pud
 */
public class TheStrokeUnit extends TheStrokeUnitUtility {

    private JSONObject jsonObjectResult = new JSONObject();
    final public Response response = new Response();
    final public static String INSERT_MODE_NO_KEY = "insertNoKey";

    public TheStrokeUnit() {
    }
    SysConfig sysConfig = new SysConfig();

    public JSONObject getSysConfig(String fs_category, String fs_configName) throws Exception {
        Trace.info("## getSysConfig category : ภาษาไทย" + fs_category + " || configName : " + fs_configName);
        sysConfig.createSysConfigsInfo();
        return sysConfig.getSysConfigsInfoByCategoryAndConfigName(fs_category, fs_configName);
        /*
        Trace.info("## getSysConfig fs_category : " + fs_category + " fs_configName : " + fs_configName);
        if (request != null) {
            createSysConfigs(request);
            return getSysConfigsByCategoryAndConfigName(request, fs_category, fs_configName);
        }
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT ConfigId , Category , ConfigName , ConfigDesc , Value , Unit , Remark ");
        knsql.append("FROM sys_Config ");
        knsql.append("WHERE IsActive = '1' AND ConfigName = ?configName AND Category = ?category");
        knsql.setParameter("category", fs_category);
        knsql.setParameter("configName", fs_configName);
        java.sql.ResultSet rs = knsql.executeQuery(connection);
        JSONObject jsonObjectConfig = new JSONObject();
        java.sql.ResultSetMetaData met = rs.getMetaData();
        if (rs.next()) {
            int colcount = met.getColumnCount();
            for (int i = 1; i <= colcount; i++) {
                String colname = met.getColumnName(i);
                jsonObjectConfig.put(colname, rs.getString(colname));
            }
        }
        return jsonObjectConfig;
         */
    }

    public JSONArray getSysConfigList(String fs_category) throws Exception {
        Trace.info("## getSysConfigList category : " + fs_category);
        sysConfig.createSysConfigsInfo();
        return sysConfig.getSysConfigsInfoListByCategory(fs_category);
        /*
        Trace.info("## getSysConfig");
        if (request != null) {
            createSysConfigs(request);
            return getSysConfigsListByCategory(request, fs_category);
        }
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT ConfigId , Category , ConfigName , ConfigDesc , Value , Unit , Remark ");
        knsql.append("FROM sys_Config ");
        knsql.append("WHERE IsActive = '1' AND Category = ?category");
        knsql.setParameter("category", fs_category);
        java.sql.ResultSet rs = knsql.executeQuery(connection);
        java.sql.ResultSetMetaData met = rs.getMetaData();
        JSONArray jsonArray = new JSONArray();
        while (rs.next()) {
            JSONObject jsonObject = new JSONObject();
            int colcount = met.getColumnCount();
            for (int i = 1; i <= colcount; i++) {
                String colname = met.getColumnLabel(i);
                jsonObject.put(colname, rs.getString(colname));
            }
            jsonArray.put(jsonObject);
        }
        return jsonArray;*/
    }

    public JSONArray getMasterArray(java.sql.Connection connection, String groupName, JSONObject json, String fs_language) throws Exception {
        JSONArray jsonArrayMaster = new JSONArray();

        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT Mw_MasterConfig.GroupName , Mw_MasterConfig.MasterName , Mw_MasterConfig.MasterId , Mw_MasterConfig.MasterType , Mw_MasterConfig.Display , Mw_MasterConfig.DisplayEN , Mw_MasterConfig.Value ,Mw_MasterConfig.Description , Mw_MasterConfig.Remark  ");
        knsql.append(" FROM Mw_MasterConfig ");
        knsql.append(" WHERE Mw_MasterConfig.IsActive = '1' AND Mw_MasterConfig.GroupName =?groupName ");
        knsql.append(" ORDER BY Mw_MasterConfig.MasterName ASC, Mw_MasterConfig.Ordinal ASC ");
        HashMap<String, String> configFieldM = new HashMap();
        // configFieldM.put("GroupName", "groupName");
        configFieldM.put("MasterName", "masterName");
        configFieldM.put("MasterId", "masterId");
        configFieldM.put("MasterType", "masterType");
        configFieldM.put("Value", "value");
        configFieldM.put("Description", "masterDescription");
        // configFieldM.put("Remark", "remark");
        if (TheStrokeUnitValidate.isEnglish(fs_language)) {
            knsql.append(",Mw_MasterConfig.DisplayEN ASC ");
            configFieldM.put("DisplayEN", "display");
        } else {
            knsql.append(",Mw_MasterConfig.Display ASC ");
            configFieldM.put("Display", "display");
        }
        knsql.setParameter("groupName", groupName);

        JSONArray jsonMasterConfig = new JSONArray(requestData(connection, knsql, true, configFieldM).toString());
        JSONObject jsonMaster = new JSONObject();
        JSONArray masterDetail = new JSONArray();
        String tempMasterName = "";
        for (int i = 0; i < jsonMasterConfig.length(); i++) {
            org.json.JSONObject jsonObjectElement = jsonMasterConfig.getJSONObject(i);
            String masterName = jsonObjectElement.getString("masterName");
            if (!tempMasterName.equals(masterName)) {
                tempMasterName = masterName;
                if (jsonMaster.length() > 0) {
                    jsonMaster.put("masterDetail", masterDetail);
                    jsonArrayMaster.put(jsonMaster);
                    jsonMaster = new JSONObject();
                    masterDetail = new JSONArray();
                }
            }
            jsonMaster.put("masterGroup", masterName);
            jsonObjectElement.remove("masterName");
            masterDetail.put(jsonObjectElement);
        }
        if (jsonMaster.length() > 0) {
            jsonMaster.put("masterDetail", masterDetail);
            jsonArrayMaster.put(jsonMaster);
        }
        
        //////////////////////////////////// Master Fix ////////////////////////////////////
        final String nursingCare = "NursingCare";
        switch (groupName) {
            case nursingCare:
                knsql = new KnSQL(this);
                knsql.append("SELECT Mw_MasterConfig.MasterId , Mw_MasterConfig.MasterType , Mw_MasterConfig.Display , Mw_MasterConfig.Value ");
                knsql.append(" FROM Mw_MasterConfig ");
                knsql.append(" WHERE IsActive = '1' AND GroupName = 'FocusNote' AND MasterName = 'Shift' ");
                HashMap<String, String> configFieldShift = new HashMap();
                configFieldShift.put("MasterId", "masterId");
                configFieldShift.put("Display", "display");
                configFieldShift.put("MasterType", "masterType");
                configFieldShift.put("Value", "value");
                JSONArray jsonMasterShift = new JSONArray(requestData(connection, knsql, true, configFieldShift).toString());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("masterGroup", "shift");
                jsonObject.put("masterDetail", jsonMasterShift);
                jsonArrayMaster.put(jsonObject);
                break;
            default:
            // code block
        }
        //////////////////////////////////// Master Fix ////////////////////////////////////
        
        
        return jsonArrayMaster;

    }

    public JSONObject getMaster(java.sql.Connection connection, String groupName, JSONObject json, String fs_language) throws Exception {
        JSONObject jsonObjectMaster = new JSONObject();
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT Mw_MasterConfig.GroupName , Mw_MasterConfig.MasterName , Mw_MasterConfig.MasterId , Mw_MasterConfig.MasterType , Mw_MasterConfig.Display , Mw_MasterConfig.DisplayEN , Mw_MasterConfig.Value ,Mw_MasterConfig.Description , Mw_MasterConfig.Remark  ");
        knsql.append(" FROM Mw_MasterConfig ");
        knsql.append(" WHERE Mw_MasterConfig.IsActive = '1' AND Mw_MasterConfig.GroupName =?groupName ");
        knsql.append(" ORDER BY Mw_MasterConfig.MasterName ASC, Mw_MasterConfig.Ordinal ASC ");
        HashMap<String, String> configFieldM = new HashMap();
        // configFieldM.put("GroupName", "groupName");
        configFieldM.put("MasterName", "masterName");
        configFieldM.put("MasterId", "masterId");
        configFieldM.put("MasterType", "masterType");
        configFieldM.put("Value", "value");
        configFieldM.put("Description", "masterDescription");
        // configFieldM.put("Remark", "remark");
        if (TheStrokeUnitValidate.isEnglish(fs_language)) {
            knsql.append(",Mw_MasterConfig.DisplayEN ASC ");
            configFieldM.put("DisplayEN", "display");
        } else {
            knsql.append(",Mw_MasterConfig.Display ASC ");
            configFieldM.put("Display", "display");
        }
        knsql.setParameter("groupName", groupName);

        org.json.JSONArray jsonMasterConfig = new org.json.JSONArray(requestData(connection, knsql, true, configFieldM).toString());

        org.json.JSONArray masterNameList = new org.json.JSONArray();
        String tempMasterName = "";
        for (int i = 0; i < jsonMasterConfig.length(); i++) {
            org.json.JSONObject jsonObjectElement = jsonMasterConfig.getJSONObject(i);
            String masterName = jsonObjectElement.getString("masterName");
            // initial Data
            if ("".equals(tempMasterName)) {
                tempMasterName = masterName;
            } else if (!tempMasterName.equals(masterName)) {
                jsonObjectMaster.put(tempMasterName, masterNameList);
                masterNameList = new org.json.JSONArray();
                tempMasterName = masterName;
            }
            jsonObjectElement.remove("masterName");
            masterNameList.put(jsonObjectElement);
        }
        if (masterNameList.length() > 0) {
            jsonObjectMaster.put(tempMasterName, masterNameList);
        }

        //////////////////////////////////// Master Fix ////////////////////////////////////
        final String patientInfo = "PatientInfo";
        final String nursingFocus = "NursingFocus";
        final String focusNote = "FocusNote";
        final String diet = "Diet";
        final String nursingCare = "NursingCare";

        switch (groupName) {
            case patientInfo:
                knsql = new KnSQL(this);
                knsql.append("SELECT StrokeTypeId , StrokeDisplay , StrokeName , StrokeCategory  ");
                knsql.append("FROM Mw_StrokeType ");
                knsql.append("WHERE IsActive = '1' ");
                HashMap<String, String> configFieldST = new HashMap();
                configFieldST.put("StrokeTypeId", "masterId");
                configFieldST.put("StrokeCategory", "masterType");
                configFieldST.put("StrokeDisplay", "display");
                configFieldST.put("StrokeName", "value");
                org.json.JSONArray jsonMasterStroke = new org.json.JSONArray(requestData(connection, knsql, true, configFieldST).toString());
                jsonObjectMaster.put("StrokeType", jsonMasterStroke);
                break;
            case nursingFocus:
                boolean haveFocusId = json.has("focusId") && json.getString("focusId").trim().length() > 0;
                boolean havePatientId = json.has("patientId") && json.getString("patientId").trim().length() > 0;
                if (haveFocusId && havePatientId) {
                    System.err.println(" haveFocusId && havePatientId");
                    knsql = new KnSQL(this);
                    knsql.append("SELECT FocusId , FocusName , FocusName as DisplayName ");
                    knsql.append(" FROM Mw_Focus ");
                    knsql.append(" WHERE IsActive = '1' AND FocusId NOT IN ");
                    knsql.append(" ( SELECT FocusId ");
                    knsql.append(" FROM Tw_FocusList ");
                    knsql.append(" WHERE PatientId = ?patientId ) OR FocusId = ?focusId ");
                    knsql.setParameter("patientId", json.getString("patientId"));
                    knsql.setParameter("focusId", json.getString("focusId"));
                    HashMap<String, String> configFieldNF = new HashMap();
                    configFieldNF.put("FocusId", "masterId");
                    configFieldNF.put("DisplayName", "display");
                    configFieldNF.put("FocusName", "value");
                    org.json.JSONArray jsonMasterNursingFocus = new org.json.JSONArray(requestData(connection, knsql, true, configFieldNF).toString());
                    jsonObjectMaster.put("FocusList", jsonMasterNursingFocus);
                } else if (haveFocusId) {
                    System.err.println(" haveFocusId ");
                    knsql = new KnSQL(this);
                    knsql.append("SELECT GoalsId , GoalsName , GoalsName as DisplayName ");
                    knsql.append(" FROM Mw_FocusGoals ");
                    knsql.append(" WHERE IsActive = '1' AND FocusId =?focusId ");
                    knsql.setParameter("focusId", json.getString("focusId"));
                    HashMap<String, String> configFieldGoals = new HashMap();
                    configFieldGoals.put("GoalsId", "masterId");
                    configFieldGoals.put("DisplayName", "display");
                    configFieldGoals.put("GoalsName", "value");
                    org.json.JSONArray jsonMasterGoals = new org.json.JSONArray(requestData(connection, knsql, true, configFieldGoals).toString());
                    jsonObjectMaster.put("GoalsList", jsonMasterGoals);
                } else if (havePatientId) {
                    System.err.println(" havePatientId ");
                    knsql = new KnSQL(this);
                    knsql.append("SELECT FocusId , FocusName , FocusName as DisplayName ");
                    knsql.append(" FROM Mw_Focus ");
                    knsql.append(" WHERE IsActive = '1' AND FocusId NOT IN  ");
                    knsql.append(" ( SELECT FocusId ");
                    knsql.append(" FROM Tw_FocusList ");
                    knsql.append(" WHERE PatientId = ?patientId ) ");
                    knsql.setParameter("patientId", json.getString("patientId"));
                    HashMap<String, String> configFieldNF = new HashMap();
                    configFieldNF.put("FocusId", "masterId");
                    configFieldNF.put("DisplayName", "display");
                    configFieldNF.put("FocusName", "value");
                    org.json.JSONArray jsonMasterNursingFocus = new org.json.JSONArray(requestData(connection, knsql, true, configFieldNF).toString());
                    jsonObjectMaster.put("FocusList", jsonMasterNursingFocus);
                }
                break;
            case diet:
                if (json.has("dietId") && json.getString("dietId").trim().length() > 0) {
                    knsql = new KnSQL(this);
                    knsql.append("SELECT Mw_SubDiet.SubDietId , Mw_SubDiet.SubDietName ");
                    knsql.append(" FROM Mw_SubDiet ");
                    knsql.append(" LEFT JOIN Mw_Diet ON Mw_SubDiet.DietId = Mw_Diet.DietId ");
                    knsql.append(" WHERE Mw_SubDiet.IsActive = '1' AND Mw_SubDiet.DietId =?dietId ");
                    knsql.setParameter("dietId", json.getString("dietId"));
                    HashMap<String, String> configSubDiet = new HashMap();
                    configSubDiet.put("SubDietId", "subDietId");
                    configSubDiet.put("SubDietName", "subDietName");
                    org.json.JSONArray jsonMasterSubDiet = new org.json.JSONArray(requestData(connection, knsql, true, configSubDiet).toString());
                    jsonObjectMaster.put("SubDietList", jsonMasterSubDiet);
                } else {
                    knsql = new KnSQL(this);
                    knsql.append("SELECT Mw_Diet.DietId , Mw_Diet.DietName , Mw_SubDiet.SubDietId , Mw_SubDiet.SubDietName ");
                    knsql.append(" FROM Mw_Diet ");
                    knsql.append(" LEFT JOIN Mw_SubDiet ON Mw_SubDiet.DietId = Mw_Diet.DietId ");
                    knsql.append(" WHERE Mw_Diet.IsActive = '1' ");
                    knsql.append(" ORDER BY Mw_Diet.DietId ASC");
                    HashMap<String, String> configDiet = new HashMap();
                    configDiet.put("DietId", "dietId");
                    configDiet.put("DietName", "dietName");
                    configDiet.put("SubDietId", "subDietId");
                    configDiet.put("SubDietName", "subDietName");
                    org.json.JSONArray jsonMasterDiet = new org.json.JSONArray(requestData(connection, knsql, true, configDiet).toString());
                    org.json.JSONArray dietList = new org.json.JSONArray();
                    if (jsonMasterDiet.length() > 0) {
                        String tempDietId = "";
                        org.json.JSONObject mainDiet = new org.json.JSONObject();
                        org.json.JSONArray subDietList = new org.json.JSONArray();
                        org.json.JSONObject subDiet = new org.json.JSONObject();
                        for (int k = 0; k < jsonMasterDiet.length(); k++) {
                            org.json.JSONObject masterDiet = jsonMasterDiet.getJSONObject(k);
                            String dietId = masterDiet.getString("dietId");
                            String dietName = masterDiet.getString("dietName");
                            String subDietId = masterDiet.getString("subDietId");
                            String subDietName = masterDiet.getString("subDietName");
                            if (k == 0) {
                                tempDietId = dietId;
                                mainDiet.put("dietId", tempDietId);
                                mainDiet.put("dietName", dietName);
                                if (!"".equals(subDietId)) {
                                    subDiet.put("subDietId", subDietId);
                                    subDiet.put("subDietName", subDietName);
                                    subDietList.put(subDiet);
                                }
                            } else {
                                if (tempDietId.equals(dietId)) {
                                    subDiet = new org.json.JSONObject();
                                    if (!"".equals(subDietId)) {
                                        subDiet.put("subDietId", subDietId);
                                        subDiet.put("subDietName", subDietName);
                                        subDietList.put(subDiet);
                                    }
                                } else {
                                    if (subDietList.length() > 0) {
                                        mainDiet.put("subDiet", subDietList);
                                    }
                                    dietList.put(mainDiet);
                                    tempDietId = dietId;
                                    mainDiet = new org.json.JSONObject();
                                    mainDiet.put("dietId", tempDietId);
                                    mainDiet.put("dietName", dietName);
                                    subDiet = new org.json.JSONObject();
                                    subDietList = new org.json.JSONArray();
                                    if (!"".equals(subDietId)) {
                                        subDiet.put("subDietId", subDietId);
                                        subDiet.put("subDietName", subDietName);
                                        subDietList.put(subDiet);
                                    }
                                }
                            }
                        }
                        if (subDietList.length() > 0) {
                            mainDiet.put("subDietList", subDietList);
                        }
                        dietList.put(mainDiet);
                    }

                    jsonObjectMaster.put("DietList", dietList);
                }

                break;
            case focusNote:
                knsql = new KnSQL(this);
                knsql.append("SELECT FocusId , FocusName , FocusName as DisplayName ");
                knsql.append(" FROM Mw_Focus ");
                knsql.append(" WHERE IsActive = '1' ");
                HashMap<String, String> configFieldFN = new HashMap();
                configFieldFN.put("FocusId", "masterId");
                configFieldFN.put("DisplayName", "display");
                configFieldFN.put("FocusName", "value");
                org.json.JSONArray jsonMasterFocusNote = new org.json.JSONArray(requestData(connection, knsql, true, configFieldFN).toString());
                jsonObjectMaster.put("FocusList", jsonMasterFocusNote);
                break;
            case nursingCare:
                knsql = new KnSQL(this);
                knsql.append("SELECT Mw_MasterConfig.MasterId , Mw_MasterConfig.MasterType , Mw_MasterConfig.Display , Mw_MasterConfig.Value ");
                knsql.append(" FROM Mw_MasterConfig ");
                knsql.append(" WHERE IsActive = '1' AND GroupName = 'FocusNote' AND MasterName = 'Shift' ");
                HashMap<String, String> configFieldShift = new HashMap();
                configFieldShift.put("MasterId", "masterId");
                configFieldShift.put("Display", "display");
                configFieldShift.put("MasterType", "masterType");
                configFieldShift.put("Value", "value");
                org.json.JSONArray jsonMasterShift = new org.json.JSONArray(requestData(connection, knsql, true, configFieldShift).toString());
                jsonObjectMaster.put("shift", jsonMasterShift);
                break;
            default:
            // code block
        }
        return jsonObjectMaster;
    }

    public JSONArray getConfigfield(java.sql.Connection connection, String fs_configName) throws Exception {
        Trace.info("## getApiConfigfield");
        JSONArray jsonArrayConfig = new JSONArray();
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT ConfigName , ParamName , TableName , FieldName , FieldType , IsKey , IsAutoKey , IsArray ");
        knsql.append("FROM Mw_ConfigField ");
        knsql.append("WHERE IsActive = '1' AND ConfigName = ?ConfigName ");
        knsql.append("ORDER BY TableName ");
        knsql.setParameter("ConfigName", fs_configName);
        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
            java.sql.ResultSetMetaData met = rs.getMetaData();
            while (rs.next()) {
                JSONObject jsonObjectConfig = new JSONObject();
                int colcount = met.getColumnCount();
                for (int i = 1; i <= colcount; i++) {
                    String colname = met.getColumnName(i);
                    jsonObjectConfig.put(colname, rs.getString(colname));
                }
                jsonArrayConfig.put(jsonObjectConfig);
            }
        }
        return jsonArrayConfig;
    }

    public JSONArray getConfigfield(java.sql.Connection connection, String fs_configName, String fs_tableName) throws Exception {
        Trace.info("## getApiConfigfield");
        JSONArray jsonArrayConfig = new JSONArray();
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT ConfigName , ParamName , TableName , FieldName , FieldType , IsKey , IsAutoKey , IsArray ");
        knsql.append("FROM Mw_ConfigField ");
        knsql.append("WHERE IsActive = '1' AND ConfigName = ?ConfigName AND TableName = ?TableName ");
        knsql.append("ORDER BY TableName ");
        knsql.setParameter("ConfigName", fs_configName);
        knsql.setParameter("TableName", fs_tableName);
        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
            java.sql.ResultSetMetaData met = rs.getMetaData();
            while (rs.next()) {
                JSONObject jsonObjectConfig = new JSONObject();
                int colcount = met.getColumnCount();
                for (int i = 1; i <= colcount; i++) {
                    String colname = met.getColumnName(i);
                    jsonObjectConfig.put(colname, rs.getString(colname));
                }
                jsonArrayConfig.put(jsonObjectConfig);
            }
        }
        return jsonArrayConfig;
    }

    public JSONArray handleConfigAllData(JSONArray jsonArrayConfigfield) throws Exception {
        Trace.info("## handleConfigData ");
        //Not Have Config
        if (jsonArrayConfigfield.length() <= 0) {
            return jsonArrayConfigfield;
            //throw new RuntimeException("Not Found ConfigName");   
        }
        HashMap<String, Object> tempConfig = new HashMap<>();
        JSONArray tableList = new JSONArray();
        String tableName = "";
        List<String> fieldNameList = new ArrayList<>();
        List<String> paramNameList = new ArrayList<>();
        List<String> fieldTypeList = new ArrayList<>();
        List<String> fieldNameAndParamNameList = new ArrayList<>();
        List<String> keyFieldNameList = new ArrayList<>();
        List<String> keyFieldTypeList = new ArrayList<>();
        List<String> keyFieldNameAndParamNameList = new ArrayList<>();
        List<String> isArrayListFieldName = new ArrayList<>();
        for (int i = 0; i < jsonArrayConfigfield.length(); i++) {
            JSONObject jsonObjectElement = jsonArrayConfigfield.getJSONObject(i);

            //initial Config Data , now not check null value -- is config 
            String tempTableName = jsonObjectElement.getString("TableName");
            String tempFieldName = jsonObjectElement.getString("FieldName");
            String tempParamName = jsonObjectElement.getString("ParamName");
            String tempFieldType = jsonObjectElement.getString("FieldType");
            String tempIsKey = jsonObjectElement.getString("IsKey");
            String tempIsArray = jsonObjectElement.getString("IsArray");
            //String tempIsAutoKey = jsonObjectElement.get("IsAutoKey").getAsString();
//            System.err.println("jsonObjectElement : "+ i + " " + jsonObjectElement);
            //initial loop
            if (tableName.isEmpty()) {
                tableName = tempTableName;
                fieldNameList.add(tempFieldName);
                paramNameList.add(tempParamName);
                fieldTypeList.add(tempFieldType);
                fieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                if ("1".equals(tempIsKey)) {
                    keyFieldNameList.add(tempParamName);
                    keyFieldTypeList.add(tempFieldType);
                    keyFieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                }
            } else {
                boolean haveTable = false;
                if (tableName.equals(tempTableName)) {
                    haveTable = true;
                    fieldNameList.add(tempFieldName);
                    paramNameList.add(tempParamName);
                    fieldTypeList.add(tempFieldType);
                    fieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                    if ("1".equals(tempIsKey)) {
                        keyFieldNameList.add(tempParamName);
                        keyFieldTypeList.add(tempFieldType);
                        keyFieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                    }
                }
                if (!haveTable) {
                    //Have New Table => Action
                    tempConfig.put("isArrayListFieldName", isArrayListFieldName);
                    tempConfig.put("tableName", tableName);
                    tempConfig.put("fieldNameList", fieldNameList);
                    tempConfig.put("paramNameList", paramNameList);
                    tempConfig.put("fieldTypeList", fieldTypeList);
                    tempConfig.put("fieldNameAndParamNameList", fieldNameAndParamNameList);
                    tempConfig.put("keyFieldNameList", keyFieldNameList);
                    tempConfig.put("keyFieldTypeList", keyFieldTypeList);
                    tempConfig.put("keyFieldNameAndParamNameList", keyFieldNameAndParamNameList);
                    tableList.put(tempConfig);
                    tempConfig = new HashMap<>();

                    //Reset Data
                    tableName = "";
                    fieldNameList = new ArrayList<>();
                    paramNameList = new ArrayList<>();
                    fieldTypeList = new ArrayList<>();
                    fieldNameAndParamNameList = new ArrayList<>();
                    keyFieldNameList = new ArrayList<>();
                    keyFieldTypeList = new ArrayList<>();
                    keyFieldNameAndParamNameList = new ArrayList<>();

                    tableName = tempTableName;
                    fieldNameList.add(tempFieldName);
                    paramNameList.add(tempParamName);
                    fieldTypeList.add(tempFieldType);
                    fieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                    if ("1".equals(tempIsKey)) {
                        keyFieldNameList.add(tempParamName);
                        keyFieldTypeList.add(tempFieldType);
                        keyFieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                    }
                }
            }
        }
        tempConfig.put("isArrayListFieldName", isArrayListFieldName);
        tempConfig.put("tableName", tableName);
        tempConfig.put("fieldNameList", fieldNameList);
        tempConfig.put("paramNameList", paramNameList);
        tempConfig.put("fieldTypeList", fieldTypeList);
        tempConfig.put("fieldNameAndParamNameList", fieldNameAndParamNameList);
        tempConfig.put("keyFieldNameList", keyFieldNameList);
        tempConfig.put("keyFieldTypeList", keyFieldTypeList);
        tempConfig.put("keyFieldNameAndParamNameList", keyFieldNameAndParamNameList);
        tableList.put(tempConfig);
        Trace.info("## tableList : " + tableList);
        return tableList;
    }

    public JSONArray handleConfigData(JSONObject jsonObjectData, JSONArray jsonArrayConfigfield) throws Exception {
        Trace.info("## handleConfigData ");
        //Not Have Config
        if (jsonArrayConfigfield.length() <= 0) {
            return jsonArrayConfigfield;
            //throw new RuntimeException("Not Found ConfigName");   
        }
        HashMap<String, Object> tempConfig = new HashMap<>();
        JSONArray tableList = new JSONArray();
        String tableName = "";
        List<String> fieldNameList = new ArrayList<>();
        List<String> paramNameList = new ArrayList<>();
        List<String> fieldTypeList = new ArrayList<>();
        List<String> fieldNameAndParamNameList = new ArrayList<>();
        List<String> keyFieldNameList = new ArrayList<>();
        List<String> keyFieldTypeList = new ArrayList<>();
        List<String> keyFieldNameAndParamNameList = new ArrayList<>();
        List<String> isArrayListFieldName = new ArrayList<>();
        for (int i = 0; i < jsonArrayConfigfield.length(); i++) {
            JSONObject jsonObjectElement = jsonArrayConfigfield.getJSONObject(i);
            //initial Config Data , now not check null value -- is config 
            String tempTableName = jsonObjectElement.getString("TableName");
            String tempFieldName = jsonObjectElement.getString("FieldName");
            String tempParamName = jsonObjectElement.getString("ParamName");
            String tempFieldType = jsonObjectElement.getString("FieldType");
            String tempIsKey = jsonObjectElement.getString("IsKey");
            String tempIsArray = jsonObjectElement.getString("IsArray");
            //String tempIsAutoKey = jsonObjectElement.get("IsAutoKey").getAsString();
//            System.err.println("jsonObjectElement : "+ i + " " + jsonObjectElement);
            //initial loop
            if (tableName.isEmpty()) {
                tableName = tempTableName;
                if (jsonObjectData.has(tempParamName)) {
                    fieldNameList.add(tempFieldName);
                    paramNameList.add(tempParamName);
                    fieldTypeList.add(tempFieldType);
                    fieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                    if ("1".equals(tempIsKey)) {
                        keyFieldNameList.add(tempParamName);
                        keyFieldTypeList.add(tempFieldType);
                        keyFieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                    }
                }
            } else {
                boolean haveTable = false;
                if (tableName.equals(tempTableName)) {
                    haveTable = true;
                    if (jsonObjectData.has(tempParamName)) {
                        fieldNameList.add(tempFieldName);
                        paramNameList.add(tempParamName);
                        fieldTypeList.add(tempFieldType);
                        fieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                        if ("1".equals(tempIsKey)) {
                            keyFieldNameList.add(tempParamName);
                            keyFieldTypeList.add(tempFieldType);
                            keyFieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                        }
                    }
                }
                if (!haveTable) {
                    //Have New Table => Action
                    tempConfig.put("isArrayListFieldName", isArrayListFieldName);
                    tempConfig.put("tableName", tableName);
                    tempConfig.put("fieldNameList", fieldNameList);
                    tempConfig.put("paramNameList", paramNameList);
                    tempConfig.put("fieldTypeList", fieldTypeList);
                    tempConfig.put("fieldNameAndParamNameList", fieldNameAndParamNameList);
                    tempConfig.put("keyFieldNameList", keyFieldNameList);
                    tempConfig.put("keyFieldTypeList", keyFieldTypeList);
                    tempConfig.put("keyFieldNameAndParamNameList", keyFieldNameAndParamNameList);
                    tableList.put(tempConfig);
                    tempConfig = new HashMap<>();

                    //Reset Data
                    tableName = "";
                    fieldNameList = new ArrayList<>();
                    paramNameList = new ArrayList<>();
                    fieldTypeList = new ArrayList<>();
                    fieldNameAndParamNameList = new ArrayList<>();
                    keyFieldNameList = new ArrayList<>();
                    keyFieldTypeList = new ArrayList<>();
                    keyFieldNameAndParamNameList = new ArrayList<>();

                    tableName = tempTableName;
                    if (jsonObjectData.has(tempParamName)) {
                        fieldNameList.add(tempFieldName);
                        paramNameList.add(tempParamName);
                        fieldTypeList.add(tempFieldType);
                        fieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                        if ("1".equals(tempIsKey)) {
                            keyFieldNameList.add(tempParamName);
                            keyFieldTypeList.add(tempFieldType);
                            keyFieldNameAndParamNameList.add(tempFieldName + "=?" + tempParamName);
                        }
                    }
                }
            }
        }
        tempConfig.put("isArrayListFieldName", isArrayListFieldName);
        tempConfig.put("tableName", tableName);
        tempConfig.put("fieldNameList", fieldNameList);
        tempConfig.put("paramNameList", paramNameList);
        tempConfig.put("fieldTypeList", fieldTypeList);
        tempConfig.put("fieldNameAndParamNameList", fieldNameAndParamNameList);
        tempConfig.put("keyFieldNameList", keyFieldNameList);
        tempConfig.put("keyFieldTypeList", keyFieldTypeList);
        tempConfig.put("keyFieldNameAndParamNameList", keyFieldNameAndParamNameList);
        tableList.put(tempConfig);
        Trace.info("## tableList : " + tableList);
        return tableList;
    }

    public Response handleParams(java.sql.Connection connection, String fs_action, JSONArray jsonArrayTableListConfig, JSONObject jsonObjectData) throws Exception {
        Trace.info("## handleParams");

        int effectedTransactions = 0;
//        JSONArray jsonArrayTableListConfig = handleConfigData(jsonObjectData, jsonArrayConfigfield);
//        List<String> isArrayListFieldName = new ArrayList<>();
        List<String> paramNameList;
        jsonObjectResult = new JSONObject();
        JSONObject jsonObjectConfigfield;

        for (int i = 0; i < jsonArrayTableListConfig.length(); i++) {
            jsonObjectConfigfield = jsonArrayTableListConfig.getJSONObject(i);
            System.err.println("jsonObjectConfigfield : " + jsonObjectConfigfield);
            paramNameList = (ArrayList<String>) jsonObjectConfigfield.get("paramNameList");
            System.err.println("paramNameList : " + paramNameList);
//            isArrayListFieldName = (ArrayList<String>) jsonObjectConfigfield.get("isArrayListFieldName");
            for (int j = 0; j < paramNameList.size(); j++) {
                if (jsonObjectData.has(paramNameList.get(j))) {
                    switch (fs_action) {
                        case GlobalBean.RETRIEVE_MODE:
                            effectedTransactions += submitData(connection, jsonObjectData, jsonObjectConfigfield);
                            break;
                        case GlobalBean.INSERT_MODE:
                            effectedTransactions += insertData(connection, jsonObjectData, jsonObjectConfigfield);
                            break;
                        case GlobalBean.UPDATE_MODE:
                            effectedTransactions += updateData(connection, jsonObjectData, jsonObjectConfigfield);
                            break;
                        case GlobalBean.DELETE_MODE:
                            effectedTransactions += deleteData(connection, jsonObjectData, jsonObjectConfigfield);
                            break;
                        case GlobalBean.VALIDATE_MODE:
                            effectedTransactions += validateData(connection, jsonObjectData, jsonObjectConfigfield);
                            break;
                        case INSERT_MODE_NO_KEY:
                            effectedTransactions += insertDataNoKey(connection, jsonObjectData, jsonObjectConfigfield);
                        default:
                            break;
                    }
                    break;
                }
            }
        }
        response.setEffectedTransactions(effectedTransactions);
        response.setJsonResult(jsonObjectResult);
        return response;

    }

    private int validateData(java.sql.Connection connection, JSONObject jsonObjectData, JSONObject jsonObjectConfigfield) throws Exception {
        Trace.info("## validateData");
        Trace.info("## jsonObjectData : " + jsonObjectData);
        Trace.info("## jsonObjectConfigfield : " + jsonObjectConfigfield);
        int count = 0;
        String tableName = jsonObjectConfigfield.getString("tableName");
        List<String> keyFieldNameAndParamNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameAndParamNameList");
        List<String> keyFieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameList");
        List<String> keyFieldTypeList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldTypeList");
        String keyFieldNameAndParamName = String.join(" AND ", keyFieldNameAndParamNameList);
        if (keyFieldNameAndParamNameList.isEmpty()) {
            throw TheConfigFieldException.invalidateConfigFieldIsKeyException();
        }
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT 1 FROM " + tableName + " ");
        knsql.append("WHERE " + keyFieldNameAndParamName);
        for (int i = 0; i < keyFieldNameList.size(); i++) {
            knsql.setParameter(keyFieldNameList.get(i), getParameterValue(jsonObjectData, keyFieldNameList.get(i), keyFieldTypeList.get(i)));
        }
        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
            if (rs.next()) {
                count++;
            } else {
                throw TheConfigFieldException.invalidateNotFound();
            }
        }
        return count;
    }

    public Object requestData(java.sql.Connection connection, KnSQL knsql, boolean isArray) throws Exception {

        if (isArray) {
            JSONArray jsonArray = new JSONArray();
            try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                java.sql.ResultSetMetaData met = rs.getMetaData();
                while (rs.next()) {
                    JSONObject jsonObject = new JSONObject();
                    int colcount = met.getColumnCount();
                    for (int i = 1; i <= colcount; i++) {
                        String colname = met.getColumnLabel(i);
                        jsonObject.put(colname, rs.getString(colname));
                    }
                    jsonArray.put(jsonObject);
                }
            }
            return jsonArray;
        } else {
            JSONObject jsonObject = new JSONObject();
            try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                java.sql.ResultSetMetaData met = rs.getMetaData();
                if (rs.next()) {
                    int colcount = met.getColumnCount();
                    for (int i = 1; i <= colcount; i++) {
                        String colname = met.getColumnLabel(i);
                        jsonObject.put(colname, rs.getString(colname));
                    }
                }
            }
            return jsonObject;
        }

    }

    public Object requestData(java.sql.Connection connection, KnSQL knsql, boolean isArray, HashMap<String, String> configField) throws Exception {
        if (isArray) {
            JSONArray jsonArray = new JSONArray();
            try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                while (rs.next()) {
                    JSONObject jsonObject = new JSONObject();
                    for (Map.Entry<String, String> m : configField.entrySet()) {
                        String result = rs.getString(m.getKey());
                        if (result == null) {
                            jsonObject.put(m.getValue(), "");
                        } else {
                            jsonObject.put(m.getValue(), result);
                        }
                    }
                    jsonArray.put(jsonObject);
                }
            }
            return jsonArray;
        } else {
            JSONObject jsonObject = new JSONObject();
            try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                if (rs.next()) {
                    for (Map.Entry<String, String> m : configField.entrySet()) {
                        String result = rs.getString(m.getKey());
                        if (result == null) {
                            jsonObject.put(m.getValue(), "");
                        } else {
                            jsonObject.put(m.getValue(), result);
                        }
                    }
                }
            }
            return jsonObject;
        }
    }

    private int insertData(java.sql.Connection connection, JSONObject jsonObjectData, JSONObject jsonObjectConfigfield) throws Exception {
        Trace.info("## insertData");
        Trace.info("## jsonObjectData : " + jsonObjectData);
        Trace.info("## jsonObjectConfigfield : " + jsonObjectConfigfield);
        String tableName = jsonObjectConfigfield.getString("tableName");
        List<String> keyFieldNameAndParamNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameAndParamNameList");
        List<String> keyFieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameList");
        List<String> keyFieldTypeList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldTypeList");
        List<String> fieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("fieldNameList");
        List<String> paramNameList = (ArrayList<String>) jsonObjectConfigfield.get("paramNameList");
        List<String> fieldTypeList = (ArrayList<String>) jsonObjectConfigfield.get("fieldTypeList");
        String fieldName = String.join(",", fieldNameList);
        String keyFieldNameAndParamName = String.join(" AND ", keyFieldNameAndParamNameList);
//        JSONObject jsonData = new JSONObject();
//        JSONObject jsonKeyAndValue = new JSONObject();
        int count = 0;
        //Check Duplicate
        // If table not have primary key cannot insert
        if (keyFieldNameAndParamNameList.isEmpty()) {
            throw TheConfigFieldException.invalidateConfigFieldIsKeyException();
        }
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT 1 FROM " + tableName + " ");
        knsql.append("WHERE " + keyFieldNameAndParamName);
        for (int i = 0; i < keyFieldNameList.size(); i++) {
            if (!jsonObjectData.has(keyFieldNameList.get(i)) || "".equals(jsonObjectData.get(keyFieldNameList.get(i)))) {
                Trace.info("## key null");
                return 0;
            }
            knsql.setParameter(keyFieldNameList.get(i), getParameterValue(jsonObjectData, keyFieldNameList.get(i), keyFieldTypeList.get(i)));
//            jsonKeyAndValue.put(keyFieldNameList.get(i), jsonObjectData.get(keyFieldNameList.get(i)));
        }
        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
            if (rs.next()) {
                count++;
            }
        }
        if (count > 0) {
            throw TheConfigFieldException.duplicateException();
        } else {
            //Create SQL Insert
            KnSQL knsqlInsert = new KnSQL(this);
            knsqlInsert.append("INSERT INTO " + tableName + " (" + fieldName + ") ");
            knsqlInsert.append("VALUES (");
            String filter = "";
            for (int i = 0; i < paramNameList.size(); i++) {
                knsqlInsert.append(filter + "?" + paramNameList.get(i));
                //checkKey if null generate uuid
                knsqlInsert.setParameter(paramNameList.get(i), checkKey(jsonObjectData, paramNameList.get(i), fieldTypeList.get(i), keyFieldNameList));
                filter = ",";
//                jsonData.put(fieldNameList.get(i), jsonObjectData.get(paramNameList.get(i)));
            }
            knsqlInsert.append(")");
//            transactionLog(connection, "", GlobalBean.INSERT_MODE, tableName, jsonKeyAndValue, null, jsonData, "");
            return knsqlInsert.executeUpdate(connection);
        }
    }

    private int insertDataNoKey(java.sql.Connection connection, JSONObject jsonObjectData, JSONObject jsonObjectConfigfield) throws Exception {
        Trace.info("## insertDataNoKey");
        Trace.info("## jsonObjectData : " + jsonObjectData);
        Trace.info("## jsonObjectConfigfield : " + jsonObjectConfigfield);
        String tableName = jsonObjectConfigfield.getString("tableName");
        List<String> keyFieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameList");
        List<String> fieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("fieldNameList");
        List<String> paramNameList = (ArrayList<String>) jsonObjectConfigfield.get("paramNameList");
        List<String> fieldTypeList = (ArrayList<String>) jsonObjectConfigfield.get("fieldTypeList");
        String fieldName = String.join(",", fieldNameList);

        //Create SQL Insert
        KnSQL knsqlInsert = new KnSQL(this);
        knsqlInsert.append("INSERT INTO " + tableName + " (" + fieldName + ") ");
        knsqlInsert.append("VALUES (");
        String filter = "";
        for (int i = 0; i < paramNameList.size(); i++) {
            knsqlInsert.append(filter + "?" + paramNameList.get(i));
            //checkKey if null generate uuid
            knsqlInsert.setParameter(paramNameList.get(i), checkKey(jsonObjectData, paramNameList.get(i), fieldTypeList.get(i), keyFieldNameList));
            filter = ",";
        }
        knsqlInsert.append(")");
        return knsqlInsert.executeUpdate(connection);
    }

    private int updateData(java.sql.Connection connection, JSONObject jsonObjectData, JSONObject jsonObjectConfigfield) throws Exception {
        Trace.info("## updateData ");
        Trace.info("## jsonObjectData : " + jsonObjectData);
        Trace.info("## jsonObjectConfigfield : " + jsonObjectConfigfield);
        String tableName = jsonObjectConfigfield.getString("tableName");
        List<String> keyFieldNameAndParamNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameAndParamNameList");
        List<String> keyFieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameList");
        List<String> paramNameList = (ArrayList<String>) jsonObjectConfigfield.get("paramNameList");
        List<String> fieldTypeList = (ArrayList<String>) jsonObjectConfigfield.get("fieldTypeList");
        List<String> fieldNameAndParamNameList = (ArrayList<String>) jsonObjectConfigfield.get("fieldNameAndParamNameList");
        List<String> fieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("fieldNameList");
        String fieldNameAndParamName = String.join(",", fieldNameAndParamNameList);
        String keyFieldNameAndParamName = String.join(" AND ", keyFieldNameAndParamNameList);
//        JSONObject jsonData = new JSONObject();
//        JSONObject jsonOldData = new JSONObject();
//        JSONObject jsonKeyAndValue = new JSONObject();

        if (keyFieldNameAndParamNameList.isEmpty()) {
            Trace.info("## keyFieldNameAndParamNameList empty return 0");
            return 0;
//            throw TheConfigFieldException.invalidateConfigFieldIsKeyException();
        }
        if (keyFieldNameList.size() == paramNameList.size()) {
            Trace.info("## Params size == key size return 0");
            return 0;
        }

        /*
        // For TransactionLog 
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT * FROM " + tableName + " ");
        knsql.append("WHERE " + keyFieldNameAndParamName);
        for (int i = 0; i < keyFieldNameList.size(); i++) {
            if (!jsonObjectData.has(keyFieldNameList.get(i)) || "".equals(jsonObjectData.get(keyFieldNameList.get(i)))) {
                throw TheConfigFieldException.invalidateParameterKeyException();
            }
            knsql.setParameter(keyFieldNameList.get(i), jsonObjectData.get(keyFieldNameList.get(i)).toString());
//            jsonKeyAndValue.put(keyFieldNameList.get(i), jsonObjectData.get(keyFieldNameList.get(i)));
        }
        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
            java.sql.ResultSetMetaData met = rs.getMetaData();
            if (rs.next()) {
                int colcount = met.getColumnCount();
                for (int i = 1; i <= colcount; i++) {
                    String colname = met.getColumnName(i);
//                    jsonOldData.put(colname, rs.getString(colname));
                }
            }
        }
        // For TransactionLog 
         */
        KnSQL knsqlUpdate = new KnSQL(this);
        knsqlUpdate.append("UPDATE " + tableName + " SET " + fieldNameAndParamName + " ");
        knsqlUpdate.append("WHERE " + keyFieldNameAndParamName);
        for (int i = 0; i < paramNameList.size(); i++) {
            knsqlUpdate.setParameter(paramNameList.get(i), getParameterValue(jsonObjectData, paramNameList.get(i), fieldTypeList.get(i)));
//            jsonData.put(fieldNameList.get(i), jsonObjectData.get((paramNameList.get(i))));
        }
//        transactionLog(connection, globalVariable.getFs_apiName(), GlobalBean.UPDATE_MODE, tableName, jsonKeyAndValue, jsonOldData, jsonData, "");
        return knsqlUpdate.executeUpdate(connection);
    }

    private int deleteData(java.sql.Connection connection, JSONObject jsonObjectData, JSONObject jsonObjectConfigfield) throws Exception {
        Trace.info("## deleteData ");
        Trace.info("## jsonObjectData : " + jsonObjectData);
        Trace.info("## jsonObjectConfigfield : " + jsonObjectConfigfield);
        String tableName = jsonObjectConfigfield.getString("tableName");
        List<String> keyFieldNameAndParamNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameAndParamNameList");
        List<String> keyFieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameList");
        String keyFieldNameAndParamName = String.join(" AND ", keyFieldNameAndParamNameList);
//        JSONObject jsonOldData = new JSONObject();
//        JSONObject jsonKeyAndValue = new JSONObject();

        if (keyFieldNameAndParamNameList.isEmpty()) {
            throw TheConfigFieldException.invalidateConfigFieldIsKeyException();
        }
        /*
        // For TransactionLog 
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT * FROM " + tableName + " ");
        knsql.append("WHERE " + keyFieldNameAndParamName);
        for (int i = 0; i < keyFieldNameList.size(); i++) {
            if (!jsonObjectData.has(keyFieldNameList.get(i)) || "".equals(jsonObjectData.get(keyFieldNameList.get(i)))) {
                throw TheConfigFieldException.invalidateParameterKeyException();
            }
            knsql.setParameter(keyFieldNameList.get(i), jsonObjectData.get(keyFieldNameList.get(i)).toString());
//            jsonKeyAndValue.put(keyFieldNameList.get(i), jsonObjectData.get(keyFieldNameList.get(i)));
        }
        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
            java.sql.ResultSetMetaData met = rs.getMetaData();
            if (rs.next()) {
                int colcount = met.getColumnCount();
                for (int i = 1; i <= colcount; i++) {
                    String colname = met.getColumnName(i);
//                    jsonOldData.put(colname, rs.getString(colname));
                }
            }
        }
        // For TransactionLog 
         */
        KnSQL knsqlDelete = new KnSQL(this);
        knsqlDelete.append("DELETE FROM " + tableName + " ");
        knsqlDelete.append("WHERE " + keyFieldNameAndParamName);
        for (int i = 0; i < keyFieldNameList.size(); i++) {
            if (!jsonObjectData.has(keyFieldNameList.get(i)) || "".equals(jsonObjectData.get(keyFieldNameList.get(i)))) {
                throw TheConfigFieldException.invalidateParameterKeyException();
            }
            knsqlDelete.setParameter(keyFieldNameList.get(i), jsonObjectData.getString(keyFieldNameList.get(i)));
            jsonObjectResult.put(keyFieldNameList.get(i), jsonObjectData.getString(keyFieldNameList.get(i)));
        }
//        transactionLog(connection, globalVariable.getFs_apiName(), GlobalBean.DELETE_MODE, tableName, jsonKeyAndValue, jsonOldData, null, "");
        return knsqlDelete.executeUpdate(connection);
    }

    private int submitData(java.sql.Connection connection, JSONObject jsonObjectData, JSONObject jsonObjectConfigfield) throws Exception {
        Trace.info("## submitData");
        Trace.info("## jsonObjectData : " + jsonObjectData);
        Trace.info("## jsonObjectConfigfield : " + jsonObjectConfigfield);
        String tableName = jsonObjectConfigfield.getString("tableName");
        List<String> keyFieldNameAndParamNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameAndParamNameList");
        List<String> keyFieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldNameList");
        List<String> keyFieldTypeList = (ArrayList<String>) jsonObjectConfigfield.get("keyFieldTypeList");
        List<String> fieldNameList = (ArrayList<String>) jsonObjectConfigfield.get("fieldNameList");
        List<String> paramNameList = (ArrayList<String>) jsonObjectConfigfield.get("paramNameList");
        List<String> fieldTypeList = (ArrayList<String>) jsonObjectConfigfield.get("fieldTypeList");
        List<String> fieldNameAndParamNameList = (ArrayList<String>) jsonObjectConfigfield.get("fieldNameAndParamNameList");
        String fieldName = String.join(",", fieldNameList);
        String keyFieldNameAndParamName = String.join(" AND ", keyFieldNameAndParamNameList);
        String fieldNameAndParamName = String.join(",", fieldNameAndParamNameList);
//        JSONObject jsonData = new JSONObject();
//        JSONObject jsonOldData = new JSONObject();
//        JSONObject jsonKeyAndValue = new JSONObject();
        int count = 0;
        //Check Duplicate
        if (keyFieldNameAndParamNameList.isEmpty()) {
            //Have Auto Increment
            KnSQL knsqlInsert = new KnSQL(this);
            knsqlInsert.append("INSERT INTO " + tableName + " (" + fieldName + ") ");
            knsqlInsert.append("VALUES (");
            String filter = "";
            for (int i = 0; i < paramNameList.size(); i++) {
                knsqlInsert.append(filter + "?" + paramNameList.get(i));
                knsqlInsert.setParameter(paramNameList.get(i), getParameterValue(jsonObjectData, paramNameList.get(i), fieldTypeList.get(i)));
                filter = ",";
//                jsonData.put(fieldNameList.get(i), jsonObjectData.get(paramNameList.get(i)));
            }
            knsqlInsert.append(")");

//            transactionLog(connection, globalVariable.getFs_apiName(), GlobalBean.INSERT_MODE, tableName, null, null, jsonData, "");
            return knsqlInsert.executeUpdate(connection);
        }

        if (keyFieldNameList.size() == paramNameList.size()) {
            Trace.info("## Params size == key size return 0");
            return 0;
        }

        // For TransactionLog 
        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT 1 FROM " + tableName + " ");
        knsql.append("WHERE " + keyFieldNameAndParamName);
        for (int i = 0; i < keyFieldNameList.size(); i++) {
            if (!jsonObjectData.has(keyFieldNameList.get(i)) || "".equals(jsonObjectData.get(keyFieldNameList.get(i)))) {
                return 0;
            }
            knsql.setParameter(keyFieldNameList.get(i), getParameterValue(jsonObjectData, keyFieldNameList.get(i), keyFieldTypeList.get(i)));
//            jsonKeyAndValue.put(keyFieldNameList.get(i), jsonObjectData.get(keyFieldNameList.get(i)));
        }
        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
//            java.sql.ResultSetMetaData met = rs.getMetaData();
            if (rs.next()) {
                count++;
                /*int colcount = met.getColumnCount();
                for (int i = 1; i <= colcount; i++) {
                    String colname = met.getColumnName(i);
//                    jsonOldData.put(colname, rs.getString(colname));
                }*/
            }
        }
        //  For TransactionLog 

        if (count > 0) {
            //No Data to update Have only key

            //Duplicate
            KnSQL knsqlUpdate = new KnSQL(this);
            knsqlUpdate.append("UPDATE " + tableName + " SET " + fieldNameAndParamName + " ");
            knsqlUpdate.append("WHERE " + keyFieldNameAndParamName);
            for (int i = 0; i < paramNameList.size(); i++) {
                knsqlUpdate.setParameter(paramNameList.get(i), getParameterValue(jsonObjectData, paramNameList.get(i), fieldTypeList.get(i)));
//                jsonData.put(fieldNameList.get(i), jsonObjectData.get(paramNameList.get(i)));
            }
//            transactionLog(connection, globalVariable.getFs_apiName(), GlobalBean.UPDATE_MODE, tableName, jsonKeyAndValue, jsonOldData, jsonData, "");
            return knsqlUpdate.executeUpdate(connection);
        } else {
            //Create SQL Insert
            KnSQL knsqlInsert = new KnSQL(this);
            knsqlInsert.append("INSERT INTO " + tableName + " (" + fieldName + ") ");
            knsqlInsert.append("VALUES (");
            String filter = "";
            for (int i = 0; i < paramNameList.size(); i++) {
                knsqlInsert.append(filter + "?" + paramNameList.get(i));
                //checkKey if null generate uuid
                knsqlInsert.setParameter(paramNameList.get(i), checkKey(jsonObjectData, paramNameList.get(i), fieldTypeList.get(i), keyFieldNameList));
                filter = ",";
//                jsonData.put(fieldNameList.get(i), jsonObjectData.get(paramNameList.get(i)));
            }
            knsqlInsert.append(")");
//            transactionLog(connection, globalVariable.getFs_apiName(), GlobalBean.INSERT_MODE, tableName, jsonKeyAndValue, null, jsonData, "");
            return knsqlInsert.executeUpdate(connection);
        }
    }

    /*
    private void transactionLog(java.sql.Connection connection, String apiName, String actionName, String tableName, JSONObject keyAndValue, JSONObject oldData, JSONObject data, String remark) throws Exception {
        //validate JSONObject
        String tempKeyAndValue = keyAndValue != null ? keyAndValue.toString() : "";
        String tempOldData = oldData != null ? oldData.toString() : "";
        String tempData = data != null ? data.toString() : "";

        KnSQL knsqlInsert = new KnSQL(this);
        knsqlInsert.append("INSERT INTO t_transactionlog (ApiName, ActionName, TableName, KeyAndValue,OldData,Data,Remark) ");
        knsqlInsert.append("VALUES (?apiName,?actionName,?tableName,?keyAndValue,?oldData,?data,?remark) ");
        knsqlInsert.setParameter("apiName", apiName);
        knsqlInsert.setParameter("actionName", actionName);
        knsqlInsert.setParameter("tableName", tableName);
        knsqlInsert.setParameter("keyAndValue", tempKeyAndValue);
        knsqlInsert.setParameter("oldData", tempOldData);
        knsqlInsert.setParameter("data", tempData);
        knsqlInsert.setParameter("remark", remark);
        knsqlInsert.executeUpdate(connection);
    }
     */
    private Object getParameterValue(JSONObject jsonData, String paramName, String typeName) throws Exception {
        typeName = typeName.toLowerCase();
        //Check null param and value
        if (!jsonData.has(paramName) || "".equals(jsonData.getString(paramName).trim())) {
            return null;
        }
        String value = jsonData.getString(paramName).trim().replaceAll("\\s+", " ");
        switch (typeName) {
            case "bit":
                if ("1".equals(value)) {
                    return true;
                } else {
                    return false;
                }
            default:
                return value;
        }
    }

    //If want check Key is null AND GENERATE-------------
    private Object checkKey(JSONObject jsonData, String paramName, String typeName, List<String> keyNameList) throws Exception {
        for (int i = 0; i < keyNameList.size(); i++) {
            if (keyNameList.get(i).equals(paramName)) {
                if (jsonData.has(paramName) && !"".equals(jsonData.get(paramName))) {
                    jsonObjectResult.put(keyNameList.get(i), jsonData.getString(paramName));
                    return getParameterValue(jsonData, paramName, typeName);
                }
            }
        }
        return getParameterValue(jsonData, paramName, typeName);
    }

    public static class HandleException {

        public static void sqlException(SQLException e) throws Exception {
            if (e.getErrorCode() == 2627) {
                throw new RuntimeException("Violation of UNIQUE KEY constraint");
            }
        }

    }

    private abstract static class TheConfigFieldException {

        public static RuntimeException duplicateException() {
            throw new RuntimeException("Duplicate data entry.");
        }

        public static RuntimeException invalidateConfigFieldIsKeyException() {
            throw new RuntimeException("Invalidate ConfigField Without Primary Key.");
        }

        public static RuntimeException invalidateParameterKeyException() {
            throw new RuntimeException("Invalidate Parameter Without Primary Key.");
        }

        public static RuntimeException invalidateConfigNameException() {
            throw new RuntimeException("Not Found ConfigName In ConfigField.");
        }

        public static RuntimeException invalidateParameterIsArrayException() {
            throw new RuntimeException("Parammeters IsArray Is Not Json.");
        }

        public static RuntimeException invalidateNotFound() {
            throw new RuntimeException("Not Found");
        }

    }

    public static class Generate {

        public static String createUUID() {
            String uuid = com.fs.bean.dba.DBAUtility.createID();
            return uuid;
        }
    }

    public class Response {

        int effectedTransactions;
        JSONObject jsonResult;

        public int getEffectedTransactions() {
            return effectedTransactions;
        }

        public void setEffectedTransactions(int effectedTransactions) {
            this.effectedTransactions = effectedTransactions;
        }

        public JSONObject getJsonResult() {
            return jsonResult;
        }

        public void setJsonResult(JSONObject jsonResult) {
            this.jsonResult = jsonResult;
        }
    }

}
