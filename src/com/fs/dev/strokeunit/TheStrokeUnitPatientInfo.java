/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fs.dev.strokeunit;

import com.fs.bean.misc.KnSQL;
import com.fs.bean.util.GlobalBean;
import java.sql.Connection;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Supakit_Pud
 */
public class TheStrokeUnitPatientInfo {

    final String fs_action = GlobalBean.RETRIEVE_MODE;
    final String fs_actionDel = GlobalBean.DELETE_MODE;
    final String fs_actionIns = GlobalBean.INSERT_MODE;

    public JSONObject request(Connection connection, JSONObject json, String fs_apiName) throws Exception {
        TheStrokeUnit su = new TheStrokeUnit();
        String patientId = json.has("patientId") ? json.getString("patientId") : "";
        String hn = json.has("hn") ? json.getString("hn") : "";

        KnSQL knsql = new KnSQL(this);
        knsql.append("SELECT p.PatientId , rn.InitiateRN , p.Ward , p.HN  , p.AN  , p.PatientName  , p.Age  , p.Gender , p.LastSeenNormal , p.StrokeOnset  ,p.AdmissionDate , p.BedId , p.NSId  ,ns.NIHSS ,ns.NIHSSTotal ");
        knsql.append(",ns.GCSE ,ns.GCSV , ns.GCSM ,ns.MRS , ns.ADL ,p.DCId ,dc.DCDate ,dc.LOS ,dc.StatusId ,dc.DCToId  ,vs.VitalSignId ,vs.VitalSignTime ,vs.Temperature  ,vs.HeartRate  , vs.RespiratoryRate , vs.SBP  , vs.DBP ,vs.OxygenSat ");
        knsql.append(" FROM Tw_PatientInfo as p ");
        knsql.append(" LEFT JOIN Tw_VitalSign as vs ON p.VitalSignId = vs.VitalSignId ");
        knsql.append(" LEFT JOIN Tw_NSScore as ns ON p.PatientId = ns.PatientId AND ns.FormId = 'PatientInfo' ");
        knsql.append(" LEFT JOIN Tw_Discharge as dc ON p.PatientId = dc.PatientId AND dc.FormId = 'PatientInfo' ");
        knsql.append(" LEFT JOIN Tw_RNofFrom as rn ON p.PatientId = rn.PatientId AND rn.FormId = 'RNP01' ");
        if (fs_apiName.equals("PatientInfoByHN")) {
            knsql.append(" WHERE p.HN = ?hn AND p.PatientId = ?patientId ");
            knsql.append(" ORDER BY p.AdmissionDate DESC ");
            knsql.setParameter("hn", hn);
            knsql.setParameter("patientId", patientId);
        } else {
            knsql.append(" WHERE p.PatientId = ?patientId ");
            knsql.setParameter("patientId", patientId);
        }

        HashMap<String, String> configFieldPI = new HashMap();
        configFieldPI.put("PatientId", "patientId");
        configFieldPI.put("HN", "hn");
        configFieldPI.put("AN", "an");
        configFieldPI.put("PatientName", "patientName");
        configFieldPI.put("Age", "age");
        configFieldPI.put("Gender", "gender");
        configFieldPI.put("LastSeenNormal", "lastSeenNormal");
        configFieldPI.put("StrokeOnset", "strokeOnset");
        configFieldPI.put("AdmissionDate", "admissionDate");
        configFieldPI.put("BedId", "bedId");
        configFieldPI.put("NSId", "nsId");
        configFieldPI.put("NIHSS", "nihss");
        configFieldPI.put("NIHSSTotal", "nihssTotal");
        configFieldPI.put("GCSE", "gcse");
        configFieldPI.put("GCSV", "gcsv");
        configFieldPI.put("GCSM", "gcsm");
        configFieldPI.put("MRS", "mRS");
        configFieldPI.put("ADL", "adl");
        configFieldPI.put("DCId", "dcId");
        configFieldPI.put("DCDate", "dcDate");
        configFieldPI.put("LOS", "dcLOS");
        configFieldPI.put("StatusId", "dcStatusId");
        configFieldPI.put("DCToId", "dcToId");
        configFieldPI.put("VitalSignId", "vsId");
        configFieldPI.put("VitalSignTime", "vsTime");
        configFieldPI.put("Temperature", "vsTemp");
        configFieldPI.put("HeartRate", "vsHR");
        configFieldPI.put("RespiratoryRate", "vsRR");
        configFieldPI.put("SBP", "vsSBP");
        configFieldPI.put("DBP", "vsDBP");
        configFieldPI.put("OxygenSat", "vsO2sat");
        configFieldPI.put("InitiateRN", "rn");
        configFieldPI.put("Ward", "ward");
        JSONObject jsonPatientInfo = new JSONObject(su.requestData(connection, knsql, false, configFieldPI).toString());
        if (jsonPatientInfo.length() > 0) {
            jsonPatientInfo.put("errorCode", 0);
        } else {
            jsonPatientInfo.put("errorCode", 2);
        }

        knsql = new KnSQL(this);
        knsql.append("SELECT Tw_StrokeType.StrokeTypeId  , Tw_StrokeType.Other ,Tw_StrokeType.IsrtPA ,Tw_StrokeType.IsThrombectomy , Tw_StrokeType.IsNontreatment ");
        knsql.append("FROM Tw_StrokeType ");
        knsql.append("WHERE PatientId = ?patientId ");
        knsql.setParameter("patientId", patientId);
        HashMap<String, String> configFieldST = new HashMap();
        configFieldST.put("StrokeTypeId", "strokeTypeId");
        configFieldST.put("Other", "other");
        configFieldST.put("IsrtPA", "isrtPA");
        configFieldST.put("IsThrombectomy", "isThrombec");
        configFieldST.put("IsNontreatment", "isNonTreatment");
        JSONArray jsonStrokeType = new JSONArray(su.requestData(connection, knsql, true, configFieldST).toString());

        knsql = new KnSQL(this);
        knsql.append("SELECT Tw_Medication.MedicationId ");
        knsql.append("FROM Tw_Medication ");
        knsql.append("WHERE PatientId = ?patientId ");
        knsql.setParameter("patientId", patientId);
        HashMap<String, String> configFieldMd = new HashMap();
        configFieldMd.put("MedicationId", "medicationId");
        JSONArray jsonMedication = new JSONArray(su.requestData(connection, knsql, true, configFieldMd).toString());
        jsonPatientInfo.put("strokeType", jsonStrokeType);
        jsonPatientInfo.put("medication", jsonMedication);
        return jsonPatientInfo;
    }

    public JSONObject submit(Connection connection, JSONObject json, String fs_apiName) throws Exception {
        JSONObject result = new JSONObject();
        //initial
        TheStrokeUnit su = new TheStrokeUnit();
        result.put("errorCode", 0);
        final String fs_fromId = "PatientInfo";
        String patientId = "";
        String vsId = "";
        String dcId = "";
        String nsId = "";

        if (!(json.has("patientId") && json.getString("patientId").trim().length() > 0)) {
            patientId = TheStrokeUnit.Generate.createUUID();
            json.put("patientId", patientId);
            result.put("patientId", patientId);
            //New Id check bedId
            if (json.has("bedId") && json.getString("bedId").trim().length() > 0) {
                String bedId = json.getString("bedId");
                KnSQL knsql = new KnSQL(this);
                knsql.append("SELECT 1 ");
                knsql.append("FROM Tw_PatientInfo ");
                knsql.append("WHERE Tw_PatientInfo.BedId = ?bedId AND Tw_PatientInfo.StatusCode IN ('N') AND Tw_PatientInfo.IsActive = '1' ");
                knsql.setParameter("bedId", bedId);
                try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                    if (rs.next()) {
                        result.put("errorCode", -4091);
                        return result;
                    } else {
                        json.put("isAvail", 0);
                    }
                }
            } else {
                result.put("errorCode", -204);
                return result;
            }

        } else {
            patientId = json.getString("patientId");
            json.remove("bedId");
        }

        String hn = json.has("hn") ? json.getString("hn") : "";
        if (hn.length() > 0) {
            //check HN Duplicate
            KnSQL knsql = new KnSQL(this);
            knsql.append("SELECT 1 ");
            knsql.append("FROM Tw_PatientInfo ");
            knsql.append("WHERE HN =?hn AND IsActive = '1' AND StatusCode IN ('N') AND patientId != ?patientId");
            knsql.setParameter("hn", hn);
            knsql.setParameter("patientId", patientId);
            try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                if (rs.next()) {
                    result.put("errorCode", -4092);
                    return result;
                }
            }
        }

        if (!(json.has("vsId") && json.getString("vsId").trim().length() > 0) && (json.has("vsTime") && json.getString("vsTime").trim().length() > 0)) {
            vsId = TheStrokeUnit.Generate.createUUID();
            json.put("vsId", vsId);
            result.put("vsId", vsId);
            /*
                        json.put("indicatePulseId", "");
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT 1 FROM Tw_IndicatePulse ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                            if (!rs.next()) {
                                String vsTimeString = json.getString("vsTime");
                                Date pulseDate = df.parse(vsTimeString);
//                                Date pulseTime = dfTime.parse(vsTimeString);
                                System.err.println("vsTimeString : " + vsTimeString);
                                System.err.println("pulseDate : " + pulseDate);
                                //insert to indicate Pulse 
//                            Date pulseDate = new Date();
                                Calendar calpulseDate = Calendar.getInstance(java.util.Locale.US);
                                calpulseDate.setTime(pulseDate);
                                int hour = calpulseDate.get(Calendar.HOUR_OF_DAY);
                                System.err.println("hour : " + hour);
                                System.err.println(" hour - 2 - (hour % 4) : " + (hour - 2 - (hour % 4)));
                                calpulseDate.set(Calendar.HOUR_OF_DAY, hour - 2 - (hour % 4));
                                calpulseDate.set(Calendar.MINUTE, 0);
                                calpulseDate.set(Calendar.SECOND, 0);
                                calpulseDate.set(Calendar.MILLISECOND, 0);
                                System.err.println("calpulseDate.getTime() : " + calpulseDate.getTime());
                                indicatePulseId = TheStrokeUnit.Generate.createUUID();
                                json.put("indicatePulseId", indicatePulseId);
                                json.put("pulseTime", dfTime.format(calpulseDate.getTime()));
                                String vsPulseId = TheStrokeUnit.Generate.createUUID();
                                json.put("vsPulseId", vsPulseId);
                                KnSQL knsqlInsert = new KnSQL(this);
                                knsqlInsert.append("INSERT INTO Tw_VitalSign (VitalSignId,PatientId,VitalSignTime,FormId,HeartRate,RespiratoryRate,SBP,DBP,OxygenSat,Temperature ) ");
                                knsqlInsert.append("VALUES (?vsPulseId,?patientId,?vsTime,?formId,?vsHR,?vsRR,?vsSBP,?vsDBP,?vsO2sat,?vsTemp)");
                                knsqlInsert.setParameter("vsPulseId", vsPulseId);
                                knsqlInsert.setParameter("patientId", patientId);
                                knsqlInsert.setParameter("vsTime", json.getString("vsTime"));
                                knsqlInsert.setParameter("formId", "ListChart");
                                knsqlInsert.setParameter("vsHR", json.has("vsHR") ? json.getString("vsHR") : "");
                                knsqlInsert.setParameter("vsRR", json.has("vsRR") ? json.getString("vsRR") : "");
                                knsqlInsert.setParameter("vsSBP", json.has("vsSBP") ? json.getString("vsSBP") : "");
                                knsqlInsert.setParameter("vsDBP", json.has("vsDBP") ? json.getString("vsDBP") : "");
                                knsqlInsert.setParameter("vsO2sat", json.has("vsO2sat") ? json.getString("vsO2sat") : "");
                                knsqlInsert.setParameter("vsTemp", json.has("vsTemp") ? json.getString("vsTemp") : "");
                                knsqlInsert.executeUpdate(connection);
                            }
                        }
             */
        }

        if (!(json.has("dcId") && json.getString("dcId").trim().length() > 0)) {
            dcId = TheStrokeUnit.Generate.createUUID();
            json.put("dcId", dcId);
            result.put("dcId", dcId);
        }
        if (!(json.has("nsId") && json.getString("nsId").trim().length() > 0)) {
            nsId = TheStrokeUnit.Generate.createUUID();
            json.put("nsId", nsId);
            result.put("nsId", nsId);
        }

        json.put("formId", fs_fromId);
        JSONArray jsonArrayConfigfield = su.getConfigfield(connection, fs_apiName);
        if (jsonArrayConfigfield.length() <= 0) {
            throw new RuntimeException("Not Found ApiName");
        }
        JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);
        System.err.println("jsonArrayTableListConfig : " + jsonArrayTableListConfig);
        JSONArray tempDelStrokeType = new JSONArray();
        JSONArray tempDelMedication = new JSONArray();
        for (int i = 0; i < jsonArrayTableListConfig.length(); i++) {
            JSONObject jsonObjectElement = jsonArrayTableListConfig.getJSONObject(i);
            if ("Tw_StrokeType".equals(jsonObjectElement.getString("tableName"))) {
                tempDelStrokeType.put(jsonObjectElement);
            } else if ("Tw_Medication".equals(jsonObjectElement.getString("tableName"))) {
                tempDelMedication.put(jsonObjectElement);
            }
        }
        su.handleParams(connection, fs_action, jsonArrayTableListConfig, json);
        if (json.has("strokeType")) {
            JSONArray jsonStrokeType = json.getJSONArray("strokeType");
            JSONArray jsonArrayConfigfieldStrokeType = su.getConfigfield(connection, fs_apiName, "Tw_StrokeType");
            JSONArray jsonArrayTableListConfigStrokeType = su.handleConfigAllData(jsonArrayConfigfieldStrokeType);
            su.handleParams(connection, fs_actionDel, tempDelStrokeType, json);
            for (int i = 0; i < jsonStrokeType.length(); i++) {
                JSONObject jsonObjectElement = jsonStrokeType.getJSONObject(i);
                jsonObjectElement.put("patientId", patientId);
                jsonObjectElement.put("formId", fs_fromId);
                su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigStrokeType, jsonObjectElement);
            }
        }
        if (json.has("medication")) {
            JSONArray jsonMedication = json.getJSONArray("medication");
            JSONArray jsonArrayConfigfieldMedication = su.getConfigfield(connection, fs_apiName, "Tw_Medication");
            JSONArray jsonArrayTableListConfigMedication = su.handleConfigAllData(jsonArrayConfigfieldMedication);
            su.handleParams(connection, fs_actionDel, tempDelMedication, json);
            for (int i = 0; i < jsonMedication.length(); i++) {
                JSONObject jsonObjectElement = jsonMedication.getJSONObject(i);
                jsonObjectElement.put("patientId", patientId);
                su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigMedication, jsonObjectElement);
            }
        }

        return result;
    }

}
