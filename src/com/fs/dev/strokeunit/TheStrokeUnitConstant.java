/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fs.dev.strokeunit;

/**
 *
 * @author Supakit_Pud
 */
public class TheStrokeUnitConstant {

    public static final String SYS_CONFIG_DESC = "ConfigDesc";
    public static final String SYS_CONFIG_NAME = "ConfigName";
    public static final String SYS_CONFIG_CATEGORY = "Category";
    public static final String SYS_CONFIG_VALUE = "Value";
    public static final String SYS_CONFIG_UNIT = "Unit";

    
    public static final String SUCCESS = "SUCCESS";
}
