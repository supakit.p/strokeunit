/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fs.dev.strokeunit;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Supakit_Pud
 */
public class HttpClientServices {

    final public static String CONTENT_TYPE = "Content-Type";
    final public static String CONTENT_TYPE_APPLICATION_JSON = "application/json;charset=UTF-8";
    final public static String CONTENT_TYPE_MULTIPART_FORM_DATA = "multipart/form-data";
    final public static String CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";

    public static HttpPost post;
    public static JSONObject result;
    public static int CONNECTION_TIMEOUT_S = 5;
    public static JSONObject sendPost(URI url, HashMap<String, String> headers, Object parameters) {
        System.err.println("url : " + url);
        System.err.println("headers : " + headers);
        try {
            post = new HttpPost();
            post.setURI(url);
            setTimeout();
            setHeader(headers);
            setParameters(parameters);
            try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(post)) {
                result = new JSONObject(EntityUtils.toString(response.getEntity()));
            }
            return result;
        } catch (IOException | JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static void setTimeout() {
        int CONNECTION_TIMEOUT_MS = CONNECTION_TIMEOUT_S * 1000;
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
                .setConnectTimeout(CONNECTION_TIMEOUT_MS)
                .setSocketTimeout(CONNECTION_TIMEOUT_MS)
                .build();
        post.setConfig(requestConfig);
    }

    private static void setHeader(HashMap<String, String> headers) {
        if (headers != null) {
            for (String header : headers.keySet()) {
                post.addHeader(header, headers.get(header));
            }
        }
    }

    private static void setParameters(Object parameters) {
        if (parameters != null) {
            if (parameters instanceof List) {
                //application/x-www-form-urlencoded
                try {
                    List<NameValuePair> params = (ArrayList<NameValuePair>) parameters;
                    post.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
                    System.err.println("params : " + params);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            } else if (parameters instanceof JSONObject) {
                //application/json
                try {
                    JSONObject paramsJson = (JSONObject) parameters;
                    post.setEntity(new StringEntity(paramsJson.toString()));
                    System.err.println("paramsJson : " + paramsJson);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            } else if (parameters instanceof HttpEntity) {
                //multipart/form-data
                HttpEntity entity = (HttpEntity) parameters;
                post.setEntity(entity);
                System.err.println("entity : " + entity);
            }
        }
    }

}

// example URL && Header
/*
        URI uri = new URI("http://localhost:8080/carloan/rest/upload/apiaddimage/submit");
        HashMap<String, String> headers = new HashMap();
        headers.put(HttpClientServices.CONTENT_TYPE, HttpClientServices.CONTENT_TYPE_MULTIPART_FORM_DATA);
 */
// example params application/x-www-form-urlencoded
/*
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("userName", "service@plending.co.th"));
        urlParameters.add(new BasicNameValuePair("userPwd", "P@ssw0rd1"));
 */
// example params application/json;charset=UTF-8
/*
        org.json.JSONObject user = new org.json.JSONObject();
        user.put("name", URLEncoder.encode(fs_username, "UTF-8"));
        user.put("pwd", URLEncoder.encode(fs_password, "UTF-8"));
 */
// example params multipart/form-data
/*
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();  
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addBinaryBody(paramFileName, new File(file), ContentType.DEFAULT_BINARY, imageFileName); // add File
        builder.addTextBody("text", message, ContentType.TEXT_PLAIN);    // add Text
        HttpEntity entity = builder.build();    
 */
