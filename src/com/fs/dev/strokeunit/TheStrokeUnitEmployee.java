/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fs.dev.strokeunit;

import com.fs.bean.util.GlobalBean;
import com.fs.dev.strok.service.TheEmployee;
import java.sql.Connection;
import org.json.JSONObject;
import com.fs.dev.strok.service.Employee;
import org.json.JSONArray;

/**
 *
 * @author Supakit_Pud
 */
public class TheStrokeUnitEmployee {

    final String fs_action = GlobalBean.RETRIEVE_MODE;

    public JSONArray getEmployee(Connection connection, String employee) throws Exception {
        TheStrokeUnit su = new TheStrokeUnit();
        JSONArray result = new JSONArray();
        JSONObject emp = new JSONObject();
        TheEmployee em = new TheEmployee();
        Employee e = null;
        Employee es[] = null;
        e = em.getEmployeeBySAP(employee);
        es = em.getEmployeeByName(employee);
        org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, "Employee", "Tw_Authority");
        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigAllData(jsonArrayConfigfield);
        if (e.isSuccess()) {
            emp.put("fullName", e.getEmpName());
            emp.put("mdNumber", e.getMdNumber());
            emp.put("sapId", e.getSapNumber());
            emp.put("job", e.getAuthorityType());
            su.handleParams(connection, fs_action, jsonArrayTableListConfig, emp);
            result.put(emp);
        } else if (es.length > 0) {
            for (int i = 0; i < es.length; i++) {
                if (es[i].isSuccess()) {
                    emp.put("fullName", es[i].getEmpName());
                    emp.put("mdNumber", es[i].getMdNumber());
                    emp.put("sapId", es[i].getSapNumber());
                    emp.put("job", es[i].getAuthorityType());
                    result.put(emp);
                    su.handleParams(connection, fs_action, jsonArrayTableListConfig, emp);
                    emp = new JSONObject();
                }
            }
        }
        return result;
    }

    public JSONObject getEmployeeBySapId(Connection connection, String employee) throws Exception {
        TheStrokeUnit su = new TheStrokeUnit();
        JSONObject emp = new JSONObject();
        TheEmployee em = new TheEmployee();
        Employee e = null;
        e = em.getEmployeeBySAP(employee);
        org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, "Employee", "Tw_Authority");
        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigAllData(jsonArrayConfigfield);
        if (e.isSuccess()) {
            emp.put("fullName", e.getEmpName());
            emp.put("mdNumber", e.getMdNumber());
            emp.put("sapId", e.getSapNumber());
            emp.put("job", e.getAuthorityType());
            su.handleParams(connection, fs_action, jsonArrayTableListConfig, emp);
        }
        return emp;
    }
}
