/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fs.dev.strokeunit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fs.bean.ExecuteData;
import com.fs.bean.misc.KnSQL;
import com.fs.bean.misc.Trace;
import com.fs.bean.util.GlobalBean;
import com.fs.dev.TheUtility;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Supakit_Pud
 */
public class TheStrokeUnitUtility extends TheUtility {

    public static String SESSION_SYSTEMCONFIG = "session.systemconfig";

    public void createSysConfigs(HttpServletRequest request) throws Exception {
        if (request.getSession() != null && request.getSession().getAttribute(SESSION_SYSTEMCONFIG) == null) {
            Trace.info("################################# createSysConfigsInfo #################################");
            KnSQL knsql = new KnSQL(this);
            knsql.append("SELECT Category , ConfigName , Value , Unit , ConfigDesc ,  Remark ");
            knsql.append(" FROM sys_Config ");
            knsql.append(" WHERE IsActive = '1' ");

            java.util.List<Map<String, String>> configs = new java.util.ArrayList();
            GlobalBean fsGlobal = new GlobalBean();
            fsGlobal.setFsSection("PROMPT");
            try (java.sql.Connection connection = ExecuteData.getNewConnection(fsGlobal.getFsSection(), false)) {
                java.sql.ResultSet rs = knsql.executeQuery(connection);
                java.sql.ResultSetMetaData met = rs.getMetaData();
                while (rs.next()) {
                    int colcount = met.getColumnCount();
                    Map config = new HashMap();
                    for (int i = 1; i <= colcount; i++) {
                        String colname = met.getColumnName(i);
                        config.put(colname, rs.getString(colname));
                    }
                    configs.add(config);
                }
            }

            request.setAttribute(SESSION_SYSTEMCONFIG, configs);
            request.getSession().setAttribute(SESSION_SYSTEMCONFIG, configs);
            Trace.info("-SYSTEM_CONFIG:" + configs);
        }
    }

    public JSONObject getSysConfigsByCategoryAndConfigName(HttpServletRequest request, String category, String configName) throws Exception {
        if (request.getSession() != null && request.getSession().getAttribute(SESSION_SYSTEMCONFIG) != null) {
            java.util.List<Map<String, String>> e = (java.util.List<Map<String, String>>) request.getSession().getAttribute(SESSION_SYSTEMCONFIG);
            Map<String, String> configs = e.stream()
                    .filter(ent -> ent.get(TheStrokeUnitConstant.SYS_CONFIG_CATEGORY) != null
                    && ent.get(TheStrokeUnitConstant.SYS_CONFIG_CATEGORY).equals(category))
                    .filter(ent -> ent.get(TheStrokeUnitConstant.SYS_CONFIG_NAME) != null
                    && ent.get(TheStrokeUnitConstant.SYS_CONFIG_NAME).equals(configName))
                    .collect(Collectors.toList())
                    .get(0);
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String jsonString = objectMapper.writeValueAsString(configs);
                JSONObject jsonConfig = new JSONObject(jsonString);
                return jsonConfig;
            } catch (Exception ex) {
                Trace.info(ex);
                return new JSONObject();
            }
        }
        return new JSONObject();
    }

    public JSONArray getSysConfigsListByCategory(HttpServletRequest request, String category) throws Exception {
        if (request.getSession() != null && request.getSession().getAttribute(SESSION_SYSTEMCONFIG) != null) {
            java.util.List<Map<String, String>> e = (java.util.List<Map<String, String>>) request.getSession().getAttribute(SESSION_SYSTEMCONFIG);
            java.util.List<Map<String, String>> configs = e.stream()
                    .filter(ent -> ent.get(TheStrokeUnitConstant.SYS_CONFIG_CATEGORY) != null
                    && ent.get(TheStrokeUnitConstant.SYS_CONFIG_CATEGORY).equals(category))
                    .collect(Collectors.toList());

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String jsonString = objectMapper.writeValueAsString(configs);
                JSONArray jsonConfigList = new JSONArray(jsonString);
                return jsonConfigList;
            } catch (Exception ex) {
                Trace.info(ex);
                return new JSONArray();
            }
        }
        return new JSONArray();
    }

    public static String changGenderTHToEn(String gender) {
        String result = "";
        switch (gender) {
            case "ชาย":
                result = "M";
                break;
            case "หญิง":
                result = "F";
                break;
            default:
                result = "O";
                break;
        }
        return result;
    }
}
