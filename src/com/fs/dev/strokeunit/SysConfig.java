/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fs.dev.strokeunit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fs.bean.ExecuteData;
import com.fs.bean.misc.KnSQL;
import java.util.Map;
import org.json.JSONObject;
import java.util.stream.Collectors;
import java.util.HashMap;
import com.fs.bean.misc.Trace;
import com.fs.bean.util.GlobalBean;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author Supakit_Pud
 */
public class SysConfig {
    java.util.List<Map<String, String>> sysConfigs;

        public void createSysConfigsInfo() throws Exception {
        Trace.info("!!!! sysConfigs is null ? : "+(sysConfigs == null? true : false));
        if (sysConfigs == null) {
            Trace.info("################################# createSysConfigsInfo #################################");
            KnSQL knsql = new KnSQL(this);
            knsql.append("SELECT Category , ConfigName , Value , Unit , ConfigDesc ,  Remark ");
            knsql.append(" FROM sys_Config ");
            knsql.append(" WHERE IsActive = '1' ");
            sysConfigs = new java.util.ArrayList();
            GlobalBean fsGlobal = new GlobalBean();
            fsGlobal.setFsSection("PROMPT");
            try (java.sql.Connection connection = ExecuteData.getNewConnection(fsGlobal.getFsSection(), false)) {
                java.sql.ResultSet rs = knsql.executeQuery(connection);
                java.sql.ResultSetMetaData met = rs.getMetaData();
                while (rs.next()) {
                    int colcount = met.getColumnCount();
                    Map config = new HashMap();
                    for (int i = 1; i <= colcount; i++) {
                        String colname = met.getColumnName(i);
                        config.put(colname, rs.getString(colname));
                    }
                    sysConfigs.add(config);
                }
            }
            Trace.info("-SYSTEM_CONFIG : \n" + sysConfigs);
        }
    }

    public JSONObject getSysConfigsInfoByCategoryAndConfigName(String category, String configName) throws Exception {
        if (sysConfigs != null) {
            java.util.List<Map<String, String>> e = sysConfigs;
            Map<String, String> configs = e.stream()
                    .filter(ent -> ent.get(TheStrokeUnitConstant.SYS_CONFIG_CATEGORY) != null
                    && ent.get(TheStrokeUnitConstant.SYS_CONFIG_CATEGORY).equalsIgnoreCase(category))
                    .filter(ent -> ent.get(TheStrokeUnitConstant.SYS_CONFIG_NAME) != null
                    && ent.get(TheStrokeUnitConstant.SYS_CONFIG_NAME).equalsIgnoreCase(configName))
                    .collect(Collectors.toList())
                    .get(0);
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String jsonString = objectMapper.writeValueAsString(configs);
                JSONObject jsonConfig = new JSONObject(jsonString);
                return jsonConfig;
            } catch (JsonProcessingException | JSONException ex) {
                Trace.info(ex);
                return new JSONObject();
            }
        }
        return new JSONObject();
    }
    
        public JSONArray getSysConfigsInfoListByCategory(String category) throws Exception {
        if (sysConfigs != null) {
            java.util.List<Map<String, String>> e = sysConfigs;
            java.util.List<Map<String, String>> configs = e.stream()
                    .filter(ent -> ent.get(TheStrokeUnitConstant.SYS_CONFIG_CATEGORY) != null
                    && ent.get(TheStrokeUnitConstant.SYS_CONFIG_CATEGORY).equalsIgnoreCase(category))
                    .collect(Collectors.toList());

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String jsonString = objectMapper.writeValueAsString(configs);
                JSONArray jsonConfigList = new JSONArray(jsonString);
                return jsonConfigList;
            } catch (JsonProcessingException | JSONException ex) {
                Trace.info(ex);
                return new JSONArray();
            }
        }
        return new JSONArray();
    }
}
