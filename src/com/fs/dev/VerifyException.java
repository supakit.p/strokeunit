package com.fs.dev;

@SuppressWarnings("serial")
public class VerifyException extends Exception {
	public VerifyException(String message) {
		super(message);
	}
}
