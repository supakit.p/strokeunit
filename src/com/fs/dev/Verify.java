package com.fs.dev;

/**
 * @author tassun_oro
 *
 */
public class Verify {
	private String verifyField;
	private String descriptionField;
	private String defaultValue;	
	public Verify() {
		super();
	}
	public Verify(String verifyField,String descriptionField,String defaultValue) {
		this.verifyField = verifyField;
		this.descriptionField = descriptionField;
		this.defaultValue = defaultValue;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public String getDescriptionField() {
		return descriptionField;
	}
	public String getVerifyField() {
		return verifyField;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public void setDescriptionField(String descriptionField) {
		this.descriptionField = descriptionField;
	}
	public void setVerifyField(String field) {
		this.verifyField = field;
	}	
}
