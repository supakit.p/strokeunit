package com.fs.dev;
import org.jsoup.*;
import org.jsoup.nodes.Document;
import com.fs.dev.Console;
public class HtmlToText {
	public HtmlToText() {
		super();
	}
	public static void main(String[] args) {		
		try {
			String html = "<p>Hello World</p>";
			if(args.length>0) html = args[0];
			Document doc = Jsoup.parse(html);
			Console.out.println("text = "+doc.text());
			Console.out.println("html = "+doc.html());
		}catch(Exception ex) {
			Console.out.print(ex);
		}
	}
	public static String htmlToText(String html) {
		Document doc = Jsoup.parse(html);
		return doc.text();		
	}
}
