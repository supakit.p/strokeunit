package com.fs.dev;

import com.fs.interfaces.VersionDefinitionInterface;

/**
 * @author tassun_oro
 *
 */
public class Vesion implements VersionDefinitionInterface {
	public Vesion() {
		super();
	}
	public String fetchDefinition() {
		StringBuilder buffer = new StringBuilder();
		buffer.append(Vesion.class);
		buffer.append("FS-20190205-105500 : Start version definition.\n");
		buffer.append("FS-20190207-144500 : Fixed bug on TheUtility over load getDataTable with addonFilters.\n");
		buffer.append("FS-20190211-100500 : SignonBean & SignonData provide more user info.\n");
		buffer.append("FS-20190212-120900 : AbsenceControl persist user in box.\n");
		buffer.append("FS-20190213-134000 : TheUtility fixed getRole with check site.\n");
		buffer.append("FS-20190217-155100 : AbsenceControl fixed calculate confirm/cancel absence.\n");
		buffer.append("FS-20190219-132500 : TheNotify class added in order to send notification.\n");
		buffer.append("FS-20190220-141800 : AbsenceApprove class added.\n");
		buffer.append("FS-20190221-121000 : AbsenceControl fix bug send mail when cancel absence.\n");
		buffer.append("FS-20190225-132300 : User & UserInterface provide getUserNameth & getUserNameen.\n");
		buffer.append("FS-20190226-101500 : TheUtility change get absence type with site only.\n");
		buffer.append("FS-20190303-141900 : TheUtility provide saveFile method.\n");
		buffer.append("FS-20190307-155000 : Modified work flow and support cancel flow.\n");
		buffer.append("FS-20190310-103500 : UserInterface support inactive.\n");
		buffer.append("FS-20190326-141000 : CancelFlow move user message box into history.\n");
		buffer.append("FS-20190330-215000 : Modified SignonBean support gps flag.\n");
		buffer.append("FS-20190401-141000 : Fixed getRoles with site filter and provide createTerminalGroups.\n");
		buffer.append("FS-20190402-115200 : Can prevent send mail by using TEST_MAIL_CONTROL=true.\n");
		buffer.append("FS-20190403-162000 : Fixed approve by manager 2 step.\n");
		buffer.append("FS-20190404-170500 : UserTree support language setting.\n");
		buffer.append("FS-20190405-094500 : TheLocale support dynamic date/time formater.\n");
		buffer.append("FS-20190409-165500 : Support approved by specify approver.\n");
		buffer.append("FS-20190410-155000 : TheUtility support default order by name.\n");
		buffer.append("FS-20190417-151617 : AbsenceControl, ClockTimeControl, OverTimeControl and ShiftTimeControl moveToHistory when found nex process.\n");
		buffer.append("FS-20190418-105000 : AbsenceControl, ClockTimeControl, OverTimeControl and ShiftTimeControl support new status code AV = approving.\n");
		buffer.append("FS-20190429-152500 : Fixed work flow approver is (can be) the reqeuester.\n");
		buffer.append("FS-20190527-194500 : Fixed workflow control.\n");
		buffer.append("\t- WorkflowPersistence ignore filter owner on updateFlow.\n");
		buffer.append("\t- Workflow pass approvers into transientVars.\n");
		buffer.append("\t- AbsenceControl, ClockTimeControl, OverTimeControl, ShiftTimeControl send mail and persist inbox with all approvers.\n" );
		buffer.append("FS-20190811-163000 : TheNotify using FirebaseNotificationSender from new package.\n");
		return buffer.toString();
	}
	public String fetchVersion() {
		StringBuilder buffer = new StringBuilder();
		buffer.append(Vesion.class);
		buffer.append("=$Revision: FS-20190811-163000 $\n");
		return buffer.toString();
	}
}
