package com.fs.dev.strok.service;
import com.fs.bean.util.BeanUtility;
import com.fs.dev.exim.service.ServiceHandler;
import org.apache.commons.httpclient.util.URIUtil;
@SuppressWarnings("deprecation")
public class TheEmployee extends ServiceHandler {
	private BeanUtility butil = new BeanUtility();
	public TheEmployee() {
		setSubUrl("RegEmp");
	}
	public static void main(String[] args) {
		//java com/fs/dev/strok/service/TheEmployee -md 12345
		//java com/fs/dev/strok/service/TheEmployee -id 10005269
		//java com/fs/dev/strok/service/TheEmployee -name test
		try {
			String md = null;
			String sap = null;
			String name = null;
			if(args.length>0) {
				TheEmployee pt = new TheEmployee();
				for(int i=0,isz=args.length;i<isz;i++) {
					String para = args[i];
					if(para.equalsIgnoreCase("-sub")) {
						if(args.length>(i+1)) {
							pt.setSubUrl(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-l")) {
						if(args.length>(i+1)) {
							pt.setLocation(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-bu")) {
						if(args.length>(i+1)) {
							pt.setBasicUser(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-bp")) {
						if(args.length>(i+1)) {
							pt.setBasicPassword(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-sr")) {
						if(args.length>(i+1)) {
							pt.setSystemRegistry(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-md")) {
						if(args.length>(i+1)) {
							md = args[i+1];
						}
					} else if(para.equalsIgnoreCase("-id")) {
						if(args.length>(i+1)) {
							sap = args[i+1];
						}
					} else if(para.equalsIgnoreCase("-name")) {
						if(args.length>(i+1)) {
							name = args[i+1];
						}
					}
				}
				if(md!=null) {
					Employee emp = pt.getEmployeeByMD(md);
					System.out.println(emp);
				}
				if(sap!=null) {
					Employee emp = pt.getEmployeeBySAP(sap);
					System.out.println(emp);
				}
				if(name!=null) {
					Employee[] emps = pt.getEmployeeByName(name);
					if(emps!=null) {
						System.out.println("employee : "+emps.length);
						for(Employee emp : emps) {
							System.out.println(emp);
						}
					}
				}
			} else {
				usage();
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	public static void usage() {
		System.out.println("USAGE : "+TheEmployee.class);
		System.out.println("\t-l  location (host or server)");
		System.out.println("\t-bu basic user");
		System.out.println("\t-bp basic password");
		System.out.println("\t-sr system registry code");
		System.out.println("\t-md MD number");
		System.out.println("\t-sap SAP number");
		System.out.println("\t-name employee name");
		System.out.println("\t-surname employee surname");
	}
	public Employee getEmployeeByMD(String md) throws Exception {
		Employee result = null;
		md = java.net.URLEncoder.encode(md);
		setUrl(getSubUrl()+"/MDNumber/"+md+"/"+getSystemRegistry());
		System.out.println("post : "+getResourceLocator());
		long st1 = System.currentTimeMillis();
		String response = post();
		long st2 = System.currentTimeMillis();
		System.out.println("get by MD Number - response time "+(st2-st1)+" msecs. : "+response);
		if(response!=null && response.trim().length()>0) {
			result = new Employee();
			result.obtain(response);
		}
		return result;
	}
	public Employee getEmployeeBySAP(String sap) throws Exception {
            System.err.println("getEmployeeBySAP #################");
		Employee result = null;
		sap = java.net.URLEncoder.encode(sap);
		setUrl(getSubUrl()+"/SAPID/"+sap+"/"+getSystemRegistry());
		System.out.println("post : "+getResourceLocator());
		long st1 = System.currentTimeMillis();
		String response = post();
		long st2 = System.currentTimeMillis();
		System.out.println("get by SAP Number - response time "+(st2-st1)+" msecs. : "+response);
		if(response!=null && response.trim().length()>0) {
			result = new Employee();
			result.obtain(response);
		}
		return result;
	}
	public Employee[] getEmployeeByName(String name) throws Exception {
		
		Employee[] result = null;
		System.out.println("before Name = "+name);
		name = URIUtil.encodeQuery(name,"UTF-8");
		System.out.println("after Name = "+name);
		setUrl(getSubUrl()+"/EmpName/"+name+"/"+getSystemRegistry());
		System.out.println("post : "+getResourceLocator());
		long st1 = System.currentTimeMillis();
		String response = post();
		System.out.println("response : "+response);
		long st2 = System.currentTimeMillis();
		System.out.println("get by Name - response time "+(st2-st1)+" msecs. : "+response);
		if(response!=null && response.trim().length()>0) {
			Employee emp = new Employee();
			emp.obtain(response);
			if(emp.getEmployees()!=null) {
				result = new Employee[emp.getEmployees().size()];
				emp.getEmployees().toArray(result);
			} else {
				result = new Employee[] { emp };
			}
		}
		return result;
	}
}
