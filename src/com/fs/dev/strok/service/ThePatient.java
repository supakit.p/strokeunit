package com.fs.dev.strok.service;

@SuppressWarnings("deprecation")
public class ThePatient extends ServiceHandler {
	public ThePatient() {
		setSubUrl("Patients");
	}
	public static void main(String[] args) {
		//java com/fs/dev/strok/service/ThePatient -hn 50322265
		//java com/fs/dev/strok/service/ThePatient -id 3102002005064
		//java com/fs/dev/strok/service/ThePatient -hn 48000001 -id 1234567890123
		try {
			String hn = null;
			String pid = null;
			if(args.length>0) {
				ThePatient pt = new ThePatient();
				for(int i=0,isz=args.length;i<isz;i++) {
					String para = args[i];
					if(para.equalsIgnoreCase("-sub")) { 
						if(args.length>(i+1)) {
							pt.setSubUrl(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-hn")) { 
						if(args.length>(i+1)) {
							hn = args[i+1];
						}
					} else if(para.equalsIgnoreCase("-id")) { 
						if(args.length>(i+1)) {
							pid = args[i+1];
						}
					} else if(para.equalsIgnoreCase("-l")) { 
						if(args.length>(i+1)) {
							pt.setLocation(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-bu")) { 
						if(args.length>(i+1)) {
							pt.setBasicUser(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-bp")) { 
						if(args.length>(i+1)) {
							pt.setBasicPassword(args[i+1]);
						}
					} else if(para.equalsIgnoreCase("-sr")) { 
						if(args.length>(i+1)) {
							pt.setSystemRegistry(args[i+1]);
						}
					}
				}
				if(hn!=null) {
					Patient p = pt.getPatientByHN(hn);
					System.out.println(p);
				}
				if(pid!=null) {
					Patient p = pt.getPatientByID(pid);
					System.out.println(p);
				}
			} else {
				usage();
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	public static void usage() {
		System.out.println("USAGE : "+ThePatient.class);
		System.out.println("\t-hn HN code");
		System.out.println("\t-id Card ID");
		System.out.println("\t-l  location (host or server)");
		System.out.println("\t-bu basic user");
		System.out.println("\t-bp basic password");
		System.out.println("\t-sr system registry code");
	}
	public Patient getPatientByHN(String hn) throws Exception {
		Patient result = null;
		hn = java.net.URLEncoder.encode(hn);
		setUrl(getSubUrl()+"/hn/"+hn+"/"+getSystemRegistry());
		System.out.println("post : "+getResourceLocator());
		long st1 = System.currentTimeMillis();
		String response = post();
		long st2 = System.currentTimeMillis();
		System.out.println("get by HN - response time "+(st2-st1)+" msecs. : "+response);
		if(response!=null && response.trim().length()>0) {
			result = new Patient();
			result.obtain(response);
		}
		return result;
	}
	public Patient getPatientByID(String id) throws Exception {
		Patient result = null;
		id = java.net.URLEncoder.encode(id);
		setUrl(getSubUrl()+"/pid/"+id+"/"+getSystemRegistry());
		System.out.println("post : "+getResourceLocator());
		long st1 = System.currentTimeMillis();
		String response = post();
		long st2 = System.currentTimeMillis();
		System.out.println("get by ID - response time "+(st2-st1)+" msecs. : "+response);
		if(response!=null && response.trim().length()>0) {
			result = new Patient();
			result.obtain(response);
		}
		return result;
	}
}
