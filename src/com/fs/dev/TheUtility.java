package com.fs.dev;

import com.fs.bean.EntityBean;
import com.fs.bean.ExecuteBean;
import com.fs.bean.misc.KnSQL;
import com.fs.bean.util.AccessorBean;
import com.fs.bean.util.BeanUtility;
import com.fs.bean.util.GlobalBean;
import com.fs.bean.util.GlobalVariable;
import com.fs.bean.util.LabelConfig;
import com.fs.bean.util.PageUtility;
import com.fs.dev.library.ScreenUtility;
import com.fs.dev.library.SmartFunction;
import com.fs.interfaces.RelatableInterface;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * The Utility class
 *
 * @author Tassun  Oros (TSO) tassun_oro@freewillsolutions.com
 */

@SuppressWarnings({"serial","rawtypes","unchecked"})
public class TheUtility extends SmartFunction {
	private java.util.Random ran = new java.util.Random();
	public TheUtility() {
		super();
		init();
	}
	public TheUtility(GlobalBean global) {
		super(global);
		init();
	}
	public TheUtility(RelatableInterface parent) {
		super(parent);
		init();
	}
	public TheUtility(String section) {
		super(section);
		init();
	}
	public TheUtility(String section,String progid) {
		super(section,progid);
		init();
	}
	public static String getContextPath(HttpServletRequest request) {
		String scheme = request.getScheme();             
		String serverName = request.getServerName(); 
		int serverPort = request.getServerPort(); 
		return scheme+"://"+serverName+(serverPort==80?"":":"+serverPort)+request.getContextPath();		
	}
	public static String getCurrentYear(){
		java.util.Calendar calendar = java.util.Calendar.getInstance(java.util.Locale.US);
		DateFormat dateFormat = new SimpleDateFormat("yyyy",java.util.Locale.US);
		dateFormat.setCalendar(calendar);
		dateFormat.setTimeZone(calendar.getTimeZone());
		return dateFormat.format(calendar.getTime());		
	}
	public static String getResourcesPath(HttpServletRequest request) {
		return getResourcesPath(request,"resources");
	}
	public static String getResourcesPath(HttpServletRequest request,String path) {
		String realpath = request.getServletContext().getRealPath("");
		String resources_path = realpath+java.io.File.separator+path;
		String config_path = (String)com.fs.bean.util.GlobalVariable.getVariable("RESOURCES_PATH");
		if(config_path!=null && config_path.trim().length()>0) {
			resources_path = config_path;
		}
		return resources_path;
	}
	public static boolean isAccessAllBranches(AccessorBean accessor) {
		if(accessor==null) { return false; }
		return "1".equalsIgnoreCase(accessor.getFsVar("fsBranchflag"));
	}
	public static boolean isAccessAllBranches(GlobalBean global) {
		if(global==null) { return false; }
		return "1".equalsIgnoreCase(global.getFsVar("fsBranchflag"));
	}	
	public static boolean isAccessAllSites(AccessorBean accessor) {
		if(accessor==null) { return false; }
		return "1".equalsIgnoreCase(accessor.getFsVar("fsSiteflag"));
	}
	public static boolean isAccessAllSites(GlobalBean global) {
		if(global==null) { return false; }
		return "1".equalsIgnoreCase(global.getFsVar("fsSiteflag"));
	}
	public static boolean isAdministrator(AccessorBean fsAccessor) {
		return isInGroup(fsAccessor,"ADMIN");		
	}
	public static boolean isAdministrator(GlobalBean fsGlobal) {
		return isInGroup(fsGlobal,"ADMIN");
	}
	public static boolean isChanged(AccessorBean fsAccessor) {
		if(fsAccessor==null) { return false; }
		return "1".equals(fsAccessor.getFsVar("fsChange"));		
	}	
	public static boolean isEnglish(GlobalBean global) {
		if(global==null) { return false; }
		return isEnglish(global.getFsLanguage());
	}
	public static boolean isEnglish(HttpServletRequest request) {
		return isEnglish(PageUtility.getDefaultLanguage(request));
	}
	public static boolean isEnglish(String language) {
		return "EN".equalsIgnoreCase(language);
	}
	public static boolean isExpired(AccessorBean fsAccessor) {
		return isExpired(fsAccessor,null);		
	}	
	public static boolean isExpired(AccessorBean fsAccessor,java.sql.Date expiredate) {
		if(fsAccessor==null) { return false; }
		String expiredatestr = fsAccessor.getFsVar("fsPasswordexpire");
		if(expiredatestr!=null && expiredatestr.trim().length()>0) {
			BeanUtility butil = new BeanUtility();
			java.util.Date expdate = butil.parseDate(expiredatestr);
			java.util.Date curdate = new java.util.Date();
			return butil.compareDate(curdate, expdate) > 0;
		}
		return false;		
	}
	public static boolean isInGroup(AccessorBean fsAccessor,String group) {
		if(fsAccessor==null) { return false; }
		return isInGroup(fsAccessor.getFsGroups(),group);		
	}
	public static boolean isInGroup(GlobalBean fsGlobal,String group) {
		if(fsGlobal==null) { return false; }
		return isInGroup(fsGlobal.getFsVar("fsUserGroup"),group);
	}	
	public static boolean isInGroup(String groups,String group) {
		if(group!=null && groups!=null) {
			String[] grps = groups.split(",");
			for(String grp : grps) {
				if(grp!=null && grp.equalsIgnoreCase(group)) {
					return true;
				}
			}
		}
		return false;
	}
	public static boolean isSignInAuthenticated(AccessorBean fsAccessor) {
		Object signing = GlobalVariable.getVariable("SIGNIN_AUTHENTICATE");
		return signing!=null && "true".equalsIgnoreCase(signing.toString());		
	}
	public static boolean isThai(GlobalBean global) {
		if(global==null) { return false; }
		return isThai(global.getFsLanguage());
	}
	public static boolean isThai(HttpServletRequest request) {
		return isThai(PageUtility.getDefaultLanguage(request));
	}
	public static boolean isThai(String language) {
		return "TH".equalsIgnoreCase(language);
	}
	public static void main(String[] args) {
		//java com/fs/dev/TheUtility -ms PROMPT -act Program
		//java com/fs/dev/TheUtility -ms PROMPT -act Group
		//java com/fs/dev/TheUtility -ms PROMPT -act Role
		//java com/fs/dev/TheUtility -ms PROMPT -act Zone
		//java com/fs/dev/TheUtility -ms PROMPT -act AbsenceTypes -site FWG
		try {
			TheUtility util = new TheUtility();
			if(args.length>0) {	
				String except = Arguments.getString(args, null, "-except");
				String directory = Arguments.getString(args, "", "-dir");
				String action = Arguments.getString(args, null, "-act");	
				AccessorBean accessor = new AccessorBean();
				GlobalBean global = new GlobalBean();
				global.setFsAction(GlobalBean.COLLECT_MODE);
				accessor.setFsUser(Arguments.getString(args, accessor.getFsUser(), "-u"));
				global.setFsProg(Arguments.getString(args, global.getFsProg(), "-pid"));
				global.setFsSection(Arguments.getString(args, global.getFsSection(), "-ms"));
				accessor.setFsSite(Arguments.getString(args, accessor.getFsSite(), "-site"));
				accessor.setFsSites(Arguments.getString(args, accessor.getFsSites(), "-sites"));
				if(action!=null && action.trim().length()>0) {
					if(action.equals("random")) {
						String filename = util.getRandomFile(directory,except==null?null:new String[]{except});
						Console.out.println("random file = "+filename);
					}
					EntityBean bean = null;
					java.util.Map map = null;
					String getMethod = "get"+action;
					String createMethod = "create"+action;					
					java.lang.reflect.Method gmet = util.getClass().getMethod(getMethod,AccessorBean.class,GlobalBean.class);
					java.lang.reflect.Method cmet = util.getClass().getMethod(createMethod,EntityBean.class);
					Object[] params = new Object[] { accessor, global };
					bean = (EntityBean)gmet.invoke(util, params);
					map = (java.util.Map)cmet.invoke(util, new Object[] { bean } );					
					Console.out.println("map : "+map.getClass()+", "+map);
					if(bean!=null) {
						java.util.Enumeration ens = bean.elements();
						if(ens!=null) {
							while(ens.hasMoreElements()) {
								Console.out.println(ens.nextElement());
							}
						}
						java.util.List list = util.createListEntities(bean,null,null);
						Console.out.println("create list entities : "+list);
						list = util.createEntitiesAsList(bean,null,null);
						Console.out.println("create as list : "+list);
					}
				}	
			} else {
				Console.out.println("USAGE : "+TheUtility.class);
				Console.out.println("\t-act action");
				Console.out.println("\t-ms maing db section");
				Console.out.println("\t-u user");
				Console.out.println("\t-pid program id");
				Console.out.println("\t-dir directory");
				Console.out.println("\t-site site");
				Console.out.println("\t-sites sites(ex. 01,02)");
			}
		} catch(Exception ex) {
			Console.out.print(ex);
		}
		System.exit(0);
	}
	public static String merge(java.util.List<String> sources) {
		if(sources!=null) {
			String[] allArray = new String[sources.size()];
			sources.toArray(allArray);
			return BeanUtility.merge(allArray);			
		}
		return null;
	}
	public static void saveFile(java.io.InputStream inputStream,java.io.File outfile) throws Exception {
		if(inputStream==null) { return; }
		int read = 0;
		byte[] bytes = new byte[1024];
		try(java.io.OutputStream outStream = new java.io.FileOutputStream(outfile)) {
			while ((read = inputStream.read(bytes)) != -1) {
				outStream.write(bytes, 0, read);
			}
			outStream.flush();
			Console.out.println("save file : "+outfile.getAbsolutePath());  
		}
	}
	public static void saveFile(java.io.InputStream inputStream,String outfile) throws Exception {
		if(outfile==null || outfile.trim().length()<0) { return; }
		saveFile(inputStream,new java.io.File(outfile));
	}
	private void init() {
		setDefaultMapInstanceClassName("java.util.LinkedHashMap");
	}
	public void composeDescriptionFields(com.fs.bean.CustomBean custom,HttpServletRequest request,String category,Verify... verifiers) {
		if(verifiers!=null && verifiers.length>0) {
			java.util.Map langmap = (java.util.Map)request.getSession().getAttribute(category+"_EN");
			if(!isEnglish(request)) {
				langmap = (java.util.Map)request.getSession().getAttribute(category+"_TH");
			}
			if(langmap==null) {
				langmap = (java.util.Map)request.getSession().getAttribute(category);
			}
			java.util.Enumeration elements = custom.elements();
			if(elements!=null) {
				while(elements.hasMoreElements()) {
					com.fs.bean.CustomBean bean = (com.fs.bean.CustomBean)elements.nextElement();
					if(bean!=null) {
						for(Verify v : verifiers) {
							if(v!=null) {
								String verifyValue = bean.getString(v.getVerifyField());
								String desc = (String)langmap.get(verifyValue==null||verifyValue.trim().length()<=0?v.getDefaultValue():verifyValue);
								bean.setString(v.getDescriptionField(), desc);
							}
						}
					}
				}
			}
		}
	}
	public java.util.Map createActive(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"activeid","nameen");
	}
	public void createActives(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createActives(accessor,pid,request,category,null);
	}
	public void createActives(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createActives(null,accessor,pid,request,category,content);
	}
	public void createActives(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getActive(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"activeid",(String)null,request,category,content);
			}
		}
	}
	public void createAllSites(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createAllSites(accessor,pid,request,category,null);
	}
	public void createAllSites(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createAllSites(null,accessor,pid,request,category,content);
	}
	public void createAllSites(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getAllSite(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"site",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createAmphur(EntityBean entity) throws Exception {
		return createMapEntries(entity,null,"provincecode","amphurcode","nameen");
	}
	public void createAmphurs(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createAmphurs(accessor,pid,request,category,null);
	}
	public void createAmphurs(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createAmphurs(null,accessor,pid,request,category,content);
	}
	public void createAmphurs(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getAmphur(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"amphurcode","provincecode",request,category,content);
			}
		}
	}		
	public void createCategories(EntityBean entity,String key,String group,HttpServletRequest request,String category,Content content) throws Exception {			
		createCategories(entity,key,group,request,category,content,"nameen","nameth");
	}
	public void createCategories(EntityBean entity,String key,String group,HttpServletRequest request,String category,Content content,String nameen,String nameth) throws Exception {
		createCategories(entity,key,group,request,category,content,new String[]{nameen},new String[]{nameth});
	}
	public void createCategories(EntityBean entity,String key,String group,HttpServletRequest request,String category,Content content,String[] nameens,String[] nameths) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(entity!=null) {
			java.util.Map enmap = createCategory(entity,key,nameens,group);
			java.util.Map thmap = createCategory(entity,key,nameths,group);
			if(enmap!=null) {
				java.util.LinkedHashMap linkmap = null;
				if(enmap instanceof java.util.LinkedHashMap) {
					linkmap = (java.util.LinkedHashMap)enmap;
				} else {
					linkmap = new java.util.LinkedHashMap();
					linkmap.putAll(enmap);
				}
				request.getSession().setAttribute(encat, linkmap);
			}
			if(thmap!=null) {
				java.util.LinkedHashMap linkmap = null;
				if(thmap instanceof java.util.LinkedHashMap) {
					linkmap = (java.util.LinkedHashMap)thmap;
				} else {
					linkmap = new java.util.LinkedHashMap();
					linkmap.putAll(thmap);
				}
				request.getSession().setAttribute(thcat, linkmap);
			}
			if(content!=null) {
				if(content.getEnKey()!=null && content.getEnValue()!=null) {
					java.util.LinkedHashMap enhat = new java.util.LinkedHashMap();
					enhat.put(content.getEnKey(), content.getEnValue());
					if(enmap!=null) enhat.putAll(enmap);
					request.getSession().setAttribute(encat, enhat);
				}
				if(content.getThKey()!=null && content.getThValue()!=null) {
					java.util.LinkedHashMap thhat = new java.util.LinkedHashMap();
					thhat.put(content.getThKey(), content.getThValue());
					if(thmap!=null) thhat.putAll(thmap);					
					request.getSession().setAttribute(thcat, thhat);
				}
			}
		}		
	}	
	public void createCategories(EntityBean entity,String key,String[] group,HttpServletRequest request,String category,Content content) throws Exception {
		createCategories(entity,key,group,request,category,content,"nameen","nameth");
	}
	public void createCategories(EntityBean entity,String key,String[] group,HttpServletRequest request,String category,Content content,String nameen,String nameth) throws Exception {
		createCategories(entity,key,group,request,category,content,new String[]{nameen},new String[]{nameth});
	}		
	public void createCategories(EntityBean entity,String key,String[] group,HttpServletRequest request,String category,Content content,String[] nameens,String[] nameths) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(entity!=null) {
			java.util.Map enmap = createCategory(entity,key,nameens,group);
			java.util.Map thmap = createCategory(entity,key,nameths,group);
			if(enmap!=null) {
				java.util.LinkedHashMap linkmap = null;
				if(enmap instanceof java.util.LinkedHashMap) {
					linkmap = (java.util.LinkedHashMap)enmap;
				} else {
					linkmap = new java.util.LinkedHashMap();
					linkmap.putAll(enmap);
				}
				request.getSession().setAttribute(encat, linkmap);
			}
			if(thmap!=null) {
				java.util.LinkedHashMap linkmap = null;
				if(thmap instanceof java.util.LinkedHashMap) {
					linkmap = (java.util.LinkedHashMap)thmap;
				} else {
					linkmap = new java.util.LinkedHashMap();
					linkmap.putAll(thmap);
				}
				request.getSession().setAttribute(thcat, linkmap);
			}
			if(content!=null) {
				if(content.getEnKey()!=null && content.getEnValue()!=null) {
					java.util.LinkedHashMap enhat = new java.util.LinkedHashMap();
					enhat.put(content.getEnKey(), content.getEnValue());
					if(enmap!=null) enhat.putAll(enmap);
					request.getSession().setAttribute(encat, enhat);
				}
				if(content.getThKey()!=null && content.getThValue()!=null) {
					java.util.LinkedHashMap thhat = new java.util.LinkedHashMap();
					thhat.put(content.getThKey(), content.getThValue());
					if(thmap!=null) thhat.putAll(thmap);					
					request.getSession().setAttribute(thcat, thhat);
				}
			}
		}		
	}	
	public java.util.Map createCategory(EntityBean entity,String key,String naming,String group) throws Exception {	
		return createMapEntities(entity,null,group,key,naming);
	}
	public java.util.Map createCategory(EntityBean entity,String key,String naming,String[] group) throws Exception {		
		return createMapEntries(entity,null,group,key,naming);
	}
	public java.util.Map createCategory(EntityBean entity,String key,String[] namings,String group) throws Exception {		
		return createMapEntities(entity,null,group,key,namings);
	}	
	public java.util.Map createCategory(EntityBean entity,String key,String[] namings,String[] group) throws Exception {		
		return createMapEntries(entity,null,group,key,namings);
	}
	public void createCompanies(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createCompanies(accessor,pid,request,category,null);
	}
	public void createCompanies(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createCompanies(null,accessor,pid,request,category,content);
	}
	public void createCompanies(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getCompany(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"site",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createCompany(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"site","nameen");
	}
	public java.util.Map createCompanyBranch(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"branch","nameen");
	}
	public void createCompanyBranches(AccessorBean accessor,String pid,HttpServletRequest request,String category,String site) throws Exception {
		createCompanyBranches(accessor,pid,request,category,site,null);
	}
	public void createCompanyBranches(AccessorBean accessor,String pid,HttpServletRequest request,String category,String site,Content content) throws Exception {
		createCompanyBranches(null,accessor,pid,request,category,site,content);
	}
	public void createCompanyBranches(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,String site,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getCompanyBranch(accessor,createGlobal(accessor,pid),site);
			if(entity!=null) {
				createCategories(entity,"branch",(String)null,request,category,content);
			}
		}
	}
	public Content createContent(String pid,HttpServletRequest request) {
		return createContent(pid,request,"all");
	}
	public Content createContent(String pid,HttpServletRequest request,String naming) {
		Content result = new Content();
		LabelConfig fsLabel = null;
		if(request!=null) {
			fsLabel = (LabelConfig)ScreenUtility.getObjectName(request,"fsLabel");
			if(fsLabel==null) {
				fsLabel = new LabelConfig();
			}
		}
		if(pid!=null) {
			if(fsLabel!=null) {
				fsLabel.load(pid,PageUtility.getDefaultLanguage(request));
			}
		}
		result.setEnKey("");
		result.setThKey("");
		result.setEnValue("");
		result.setThValue("");
		if(fsLabel!=null && (naming!=null && naming.trim().length()>0)) {
			result.setEnValue(fsLabel.getText(naming, "", "EN"));
			result.setThValue(fsLabel.getText(naming, "", "TH"));
		}
		return result;
	}
	public void createCountries(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createCountries(accessor,pid,request,category,null);
	}
	public void createCountries(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createCountries(null,accessor,pid,request,category,content);
	}
	public void createCountries(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getCountry(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"countrycode",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createCountry(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"countrycode","nameen");
	}
	public void createDays(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		java.util.Map daysmap = (java.util.Map)request.getSession().getAttribute(category);
		if(daysmap==null) {
			java.util.TreeMap tmap = new java.util.TreeMap();
			for(int i=1;i<=31;i++) {
				String value = Integer.toString(i);
				tmap.put(i, value);
			}
			request.getSession().setAttribute(category, tmap);
		}
	}				
	public java.util.Map createDistrict(EntityBean entity) throws Exception {
		return createMapEntries(entity,null,new String[]{"provincecode","amphurcode"},"districtcode","nameen");
	}		
	public void createDistricts(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createDistricts(accessor,pid,request,category,null);
	}
	public void createDistricts(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createDistricts(null,accessor,pid,request,category,content);
	}
	public void createDistricts(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getDistrict(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"districtcode",new String[]{"provincecode","amphurcode"},request,category,content);
			}
		}
	}
	public void createFromCategories(String fromCategory,HttpServletRequest request,String category) {
		createFromCategories(fromCategory,request,category,null);
	}		
	public void createFromCategories(String fromCategory,HttpServletRequest request,String category,Content content) {
		String encat = category+"_EN";
		String thcat = category+"_TH";
		java.util.Map enmap = (java.util.Map)request.getSession().getAttribute(fromCategory+"_EN");
		java.util.Map thmap = (java.util.Map)request.getSession().getAttribute(fromCategory+"_TH");
		if(enmap!=null) {			
			java.util.LinkedHashMap enhat = new java.util.LinkedHashMap();
			enhat.putAll(enmap);
			enhat.remove("");
			request.getSession().setAttribute(encat, enhat);
		}
		if(thmap!=null) {
			java.util.LinkedHashMap thhat = new java.util.LinkedHashMap();
			thhat.putAll(thmap);
			thhat.remove("");
			request.getSession().setAttribute(thcat, thhat);
		}
		if(content!=null) {
			if(content.getEnKey()!=null && content.getEnValue()!=null) {
				java.util.LinkedHashMap enhat = new java.util.LinkedHashMap();
				enhat.put(content.getEnKey(), content.getEnValue());
				if(enmap!=null) enhat.putAll(enmap);
				request.getSession().setAttribute(encat, enhat);
			}
			if(content.getThKey()!=null && content.getThValue()!=null) {
				java.util.LinkedHashMap thhat = new java.util.LinkedHashMap();
				thhat.put(content.getThKey(), content.getThValue());
				if(thmap!=null) thhat.putAll(thmap);					
				request.getSession().setAttribute(thcat, thhat);
			}
		}		
	}
	public GlobalBean createGlobal(AccessorBean accessor,String pid) {
		return createGlobal(accessor,getSection(),pid);
	}
	public GlobalBean createGlobal(AccessorBean accessor,String section,String pid) {
		GlobalBean global = new GlobalBean();
		global.setFsSection(section);
		global.setFsProg(pid);
		global.setFsAction(GlobalBean.COLLECT_MODE);		
		if(accessor!=null) global.obtain(accessor);
		return global;
	}
	public java.util.Map createGroup(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"groupname","nameen");
	}
	public java.util.Map createGroupPublic(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"groupname","nameen");
	}	
	public void createGroupPublics(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createGroupPublics(accessor,pid,request,category,null);
	}
	public void createGroupPublics(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createGroupPublics(null,accessor,pid,request,category,content);
	}
	public void createGroupPublics(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getGroupPublic(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"groupname",(String)null,request,category,content);
			}
		}
	}
	public void createGroups(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createGroups(accessor,pid,request,category,null);
	}
	public void createGroups(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createGroups(null,accessor,pid,request,category,content);
	}
	public void createGroups(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getGroup(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"groupname",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createMonth(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"month","nameen");
	}
	public void createMonths(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createMonths(accessor,pid,request,category,null);
	}	
	public void createMonths(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createMonths(null,accessor,pid,request,category,content);
	}
	public void createMonths(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getMonth(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"month",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createProduct(AccessorBean accessor,GlobalBean global) throws Exception {
		return createProduct(getProduct(accessor,global));
	}
	public java.util.Map createProduct(AccessorBean accessor,String section,String pid) throws Exception {
		return createProduct(getProduct(accessor,section,pid));
	}
	public java.util.Map createProduct(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"product","nameen");
	}
	public void createProducts(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createProducts(accessor,pid,request,category,null);
	}
	public void createProducts(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createProducts(null,accessor,pid,request,category,content);
	}
	public void createProducts(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getProduct(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"product",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createProgram(AccessorBean accessor,String section,String pid) throws Exception {
		return createProgram(getProgram(accessor,createGlobal(accessor,section,pid)));
	}
	public java.util.Map createProgram(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"programid","progname");
	}
	public java.util.Map createProgramBySystem(EntityBean entity,String system) throws Exception {
		return createMapEntities(entity,system,"system","programid","progname");
	}
	public java.util.Map createProgramByUser(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"programid","progname");
	}
	public java.util.Map createProgramByUser(String userid,AccessorBean accessor,GlobalBean global) throws Exception {
		return createProgramByUser(getProgramByUser(userid,accessor,global));
	}
	public java.util.Map createProgramConfig(AccessorBean accessor,GlobalBean global) throws Exception {
		return createProgramConfig(getProgramConfig(accessor,global));
	}
	public java.util.Map createProgramConfig(AccessorBean accessor,String section,String pid) throws Exception {
		return createProgramConfig(getProgramConfig(accessor,createGlobal(accessor,section,pid)));
	}
	public java.util.Map createProgramConfig(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"xmlfile","progname");
	}
	public java.util.Map createProgramDescription(AccessorBean accessor,GlobalBean global) throws Exception {
		return createProgramDescription(getProgram(accessor,global));
	}
	public java.util.Map createProgramDescription(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"programid","description");
	}
	public java.util.Map createProgramIcon(AccessorBean accessor,GlobalBean global) throws Exception {
		return createProgramIcon(getProgram(accessor,global));
	}
	public java.util.Map createProgramIcon(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"programid","iconfile");
	}
	public void createPrograms(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createPrograms(accessor,pid,request,category,null);
	}
	public void createPrograms(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createPrograms(null,accessor,pid,request,category,content);
	}
	public void createPrograms(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getProgram(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"programid",(String)null,request,category,content,"progname","prognameth");
			}
		}
	}
	public java.util.Map createProgramShortNameByUser(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"programid","shortname");
	}
	public java.util.Map createProgramShortNameByUser(String userid,AccessorBean accessor,GlobalBean global) throws Exception {
		return createProgramShortNameByUser(getProgramByUser(userid,accessor,global));
	}
	public java.util.Map createProgramShortNameByUser(String userid,AccessorBean accessor,String section,String pid) throws Exception {
		return createProgramShortNameByUser(getProgramByUser(userid,accessor,createGlobal(accessor,section,pid)));
	}
	public void createProgramShortNamesByUser(EntityBean entity,String userid,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getProgramByUser(userid,accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"programid",(String)null,request,category,content,"shortname","shortnameth");
			}
		}
	}
	public void createProgramShortNamesByUser(String userid,AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createProgramShortNamesByUser(userid,accessor,pid,request,category,null);
	}
	public void createProgramShortNamesByUser(String userid,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createProgramShortNamesByUser(null,userid,accessor,pid,request,category,content);
	}
	public java.util.Map createProvince(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"provincecode","nameen");
	}
	public void createProvinces(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createProvinces(accessor,pid,request,category,null);
	}
	public void createProvinces(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createProvinces(null,accessor,pid,request,category,content);
	}
	public void createProvinces(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getProvince(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"provincecode",(String)null,request,category,content);
			}
		}
	}
	public void createSites(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createSites(accessor,pid,request,category,null);
	}
	public void createSites(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createSites(null,accessor,pid,request,category,content);
	}
	public void createSites(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getCompany(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"site",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createStyle(AccessorBean accessor,GlobalBean global) throws Exception {
		return createStyle(getStyle(accessor,global));
	}
	public java.util.Map createStyle(AccessorBean accessor,String section,String pid) throws Exception {
		return createStyle(getStyle(accessor,createGlobal(accessor,section,pid)));
	}
	public java.util.Map createStyle(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"styleid","styletext");
	}
	public java.util.Map createUserStatus(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"userstatus","nameen");
	}
	public void createUserStatuses(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createUserStatuses(accessor,pid,request,category,null);
	}
	public void createUserStatuses(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createUserStatuses(null,accessor,pid,request,category,content);
	}
	public void createUserStatuses(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getUserStatus(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"userstatus",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createUserType(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"usertype","nameen");
	}
	public void createUserTypes(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createUserTypes(accessor,pid,request,category,null);
	}
	public void createUserTypes(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createUserTypes(null,accessor,pid,request,category,content);
	}
	public void createUserTypes(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getUserType(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"usertype",(String)null,request,category,content);
			}
		}
	}
	public java.util.Map createWeekday(EntityBean entity) throws Exception {
		return createMapEntities(entity,null,null,"dayofweek","nameen");
	}
	public void createWeekdays(AccessorBean accessor,String pid,HttpServletRequest request,String category) throws Exception {
		createWeekdays(accessor,pid,request,category,null);
	}
	public void createWeekdays(AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {
		createWeekdays(null,accessor,pid,request,category,content);
	}
	public void createWeekdays(EntityBean entity,AccessorBean accessor,String pid,HttpServletRequest request,String category,Content content) throws Exception {			
		String encat = category+"_EN";
		String thcat = category+"_TH";
		if(request.getSession().getAttribute(encat)==null || request.getSession().getAttribute(thcat)==null) {
			if(entity==null) entity = getWeekday(accessor,createGlobal(accessor,pid));
			if(entity!=null) {
				createCategories(entity,"dayofweek",(String)null,request,category,content);
			}
		}
	}
	public String fetchVersion() {
		return super.fetchVersion()+TheUtility.class+"=$Revision: FS-20180917-161616 $\n";
	}
	public EntityBean getActive(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tactive","activeid",null,null,false,false);
	}
	public EntityBean getAllSite(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tcomp","site",null,null,true,false);
	}
	public EntityBean getAmphur(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tamphur","amphurcode","zoneid","zoneid,provincecode,amphurcode",false,false);
	}	
	public java.util.Map getCategory(HttpServletRequest request,String category) {
		String language = PageUtility.getDefaultLanguage(request);
		if(language==null || language.trim().length()<=0) language = "EN";
		java.util.Map result = (java.util.Map)getContextAttribute(request,category+"_"+language.toUpperCase());
		if(result==null) result = (java.util.Map)getContextAttribute(request,category);		
		return result;
	}
	public String getCategoryValue(HttpServletRequest request,String category,String key) {
		Object result = null;
		java.util.Map map = getCategory(request,category);
		if(map!=null) {
			result = map.get(key);
		}
		return result==null?null:result.toString();
	}
	public EntityBean getCompany(AccessorBean accessor,GlobalBean global) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select site,shortname,nameen,nameth ");
		knsql.append("from tcomp ");
		if(isAllSites(accessor)) {
			knsql.append("where (site = '"+accessor.getFsSite()+"' or headsite = '"+accessor.getFsSite()+"') ");						
		} else {
			if(accessor.getFsSites()!=null && accessor.getFsSites().length()>0) {
				String sites = BeanUtility.getFilterQoute(accessor.getFsSites());
				knsql.append("where ( site = '"+accessor.getFsSite()+"' ");
				knsql.append("or site in ("+sites+") ) ");
			} else {
				knsql.append("where site = '"+accessor.getFsSite()+"' ");			
			}
		}
		knsql.append("and (inactive is null or inactive != '1') ");
		knsql.append("order by nameen, nameth ");
		return getEntities(entity,accessor,global);
	}
	public EntityBean getCompanyBranch(AccessorBean accessor,GlobalBean global,String site) throws Exception {
		return getDataTable(accessor,global,"tcompbranch","branch",null,"nameen,branch",true,false," and site = '"+site+"' ");
	}
	public EntityBean getConfig(AccessorBean accessor,GlobalBean global) throws Exception {
		EntityBean entity = new EntityBean();
		entity.setTable("tconfig");
		entity.addSchema("category",java.sql.Types.VARCHAR);
		entity.addSchema("colname",java.sql.Types.VARCHAR);
		entity.addSchema("colvalue",java.sql.Types.VARCHAR);
		return getEntities(entity,accessor,global);
	}
	public EntityBean getConfig(AccessorBean accessor,String section,String pid) throws Exception {
		GlobalBean global = new GlobalBean();
		global.setFsSection(section);
		global.setFsProg(pid);
		global.setFsAction(GlobalBean.COLLECT_MODE);		
		return getConfig(accessor,global);
	}
	public java.util.Map getConfigByCategory(EntityBean entity,String gid) throws Exception {
		return createMapEntities(entity,gid,"category","colname","colvalue");
	}
	public Object getContextAttribute(HttpServletRequest request,String category) {
		Object result = request.getAttribute(category);
		if(result==null) {
			result = request.getSession().getAttribute(category);		
		}
		return result;
	}	
	public EntityBean getCountry(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tcountry","countrycode","countryalias","countrycode",false,false);
	}	
	public EntityBean getDataTable(AccessorBean accessor,GlobalBean global,String tableName,String keyField,String addonFields,String orderFields,boolean checkActive,boolean checkSite) throws Exception {
		return getDataTable(accessor,global,tableName,keyField,addonFields,orderFields,checkActive,checkSite,null);
	}
	public EntityBean getDataTable(AccessorBean accessor,GlobalBean global,String tableName,String keyField,String addonFields,String orderFields,boolean checkActive,boolean checkSite,String addonFilters) throws Exception {
		return getDataTable(accessor,global,tableName,keyField,addonFields,orderFields,checkActive,checkSite,addonFilters,false);		
	}	
	public EntityBean getDataTable(AccessorBean accessor,GlobalBean global,String tableName,String keyField,String addonFields,String orderFields,boolean checkActive,boolean checkSite,String addonFilters,boolean siteOnly) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select ");
		knsql.append(keyField);
		knsql.append(",nameen,nameth");
		if(addonFields!=null && addonFields.trim().length()>0) {
			knsql.append(",");
			knsql.append(addonFields);
		}
		knsql.append(" from ");
		knsql.append(tableName);
		String filter = " where";
		if(checkSite) {
			if(siteOnly) {
				knsql.append(filter+" site = '"+accessor.getFsSite()+"' ");							
			} else {
				if(accessor.getFsSites()!=null && accessor.getFsSites().length()>0) {
					String sites = BeanUtility.getFilterQoute(accessor.getFsSites());
					knsql.append(filter+" ( site = '"+accessor.getFsSite()+"' ");
					knsql.append("or site in ("+sites+") ) ");
				} else {
					knsql.append(filter+" site = '"+accessor.getFsSite()+"' ");			
				}
			}
			filter = " and";
		}
		if(checkActive) {
			knsql.append(filter+" (inactive is null or inactive != '1') ");
			filter = " and";
		}
		if(addonFilters!=null && addonFilters.trim().length()>0) {
			knsql.append(addonFilters);
		}
		if(orderFields!=null && orderFields.trim().length()>0) {
			knsql.append(" order by ");
			knsql.append(orderFields);
		} else {
			knsql.append(" order by nameen, nameth ");
		}
		return getEntities(entity,accessor,global);
	}	
	public EntityBean getDistrict(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tdistrict","districtcode","provincecode,amphurcode,zoneid","zoneid,provincecode,amphurcode,districtcode",false,false);
	}
	public EntityBean getGroup(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tgroup","groupname",null,"groupname",false,false);
	}	
	public EntityBean getGroupPublic(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tgroup","groupname",null,"groupname",false,false," where (privateflag is null or privateflag != '1')");
	}	
	public EntityBean getMonth(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tmonth","month",null,null,false,false);
	}
	public EntityBean getProduct(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tprod","product",null,"product",false,false);
	}
	public EntityBean getProduct(AccessorBean accessor,String section,String pid) throws Exception {
		GlobalBean global = new GlobalBean();
		global.setFsSection(section);
		global.setFsProg(pid);
		global.setFsAction(GlobalBean.COLLECT_MODE);		
		return getProduct(accessor,global);
	}
	public EntityBean getProgram(AccessorBean accessor,GlobalBean global) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select programid,progname,prognameth ");
		knsql.append("from tprog ");
		knsql.append("order by programid ");
		return getEntities(entity,accessor,global);
	}
	public EntityBean getProgramByUser(String userid,AccessorBean accessor,GlobalBean global) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select tprog.* ");
		knsql.append("from tprog,tproggrp,tusergrp ");
		knsql.append("where tusergrp.userid = ?userid ");
		knsql.append("and tusergrp.groupname = tproggrp.groupname ");
		knsql.append("and tproggrp.programid = tprog.programid ");
		knsql.append("order by programid ");
		knsql.setParameter("userid", userid);
		return getEntities(entity,accessor,global);
	}		
	public EntityBean getProgramConfig(AccessorBean accessor,GlobalBean global) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select progid,progname,tablename,xmlfile ");
		knsql.append("from tprogconfig ");
		knsql.append("order by progid ");
		return getEntities(entity,accessor,global);
	}
	public EntityBean getProvince(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tprovince","provincecode","zoneid","provincecode",false,false);
	}
	public String getRandomFile(String directory) {
		return getRandomFile(directory,null);
	}
	public String getRandomFile(String directory,String[] excepts) {
		if(directory==null) return null;
		java.io.File dir = new java.io.File(directory);
		if(dir.exists()) {
			java.io.File[] files = dir.listFiles();
			if(files.length>0) {
				boolean found = false;
				while(!found) {
					int minquest = 1;
					int maxquest = files.length;					
					int n = ran.nextInt(maxquest-minquest+1)+minquest;
					if(maxquest<=1) n = 1;
					java.io.File f = files[n-1];
					if(f!=null) {
						if(excepts!=null) {
							for(String s : excepts) {
								if(f.getName().indexOf(s)>=0) {
									found = true;
								}
							}
						}
						if(found) continue;
						return f.getName();
					}
				}
			}
		}
		return null;
	}
	public EntityBean getStyle(AccessorBean accessor,GlobalBean global) throws Exception {
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select styleid,styletext ");
		knsql.append("from tstyle ");
		knsql.append("order by styleid ");
		return getEntities(entity,accessor,global);
	}	
	public EntityBean getUser(AccessorBean accessor,GlobalBean global) throws Exception {
		if(accessor==null) { return null; }
		ExecuteBean entity = new ExecuteBean();
		KnSQL knsql = entity.getKnSQL();
		knsql.clear();
		knsql.append("select * ");
		knsql.append("from tuser ");
		knsql.append("where userid = ?userid ");
		knsql.setParameter("userid", accessor.getFsUser());
		return getEntities(entity,accessor,global);
	}
	public EntityBean getUser(AccessorBean accessor,String section,String pid) throws Exception {
		GlobalBean global = new GlobalBean();
		global.setFsSection(section);
		global.setFsProg(pid);
		global.setFsAction(GlobalBean.RETRIEVE_MODE);		
		return getUser(accessor,global);
	}
	public EntityBean getUser(String userid,String site,String section,String pid) throws Exception {
		AccessorBean accessor = new AccessorBean();
		accessor.setFsSite(site);
		accessor.setFsVar("fsUser",userid);
		return getUser(accessor,section,pid);
	}
	public EntityBean getUserStatus(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tuserstatus","userstatus",null,null,false,false);
	}		
	public EntityBean getUserType(AccessorBean accessor,GlobalBean global) throws Exception {
		return getDataTable(accessor,global,"tusertype","usertype",null,null,false,false);
	}
	public EntityBean getWeekday(AccessorBean accessor,GlobalBean global) throws Exception {		
		return getDataTable(accessor,global,"tweekday","dayofweek",null,"dayofweek",false,false);
	}
	public boolean isAllBranches(AccessorBean accessor) {
		return isAccessAllBranches(accessor);
	}
	public boolean isAllSites(AccessorBean accessor) {
		return isAccessAllSites(accessor);
	}	
	public boolean isEng(GlobalBean global) {
		return isEnglish(global);
	}
	public boolean isEng(HttpServletRequest request) {
		return isEnglish(request);
	}	
	public boolean isThi(GlobalBean global) {
		return isThai(global);
	}
	public boolean isThi(HttpServletRequest request) {
		return isThai(request);
	}
	public java.util.Map orderByValue(java.util.Map unsortMap) {
		if(unsortMap==null) return unsortMap;
		java.util.List<Map.Entry<Object, Object>> list = new java.util.ArrayList<>(unsortMap.entrySet());
		java.util.Collections.sort(list, new java.util.Comparator<Map.Entry<Object, Object>>() { 
			public int compare(Map.Entry<Object, Object> o1, Map.Entry<Object, Object> o2) {
				if(o1==o2) return 0;
				if(o1==null && o2!=null) return -1;
				if(o1!=null && o2==null) return 1;
				if(o1.getValue()==null && o2.getValue()==null) return 0;
				if(o1.getValue()==null && o2.getValue()!=null) return -1;
				if(o1.getValue()!=null && o2.getValue()==null) return 1;
				return o1.getValue().toString().compareTo(o2.getValue().toString());
			}
		});
		java.util.Map sortedMap = new java.util.LinkedHashMap();
		for(Map.Entry<Object, Object> entry : list) {
			if(entry!=null) {
				sortedMap.put(entry.getKey(), entry.getValue());
			}
		}
		return sortedMap;
	}
	public void orderCategory(HttpServletRequest request,String... categories) {
		if(categories!=null && categories.length>0) {
			for(String category : categories) {
				if(category!=null && category.trim().length()>0) {
					String encat = category+"_EN";
					String thcat = category+"_TH";
					java.util.Map enmap = (java.util.Map)request.getSession().getAttribute(encat);
					java.util.Map thmap = (java.util.Map)request.getSession().getAttribute(thcat);
					if(enmap!=null) {
						enmap = orderByValue(enmap);
						java.util.LinkedHashMap linkmap = null;
						if(enmap instanceof java.util.LinkedHashMap) {
							linkmap = (java.util.LinkedHashMap)enmap;
						} else {
							linkmap = new java.util.LinkedHashMap();
							linkmap.putAll(enmap);
						}
						request.getSession().setAttribute(encat, linkmap);
					}
					if(thmap!=null) {
						thmap = orderByValue(thmap);
						java.util.LinkedHashMap linkmap = null;
						if(thmap instanceof java.util.LinkedHashMap) {
							linkmap = (java.util.LinkedHashMap)thmap;
						} else {
							linkmap = new java.util.LinkedHashMap();
							linkmap.putAll(thmap);
						}						
						request.getSession().setAttribute(thcat, linkmap);
					}
				}
			}
		}
	}
	public void removeCategories(HttpServletRequest request,String category) {
		if(category==null) { return; }
		removeCategories(request,new String[] { category });
	}
	public void removeCategories(HttpServletRequest request,String... categories) {
		if(categories!=null && categories.length>0) {
			for(String category : categories) {
				if(category!=null && category.trim().length()>0) {
					String encat = category+"_EN";
					String thcat = category+"_TH";
					request.getSession().removeAttribute(category);
					request.getSession().removeAttribute(encat);
					request.getSession().removeAttribute(thcat);
				}
			}
		}
	}
}
