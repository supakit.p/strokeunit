<%@page import="org.apache.http.HttpResponse"%>
<%@page import="org.apache.http.entity.mime.HttpMultipartMode"%>
<%@page import="org.apache.http.client.methods.HttpPost"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="org.apache.http.entity.mime.MultipartEntityBuilder"%>
<%@page import="org.apache.commons.fileupload.MultipartStream"%>
<%@page import="org.apache.http.entity.ContentType"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.http.HttpEntity"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.http.message.BasicNameValuePair"%>
<%@page import="org.apache.http.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.fs.dev.strokeunit.HttpClientServices"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.net.URI"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("apiTest");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("Test");
    header.setMethod("Submit");
    try {
        Trace.info("############### Test/Submit ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
//        final String fs_data = fsBean.getDataValue("data").toString();
//        org.json.JSONObject json = new org.json.JSONObject(fs_data);
        Trace.info("##fs_map : " + fs_map);
        //call my api
//        org.json.JSONObject dataPatient = new org.json.JSONObject();
//        dataPatient.put("hn", hn);
//        dataPatient.put("patientId", patientId);
        URI url = new URI("https://quickloan.freewillsolutions.com/rest/handler/as001/check_service");
        HashMap<String, String> headers = new HashMap();
        headers.put(HttpClientServices.CONTENT_TYPE, HttpClientServices.CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED);
        List<NameValuePair> urlParameters = new ArrayList<>();
//        urlParameters.add(new BasicNameValuePair("apiName", "PatientInfoByHN"));
//        urlParameters.add(new BasicNameValuePair("data", dataPatient.toString()));
        org.json.JSONObject resultss = HttpClientServices.sendPost(url, headers, urlParameters);
        System.out.println("resultss : " + resultss);
//        org.json.JSONObject patientInfoHead = new org.json.JSONObject(patientInfo.getString("head"));
//        if (!"2".equals(patientInfoHead.getString("errorcode"))) {
//            org.json.JSONObject patientInfoBody = new org.json.JSONObject(patientInfo.getString("body"));
//        }
        

        header.setErrorflag("N");
        header.setErrorcode("0");
        result.put("head", header);
        result.put("body", resultss);
        out.println(result.toJSONString());
        return;

    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
%>
