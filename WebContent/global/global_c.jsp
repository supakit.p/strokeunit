<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.setup.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%fsAccessor.validate(); %>
<%
	String action = PageUtility.getParameter(request,"action");
	String key = PageUtility.getParameter(request,"key");
	String value = PageUtility.getParameter(request,"value");
	String desc = PageUtility.getParameter(request,"desc");
	if(action!=null) {
		if(action.equals("insert")) {
			if((key!=null) && !key.trim().equals("") && (value!=null)) {
				GlobalVariable.setVariable(key,value);
				value = GlobalVariable.encryptVariable(key,value);
				String realpath = request.getServletContext().getRealPath("");
				String filename = realpath+java.io.File.separator+"global"+java.io.File.separator+"global_descriptor.xml";
				XMLFile xf = new XMLFile();
				java.net.URL url = xf.openResource("global_config.xml");
				xf.setProperty(key,value);
				xf.saveAs(url);
				if(desc!=null && !desc.trim().equals("")) {
					xf = new XMLFile();
					xf.open(filename);
					desc = BeanUtility.nativeToUnicode(desc);
					xf.setProperty(key,desc);
					xf.saveAs(filename);
				}			
			}
			String fs_forwarder = "/global/global.jsp";
			RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
			rd.forward(request,response);
			return;		
		} else if(action.equals("add")) {
		} else if(action.equals("cancel")) {
			String fs_forwarder = "/global/global.jsp";
			RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
			rd.forward(request,response);
			return;
		} else {
			if((key!=null) && (value!=null)) {
				GlobalVariable.setVariable(key,value);
				value = GlobalVariable.encryptVariable(key,value);	
				XMLFile xf = new XMLFile();
				java.net.URL url = xf.openResource("global_config.xml");
				xf.setProperty(key,value);
				xf.saveAs(url);			
			}
			String fs_forwarder = "/global/global.jsp";
			RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
			rd.forward(request,response);
			return;
		}
	}
	String fs_forwarder = "/global/global_de.jsp";
	RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
	rd.forward(request, response);	
%>
