<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.setup.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%fsAccessor.validate(); %>
<%
	String action = PageUtility.getParameter(request,"action");
	String key = PageUtility.getParameter(request,"key");
	String value = PageUtility.getParameter(request,"value");
	String desc = PageUtility.getParameter(request,"desc");
	String initjs = "";
	if("add".equals(action)) {
		initjs = "detform.key.focus()";
	} else {
		initjs = "detform.value.focus()";
	}
%>
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<head>
<title>Global Variables</title>
</head>
<body onload="<%=initjs%>">
<h1 class="page-header-title" title="global">Global Variable Information</h1>
<%if("add".equals(action)) {%>
<form name="detform" method="post" action="global_c.jsp">
	<input type="hidden" name="action" value="insert"></input>
	<table border="1" style="width:100%">
		<tr><th>Key</th><th>Value</th><th>Description</th></tr>
		<tr>
			<td><input type="text" name="key" value="" style="width:99%"/></td>
			<td><input type="text" name="value" value="" style="width:99%" /></td>
			<td><input type="text" name="desc" value="" style="width:99%" /></td>
		</tr>
		<tr>
			<td colspan="3" align="right">
				<input type="submit" name="submit" value="Submit" />
				<input type="reset" name="reset" value="Reset" />
				<input type="submit" name="resume" value="Resume" onclick="action.value='cancel'" />
			</td>
		</tr>
	</table>
</form>
<%} else {%>
<form name="detform" method="post" action="global_c.jsp">
	<input type="hidden" name="action" value="edit"></input>
	<input type="hidden" name="key" value="<%=key%>"></input>
	<table border="1" style="width:100%">
		<tr><th>Key</th><th>Value</th></tr>
		<tr>
			<td><%=key%></td>
			<td><input type="text" name="value" value="<%=value%>" style="width:99%" /></td>
		</tr>
		<tr>
		<td colspan="2" align="right">
			<input type="submit" name="submit" value="Submit" />
			<input type="reset" name="reset" value="Reset" />
			<input type="submit" name="resume" value="Resume" onclick="action.value='cancel'" />
		</td>
		</tr>
	</table>
</form>
<%}%>
</body>
</html>
