<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<jsp:useBean id="fsPermission" scope="page" class="com.fs.bean.util.PermissionBean"/>
<jsp:setProperty name="fsPermission" property="*"/>
<%
	fsPermission.setProgram("global");
	fsAccessor.addPermission(fsPermission);
	String fs_forwarder = "/global/global.jsp";
	RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
	rd.forward(request, response);	
%>
