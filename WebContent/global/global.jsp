<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.dom.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%fsAccessor.validate(); %>
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<head>
<title>Global Variables</title>
</head>
<body>
	<h1 class="page-header-title" title="global">Global Variable Information</h1>
	<table border="1" style="width:100%">
		<tr>
			<td colspan="3">
				<form name="newform" method="post" action="global_c.jsp">
					<input type="submit" value="Add New Key" />
					<input type="hidden" name="action" value="add" />
				</form>
			</td>
		</tr>
		<tr>
			<th>Key</th><th>Value</th><th>Description</th>
		</tr>
<%
	DOMReader dom = new DOMReader();
	String realpath = request.getServletContext().getRealPath("");
	String filename = realpath+java.io.File.separator+"global"+java.io.File.separator+"global_descriptor.xml";
	try(java.io.FileInputStream fin = new java.io.FileInputStream(filename)) {
		dom.loadXML(fin);
	}
	java.util.Map hat = dom.getRootAttributes();	
	int idx = 0;
	for(java.util.Enumeration en = GlobalVariable.variables();en.hasMoreElements();) {
		String key = (String)en.nextElement();	      
		if(key.equals("CONFIG")) continue; //ignore root tag
		String value = "";
		Object object = GlobalVariable.getVariable(key);
		if(object!=null) value = object.toString();
		String desc = (String)hat.get(key);
		if(desc==null) desc = "";
		desc = BeanUtility.nativeToUnicode(desc);	
		idx++;
		out.println("<tr>");
		out.println("<td>");
		out.println("<form id=\"detform"+idx+"\" name=\"detform\" method=\"post\" action=\"global_c.jsp\">");
		out.println("<input type=\"hidden\" name=\"key\" value=\""+key+"\" />");
		out.println("<input type=\"hidden\" name=\"value\" value=\""+value+"\" />");
		out.println("<a href=\"javascript:void(0);\" ondragstart=\"return false;\" onclick=\"detform"+idx+".submit()\">"+key+"</a>");
		out.println("</form>");
		out.println("</td>");
		out.println("<td>"+value+"&nbsp;</td>");
		out.println("<td>"+desc+"&nbsp;</td>");
		out.println("</tr>");
	}
%>
	</table>
</body>
</html>