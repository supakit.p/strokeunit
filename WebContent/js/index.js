		var mouseX = 0;
		var mouseY = 0;
		var $currPage = "";
		var $previousApplication;
		var $currentApplication;
		var fs_working = true;		
		var msgdialog;
		var acceptdialog;
		function validInputUser() {
			if($.trim($("#login_username").val())=="") { alertbox("User is undefined"); return false; }
			return true;
		}
		function connectServer() {	
			if(!validInputUser()) return;
			login();
		}
		function disConnectServer(){
			logOut();
		}
		function reLogin() {
			showRelogin();
		}
		function login(){
			startWaiting();
			jQuery.ajax({
				url: "logon/logon_c.jsp",
				type: "POST",
				contentType: defaultContentType,
				data: $("#login_form").serialize(), 
				dataType: "html",
				error : function(transport,status,errorThrown) { 
					stopWaiting();
					errorThrown = parseErrorThrown(transport, status, errorThrown);
					alertbox(errorThrown);
				},
				success: function(data,status,xhr){ 
					console.log("success : "+xhr.responseText);
					stopWaiting();
					loginSuccess(data);
				}
			});			
		}
		function loginSuccess(data) {
			console.log("login success : "+data);
			var unloadFirstPage = false;
			if($("#fsloginmodaldialog_layer").is(":visible")) {
				unloadFirstPage = true;
			}
			var xmldoc = $.parseXML($.trim(data));
			var type = $("root",xmldoc).attr("type");
			if("error"==type) {
				alertbox($("body",xmldoc).text());
			} else {
				var fs_userid = $("fsUser",xmldoc).text();
				var fs_userdetail = $("fsUserName",xmldoc).text();
				if(fs_default_language=="EN") fs_userdetail = $("fsNameEnglish",xmldoc).text();
				$("#login_user").val(fs_userid);
				$("#accessor_label").html(fs_userdetail);
				$("#accessor_label").data("EN",$("fsNameEnglish",xmldoc).text());
				$("#accessor_label").data("TH",$("fsUserName",xmldoc).text());
				var fs_date = $("fsAccessdate",xmldoc).text();
				var fs_time = $("fsAccesstime",xmldoc).text();
				$("#lastdate_label").html(fs_date+" "+fs_time);
				var expireflag = $("expire",xmldoc).text();
				var changeflag = $("change",xmldoc).text();
				if("1"==changeflag) {
					doChangePassword(fs_userid,xmldoc);
				} else if("1"==expireflag) {
					window.open("change_password/change_password.jsp","_self");
				} else {
					doAfterLogin(xmldoc,unloadFirstPage);
				}
			}			
		}
		function doChangePassword(fs_userid,xmldoc) {
			$("#fsuserid").val(fs_userid);
			$("#page_login").hide();
			$("#fsuserpassword").unbind("focus");
			$("#fsuserpassword").bind("focus",function() { 
				$("#userpassword_alert").hide();
			});
			$("#savechangebutton").unbind("click");
			$("#savechangebutton").bind("click",function() {
				$("#userpassword_alert").hide();
				if($.trim($("#fsuserpassword").val())=="") {
					$("#userpassword_alert").show();
					return;
				}
				startWaiting();
				var xhr = jQuery.ajax({
					url: "logon/password_change_c.jsp",
					type: "POST",
					contentType: defaultContentType,
					data: $("#fspasswordentryform").serialize(),
					dataType: "html",
					error : function(transport,status,errorThrown) {
						stopWaiting();
						errorThrown = parseErrorThrown(transport, status, errorThrown);
						alertbox(errorThrown);
					},
					success: function(data,status,xhr){
						console.log("success : "+xhr.responseText);
						stopWaiting();
						$("#fspasswordmodaldialog_layer").modal("hide");
						doAfterLogin(xmldoc,false);
					}
				});						
			});
			$("#fspasswordmodaldialog_layer").modal("show");			
		}
		function doAfterLogin(xmldoc,unloadFirstPage) {
			startWorking(unloadFirstPage);
			refreshScreen();
			if(xmldoc) showBackground($("fsBackground",xmldoc).text());
			$("#fsloginmodaldialog_layer").modal("hide");
		}
		function startWorking(unloadFirstPage) {
			$('#page_login').hide();
			createMenu(); 
			startupPage(unloadFirstPage); 
		}
		function createMenu(){
			$("#homelayer").show();
			$("#mainmenu").show();
			$("#usermenuitem").show();
			$("#favormenuitem").show();
			$("#loginlayer").hide();
			$("#languagemenuitem").removeClass("language-menu-item");
		}
		function startupPage(unloadFirstPage){
			if(!unloadFirstPage) {
				load_page("page_first");
			}
			load_sidebar_menu();
			load_favor_menu();
			load_prog_item();
			$("#languagemenuitem").show();
		}
		function hideMenu(){
			$("#page_first").hide();
		}
		function fs_changingPlaceholder(lang) {
			if(!lang) return;
			var u_placeholder = fs_getLabelName("login_user_placeholder","index",lang);
			var p_placeholder = fs_getLabelName("login_pass_placeholder","index",lang);
			if(u_placeholder) {
				$("#login_username").attr("placeholder",u_placeholder);
				$("#loginframe").contents().find("#login_username").attr("placeholder",u_placeholder);
			}
			if(p_placeholder) {
				$("#login_pass").attr("placeholder",p_placeholder);
				$("#loginframe").contents().find("#login_pass").attr("placeholder",p_placeholder);
			}
			var last_label = fs_getLabelName("lastaccess_label","index",lang);
			if(last_label) $("#lastaccess_label").html(last_label);
			var log_label = fs_getLabelName("logout_label","index",lang);
			if(log_label) {
				$("#logingout_label").html(log_label);
				$("#logout_label").html(" "+log_label);
			}
			var changepwd_label = fs_getLabelName("changepwd_label","index",lang);
			if(changepwd_label) $("#changepwd_label").html(" "+changepwd_label);
			var profile_label = fs_getLabelName("profile_label","index",lang);
			if(profile_label) $("#profile_label").html(" "+profile_label);
			var signin_label = fs_getLabelName("signin_label","index",lang);
			if(signin_label) $("#loginmenutrigger").html(signin_label);
			console.log("lang = "+lang+" : "+log_label);
			var login_header_label = fs_getLabelName("login_label","index",lang); 
			if(login_header_label) {
				$("#loginframe").contents().find("#login_label").html(login_header_label);
			}
			var login_button_label = fs_getLabelName("login_button","index",lang); 
			if(login_button_label) {
				$("#loginframe").contents().find("#login_button").val(login_button_label);
			}
		}
		function goHome() {
			load_page("page_first");
			$("#languagemenuitem").show();
		}
		function doSignOut() {
			window.open("signout.jsp?seed="+Math.random(),"_self");
		}
		function forceLogout() {
			$.ajax({ async: false, url : "logon/logout.jsp?seed="+Math.random(), type : "POST" });
		}
		function profileClick() {
			open_page("page_profile");
		}
		function changeClick() {
			open_page("page_change");
		}
		function logOut() {
			forceLogout();
			//doSignOut();
			doLogout();
		}
		function doLogout() {
			try{ closeMenuBar(); }catch(ex) { }
			$("#pagecontainer").empty();
			$("#mainmenu").hide();
			if($currPage=="") $currPage = $("#page_first");
			if($currPage) {
				$currPage.removeClass('pt-page-current pt-page-moveFromRight pt-page-moveFromLeft');	
			}
			logInClick();
			hideWorkSpace();
			$("#homelayer").hide();
			$("#mainmenu").hide();
			$("#usermenuitem").hide();
			$("#favormenuitem").hide();
			$("#languagemenuitem").addClass("language-menu-item").show();
			$("#recentmenulist").empty();
			$("#recentcaret").hide();
			$("#loginlayer").show();
			hideNewFavorItem();
			clearBackground();
		}
		function clearBackground() {
			$("body").css("background-image","none");
		}
		function showBackground(bgfile) {
			if(!bgfile) return;
			if($.trim(bgfile)!="") {
				//$("body").css("background-image","url(resources/background/"+bgfile+");");
				$("body").attr("style","background-image: url(resources/background/"+bgfile+");");
			}
		}		
		function logInClick() {
			hideWorkingFrame();
			$("#page_login").show();
			try {
				login_form.reset();
			}catch(ex) { }
			$("#loginframe").attr("src","page_login/page_login_main.jsp?"+Math.random());
		}
		function doLogin() {
			$("#pagecontainer").empty();
			$("#mainmenu").hide();
			if($currPage=="") $currPage = $("#page_first");
			if($currPage) {
				$currPage.removeClass('pt-page-current pt-page-moveFromRight pt-page-moveFromLeft');	
			}
			logInClick();
			hideWorkSpace();
			$("#homelayer").hide();
			$("#mainmenu").hide();
			$("#usermenuitem").hide();
			$("#favormenuitem").hide();
			$("#languagemenuitem").addClass("language-menu-item").show();
			$("#recentmenulist").empty();
			$("#recentcaret").hide();
			$("#loginlayer").show();
			hideNewFavorItem();
		}		
		function forgotClick() {
			hideLoginForm();
			open_page("page_forgot");
		}		
		function load_sidebar_menu() {
			var fs_user = $("#login_user").val();
			if($.trim(fs_user)=="") return;
			jQuery.get("main/side_menu.jsp?fsAjax=true&userid="+fs_user+"&x=1&seed="+Math.random(),function(data){ $("#sidebarlayer").html(data); bindingOnSideBarMenu(); });
		}
		function load_favor_menu() {
			var fs_user = $("#login_user").val();
			if($.trim(fs_user)=="") return;
			jQuery.get("main/favor_menu.jsp?fsAjax=true&userid="+fs_user+"&seed="+Math.random(),function(data){ $("#favorbarmenu").html(data); bindingOnFavorMenu(); });
		}
		function fs_changingLanguage(fs_Language) {
			console.log("changing language = "+fs_Language);
			try{
				fs_changingPlaceholder(fs_Language);
				if(fs_currentpid && fs_currentpid!="index") {
					fs_switchingLanguage(fs_Language,"index");
				}
			}catch(ex) { }
			var fs_name = $("#accessor_label").data(fs_Language);
			if(fs_name) $("#accessor_label").html(fs_name);
			load_sidebar_menu();
		}
		function refreshScreen() {
			$(window).trigger("resize");
		}
		function getTargetFrameName() { return "workingframe"; }
		function hideLoginForm() {
			$("#page_login").hide();
		}
		function showWorkingFrame() {
			$("#pagecontainer").hide();
			$("#workingframe").show();
		}
		function hideWorkingFrame() {
			$("#pagecontainer").hide();
			hideWorkSpace();
		}
		function hideWorkSpace() {
			$("#workingframe").hide();
			window.open("assets/blank.html","workingframe");
		}
		function showRelogin() {
			$("#reloginframe").attr("src","page_login/page_login_main.jsp?"+Math.random());
			$("#fsloginmodaldialog_layer").modal("show");			
		}
		var fs_workingframe_offset = 10;
		$(function(){
			$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
			msgdialog = createDialog("#fsdialoglayer");	
			acceptdialog = createDialog("#fsacceptlayer");
			try { startApplication("index",true); }catch(ex) { }
			//ignore force logout coz it was invalidate when refresh
			//try { $(window).bind("unload",forceLogout); }catch(ex) { }
			$("#login_pass").on("keydown", function (e) {
				if(e.which==13) { connectServer(); }
			});
			$(window).resize(function() { 
					var wh = $(window).height();
					var nh = $("#navigatebar").height();
					var fh = $("#footerbar").height();
					$("#workingframe").height((wh-nh-fh) - fs_workingframe_offset);
			}).trigger("resize");
			var pos = $("#loginframe").position();
			if(pos) { mouseX = pos.left; mouseY = pos.top; }
		});
		
