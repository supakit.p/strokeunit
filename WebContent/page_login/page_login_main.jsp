﻿<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/logon/logon_errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<c:if test="${fsScreen.initial(pageContext.request, pageContext.response)}"></c:if>
<c:if test="${fsScreen.define(pageContext.request, pageContext.response)}"></c:if>
<c:if test="${fsScreen.initialConfig('index',pageContext.request, pageContext.response)}"></c:if>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Log In</title>		
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../bootstrap/bootstrap-min.css" rel="stylesheet" type="text/css" />
		<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="../page_login/page_login_main.css" /> 
		<link rel="stylesheet" type="text/css" href="../css/user_style.css" /> 
		<script type="text/javascript">
			function onInit() {
			<%
				String errorMessage = (String)request.getAttribute("error_message");
				if(errorMessage!=null && errorMessage.trim().length()>0) {
					%>
						try { window.parent.alertbox("<%=errorMessage%>"); }catch(ex) { alert("<%=errorMessage%>"); }
					<%
				}
			%>				
			}
			function submitLogin() {
				try { window.parent.startWaiting(); } catch(ex) { }
				login_form.submit();
			}
		</script>			
	</head>
	<body class="portalbody portalbody-off" onload="onInit()">
		<div id="page_login_layer">
			<form class="cbp-mc-form" id="login_form" name="login_form" autocomplete="off" method="POST" action="../logon/logon_frame_c.jsp">
				<input type="hidden" id="login_user" />
				<div id="login_formarea" class="login_form portal-area">     
					<h3 id="login_label">${fsLabel.getText('login_label','Log In')}</h3> 
					<div class="input-group" id="user_layear">
						<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
						<input type="text" class="form-control input-md" id="login_username" name="username" placeholder="${fsLabel.getText('login_user_placeholder','User')}" maxlength="45"/>
					</div>
					<br/>
					<div class="input-group" id="password_layer">
						<span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
						<input type="password" class="form-control input-md" id="login_pass" name="password" placeholder="${fsLabel.getText('login_pass_placeholder','Password')}" autocomplete="off" />
					</div>
					<br/>
					<div class="text-center">
						<input type="button" onclick="submitLogin()" id="login_button" value="${fsLabel.getText('login_button','Log In')}"/>
					</div>
					<br/>
				</div>
			</form>
		</div>                                                        		
	</body>
</html>