<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>

<div id="fsloginmodaldialog_layer" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
				<div id="page_login" class="pt-page pt-page-1 pt-page-current">
					<div id="reloginformarea" class="login_form portal-area" style="width: 340px;">     
						<iframe id="reloginframe" name="reloginframe" src="page_login/page_login_main.jsp" style="border:none; margin:0px; width: 300px; height: 250px;" onload="try{stopWaiting();}catch(ex){}" title=""></iframe>        
					</div>
				</div>                                                        
		</div>
	</div>
</div>
