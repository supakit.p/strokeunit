                	<div id="page_login" class="pt-page pt-page-1 pt-page-current">
                        	<form class="cbp-mc-form" id="login_form" name="login_form" autocomplete="off">
                                <div id="loginformarea" class="login_form portal-area">     
                                	<label id="login_label">${fsLabel.getText('login_label','Log In')}</label>   
									<br/>      
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
										<input type="text" id="login_username" name="username" placeholder="${fsLabel.getText('login_user_placeholder','User')}" maxlength="45"/>
										<input type="hidden" id="login_user" />
									</div>
                                    <br/>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
										<input type="password" id="login_pass" name="password" placeholder="${fsLabel.getText('login_pass_placeholder','Password')}" autocomplete="off" />
									</div>
									<br/>
                                    <input type="button" onclick="connectServer();" id="login_button" value="${fsLabel.getText('login_button','Log In')}"/>
                                    <br/>
                                </div>
                            </form>
                    </div>                                                        
