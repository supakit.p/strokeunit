﻿<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% 
    String fs_forwarder = "/intro/intro.jsp?home=true";
    RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
    rd.forward(request, response);
%>
