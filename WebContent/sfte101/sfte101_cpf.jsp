<?xml version="1.0" encoding="UTF-8"?>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/xmlerrorpage.jsp"%>
<%@ page contentType="application/pdf"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.dev.exim.*"%>
<%!
//#it's strong enough to break into another activities here
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
<%
//#scrape & keeping say something anyway
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<% fsAccessor.validate(); %>
<% session.removeAttribute("fsGlobal"); %>
<jsp:useBean id="fsGlobal" scope="session" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<% session.removeAttribute("fsSFTE101Bean"); %>
<jsp:useBean id="fsSFTE101Bean" scope="session" class="com.fs.bean.SFTE101Bean"/>
<jsp:setProperty name="fsSFTE101Bean" property="*"/>
<jsp:useBean id="fsSFTE101Bean" scope="session" class="com.fs.bean.SFTE101Bean"/>
<%
//#import and uses wherever you will go
//#(10000) programmer code begin;
//#(10000) programmer code end;
//#if the condition come to me
//#(15000) programmer code begin;
//#(15000) programmer code end;
String fs_type = "result";
String fs_code = "";
StringBuffer fs_result = new StringBuffer();
boolean fsIsXML = fsGlobal.isXml();
fsGlobal.setFsProg("sfte101");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.setFsSection(null);
try {
	fsSFTE101Bean.obtain(session,request);
	//#initialize & assigned variables sitting down here
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	String[] fs_fields = new String[] {"data"};
	PDFWriter fs_writer = new PDFWriter();
	fs_writer.addHeader("data","data :");
	//#color of the wind
	//#(22500) programmer code begin;
	//#(22500) programmer code end;
	PageUtility.createExportSchema(fs_writer,fsGlobal,request,fs_fields);
	//#handle array fields
	//#(25000) programmer code begin;
	//#(25000) programmer code end;
	if(fsIsXML) {
		out.println("<root>");
		out.println(fsSFTE101Bean.toXMLData(BeanUtility.NATIVE,fs_fields));
		out.println("</root>");
	} else {
		if(fsSFTE101Bean.size()>0) {
			fs_writer.execute(request,response,"Authen",fsSFTE101Bean.iteratorElements());
		} else {
			fs_writer.process(request,response,"Authen","Records not found");
		}
	}
	//#mode handle & other values never be the same again
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	return;
} catch(Throwable ex) {
	fsGlobal.setThrowable(ex);
	Trace.error(fsAccessor,ex);
}
//#before handle forward page
//#(35000) programmer code begin;
//#(35000) programmer code end;
if(fsGlobal.isException()) {
	fs_type = "error";
	fs_result.append("<body>");
	fs_result.append(fsGlobal.getFsMessage());
	fs_result.append("</body>");
}
//#born to try handle forward page
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
<message type="<%=fs_type%>" code="<%=fs_code%>">
<%=fs_result.toString()%>
</message>
