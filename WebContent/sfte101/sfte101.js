/* SCCS Id: $Id$ */
var defaultContentType = "application/x-www-form-urlencoded; charset=UTF-8";
var globalMode = "U"; //unknown mode
var insertHTML; //cache insert screen
var mouseX=0;
var mouseY=0;
var msgdialog = null;
var acceptdialog = null;
var saveSuccessMessage = 'Process Success';
var deleteSuccessMessage = 'Process Success';
var mergeHTML = false;
var isDXGrid = false;
var isAXGrid = false;
//#declare variable script
//#(31000) programmer code begin;
//#(31000) programmer code end;
$(function() {
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	//#(31500) programmer code begin;
	//#(31500) programmer code end;
	$("#workbook").tabs({  
		//#(32000) programmer code begin;
		//#(32000) programmer code end;
		select : function(event,ui) { 
			//verify entry page? go to insert mode
			try{ startChangePage(); }catch(ex) { }
			if(ui.index==1) { 
				hideLayer(fsexportlayer);
				if(globalMode && globalMode=="E") return true;
				startInsert(); 
			}
			//#(32250) programmer code begin;
			//#(32250) programmer code end;
			globalMode="U";
			return true;
		} , show : function(event,ui) {
			if(ui.index==0) {
				$("input:visible:eq(0)",$("#fssearchform")).trigger("focusin").focus();
			} else {
				$("input:visible:eq(0)",$("#null")).trigger("focusin").focus();
			}
			//#(32350) programmer code begin;
			//#(32350) programmer code end;
		}
		//#(32500) programmer code begin;
		//#(32500) programmer code end;
	});
	//#(32750) programmer code begin;
	//#(32750) programmer code end;
	$("#fssearchform").submit(function() {
		return submitSearch(this);
	});
	initialApplication();
	handleFocus();
	try { $('#fsswitcher').themeswitcher(); } catch(ex) { }
	try { msgdialog = createDialog("#fsdialoglayer");	} catch(ex) { }
	try { acceptdialog = createDialog("#fsacceptlayer");	} catch(ex) { }
	try { setupScreen(); } catch(ex) { }
	try { startApplication("sfte101"); } catch(ex) { }
	try { setUnknownMode(); } catch(ex) { }
	//#(32800) programmer code begin;
	//#(32800) programmer code end;
	initialFocus($("#fssearchform"));
	$("#fsinsertbutton").click(function() { $("#workbook").tabs("option","selected",1); });
	//#(33000) programmer code begin;
	//#(33000) programmer code end;
});
function initialApplication() {
	//#(33100) programmer code begin;
	//#(33100) programmer code end;
	createTaborder();
	setupApplication();
	handleFocus();
	return;
	//#(33250) programmer code begin;
	//#(33250) programmer code end;
	if(isAXGrid) {
		$("#workbook").tabs("remove",1);
		createActiveXGrid();
		return;
	}
	if(mergeHTML) {
		insertHTML = "{}";
		//#(33300) programmer code begin;
		//#(33300) programmer code end;
		createTaborder();
		setupApplication();
		handleFocus();
		startEntry(null,"");
		return;
	}
	//#(33400) programmer code begin;
	//#(33400) programmer code end;
	var params = "fsAction=entry&fsAjax=true";
	var xhr = jQuery.ajax({
		url: "sfte101_de.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: params, 
		dataType: "html",
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data){ 
			insertHTML = data; 
			$("#entrypanel").html(data);
			//#(33500) programmer code begin;
			//#(33500) programmer code end;
			createTaborder();
			setupApplication();
			handleFocus();
			startEntry(null,"");
		}
	});
	//#(33750) programmer code begin;
	//#(33750) programmer code end;
}
function submitSearch(aform) {
	if(isAXGrid) aform.fsDatatype.value = "jsonarray";
	if(isDXGrid) aform.fsXmldata.value = "true";
	//#(34050) programmer code begin;
	//#(34050) programmer code end;
	disableForm(aform);
	startWaiting();
	var xhr = jQuery.ajax({
		url: "sfte101_c.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: $(aform).serialize(),
		dataType: "html",
		error : function(transport,status,errorThrown) { 
			enableForm(aform); 
			submitFailure(transport,status,errorThrown); 
		},
		success: function(data){ 
			enableForm(aform); 
			searchComplete(xhr,data); 
		}
	});
	//#(34500) programmer code begin;
	//#(34500) programmer code end;
	return false;
}
function searchComplete(xhr,data) {
	//#(34750) programmer code begin;
	//#(34750) programmer code end;
	stopWaiting();
	showLayer(workpanel);
	//#(34800) programmer code begin;
	//#(34800) programmer code end;
	if(isAXGrid) {
		prepareActiveXGridContent(xhr,data);
		return;
	}
	if(isDXGrid) {
		try { prepareGridContent(data,1); } catch(ex) { }
	} else {
		$("#listpanel").html(data);
	}
	//#(34850) programmer code begin;
	//#(34850) programmer code end;
	$("#workpanel").css("display","");
	$("#workbook").tabs({ disabled: [] });
	$("#workbook").tabs("option","selected",0);
	try { handleDataTable(); }catch(ex) { }
	try { refreshScreen(); }catch(ex) { }
	//#(35050) programmer code begin;
	//#(35050) programmer code end;
}
function startInsert(aform) {
	//#(35250) programmer code begin;
	//#(35250) programmer code end;
	if(insertHTML) {
		startEntry(null,insertHTML);
		//#(35500) programmer code begin;
		//#(35500) programmer code end;
		return;
	}
	//#(35750) programmer code begin;
	//#(35750) programmer code end;
	startWaiting();
	var params = "fsAction=entry&fsAjax=true";
	if(aform) params = $(aform).serialize();
	var xhr = jQuery.ajax({
		url: "sfte101_dc.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: params, 
		dataType: "html",
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data){ 
			stopWaiting();
			insertHTML = data; 
			startEntry(xhr,data); 
		}
	});
	//#(36000) programmer code begin;
	//#(36000) programmer code end;
}
function submitDetail(aform,rowIndex) {
	//#(36500) programmer code begin;
	//#(36500) programmer code end;
	startWaiting();
	globalMode="E";
	aform.fsJson.value='false';
	var isDeleteMode = aform.fsAction.value=='delete';
	if(!isDeleteMode) aform.fsJson.value='true';
	var xhr = jQuery.ajax({
		url: "sfte101_dc.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: $(aform).serialize(),
		dataType: "html",
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data){ 
			stopWaiting();
			//#(36600) programmer code begin;
			//#(36600) programmer code end;
			if(isDeleteMode) {
				globalMode="U";
				//#(36620) programmer code begin;
				//#(36620) programmer code end;
				if(isDXGrid) {
					try { prepareGridContent(data); } catch(ex) { }
				} else {
					$("#listpanel").html(data); 
				}
				//#(36630) programmer code begin;
				//#(36630) programmer code end;
				$("#workbook").tabs("option","selected",0); 
				try { handleDataTable(); } catch(ex) { }
				try { refreshScreen(); }catch(ex) { }
				//#(36650) programmer code begin;
				//#(36650) programmer code end;
				try { successbox(deleteSuccessMessage); } catch(ex) { }
			} else {
				startEntry(xhr,data); 
				$("#workbook").tabs("option","selected",1);
			}
			//#(36700) programmer code begin;
			//#(36700) programmer code end;
		}
	});
	//#(37000) programmer code begin;
	//#(37000) programmer code end;
	return false;
}
function submitFailure(xhr,status,errorThrown) {
	stopWaiting();
	errorThrown = parseErrorThrown(xhr, status, errorThrown);
	//#(37500) programmer code begin;
	//#(37500) programmer code end;
	alertbox(errorThrown);
}
function prepareScreenToInsert(aform,data) {
	//#(37525) programmer code begin;
	//#(37525) programmer code end;
	aform.fsJson.value = "true";
	aform.fsAction.value = "enter";
	try { clearingFields(aform); } catch(ex) { }
	$("input.ikeyclass",aform).editor({edit:true});
	//#(37550) programmer code begin;
	//#(37550) programmer code end;
}
function prepareScreenToUpdate(aform,data) {
	//#(37575) programmer code begin;
	//#(37575) programmer code end;
	aform.fsJson.value = "false";
	aform.fsAction.value = "edit";
	try {
		var jsRecord = $.parseJSON(data);
		for(var p in jsRecord) {
			$("#"+p,aform).val(jsRecord[p]);
		}
		//#(37600) programmer code begin;
		//#(37600) programmer code end;
	} catch(ex) { }
	$("input.ikeyclass",aform).editor({edit:false});
	//#(37650) programmer code begin;
	//#(37650) programmer code end;
}
function startEntry(xhr,data) {
	//#(37750) programmer code begin;
	//#(37750) programmer code end;
	try {
		if(globalMode && globalMode=="E") {
			prepareScreenToUpdate(null,data);
		} else {
			prepareScreenToInsert(null);
		}
	} catch(ex) { }
	handleFocus();
	try { if(globalMode && globalMode=="E") setEditMode(); else setInsertMode(); } catch(ex) { }
	//#(38000) programmer code begin;
	//#(38000) programmer code end;
}
function submitEntry(aform) {
	if(isDXGrid) aform.fsXmldata.value = "true";
	//#(39000) programmer code begin;
	//#(39000) programmer code end;
	disableForm(aform);
	startWaiting();
	try { aform.fsPage.value = fslistform.fsPage.value; } catch(ex) { }
	var isInsertMode = aform.fsAction.value=='enter';
	var xhr = jQuery.ajax({
		url: "sfte101_dc.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: $(aform).serialize(),
		dataType: "html",
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data) { 
			stopWaiting();
			//#(39500) programmer code begin;
			//#(39500) programmer code end;
			if(isInsertMode) {
				prepareScreenToInsert(aform);
				initialFocus(aform);
				//#(39650) programmer code begin;
				//#(39650) programmer code end;
			} else {
				//#(39660) programmer code begin;
				//#(39660) programmer code end;
				if(isDXGrid) {
					try { prepareGridContent(data); } catch(ex) { }
				} else {
					$("#listpanel").html(data); 
				}
				//#(39680) programmer code begin;
				//#(39680) programmer code end;
				$("#workbook").tabs("option","selected",0); 
				try { handleDataTable(); } catch(ex) { }
				try { refreshScreen(); }catch(ex) { }
				//#(39700) programmer code begin;
				//#(39700) programmer code end;
			}
			try { successbox(saveSuccessMessage); } catch(ex) { }
			//#(39750) programmer code begin;
			//#(39750) programmer code end;
		}
	});
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	return false;
}
function submitChapter(aform) {
	//#(41000) programmer code begin;
	//#(41000) programmer code end;
	startWaiting();
	var xhr = jQuery.ajax({
		url: "sfte101_c.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: $(aform).serialize(),
		dataType: "html",
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data){ 
			stopWaiting();
			$("#listpanel").html(data); 
			try { handleDataTable(); } catch(ex) { }
			try { refreshScreen(); }catch(ex) { }
			//#(41500) programmer code begin;
			//#(41500) programmer code end;
		}
	});
	//#(42000) programmer code begin;
	//#(42000) programmer code end;
	return false;
}
function submitOrder(fsParams) {
	//#(43000) programmer code begin;
	//#(43000) programmer code end;
	startWaiting();
	var xhr = jQuery.ajax({
		url: "sfte101_cd.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: fsParams,
		dataType: "html",
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data){ 
			stopWaiting();
			$("#listpanel").html(data); 
			try { handleDataTable(); } catch(ex) { }
			try { refreshScreen(); }catch(ex) { }
			//#(43500) programmer code begin;
			//#(43500) programmer code end;
		}
	});
	//#(44000) programmer code begin;
	//#(44000) programmer code end;
	return false;
}
function cancelAction() {
	//#(44250) programmer code begin;
	//#(44250) programmer code end;
	$("#workbook").tabs("option","selected",0); 
	//#(44350) programmer code begin;
	//#(44350) programmer code end;
	return false;
}
function insertAction() {
	//#(44450) programmer code begin;
	//#(44450) programmer code end;
	$("#workbook").tabs("option","selected",1); 
	//#(44550) programmer code begin;
	//#(44550) programmer code end;
}
function exportExcelHandler() {
	var fs_params = "";
	//#(45000) programmer code begin;
	//#(45000) programmer code end;
	if(isAXGrid) {
		exportActiveXGridToExcel();
		return;
	}
	if(isDXGrid) {
		exportGridToExcel();
	} else {
		openWindow("sfte101_dex.jsp?file=sfte101_dex.xls"+fs_params,"sfte101_excel_window");
	}
	//#(45500) programmer code begin;
	//#(45500) programmer code end;
}
function exportPdfHandler() {
	var fs_params = "";
	//#(46000) programmer code begin;
	//#(46000) programmer code end;
	if(isDXGrid) {
		exportGridToPDF();
	} else {
		openWindow("sfte101_dpf.jsp?file=sfte101_dpf.pdf"+fs_params,"sfte101_pdf_window");
	}
	//#(46500) programmer code begin;
	//#(46500) programmer code end;
}
function setupScreen() {
	//#(47000) programmer code begin;
	//#(47000) programmer code end;
	if(isDXGrid) {
		try {
			fsCreateGridView();
			fssearchform.fsXmldata.value = "true";
		}catch(ex) { }
	}
	//#(47500) programmer code begin;
	//#(47500) programmer code end;
}
//#script as need so amazing
//#(30000) programmer code begin;
//#(30000) programmer code end;
