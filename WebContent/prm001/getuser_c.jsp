<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('prm001',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("prm001");
fsGlobal.setFsSection("PROMPT");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
JSONHeader header = new JSONHeader();
header.setModel("prm001");
header.setMethod("getuser");
try { 
	fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
	//create bean in order to obtain parameter from request or rest injection
	com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
//	fsBean.addSchema("userid",java.sql.Types.VARCHAR,"userid");
	fsBean.obtainFrom(request); //assign variable from request
	fsBean.forceObtain(fsGlobal);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsBean.obtain(fs_map); //assign variable from rest
	fsBean.obtain(session,request);
	JSONObject body = new JSONObject();
	final String fs_userid = fsBean.getString("userid");
        Trace.info("fsBean : " + fsBean + " fs_map : " + fs_map.containsKey("useridddd"));
	if(fs_userid !=null && fs_userid.trim().length()>0) {
		body.put("userid",fs_userid);
		final JSONObject json = new JSONObject();
		com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
			//override method retrieve depend on action from fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
			public int retrieve(java.sql.Connection connection) throws Exception {
				int result = 0;
				KnSQL knsql = new KnSQL(this);
				knsql.append("select * ");
				knsql.append("from tuser ");
				//knsql.append("left join tuserinfo on tuserinfo.userid = tuser.userid ");
				knsql.append("where tuser.userid = ?userid ");
				knsql.setParameter("userid",fs_userid);
				try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
					java.sql.ResultSetMetaData met = rs.getMetaData();
					if(rs.next()) {
						result++;
						int colcount = met.getColumnCount();
						for(int i=1;i<=colcount;i++) {
							String colname = met.getColumnName(i);
							json.put(colname,rs.getString(colname));
						}
					}
				}
				return result;
			}	
		};
		TheTransportor.transport(fsGlobal,fsExecuter);
		if(fsExecuter.effectedTransactions()>0) {
			header.setErrorflag("N");
			header.setErrorcode("0");
			result.put("head",header);
			body.put("userinfo",json);
			result.put("body",body);
			out.println(result.toJSONString());
			return;
		}
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	header.setErrorflag("Y");
	header.setErrorcode("1");
	header.setErrordesc(fsGlobal.getFsMessage());
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
header.setErrorflag("Y");
header.setErrorcode("2");
header.setErrordesc(fsLabel.getText("notfound","Not Found"));
result.put("head",header);
out.println(result.toJSONString());
%>
