<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<c:if test="${fsScreen.config('prm001',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("prm001");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
JSONHeader header = new JSONHeader();
header.setModel("prm001");
header.setMethod("edituser");
try { 
	fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
	//create bean in order to obtain parameter from request or rest injection
	com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
	fsBean.addSchema("userid",java.sql.Types.VARCHAR,"userid");
	fsBean.addSchema("status",java.sql.Types.VARCHAR,"status");
	fsBean.addSchema("lockflag",java.sql.Types.VARCHAR,"lockflag");
	fsBean.obtainFrom(request); //assign variable from request
	fsBean.forceObtain(fsGlobal);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsBean.obtain(fs_map); //assign variable from rest
	fsBean.obtain(session,request);
	final String fs_userid = fsBean.getString("userid");
	final String fs_status = fsBean.getString("status");
	final String fs_lockflag = fsBean.getString("lockflag");
	if((fs_userid !=null && fs_userid.trim().length()>0) && (fs_status !=null && fs_status.trim().length()>0)) {
		com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
			//override method update depend on action from fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
			public int update(java.sql.Connection connection) throws Exception {
				KnSQL knsql = new KnSQL(this);
				knsql.append("update tuser set status=?status, lockflag=?lockflag ");
				knsql.append("where userid = ?userid ");
				knsql.setParameter("userid",fs_userid);
				knsql.setParameter("status",fs_status);
				knsql.setParameter("lockflag",fs_lockflag);				
				return knsql.executeUpdate(connection);
			}	
		};
		TheTransportor.transport(fsGlobal,fsExecuter);
		if(fsExecuter.effectedTransactions()>0) {
			header.setErrorflag("N");
			header.setErrorcode("0");
			result.put("head",header);
			out.println(result.toJSONString());
			return;
		}
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	header.setErrorflag("Y");
	header.setErrorcode("1");
	header.setErrordesc(fsGlobal.getFsMessage());
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
header.setErrorflag("Y");
header.setErrorcode("2");
header.setErrordesc(fsLabel.getText("notfound","Not Found"));
result.put("head",header);
out.println(result.toJSONString());
%>
