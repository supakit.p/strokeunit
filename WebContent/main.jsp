﻿<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<c:if test="${fsScreen.initial(pageContext.request, pageContext.response)}"></c:if>
<c:if test="${fsScreen.define(pageContext.request, pageContext.response)}"></c:if>
<c:if test="${fsScreen.initialConfig('index',pageContext.request, pageContext.response)}"></c:if>
<%
if(com.fs.dev.TheUtility.isSignInAuthenticated(fsAccessor) && com.fs.dev.TheUtility.isExpired(fsAccessor)) {
	response.sendRedirect(request.getContextPath()+"/change_password/change_password.jsp");
	return;
}
%>
<jsp:include page="jsp/checkbrowser.jsp"/>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Car Loan</title>		
		<jsp:include page="jsp/meta.jsp"/>
		<link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles('MAIN_STYLES')}" escapeXml="false"></c:out>
		<jsp:include page="jsp/stylesmain.jsp"/>
		<link rel="stylesheet" type="text/css" href="page_login/page_login.css" />
		<link rel="stylesheet" type="text/css" href="css/user_style.css" />
		<c:out value="${fsScreen.createScripts('MAIN_SCRIPTS',pageContext.request, pageContext.response)}" escapeXml="false"></c:out>
		<jsp:include page="jsp/scriptsmain.jsp"/>
		<script type="text/javascript" src="js/index.js?v4"></script>
		<% 
		String fs_backgroundfilename = "";
		String fs_accessdatetime = "";
		String fs_accessorname = fsAccessor.getFsUserName();
		String fs_thname = fsAccessor.getFsUserName();
		String fs_enname = fsAccessor.getFsVar("fsNameEnglish");
		if(fs_enname==null || fs_enname.trim().length()<=0) fs_enname = fs_thname;
		if("EN".equals(com.fs.bean.util.PageUtility.getDefaultLanguage(request))) {
			fs_accessorname = fs_enname;
		}
		if(fsAccessor.isValid()) {
			String fs_bgfile = fsAccessor.getFsVar("fsBackground");
			if(fs_bgfile!=null && fs_bgfile.trim().length()>0) {
				fs_backgroundfilename = "style=\"background-image: url(resources/background/"+fs_bgfile+"); \"";
			}
			fs_accessdatetime = fsAccessor.getFsVar("fsAccessdate")+" "+fsAccessor.getFsVar("fsAccesstime");
		%>
		<script type="text/javascript">
			$(function() { 
				$("#login_user").val('<%=fsAccessor.getFsUser()%>');
				$("#accessor_label").html('<%=fs_accessorname%>');
				$("#accessor_label").data("EN",'<%=fs_enname%>');
				$("#accessor_label").data("TH",'<%=fs_thname%>');
				<%if(com.fs.dev.TheUtility.isChanged(fsAccessor)) {%>
				doChangePassword('<%=fsAccessor.getFsUser()%>');
				<%} else {%>
				doAfterLogin();
				<%}%>
			});
		</script>
		<%}%>
	</head>
	<body class="portalbody portalbody-off" <%=fs_backgroundfilename %>>
		<nav id="navigatebar" class="navbar navigatebar-top" style="min-height: 30px;">
			<jsp:include page="main/main_header.jsp"/>
		</nav>
		<nav id="sidebarmenu" class="sidebar sidebar-menu sidebar-hide">
			<div id="sidebarheader" class="sidebar-header">
				<label id="lastaccess_label" style="font-size:12px;">${fsLabel.getText('lastaccess_label','Last Access')}</label>&nbsp;&nbsp;&nbsp;<label id="lastdate_label" style="font-size:12px;"><%=fs_accessdatetime%></label><br/>
			</div>
			<div id="sidebarlayer" class="sidebar-layer">				
			</div>
			<div id="sidebarfooter" class="sidebar-footer">
				<a href="javascript:void(0)" onclick="logOut()"><em class="fa fa-sign-out fa-rotate-180"></em> &nbsp;<label id="logingout_label">${fsLabel.getText('logout_label','Log Out')}</label></a>
			</div>
		</nav>
		<jsp:include page="page_login/page_login.jsp"/>
		<div id="pagecontainer" class="pt-pager"></div> 
		<iframe id="workingframe" name="workingframe" width="100%" style="border:none; margin:0px; display:none;" onload="try{stopWaiting();}catch(ex){}" title=""></iframe>
		<jsp:include page="main/main_footer.jsp"/>
		<div id="fsdialoglayer" style="display:none;"><span id="fsmsgbox"></span></div>
		<div id="fsacceptlayer" style="display:none;"><span id="fsacceptbox"></span></div>
		<div id="fswaitlayer" style="display:none; position:absolute; left:1px; top:1px; z-Index:9999;"><img id="waitImage" class="waitimgclass" src="images/waiting.gif" width="50px" height="50px" alt=""></img></div>	
		<jsp:include page="main/main_context.jsp"/>
		<jsp:include page="main/password_dialog.jsp"/>		
	</body>
</html>