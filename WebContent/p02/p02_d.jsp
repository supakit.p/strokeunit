<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsP02Bean" scope="session" class="com.fs.bean.P02Bean"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<form name="fslistform" id="fslistform" method="post" autocomplete="off">
	<input type="hidden" name="fsAction" value="view"/>
	<input type="hidden" name="fsAjax" value="false"/>
	<input type="hidden" name="fsDatatype" value="text"/>
	<input type="hidden" name="fsRowid" value=""/>
	<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
	<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
	<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>
	<input type="hidden" name="focusid" />
<%
//#(11000) programmer code begin;
//#(11000) programmer code end;
%>
</form>
<div id="fseffectedtransactions" style="display:none;">${fsP02Bean.effectedTransactions()}</div>
<table id="datatable" class="table table-bordered table-hover table-striped tablesorter">
	<thead>
		<tr>
			<th class="text-center"  style="width: 10%;"><fs:label tagid="seqno_headerlabel" >ลำดับที่ <br /> <t1 style="font-size:14px">(No.)</t1> </fs:label></th>
			<th class="text-center"style="width: 10%;"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=focusid&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="focusid_headerlabel">รหัส Focus <br /><t1 style="font-size:14px">(Focus ID)</t1> </fs:label></a></th>
			<th class="text-center" style="width: 50%;"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=focusname&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="focusname_headerlabel">ชื่อ Focus <br /><t1 style="font-size:14px">(Focus Name)</t1></fs:label></a></th>
			<th class="text-center" style="width: 20%;"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=isactive&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="isactive_headerlabel">สถานะการใช้งาน<br /><t1 style="font-size:14px">(Active)</t1></fs:label></a></th>
<%
			//#(12000) programmer code begin;
			//#(12000) programmer code end;
%>
			<th class="text-center" style="width: 10%;">การดำเนินการ <br /> <t1 style="font-size:14px">(Action)</t1></em></th>
<%
			//#(13000) programmer code begin;
			//#(13000) programmer code end;
%>
		</tr>
	</thead>
	<tbody id="datatablebody">							
		<c:choose>
			<c:when test="${fsP02Bean.size() > 0}">
				<c:forEach var="fsElement" items="${fsP02Bean.elements()}" varStatus="records">
					<c:if test="${!fsGlobal.nextChapter(records.count)}">
						<tr>
							<td class="text-center">${records.count}</td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count},'${fsElement.focusid}');">${fsElement.focusid}</a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count},'${fsElement.focusid}');">${fsElement.focusname}</a></td>
							<c:choose>
							<c:when test = "${fsElement.isactive == 'false'}">
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count},'${fsElement.focusid}');">Inactive</a></td>
							</c:when>
							<c:otherwise>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count},'${fsElement.focusid}');">Active</a></td>
    						  
    						</c:otherwise>
    						</c:choose>
<%
							//#(14000) programmer code begin;
							//#(14000) programmer code end;
%>
							<td class="text-center">
									<button name="btn-edit" type="button" class="btn-edit" title="${fsLabel.getLabel('btnedit_tooltip')}"
											onclick="submitRetrieve(${records.count},'${fsElement.getString('focusid')}');"></button>
									
							</td>
<%
							//#(15000) programmer code begin;
							//#(15000) programmer code end;
%>
						</tr>
					</c:if>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:if test="${!fsScreen.isViewState()}">
					<tr>
						<td class="text-center" colspan="5">
								${fsLabel.getLabel("recordnotfound")}
						</td>
					</tr>
				</c:if>
			</c:otherwise>
		</c:choose>		
	</tbody>
</table>	
<%
//#(16000) programmer code begin;
//#(16000) programmer code end;
%>
<div style="text-align: center;" class="fschaptertablelayer">
<table style="margin: auto;" class="fschaptertable">
	<tr class="fschapterrow"><td class="fschaptercolumn">
	<form name="fschapterform" id="fschapterform" method="post" autocomplete="off">
		<input type="hidden" name="fsAction" value="chapter"/>
		<input type="hidden" name="fsAjax" value="true"/>
		<input type="hidden" name="fsDatatype" value="text"/>
		<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
		<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
		<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>
		<input type="hidden" name="fsSorter" value=""/>
	</form>
	<div id="fschapterlayer">
		<c:if test="${fsGlobal.hasPaging(fsPager)}">
			${fsGlobal.createPaging(fsPager)}
		</c:if>
	</div>
	</td>
	</tr>
</table>
</div>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
