<%@ page info="SCCS id: $Id$"%>
<%@ page language="java" contentType="application/pdf" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.dba.*"%>
<%@ page import="com.fs.dev.exim.*"%>
<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsP02Bean" scope="session" class="com.fs.bean.P02Bean"/>
<jsp:setProperty name="fsP02Bean" property="*"/>
<%
fsGlobal.setFsAction(GlobalBean.EXPORT_MODE);
//#(10000) programmer code begin;
//#(10000) programmer code end;
fsGlobal.setFsProg("p02");
fsGlobal.setFsSection("PROMPT");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
//#(20000) programmer code begin;
//#(20000) programmer code end;
PDFWriter fs_writer = new PDFWriter();
try { 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	String[] fs_fields = new String[] {"focusid","focusname","isactive"};
	fs_writer.addHeader("focusid","FocusId");
	fs_writer.addHeader("focusname","FocusName");
	fs_writer.addHeader("isactive","Active");
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	ExportPageUtility.createExportSchema(fs_writer,fsGlobal,request,fs_fields);
	//#(50000) programmer code begin;
	//#(50000) programmer code end;
	if(fsP02Bean.size()>0) {
		fs_writer.execute(request,response,"p02",fsP02Bean.iteratorElements());
	} else {
		fs_writer.process(request,response,"p02","Records not found");
	}
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
}
//#(70000) programmer code begin;
//#(70000) programmer code end;
fs_writer.process(request,response,"p02","Records not found");
//#(90000) programmer code begin;
//#(90000) programmer code end;
%>
