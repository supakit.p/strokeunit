<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<%@page import="com.fs.bean.misc.KnSQL"%>
<%@page import="com.fs.bean.ExecuteBean"%>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.DriverManager" %>
<%@page import="java.util.Enumeration"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>

<jsp:useBean id="fsP02" scope="request" class="com.fs.bean.P02ABean"/>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
	<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-10">
<fs:label for="focusnames" tagid="focusnames_label" tagclass="control-label">ชื่อ Focus <t1 style="font-size:14px">(Focus Name)</t1></fs:label><div class="input-group">
<textarea maxlength="200" id="focusnames" name="focusname" rows="2" class="form-control input-md irequired alert-input"value="${fsP02.getString('FocusName')}"></textarea>
<span></span><div class="input-group-required"><label class="required">*</label></div></div><div id="focusnames_alert" role="alert" class="has-feedback has-error" style="display:none;">${fsLabel.getText('focusnames_alert','You can not leave this empty')}</div>
						</div>
					</div>
				</div>
		</div>
	</div>
	<div class="row row-height">
		<div id="Addmaster2" class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-2">
<div class="radio form-check"><label><input type="radio" value="1" id="isactive1" name="isactive" class="form-control input-md form-check-input" ${fsP02.getChecked('IsActive','1')} /><span></span><fs:label for="isactive1" tagid="isactive1_label" tagclass="control-label">Active</fs:label></label></div>
						</div>
						<div class="col-height col-md-3">
<div class="radio form-check"><label><input type="radio" value="0" id="isactive2" name="isactive" class="form-control input-md form-check-input" ${fsP02.getChecked('IsActive','0')} /><span></span><fs:label for="isactive2" tagid="isactive2_label" tagclass="control-label">Inactive</fs:label></label></div>
						</div>
					</div>
				</div>
		</div>
		</div>
		<table  class="partition-table"><tr><td class="partition-head-column">รายการ Goals (Goals List)</td><td class="partition-label-column"><a href="javascript:void(0);" onclick="showAddpopup()" class="pull-right part-linker"><i class="fa fa-plus" style="margin-left: 2px; margin-right: 2px;"></i></a></td></tr></table>
		<div class="row row-height">
		
		<div class="col-height col-md-12">
		
		<table id="datatable2" class="table table-bordered table-hover table-striped tablesorter">
	<thead>
		<tr>
			<th class="text-center" style="width: 20%;"><a href="javascript:void(0)" class="alink-sorter" ><fs:label tagid="start_date_headerlabel">ลำดับที่ <br /><t1 style="font-size:14px">(No.)</t1></fs:label></a></th>
			<th class="text-center" style="width: 50%;"><a href="javascript:void(0)" class="alink-sorter" ><fs:label tagid="start_time_headerlabel">ชื่อ Goals <br /><t1 style="font-size:14px">(Goals Name)</t1></fs:label></a></th>
			<th class="text-center" style="width: 30%;"><a href="javascript:void(0)" class="alink-sorter" ><fs:label tagid="to_date_headerlabel">สถานะการใช้งาน <br /><t1 style="font-size:14px">(Active)</t1></fs:label></a></th>
<%
			//#(12000) programmer code begin;
			//#(12000) programmer code end;
%>
<%
			//#(13000) programmer code begin;
			//#(13000) programmer code end;
%>
		</tr>
	</thead>
	<tbody id="datatablebody">							
					<%
					String focusid = (String)request.getParameter("focusid");
					com.fs.bean.ExecuteBean execute = new com.fs.bean.ExecuteBean();
					Object  FocusName = request.getAttribute("FocusName");
					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
					KnSQL sql = execute.getKnSQL();
					sql.clear();
					sql.append("SELECT ROW_NUMBER() OVER(ORDER BY goalsid ASC) AS Row,*,CASE WHEN IsActive = 'true' THEN 'Active'ELSE 'Inactive'END AS IsActiveText FROM Mw_FocusGoals  WHERE FocusId = '"+focusid+"' ORDER BY goalsid ;");
					fsGlobal.obtain(fsAccessor);
					fsGlobal.setFsAction(com.fs.bean.util.GlobalBean.COLLECT_MODE);
					execute.transport(fsGlobal);
					Enumeration ens = execute.elements();

%>
<%while((ens!=null) && (ens.hasMoreElements())) {
	ExecuteBean bean = (ExecuteBean) ens.nextElement();%>
			<tr>
							<td><a href="javascript:void(0)" class="alink-data" onclick="updateAddFocusgoals('<%=bean.getString("GoalsId")%>','<%=bean.getString("GoalsName")%>','<%=bean.getString("IsActive")%>');"><%=bean.getString("Row")%></a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="updateAddFocusgoals('<%=bean.getString("GoalsId")%>','<%=bean.getString("GoalsName")%>','<%=bean.getString("IsActive")%>');"><%=bean.getString("GoalsName")%></a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="updateAddFocusgoals('<%=bean.getString("GoalsId")%>','<%=bean.getString("GoalsName")%>','<%=bean.getString("IsActive")%>');"><%=bean.getString("IsActiveText")%></a></td>
						</tr>
						<%}%>
					<tr>
						
						
					</tr>
	</tbody>
</table>
		</div>
	
	</div>


<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
