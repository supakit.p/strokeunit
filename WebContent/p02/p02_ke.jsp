<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsP02" scope="request" class="com.fs.bean.P02ABean"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Master: Focus</title>
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createImportScripts('p02',pageContext.request,pageContext.response)}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/scripts.jsp"/>
<%
//#(10500) programmer code begin;
//#(10500) programmer code end;
%>
		<link rel="stylesheet" type="text/css" href="../css/program_style.css" />
		<link rel="stylesheet" type="text/css" href="p02.css?${fsScreen.currentTime()}" />
		<script  type="text/javascript" src="p02.js?${fsScreen.currentTime()}"></script>
		<script  type="text/javascript" src="p02_ke.js?${fsScreen.currentTime()}"></script>
		<script  type="text/javascript" src="p02_init.js?${fsScreen.currentTime()}"></script>
<%
//#(11000) programmer code begin;
//#(11000) programmer code end;
%>
	</head>
	<body class="portalbody portalbody-off">
		<c:out value="${fsScreen.createDialogLayer()}" escapeXml="false"></c:out>
		<c:out value="${fsScreen.createWaitLayer()}" escapeXml="false"></c:out>
		<div id="ptmainpager" class="pt-page pt-page-current pt-page-controller p02-entry-pager">
			<c:out value="${fsScreen.createHeaderLayer(pageContext.request,'p02',fsLabel.getText('caption','Master: Focus'))}" escapeXml="false"></c:out>
			<div id="entrypanel">
				<div id="entrylayer" class="entry-layer">
<%
//#(11500) programmer code begin;
//#(11500) programmer code end;
%>
					<form id="fsentryform" name="fsentryform" method="post">	
						<input type="hidden" name="fsAction" value="${fsP02.parseAction()}"/>
						<input type="hidden" name="fsAjax" value="true"/>
						<input type="hidden" name="fsDatatype" value="json"/>
						<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
						<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
						<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>	
						<input type="hidden" name="fsLanguage" value="${fsGlobal.fsLanguage}"/>	
						<div class="portal-area sub-entry-layer" style="padding-left:5px;">
<%
//#(12000) programmer code begin;
//#(12000) programmer code end;
%>
							<div class="entry-key-layer">
								<jsp:include page="p02_key.jsp"/>
<%
//#(13000) programmer code begin;
//#(13000) programmer code end;
%>
								<c:if test="${fsGlobal.insertMode()}">
								<div class="row">
									<div id="fs_controlbuttonheaderlayer" class="col-md-4 pull-right text-right">
										<input type="button" id="findbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('findbutton','Search')}"/>
									</div>
								</div>
								</c:if>
<%
//#(14000) programmer code begin;
//#(14000) programmer code end;
%>
							</div>
							<hr id="horizotalkeyline" class="entry-horizontal"/>
<%
//#(15000) programmer code begin;
//#(15000) programmer code end;
%>
							<jsp:include page="p02_kit.jsp"/>
<%
//#(16000) programmer code begin;
//#(16000) programmer code end;
%>
						</div>
					</form>
				</div>
			</div>
<%
//#(16500) programmer code begin;
//#(16500) programmer code end;
%>
		</div>
<%
//#(17000) programmer code begin;
//#(17000) programmer code end;
%>
		<nav id="buttonfooterbar" class="navbar navbar-expand fixed-bottom navbar-fixed-bottom">
			<div class="row collapse navbar-collapse">
				<div id="fs_controlbuttonfooterlayer" class="col-md-12 pull-right text-right" style="margin-right: 20px; margin-bottom:5px;">
<%
//#(18000) programmer code begin;
//#(18000) programmer code end;
%>
					<c:if test="${fsGlobal.insertMode()}">
						<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save')}"/>
					</c:if>
					<c:if test="${fsGlobal.updateMode()}">
						<input type="button" id="deletebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('deletebutton','Delete')}"/>
						<input type="button" id="updatebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatebutton','Update')}"/>
					</c:if>
					<input type="button" id="cancelbutton" class="btn btn-spot btn-sm" value="${fsLabel.getText('cancelbutton','Cancel')}"/>
<%
//#(19000) programmer code begin;
//#(19000) programmer code end;
%>
				</div>
			</div>
		</nav>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
	</body>
</html>
<%
//#(30000) programmer code begin;
//#(30000) programmer code end;
%>
