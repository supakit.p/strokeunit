<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<!DOCTYPE html>
<html>
	<head>
		<title>AddFocusMaster</title>		
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createImportScripts('p02',pageContext.request,pageContext.response)}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/scripts.jsp"/>
		<link rel="stylesheet" type="text/css" href="../css/program_style.css?${fsScreen.currentTime()}" />
		<link rel="stylesheet" type="text/css" href="p02.css?${fsScreen.currentTime()}" />
		<script  type="text/javascript" src="p02.js?${fsScreen.currentTime()}"></script>
<%
//#(11000) programmer code begin;
//#(11000) programmer code end;
%>
	</head>
	<body class="portalbody portalbody-off">
		<c:out value="${fsScreen.createDialogLayer()}" escapeXml="false"></c:out>
		<c:out value="${fsScreen.createWaitLayer()}" escapeXml="false"></c:out>
		<div id="ptsearchpager" class="pt-page pt-page-current pt-page-controller p02-search-pager">
			<c:out value="${fsScreen.createHeaderLayer(pageContext.request,'p02',fsLabel.getText('caption','AddFocusMaster'))}" escapeXml="false"></c:out>
			<div id="searchpanel" class="panel-body">
<%
//#(12000) programmer code begin;
//#(12000) programmer code end;
%>
				<br/>
				<div id="messagepanellayer" class="message-panel-class">
					${fsLabel.getText("trxnotfound","Transaction not found")}
				</div>
				<br/>
				<div id="gobacklayer" class="goback-class">
					<a href="javascript:window.history.back();">${fsLabel.getText("goback","Go Back")}</a>
				</div>
<%
//#(13000) programmer code begin;
//#(13000) programmer code end;
%>
			</div>
		</div>
	</body>
</html>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
