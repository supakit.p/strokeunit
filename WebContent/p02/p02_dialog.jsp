<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<%@page import="com.fs.bean.misc.KnSQL"%>
<%@page import="com.fs.bean.ExecuteBean"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@page import="java.util.Enumeration"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsP02" scope="request" class="com.fs.bean.P02ABean"/>

<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<div id="fsmodaldialog_layer" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xm">
		<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
			<div class="modal-header">
					<button type="button" class="close" onclick="closePopup()">&times;</button>
				<h4 class="modal-title" id="modalheadertitle">
					<c:if test="${!fsGlobal.updateMode()}">
						เพิ่มรายการใหม่ (Add)
						<input id="check" type="hidden"  value="new"/>
					</c:if>
					<c:if test="${fsGlobal.updateMode()}">
				<h4 class="modal-title" id="modalheadertitle">
					แก้ไขรายการ (Edit)</h4>					
					<input id="check" type="hidden" value="update"/>
					</c:if>
				</h4>

			</div>
			<div id="entrydialoglayer" class="entry-dialog-layer">
			<form id="fsentryform" role="form" data-toggle="validator" name="fsentryform" method="post">
				<c:if test="${!fsGlobal.updateMode()}">
					<input type="hidden" name="fsAction" value="enter"/>
				</c:if>
				<c:if test="${fsGlobal.updateMode()}">
					<input type="hidden" name="fsAction" value="update"/>
				</c:if>
				<input type="hidden" name="fsAjax" value="true"/>
				<input type="hidden" name="fsDatatype" value="json"/>
				<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
				<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
				<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
				<jsp:include page="p02_key.jsp"/>
<%
//#(30000) programmer code begin;
//#(30000) programmer code end;
%>
				<jsp:include page="p02_kit.jsp"/>
<%
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
			</form>	
			</div>
			
<%
//#(16000) programmer code begin;
//#(16000) programmer code end;
%>
<div style="text-align: center;" class="fschaptertablelayer">

			<div class="row-heighter modal-footer" >
<%
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>					
						<button type="button" id="updatebutton" class="btn btn-dark btn-sm" data-dismiss="modal"onclick="updateFocus()" style="display:none">Update</button>
						<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save!')}"/>
					
					<button type="button" id="canceldialogbutton" class="btn btn-spot btn-sm" data-dismiss="modal"onclick="closePopup()" >Cancel</button>
<%
//#(70000) programmer code begin;
//#(70000) programmer code end;
%>
			</div>
		</div>
	</div>
</div>
</div>


<div id="fsmodaldialog_layer2" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xm">

		<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
			<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modalheadertitle">
					เพิ่มรายการใหม่ (Add)
				</h4>

			</div>
			<div id="entrydialoglayer" class="entry-dialog-layer">
				<div class="row row-height">

		</div>
				<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-10">
							<fs:label  tagid="goalsname_label" tagclass="control-label">ชื่อ Goals <t1 style="font-size:14px">(Goals Name)</t1></fs:label>
							<div class="input-group">
							<textarea maxlength="201" id="goalsname" name="goalsnames" rows="2" class="form-control input-md irequired alert-input" value=""></textarea>
							<span></span><div class="input-group-required"><label class="required">*</label></div></div><div id="goalsnames_alert1" role="alert" class="has-feedback has-error" style="display:none;">${fsLabel.getText('focusnames_alert','You can not leave this empty')}</div>
						</div>
					</div>
				</div>
		</div>
		<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-2">
<div class="radio form-check"><label><input type="radio" value="1" id="isactives1" name="isactive" class="form-control input-md form-check-input" ${fsP02.getChecked('IsActive','1')} checked/><span></span><fs:label for="isactive1" tagid="isactive1_label" tagclass="control-label">Active</fs:label></label></div>
						</div>
						<div class="col-height col-md-3">
<div class="radio form-check"><label><input type="radio" value="0" id="isactives2" name="isactive" class="form-control input-md form-check-input" ${fsP02.getChecked('IsActive','0')} /><span></span><fs:label for="isactive2" tagid="isactive2_label" tagclass="control-label">Inactive</fs:label></label></div>
						</div>
					</div>
				</div>
	</div>
			</div>
			
<%
//#(16000) programmer code begin;
//#(16000) programmer code end;
%>
<div style="text-align: center;" class="fschaptertablelayer">

			<div class="row-heighter modal-footer" >
<%
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
					<c:if test="${!fsGlobal.updateMode()}">
						<button type="button" id="savebutton2" class="btn btn-dark btn-sm" onclick="saveAddFocusgoals()"value=>Save</button>
					</c:if>
					<c:if test="${fsGlobal.updateMode()}">
						<input type="button" id="updatebutton2" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatebutton','Update')}"/>
					</c:if>
					<input type="button" id="canceldialogbutton2" class="btn btn-spot btn-sm" data-dismiss="modal" value="${fsLabel.getText('canceldialogbutton','Cancel')}"/>
<%
//#(70000) programmer code begin;
//#(70000) programmer code end;
%>
			</div>
		</div>
	</div>
</div>
</div>

<div id="fsmodaldialog_layer3" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xm">

		<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
			<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modalheadertitle">
					แก้ไขรายการ (Edit)
				</h4>

			</div>
			<div id="entrydialoglayer" class="entry-dialog-layer">
				<div class="row row-height">
			<div class="col-md-12 col-height">
					<div class="table-layer-class">
						<div class="row row-height">
							<div class="col-height col-md-3">
						<fs:label tagid="focusids_label" tagclass="control-label">รหัส Goals (Goals ID)</fs:label><div class="input-group"><input type="text" id="goalsidsUD" name="goalsidUD" class="form-control input-md irequired ikeyclass alert-input" value="" readonly /><span></span><div class="input-group-required"><label class="required">*</label></div></div><div id="focusids_alert" role="alert" class="has-feedback has-error" style="display:none;">${fsLabel.getText('focusids_alert','You can not leave this empty')}</div>
							</div>
						</div>
					</div>
			</div>
		</div>
				<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-10">
							<fs:label  tagid="goalsname_label" tagclass="control-label">ชื่อ Goals <t1 style="font-size:14px">(Goals Name)</t1></fs:label><div class="input-group">
							 <textarea maxlength="201" id="goalsnameUD" name="goalsnamesUD" rows="2" class="form-control input-md irequired alert-input" value=""></textarea>
							<div class="input-group-required">
							<label class="required">*</label></div></div><div id="goalsnames_alert2" role="alert" class="has-feedback has-error" style="display:none;">${fsLabel.getText('focusnames_alert','You can not leave this empty')}</div>
						</div>
					</div>
				</div>
		</div>
		<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-2">
<div class="radio form-check"><label><input type="radio" value="1" id="isactives1UD" name="isactiveUD" class="form-control input-md form-check-input" ${fsP02.getChecked('IsActive','1')} /><span></span><fs:label for="isactive1" tagid="isactive1_label" tagclass="control-label">Active</fs:label></label></div>
						</div>
						<div class="col-height col-md-3">
<div class="radio form-check"><label><input type="radio" value="0" id="isactives2UD" name="isactiveUD" class="form-control input-md form-check-input" ${fsP02.getChecked('IsActive','0')} /><span></span><fs:label for="isactive2" tagid="isactive2_label" tagclass="control-label">Inactive</fs:label></label></div>
						</div>
					</div>
				</div>
	</div>
			</div>
			
<%
//#(16000) programmer code begin;
//#(16000) programmer code end;
%>
<div style="text-align: center;" class="fschaptertablelayer">

			<div class="row-heighter modal-footer" >
<%
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
					<c:if test="${!fsGlobal.updateMode()}">
						<button type="button" id="savebutton3" class="btn btn-dark btn-sm" onclick="updateFocusgoals()"value=>Update</button>
					</c:if>
					<c:if test="${fsGlobal.updateMode()}">
						<input type="button" id="updatebutton3" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatebutton','Update')}"/>
					</c:if>
					<input type="button" id="canceldialogbutton3" class="btn btn-spot btn-sm" data-dismiss="modal" value="${fsLabel.getText('canceldialogbutton','Cancel')}"/>
<%
//#(70000) programmer code begin;
//#(70000) programmer code end;
%>
			</div>
		</div>
	</div>
</div>
</div>
<%
//#(90000) programmer code begin;
//#(90000) programmer code end;
%>
