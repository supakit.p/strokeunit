<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsP02Bean" scope="session" class="com.fs.bean.P02Bean"/>
<% String fs_express_readonly = fsScreen.getEquals("retrieve",fsGlobal.getFsAction()," readonly ",""); %>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
			<div class="row row-height">
				<div class="col-md-12 col-height">
						<div class="table-layer-class">
							<div class="row row-height">
								<div class="col-height col-md-3">
<fs:label for="focusid" tagid="focusid_label" tagclass="control-label">รหัส Focus <t1 style="font-size:14px">(Focus ID)</t1></fs:label><input type="text" id="focusid" name="focusid" class="form-control input-md" value="${fsP02Bean.getString('FocusId')}" /><span></span>
								</div>
								<div class="col-height col-md-3">
<fs:label for="focusname" tagid="focusname_label" tagclass="control-label">ชื่อ Focus <t1 style="font-size:14px">(Focus Name)</t1></fs:label><input type="text" id="focusname" name="focusname" class="form-control input-md" value="${fsP02Bean.getString('FocusName')}" /><span></span>
								</div>
							</div>
						</div>
				</div>
			</div>

<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
					<div class="row">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 10px;">
<%
//#(30000) programmer code begin;
//#(30000) programmer code end;
%>
<%
//#(35000) programmer code begin;
//#(35000) programmer code end;
%>
							<div class="pull-right">
								<button type="button" id="insertbutton" class="btn btn-dark btn-sm" style=""><i class="fa fa-plus" aria-hidden="true" style="margin-right:10px;"></i>Add</button>
							</div>
							<div class="pull-right" style="margin-left: 20px;"></div>
							<div class="pull-right">
								<button type="button" id="searchbutton" class="btn btn-dark btn-sm" style=""><i class="fa fa-search" aria-hidden="true" style="margin-right:10px;"></i>${fsLabel.getText('searchbutton','Search')}</button>
							</div>
<%
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
						</div>
					</div>
<%
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
