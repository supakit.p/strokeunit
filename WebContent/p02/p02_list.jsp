<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.dba.*"%>
<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<%if(request.getParameter("clear")!=null) {
	session.removeAttribute("fsP02Bean"); 
}%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsP02Bean" scope="session" class="com.fs.bean.P02Bean"/>
<jsp:setProperty name="fsP02Bean" property="*"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("p02");
fsGlobal.setFsSection("PROMPT");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
fsGlobal.obtain(fsPager);
String fs_forwarder = "/p02/p02_m.jsp";
//#(20000) programmer code begin;
//#(20000) programmer code end;
fsGlobal.obtainPage(request,"fsGlobalp02");
fsP02Bean.forceObtain(fsGlobal);
//#(30000) programmer code begin;
//#(30000) programmer code end;
fsPager.setRows(fsP02Bean);
fsGlobal.adjustPage(fsPager);
fsP02Bean.obtain(session,request);
//#(40000) programmer code begin;
//#(40000) programmer code end;
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
