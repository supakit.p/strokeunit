<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsP02" scope="request" class="com.fs.bean.P02ABean"/>
<% String fs_express_readonly = fsScreen.getEquals("retrieve",fsGlobal.getFsAction()," readonly ",""); %>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
		<div class="row row-height">
			<div class="col-md-12 col-height">
					<div class="table-layer-class">
						<div class="row row-height">
							<div class="col-height col-md-3">
<fs:label for="focusids" tagid="focusids_label" tagclass="control-label">รหัส Focus <t1 style="font-size:14px">(Focus ID)</t1></fs:label><div class="input-group" ><input type="text" id="focusids" name="focusid" class="form-control input-md irequired ikeyclass alert-input" readonly value="${fsP02.getString('FocusId')}  "  /><span></span><div class="input-group-required"><label class="required">*</label></div></div><div id="focusids_alert" role="alert" class="has-feedback has-error" style="display:none;">${fsLabel.getText('focusids_alert','You can not leave this empty')}</div>
							</div>
						</div>
					</div>
			</div>
		</div>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
