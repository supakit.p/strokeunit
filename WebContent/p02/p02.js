var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
var is_using_dialog = true;
//#(10000) programmer code begin;
//#(10000) programmer code end;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("p02"); }catch(ex) { }
	initialApplication();
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
});
function initialApplication() {
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	setupComponents();
	setupAlertComponents();
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
}
function setupComponents() {
	//#(50000) programmer code begin;
	//#(50000) programmer code end;
	$("#searchbutton").click(function(evt) {
		try { fssearchform.fsPage.value = "1"; }catch(ex) { }
		search();  return false;
	});
	$("#insertbutton").click(function(evt) {
		insert(); 
		return false;
	});
	$("#savebutton").click(function() {
		save();  return false;
	});
	$("#cancelbutton").click(function() {
		cancel();  return false;
	});
	$("#updatebutton").click(function() {
		update();  return false;
	});
	$("#deletebutton").click(function() {
		deleted();  return false;
	});
	$("#findbutton").click(function() {
		find();  return false;
	});
	//#(60000) programmer code begin;
	//#(60000) programmer code end;
}
function search(aform) {
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	if(!aform) aform = fssearchform;
	startWaiting();
	jQuery.ajax({
		url: "p02_c.jsp",
		type: "POST",
		data: $(aform).serialize(),
		dataType: "html",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) {
			submitFailure(transport,status,errorThrown);
		},
		success: function(data,status,transport){
			searchComplete(transport,data);
		}
	});	
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
}
function searchComplete(xhr,data) {
	//#(90000) programmer code begin;
	//#(90000) programmer code end;
	stopWaiting();
	$("#listpanel").html(data);
	//#(100000) programmer code begin;
	//#(100000) programmer code end;
}
function insert() {
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	var aform = fslistform;
	aform.fsRowid.value = "";
	aform.fsAjax.value = "false";
	aform.fsDatatype.value = "text";
	aform.focusid.value = "";
	aform.action = "p02_insert.jsp";
	if(is_using_dialog) {
		aform.fsAjax.value = "true";
		startWaiting();
		var xhr = jQuery.ajax({
			url: "p02_insert_dialog.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) {
				submitFailure(transport,status,errorThrown);
			},
			success: function(data,status,transport){
				stopWaiting();
				$("#dialogpanel").html(data);
				setupDialogComponents();
				$("#fsmodaldialog_layer").modal("show");
				getfocusid();
				$(".partition-table").hide();
				$("#datatable2").hide();
			}
		});
		return;
	}
	aform.submit();
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
}
function clearingFields() {
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	fsentryform.reset();
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
}
function cancel() {
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	confirmCancel(function() {
		window.open("p02_list.jsp?seed="+Math.random(),"_self");
	});
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
}
function validSaveForm(callback) {
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	return true;
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
}
function save(aform) {
	//#(190000) programmer code begin;
	//#(190000) programmer code end;
	if(!aform) aform = fsentryform;
	if(!validNumericFields(aform)) return false;
	validSaveForm(function() {
		//#(195000) programmer code begin;
		//#(195000) programmer code end;
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "p02_insert_c.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) {
					submitFailure(transport,status,errorThrown);
				},
				success: function(data,status,transport){
					stopWaiting();
					//#(195300) programmer code begin;
					//#(195300) programmer code end;
					successbox(function() { 
						aform.reset();
						setTimeout(function() { $("#FocusId").focus(); },200);
						closePopup2();
					});
					//#(195500) programmer code begin;
					//#(195500) programmer code end;
				}
			});
		});
	});
	return false;
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
}
function validSendForm(callback) {
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
	//#(220000) programmer code begin;
	//#(220000) programmer code end;
}
function update(aform) {
	//#(230000) programmer code begin;
	//#(230000) programmer code end;
	if(!aform) aform = fsentryform;
	if(!validNumericFields(aform)) return false;
	validSaveForm(function() {
		//#(235000) programmer code begin;
		//#(235000) programmer code end;
		confirmUpdate(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "p02_update_c.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) {
					submitFailure(transport,status,errorThrown);
				},
				success: function(data,status,transport){ 
					stopWaiting();
					//#(235300) programmer code begin;
					//#(235300) programmer code end;
					successbox(function() { 
						if(is_using_dialog) {
							$("#fsmodaldialog_layer").modal("hide");
							search();
							return;
						}
						window.open("p02_search.jsp?seed="+Math.random(),"_self");
					});
					//#(235500) programmer code begin;
					//#(235500) programmer code end;
				}
			});
		});
	});
	return false;
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
}
function updateAddFocusgoals(goalsId,goalsName,isactives) {
	$("#fsmodaldialog_layer3").modal("show");
	   $("#goalsidsUD").val(goalsId);
	   $("#goalsnameUD").val(goalsName);
	   if(isactives == 'true'){
		   console.log('aaaa'+isactives);

			$("#isactives1UD").prop("checked", true);
		}else{
			$("#isactives2UD").prop("checked", true);
		}
	};
function closePopup(){
	$("#fsmodaldialog_layer").modal("hide");
}

function closePopup2(){
	$("#fsmodaldialog_layer").modal("hide");
	document.getElementById("searchbutton").click();
	location.reload();
}

function closePopup3(){
	
	$("#fsmodaldialog_layer3").modal("hide");

	$("#fsmodaldialog_layer2").modal("hide");
	$("#fsmodaldialog_layer").modal("hide");
}

function saveAddFocusgoals() {
	
	if($("#goalsname").val() == ''){
		$("#goalsnames_alert1").modal("show");
	}else{
		 $.ajax({
	       url: "p02_s3.jsp",
	       method: "GET",
	       dataType: "json",
	       success: function (response) {
	    	   $.each(JSON.parse(response.list), function(key,value){
	        		var goalsId = value.orders;
	        		saveAddFocusgoals2(goalsId);
	            });
	       },
	       error: function (jqXHR, textStatus, errorThrown) {
	       },
	       cache: false
	   });
	}
	
	 
  
};

function updateFocusgoals() {
	if($("#goalsnameUD").val() == ''){
		$("#goalsnames_alert2").modal("show");
	}else{
	var goalsId = $("#goalsidsUD").val();	
	 var goalsName = $("#goalsnameUD").val();
	   var focusId = $("#focusids").val();
	   var isactives = 0;
	   if (document.getElementById('isactives1UD').checked) {
		  isactives = document.getElementById('isactives1UD').value;
		 }
	   if (document.getElementById('isactives2UD').checked) {
		   isactives = document.getElementById('isactives2UD').value;
		 }
	   
	   let data = {
			   goalsId: goalsId,
			   goalsName: goalsName,
			   focusId: focusId,
			   isactives: isactives
	   };

	   $.ajax({
	       url: "p02_s4.jsp",
	       method: "GET",
	       data: data,
	       dataType: "json",
	       success: function (response) {
	    	   closePopup3();
	       },
	       error: function (jqXHR, textStatus, errorThrown) {
	       },
	       cache: false
	   });
	}
}

function updateFocus() {
	 var focusName = $("#focusnames").val();
	   var focusId = $("#focusids").val();
	   var isactives = 0;
	   if (document.getElementById('isactive1').checked) {
		  isactives = document.getElementById('isactive1').value;
		 }
	   if (document.getElementById('isactive2').checked) {
		   isactives = document.getElementById('isactive2').value;
		 }
	   
	   let data = {
			   focusName: focusName,
			   focusId: focusId,
			   isactives: isactives
	   };

	   $.ajax({
	       url: "p02_s5.jsp",
	       method: "GET",
	       data: data,
	       dataType: "json",
	       success: function (response) {
	    	   closePopup2();
	       },
	       error: function (jqXHR, textStatus, errorThrown) {
	       },
	       cache: false
	   });
	
}

function getfocusid() {

	   $.ajax({
	       url: "p02_s6.jsp",
	       method: "GET",
	       dataType: "json",
	       success: function (response) {
	    	   $.each(JSON.parse(response.list), function(key,value){
	        		var goalsId = value.orders;
	        		$("#focusids").val(goalsId);
	            });
	       },
	       error: function (jqXHR, textStatus, errorThrown) {
	       },
	       cache: false
	   });
	
}

function updateFocus() {
	 var focusName = $("#focusnames").val();
	   var focusId = $("#focusids").val();
	   var isactives = 0;
	   if (document.getElementById('isactive1').checked) {
		  isactives = document.getElementById('isactive1').value;
		 }
	   if (document.getElementById('isactive2').checked) {
		   isactives = document.getElementById('isactive2').value;
		 }
	   
	   let data = {
			   focusName: focusName,
			   focusId: focusId,
			   isactives: isactives
	   };

	   $.ajax({
	       url: "p02_s5.jsp",
	       method: "GET",
	       data: data,
	       dataType: "json",
	       success: function (response) {
	    	   closePopup2();
	       },
	       error: function (jqXHR, textStatus, errorThrown) {
	       },
	       cache: false
	   });
	
}

function saveAddFocusgoals2(goalsIds) {
	var goalsId = goalsIds;
	 var goalsName = $("#goalsname").val();
	   var focusId = $("#focusids").val();
	   var isactives = 0;
	   if (document.getElementById('isactives1').checked) {
		  isactives = document.getElementById('isactives1').value;
		 }
	   if (document.getElementById('isactives2').checked) {
		   isactives = document.getElementById('isactives2').value;
		 }
	   
	   let data = {
			   goalsId: goalsId,
			   goalsName: goalsName,
			   focusId: focusId,
			   isactives: isactives
	   };

	   $.ajax({
	       url: "p02_s2.jsp",
	       method: "GET",
	       data: data,
	       dataType: "json",
	       success: function (response) {
	    	   closePopup3();
	       },
	       error: function (jqXHR, textStatus, errorThrown) {
	       },
	       cache: false
	   });
	
}
function submitRetrieve(rowIndex,FocusId) {
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	var aform = fslistform;
	aform.fsRowid.value = ""+rowIndex;
	aform.fsAjax.value = "false";
	aform.fsDatatype.value = "text";
	aform.focusid.value = FocusId;
	aform.action = "p02_retrieve_c.jsp";
	if(is_using_dialog) {
		aform.fsAjax.value = "true";
		startWaiting();
		var xhr = jQuery.ajax({
			url: "p02_retrieve_dialog_c.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) {
				submitFailure(transport,status,errorThrown);
			},
			success: function(data,status,transport){
				stopWaiting();
				$("#dialogpanel").html(data);
				setupDialogComponents();
				$("#fsmodaldialog_layer").modal("show");
				getsection(FocusId);
				if($("#check").val() == 'update'){
					$("#updatebutton").show()
					$("#savebutton").hide();

				}
			}
		});
		return;
	}
	aform.submit();
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
function showAddpopup() {
	$("#fsmodaldialog_layer2").modal("show");

};
function getsection(val) {

    let FocusId = val;
    let data = {
    	FocusId: FocusId,
    };

    $.ajax({
        url: "p02_s.jsp",
        method: "GET",
        data: data,
        dataType: "json",
        success: function (response) {
        	$.each(JSON.parse(response.list), function(key,value){
        		$("#focusnames").val(value.FocusName);
        		console.log('1111'+value.FocusName+value.IsActive);
        		if(value.IsActive == 'true'){
        			$("#isactive1").prop("checked", true);
        		}else{
        			$("#isactive2").prop("checked", true);
        		}
            });
        	
        },
        error: function (jqXHR, textStatus, errorThrown) {
        },
        cache: false
    });
};
function submitChapter(aform,index) {
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	startWaiting();
	var xhr = jQuery.ajax({
		url: "p02_page.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: $(aform).serialize(),
		dataType: "html",
		error : function(transport,status,errorThrown) {
			submitFailure(transport,status,errorThrown);
		},
		success: function(data,status,transport){
			stopWaiting();
			$("#listpanel").html(data);
		}
	});
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
function submitOrder(fsParams) {
	//#(290000) programmer code begin;
	//#(290000) programmer code end;
	startWaiting();
	var xhr = jQuery.ajax({
		url: "p02_cd.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: fsParams,
		dataType: "html",
		error : function(transport,status,errorThrown) {
			submitFailure(transport,status,errorThrown);
		},
		success: function(data,status,transport){
			stopWaiting();
			$("#listpanel").html(data);
		}
	});
	return false;
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
function submitDelete(fsParams,rowIndex) {
	//#(310000) programmer code begin;
	//#(310000) programmer code end;
	confirmDelete([fsParams[0]],function() {
		deleteRecord(fsParams);
	});
	//#(320000) programmer code begin;
	//#(320000) programmer code end;
}
function deleteRecord(fsParams) {
	//#(330000) programmer code begin;
	//#(330000) programmer code end;
	startWaiting();
	jQuery.ajax({
		url: "p02_delete_c.jsp",
		type: "POST",
		data: {
			fsAction : "delete",
			fsDatatype : "json",
			FocusId : fsParams[0]
		},
		dataType: "html",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) {
			submitFailure(transport,status,errorThrown);
		},
		success: function(data,status,transport){
			stopWaiting();
			//#(333000) programmer code begin;
			//#(333000) programmer code end;
			fssearchform.fsPage.value = fslistform.fsPage.value;
			search();
			//#(335000) programmer code begin;
			//#(335000) programmer code end;
		}
	});
	//#(340000) programmer code begin;
	//#(340000) programmer code end;
}
function deleted(aform) {
	//#(345000) programmer code begin;
	//#(345000) programmer code end;
	if(!aform) aform = fsentryform;
	confirmDelete([],function() {
		//#(347000) programmer code begin;
		//#(347000) programmer code end;
		startWaiting();
		var xhr = jQuery.ajax({
			url: "p02_delete_c.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) {
				submitFailure(transport,status,errorThrown);
			},
			success: function(data,status,transport){ 
				stopWaiting();
				//#(347500) programmer code begin;
				//#(347500) programmer code end;
				if(is_using_dialog) {
					$("#fsmodaldialog_layer").modal("hide");
					search();
					return;
				}
				//#(347600) programmer code begin;
				//#(347600) programmer code end;
				successbox(function() { window.open("p02_search.jsp?seed="+Math.random(),"_self"); });
				//#(347700) programmer code begin;
				//#(347700) programmer code end;
			}
		});
	});
	return false;
	//#(348000) programmer code begin;
	//#(348000) programmer code end;
}
function find(aform) {
	if(!aform) aform = fsentryform;
	//#(350000) programmer code begin;
	//#(350000) programmer code end;
	var paras = [
		{name:"FocusId", value: $("#focusids").val()},
		{name:"clear", value: "true" }
	];
	//#(360000) programmer code begin;
	//#(360000) programmer code end;
	submitWindow({ params: paras, url: "p02_search.jsp", windowName: "_self" });
	//#(370000) programmer code begin;
	//#(370000) programmer code end;
}
function setupDialogComponents() {
	//#(380000) programmer code begin;
	//#(380000) programmer code end;
	$("#savebutton").click(function() {
		save(); return false;
	});
	$("#updatebutton").click(function() {
		update(); return false;
	});
	$("#deletebutton").click(function() {
		deleted(); return false;
	});
	setupAlertComponents($("#dialogpanel"));
	initialApplicationControls($("#dialogpanel"));
	$("#dialogpanel").find(".modal-dialog").draggable();
	try { initLookupControls(); }catch(ex) { }
	//#(385000) programmer code begin;
	//#(385000) programmer code end;
}
function exportExcelHandler() {
	var fs_params = null;
	//#(386000) programmer code begin;
	//#(386000) programmer code end;
	openNewWindow({ url: "p02_export_excel.jsp", windowName: "p02_excel_window", params:  fs_params });
	//#(387000) programmer code begin;
	//#(387000) programmer code end;
	return false;
}
function exportPdfHandler() {
	var fs_params = null;
	//#(388000) programmer code begin;
	//#(388000) programmer code end;
	openNewWindow({ url: "p02_export_pdf.jsp", windowName: "p02_pdf_window", params:  fs_params });
	//#(389000) programmer code begin;
	//#(389000) programmer code end;
	return false;
}
//#(390000) programmer code begin;
//#(390000) programmer code end;

