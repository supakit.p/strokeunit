<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.dev.servlet.*" %>
<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<jsp:useBean id="fsPermission" scope="page" class="com.fs.bean.util.PermissionBean"/>
<jsp:setProperty name="fsPermission" property="*"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
String fs_target = request.getParameter("target");
TheLauncher.launchApplication(session,"p02",fs_target!=null,request);
fsPermission.setProgram("p02");
fsAccessor.addPermission(fsPermission);
String fs_forwarder = "/p02/p02.jsp";
//#(20000) programmer code begin;
//#(20000) programmer code end;
//#(30000) programmer code begin;
//#(30000) programmer code end;
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
