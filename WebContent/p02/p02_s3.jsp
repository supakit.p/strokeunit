<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<%@page import="java.util.Enumeration"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.fs.bean.misc.KnSQL"%>
<%@page import="com.fs.bean.ExecuteBean"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="org.json.simple.*" %>
<%@ page import="com.google.gson.*" %>


<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>

<jsp:useBean id="P02Bean" scope="session" class="com.fs.bean.P02Bean"/>
<%
try { 
	 final JSONArray body = new JSONArray();
	 com.fs.bean.ExecuteBean execute = new com.fs.bean.ExecuteBean();

		KnSQL sql = execute.getKnSQL();
		sql.clear();

		sql.append("SELECT COUNT(GoalsId) AS orders  FROM Mw_FocusGoals ");
		fsGlobal.obtain(fsAccessor);
		fsGlobal.setFsAction(com.fs.bean.util.GlobalBean.COLLECT_MODE);
		execute.transport(fsGlobal);

		Enumeration ens = execute.elements();
			while((ens!=null) && (ens.hasMoreElements())) {
					ExecuteBean bean = (ExecuteBean) ens.nextElement();
					JSONObject json = new JSONObject();
					json.put("orders",bean.getString("orders"));
					body.add(json);
					}
				  Gson gson = new Gson();
				  JsonObject root = new JsonObject();
				  root.addProperty("list", gson.toJson(body)); //add list

				  out.println(gson.toJson(root));
				  out.flush();
				return ;
}catch (Exception ex) {
    out.println(ex);
}
%>
