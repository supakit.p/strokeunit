<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE005CBean" scope="request" class="com.fs.bean.SFTE005CBean"/>
<jsp:setProperty name="fsSFTE005CBean" property="*"/>
<%
	JSONObject result = new JSONObject();
	try { 
		fsGlobal.setFsSection("AUTH");
		fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
		int fs_action = fsGlobal.parseAction();
		fsGlobal.obtain(fsAccessor);
		fsSFTE005CBean.forceObtain(fsGlobal);
		fsSFTE005CBean.obtainFrom(request);
		String fs_language = PageUtility.getDefaultLanguage(request);
		java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
		if(fs_map!=null) {
			fsSFTE005CBean.obtain(fs_map);
			if(fs_map.get("language")!=null) fs_language = (String)fs_map.get("language");
		}
		fsGlobal.setFsLanguage(fs_language);
		fsSFTE005CBean.obtain(session,request);
		if(fsSFTE005CBean.getUserid()==null || fsSFTE005CBean.getUserid().trim().length()<=0) {
			fsSFTE005CBean.setUserid(fsAccessor.getFsUser());
		}
		if(fsSFTE005CBean.getUserid()==null || fsSFTE005CBean.getUserid().trim().length()<=0) {
			throw new BeanException("User is undefined",-8891,fs_language);
		}
		if(fsSFTE005CBean.getUserpassword().equals(fsSFTE005CBean.getUserid())) {
			throw new BeanException("Not allow password as same as user",-8899,fs_language);
		}
		if(fsSFTE005CBean.getUserpassword().length()>8) {
			throw new BeanException("Password length not over 8 characters",-8896,fs_language);
		}
		fsSFTE005CBean.setCheckflag("0");	
		fsSFTE005CBean.transport(fsGlobal);
		fsAccessor.setFsVar("fsChange","0");
		fsAccessor.setFsVar("fsPasswordexpire",fsSFTE005CBean.getPasswordexpiredate());
	}catch(Exception ex) { 
		Trace.error(fsAccessor,ex);
		fsGlobal.setThrowable(ex);
		fsGlobal.createResponseStatus(out, response);
		return;
	}
	result.put("status","200");
	result.put("text","Password changed");
	out.println(result.toJSONString());
%>
