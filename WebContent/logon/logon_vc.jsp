<?xml version="1.0" encoding="UTF-8" ?>
<%@ page errorPage="/jsp/xmlerrorpage.jsp"%>
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.auth.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsLogonBean" scope="request" class="com.fs.dev.auth.SignonBean"/>
<jsp:setProperty name="fsLogonBean" property="*"/>
<%
	Trace.info("signin "+request.getContextPath()+" : "+request.getQueryString());
	Trace.info("signin "+request.getContextPath()+" : "+fsLogonBean);
	session.setAttribute("fsLogonBean.user",fsLogonBean.getUsername());
	session.setAttribute("fsLogonBean.password",fsLogonBean.getPassword());
	fsGlobal.setFsVar("fsAddress",request.getRemoteAddr());
	fsGlobal.setFsSection("AUTH");
	String fs_language = PageUtility.getDefaultLanguage(request);
	StringBuilder result = new StringBuilder();
	if(fsLogonBean.getUsername().trim().equals("")) {
		String fs_msg = "User or Password is undefined";
		fs_msg = ErrorConfig.getError("5001",fs_msg,fs_language);
		fsGlobal.setFsMessage(fs_msg);
		result.setLength(0);
		result.append("<root type=\"error\">");
		result.append("<body>").append(fs_msg).append("</body>");
		result.append("</root>");
		out.println(result.toString());
		return;
	}
	String signon = (String)GlobalVariable.getVariable("VALIDATE_SIGNON");
	if(!(signon!=null && signon.equalsIgnoreCase("false"))) {
		int maxfailure = 3;
		String maxfail = (String)GlobalVariable.getVariable("MAXFAIL_SIGNON");
		if((maxfail!=null) && !maxfail.trim().equals("")) {
			try {
				maxfailure = Integer.parseInt(maxfail);
			} catch(Exception ex) { maxfailure = 3; }
		}
		Integer counter = (Integer)session.getAttribute(fsLogonBean.getUsername()+"_signoncounter");
		if(counter==null) counter = new Integer(0);
		if(counter.intValue()>=maxfailure) {
			String fs_msg = "Signon failure over "+maxfailure+" times.";
			fs_msg = ErrorConfig.getError("5003",fs_msg,fs_language,new String[] { ""+maxfailure });
			fsGlobal.setFsMessage(fs_msg);
			result.setLength(0);
			result.append("<root type=\"error\">");
			result.append("<body>").append(fs_msg).append("</body>");
			result.append("</root>");
			out.println(result.toString());
			return;		
		}
		try {
			boolean passed = false;
			String fs_msg = "User or Password is incorrect";
			fs_msg = ErrorConfig.getError("5001",fs_msg,fs_language);
			String fs_password = fsLogonBean.getPassword();
			fsGlobal.setFsVar("fsUser",fsLogonBean.getUsername());
			fsGlobal.setFsVar("fsBranch",fsLogonBean.getBranch());
			fsGlobal.setFsProg("signin");
			fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
			BeanTransport fsTransport = TheTransportor.transport(fsGlobal,fsLogonBean);
			if(fsTransport!=null) fsGlobal.setSuccess(fsTransport.isSuccess());
			boolean found = false;
			if(fsLogonBean.effectedTransactions()>0) {
				found = true;
				passed = true;
				if(passed) {
					BeanUtility butil = new BeanUtility();
					if(fsLogonBean.getActiveflag()!=null && !fsLogonBean.getActiveflag().equals("1")) {
						passed = false;
						fs_msg = "You are not active, please contact administrator";
						fs_msg = ErrorConfig.getError("5004",fs_msg,fs_language);
						if(fsLogonBean.getStatus().equals("P")) { //Pending
							fs_msg = ErrorConfig.getError("5007",fs_msg,fs_language);
						} else if(fsLogonBean.getStatus().equals("C")) { //Closed
							fs_msg = ErrorConfig.getError("5008",fs_msg,fs_language);
						}
					}
					if(passed && (fsLogonBean.getLockflag()!=null && fsLogonBean.getLockflag().equals("1"))) {
						passed = false;
						fs_msg = "You are locking, please contact administrator";
						fs_msg = ErrorConfig.getError("5005",fs_msg,fs_language);
						if(fsLogonBean.getFailtime()!=null && fsLogonBean.getFailtime().trim().length()>0) {
							long fs_failtime = butil.parseBigDecimal(fsLogonBean.getFailtime()).longValue();
							long fs_curtime = System.currentTimeMillis();
							long fs_difftime = fs_curtime - fs_failtime;
							if(fs_difftime<=180000) {
								fs_msg = "Your User ID has been locked. Please contact administrator or wait and retry again after 3 minute.";
								fs_msg = ErrorConfig.getError("5009",fs_msg,fs_language);
							} else {
								passed = true;
								com.fs.dev.auth.LockBean fsLockBean = new com.fs.dev.auth.LockBean();
								fsLockBean.setUser(fsLogonBean.getUsername());
								fsLockBean.setLock("0");
								GlobalBean fsHandler = (GlobalBean)fsGlobal;
								fsHandler.setFsAction(GlobalBean.UPDATE_MODE);
								TheTransportor.transport(fsHandler,fsLockBean);
							}
						}
					}
					if(passed && (fsLogonBean.getEffectdate()!=null && fsLogonBean.getEffectdate().trim().length()>0)) {
						java.util.Date effectdate = butil.parseDate(fsLogonBean.getEffectdate());
						java.util.Date currentdate = new java.util.Date();
						int reply = butil.compareDate(currentdate,effectdate);
						if(reply<0) {
							passed = false;
							fs_msg = "You are not active, out of effective date.";
							fs_msg = ErrorConfig.getError("5010",fs_msg,fs_language);						
						}
					}
				}
			}
			if(!passed) {
				if((counter.intValue()+1)>=maxfailure) {
					com.fs.dev.auth.LockBean fsLockBean = new com.fs.dev.auth.LockBean();
					fsLockBean.setUser(fsLogonBean.getUsername());
					fsLockBean.setLock("1");
					GlobalBean fsHandler = (GlobalBean)fsGlobal;
					fsHandler.setFsAction(GlobalBean.UPDATE_MODE);
					TheTransportor.transport(fsHandler,fsLockBean);
				}
				session.setAttribute(fsLogonBean.getUsername()+"_signoncounter",new Integer(counter.intValue()+1));	
				fsLogonBean.setPassword(fs_password);
				fsGlobal.setFsMessage(fs_msg);
				Tracker.track(fsGlobal);
				result.setLength(0);
				result.append("<root type=\"error\">");
				result.append("<body>").append(fs_msg).append("</body>");
				result.append("</root>");
				out.println(result.toString());
				return;
			}
			session.removeAttribute(fsLogonBean.getUsername()+"_signoncounter"); 
		} catch(Exception ex) {
			fsGlobal.setThrowable(ex);		
			result.setLength(0);
			result.append("<root type=\"error\">");
			result.append("<body>").append(fsGlobal.getFsMessage()).append("</body>");
			result.append("</root>");
			out.println(result.toString());
			return;
		}
	}
	if((fsLogonBean.getLogondate()==null) || fsLogonBean.getLogondate().equals("")) {
		BeanFormat formater = new BeanFormat();
		java.util.Date date = new java.util.Date();
		fsLogonBean.setLogondate(formater.formatDate(date,"dd/MM/yyyy"));
	}
	fsAccessor.setFsKey(session.getId());
	fsLogonBean.assign(fsAccessor);
	Trace.debug(fsLogonBean);
	Trace.debug(fsAccessor);
	fsGlobal.obtain(fsAccessor);
	fsGlobal.retain(request);
	Tracker.trace(fsGlobal);
	final GlobalBean fsAccessHandler = (GlobalBean)fsGlobal;
	final com.fs.dev.auth.AccessBean access = new com.fs.dev.auth.AccessBean();
	access.setAccessor(fsLogonBean.getUser());
	new Thread() {
		public void run() {
			try { access.update(fsAccessHandler); }catch(Exception ex) { }
			this.interrupt();
		}
	}.start();
	session.setAttribute("fsLogonBean.securepassword",fsLogonBean.getPassword());
	String fs_ename = fsAccessor.getFsVar("fsUserename");
	String fs_esurname = fsAccessor.getFsVar("fsUseresurname");
	String fs_engname = (fs_ename!=null&&fs_ename.trim().length()>0?fs_ename:"")+" "+(fs_esurname!=null&&fs_esurname.trim().length()>0?fs_esurname:"");
	if(fs_engname.trim().length()<=0) fs_engname = fsAccessor.getFsUserName();
	fsAccessor.setFsVar("fsNameEnglish",fs_engname);
	fsAccessor.setFsVar("fsChange",fsLogonBean.getChangeflag());
	result.append("<root type=\"result\">");
	result.append("<body>Signon</body>");
	result.append("<branch>"+fsLogonBean.getBranch()+"</branch>");
	result.append("<fskey>"+fsAccessor.getFsKey()+"</fskey>");
	String fs_defaultLang = PageUtility.getParameter(request,"language");
	if(fs_defaultLang!=null && !fs_defaultLang.trim().equals("")) {
		session.setAttribute("default_language",fs_defaultLang);	
		result.append("<language>").append(fs_defaultLang).append("</language>");
	}
	result.append("<change>"+fsLogonBean.getChangeflag()+"</change>");
	result.append(fsAccessor.toXML());
	result.append("</root>");
	out.println(result.toString());
	StringBuilder fs_info = new StringBuilder();
	fs_info.append("{\"user\":\"").append(fsLogonBean.getUsername()).append("\",\"password\":\"").append(fsLogonBean.getPassword()).append("\",").append(fsAccessor.toJSON(false)).append("}");
	SingleLogOn logon = new SingleLogOn(request,response);
	logon.signon(fsLogonBean.getUser(),fsLogonBean.getPassword(),fs_info.toString());
	//logon.signon(fsLogonBean.getUser(),fsLogonBean.getPassword(),fsAccessor.toJSON());
	Trace.debug(fsAccessor,"========================================================================");
	fsAccessor.startSaveAccessor();
%>
