﻿<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/logon/logon_errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsLogonBean" scope="request" class="com.fs.dev.auth.SignonBean"/>
<%
	StringBuilder result = new StringBuilder();
	result.append("<root type=\"result\">");
	result.append("<body>Signon</body>");
	result.append("<branch>"+fsLogonBean.getBranch()+"</branch>");
	result.append("<fskey>"+fsAccessor.getFsKey()+"</fskey>");
	String fs_defaultLang = PageUtility.getParameter(request,"language");
	if(fs_defaultLang!=null && !fs_defaultLang.trim().equals("")) {
		session.setAttribute("default_language",fs_defaultLang);	
		result.append("<language>").append(fs_defaultLang).append("</language>");
	}
	result.append("<change>"+fsLogonBean.getChangeflag()+"</change>");
	result.append(fsAccessor.toXML());
	result.append("</root>");
	Trace.info(result);
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Log In</title>		
		<script type="text/javascript">
			function onInit() {
				console.log(user_info.value);
				try { window.parent.loginSuccess(user_info.value); }catch(ex) { }
			}
		</script>			
	</head>
	<body onload="onInit()">
		<h3 style="text-align:center;">Log In Success</h3>
		<textarea id="user_info" style="display:none;">
			<%=result.toString()%>
		</textarea> 
	</body>
</html>