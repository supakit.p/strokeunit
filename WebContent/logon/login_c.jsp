<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.auth.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsLogonBean" scope="request" class="com.fs.dev.auth.SignonBean"/>
<jsp:setProperty name="fsLogonBean" property="*"/>
<%
Trace.info("login "+request.getContextPath()+" : "+request.getQueryString());
java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
if(fs_map!=null) fsLogonBean.obtain(fs_map);
String fs_language = fsLogonBean.getLanguage();
boolean eng = com.fs.dev.TheUtility.isEnglish(fsLogonBean.getLanguage());
Trace.info("fsLogonBean = "+fsLogonBean);
session.setAttribute("fsLogonBean.user",fsLogonBean.getUser());
session.setAttribute("fsLogonBean.password",fsLogonBean.getPassword());
fsGlobal.setFsVar("fsAddress",request.getRemoteAddr());
fsGlobal.setFsSection("AUTH");
JSONObject result = new JSONObject();
if(fsLogonBean.getUsername().trim().length()<=0) {
	fsLogonBean.setUsername(PageUtility.getParameter(request,"userid"));
	if(fs_map!=null) {
		fsLogonBean.setUsername((String)fs_map.get("userid"));
	}
}
if(fsLogonBean.getUsername().trim().length()<=0) {
	String fs_msg = "User or Password is undefined";
	fs_msg = ErrorConfig.getError("5001",fs_msg,fs_language);
	fsGlobal.setFsMessage(fs_msg);
	JSONHeader header = new JSONHeader();
	header.setModel("ez001");
	header.setMethod("login");
	header.setErrorflag("Y");
	header.setErrorcode("5001");
	header.setErrordesc(fs_msg);
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
String signon = (String)GlobalVariable.getVariable("VALIDATE_SIGNON");
if(!(signon!=null && signon.equalsIgnoreCase("false"))) {
	int maxfailure = 3;
	String maxfail = (String)GlobalVariable.getVariable("MAXFAIL_SIGNON");
	if((maxfail!=null) && !maxfail.trim().equals("")) {
		try {
			maxfailure = Integer.parseInt(maxfail);
		} catch(Exception ex) { maxfailure = 3; }
	}
	Integer counter = (Integer)session.getAttribute(fsLogonBean.getUser()+"_signoncounter");
	if(counter==null) counter = new Integer(0);
	if(counter.intValue()>=maxfailure) {
		String fs_msg = "Signon failure over "+maxfailure+" times.";
		fs_msg = ErrorConfig.getError("5003",fs_msg,fs_language,new String[] { ""+maxfailure });
		fsGlobal.setFsMessage(fs_msg);
		JSONHeader header = new JSONHeader();
		header.setModel("ez001");
		header.setMethod("login");
		header.setErrorflag("Y");
		header.setErrorcode("5003");
		header.setErrordesc(fs_msg);
		result.put("head",header);
		out.println(result.toJSONString());
		return;		
	}
	try {
		boolean passed = false;
		String fs_errcode = "5001";
		String fs_msg = "User or Password is incorrect";
		fs_msg = ErrorConfig.getError("5001",fs_msg,fs_language);
		String fs_password = fsLogonBean.getPassword();
		fsGlobal.setFsVar("fsUser",fsLogonBean.getUser());
		fsGlobal.setFsVar("fsBranch",fsLogonBean.getBranch());
		fsGlobal.setFsProg("signin");
		fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
		BeanTransport fsTransport = TheTransportor.transport(fsGlobal,fsLogonBean);
                Trace.info("fsLogonBean :" +  fsLogonBean);
		if(fsTransport!=null) fsGlobal.setSuccess(fsTransport.isSuccess());
		boolean found = false;
		if(fsLogonBean.effectedTransactions()>0) {
			found = true;
			if(fs_password.equals(fsLogonBean.getPassword())) {
				passed = true;
			} else {
				if(fs_password.length()>8) {
					//fs_password = fs_password.substring(0,8);
					fs_errcode = "5002";
					fs_msg = "User or Password is incorrect";
					fs_msg = ErrorConfig.getError("5002",fs_msg,fs_language);
				} else {
					com.fs.dev.auth.PasswordLibrary encp = new com.fs.dev.auth.PasswordLibrary();
					String password = encp.encrypt(fs_password);
					Trace.debug("password encrypt : "+fs_password+" ("+password+") = "+fsLogonBean.getPassword());
					if(password.equals(fsLogonBean.getPassword())) {
						passed = true;
					} else {
						fs_errcode = "5002";
						fs_msg = "User or Password is incorrect";
						fs_msg = ErrorConfig.getError("5002",fs_msg,fs_language);
					}
				}
			}
			if(passed) {
				BeanUtility butil = new BeanUtility();
				if(fsLogonBean.getActiveflag()!=null && !fsLogonBean.getActiveflag().equals("1")) {
					passed = false;
					fs_errcode = "5004";
					fs_msg = "You are not active, please contact administrator";
					fs_msg = ErrorConfig.getError("5004",fs_msg,fs_language);
					if(fsLogonBean.getStatus().equals("P")) { //Pending
						fs_errcode = "5007";
						fs_msg = ErrorConfig.getError("5007",fs_msg,fs_language);
					} else if(fsLogonBean.getStatus().equals("C")) { //Closed
						fs_errcode = "5008";
						fs_msg = ErrorConfig.getError("5008",fs_msg,fs_language);
					}
				}
				if(passed && (fsLogonBean.getLockflag()!=null && fsLogonBean.getLockflag().equals("1"))) {
					passed = false;
					fs_errcode = "5005";
					fs_msg = "You are locking, please contact administrator";
					fs_msg = ErrorConfig.getError("5005",fs_msg,fs_language);
					if(fsLogonBean.getFailtime()!=null && fsLogonBean.getFailtime().trim().length()>0) {
						long fs_failtime = butil.parseBigDecimal(fsLogonBean.getFailtime()).longValue();
						long fs_curtime = System.currentTimeMillis();
						long fs_difftime = fs_curtime - fs_failtime;
						if(fs_difftime<=180000) {
							fs_errcode = "5009";
							fs_msg = "Your User ID has been locked. Please contact administrator or wait and retry again after 3 minute.";
							fs_msg = ErrorConfig.getError("5009",fs_msg,fs_language);
						} else {
							passed = true;
							com.fs.dev.auth.LockBean fsLockBean = new com.fs.dev.auth.LockBean();
							fsLockBean.setUser(fsLogonBean.getUser());
							fsLockBean.setLock("0");
							GlobalBean fsHandler = (GlobalBean)fsGlobal;
							fsHandler.setFsAction(GlobalBean.UPDATE_MODE);
							TheTransportor.transport(fsHandler,fsLockBean);
						}
					}
				}
				if(passed && (fsLogonBean.getEffectdate()!=null && fsLogonBean.getEffectdate().trim().length()>0)) {
					java.util.Date effectdate = butil.parseDate(fsLogonBean.getEffectdate());
					java.util.Date currentdate = new java.util.Date();
					int reply = butil.compareDate(currentdate,effectdate);
					if(reply<0) {
						passed = false;
						fs_msg = "You are not active, out of effective date.";
						fs_msg = ErrorConfig.getError("5010",fs_msg,fs_language);						
					}
				}				
			}
		}
		if(!passed) {
			if((counter.intValue()+1)>=maxfailure) {
				com.fs.dev.auth.LockBean fsLockBean = new com.fs.dev.auth.LockBean();
				fsLockBean.setUser(fsLogonBean.getUser());
				fsLockBean.setLock("1");
				GlobalBean fsHandler = (GlobalBean)fsGlobal;
				fsHandler.setFsAction(GlobalBean.UPDATE_MODE);
				TheTransportor.transport(fsHandler,fsLockBean);
			}
			session.setAttribute(fsLogonBean.getUser()+"_signoncounter",new Integer(counter.intValue()+1));	
			fsLogonBean.setPassword(fs_password);
			fsGlobal.setFsMessage(fs_msg);
			Tracker.track(fsGlobal);
			JSONHeader header = new JSONHeader();
			header.setModel("ez001");
			header.setMethod("login");
			header.setErrorflag("Y");
			header.setErrorcode(fs_errcode);
			header.setErrordesc(fs_msg);
			result.put("head",header);
			out.println(result.toJSONString());
			return;
		}
		session.removeAttribute(fsLogonBean.getUser()+"_signoncounter"); 
	} catch(Exception ex) {
		fsGlobal.setThrowable(ex);	
		JSONHeader header = new JSONHeader();
		header.setModel("ez001");
		header.setMethod("login");
		header.setErrorflag("Y");
		header.setErrorcode("1");
		header.setErrordesc(fsGlobal.getFsMessage());
		result.put("head",header);
		out.println(result.toJSONString());
		return;
	}
}
if((fsLogonBean.getLogondate()==null) || fsLogonBean.getLogondate().equals("")) {
	BeanFormat formater = new BeanFormat();
	java.util.Date date = new java.util.Date();
	fsLogonBean.setLogondate(formater.formatDate(date,"dd/MM/yyyy"));
}
fsAccessor.setFsKey(session.getId());
fsLogonBean.assign(fsAccessor);
Trace.debug(fsLogonBean);
Trace.debug(fsAccessor);
fsGlobal.obtain(fsAccessor);
fsGlobal.retain(request);
Tracker.trace(fsGlobal);
final GlobalBean fsAccessHandler = (GlobalBean)fsGlobal;
final com.fs.dev.auth.AccessBean access = new com.fs.dev.auth.AccessBean();
access.setAccessor(fsLogonBean.getUser());
new Thread() {
	public void run() {
		try { access.update(fsAccessHandler); }catch(Exception ex) { }
		this.interrupt();
	}
}.start();
session.setAttribute("fsLogonBean.securepassword",fsLogonBean.getPassword());
String fs_url = com.fs.dev.TheUtility.getContextPath(request);
String fs_img = fsAccessor.getFsImage();
JSONHeader header = new JSONHeader();
header.setModel("logon");
header.setMethod("login");
header.setErrorflag("N");
header.setErrorcode("0");
header.setErrordesc(fsGlobal.getFsMessage());
result.put("head",header);
JSONObject body = new JSONObject();
body.put("userid",fsAccessor.getFsUser());
body.put("usertype",fsAccessor.getFsUsertype());
body.put("email",fsAccessor.getFsEmail());
body.put("role",fsAccessor.getFsGroups());
body.put("key",fsAccessor.getFsKey());
body.put("photoimage",fs_img.trim().length()<=0?"":(fs_url+"/resources/photo/"+fs_img));
body.put("compid",fsAccessor.getFsSite());
if(eng) {
	body.put("usertname",fsAccessor.getFsVar("fsUserename"));
	body.put("usertsurname",fsAccessor.getFsVar("fsUseresurname"));
	body.put("comname",fsAccessor.getFsVar("fsCompename"));
	body.put("branchname",fsAccessor.getFsVar("fsBranchename"));
} else {
	body.put("usertname",fsAccessor.getFsVar("fsUsertname"));
	body.put("usertsurname",fsAccessor.getFsVar("fsUsertsurname"));	
	body.put("comname",fsAccessor.getFsVar("fsComptname"));
	body.put("branchname",fsAccessor.getFsVar("fsBranchtname"));
}
body.put("change",fsLogonBean.getChangeflag());
result.put("body",body);
fsAccessor.startSaveAccessor();
out.println(result.toJSONString());
String fs_info = "{\"user\":\""+fsLogonBean.getUsername()+"\",\"password\":\""+fsLogonBean.getPassword()+"\","+fsAccessor.toJSON(false)+"}";
SingleLogOn logon = new SingleLogOn(request,response);
logon.signon(fsLogonBean.getUser(),fsLogonBean.getPassword(),fs_info);
%>
