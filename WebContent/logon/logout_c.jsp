<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.auth.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="org.json.simple.*" %>
<%@ page import="com.fs.dev.servlet.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="page" class="com.fs.bean.util.GlobalBean"/>
<%
	JSONObject result = new JSONObject();
	fsGlobal.setFsProg("logon");
	fsGlobal.setFsSection("AUTH");
	fsGlobal.obtain(session);
	fsGlobal.obtain(fsAccessor);
	JSONHeader header = new JSONHeader();
	header.setModel("logon");
	header.setMethod("logout");
	fsGlobal.setFsVar("fsUser",fsAccessor.getFsUser());
	fsGlobal.setFsVar("fsBranch",fsAccessor.getFsBranch());
	fsGlobal.setFsVar("fsSite",fsAccessor.getFsSite());
	fsGlobal.setFsProg("logout");	
	Tracker.track(fsGlobal,Logger.ACTION_STYLE+Logger.INOUT_STYLE);
	fsAccessor.wrap(new AccessorBean());
	session.removeAttribute("signoncounter"); 
	session.removeAttribute("fsAccessor");
	SingleLogOn logon = new SingleLogOn(request,response);
	logon.unregister();
	try{
		String fsKey = fsAccessor.getFsKey();
		HttpSession hs = (HttpSession)TheSession.sessions().get(fsKey);
		if(hs!=null) hs.invalidate();
		String tokenkey = request.getHeader("tokenkey");
		if(tokenkey!=null && !tokenkey.equals(fsKey)) {
			HttpSession hss = (HttpSession)TheSession.sessions().get(tokenkey);
			if(hss!=null) hss.invalidate();		
		}
	}catch(Exception ex) { }
	try {
		com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
		fsBean.addSchema("userid",java.sql.Types.VARCHAR,"userid");
		fsBean.obtainFrom(request); //assign variable from request
		fsBean.forceObtain(fsGlobal);
		java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
		if(fs_map!=null) fsBean.obtain(fs_map); //assign variable from rest
		fsBean.obtain(session,request);
		String fs_userid = fsBean.getString("userid");
		if(fs_userid !=null && fs_userid.trim().length()>0) {
			TheSession.invalidate(fs_userid);
		}	
	}catch(Exception ex) {
		Trace.error(fsAccessor,ex);
	}
	request.getSession().invalidate();
	header.setErrorflag("N");
	header.setErrorcode("0");
	result.put("head",header);
	out.println(result.toJSONString());
%>
