<%@ page isErrorPage="true" %>
<%@ page import="com.fs.bean.util.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%
	String errorMessage = PageUtility.getParameter(request,"message");
	if((errorMessage==null) || errorMessage.trim().equals("")) errorMessage = "General Protection Error Occured";
	if(exception!=null) {
		exception.printStackTrace();
		errorMessage = exception.getMessage();
		if(exception instanceof javax.el.ELException) {
			javax.el.ELException ex = (javax.el.ELException)exception;
			if(ex.getCause() instanceof com.fs.bean.BeanException) {
				errorMessage = ((com.fs.bean.BeanException)ex.getCause()).getMessage();
			}
		}
	}
	request.setAttribute("error_message",errorMessage);		
	String fs_forwarder = "/page_login/page_login_main.jsp";
	RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
	rd.forward(request, response); 
%>
