<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.dev.servlet.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%
	String result = "";
	String msgType = "result";
	String key = PageUtility.getParameter(request,"key");
	if(key!=null) {
		AccessorBean bean = TheSession.getAccessorByKey(key);
		if(bean!=null) {
			result = bean.toJSON(false);
		} else {
			result = "\"accessor\":\"Accessor not found\"";			
			msgType = "none";
		}
	} else {
		msgType = "default";
		result = fsAccessor.toJSON(false);
	}
%>
{
	"type":"<%=msgType%>",
	<%=result%>
}
