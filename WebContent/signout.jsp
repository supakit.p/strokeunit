<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.auth.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.dev.util.*"%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="page" class="com.fs.bean.util.GlobalBean"/>
<%
	fsGlobal.setFsVar("fsUser",fsAccessor.getFsUser());
	fsGlobal.setFsVar("fsBranch",fsAccessor.getFsBranch());
	fsGlobal.setFsVar("fsSite",fsAccessor.getFsSite());
	fsGlobal.setFsProg("signout");
	fsGlobal.setFsSection("AUTH");
	Tracker.track(fsGlobal,Logger.ACTION_STYLE+Logger.INOUT_STYLE);
	fsAccessor.wrap(new AccessorBean());
	StringBuilder result = new StringBuilder();
	result.append("<body>Signout</body>");
	session.removeAttribute("signoncounter"); 
	session.removeAttribute("fsAccessor");
	SingleLogOn logon = new SingleLogOn(request,response);
	logon.unregister();
	request.getSession().invalidate();
	String fs_forwarder = "/home.jsp";
	RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
	rd.forward(request, response);
	
%>
