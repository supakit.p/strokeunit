<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE005CBean" scope="request" class="com.fs.bean.SFTE005CBean"/>
<jsp:setProperty name="fsSFTE005CBean" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("change_password");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
JSONHeader header = new JSONHeader();
header.setModel("change_password");
header.setMethod("change");
try { 
	fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	fsSFTE005CBean.forceObtain(fsGlobal);
	fsSFTE005CBean.obtainFrom(request);
	String fs_language = fsGlobal.getFsLanguage();
	String fs_lang = request.getParameter("language");
	if(fs_lang!=null && fs_lang.trim().length()>0) fs_language = fs_lang;
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) {
		fsSFTE005CBean.obtain(fs_map);
		if(fs_map.get("language")!=null) fs_language = (String)fs_map.get("language");
	}
	fsGlobal.setFsLanguage(fs_language);
	fsSFTE005CBean.obtain(session,request);
	if(fsSFTE005CBean.getUserid()==null || fsSFTE005CBean.getUserid().trim().length()<=0) {
		fsSFTE005CBean.setUserid(fsAccessor.getFsUser());
	}
	if(fsSFTE005CBean.getUserid()==null || fsSFTE005CBean.getUserid().trim().length()<=0) {
		String fs_msg = "User is undefined";
		fs_msg = ErrorConfig.getError("-8891",fs_msg,fs_language);
		fsGlobal.setFsMessage(fs_msg);
		header.setErrorflag("Y");
		header.setErrorcode("-8891");
		header.setErrordesc(fs_msg);
		result.put("head",header);
		out.println(result.toJSONString());
		return;		
	}
	if(!fsSFTE005CBean.getUserpassword().equals(fsSFTE005CBean.getConfirmpassword())) {
		String fs_msg = "Confirm Password Mismatch";
		fs_msg = ErrorConfig.getError("-8897",fs_msg,fs_language);
		fsGlobal.setFsMessage(fs_msg);
		header.setErrorflag("Y");
		header.setErrorcode("-8897");
		header.setErrordesc(fs_msg);
		result.put("head",header);
		out.println(result.toJSONString());
		return;
	}
	if(fsSFTE005CBean.getUserpassword().equals(fsSFTE005CBean.getUserid())) {
		String fs_msg = "Not allow password as same as user";
		fs_msg = ErrorConfig.getError("-8899",fs_msg,fs_language);
		fsGlobal.setFsMessage(fs_msg);
		header.setErrorflag("Y");
		header.setErrorcode("-8899");
		header.setErrordesc(fs_msg);
		result.put("head",header);
		out.println(result.toJSONString());
		return;
	}
	if(fsSFTE005CBean.getUserpassword().length()>8) {
		String fs_msg = "Password length not over 8 characters";
		fs_msg = ErrorConfig.getError("-8896",fs_msg,fs_language);
		fsGlobal.setFsMessage(fs_msg);
		header.setErrorflag("Y");
		header.setErrorcode("-8896");
		header.setErrordesc(fs_msg);
		result.put("head",header);
		out.println(result.toJSONString());
		return;
	}
	fsSFTE005CBean.transport(fsGlobal);
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	header.setErrorflag("Y");
	header.setErrorcode("1");
	header.setErrordesc(fsGlobal.getFsMessage());
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
header.setErrorflag("N");
header.setErrorcode("0");
header.setErrordesc(fsGlobal.getFsMessage());
result.put("head",header);
out.println(result.toJSONString());
%>
