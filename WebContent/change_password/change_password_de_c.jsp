<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('change_password',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE005CBean" scope="request" class="com.fs.bean.SFTE005CBean"/>
<jsp:setProperty name="fsSFTE005CBean" property="*"/>
<%
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("change_password");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/change_password/change_password_m.jsp";
try { 	
	fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	fsSFTE005CBean.obtain(session,request);
	if(fsSFTE005CBean.getUserid()==null || fsSFTE005CBean.getUserid().trim().length()<=0) {
		fsSFTE005CBean.setUserid(fsAccessor.getFsUser());
	}
	if(!fsSFTE005CBean.getUserpassword().equals(fsSFTE005CBean.getConfirmpassword())) {
		throw new BeanException("Confirm Password Mismatch",-8897);
	}
	if(fsSFTE005CBean.getUserpassword().equals(fsSFTE005CBean.getUserid())) {
		throw new BeanException("Not allow password as same as user",-8899);
	}
	if(fsSFTE005CBean.getUserpassword().length()>8) {
		throw new BeanException("Password length not over 8 characters",-8896);
	}
	fsSFTE005CBean.transport(fsGlobal);
	fsAccessor.setFsVar("fsPasswordexpire",fsSFTE005CBean.getPasswordexpiredate());
	if(fsSFTE005CBean.effectedTransactions()>0) {		
		response.sendRedirect(request.getContextPath()+"/index.jsp");
		return;
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsSFTE005CBean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE005CBean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE005CBean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE005CBean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE005CBean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
