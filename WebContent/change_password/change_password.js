var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("change_password"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
		setupComponents();
		setupAlertComponents();
		$("#userpassword").blur(function() { 
			$("#matchpassword_alert").hide();
		});
		$("#confirmpassword").blur(function() {
			$("#matchpassword_alert").hide();
		});
}
function setupComponents() {
	$("#savebutton").click(function() { 
		return save();
	});
}
function clearingFields() {
}
function validForm() {
	clearAlerts();
	$("#matchpassword_alert").hide();
	var validator = null;
	if($.trim($("#oldpassword").val())=="") {
		$("#oldpassword").parent().addClass("has-error");
		$("#oldpassword_alert").show();
		if(!validator) validator = "oldpassword";
	}
	if($.trim($("#userpassword").val())=="") {
		$("#userpassword").parent().addClass("has-error");
		$("#userpassword_alert").show();
		if(!validator) validator = "userpassword";
	}
	if($.trim($("#confirmpassword").val())=="") {
		$("#confirmpassword").parent().addClass("has-error");
		$("#confirmpassword_alert").show();
		if(!validator) validator = "confirmpassword";
	}
	if($("#userpassword").val()!=$("#confirmpassword").val()) {
		$("#confirmpassword").parent().addClass("has-error");
		$("#matchpassword_alert").show();
		if(!validator) validator = "matchpassword";
	}	
	if(validator) {
		var matching = validator=="matchpassword";
		if(matching) validator = "confirmpassword";
		$("#"+validator).focus();
		setTimeout(function() { 
			$("#"+validator).parent().addClass("has-error");
			if(!matching) $("#"+validator+"_alert").show();
		},100);
		return false;
	}
	return true;
}
function save(aform) {
	if(!validForm()) return false;
	fsentryform.submit();
	return true;
}

