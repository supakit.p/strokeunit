<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("P01");
    header.setMethod("Submit");
    try {
        Trace.info("############### P01/Submit ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");

        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();

        if (fs_apiName != null && fs_apiName.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            final String fs_action = GlobalBean.RETRIEVE_MODE;
            final String fs_actionIns = GlobalBean.INSERT_MODE;
            final String fs_actionInsNoKey = TheStrokeUnit.INSERT_MODE_NO_KEY;
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                final String PatientInfo = "PatientInfo";
                final String PersonalData = "PersonalData";
                final String PresentIllness = "PresentIllness";
                final String PastIllness = "PastIllness";
                final String Skin = "Skin";
                final String LNHospital = "LNHospital";
                final String LNDischargs = "LNDischargs";

                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();
                    org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, fs_apiName);
                    if (jsonArrayConfigfield.length() <= 0) {
                        throw new RuntimeException("Not Found ApiName");
                    }

                    if (PersonalData.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ PersonalData " + patientId + " ############");
                        String vsId = "";
                        if (!(json.has("vsId") && json.getString("vsId").trim().length() > 0)) {
                            vsId = TheStrokeUnit.Generate.createUUID();
                            json.put("vsId", vsId);
                            body.put("vsId", vsId);
                        }
                        json.put("formId", PatientInfo);
                        if (json.has("bmi") && json.getString("bmi").length() > 0) {
                            double bmi = json.getDouble("bmi");
                            if (bmi < 18.5 || bmi > 25.0) {
                                json.put("ssBMI", "Y");
                            } else {
                                json.put("ssBMI", "N");
                            }
                        }

                        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);

                        // DELETE Tw_Symptom
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("DELETE FROM Tw_CoMorbility WHERE PatientId =?patientId ");
                        knsql.setParameter("patientId", patientId);
                        knsql.executeUpdate(connection);
                        // DELETE Tw_InitialDiagnosis
                        knsql = new KnSQL(this);
                        knsql.append("DELETE FROM Tw_InitialDiagnosis WHERE PatientId =?patientId ");
                        knsql.setParameter("patientId", patientId);
                        knsql.executeUpdate(connection);

                        if (json.has("coMobility")) {
                            org.json.JSONArray jsonCoMobility = json.getJSONArray("coMobility");
                            org.json.JSONArray jsonArrayConfigfieldCoMobility = su.getConfigfield(connection, fs_apiName, "Tw_CoMorbility");
                            org.json.JSONArray jsonArrayTableListConfigCoMobility = su.handleConfigAllData(jsonArrayConfigfieldCoMobility);
                            for (int i = 0; i < jsonCoMobility.length(); i++) {
                                org.json.JSONObject jsonObjectElement = jsonCoMobility.getJSONObject(i);
                                jsonObjectElement.put("patientId", patientId);
                                result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigCoMobility, jsonObjectElement).getEffectedTransactions();
                            }
                        }

                        if (json.has("initialDiagnosis")) {
                            org.json.JSONArray jsonInitialDiagnosis = json.getJSONArray("initialDiagnosis");
                            org.json.JSONArray jsonArrayConfigfieldInitialDiagnosis = su.getConfigfield(connection, fs_apiName, "Tw_InitialDiagnosis");
                            org.json.JSONArray jsonArrayTableListConfigInitialDiagnosis = su.handleConfigAllData(jsonArrayConfigfieldInitialDiagnosis);
                            for (int i = 0; i < jsonInitialDiagnosis.length(); i++) {
                                org.json.JSONObject jsonObjectElement = jsonInitialDiagnosis.getJSONObject(i);
                                jsonObjectElement.put("patientId", patientId);
                                result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigInitialDiagnosis, jsonObjectElement).getEffectedTransactions();
                            }
                        }

                        result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();

                    } else if (PresentIllness.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ PresentIllness " + patientId + " ############");
                        String nsId = "";
                        if (!(json.has("nsId") && json.getString("nsId").trim().length() > 0)) {
                            nsId = TheStrokeUnit.Generate.createUUID();
                            json.put("nsId", nsId);
                        }
                        json.put("formId", PatientInfo);
                        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);

                        // DELETE Tw_Symptom
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("DELETE FROM Tw_Symptom WHERE PatientId =?patientId ");
                        knsql.setParameter("patientId", patientId);
                        knsql.executeUpdate(connection);
                        // DELETE Tw_AntiPlatelet
                        knsql = new KnSQL(this);
                        knsql.append("DELETE FROM Tw_AntiPlatelet WHERE PatientId =?patientId ");
                        knsql.setParameter("patientId", patientId);
                        knsql.executeUpdate(connection);

                        if (json.has("symptom")) {
                            org.json.JSONArray jsonSymptom = json.getJSONArray("symptom");
                            org.json.JSONArray jsonArrayConfigfieldSymptom = su.getConfigfield(connection, fs_apiName, "Tw_Symptom");
                            org.json.JSONArray jsonArrayTableListConfigSymptom = su.handleConfigAllData(jsonArrayConfigfieldSymptom);
                            for (int i = 0; i < jsonSymptom.length(); i++) {
                                org.json.JSONObject jsonObjectElement = jsonSymptom.getJSONObject(i);
                                jsonObjectElement.put("patientId", patientId);
                                result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigSymptom, jsonObjectElement).getEffectedTransactions();
                            }
                        }
                        if (json.has("antiPlatelet")) {
                            org.json.JSONArray jsonAntiPlatelet = json.getJSONArray("antiPlatelet");
                            org.json.JSONArray jsonArrayConfigfieldAntiPlatelet = su.getConfigfield(connection, fs_apiName, "Tw_AntiPlatelet");
                            org.json.JSONArray jsonArrayTableListConfigAntiPlatelet = su.handleConfigAllData(jsonArrayConfigfieldAntiPlatelet);
                            for (int i = 0; i < jsonAntiPlatelet.length(); i++) {
                                org.json.JSONObject jsonObjectElement = jsonAntiPlatelet.getJSONObject(i);
                                jsonObjectElement.put("patientId", patientId);
                                result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigAntiPlatelet, jsonObjectElement).getEffectedTransactions();
                            }
                        }

                        result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();

                    } else if (PastIllness.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ PastIllness " + patientId + " ############");
                        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);

                        // DELETE Tw_Allergies
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("DELETE FROM Tw_Allergies WHERE PatientId =?patientId ");
                        knsql.setParameter("patientId", patientId);
                        knsql.executeUpdate(connection);
                        
                        if (json.has("allergie")) {
                            org.json.JSONArray jsonAllergies = json.getJSONArray("allergie");
                            org.json.JSONArray jsonArrayConfigfieldAllergies = su.getConfigfield(connection, fs_apiName, "Tw_Allergies");
                            org.json.JSONArray jsonArrayTableListConfigAllergies = su.handleConfigAllData(jsonArrayConfigfieldAllergies);
                            for (int i = 0; i < jsonAllergies.length(); i++) {
                                org.json.JSONObject jsonObjectElement = jsonAllergies.getJSONObject(i);
                                jsonObjectElement.put("patientId", patientId);
                                result += su.handleParams(connection, fs_actionInsNoKey, jsonArrayTableListConfigAllergies, jsonObjectElement).getEffectedTransactions();
                            }
                        }

                        result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                    } else if (Skin.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Skin " + patientId + " ############");
                        
                        // DELETE Tw_SkinAbnormal
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("DELETE FROM Tw_SkinAbnormal WHERE PatientId =?patientId ");
                        knsql.setParameter("patientId", patientId);
                        knsql.executeUpdate(connection);
                        
                        if (json.has("abnormal")) {
                            org.json.JSONArray jsonSkinAbnormal = json.getJSONArray("abnormal");
                            org.json.JSONArray jsonArrayConfigfieldSkinAbnormal = su.getConfigfield(connection, fs_apiName, "Tw_SkinAbnormal");
                            org.json.JSONArray jsonArrayTableListConfigSkinAbnormal = su.handleConfigAllData(jsonArrayConfigfieldSkinAbnormal);
                            for (int i = 0; i < jsonSkinAbnormal.length(); i++) {
                                org.json.JSONObject jsonObjectElement = jsonSkinAbnormal.getJSONObject(i);
                                jsonObjectElement.put("patientId", patientId);
                                result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigSkinAbnormal, jsonObjectElement).getEffectedTransactions();
                            }
                        }
                        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);
                        result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                    } else if (LNHospital.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ LNHospital " + patientId + " ############");
                        String lnId = "";
                        if (!(json.has("lnId") && json.getString("lnId").trim().length() > 0)) {
                            lnId = TheStrokeUnit.Generate.createUUID();
                            json.put("lnId", lnId);
                            body.put("lnId", lnId);
                        }
                        json.put("hospital", 1);
                        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);
                        result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                    } else if (LNDischargs.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ LNDischargs " + patientId + " ############");
                        String lnId = "";
                        if (!(json.has("lnId") && json.getString("lnId").trim().length() > 0)) {
                            lnId = TheStrokeUnit.Generate.createUUID();
                            json.put("lnId", lnId);
                            body.put("lnId", lnId);
                        }
                        json.put("dischargs", 1);
                        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);
                        result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                    } else {
                        String patientId = json.has("patientId") ? json.getString("patientId") : "";
                        Trace.info("############ " + fs_apiName + " " + patientId + " ############");
                        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);
                        result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                    }

                    Trace.info("body : " + body);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);

            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
