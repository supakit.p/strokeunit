<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("P01");
    header.setMethod("request");
    try {
        Trace.info("############### P01/request ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();

        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();

        if (fs_apiName != null && fs_apiName.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                final String PatientInfo = "PatientInfo";
                final String PersonalData = "PersonalData";
                final String PresentIllness = "PresentIllness";
                final String PastIllness = "PastIllness";
                final String MoreInformation = "MoreInformation";
                final String Nutrition = "Nutrition";
                final String Skin = "Skin";
                final String Cadiopulmonary = "Cadiopulmonary";
                final String Neuromuscular = "Neuromuscular";
                final String Mobility = "Mobility";
                final String Safety = "Safety";
                final String Elimination = "Elimination";
                final String PainManagement = "PainManagement";
                final String Spiritual = "Spiritual";
                final String LNHospital = "LNHospital";
                final String DischargePlanning = "DischargePlanning";
                final String ContinuingCare = "ContinuingCare";
                final String LNDischargs = "LNDischargs";

                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();

                    if (PersonalData.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ PersonalData " + patientId + " ############");

                        // PersonalData
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_PatientInfo.PatientId, Tw_PatientInfo.Gender , Tw_PatientInfo.LanguageId , Tw_PatientInfo.LanguageText ,Tw_PatientInfo.EducationId,Tw_PatientInfo.EducationText,Tw_PatientInfo.OccupationId,Tw_PatientInfo.OccupationText,Tw_PatientInfo.Height,Tw_PatientInfo.HeightId,Tw_PatientInfo.Weight,Tw_PatientInfo.WeightId,Tw_PatientInfo.BMI,Tw_PatientInfo.DominantHandId ");
                        knsql.append(",Tw_VitalSign.VitalSignId,Tw_VitalSign.Temperature,Tw_VitalSign.HeartRate,Tw_VitalSign.RespiratoryRate,Tw_VitalSign.SBP,Tw_VitalSign.DBP ");
                        knsql.append(",Tw_PersonalData.ModeOfArrivalId,Tw_PersonalData.ModeOfArrivalText,Tw_PersonalData.AdmittedFromId,Tw_PersonalData.AdmittedFromText,Tw_PersonalData.InitialDiagnosis,Tw_PersonalData.ChiefComplaint , Tw_RNofFrom.InitiateRN ");
                        knsql.append(" FROM Tw_PatientInfo ");
                        knsql.append(" LEFT JOIN Tw_PersonalData ON Tw_PatientInfo.PatientId = Tw_PersonalData.PatientId ");
                        knsql.append(" LEFT JOIN Tw_VitalSign ON Tw_PatientInfo.PatientId = Tw_VitalSign.PatientId AND Tw_VitalSign.FormId = 'PatientInfo' ");
                        knsql.append(" LEFT JOIN Tw_RNofFrom ON Tw_PatientInfo.PatientId = Tw_RNofFrom.PatientId AND Tw_RNofFrom.FormId = 'RNP01' ");
                        knsql.append(" WHERE Tw_PatientInfo.PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldPD = new HashMap();
                        configFieldPD.put("PatientId", "patientId");
                        configFieldPD.put("Gender", "gender");
                        configFieldPD.put("LanguageId", "languageId");
                        configFieldPD.put("LanguageText", "languageText");
                        configFieldPD.put("EducationId", "educationId");
                        configFieldPD.put("EducationText", "educationText");
                        configFieldPD.put("OccupationId", "occupationId");
                        configFieldPD.put("OccupationText", "occupationText");
                        configFieldPD.put("Height", "height");
                        configFieldPD.put("HeightId", "heightId");
                        configFieldPD.put("Weight", "weight");
                        configFieldPD.put("WeightId", "weightId");
                        configFieldPD.put("BMI", "bmi");
                        configFieldPD.put("DominantHandId", "dominantHandId");
                        configFieldPD.put("InitiateRN", "rn");
                        configFieldPD.put("VitalSignId", "vsId");
                        configFieldPD.put("Temperature", "vsTemp");
                        configFieldPD.put("HeartRate", "vsHR");
                        configFieldPD.put("RespiratoryRate", "vsRR");
                        configFieldPD.put("SBP", "vsSBP");
                        configFieldPD.put("DBP", "vsDBP");
                        configFieldPD.put("ModeOfArrivalId", "moaId");
                        configFieldPD.put("ModeOfArrivalText", "moaText");
                        configFieldPD.put("AdmittedFromId", "admitId");
                        configFieldPD.put("AdmittedFromText", "admitText");
                        configFieldPD.put("InitialDiagnosis", "initialDia");
                        configFieldPD.put("ChiefComplaint", "chiefComplaint");
                        org.json.JSONObject jsonPD = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldPD).toString());
                        result += jsonPD.length();
                        System.err.println("jsonPD : " + jsonPD);
                        
                        // Co-Mobility
                        knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_CoMorbility.CoMobilityId , Tw_CoMorbility.CoMobilityText ");
                        knsql.append("FROM Tw_CoMorbility ");
                        knsql.append("WHERE Tw_CoMorbility.PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldCM = new HashMap();
                        configFieldCM.put("CoMobilityId", "coMobilityId");
                        configFieldCM.put("CoMobilityText", "coMobilityText");
                        org.json.JSONArray jsonCM = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldCM).toString());
                        result += jsonCM.length();
                        jsonPD.put("coMobility", jsonCM);

                        // InitialDiagnosis
                        knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_InitialDiagnosis.InitialDiagnosisId , Tw_InitialDiagnosis.initialDiagnosisText ");
                        knsql.append("FROM Tw_InitialDiagnosis ");
                        knsql.append("WHERE Tw_InitialDiagnosis.PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldID = new HashMap();
                        configFieldID.put("InitialDiagnosisId", "initialDiagnosisId");
                        configFieldID.put("InitialDiagnosisText", "initialDiagnosisText");
                        org.json.JSONArray jsonID = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldID).toString());
                        result += jsonID.length();
                        jsonPD.put("initialDiagnosis", jsonID);

                        body.put("body", jsonPD);

                    } else if (PresentIllness.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ PresentIllness " + patientId + " ############");
                        // PresentIllness
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_PresentIllness.PresentIllness , Tw_PresentIllness.HospitalId,Tw_PresentIllness.HospitalText,Tw_PresentIllness.CTBrainNC,Tw_PresentIllness.IsrtPA,Tw_PresentIllness.rtPAPushAmt,Tw_PresentIllness.rtPAPushTime,Tw_PresentIllness.rtPADripAmt,Tw_PresentIllness.rtPADripTime,Tw_PresentIllness.IsCTA,Tw_PresentIllness.CTAResults,Tw_PresentIllness.CTAASPECT,Tw_PresentIllness.IsCTAThrombec ");
                        knsql.append(", Tw_PresentIllness.IsCTP,Tw_PresentIllness.CTPResults,Tw_PresentIllness.CTPASPECT,Tw_PresentIllness.IsCTPThrombec,Tw_PresentIllness.IsAntiPlatelet,Tw_PresentIllness.IsOther,Tw_PresentIllness.Other,Tw_PresentIllness.AuthorityId ");
                        knsql.append(", Tw_PatientInfo.LastSeenNormal ,Tw_PatientInfo.StrokeOnset ");
                        knsql.append(", Tw_NSScore.MRS ,Tw_NSScore.NIHSS ");
                        knsql.append(" FROM Tw_PatientInfo ");
                        knsql.append(" LEFT JOIN Tw_PresentIllness ON Tw_PresentIllness.PatientId = Tw_PatientInfo.PatientId ");
                        knsql.append(" LEFT JOIN Tw_NSScore ON Tw_PatientInfo.PatientId = Tw_NSScore.PatientId AND Tw_NSScore.FormId = '" + PatientInfo + "' ");
                        knsql.append(" WHERE Tw_PatientInfo.PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldPSI = new HashMap();
                        configFieldPSI.put("PresentIllness", "presentIllness");
                        configFieldPSI.put("HospitalId", "hospitalId");
                        configFieldPSI.put("HospitalText", "hospitalText");
                        configFieldPSI.put("CTBrainNC", "ctBrainNC");
                        configFieldPSI.put("IsrtPA", "isrtPa");
                        configFieldPSI.put("rtPAPushAmt", "rtPAPushAmt");
                        configFieldPSI.put("rtPAPushTime", "rtPAPushTime");
                        configFieldPSI.put("rtPADripAmt", "rtPADripAmt");
                        configFieldPSI.put("rtPADripTime", "rtPADripTime");
                        configFieldPSI.put("IsCTA", "isCTA");
                        configFieldPSI.put("CTAResults", "ctaResults");
                        configFieldPSI.put("CTAASPECT", "ctaASPECT");
                        configFieldPSI.put("IsCTAThrombec", "isCTAThrombec");
                        configFieldPSI.put("IsCTP", "isCTP");
                        configFieldPSI.put("CTPResults", "ctpResults");
                        configFieldPSI.put("CTPASPECT", "ctpASPECT");
                        configFieldPSI.put("IsCTPThrombec", "isCTPThrombec");
                        configFieldPSI.put("IsAntiPlatelet", "isAntiPlatelet");
                        configFieldPSI.put("IsOther", "isOther");
                        configFieldPSI.put("Other", "other");
                        configFieldPSI.put("AuthorityId", "authorityId");
                        configFieldPSI.put("LastSeenNormal", "lastSeenNormal");
                        configFieldPSI.put("StrokeOnset", "strokeOnset");
                        configFieldPSI.put("MRS", "mRS");
                        configFieldPSI.put("NIHSS", "nihss");

                        org.json.JSONObject jsonPSI = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldPSI).toString());
                        result += jsonPSI.length();

                        // Symtom
                        knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_Symptom.SymptomId,Tw_Symptom.SymptomText ");
                        knsql.append("FROM Tw_Symptom ");
                        knsql.append("WHERE Tw_Symptom.PatientId = ?PatientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldST = new HashMap();
                        configFieldST.put("SymptomId", "symptomId");
                        configFieldST.put("SymptomText", "symptomText");
                        org.json.JSONArray jsonST = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldST).toString());
                        result += jsonST.length();

                        // AntiPlatelet
                        knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_AntiPlatelet.AntiPlateletId,Tw_AntiPlatelet.AntiPlateletText ");
                        knsql.append("FROM Tw_AntiPlatelet ");
                        knsql.append("WHERE Tw_AntiPlatelet.PatientId = ?PatientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldAP = new HashMap();
                        configFieldAP.put("AntiPlateletId", "antiPlateletId");
                        configFieldAP.put("AntiPlateletText", "antiPlateletText");
                        org.json.JSONArray jsonAP = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldAP).toString());
                        result += jsonAP.length();

                        jsonPSI.put("symptom", jsonST);
                        jsonPSI.put("antiPlatelet", jsonAP);
                        body.put("body", jsonPSI);

                    } else if (PastIllness.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ PastIllness " + patientId + " ############");

                        // PastIllness
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_PastIllness.PastIllnessHistory, Tw_PastIllness.FamilyIllnessHistory , Tw_PastIllness.AllergiesId,Tw_PastIllness.InfoProvidId,Tw_PastIllness.InfoProvidText,Tw_PastIllness.EmergencyName,Tw_PastIllness.EmergencyRelationship,Tw_PastIllness.EmergencyPhone ");
                        knsql.append("FROM Tw_PastIllness ");
                        knsql.append("WHERE Tw_PastIllness.PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldPI = new HashMap();
                        configFieldPI.put("PastIllnessHistory", "pastIllnessHistory");
                        configFieldPI.put("FamilyIllnessHistory", "familyIllnessHistory");
                        configFieldPI.put("AllergiesId", "allergieId");
                        configFieldPI.put("InfoProvidId", "infoProvidId");
                        configFieldPI.put("InfoProvidText", "infoProvidText");
                        configFieldPI.put("EmergencyName", "emergencyName");
                        configFieldPI.put("EmergencyRelationship", "emergencyRelationship");
                        configFieldPI.put("EmergencyPhone", "emergencyPhone");
                        org.json.JSONObject jsonPI = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldPI).toString());
                        result += jsonPI.length();

                        // Allergie
                        knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_Allergies.AllergiesType , Tw_Allergies.TypeId , Tw_Allergies.TypeText ,Tw_Allergies.SymptomId,Tw_Allergies.SymptomText  ");
                        knsql.append("FROM Tw_Allergies ");
                        knsql.append("WHERE Tw_Allergies.PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldA = new HashMap();
                        configFieldA.put("AllergiesType", "allergieType");
                        configFieldA.put("TypeId", "typeId");
                        configFieldA.put("TypeText", "typeText");
                        configFieldA.put("SymptomId", "symtomId");
                        configFieldA.put("SymptomText", "symtomText");
                        org.json.JSONArray jsonA = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldA).toString());
                        result += jsonA.length();

                        jsonPI.put("allergie", jsonA);
                        body.put("body", jsonPI);

                    } else if (MoreInformation.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ MoreInformation " + patientId + " ############");

                        // MoreInformation
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT TobaccoActionId ,TobaccoId ,TobaccoText,TobaccoSmokingDuration,TobaccoQuitDuration,TobaccoFrequency,TobaccoDuration,AlcohoActionId ");
                        knsql.append(",AlcohoId,AlcohoText,AlcohoDrankDuration,AlcohoQuitDuration,AlcohoFrequency,AlcohoDuration,OtherDrugsActionId,OtherDrugsId,OtherDrugsText ");
                        knsql.append(",OtherDrugsUsedDuration,OtherDrugsQuitDuration,OtherDrugsFrequency,OtherDrugsDuration,ExerciseId,ExerciseText,Rest,RestId,HelpPastId,HelpPastText ");
                        knsql.append("FROM Tw_MoreInformation ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configField = new HashMap();
                        configField.put("TobaccoActionId", "tobaccoActionId");
                        configField.put("TobaccoId", "tobaccoId");
                        configField.put("TobaccoText", "tobaccoText");
                        configField.put("TobaccoSmokingDuration", "tobaccoSmokingDuration");
                        configField.put("TobaccoQuitDuration", "tobaccoQuitDuration");
                        configField.put("TobaccoFrequency", "tobaccoFrequency");
                        configField.put("TobaccoDuration", "tobaccoDuration");
                        configField.put("AlcohoActionId", "alcohoActionId");
                        configField.put("AlcohoId", "alcohoId");
                        configField.put("AlcohoText", "alcohoText");
                        configField.put("AlcohoDrankDuration", "alcohoDrankDuration");
                        configField.put("AlcohoQuitDuration", "alcohoQuitDuration");
                        configField.put("AlcohoFrequency", "alcohoFrequency");
                        configField.put("AlcohoDuration", "alcohoDuration");
                        configField.put("OtherDrugsActionId", "otherDrugsActionId");
                        configField.put("OtherDrugsId", "otherDrugsId");
                        configField.put("OtherDrugsText", "otherDrugsText");
                        configField.put("OtherDrugsUsedDuration", "otherDrugsUsedDuration");
                        configField.put("OtherDrugsQuitDuration", "otherDrugsQuitDuration");
                        configField.put("OtherDrugsFrequency", "otherDrugsFrequency");
                        configField.put("OtherDrugsDuration", "otherDrugsDuration");
                        configField.put("ExerciseId", "exerciseId");
                        configField.put("ExerciseText", "exerciseText");
                        configField.put("Rest", "rest");
                        configField.put("RestId", "restId");
                        configField.put("HelpPastId", "helpPastId");
                        configField.put("HelpPastText", "helpPastText");
                        org.json.JSONObject jsonMI = new org.json.JSONObject(su.requestData(connection, knsql, false, configField).toString());
                        result += jsonMI.length();

                        body.put("body", jsonMI);
                    } else if (Nutrition.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Nutrition " + patientId + " ############");

                        // Nutrition
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT FeedIsOral,OralIsSelf,OralIsAssisted,FeedIsTubeOther,TubeIsNGOG,TubeIsGastrostomy,TubeIsParenteralNutrition ");
                        knsql.append(",DietId,NPO,SDIsDM,SDIsLowNa,SDIsLowProt,SDIsHighProt,SDIsOther,SDOtherText,SwollowId,AppetiteId,GIIsNONE ");
                        knsql.append(",GIIsNausea,GIIsVomiting,GIIsOther,GIOtherText,ScreeningWeightLoss,ScreeningDecreasedNutritional,ScreeningStandardBMI,ScreeningCritically ");
                        knsql.append("FROM Tw_Nutrition ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldN = new HashMap();
                        configFieldN.put("FeedIsOral", "feedIsOral");
                        configFieldN.put("OralIsSelf", "oralIsSelf");
                        configFieldN.put("OralIsAssisted", "oralIsAssisted");
                        configFieldN.put("FeedIsTubeOther", "feedIsTubeOther");
                        configFieldN.put("TubeIsNGOG", "tubeIsNGOG");
                        configFieldN.put("TubeIsGastrostomy", "tubeIsGastrostomy");
                        configFieldN.put("TubeIsParenteralNutrition", "tubeIsParenteralNutrition");
                        configFieldN.put("DietId", "dietId");
                        configFieldN.put("NPO", "npo");
                        configFieldN.put("SDIsDM", "sdIsDM");
                        configFieldN.put("SDIsLowNa", "sdIsLowNa");
                        configFieldN.put("SDIsLowProt", "sdIsLowProt");
                        configFieldN.put("SDIsHighProt", "sdIsHighProt");
                        configFieldN.put("SDIsOther", "sdIsOther");
                        configFieldN.put("SDOtherText", "sdOtherText");
                        configFieldN.put("SwollowId", "swollowId");
                        configFieldN.put("AppetiteId", "appetiteId");
                        configFieldN.put("GIIsNONE", "giIsNONE");
                        configFieldN.put("GIIsNausea", "giIsNausea");
                        configFieldN.put("GIIsVomiting", "giIsVomiting");
                        configFieldN.put("GIIsOther", "giIsOther");
                        configFieldN.put("GIOtherText", "giOtherText");
                        configFieldN.put("ScreeningWeightLoss", "swl");
                        configFieldN.put("ScreeningDecreasedNutritional", "sdn");
                        configFieldN.put("ScreeningStandardBMI", "ssBMI");
                        configFieldN.put("ScreeningCritically", "sc");

                        org.json.JSONObject jsonN = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldN).toString());
                        result += jsonN.length();

                        body.put("body", jsonN);
                    } else if (Skin.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Skin " + patientId + " ############");

                        // Skin
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT DermalAssessmentId,SkinColorId,CyanosisText,TempId,MoistureId,TurgorId ");
                        knsql.append("FROM Tw_Skin ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldS = new HashMap();
                        configFieldS.put("DermalAssessmentId", "dermalAssessmentId");
                        configFieldS.put("SkinColorId", "skinColorId");
                        configFieldS.put("CyanosisText", "cyanosisText");
                        configFieldS.put("TempId", "tempId");
                        configFieldS.put("MoistureId", "moistureId");
                        configFieldS.put("TurgorId", "turgorId");
                        org.json.JSONObject jsonS = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldS).toString());
                        result += jsonS.length();

                        // AbnormalSkin
                        knsql = new KnSQL(this);
                        knsql.append("SELECT AbnormalSkinId,PUStageId,Location,CmGrade ");
                        knsql.append("FROM Tw_SkinAbnormal ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldAS = new HashMap();
                        configFieldAS.put("AbnormalSkinId", "abnormalSkinId");
                        configFieldAS.put("PUStageId", "puStageId");
                        configFieldAS.put("Location", "location");
                        configFieldAS.put("CmGrade", "cmGrade");
                        org.json.JSONArray jsonAS = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldAS).toString());
                        result += jsonAS.length();
                        jsonS.put("abnormal", jsonAS);

                        body.put("body", jsonS);
                    } else if (Cadiopulmonary.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Cadiopulmonary " + patientId + " ############");

                        // Cadiopulmonary
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT RateId,RythmId,DepthId,EffortId,EffortText,CoughId,CoughText,SputumId,SputumColorId,SputumText,TrackId ");
                        knsql.append(",TrackText,OxygenTherapyId,OxygenTherapyText,ADIsVentilator,ADIsChestTube,ADIsAssistedDeviceOther,ADOtherText ");
                        knsql.append(",PulseRhythmId,PAIsStrong,PAIsWeak,PAWeakText,PAIsAbsent,PAAbsentText,PulseRateId,NeckVeinEngorgedId,EdemaId ");
                        knsql.append(",EdemaText,ChestPainId,CPLocation,CPReferredPain,CPDuration,CPFrequency,CPFrequencyId ");
                        knsql.append("FROM Tw_Cadiopulmonary ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldC = new HashMap();
                        configFieldC.put("RateId", "rateId");
                        configFieldC.put("RythmId", "rythmId");
                        configFieldC.put("DepthId", "depthId");
                        configFieldC.put("EffortId", "effortId");
                        configFieldC.put("EffortText", "effortText");
                        configFieldC.put("CoughId", "coughId");
                        configFieldC.put("CoughText", "coughText");
                        configFieldC.put("SputumId", "sputumId");
                        configFieldC.put("SputumColorId", "sputumColorId");
                        configFieldC.put("SputumText", "sputumText");
                        configFieldC.put("TrackId", "trackId");
                        configFieldC.put("TrackText", "trackText");
                        configFieldC.put("OxygenTherapyId", "oxygenTherapyId");
                        configFieldC.put("OxygenTherapyText", "oxygenTherapyText");
                        configFieldC.put("ADIsVentilator", "adIsVentilator");
                        configFieldC.put("ADIsChestTube", "adIsChestTube");
                        configFieldC.put("ADIsAssistedDeviceOther", "adIsAssistedDeviceOther");
                        configFieldC.put("ADOtherText", "adOtherText");
                        configFieldC.put("PulseRhythmId", "pulseRhythmId");
                        configFieldC.put("PAIsStrong", "paIsStrong");
                        configFieldC.put("PAIsWeak", "paIsWeak");
                        configFieldC.put("PAWeakText", "paWeakText");
                        configFieldC.put("PAIsAbsent", "paIsAbsent");
                        configFieldC.put("PAAbsentText", "paAbsentText");
                        configFieldC.put("PulseRateId", "pulseRateId");
                        configFieldC.put("NeckVeinEngorgedId", "neckVeinEngorgedId");
                        configFieldC.put("EdemaId", "edemaId");
                        configFieldC.put("EdemaText", "edemaText");
                        configFieldC.put("ChestPainId", "chestPainId");
                        configFieldC.put("CPLocation", "cpLocation");
                        configFieldC.put("CPReferredPain", "cpReferredPain");
                        configFieldC.put("CPDuration", "cpDuration");
                        configFieldC.put("CPFrequency", "cpFrequency");
                        configFieldC.put("CPFrequencyId", "cpFrequencyId");
                        org.json.JSONObject jsonC = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldC).toString());
                        result += jsonC.length();

                        body.put("body", jsonC);
                    } else if (Neuromuscular.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Neuromuscular " + patientId + " ############");

                        // Neuromuscular
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT LOCId,VisionId,VisionRtId,VisionRtText,VisionLtId,VisionLtText,VisionDeviceId,VisionDeviceText,HearingId ");
                        knsql.append(",HearingRtId,HearingRtText,HearingLtId,HearingLtText,HearingDeviceId,HearingDeviceText,SmellId,SmellText,SpeechId ");
                        knsql.append(",SpeechImpairedId,SpeechText,SenIsNA,SenIsNormal,SenIsNumbness,SenNumbnessText,SenIsTingling,SenTinglingText ");
                        knsql.append(",HandGraspsId,HGIsWeak,HGIsWeakRt,HGIsWeakLt,HGIsAbsent,HGIsAbsentRt,HGIsAbsentLt,JointId,JointIsSwollen,JointSwollenText ");
                        knsql.append(",JointIsStiff,JointStiffText,JointIsTender,JointTenderText,JointIsOther,JointOtherText,WeaknessId,MotorPowerLU ");
                        knsql.append(",MotorPowerRU,MotorPowerLB,MotorPowerRB,ParalysisId,ParalysisText,SeizureId,IsGeneralized,GeneralizedText,IsLocalized ");
                        knsql.append(",LocalizedText,MovementId,MovementText ");
                        knsql.append("FROM Tw_Neuromuscular ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldN = new HashMap();
                        configFieldN.put("LOCId", "locId");
                        configFieldN.put("VisionId", "visionId");
                        configFieldN.put("VisionRtId", "visionRtId");
                        configFieldN.put("VisionRtText", "visionRtText");
                        configFieldN.put("VisionLtId", "visionLtId");
                        configFieldN.put("VisionLtText", "visionLtText");
                        configFieldN.put("VisionDeviceId", "visionDeviceId");
                        configFieldN.put("VisionDeviceText", "visionDeviceText");
                        configFieldN.put("HearingId", "hearingId");
                        configFieldN.put("HearingRtId", "hearingRtId");
                        configFieldN.put("HearingRtText", "hearingRtText");
                        configFieldN.put("HearingLtId", "hearingLtId");
                        configFieldN.put("HearingLtText", "hearingLtText");
                        configFieldN.put("HearingDeviceId", "hearingDeviceId");
                        configFieldN.put("HearingDeviceText", "hearingDeviceText");
                        configFieldN.put("SmellId", "smellId");
                        configFieldN.put("SmellText", "smellText");
                        configFieldN.put("SpeechId", "speechId");
                        configFieldN.put("SpeechImpairedId", "speechImpairedId");
                        configFieldN.put("SpeechText", "speechText");
                        configFieldN.put("SenIsNA", "senIsNA");
                        configFieldN.put("SenIsNormal", "senIsNormal");
                        configFieldN.put("SenIsNumbness", "senIsNumbness");
                        configFieldN.put("SenNumbnessText", "senNumbnessText");
                        configFieldN.put("SenIsTingling", "senIsTingling");
                        configFieldN.put("SenTinglingText", "senTinglingText");
                        configFieldN.put("HandGraspsId", "handGraspsId");
                        configFieldN.put("HGIsWeak", "hgIsWeak");
                        configFieldN.put("HGIsWeakRt", "hgIsWeakRt");
                        configFieldN.put("HGIsWeakLt", "hgIsWeakLt");
                        configFieldN.put("HGIsAbsent", "hgIsAbsent");
                        configFieldN.put("HGIsAbsentRt", "hgIsAbsentRt");
                        configFieldN.put("HGIsAbsentLt", "hgIsAbsentLt");
                        configFieldN.put("JointId", "jointId");
                        configFieldN.put("JointIsSwollen", "jointIsSwollen");
                        configFieldN.put("JointSwollenText", "jointSwollenText");
                        configFieldN.put("JointIsStiff", "jointIsStiff");
                        configFieldN.put("JointStiffText", "jointStiffText");
                        configFieldN.put("JointIsTender", "jointIsTender");
                        configFieldN.put("JointTenderText", "jointTenderText");
                        configFieldN.put("JointIsOther", "jointIsOther");
                        configFieldN.put("JointOtherText", "jointOtherText");
                        configFieldN.put("WeaknessId", "weaknessId");
                        configFieldN.put("MotorPowerLU", "motorPowerLU");
                        configFieldN.put("MotorPowerRU", "motorPowerRU");
                        configFieldN.put("MotorPowerLB", "motorPowerLB");
                        configFieldN.put("MotorPowerRB", "motorPowerRB");
                        configFieldN.put("ParalysisId", "paralysisId");
                        configFieldN.put("ParalysisText", "paralysisText");
                        configFieldN.put("SeizureId", "seizureId");
                        configFieldN.put("IsGeneralized", "isGeneralized");
                        configFieldN.put("GeneralizedText", "generalizedText");
                        configFieldN.put("IsLocalized", "isLocalized");
                        configFieldN.put("LocalizedText", "localizedText");
                        configFieldN.put("MovementId", "movementId");
                        configFieldN.put("MovementText", "movementText");
                        org.json.JSONObject jsonN = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldN).toString());
                        result += jsonN.length();

                        body.put("body", jsonN);
                    } else if (Mobility.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Mobility " + patientId + " ############");

                        // Mobility
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT ActivityId,RAIsEating,RAIsDressing,RAIsToileting,RAIsBedMobility,RAIsTransferring,RAIsAmbulation ");
                        knsql.append(",RAIsOther,RAOtherText,AssistDeviceId,ADIsCane,ADIsWalker,ADIsWheelChair,ADIsArtificialLimb,ADIsOther,ADOtherText ");
                        knsql.append("FROM Tw_Mobility ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldM = new HashMap();
                        configFieldM.put("ActivityId", "activityId");
                        configFieldM.put("RAIsEating", "raIsEating");
                        configFieldM.put("RAIsDressing", "raIsDressing");
                        configFieldM.put("RAIsToileting", "raIsToileting");
                        configFieldM.put("RAIsBedMobility", "raIsBedMobility");
                        configFieldM.put("RAIsTransferring", "raIsTransferring");
                        configFieldM.put("RAIsAmbulation", "raIsAmbulation");
                        configFieldM.put("RAIsOther", "raIsOther");
                        configFieldM.put("RAOtherText", "raOtherText");
                        configFieldM.put("AssistDeviceId", "assistDeviceId");
                        configFieldM.put("ADIsCane", "adIsCane");
                        configFieldM.put("ADIsWalker", "adIsWalker");
                        configFieldM.put("ADIsWheelChair", "adIsWheelChair");
                        configFieldM.put("ADIsArtificialLimb", "adIsArtificialLimb");
                        configFieldM.put("ADIsOther", "adIsOther");
                        configFieldM.put("ADOtherText", "adOtherText");
                        org.json.JSONObject jsonM = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldM).toString());
                        result += jsonM.length();

                        body.put("body", jsonM);
                    } else if (Safety.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Safety " + patientId + " ############");

                        // Safety
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT FallHistoryId,FallHistoryDate,FallRiskFactorId,IsNoRisk,IsPoorBalance,IsHistoryFall,IsConvulsiveSeizure ");
                        knsql.append(",IsDrugsDrowsy,IsPsychiatricSymptoms,IsVisualSymptoms,IsExcretorySystem,IsSevereConditions ");
                        knsql.append("FROM Tw_Safety ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldS = new HashMap();
                        configFieldS.put("FallHistoryId", "fallHistoryId");
                        configFieldS.put("FallHistoryDate", "fallHistoryDate");
                        configFieldS.put("FallRiskFactorId", "fallRiskFactorId");
                        configFieldS.put("IsNoRisk", "isNoRisk");
                        configFieldS.put("IsPoorBalance", "isPoorBalance");
                        configFieldS.put("IsHistoryFall", "isHistoryFall");
                        configFieldS.put("IsConvulsiveSeizure", "isConvulsiveSeizure");
                        configFieldS.put("IsDrugsDrowsy", "isDrugsDrowsy");
                        configFieldS.put("IsPsychiatricSymptoms", "isPsychiatricSymptoms");
                        configFieldS.put("IsVisualSymptoms", "isVisualSymptoms");
                        configFieldS.put("IsExcretorySystem", "isExcretorySystem");
                        configFieldS.put("IsSevereConditions", "isSevereConditions");
                        org.json.JSONObject jsonS = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldS).toString());
                        result += jsonS.length();

                        body.put("body", jsonS);
                    } else if (Elimination.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Elimination " + patientId + " ############");

                        // Elimination
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT OCIsMoist,OCIsDry,OCIsAbrasion,OCIsTumor,OCIsDenture,OCIsOther,OCOtherText,AbdomenId,AbdomenText,BowelTime ");
                        knsql.append(",BowelDay,EliminationId,EliminationText,BladderId,BladderText,VoidingDay,VoidingNight,VoidingId,CatheterId ");
                        knsql.append(",CatheterText,UrineId,UrineText,GenitalOrganId,GenitalOrganText,BreastId,BreastText,MenstrualId,MenstrualText,CONVERT(NVARCHAR,LMP) as LMP ");
                        knsql.append("FROM Tw_Elimination ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldE = new HashMap();
                        configFieldE.put("OCIsMoist", "ocIsMoist");
                        configFieldE.put("OCIsDry", "ocIsDry");
                        configFieldE.put("OCIsAbrasion", "ocIsAbrasion");
                        configFieldE.put("OCIsTumor", "ocIsTumor");
                        configFieldE.put("OCIsDenture", "ocIsDenture");
                        configFieldE.put("OCIsOther", "ocIsOther");
                        configFieldE.put("OCOtherText", "ocOtherText");
                        configFieldE.put("AbdomenId", "abdomenId");
                        configFieldE.put("AbdomenText", "abdomenText");
                        configFieldE.put("BowelTime", "bowelTime");
                        configFieldE.put("BowelDay", "bowelDay");
                        configFieldE.put("EliminationId", "eliminationId");
                        configFieldE.put("EliminationText", "eliminationText");
                        configFieldE.put("BladderId", "bladderId");
                        configFieldE.put("BladderText", "bladderText");
                        configFieldE.put("VoidingDay", "voidingDay");
                        configFieldE.put("VoidingNight", "voidingNight");
                        configFieldE.put("VoidingId", "voidingId");
                        configFieldE.put("CatheterId", "catheterId");
                        configFieldE.put("CatheterText", "catheterText");
                        configFieldE.put("UrineId", "urineId");
                        configFieldE.put("UrineText", "urineText");
                        configFieldE.put("GenitalOrganId", "genitalOrganId");
                        configFieldE.put("GenitalOrganText", "genitalOrganText");
                        configFieldE.put("BreastId", "breastId");
                        configFieldE.put("BreastText", "breastText");
                        configFieldE.put("MenstrualId", "menstrualId");
                        configFieldE.put("MenstrualText", "menstrualText");
                        configFieldE.put("LMP", "lmp");
                        org.json.JSONObject jsonE = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldE).toString());
                        result += jsonE.length();

                        body.put("body", jsonE);
                    } else if (PainManagement.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ PainManagement " + patientId + " ############");

                        // PainManagement
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT PainScaleId,PainLocation,PainWhen,PainCauses,PatternId,PatternText,PainDescId,PainDescText ");
                        knsql.append(",PainToolId,IntensityId,PainAffectId,PainRelievesId,RelievesPainText ");
                        knsql.append("FROM Tw_PainManagement ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldPM = new HashMap();
                        configFieldPM.put("PainScaleId", "painScaleId");
                        configFieldPM.put("PainLocation", "painLocation");
                        configFieldPM.put("PainWhen", "painWhen");
                        configFieldPM.put("PainCauses", "painCauses");
                        configFieldPM.put("PatternId", "patternId");
                        configFieldPM.put("PatternText", "patternText");
                        configFieldPM.put("PainDescId", "painDescId");
                        configFieldPM.put("PainDescText", "painDescText");
                        configFieldPM.put("PainToolId", "painToolId");
                        configFieldPM.put("IntensityId", "intensityId");
                        configFieldPM.put("PainAffectId", "painAffectId");
                        configFieldPM.put("PainRelievesId", "painRelievesId");
                        configFieldPM.put("RelievesPainText", "relievesPainText");
                        org.json.JSONObject jsonPM = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldPM).toString());
                        result += jsonPM.length();

                        body.put("body", jsonPM);
                    } else if (Spiritual.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ Spiritual " + patientId + " ############");

                        // Spiritual
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT ReligionId,ReligionText,SpecialReligiousId,SpecialReligiousText,PalliativeCareId,PalliativeCareText ");
                        knsql.append(",AnxietyIsNone,AnxietyIsIllness,AnxietyIsFamily,AnxietyIsFinance,AnxietyIsOther,AnxietyOtherText,SupportIsNone ");
                        knsql.append(",SupportIsParents,SupportIsFamily,SupportIsFriends,SupportIsOther,SupportOtherText ");
                        knsql.append("FROM Tw_Spiritual ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldS = new HashMap();
                        configFieldS.put("ReligionId", "religionId");
                        configFieldS.put("ReligionText", "religionText");
                        configFieldS.put("SpecialReligiousId", "specialReligiousId");
                        configFieldS.put("SpecialReligiousText", "specialReligiousText");
                        configFieldS.put("PalliativeCareId", "palliativeCareId");
                        configFieldS.put("PalliativeCareText", "palliativeCareText");
                        configFieldS.put("AnxietyIsNone", "anxietyIsNone");
                        configFieldS.put("AnxietyIsIllness", "anxietyIsIllness");
                        configFieldS.put("AnxietyIsFamily", "anxietyIsFamily");
                        configFieldS.put("AnxietyIsFinance", "anxietyIsFinance");
                        configFieldS.put("AnxietyIsOther", "anxietyIsOther");
                        configFieldS.put("AnxietyOtherText", "anxietyOtherText");
                        configFieldS.put("SupportIsNone", "supportIsNone");
                        configFieldS.put("SupportIsParents", "supportIsParents");
                        configFieldS.put("SupportIsFamily", "supportIsFamily");
                        configFieldS.put("SupportIsFriends", "supportIsFriends");
                        configFieldS.put("SupportIsOther", "supportIsOther");
                        configFieldS.put("SupportOtherText", "supportOtherText");
                        org.json.JSONObject jsonS = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldS).toString());
                        result += jsonS.length();

                        body.put("body", jsonS);
                    } else if (LNHospital.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ LNHospital " + patientId + " ############");

                        // LNHospital
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT IsNone,IsPain,IsMotivation,IsLanguageBarriers,IsReligiousPractices,IsCultural,IsCongnetive,IsPsychosocial ");
                        knsql.append(",IsHVS,LNId,IsActivity,IsDiseaseProcess,IsMedication,IsSafety,IsWoundOstomy,IsDietChanges,IsSelfCare,IsOpTeach ");
                        knsql.append(",IsInfectionControl,IsProcessTreatment,IsSigns,IsEquipment,EquipmentText,IsOther,OtherText ");
                        knsql.append("FROM Tw_Information ");
                        knsql.append("LEFT JOIN Tw_LearningNeeds ON Tw_Information.LNHospitalId = Tw_LearningNeeds.LNId ");
                        knsql.append("WHERE Tw_Information.PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldLN = new HashMap();
                        configFieldLN.put("IsNone", "isNone");
                        configFieldLN.put("IsPain", "isPain");
                        configFieldLN.put("IsMotivation", "isMotivation");
                        configFieldLN.put("IsLanguageBarriers", "isLanguageBarriers");
                        configFieldLN.put("IsReligiousPractices", "isReligiousPractices");
                        configFieldLN.put("IsCultural", "isCultural");
                        configFieldLN.put("IsCongnetive", "isCongnetive");
                        configFieldLN.put("IsPsychosocial", "isPsychosocial");
                        configFieldLN.put("IsHVS", "isHVS");
                        configFieldLN.put("LNId", "lnId");
                        configFieldLN.put("IsActivity", "lnIsActivity");
                        configFieldLN.put("IsDiseaseProcess", "lnIsDiseaseProcess");
                        configFieldLN.put("IsMedication", "lnIsMedication");
                        configFieldLN.put("IsSafety", "lnIsSafety");
                        configFieldLN.put("IsWoundOstomy", "lnIsWoundOstomy");
                        configFieldLN.put("IsDietChanges", "lnIsDietChanges");
                        configFieldLN.put("IsSelfCare", "lnIsSelfCare");
                        configFieldLN.put("IsOpTeach", "lnIsOpTeach");
                        configFieldLN.put("IsInfectionControl", "lnIsInfectionControl");
                        configFieldLN.put("IsProcessTreatment", "lnIsProcessTreatment");
                        configFieldLN.put("IsSigns", "lnIsSigns");
                        configFieldLN.put("IsEquipment", "lnIsEquipment");
                        configFieldLN.put("EquipmentText", "lnEquipmentText");
                        configFieldLN.put("IsOther", "lnIsOther");
                        configFieldLN.put("OtherText", "lnOtherText");
                        org.json.JSONObject jsonLN = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldLN).toString());
                        result += jsonLN.length();

                        body.put("body", jsonLN);
                    } else if (DischargePlanning.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ DischargePlanning " + patientId + " ############");

                        // DischargePlanning
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT NeedCriteria,NeedDischargeAssistance,NeedDischargePlanning,Caregiver,HaveFamilyCapable,FamilyCapableId,FamilyCapableText ");
                        knsql.append(",NeedAssistance,EquipmentId,EquipmentText,Equipment,Environment,Financial,LivesWithId,LivesWithText,LivesWhereId ");
                        knsql.append(",LivesWhereText,DPNIsMedication,DPNIsMeeting,DPNIsAbnormalSymptoms,DPNIsEmergencyAmbulance,DPNIsControlRiskFactor,DPNIsOther,DPNOtherText ");
                        knsql.append("FROM Tw_DischargePlanning ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldDP = new HashMap();
                        configFieldDP.put("NeedCriteria", "needCriteria");
                        configFieldDP.put("NeedDischargeAssistance", "needDischargeAssistance");
                        configFieldDP.put("NeedDischargePlanning", "needDischargePlanning");
                        configFieldDP.put("Caregiver", "caregiver");
                        configFieldDP.put("HaveFamilyCapable", "haveFamilyCapable");
                        configFieldDP.put("FamilyCapableId", "familyCapableId");
                        configFieldDP.put("FamilyCapableText", "familyCapableText");
                        configFieldDP.put("NeedAssistance", "needAssistance");
                        configFieldDP.put("EquipmentId", "equipmentId");
                        configFieldDP.put("EquipmentText", "equipmentText");
                        configFieldDP.put("Equipment", "equipment");
                        configFieldDP.put("Environment", "environment");
                        configFieldDP.put("Financial", "financial");
                        configFieldDP.put("LivesWithId", "livesWithId");
                        configFieldDP.put("LivesWithText", "livesWithText");
                        configFieldDP.put("LivesWhereId", "livesWhereId");
                        configFieldDP.put("LivesWhereText", "livesWhereText");
                        configFieldDP.put("DPNIsMedication", "dpnIsMedication");
                        configFieldDP.put("DPNIsMeeting", "dpnIsMeeting");
                        configFieldDP.put("DPNIsAbnormalSymptoms", "dpnIsAbnormalSymptoms");
                        configFieldDP.put("DPNIsEmergencyAmbulance", "dpnIsEmergencyAmbulance");
                        configFieldDP.put("DPNIsControlRiskFactor", "dpnIsControlRiskFactor");
                        configFieldDP.put("DPNIsOther", "dpnIsOther");
                        configFieldDP.put("DPNOtherText", "dpnOtherText");
                        org.json.JSONObject jsonDP = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldDP).toString());
                        result += jsonDP.length();

                        body.put("body", jsonDP);

                    } else if (ContinuingCare.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ ContinuingCare " + patientId + " ############");

                        // ContinuingCare
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT StatusDCId,TransferId,TransferText,DischargeId,NeedConstantCareId,ConstantCareId,PRNIsWoundCare,PRNIsRehabilitation ");
                        knsql.append(",PRNIsPsychologist,PRNIsSocialService,PRNIsSpeech,PRNIsOT,PRNIsOther,PRNOtherText ");
                        knsql.append("FROM Tw_ContinuingCare ");
                        knsql.append("WHERE PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldCC = new HashMap();
                        configFieldCC.put("StatusDCId", "statusDCId");
                        configFieldCC.put("TransferId", "transferId");
                        configFieldCC.put("TransferText", "transferText");
                        configFieldCC.put("DischargeId", "dischargeId");
                        configFieldCC.put("NeedConstantCareId", "needConstantCareId");
                        configFieldCC.put("ConstantCareId", "constantCareId");
                        configFieldCC.put("PRNIsWoundCare", "prnIsWoundCare");
                        configFieldCC.put("PRNIsRehabilitation", "prnIsRehabilitation");
                        configFieldCC.put("PRNIsPsychologist", "prnIsPsychologist");
                        configFieldCC.put("PRNIsSocialService", "prnIsSocialService");
                        configFieldCC.put("PRNIsSpeech", "prnIsSpeech");
                        configFieldCC.put("PRNIsOT", "prnIsOT");
                        configFieldCC.put("PRNIsOther", "prnIsOther");
                        configFieldCC.put("PRNOtherText", "prnOtherText");
                        org.json.JSONObject jsonCC = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldCC).toString());
                        result += jsonCC.length();

                        body.put("body", jsonCC);

                    } else if (LNDischargs.equals(fs_apiName)) {
                        String patientId = json.getString("patientId");
                        Trace.info("############ LNDischargs " + patientId + " ############");

                        // LNDischargs
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT LNId,IsActivity,IsDiseaseProcess,IsMedication,IsSafety,IsWoundOstomy,IsDietChanges,IsSelfCare,IsOpTeach ");
                        knsql.append(",IsInfectionControl,IsProcessTreatment,IsSigns,IsEquipment,EquipmentText,IsOther,OtherText ");
                        knsql.append("FROM Tw_Information ");
                        knsql.append("LEFT JOIN Tw_LearningNeeds ON Tw_Information.LNDischargsId = Tw_LearningNeeds.LNId ");
                        knsql.append("WHERE Tw_Information.PatientId = ?patientId ");
                        knsql.setParameter("patientId", patientId);
                        HashMap<String, String> configFieldLN = new HashMap();
                        configFieldLN.put("LNId", "lnId");
                        configFieldLN.put("IsActivity", "lnIsActivity");
                        configFieldLN.put("IsDiseaseProcess", "lnIsDiseaseProcess");
                        configFieldLN.put("IsMedication", "lnIsMedication");
                        configFieldLN.put("IsSafety", "lnIsSafety");
                        configFieldLN.put("IsWoundOstomy", "lnIsWoundOstomy");
                        configFieldLN.put("IsDietChanges", "lnIsDietChanges");
                        configFieldLN.put("IsSelfCare", "lnIsSelfCare");
                        configFieldLN.put("IsOpTeach", "lnIsOpTeach");
                        configFieldLN.put("IsInfectionControl", "lnIsInfectionControl");
                        configFieldLN.put("IsProcessTreatment", "lnIsProcessTreatment");
                        configFieldLN.put("IsSigns", "lnIsSigns");
                        configFieldLN.put("IsEquipment", "lnIsEquipment");
                        configFieldLN.put("EquipmentText", "lnEquipmentText");
                        configFieldLN.put("IsOther", "lnIsOther");
                        configFieldLN.put("OtherText", "lnOtherText");
                        org.json.JSONObject jsonLN = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldLN).toString());
                        result += jsonLN.length();

                        body.put("body", jsonLN);
                    }

                    Trace.info("body : " + body);
                    return result;

                }
            };

            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body.get("body"));
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
