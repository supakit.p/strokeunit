<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.smart.aps.*"%>
<%@ page import="com.fs.dev.smart.sms.ais.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.glassfish.jersey.uri.UriComponent"%>
<%@ page import="javax.ws.rs.core.MultivaluedMap"%>
<%
//create bean in order to obtain parameter from request or rest injection
com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
fsBean.addSchema("TRANSID",java.sql.Types.VARCHAR,"TRANSID");
fsBean.addSchema("CMD",java.sql.Types.VARCHAR,"CMD");
fsBean.addSchema("FET",java.sql.Types.VARCHAR,"FET");
fsBean.addSchema("NTYPE",java.sql.Types.VARCHAR,"NTYPE");
fsBean.addSchema("CTYPE",java.sql.Types.VARCHAR,"CTYPE");
fsBean.addSchema("FROM",java.sql.Types.VARCHAR,"FROM");
fsBean.addSchema("TO",java.sql.Types.VARCHAR,"TO");
fsBean.addSchema("CODE",java.sql.Types.VARCHAR,"CODE");
fsBean.addSchema("CONTENT",java.sql.Types.VARCHAR,"CONTENT");
fsBean.obtainFrom(request); //assign variable from request
java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
if(fs_map!=null) fsBean.obtain(fs_map); //assign variable from rest
fsBean.obtain(session,request);
Trace.debug(this,fsBean);
Trace.debug(this,"+++++++++++++++++++++++++++++++++++");
StringBuilder buf = new StringBuilder();
java.util.Enumeration en = request.getParameterNames();
for(;en.hasMoreElements();) {
	String key = (String)en.nextElement();
	String value = request.getParameter(key);
	Trace.debug(this,key+"="+value);
	if(buf.length()>0) buf.append("&");
	buf.append(key+"="+value);
}
Trace.debug(this,"+++++++++++++++++++++++++++++++++++");
Trace.debug(this,">>> parameters = "+buf.length());
if(buf.length()<=0) {
	java.io.BufferedReader br = request.getReader();
	String line = br.readLine();
	while(line!=null) {
		buf.append(line);
		Trace.debug(this,">>> "+line);
		line = br.readLine();
	}
	if(buf.length()>0) {
		MultivaluedMap multimap = UriComponent.decodeQuery(buf.toString(), true);
		Trace.debug(this,">>> "+multimap);
		if(multimap!=null) {
			java.util.Map map = new java.util.HashMap();
			for(java.util.Iterator<String> it = multimap.keySet().iterator();it.hasNext();) {
				String key = it.next();
				map.put(key, multimap.getFirst(key));
			}
			fsBean.obtain(map);			
			Trace.debug(this,fsBean);
		}
	}
}
String fs_transid = fsBean.getString("TRANSID");
String fs_from = fsBean.getString("FROM");
String fs_to = fsBean.getString("TO");
String fs_content = fsBean.getString("CONTENT");
if(fs_transid!=null && fs_transid.trim().length()>0) {
	try {
		SmartContentLogger log = new SmartContentLogger("trxres");
		log.setTime(System.currentTimeMillis());
		log.setPackage(fs_transid);
		log.setContent(buf.toString());
		log.setProcess("sms");
		log.setSender("ais");
		log.setAction("msg");
		log.setNotation(com.fs.bean.util.GlobalBean.getClientAddress(request));
		log.setStatus("C");
		log.setRemark(fs_content);
		log.setCaller(fs_from);
		log.setOwner(fs_to);
		SmartTracker.track(log);
	}catch(Throwable ex) { Trace.error(this,ex); }
}
StringBuilder sb = new StringBuilder();
sb.append("<XML>");
sb.append("<STATUS>OK</STATUS>");
sb.append("<DETAIL></DETAIL>");
sb.append("</XML>");
%>
<%=sb.toString()%>