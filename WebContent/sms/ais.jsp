<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%
System.out.println("+++++++++++++++++++++++++++++++++++");
java.util.Enumeration en = request.getParameterNames();
for(;en.hasMoreElements();) {
	String key = (String)en.nextElement();
	String value = request.getParameter(key);
	System.out.println(key+"="+value);
}
System.out.println("+++++++++++++++++++++++++++++++++++");
StringBuilder sb = new StringBuilder();
sb.append("<XML>");
sb.append("<STATUS>OK</STATUS>");
sb.append("<DETAIL></DETAIL>");
sb.append("<SMID>"+System.currentTimeMillis()+"</SMID>");
sb.append("</XML>");
%>
<%=sb.toString()%>