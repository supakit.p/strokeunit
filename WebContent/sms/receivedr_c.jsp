<%@page import="org.apache.commons.httpclient.util.ParameterParser"%>
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.smart.aps.*"%>
<%@ page import="com.fs.dev.smart.sms.ais.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.glassfish.jersey.uri.UriComponent"%>
<%@ page import="javax.ws.rs.core.MultivaluedMap"%>
<%
//create bean in order to obtain parameter from request or rest injection
com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
fsBean.addSchema("CMD",java.sql.Types.VARCHAR,"CMD");
fsBean.addSchema("NTYPE",java.sql.Types.VARCHAR,"NTYPE");
fsBean.addSchema("FROM",java.sql.Types.VARCHAR,"FROM");
fsBean.addSchema("SMID",java.sql.Types.VARCHAR,"SMID");
fsBean.addSchema("STATUS",java.sql.Types.VARCHAR,"STATUS");
fsBean.addSchema("DETAIL",java.sql.Types.VARCHAR,"DETAIL");
fsBean.obtainFrom(request); //assign variable from request
java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
if(fs_map!=null) fsBean.obtain(fs_map); //assign variable from rest
fsBean.obtain(session,request);
Trace.debug(this,fsBean);
Trace.debug(this,"+++++++++++++++++++++++++++++++++++");
StringBuilder buf = new StringBuilder();
java.util.Enumeration en = request.getParameterNames();
for(;en.hasMoreElements();) {
	String key = (String)en.nextElement();
	String value = request.getParameter(key);
	Trace.debug(this,key+"="+value);
	if(buf.length()>0) buf.append("&");
	buf.append(key+"="+value);
}
Trace.debug(this,"+++++++++++++++++++++++++++++++++++");
Trace.debug(this,">>> parameters = "+buf.length());
if(buf.length()<=0) {
	java.io.BufferedReader br = request.getReader();
	String line = br.readLine();
	while(line!=null) {
		buf.append(line);
		Trace.debug(this,">>> "+line);
		line = br.readLine();
	}
	if(buf.length()>0) {
		MultivaluedMap multimap = UriComponent.decodeQuery(buf.toString(), true);
		Trace.debug(this,">>> "+multimap);
		if(multimap!=null) {
			java.util.Map map = new java.util.HashMap();
			for(java.util.Iterator<String> it = multimap.keySet().iterator();it.hasNext();) {
				String key = it.next();
				map.put(key, multimap.getFirst(key));
			}
			fsBean.obtain(map);			
			Trace.debug(this,fsBean);
		}
	}
}
String fs_smid = fsBean.getString("SMID");
String fs_status = fsBean.getString("STATUS");
String fs_from = fsBean.getString("FROM");
String fs_detail = fsBean.getString("DETAIL");
if(fs_smid!=null && fs_smid.trim().length()>0) {
	try {
		SmartContentLogger log = new SmartContentLogger("trxres");
		log.setTime(System.currentTimeMillis());
		log.setPackage(fs_smid);
		log.setContent(buf.toString());
		log.setProcess("sms");
		log.setSender("ais");
		log.setAction("res");
		log.setNotation(com.fs.bean.util.GlobalBean.getClientAddress(request));
		log.setStatus("C");
		boolean isFailure = !"OK".equalsIgnoreCase(fs_status);
		if(isFailure) {
			log.setStatus("N");
		}
		log.setRemark(fs_detail);
		log.setCaller(fs_from);
		if(fs_from!=null) {
			int len = fs_from.length();
			if(len>9) {
				String owner = fs_from.substring(len-9);
				log.setOwner("0"+owner);
			}
		}
		SmartTracker.track(log);
	}catch(Throwable ex) { Trace.error(this,ex); }
}
StringBuilder sb = new StringBuilder();
sb.append("<XML>");
sb.append("<STATUS>OK</STATUS>");
sb.append("<DETAIL></DETAIL>");
sb.append("<SMID>"+(fs_smid==null?"":fs_smid)+"</SMID>");
sb.append("</XML>");
%>
<%=sb.toString()%>