<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="activate_error.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html>
<head>
<title>Activate</title>
<script>
function openlink() {
<%String fs_linker = (String)session.getAttribute("activatelink");
if(fs_linker==null) fs_linker = (String)request.getAttribute("activatelink");
if(fs_linker==null) fs_linker = request.getParameter("activatelink");
if(fs_linker!=null && fs_linker.trim().length()>0) {
out.print("window.open('"+fs_linker+"','_self');");
}%>
}
</script>
</head>
<body onload="openlink()">
</body>
</html>