<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="activate_error.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.dev.mail.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsActivateBean" scope="request" class="com.fs.bean.ActivateBean"/>
<jsp:setProperty name="fsActivateBean" property="*"/>
<jsp:useBean id="fsResetPasswordBean" scope="request" class="com.fs.dev.auth.ResetPasswordBean"/>
<c:if test="${fsScreen.config('activate', pageContext.request, pageContext.response, false, false)}"></c:if>
<%
//#import and uses wherever you will go
//#(10000) programmer code begin;
StringBuffer result = new StringBuffer();
String fs_activatemessage = null;
//#(10000) programmer code end;
//#if the condition come to me
//#(15000) programmer code begin;
//#(15000) programmer code end;
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("activate");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
String fs_forwarder = "/activate/activate_success.jsp";
try {
	fsGlobal.setFsVar("fsUser",request.getRemoteAddr());
	fsActivateBean.invalidate(fsGlobal); 
	if(fsActivateBean.effectedTransactions()>0) {
		fsActivateBean.activate(fsGlobal);
		fsResetPasswordBean.setEmail(fsActivateBean.getActivateremark());
		if(fsResetPasswordBean.getEmail().trim().length()<=0) throw new BeanException("Email address is invalid",-18896);
		fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
		fsResetPasswordBean.obtain(session,request);
		fsResetPasswordBean.transport(fsGlobal);
		System.out.println("effected transaction : "+fsResetPasswordBean.effectedTransactions());
		if(fsResetPasswordBean.effectedTransactions()>0) {
			//fs_forwarder = "/page_forgot/page_forgot_success.jsp";
			java.util.Map map = null;
			java.sql.Connection connection = null;
			try {
				connection = DBConnection.getConnection(fsGlobal.getFsSection());
				map = (java.util.Map)XMLConfig.create(request).getConfigure("FORGOTPASSWORDMAIL",connection);
			} finally { 
				if(connection!=null) { try { connection.close(); }catch(Exception ex) {} }
			}
			Trace.info("FORGOTPASSWORDMAIL = "+map);
			if(map!=null) {
				String fs_email = fsResetPasswordBean.getEmail();
				if(fs_email!=null && fs_email.trim().length()>0) {
					StringBuffer msg = new StringBuffer();
					msg.append("Dear, "+fsResetPasswordBean.getUsername()+"<br>");
					msg.append("Confirm your password was changed<br>");
					msg.append("user = "+fsResetPasswordBean.getUserid()+"<br>");
					msg.append("new password = "+fsResetPasswordBean.getPassword()+"<br>");
					msg.append("       yours sincerely,<br>");
					Trace.info("forgot password : "+fsResetPasswordBean.getUserid()+" ("+fs_email+")");
					SendMail mailer = new SendMail();
					mailer.obtain(map);
					mailer.setTo(fs_email);
					mailer.setMessage(msg.toString());
					mailer.start();
				}
			}
		}
	} else {
		throw new BeanException("Request activation not found or already activated",-18801);
	}
} catch(Throwable ex) {
	Trace.error(fsGlobal,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
	throw ex;
}
if(fsIsJSON) {
	out.print(fsActivateBean.toJSON());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsActivateBean.toXML());
	return;
}
if(fs_forwarder!=null && fs_forwarder.trim().length()>0) {
	RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
	rd.forward(request,response);
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
