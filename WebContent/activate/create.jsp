<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%!
//#it's strong enough to break into another method here
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
<%
//#scrape & keeping say something anyway
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsActivateBean" scope="request" class="com.fs.bean.ActivateBean"/>
<jsp:setProperty name="fsActivateBean" property="*"/>
<%
//#import and uses wherever you will go
//#(10000) programmer code begin;
StringBuffer result = new StringBuffer();
//#(10000) programmer code end;
//#if the condition come to me
//#(15000) programmer code begin;
//#(15000) programmer code end;
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("activate");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
String fs_forwarder = "";
try {
	BeanFormat format = new BeanFormat();		
	if(fsActivateBean.getActivateuser()==null || fsActivateBean.getActivateuser().trim().length()<=0) {
		fsActivateBean.setActivateuser(request.getRemoteAddr());
	}
	String scheme = request.getScheme();             
	String serverName = request.getServerName(); 
	int serverPort = request.getServerPort(); 
	String aurl = scheme+"://"+serverName+(serverPort==80?"":":"+serverPort)+request.getContextPath();
	Object activateurl = GlobalVariable.getVariable("ACTIVATE_URL");
	if(activateurl!=null && activateurl.toString().trim().length()>0) {
		aurl = activateurl.toString().trim();
	}
	String fs_activatekey = fsActivateBean.createActivateKey();
	String fs_activatelink = aurl+"/activate/activate.jsp?activatekey="+fs_activatekey;
	fsActivateBean.setActivatetimes("1");
	fsActivateBean.setActivatepage("/index.jsp");
	fsActivateBean.setActivatelink(fs_activatelink);
	fsActivateBean.setSenddate(format.formatDate(new java.util.Date()));
	fsActivateBean.setSendtime(format.formatTime(new java.sql.Time(System.currentTimeMillis())));
	int rows = fsActivateBean.insert(fsGlobal); 	
	if(rows>0) {
		//send mail or im with fs_activatekey
	}
} catch(Throwable ex) {
	Trace.error(fsGlobal,ex);
	fsGlobal.setThrowable(ex);
	int fs_errorcode = fsGlobal.parseErrorcode();
	String fs_errormessage = fsGlobal.getFsMessage();
	if(fsIsAjax) {
		response.setStatus(fs_errorcode,fs_errormessage);
		return;
	}
	result.append("<body>");
	result.append(fs_errormessage);
	result.append("</body>");
	out.println("<message type=\"error\">");
	out.println(result.toString());
	out.println("</message>");
	return;
}
if(fsIsJSON) {
	out.print(fsActivateBean.toJSON());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\"windows-874\"?>");
	out.print(fsActivateBean.toXML());
	return;
}
result.setLength(0);
result.append("<body>");
result.append("Activate Created");
result.append("</body>");
%>
<message type="result">
	<%=result.toString()%>
</message>
