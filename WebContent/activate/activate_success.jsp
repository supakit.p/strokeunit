<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsActivateBean" scope="request" class="com.fs.bean.ActivateBean"/>
<c:if test="${fsScreen.config('activate', pageContext.request, pageContext.response, true, false)}"></c:if>
<%
	String scheme = request.getScheme();             
	String serverName = request.getServerName(); 
	int serverPort = request.getServerPort(); 
	String aurl = scheme+"://"+serverName+(serverPort==80?"":":"+serverPort)+request.getContextPath()+"/index.jsp";
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Activation</title>
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<link rel="stylesheet" type="text/css" href="../css/program_style.css" />
		<style>
			body { 
  				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif, "Courier New";
  				line-height: 1.42857143;
  				color: #333;
  				background-color: #E7F7F7;
			}
			a { color: #428bca; }						
			a:hover { text-decoration: none; color: #ff9933; }
			label {
			  display: inline-block;
			  max-width: 100%;
			  margin-bottom: 5px;
			  font-weight: bold;
			}			
		</style>
	</head>
	<body class="portalbody portalbody-off">
		<div class="row portal-area sub-entry-layer" style="padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
		<table style="width:100%;">
			<tbody>
				<tr class="rclass">
					<td height="50">&nbsp;</td>
				</tr>
				<tr class="rclass">
					<td align="center" height="30"><fs:label tagclass="lclass" tagid="success_label" required="false">Reset password success</fs:label></td>
				</tr>
				<tr>
					<td align="center" height="30"><fs:label tagclass="lclass" tagid="verify_label" required="false">Please verify your email address for new changed</fs:label></td>
				</tr>
				<tr>
					<td align="center" height="30"><a href="<%=aurl%>" id="loginlink">Log In</a></td>
				</tr>
			</tbody>
		</table>
		</div>
	</body>
</html>
