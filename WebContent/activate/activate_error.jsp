<%@ page isErrorPage="true" %>
<%@ page import="com.fs.bean.util.*" %>
<%
	if(exception!=null) exception.printStackTrace();
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Error</title>
		<link href="../css/base_style.css" rel="stylesheet" type="text/css"></link>
	</head>
	<body style="background-color: whitesmoke;">
		<br></br><br></br>
		<div style="text-align:center; color: red; font-size: 20px; ">
				Sorry, Activation on your request has failure
		</div>
		<br></br>
		<div style="text-align:center;">
			<a href="javascript:window.history.back();">Go Back</a>
		</div>
		<br></br>
		<div id="errorlayer" style="text-align:center; font-size: 16px;">
		<%if(exception!=null && exception.getMessage()!=null && !exception.getMessage().equals("")) {%>
		<%=BeanUtility.preserveXML(exception.getMessage())%>
		<%}else{ String fs_exception = exception==null?"":""+exception;  %>
		<%=BeanUtility.preserveXML(fs_exception)%>
		<%}%>
		</div>
		<br></br>
	</body>
</html>