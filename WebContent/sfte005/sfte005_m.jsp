<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte005',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE005Bean" scope="session" class="com.fs.bean.SFTE005Bean"/>
<jsp:include page="sfte005_md.jsp" />
<!DOCTYPE html>
<html>
	<head>
		<title>User Information</title>
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createImportScripts('sfte005',pageContext.request,pageContext.response)}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/scripts.jsp"/>
		<link rel="stylesheet" type="text/css" href="../css/program_style.css" />
		<link rel="stylesheet" type="text/css" href="sfte005.css?${fsScreen.currentTime()}" />
		<script type="text/javascript" src="sfte005.js?${fsScreen.currentTime()}"></script>
	</head>
	<body class="portalbody portalbody-off">
		<div id="fsdialoglayer" style="display:none;"><span id="fsmsgbox"></span></div>
		<div id="fsacceptlayer" style="display:none;"><span id="fsacceptbox"></span></div>
		<div id="fswaitlayer" style="display:none; position:absolute; left:1px; top:1px; z-Index:9999;"><img id="waitImage" class="waitimgclass" src="../images/waiting.gif" width="50px" height="50px" alt=""></img></div>	
		<div id="sfte005" class="pt-page pt-page-current pt-page-controller">
			<h1 class="page-header-title" title="sfte005">${fsLabel.getText('caption','User Information')}</h1>
			<div id="searchpanel" class="panel-body">
				<form id="fssearchform" name="fssearchform" method="post">	
					<input type="hidden" name="fsAction" value="collect"/>
					<input type="hidden" name="fsAjax" value="true"/>
					<input type="hidden" name="fsDatatype" value="text"/>
					<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
					<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
					<input type="hidden" name="fsPage" value="1"/>						
					<div class="row row-height filter-layer">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
							<div class="col-md-3 col-height search-group">
								<fs:label tagid="sites_label" tagclass="control-label">Company</fs:label>
								<fs:select tagclass="form-control input-sm" tagid="sites" name="site" section="ALL_SITE_CATEGORY"> </fs:select>
							</div>
							<div class="col-md-2 col-height search-group">
								<fs:label tagid="userids_label" tagclass="control-label">User ID</fs:label>
								<input class="form-control input-sm" id="userids" name="userid" placeholder="" autocomplete="off">
							</div>
							<div class="col-md-3 col-height search-group">
								<fs:label tagid="usertnames_label" tagclass="control-label">User Name</fs:label>
								<input class="form-control input-sm" id="usertnames" name="usertname" placeholder="" autocomplete="off">
							</div>
							<div class="col-md-3 col-height search-group">
								<fs:label tagid="usertsurnames_label" tagclass="control-label">User Surname</fs:label>
								<input class="form-control input-sm" id="usertsurnames" name="usertsurname" placeholder="" autocomplete="off">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 10px;">
							<div class="pull-right">
								<button type="button" id="insertbutton" class="btn btn-dark btn-sm" style="margin-top: 5px;"><i class="fa fa-plus" aria-hidden="true" style="margin-right:1px;"></i>${fsLabel.getText('insertbutton','Insert')}</button>
							</div>
							<div class="pull-right" style="margin-left: 20px;"></div>
							<div class="pull-right">
								<button type="button" id="searchbutton" class="btn btn-dark btn-sm" style="margin-top: 5px;"><i class="fa fa-search" aria-hidden="true" style="margin-right:10px;"></i>${fsLabel.getText('searchbutton','Search')}</button>
							</div>
						</div>
					</div>
				</form>
				<div id="listpanel" class="table-responsive" style="padding-top: 10px;">
					<jsp:include page="sfte005_d.jsp"/>
				</div>
			</div>
			<div id="entrypanel" style="display:none;">
					<jsp:include page="sfte005_de.jsp"/>
			</div>
		</div>
	</body>
</html>
