<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE005Bean" scope="session" class="com.fs.bean.SFTE005Bean"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form name="fslistform" id="fslistform" method="post" autocomplete="off">
	<input type="hidden" name="fsAction" value="view"/>
	<input type="hidden" name="fsAjax" value="true"/>
	<input type="hidden" name="fsDatatype" value="text"/>
	<input type="hidden" name="fsRowid" value=""/>
	<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
	<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
	<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>
</form>
<div id="fseffectedtransactions" style="display:none;">${fsSFTE005Bean.effectedTransactions()}</div>
<table id="datatable" class="table table-bordered table-hover table-striped tablesorter">
	<thead>
		<tr>
			<th width="75px"  class="text-center" style="cursor: default;"><fs:label tagid="seqno_headerlabel" >No.</fs:label></th>
			<th width="200px" class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=site&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="site_headerlabel">Company</fs:label></a></th>
			<th width="200px" class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=userid&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="userid_headerlabel">User ID</fs:label></a></th>
			<th width="200px" class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=usertname&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="usertname_headerlabel">User Name</fs:label></a></th>
			<th width="200px" class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=usertsurname&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="usertsurname_headerlabel">User Surname</fs:label></a></th>
			<th width="150px" class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=email&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="email_headerlabel">Email</fs:label></a></th>
			<th width="150px" class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=mobile&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="mobile_headerlabel">Mobile</fs:label></a></th>
			<th width="100px" class="text-center" style="cursor: default;"><i class="fa fa-bolt" aria-hidden="true"></i></th>
		</tr>
	</thead>
	<tbody id="datatablebody">							
		<c:choose>
			<c:when test="${fsSFTE005Bean.size() > 0}">
				<c:forEach var="fsElement" items="${fsSFTE005Bean.elements()}" varStatus="records">
					<c:if test="${!fsGlobal.nextChapter(records.count)}">
						<tr>
							<td class="text-center"><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count});"><c:out value="${records.count}"></c:out></a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count});"><c:out value="${fsElement.sitedesc}"></c:out></a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count});"><c:out value="${fsElement.userid}"></c:out></a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count});"><c:out value="${fsElement.usertname}"></c:out></a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count});"><c:out value="${fsElement.usertsurname}"></c:out></a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count});"><c:out value="${fsElement.email}"></c:out></a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count});"><c:out value="${fsElement.mobile}"></c:out></a></td>
							<td class="text-center">
									<button name="btn-edit" type="button" class="btn-edit" title="${fsLabel.getLabel('btnedit_tooltip')}"
											onclick="submitRetrieve(${records.count});"></button>
									<button type="button" class="btn-delete" title="${fsLabel.getLabel('btndelete_tooltip')}" 
											onclick="submitDelete(['${fsElement.userid}']);"></button>
							</td>
						</tr>
					</c:if>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:if test="${!fsScreen.isViewState()}">
					<tr>
						<td class="text-center" colspan="8">
								${fsLabel.getLabel("recordnotfound")}
						</td>
					</tr>
				</c:if>
			</c:otherwise>
		</c:choose>		
	</tbody>
</table>	
<div class="fschaptertablelayer" style="text-align: center;">
<table class="fschaptertable" style="margin: auto;">
	<tr class="fschapterrow"><td class="fschaptercolumn">
	<form name="fschapterform" id="fschapterform" method="post" autocomplete="off">
		<input type="hidden" name="fsAction" value="chapter"/>
		<input type="hidden" name="fsAjax" value="true"/>
		<input type="hidden" name="fsDatatype" value="text"/>
		<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
		<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
		<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>
		<input type="hidden" name="fsSorter" value=""/>
	</form>
	<div id="fschapterlayer">
<c:if test="${fsGlobal.hasPaging(fsPager.rows)}">
	${fsGlobal.createPaging(fsPager.rows)}
</c:if>
	</div>
	</td>
	</tr>
</table>
</div>	
