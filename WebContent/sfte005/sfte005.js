var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("sfte005"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
		setupComponents();
		setupAlertComponents();
}
function setupComponents() {
		$("#searchbutton").click(function(evt) { 
			//if(!confirm("search")) return false;
			search(); 
			return false;
		});
		$("#insertbutton").click(function(evt) { 
			insert(); 
			return false;
		});
		$("#savebutton").click(function() { 
			save();
			return false;
		});
		$("#cancelbutton").click(function() { 
			cancel();
			return false;
		});
}
function clearingFields() {
		fsentryform.reset();
}
function search(aform) {
		if(!aform) aform = fssearchform;
		//alert($(aform).serialize());
		startWaiting();
		jQuery.ajax({
			url: "sfte005_c.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				//alert(transport.responseText);
				searchComplete(transport,data);
			}
		});	
}
function searchComplete(xhr,data) {
		stopWaiting();
		$("#listpanel").html(data);
}
function insert() {
		clearingFields();
		fsentryform.fsDatatype.value = "json";
		fsentryform.fsAction.value = "enter";
		$("#userid").attr("readonly",false);
		var parent = $("#site").parent();
		if(parent.is("fieldset")) $("#site").unwrap();
		showPageEntry();
}
function showPageSearch() {
		$("#entrypanel").hide();
		$("#searchpanel").show();
}
function showPageEntry() {
		$("#entrypanel").show();
		$("#searchpanel").hide();
}
function submitRetrieve(rowIndex) {
		var aform = fslistform;
		aform.fsRowid.value = ""+rowIndex;
		aform.fsDatatype.value = 'json';
		//alert($(aform).serialize());
		startWaiting();
		jQuery.ajax({
			url: "sfte005_dc.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				//alert(transport.responseText);
				prepareScreenToUpdate(fsentryform,data);
			}
		});	
}
function prepareScreenToUpdate(aform,data) {
		var parent = $("#site").parent();
		if(parent.is("fieldset")) $("#site").unwrap();
		$(".alert-input").each(function(index,element) {
			var ths = $(this);
			var thid = ths.attr("id");
			$("#"+thid+"_alert").hide();
			ths.parent().removeClass("has-error");
		});
		aform.fsDatatype.value = "text";
		aform.fsAction.value = "edit";
		try {
			var jsRecord = $.parseJSON(data);
			for(var p in jsRecord) {
				$("#"+p,aform).val(jsRecord[p]);
			}
			$("#gendermale").val("M");
			$("#genderfemale").val("F");
			$("#gendermale").attr("checked",jsRecord["gender"]=="M");
			$("#genderfemale").attr("checked",jsRecord["gender"]=="F");			
		} catch(ex) { }
		$("#userid").attr("readonly",true);
		$("#site").wrap($("<fieldset disabled></fieldset>"));
		showPageEntry();
}
function cancel() {
		confirmCancel(function() {
			clearingFields();
			showPageSearch();
		});
}
function validForm() {
	clearAlerts();
	var validator = null;
	if($.trim($("#site").val())=="") {
		$("#site").parent().addClass("has-error");
		$("#site_alert").show();
		if(!validator) validator = "site";
	}
	if($.trim($("#userid").val())=="") {
		$("#userid").parent().addClass("has-error");
		$("#userid_alert").show();
		if(!validator) validator = "userid";
	}
	if($.trim($("#usertname").val())=="") {
		$("#usertname").parent().addClass("has-error");
		$("#usertname_alert").show();
		if(!validator) validator = "usertname";
	}
	if($.trim($("#usertsurname").val())=="") {
		$("#usertsurname").parent().addClass("has-error");
		$("#usertsurname_alert").show();
		if(!validator) validator = "usertsurname";
	}
	if($.trim($("#userename").val())=="") {
		$("#userename").parent().addClass("has-error");
		$("#userename_alert").show();
		if(!validator) validator = "userename";
	}
	if($.trim($("#useresurname").val())=="") {
		$("#useresurname").parent().addClass("has-error");
		$("#useresurname_alert").show();
		if(!validator) validator = "useresurname";
	}
	if($.trim($("#email").val())=="") {
		$("#email").parent().addClass("has-error");
		$("#email_alert").show();
		if(!validator) validator = "email";
	}
	if(validator) {
		$("#"+validator).focus();
		setTimeout(function() { 
			$("#"+validator).parent().addClass("has-error");
			$("#"+validator+"_alert").show();
		},100);
		return false;
	}
	return true;
}
function save(aform) {
		if(!aform) aform = fsentryform;
		//alert($(aform).serialize());
		if(!validForm()) return false;
		var isInsertMode = aform.fsAction.value=='enter';
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte005_dc.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					if(isInsertMode) {
						successbox(function() { clearingFields(); });					
					} else {
						showPageSearch();
						fssearchform.fsPage.value = fslistform.fsPage.value;
						search();
					}
				}
			});
		});
		return false;
}
function submitChapter(aform,index) {
		//alert($(aform).serialize());
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte005_c.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
				//alert(transport.responseText);
			}
		});
}
function submitOrder(fsParams) {
		//alert(fsParams);
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte005_cd.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: fsParams,
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
				//alert(transport.responseText);
			}
		});
		return false;
}
function submitDelete(fsParams) {
		//alert("delete : "+fsParams);
		confirmDelete([fsParams[0]],function() {
			deleteRecord(fsParams);
		});
}
function deleteRecord(fsParams) {
		//alert(fsParams);
		startWaiting();
		jQuery.ajax({
			url: "sfte005_dc.jsp",
			type: "POST",
			data: "fsAction=delete&fsDatatype=json&userid="+fsParams[0],
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				showPageSearch();
				fssearchform.fsPage.value = fslistform.fsPage.value;
				search();
			}
		});	
}


