<%@ page info="SCCS id: $Id$"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.initialConfig('intro',pageContext.request, pageContext.response)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<%
	String fs_homer = request.getParameter("home");
	String fs_prefix = "";
	String fs_relative = "../";
	if("true".equalsIgnoreCase(fs_homer)) {
		fs_prefix = "intro/";
		fs_relative = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Car Loan</title>
<link href="<%=fs_relative%>img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link rel="stylesheet" href="<%=fs_prefix%>css/bootstrap.min.css" />
<link rel="stylesheet" href="<%=fs_prefix%>css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<%=fs_prefix%>intro.css?<%=System.currentTimeMillis()%>" />
</head>
<body>
	<div class="topnav" id="headtopnav">
      	<a href="#home"><img alt="Mini Logo" id="minilogo" src="<%=fs_prefix%>img/head_logo.png" width="140px" height="40px" /></a>
	  	<a href="<%=fs_relative%>main.jsp" style="margin-right: 15px; margin-top: 1px; float: right;" id="login">${fsLabel.getText('login_navbar','เข้าสู่ระบบ')}</a>
	</div>
	<div id="content" class="text-center">
		<div class="container">
		  <div class="row anchor" id="home" style="padding-top: 80px;">
				<img alt="logo" id="logo" src="<%=fs_prefix%>img/logo.jpg" width="200px"><br/><br/>
				<h2 class="header">จัดหาสินเชื่อทุกประเภท  วงเงินสูง อนุมัติไว</h2><br/>
				<div class="col-md-12">
					<div class="row anchor">
						<div class="col-md-12 text-center"><img class="credit-class" src="<%=fs_prefix%>img/credits_1.png" width="500px" height="500px" alt="Motorcycle" /></div>
					</div>
					<div class="row anchor">
						<div class="col-md-12 text-center">
							<label class="credits-label">ไม่ต้องโอนเล่ม</label>
						</div>
					</div>
					<div class="row anchor">
						<div class="col-md-12 text-center">
							<label class="credits-label">รับเงินเร็ว</label>
						</div>
					</div>
				</div>				
		  </div>
	 	</div>
	</div>
	<br/>
	<br/>
	<br/>
	<br/>
</body>
</html>