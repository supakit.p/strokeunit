<%@page import="com.fs.dev.strokeunit.TheStrokeUnitPatientInfo"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnitUtility"%>
<%@page import="com.fs.dev.strokeunit.HttpClientServices"%>
<%@page import="com.fs.dev.strok.service.Patient"%>
<%@page import="com.fs.dev.strok.service.ThePatient"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnitConstant"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.http.NameValuePair" %>
<%@ page import="org.apache.http.message.BasicNameValuePair" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.net.URI" %>


<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("FastTrack");
    header.setMethod("request");
    try {
        Trace.info("############### FastTrack/request ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();

        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();

        if (fs_apiName != null && fs_apiName.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", java.util.Locale.US);
            final BeanUtility butil = new BeanUtility();
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                final String FastTrackByHN = "FastTrackByHN";

                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();
                    String patientId = json.has("patientId") ? json.getString("patientId") : "";
                    String bedId = json.has("bedId") ? json.getString("bedId") : "";
                    if (FastTrackByHN.equalsIgnoreCase(fs_apiName)) {

                        String hn = json.getString("hn");
                        Trace.info("############ HN " + hn + " ############");

                        //check HN Duplicate
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("SELECT 1 ");
                        knsql.append("FROM Tw_PatientInfo ");
                        knsql.append("WHERE HN =?hn AND IsActive = '1' AND StatusCode IN ('N') ");
                        knsql.setParameter("hn", hn);
                        if (patientId.length() > 0) {
                            knsql.append(" AND patientId != ?patientId ");
                            knsql.setParameter("patientId", patientId);
                        }
                        try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                            if (rs.next()) {
                                return -409;
                            }
                        }
                        if (patientId.length() > 0) {
                            knsql = new KnSQL(this);
                            knsql.append("SELECT 1 ");
                            knsql.append("FROM Tw_PatientInfo ");
                            knsql.append("WHERE HN =?hn AND patientId =?patientId ");
                            knsql.setParameter("hn", hn);
                            knsql.setParameter("patientId", patientId);
                            try (java.sql.ResultSet rs2 = knsql.executeQuery(connection)) {
                                if (rs2.next()) {
                                    org.json.JSONObject dataPatient = new org.json.JSONObject();
                                    dataPatient.put("hn", hn);
                                    dataPatient.put("patientId", patientId);
                                    TheStrokeUnitPatientInfo patientInfo = new TheStrokeUnitPatientInfo();
                                    org.json.JSONObject patientInfoBody = patientInfo.request(connection, dataPatient, "PatientInfoByHN");
                                    if (patientInfoBody.getInt("errorCode") != 2) {
                                        body.put("body", patientInfoBody);
                                        return patientInfoBody.length();
                                    }
                                }
                            }
                        }

                        org.json.JSONObject jsonAdmitWardTimeConfig = su.getSysConfig("fastTrack", "admitWardTime");
                        int admitWardTimeConfig = jsonAdmitWardTimeConfig.has(TheStrokeUnitConstant.SYS_CONFIG_VALUE) ? jsonAdmitWardTimeConfig.getInt(TheStrokeUnitConstant.SYS_CONFIG_VALUE) : 0;
                        Date date = new Date();
                        Calendar calConfig = Calendar.getInstance(java.util.Locale.US);
                        calConfig.setTime(date);
                        calConfig.add(Calendar.HOUR_OF_DAY, -admitWardTimeConfig);

                        String admitWardTime = df.format(calConfig.getTime());

                        // FastTrackByHN
                        // T_Stroke T_tPA T_Investigation
                        knsql = new KnSQL(this);
                        knsql.append("SELECT T_Stroke.StrokeId , HN , PrefixName , FirstName , LastName , Age , Gender , AdmitWardTime ");
                        knsql.append(" , LastSeenNormal , StrokeOnset , rtPAPushAmt , rtPADripAmt  , BodyWeight ");
                        knsql.append(" , T_tPA.rtPAPushTime , T_tPA.rtPADripTime  , T_Thrombectomy.PunctureTime , T_Treatment.CTAFlag, T_Treatment.CTASPECT , T_Treatment.ConsentThrombecFlag , T_Treatment.ConsentTPAFlag ");
                        knsql.append(" FROM T_Stroke ");
                        knsql.append(" LEFT JOIN T_tPA ON T_Stroke.StrokeId = T_tPA.StrokeId ");
                        knsql.append(" LEFT JOIN T_Thrombectomy ON T_Stroke.StrokeId = T_Thrombectomy.StrokeId ");
                        knsql.append(" LEFT JOIN T_Treatment ON T_Stroke.StrokeId = T_Treatment.StrokeId ");
                        knsql.append(" LEFT JOIN T_Investigation ON T_Stroke.StrokeId = T_Investigation.StrokeId ");
                        knsql.append(" WHERE WardId = '1' AND CompleteMobileTime IS NOT NULL AND T_Stroke.HN = ?hn ");
                        if (-1 != admitWardTimeConfig) {
                            knsql.append(" AND AdmitWardTime >= ?admitWardTime ");
                            knsql.setParameter("admitWardTime", admitWardTime);
                        }
                        knsql.append(" ORDER BY AdmitWardTime DESC ");
                        knsql.setParameter("hn", hn);
                        HashMap<String, String> configField = new HashMap();
                        configField.put("StrokeId", "strokeId");
                        configField.put("HN", "hn");
                        configField.put("PrefixName", "prefixName");
                        configField.put("FirstName", "firstName");
                        configField.put("LastName", "lastName");
                        configField.put("Age", "age");
                        configField.put("Gender", "gender");
                        configField.put("AdmitWardTime", "admitWardTime");
                        configField.put("LastSeenNormal", "lastSeenNormal");
                        configField.put("StrokeOnset", "strokeOnset");
                        configField.put("rtPAPushAmt", "rtPAPushAmt");
                        configField.put("rtPADripAmt", "rtPADripAmt");
                        configField.put("rtPAPushTime", "rtPAPushTime");
                        configField.put("rtPADripTime", "rtPADripTime");
                        configField.put("BodyWeight", "weight");

                        configField.put("ConsentTPAFlag", "consentTPAFlag");
                        configField.put("ConsentThrombecFlag", "consentThrombecFlag");
                        configField.put("PunctureTime", "punctureTime");

                        org.json.JSONObject jsonFastTrack = new org.json.JSONObject(su.requestData(connection, knsql, false, configField).toString());
                        if (jsonFastTrack.length() <= 0) {
                            // Call API Siriraj 
                            org.json.JSONObject patient = new org.json.JSONObject();
                            //get data from siriraj
                            ThePatient pt = new ThePatient();
                            Patient p = null;
                            p = pt.getPatientByHN(hn);
                            Trace.info("connecting siriraj server by hn : " + p);
                            if (p.isSuccess()) {
                                result++;
                                patient.put("prefixName", butil.MS874ToUnicode(p.getPrefixName()));
                                patient.put("hn", p.getHN());
                                patient.put("citizenId", p.getCitizenId());
//                            patient.put("passportNo", p.getPassportNo());
                                patient.put("firstName", butil.MS874ToUnicode(p.getFirstName()));
                                patient.put("lastName", butil.MS874ToUnicode(p.getLastName()));
//                            patient.put("dateOfBirth", p.getDateOfBirth());
                                patient.put("gender", TheStrokeUnitUtility.changGenderTHToEn(butil.MS874ToUnicode(p.getGender())));
                                patient.put("patientName", patient.getString("prefixName") + patient.getString("firstName") + " " + patient.getString("lastName"));

                                java.util.Date bdate = p.getBirthDay();
                                java.sql.Date bddate = new java.sql.Date(bdate.getTime());
                                int getYearBdate = butil.getYearFromYMD(bddate.toString());
                                String currentdate = butil.formatDateTime(butil.getCurrentDate(), "yyyyMMdd");
                                int getYearCurdate = butil.getYearFromYMD(currentdate);
                                patient.put("age", getYearCurdate - getYearBdate);

                                patient.put("bedId", bedId);
                                patient.put("patientId", patientId);
                                patient.put("vsId", "");
                                patient.put("dcId", "");
                                patient.put("nsId", "");

                                TheStrokeUnitPatientInfo patientInfo = new TheStrokeUnitPatientInfo();
                                org.json.JSONObject jsonResult = patientInfo.submit(connection, patient, "PatientInfo");
                                int errorCode = jsonResult.getInt("errorCode");
                                if (errorCode == -4091) {
                                    throw new RuntimeException("BedNo. has already used");
                                } else if (errorCode == -204) {
                                    throw new RuntimeException("BedId may not be null");
                                }
                                patientId = jsonResult.has("patientId") ? jsonResult.getString("patientId") : patientId;
                                patient.put("patientId", patientId);
                                patient.put("vsId", jsonResult.has("vsId") ? jsonResult.get("vsId") : "");
                                patient.put("dcId", jsonResult.has("vsId") ? jsonResult.get("vsId") : "");
                                patient.put("nsId", jsonResult.has("nsId") ? jsonResult.get("nsId") : "");
                                body.put("body", patient);
                                return result;
                            } else {
                                // Not Found
                                return 0;
                            }
                        }
                        jsonFastTrack.put("patientName", jsonFastTrack.getString("prefixName") + jsonFastTrack.getString("firstName") + " " + jsonFastTrack.getString("lastName"));
                        jsonFastTrack.put("isrtPa", "".equals(jsonFastTrack.getString("rtPAPushAmt")) ? "0" : "1");
                        result += jsonFastTrack.length();

                        String strokeId = jsonFastTrack.getString("strokeId");
                        String consentTPAFlag = jsonFastTrack.getString("consentTPAFlag");
                        String rtPAPushTime = jsonFastTrack.getString("rtPAPushTime");
                        String consentThrombecFlag = jsonFastTrack.getString("consentThrombecFlag");
                        String punctureTime = jsonFastTrack.getString("punctureTime");

                        // T_VitalSign
                        KnSQL knsqlVS = new KnSQL(this);
                        knsqlVS.append("SELECT VitalSignTime , HeartRate , RespiratoryRate , SBP , DBP , OxygenSat , Temperature ");
                        knsqlVS.append(" FROM T_VitalSign ");
                        knsqlVS.append(" WHERE StrokeId = ?strokeId ");
                        knsqlVS.append(" ORDER BY VitalSignTime DESC ");
                        knsqlVS.setParameter("strokeId", strokeId);
                        HashMap<String, String> configFieldVS = new HashMap();
                        configFieldVS.put("HeartRate", "vsHR");
                        configFieldVS.put("VitalSignTime", "vsTime");
                        configFieldVS.put("RespiratoryRate", "vsRR");
                        configFieldVS.put("SBP", "vsSBP");
                        configFieldVS.put("DBP", "vsDBP");
                        configFieldVS.put("OxygenSat", "vsO2sat");
                        configFieldVS.put("Temperature", "vsTemp");
                        org.json.JSONObject jsonVS = new org.json.JSONObject(su.requestData(connection, knsqlVS, false, configFieldVS).toString());

                        if (jsonVS.length() > 0) {
                            result += jsonVS.length();
                            jsonFastTrack.put("vsHR", jsonVS.get("vsHR"));
                            jsonFastTrack.put("vsTime", jsonVS.get("vsTime"));
                            jsonFastTrack.put("vsRR", jsonVS.get("vsRR"));
                            jsonFastTrack.put("vsSBP", jsonVS.get("vsSBP"));
                            jsonFastTrack.put("vsDBP", jsonVS.get("vsDBP"));
                            jsonFastTrack.put("vsO2sat", jsonVS.get("vsO2sat"));
                            jsonFastTrack.put("vsTemp", jsonVS.get("vsTemp"));
                        } else {
                            jsonFastTrack.put("vsHR", "");
                            jsonFastTrack.put("vsTime", "");
                            jsonFastTrack.put("vsRR", "");
                            jsonFastTrack.put("vsSBP", "");
                            jsonFastTrack.put("vsDBP", "");
                            jsonFastTrack.put("vsO2sat", "");
                            jsonFastTrack.put("vsTemp", "");
                        }

                        // T_NIHSS
                        KnSQL knsqlNIHSS = new KnSQL(this);
                        knsqlNIHSS.append("SELECT  NIHSSScore , TotalScore , NIHSSTime ");
                        knsqlNIHSS.append(" FROM T_NIHSS ");
                        knsqlNIHSS.append(" WHERE StrokeId = ?strokeId ");
                        knsqlNIHSS.append(" ORDER BY NIHSSTime ASC ");
                        knsqlNIHSS.setParameter("strokeId", strokeId);
                        HashMap<String, String> configFieldNIHSS = new HashMap();
                        configFieldNIHSS.put("NIHSSScore", "nihss");
                        configFieldNIHSS.put("TotalScore", "nihssTotal");
                        configFieldNIHSS.put("NIHSSTime", "nihssTime");
                        org.json.JSONObject jsonNIHSS = new org.json.JSONObject(su.requestData(connection, knsqlNIHSS, false, configFieldNIHSS).toString());

                        if (jsonNIHSS.length() > 0) {
                            result += jsonNIHSS.length();
                            jsonFastTrack.put("nihss", jsonNIHSS.get("nihss"));
                            jsonFastTrack.put("nihssTotal", jsonNIHSS.get("nihssTotal"));
                            jsonFastTrack.put("nihssTime", jsonNIHSS.get("nihssTime"));
                        } else {
                            jsonFastTrack.put("nihss", "");
                            jsonFastTrack.put("nihssTotal", "");
                            jsonFastTrack.put("nihssTime", "");
                        }

                        // T_StrokeType
                        KnSQL knsqlStrokeType = new KnSQL(this);
                        knsqlStrokeType.append("SELECT  StrokeTypeId , MIMICS , StrokeTypeTime ");
                        knsqlStrokeType.append(" FROM T_StrokeType ");
                        knsqlStrokeType.append(" WHERE StrokeId = ?strokeId ");
                        knsqlStrokeType.setParameter("strokeId", strokeId);
                        HashMap<String, String> configFieldStrokeType = new HashMap();
                        configFieldStrokeType.put("StrokeTypeId", "strokeTypeId");
                        configFieldStrokeType.put("MIMICS", "mimics");
                        configFieldStrokeType.put("StrokeTypeTime", "strokeTypeTime");
                        org.json.JSONArray jsonStrokeType = new org.json.JSONArray(su.requestData(connection, knsqlStrokeType, true, configFieldStrokeType).toString());
                        result += jsonStrokeType.length();
                        org.json.JSONArray strokeTypes = new org.json.JSONArray();

                        for (int i = 0; i < jsonStrokeType.length(); i++) {
                            org.json.JSONObject jsonObjectElement = jsonStrokeType.getJSONObject(i);
                            String strokeTypeId = jsonObjectElement.getString("strokeTypeId");
                            org.json.JSONObject strokeType = new org.json.JSONObject();
                            strokeType.put("strokeTypeId", "");
                            strokeType.put("other", "");
                            strokeType.put("isrtPA", "");
                            strokeType.put("isThrombec", "");
                            strokeType.put("isNonTreatment", "");
                            boolean isNonTreatment = true;
                            switch (strokeTypeId) {
                                case "1":
                                    // Ischemic	, Acute Ischemic Stroke (AIS) To Acute Ischemic Stroke (AIS)
                                    strokeType.put("strokeTypeId", "2");
                                    if ("Y".equals(consentTPAFlag) && !"".equals(rtPAPushTime)) {
                                        strokeType.put("isrtPA", "Y");
                                        isNonTreatment = false;
                                    }
                                    if ("Y".equals(consentThrombecFlag) && !"".equals(punctureTime)) {
                                        strokeType.put("isThrombec", "Y");
                                        isNonTreatment = false;
                                    }
                                    if (isNonTreatment) {
                                        strokeType.put("isNonTreatment", "Y");
                                    }
                                    strokeTypes.put(strokeType);
                                    break;
                                case "4":
                                    // SAH To SAH+IVH
                                    strokeType.put("strokeTypeId", "4");
                                    strokeTypes.put(strokeType);
                                    strokeType = new org.json.JSONObject();
                                    strokeType.put("other", "");
                                    strokeType.put("isrtPA", "");
                                    strokeType.put("isThrombec", "");
                                    strokeType.put("isNonTreatment", "");
                                    strokeType.put("strokeTypeId", "5");
                                    strokeTypes.put(strokeType);
                                    break;
                                case "3":
                                    // ICH To ICH
                                    strokeType.put("strokeTypeId", "3");
                                    strokeTypes.put(strokeType);
                                    break;
                            }
                        }

                        jsonFastTrack.put("strokeType", strokeTypes);

                        // T_SymptomList
                        KnSQL knsqlSymptom = new KnSQL(this);
                        knsqlSymptom.append("SELECT  SymptomId , OtherSymptom ");
                        knsqlSymptom.append(" FROM T_SymptomList ");
                        knsqlSymptom.append(" WHERE StrokeId = ?strokeId ");
                        knsqlSymptom.setParameter("strokeId", strokeId);
                        HashMap<String, String> configFieldSymptom = new HashMap();
                        configFieldSymptom.put("SymptomId", "symptomId");
                        configFieldSymptom.put("OtherSymptom", "symtomText");
                        org.json.JSONArray jsonSymptom = new org.json.JSONArray(su.requestData(connection, knsqlSymptom, true, configFieldSymptom).toString());
                        result += jsonSymptom.length();

                        jsonFastTrack.put("symptom", jsonSymptom);
                        jsonFastTrack.put("bedId", bedId);
                        jsonFastTrack.put("patientId", patientId);
                        jsonFastTrack.put("vsId", "");
                        jsonFastTrack.put("dcId", "");
                        jsonFastTrack.put("nsId", "");

                        TheStrokeUnitPatientInfo patientInfo = new TheStrokeUnitPatientInfo();
                        org.json.JSONObject jsonResult = patientInfo.submit(connection, jsonFastTrack, "PatientInfo");
                        int errorCode = jsonResult.getInt("errorCode");
                        if (errorCode == -4091) {
                            throw new RuntimeException("BedNo. has already used");
                        } else if (errorCode == -204) {
                            throw new RuntimeException("BedId may not be null");
                        }
                        patientId = jsonResult.has("patientId") ? jsonResult.getString("patientId") : patientId;
                        jsonFastTrack.put("patientId", patientId);
                        jsonFastTrack.put("vsId", jsonResult.has("vsId") ? jsonResult.get("vsId") : "");
                        jsonFastTrack.put("dcId", jsonResult.has("vsId") ? jsonResult.get("vsId") : "");
                        jsonFastTrack.put("nsId", jsonResult.has("nsId") ? jsonResult.get("nsId") : "");

                        body.put("body", jsonFastTrack);
                        return result;
                    }
                    /*else {
                        
                        // Call API Siriraj 
                        org.json.JSONObject patient = new org.json.JSONObject();
                        //get data from siriraj
                        ThePatient pt = new ThePatient();
                        Patient p = null;
                        p = pt.getPatientByHN("11223344");
                        Trace.info("connecting siriraj server by hn : " + p);
                        if (p.isSuccess()) {
                            result++;
                            patient.put("prefixName", p.getPrefixName());
                            patient.put("hn", p.getHN());
                            patient.put("citizenId", p.getCitizenId());
//                            patient.put("prefixName", p.getPassportNo());
                            patient.put("firstName", butil.MS874ToUnicode(p.getFirstName()));
                            patient.put("lastName", butil.MS874ToUnicode(p.getLastName()));
//                            patient.put("prefixName", p.getDateOfBirth());
                            patient.put("gender", TheStrokeUnitUtility.changGenderTHToEn(butil.MS874ToUnicode(p.getGender())));
                            patient.put("patientName", patient.getString("firstName") + patient.getString("lastName"));
                            patient.put("bedId", 11);
                            java.util.Date bdate = p.getBirthDay();
                            java.sql.Date bddate = new java.sql.Date(bdate.getTime());
                            int getYearBdate = butil.getYearFromYMD(bddate.toString());
                            String currentdate = butil.formatDateTime(butil.getCurrentDate(), "yyyyMMdd");
                            int getYearCurdate = butil.getYearFromYMD(currentdate);
                            patient.put("age", getYearCurdate - getYearBdate);

                            patient.put("bedId", bedId);
                            patient.put("patientId", patientId);

                            System.err.println("patient : " + patient);

                            //test call my api
                            URI url = new URI("http://localhost:8080/strokeunit/rest/handler/apipatientinfo/submit");
                            HashMap<String, String> headers = new HashMap();
                            headers.put(HttpClientServices.CONTENT_TYPE, HttpClientServices.CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED);
                            List<NameValuePair> urlParameters = new ArrayList<>();
                            urlParameters.add(new BasicNameValuePair("apiName", "PatientInfo"));
                            urlParameters.add(new BasicNameValuePair("data", patient.toString()));
                            org.json.JSONObject patientInfo = HttpClientServices.sendPost(url, headers, urlParameters);
                            System.err.println("patientInfo : " + patientInfo);
                            // Validate 
                            org.json.JSONObject patientInfoHead = new org.json.JSONObject(patientInfo.getString("head"));
                            if ("1".equals(patientInfoHead.getString("errorcode"))) {
                                throw new RuntimeException("BedNo. has already used");
                            }
                            org.json.JSONObject patientInfoBody = new org.json.JSONObject(patientInfo.getString("body"));
                            System.err.println("patientInfoBody : " + patientInfoBody);
                            patientId = patientInfoBody.has("patientId") ? patientInfoBody.getString("patientId") : patientId;

                            patient.put("patientId", patientId);
                            patient.put("vsId", patientInfoBody.get("vsId"));
                            patient.put("dcId", patientInfoBody.get("dcId"));
                            patient.put("nsId", patientInfoBody.get("nsId"));
                            body.put("body", patient);
                        }
                    }

                    Trace.info("body : " + body);
                    return result;
                    } */
                    return 0;
                }

            };
            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body.get("body"));
                out.println(result.toJSONString());
                return;
            } else if (fsExecuter.effectedTransactions() == -409) {
                header.setErrorflag("Y");
                header.setErrorcode("409");
                header.setErrordesc("HN already exists");
                result.put("head", header);
                response.setStatus(409);
                out.println(result.toJSONString());
                return;
            }
        }
//            else {
//                String fs_forwarder = "/api/patients_c.jsp";
//                RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
//                rd.forward(request, response);
//            }

    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
