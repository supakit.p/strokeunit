<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<c:if test="${fsScreen.init('sfte007',pageContext.request,pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE007Keeper" scope="session" class="com.fs.bean.SFTE007ABean"/>

<div id="reset_dialog_layer" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modalheader">${fsLabel.getText("reset_dialog_title","Reset Password")}</h4>
			</div>
			<form id="fsresetform" role="form" data-toggle="validator" name="fsresetform" method="post" action="pmse001_reset_c.jsp">
				<input type="hidden" name="fsAction" value="update"/>
				<input type="hidden" name="fsAjax" value="true"/>
				<input type="hidden" name="fsDatatype" value="json"/>	
				<input type="hidden" name="userid" value="${fsSFTE007Keeper.getString('userid')}"/>
				<div class="row row-heighter center-block" style="margin-top: 15px;">
					<div class="col-md-3 col-height col-label text-right">
						<fs:label tagid="emaildialog_label" tagclass="control-label" required="true">Email</fs:label>
					</div>
					<div class="col-md-8 col-height">
						<input class="form-control input-sm ikeyclass alert-input email" id="emaildialog" name="email" placeholder="Email" autocomplete="off" size="25" value="${fsSFTE007Keeper.getString('email')}"/>
						<div id="emaildialog_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('emaildialog_alert','You can not leave this empty')}</div>
					</div>
				</div>
			</form>			
			<div class="row-heighter modal-footer" >
				<div class="col-md-9 col-height pull-right">
					<input type="button" id="savebuttondialog" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebuttondialog','Reset')}"/>
					<input type="button" id="cancelbuttondialog" class="btn btn-dark btn-sm" data-dismiss="modal" value="${fsLabel.getText('cancelbuttondialog','Cancel')}"/>
				</div>
			</div>
		</div>
	</div>
</div>
