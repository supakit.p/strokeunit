<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE007" scope="request" class="com.fs.bean.SFTE007ABean"/>
<% String fs_express_readonly = fsScreen.getEquals("retrieve",fsGlobal.getFsAction()," readonly ",""); %>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
		<div class="row row-height">
			<div class="col-md-12 col-height">
					<div class="table-layer-class">
						<div class="row row-height">
							<div class="col-height col-md-3">
<input type="hidden" id="site" name="site" class="form-control input-md ikeyclass alert-input" value="${fsSFTE007.getString('site')}" <%=fs_express_readonly%> /><div id="site_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('site_alert','You can not leave this empty')}</div>
							</div>
							<div class="col-height col-md-3">
<input type="hidden" id="employeeid" name="employeeid" class="form-control input-md ikeyclass alert-input" value="${fsSFTE007.getString('employeeid')}" <%=fs_express_readonly%> /><div id="employeeid_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('employeeid_alert','You can not leave this empty')}</div>
							</div>
						</div>
					</div>
			</div>
		</div>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
