<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%
com.fs.dev.TheUtility smutil = new com.fs.dev.TheUtility("AUTH");
smutil.createAllSites(fsAccessor, "sfte007", request, "ALL_SITES_CATEGORY", com.fs.dev.Content.emptyContent());
smutil.createUserStatuses(fsAccessor, "sfte007", request, "USER_STATUS_CATEGORY");
smutil.createGroupPublics(fsAccessor, "sfte007", request, "PUBLIC_GROUP_CATEGORY");
%>
