var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
var is_using_dialog = true;
//#(10000) programmer code begin;
//#(10000) programmer code end;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("sfte007"); }catch(ex) { }
	initialApplication();
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
});
function initialApplication() {
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	setupComponents();
	setupAlertComponents();
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
}
function setupComponents() {
	//#(50000) programmer code begin;
	//#(50000) programmer code end;
	$("#searchbutton").click(function(evt) {
		search();  return false;
	});
	$("#insertbutton").click(function(evt) {
		insert();  return false;
	});
	$("#savebutton").click(function() {
		save();  return false;
	});
	$("#cancelbutton").click(function() {
		cancel();  return false;
	});
	$("#updatebutton").click(function() {
		update();  return false;
	});
	$("#deletebutton").click(function() {
		deleted();  return false;
	});
	$("#findbutton").click(function() {
		find();  return false;
	});
	//#(60000) programmer code begin;
	setupEntryComponents();
	//#(60000) programmer code end;
}
function search(aform) {
	//#(70000) programmer code begin;
	//#(70000) programmer code end;
	if(!aform) aform = fssearchform;
	startWaiting();
	jQuery.ajax({
		url: "sfte007_c.jsp",
		type: "POST",
		data: $(aform).serialize(),
		dataType: "html",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) {
			submitFailure(transport,status,errorThrown);
		},
		success: function(data,status,transport){
			searchComplete(transport,data);
		}
	});	
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
}
function searchComplete(xhr,data) {
	//#(90000) programmer code begin;
	//#(90000) programmer code end;
	stopWaiting();
	$("#listpanel").html(data);
	//#(100000) programmer code begin;
	//#(100000) programmer code end;
}
function insert() {
	//#(110000) programmer code begin;
	//#(110000) programmer code end;
	var aform = fslistform;
	aform.fsRowid.value = "";
	aform.fsAjax.value = "false";
	aform.fsDatatype.value = "text";
	aform.site.value = "";
	aform.employeeid.value = "";
	aform.action = "sfte007_insert.jsp";
	if(is_using_dialog) {
		aform.fsAjax.value = "true";
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte007_insert_dialog.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) {
				submitFailure(transport,status,errorThrown);
			},
			success: function(data,status,transport){
				stopWaiting();
				$("#dialogpanel").html(data);
				setupDialogComponents();
				$("#fsmodaldialog_layer").modal("show");
			}
		});
		return;
	}
	aform.submit();
	//#(120000) programmer code begin;
	//#(120000) programmer code end;
}
function clearingFields() {
	//#(130000) programmer code begin;
	//#(130000) programmer code end;
	fsentryform.reset();
	//#(140000) programmer code begin;
	//#(140000) programmer code end;
}
function cancel() {
	//#(150000) programmer code begin;
	//#(150000) programmer code end;
	confirmCancel(function() {
		window.open("sfte007_list.jsp?seed="+Math.random(),"_self");
	});
	//#(160000) programmer code begin;
	//#(160000) programmer code end;
}
function validSaveForm(callback) {
	//#(170000) programmer code begin;
	//#(170000) programmer code end;
	return true;
	//#(180000) programmer code begin;
	//#(180000) programmer code end;
}
function save(aform) {
	//#(190000) programmer code begin;
	if(!validateOptions()) return false;
	//#(190000) programmer code end;
	if(!aform) aform = fsentryform;
	if(!validNumericFields(aform)) return false;
	validSaveForm(function() {
		//#(195000) programmer code begin;
		//#(195000) programmer code end;
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte007_insert_c.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) {
					submitFailure(transport,status,errorThrown);
				},
				success: function(data,status,transport){
					stopWaiting();
					//#(195300) programmer code begin;
					try { window.parent.notifyChanged("sfte007"); }catch(ex) { }
					//#(195300) programmer code end;
					successbox(function() { 
						if(is_using_dialog) {
							$("#fsmodaldialog_layer").modal("hide");
							try{
								fssearchform.fsPage.value = fslistform.fsPage.value;
								search();
							}catch(ex) { }
							return;
						}
					});
					//#(195500) programmer code begin;
					//#(195500) programmer code end;
				}
			});
		});
	});
	return false;
	//#(200000) programmer code begin;
	//#(200000) programmer code end;
}
function validSendForm(callback) {
	//#(210000) programmer code begin;
	//#(210000) programmer code end;
	return true;
	//#(220000) programmer code begin;
	//#(220000) programmer code end;
}
function update(aform) {
	//#(230000) programmer code begin;
	if(!validateOptions()) return false;
	//#(230000) programmer code end;
	if(!aform) aform = fsentryform;
	if(!validNumericFields(aform)) return false;
	validSaveForm(function() {
		//#(235000) programmer code begin;
		//#(235000) programmer code end;
		confirmUpdate(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte007_update_c.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) {
					submitFailure(transport,status,errorThrown);
				},
				success: function(data,status,transport){ 
					stopWaiting();
					//#(235300) programmer code begin;
					try { window.parent.notifyChanged("sfte007"); }catch(ex) { }
					//#(235300) programmer code end;
					successbox(function() { 
						if(is_using_dialog) {
							$("#fsmodaldialog_layer").modal("hide");
							try{
								fssearchform.fsPage.value = fslistform.fsPage.value;
								search();
							}catch(ex) { }
							return;
						}
						window.open("sfte007_search.jsp?seed="+Math.random(),"_self");
					});
					//#(235500) programmer code begin;
					//#(235500) programmer code end;
				}
			});
		});
	});
	return false;
	//#(240000) programmer code begin;
	//#(240000) programmer code end;
}
function submitRetrieve(rowIndex,site,employeeid) {
	//#(250000) programmer code begin;
	//#(250000) programmer code end;
	var aform = fslistform;
	aform.fsRowid.value = ""+rowIndex;
	aform.fsAjax.value = "false";
	aform.fsDatatype.value = "text";
	aform.site.value = site;
	aform.employeeid.value = employeeid;
	aform.action = "sfte007_retrieve_c.jsp";
	if(is_using_dialog) {
		aform.fsAjax.value = "true";
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte007_retrieve_dialog_c.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) {
				submitFailure(transport,status,errorThrown);
			},
			success: function(data,status,transport){
				stopWaiting();
				$("#dialogpanel").html(data);
				setupDialogComponents();
				$("#fsmodaldialog_layer").modal("show");
			}
		});
		return;
	}
	aform.submit();
	//#(260000) programmer code begin;
	//#(260000) programmer code end;
}
function submitChapter(aform,index) {
	//#(270000) programmer code begin;
	//#(270000) programmer code end;
	startWaiting();
	var xhr = jQuery.ajax({
		url: "sfte007_page.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: $(aform).serialize(),
		dataType: "html",
		error : function(transport,status,errorThrown) {
			submitFailure(transport,status,errorThrown);
		},
		success: function(data,status,transport){
			stopWaiting();
			$("#listpanel").html(data);
		}
	});
	//#(280000) programmer code begin;
	//#(280000) programmer code end;
}
function submitOrder(fsParams) {
	//#(290000) programmer code begin;
	//#(290000) programmer code end;
	startWaiting();
	var xhr = jQuery.ajax({
		url: "sfte007_cd.jsp",
		type: "POST",
		contentType: defaultContentType,
		data: fsParams,
		dataType: "html",
		error : function(transport,status,errorThrown) {
			submitFailure(transport,status,errorThrown);
		},
		success: function(data,status,transport){
			stopWaiting();
			$("#listpanel").html(data);
		}
	});
	return false;
	//#(300000) programmer code begin;
	//#(300000) programmer code end;
}
function submitDelete(fsParams,rowIndex) {
	//#(310000) programmer code begin;
	//#(310000) programmer code end;
	confirmDelete([fsParams[0]],function() {
		deleteRecord(fsParams);
	});
	//#(320000) programmer code begin;
	//#(320000) programmer code end;
}
function deleteRecord(fsParams) {
	//#(330000) programmer code begin;
	//#(330000) programmer code end;
	startWaiting();
	jQuery.ajax({
		url: "sfte007_delete_c.jsp",
		type: "POST",
		data: "fsAction=delete&fsDatatype=json"+"&site="+fsParams[0]+"&employeeid="+fsParams[1],
		dataType: "html",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) {
			submitFailure(transport,status,errorThrown);
		},
		success: function(data,status,transport){
			stopWaiting();
			//#(333000) programmer code begin;
			//#(333000) programmer code end;
			fssearchform.fsPage.value = fslistform.fsPage.value;
			search();
			//#(335000) programmer code begin;
			//#(335000) programmer code end;
		}
	});
	//#(340000) programmer code begin;
	//#(340000) programmer code end;
}
function deleted(aform) {
	//#(345000) programmer code begin;
	//#(345000) programmer code end;
	if(!aform) aform = fsentryform;
	confirmUpdate(function() {
		//#(347000) programmer code begin;
		//#(347000) programmer code end;
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte007_delete_c.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) {
				submitFailure(transport,status,errorThrown);
			},
			success: function(data,status,transport){ 
				stopWaiting();
				//#(347500) programmer code begin;
				//#(347500) programmer code end;
				successbox(function() { window.open("sfte007_search.jsp?seed="+Math.random(),"_self"); });
				//#(347700) programmer code begin;
				//#(347700) programmer code end;
			}
		});
	});
	return false;
	//#(348000) programmer code begin;
	//#(348000) programmer code end;
}
function find(aform) {
	if(!aform) aform = fsentryform;
	//#(350000) programmer code begin;
	//#(350000) programmer code end;
	var paras = [
		{name:"site", value: $("#site").val()},
		{name:"employeeid", value: $("#employeeid").val()},
		{name:"clear", value: "true" }
	];
	//#(360000) programmer code begin;
	//#(360000) programmer code end;
	submitWindow({ params: paras, url: "sfte007_search.jsp", windowName: "_self" });
	//#(370000) programmer code begin;
	//#(370000) programmer code end;
}
function setupDialogComponents() {
	//#(380000) programmer code begin;
	//#(380000) programmer code end;
	$("#savebutton").click(function() {
		save(); return false;
	});
	$("#updatebutton").click(function() {
		update(); return false;
	});
	setupAlertComponents($("#dialogpanel"));
	$("#dialogpanel").find(".modal-dialog").draggable();
	//#(385000) programmer code begin;
	setupEntryComponents();
	//#(385000) programmer code end;
}
function exportExcelHandler() {
	var fs_params = null;
	//#(386000) programmer code begin;
	//#(386000) programmer code end;
	openNewWindow({ url: "sfte007_export_excel.jsp", windowName: "sfte007_excel_window", params:  fs_params });
	//#(387000) programmer code begin;
	//#(387000) programmer code end;
	return false;
}
function exportPdfHandler() {
	var fs_params = null;
	//#(388000) programmer code begin;
	//#(388000) programmer code end;
	openNewWindow({ url: "sfte007_export_pdf.jsp", windowName: "sfte007_pdf_window", params:  fs_params });
	//#(389000) programmer code begin;
	//#(389000) programmer code end;
	return false;
}
//#(390000) programmer code begin;
function setupEntryComponents() {
	$("#resetpasswordbutton").click(function(){ 
		startResetPassword();		
	});
}
function validateOptions() {	
	return true;
}
function startResetPassword() {
	startWaiting();
	var xhr = jQuery.ajax({
		url: "sfte007_reset_dialog.jsp",
		type: "POST",
		dataType: "html",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data,status,transport){ 
			stopWaiting();
			$("#resetdialoglayer").html(data);			
			displayResetPasswordDialog();
		}
	});
}
function displayResetPasswordDialog() {
	$("#emaildialog").focus(function(){
		$("#emaildialog_alert").hide();
	});
	$("#savebuttondialog").unbind("click");
	$("#savebuttondialog").bind("click",function() { 
		var email = $("#emaildialog").val();
		if($.trim(email)=="") {
			$("#emaildialog_alert").show();		
			return false;
		}
		resetPassword();
		return false;		
	});
	$("#reset_dialog_layer").modal("show");	
}
function resetPassword(aform) {
	if(!aform) aform = fsresetform;
	startWaiting();
	var xhr = jQuery.ajax({
		url: "sfte007_reset_c.jsp",
		type: "POST",
		data: $(aform).serialize(),
		dataType: "html",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data,status,transport){ 
			stopWaiting();
			$("#reset_dialog_layer").modal("hide");	
			alertmsg("QS0200","Reset password success");
		}
	});
}
//#(390000) programmer code end;

