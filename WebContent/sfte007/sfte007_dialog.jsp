<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<div id="fsmodaldialog_layer" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xm">
		<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modalheadertitle">
					<c:if test="${fsScreen.isNullOrEmpty(fsSFTE007.getString('userid'))}">
						${fsLabel.getText("modalheadertitle_new","New")}
					</c:if>
					<c:if test="${!fsScreen.isNullOrEmpty(fsSFTE007.getString('userid'))}">
						${fsLabel.getText("modalheadertitle_edit","Edit")}
					</c:if>
				</h4>
			</div>
			<div id="entrydialoglayer" class="entry-dialog-layer">
			<form id="fsentryform" role="form" data-toggle="validator" name="fsentryform" method="post">
				<c:if test="${fsScreen.isNullOrEmpty(fsSFTE007.getString('userid'))}">
					<input type="hidden" name="fsAction" value="enter"/>
				</c:if>
				<c:if test="${!fsScreen.isNullOrEmpty(fsSFTE007.getString('userid'))}">
					<input type="hidden" name="fsAction" value="update"/>
				</c:if>
				<input type="hidden" name="fsAjax" value="true"/>
				<input type="hidden" name="fsDatatype" value="json"/>
				<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
				<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
				<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
				<jsp:include page="sfte007_key.jsp"/>
<%
//#(30000) programmer code begin;
//#(30000) programmer code end;
%>
				<jsp:include page="sfte007_kit.jsp"/>
<%
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
			</form>	
			</div>
			<div class="row-heighter modal-footer" >
				<div class="col-md-9 col-height pull-right">
<%
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
					<c:if test="${fsScreen.isNullOrEmpty(fsSFTE007.getString('userid'))}">
						<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save')}"/>
					</c:if>
					<c:if test="${!fsScreen.isNullOrEmpty(fsSFTE007.getString('userid'))}">
						<input type="button" id="updatebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatebutton','Update')}"/>
					</c:if>
					<input type="button" id="canceldialogbutton" class="btn btn-dark btn-sm" data-dismiss="modal" value="${fsLabel.getText('canceldialogbutton','Cancel')}"/>
<%
//#(70000) programmer code begin;
//#(70000) programmer code end;
%>
				</div>
			</div>
		</div>
	</div>
</div>
<%
//#(90000) programmer code begin;
//#(90000) programmer code end;
%>
