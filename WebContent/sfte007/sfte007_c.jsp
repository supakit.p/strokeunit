<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.dba.*"%>
<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte007', pageContext.request, pageContext.response, false)}"></c:if>
<%if(request.getParameter("clear")!=null) {
	session.removeAttribute("fsSFTE007Bean");
}%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE007Bean" scope="session" class="com.fs.bean.SFTE007Bean"/>
<jsp:setProperty name="fsSFTE007Bean" property="*"/>
<%
fsGlobal.setFsAction(GlobalBean.COLLECT_MODE);
//#(10000) programmer code begin;
//#(10000) programmer code end;
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte007");
fsGlobal.setFsSection("AUTH");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte007/sfte007_d.jsp";
//#(20000) programmer code begin;
//#(20000) programmer code end;
fsGlobal.attainPage(request,"fsGlobalsfte007");
try { 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	fsSFTE007Bean.forceObtain(fsGlobal);
	fsSFTE007Bean.obtainFrom(request);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsSFTE007Bean.obtain(fs_map);
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
	fsSFTE007Bean.obtain(session,request);
	fsSFTE007Bean.transport(fsGlobal);
	//#(50000) programmer code begin;
	//#(50000) programmer code end;
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
	throw ex;
}
fsPager.setRows(fsSFTE007Bean.size());
fsGlobal.adjustPage(fsPager);
fsSFTE007Bean.obtain(session,request);
//#(60000) programmer code begin;
//#(60000) programmer code end;
if(fsIsJSONData) {
	out.print(fsSFTE007Bean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE007Bean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE007Bean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE007Bean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
//#(70000) programmer code begin;
//#(70000) programmer code end;
%>
