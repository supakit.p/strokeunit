<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.initial('sfte007', pageContext.request, pageContext.response, true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE007" scope="request" class="com.fs.bean.SFTE007ABean"/>
<%
//#(5000) programmer code begin;
com.fs.dev.TheUtility smutil = new com.fs.dev.TheUtility("AUTH");
//#(5000) programmer code end;
//#(10000) programmer code begin;
String fs_userid = fsSFTE007.getUserid();
if(fs_userid==null || fs_userid.trim().length()<=0) fs_userid = fsSFTE007.getEmail();
java.util.Map fs_usergroupmap = new java.util.LinkedHashMap();
final boolean eng = smutil.isEng(request);
final String fs_site = fsAccessor.getFsSite();
final String fs_headsite = fsAccessor.getFsHeadSite();
com.fs.bean.util.GlobalBean fsGlobalAction = smutil.createGlobal(fsAccessor,"sfte007");
fsGlobalAction.setFsAction(com.fs.bean.util.GlobalBean.COLLECT_MODE);
com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
	public int collect(java.sql.Connection connection) throws Exception {
		int result = 0;
		com.fs.bean.misc.KnSQL knsql = new com.fs.bean.misc.KnSQL(this);
		knsql.append("select tgroup.groupname,tgroup.seqno ");
		if(eng) {
			knsql.append(",tgroup.nameen as grouptext ");
		} else {
			knsql.append(",tgroup.nameth as grouptext ");
		}
		knsql.append("from tgroup,tgroupcomp ");
		knsql.append("where ( tgroup.privateflag is null or tgroup.privateflag != '1' ) ");
		knsql.append("and tgroup.groupname = tgroupcomp.groupname ");
		knsql.append("and tgroupcomp.site = ?site ");
		knsql.setParameter("site",fs_site);			
		knsql.append("order by seqno ");
		try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
			while(rs.next()) {
				result++;
				com.fs.bean.ExecuteData aData = new com.fs.bean.ExecuteData();
				aData.setDataLabeled(true);
				aData.fetchResult(rs);
				add(aData);				
			}
		}	
		if(result<=0) {
			if(fs_headsite!=null && fs_headsite.trim().length()>0) {
				knsql.clearParameters();
				knsql.setParameter("site",fs_headsite);
				try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
					while(rs.next()) {
						result++;
						com.fs.bean.ExecuteData aData = new com.fs.bean.ExecuteData();
						aData.setDataLabeled(true);
						aData.fetchResult(rs);
						add(aData);				
					}
				}	
			}
		}
		return result;
	}	
};
com.fs.bean.ctrl.TheTransportor.transport(fsGlobalAction,fsExecuter);
if(fsExecuter.effectedTransactions()>0) {
	java.util.Enumeration enums = fsExecuter.childElements();
	if(enums!=null) {
		while(enums.hasMoreElements()) {
			com.fs.bean.ctrl.BeanData b = (com.fs.bean.ctrl.BeanData)enums.nextElement();
			String groupname = b.getString("groupname");
			String grouptext = b.getString("grouptext");
			fs_usergroupmap.put(groupname,grouptext);			
		}
	}
}
if(fs_usergroupmap.isEmpty()) {
	java.util.Map fs_publicgroupmap = smutil.getCategory(request, "PUBLIC_GROUP_CATEGORY");
	if(fs_publicgroupmap!=null) fs_usergroupmap.putAll(fs_publicgroupmap);
}
session.setAttribute("USER_GROUP_CATEGORY",fs_usergroupmap);
//#(10000) programmer code end;
%>
	<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-7">
<fs:label for="userid" tagid="userid_label" tagclass="control-label">User ID</fs:label>
<div class="input-group">
<input type="text" id="username" name="username" class="form-control input-md irequired alert-input" maxlength="50" value="${fsSFTE007.getString('username')}" />
	<div class="input-group-required"><label class="required">*</label></div>
</div>
<div id="username_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('username_alert','You can not leave this empty')}</div>

<input type="hidden" id="userid" name="userid" class="form-control input-md" value="<%=fs_userid %>" />
						</div>
					</div>
				</div>
		</div>
	</div>
	<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-4">
<input type="text" id="usertname" name="usertname" class="form-control input-md" value="${fsSFTE007.getString('usertname')}" readonly/>
						</div>
						<div class="col-height col-md-4">
<input type="text" id="usertsurname" name="usertsurname" class="form-control input-md" value="${fsSFTE007.getString('usertsurname')}" readonly/>
						</div>
					</div>
				</div>
		</div>
	</div>
	<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-4">
<fs:label for="userpassword" tagid="userpassword_label" tagclass="control-label">Password</fs:label><input type="text" id="userpassword" name="userpassword" class="form-control input-md" disabled value="${fsSFTE007.getString('userpassword')}" />
						</div>
					</div>
				</div>
		</div>
	</div>
	<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-7">
<fs:label for="email" tagid="email_label" tagclass="control-label">Email</fs:label><input type="text" id="email" name="email" class="form-control input-md" value="${fsSFTE007.getString('email')}" readonly/>
						</div>
					</div>
				</div>
		</div>
	</div>
	<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-3">
<fs:label for="status" tagid="status_label" tagclass="control-label">Status</fs:label><fs:select tagid="status" name="status" tagclass="form-control input-md" section="USER_STATUS_CATEGORY">${fsSFTE007.getString('status')}</fs:select>
						</div>
						<div class="col-height col-md-4">
<fs:label for="effectdate" tagid="effectdate_label" tagclass="control-label">Effective Date</fs:label><fs:date tagid="effectdate" name="effectdate" tagclass="form-control input-md">${fsSFTE007.getString('effectdate')}</fs:date>
						</div>
					</div>
				</div>
		</div>
	</div>

	<div class="row row-height">
		<div id="accessgroups" class="col-md-12 col-height">
				<fieldset id="accessgroupsheaderinfo" style="min-height: 35px; padding-left: 5px;">
					<legend style="margin-bottom:0px;">
						<fs:label tagid="accessgroups_sectionlabel" tagclass="control-label">Privileges</fs:label>
					</legend>
				</fieldset>									
				<div class="table-layer-class" style="padding-left: 5px;">
<%
	java.util.Map fs_groupmap = smutil.getCategory(request, "USER_GROUP_CATEGORY");
	if(fs_groupmap!=null) {
		int isz = fs_groupmap.size();
		int counter = isz;
		int max = 3;
		int index = 0;
		java.util.Iterator it = fs_groupmap.keySet().iterator();
		do {
%>
					<div class="row row-height">
<%			
			for(int k=0;k<max;k++) {
				if(it.hasNext()) {
					index++;
					String key = (String)it.next();
					String value = (String)fs_groupmap.get(key);
%>
						<div class="col-height col-md-4">
<div class="checkbox"><input type="checkbox" value="<%=key%>" id="usergroups_<%=index%>" name="usergroups" class="form-control input-md" <%=fsSFTE007.getChecked("usergroups", key)%>/><label for="usergroups_<%=index%>" class="control-label"><%=value%></label></div>
						</div>
<%					
				}
			}
%>
					</div>
<%
			counter -= max;
		}while(counter>0);
	}
%>				
				</div>
		</div>
	</div>
	<div class="row row-height">
		<div id="accessgroups_alert" role="alert" class="has-error alert-layer" style="display:none;">${fsLabel.getText('accessgroups_alert','You can not leave this empty')}</div>
	</div>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
