<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.dba.*"%>
<%@ page import="com.fs.dev.mail.*" %>
<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte007',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsReset" scope="request" class="com.fs.dev.auth.ResetPasswordBean"/>
<jsp:setProperty name="fsReset" property="*"/>
<%
fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
//#(10000) programmer code begin;
//#(10000) programmer code end;
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte007");
fsGlobal.setFsSection("AUTH");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte007/sfte007_de.jsp";
//#(20000) programmer code begin;
//#(20000) programmer code end;
try { 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	fsReset.forceObtain(fsGlobal);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsReset.obtain(fs_map);
	//#(40000) programmer code begin;
	com.fs.bean.SFTE007ABean fsSFTE007Keeper = (com.fs.bean.SFTE007ABean)session.getAttribute("fsSFTE007Keeper");
	if(fsSFTE007Keeper!=null) {
		//#(50000) programmer code begin;
		//#(50000) programmer code end;
		if((!fsReset.getString("userid").equals(fsSFTE007Keeper.getString("userid")))) {
			throw new BeanException("Transaction conflict",7003);
		}
		//#(60000) programmer code begin;
		//#(60000) programmer code end;
	}
	//#(40000) programmer code end;
	fsReset.obtain(session,request);
	fsReset.transport(fsGlobal);
	//#(50000) programmer code begin;
	if(fsReset.effectedTransactions()>0) {
		java.util.Map map = null;
		java.sql.Connection connection = null;
		try {
			connection = DBConnection.getConnection(fsGlobal.getFsSection());
			map = (java.util.Map)XMLConfig.create(request).getConfigure("RESETPASSWORDMAIL",connection);
		} finally { 
			if(connection!=null) { try { connection.close(); }catch(Exception ex) {} }
		}
		Trace.info("RESETPASSWORDMAIL = "+map);
		if(map!=null) {
			String fs_email = fsReset.getEmail();
			if(fs_email!=null && fs_email.trim().length()>0) {
				StringBuilder msg = new StringBuilder();
				msg.append("Dear, "+fsReset.getUsername()+"<br>");
				msg.append("Confirm your password was changed<br>");
				msg.append("user = "+fsReset.getUserid()+"<br>");
				msg.append("new password = "+fsReset.getPassword()+"<br>");
				msg.append("       yours sincerely,<br>");
				Trace.info("reset password : "+fsReset.getUserid()+" ("+fs_email+")");
				SendMail mailer = new SendMail();
				mailer.obtain(map);
				mailer.setTo(fs_email);
				mailer.setMessage(msg.toString());
				mailer.start();
			}
		}
	}	
	//#(50000) programmer code end;
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
	throw ex;
}
//#(60000) programmer code begin;
//#(60000) programmer code end;
fsReset.obtain(session,request);
//#(70000) programmer code begin;
//#(70000) programmer code end;
if(fsIsJSONData) {
	out.print(fsReset.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsReset.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsReset.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsReset.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
//#(80000) programmer code begin;
//#(80000) programmer code end;
%>
