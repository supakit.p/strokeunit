<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE007Bean" scope="session" class="com.fs.bean.SFTE007Bean"/>
<% String fs_express_readonly = fsScreen.getEquals("retrieve",fsGlobal.getFsAction()," readonly ",""); %>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
			<div class="row row-height">
				<div class="col-md-12 col-height">
						<div class="table-layer-class">
							<div class="row row-height">
								<div class="col-height col-md-3">
<fs:label for="sites" tagid="sites_label" tagclass="control-label">Company</fs:label><fs:select tagid="sites" name="site" tagclass="form-control input-md" section="ALL_SITES_CATEGORY">${fsSFTE007Bean.getString('site')}</fs:select>
								</div>
								<div class="col-height col-md-3">
<fs:label for="employeecodes" tagid="employeecodes_label" tagclass="control-label">Employee ID</fs:label><input type="text" id="employeecodes" name="employeecode" class="form-control input-md" picture="(30)X" maxlength="30" value="${fsSFTE007Bean.getString('employeecode')}" />
								</div>
								<div class="col-height col-md-3">
<fs:label for="usertnames" tagid="usertnames_label" tagclass="control-label">First Name</fs:label><input type="text" id="usertnames" name="usertname" class="form-control input-md" maxlength="50" value="${fsSFTE007Bean.getString('usertname')}" />
								</div>
								<div class="col-height col-md-3">
<fs:label for="usertsurnames" tagid="usertsurnames_label" tagclass="control-label">Last Name</fs:label><input type="text" id="usertsurnames" name="usertsurname" class="form-control input-md" maxlength="50" value="${fsSFTE007Bean.getString('usertsurname')}" />
								</div>
							</div>
						</div>
				</div>
			</div>

<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
					<div class="row">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 10px;">
<%
//#(30000) programmer code begin;
//#(30000) programmer code end;
%>
<%
//#(35000) programmer code begin;
//#(35000) programmer code end;
%>
							<div class="pull-right">
								<button type="button" id="searchbutton" class="btn btn-dark btn-sm" style="margin-right: 10px;"><i class="fa fa-search" aria-hidden="true" style="margin-right:10px;"></i>${fsLabel.getText('searchbutton','Search')}</button>
							</div>
<%
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
						</div>
					</div>
<%
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
