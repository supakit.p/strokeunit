<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>

	<div id="entrylayer" class="entry-layer">
			<table id="programtable" class="program-table-class" style="width:100%;" border="0">
				<tr class="rclass" id="filterrow">
					<td style="vertical-align:top; padding-bottom:3px;" id="filtercolumn1">
						<table class="header-table-class header-left-table">
							<tr class="topic-row">
								<td class="topic-column" nowrap>
									<fs:label tagclass="lclass" tagid="programfilterproperties_label">Filter Section</fs:label>
								</td>
								<td class="topic-control-column">									
									<a href="javascript:void(0)" onclick="deleteSelectedFilter()" title="Delete Selected Filter" style="float:right;"><em class="fa fa-minus-circle fa-class" aria-hidden="true"></em></a>
									<ul id="filterfieldmenulistitem" class="navbar-nav navbar-user" style="float:right; padding-right:20px;">	
										<li class="dropdown user-dropdown" id="filterfieldmenuitem">
											<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-plus-circle fa-class" aria-hidden="true" title="Add New"></em><strong class="caret"></strong></a>														
											<ul class="dropdown-menu">
												<li><a href="javascript:void(0)" onclick="addNewFilter()" title="Add New Filter"><em class="fa fa-pencil-square-o" aria-hidden="true"></em> New Input</a></li>
												<li><a href="javascript:void(0)" onclick="addNewFilterRow()" title="Add New Row"><em class="fa fa-align-justify" aria-hidden="true"></em> New Row</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
						</table>
					</td>
					<td style="vertical-align:top; padding-bottom:3px;" id="programcolumn1">
						<table class="header-table-class header-right-table">
							<tr class="topic-row">
								<td style="width:30px;">
									<a href="javascript:void(0)" id="togglefilter" class="left" onclick="toggleReservedAndFilter()" title="Toggle Reserved And Filter Panel" style="padding-left:5px;"><em class="fa fa-chevron-circle-left fa-class" aria-hidden="true"></em></a>
								</td>
								<td class="topic-column" nowrap>
									<fs:label tagclass="lclass" tagid="programproperties_label">Program Information</fs:label>
								</td>
								<td align="right">
									<a href="javascript:void(0);" onclick="programCollapseExpand(this)" class="pull-right"><em class="fa fa-chevron-circle-up fa-class" style="margin-right: 2px;"></em></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="rclass" id="filterfieldrow"> 
					<td class="inclass" style="vertical-align:top;" id="filtercolumn2">
						<div id="filtersectionlayer" class="section-layer-class" style="display:none;">
							<ul id="filtersectionlisting" style="margin-left:5px;">
								<!-- temporary filter section -->
							</ul>
						</div>
						<div id="filtersublistcolumn" class="sublist-layer-class" style="overflow: auto; height:250px;">
							<div id="filtersublistinglayer" class="portal-area sublayer-class">
								<ul id="filtersubitemlisting" class="sublisting-class">
								</ul>
							</div>		
						</div>						
					</td>
					<td style="width: 45%; vertical-align:top;" id="programcolumn2">
						<form id="fsentryform" name="fsentryform" method="post">	
							<input type="hidden" name="fsAction" value="enter"/>
							<input type="hidden" name="fsAjax" value="true"/>
							<input type="hidden" name="fsDatatype" value="text"/>
							<input type="hidden" name="xmlfile" />	
							<input type="hidden" name="xmltext" />	

							<div class="row portal-area sub-entry-layer">
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="progid_label" >ID</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<input class="form-control input-md" id="progid" name="progid" placeholder="" autocomplete="off" size="10" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="progname_label" >Title</fs:label>
									</div>
									<div class="col-md-6 col-height">
										<input class="form-control input-md" id="progname" name="progname" placeholder="" autocomplete="off" size="15"/>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="tablename_label" >Table Name</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<input class="form-control input-md" id="tablename" name="tablename" placeholder="" autocomplete="off"/>
									</div>
								</div>								
								<div id="headlinerow" class="row row-height" style="display:none">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="headline_label">Head Line</fs:label>
									</div>
									<div class="col-md-7 col-height">
										<input class="form-control input-md" id="headline" placeholder="" autocomplete="off" size="35"/>
									</div>
								</div>
								<div id="subheadlinerow" class="row row-height" style="display:none">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="subheadline_label">Sub Head Line</fs:label>
									</div>
									<div class="col-md-7 col-height">
										<input class="form-control input-md" id="subheadline" placeholder="" autocomplete="off" size="35"/>
									</div>
								</div>	
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="displaybutton_label">Listing Action</fs:label>
									</div>
									<div class="col-md-8 col-height radio my-radio currenting-field">
											<table><tr><td>
												<input type="checkbox" class="form-control input-md" id="actioneditbutton"/>
											</td><td valign="middle">
												<fs:label tagclass="lclass control-label" tagid="actioneditbutton_label" for="actioneditbutton">Edit</fs:label>
											</td><td style="padding-left: 20px;">
												<input type="checkbox" class="form-control input-md" id="actiondeletebutton"/>
											</td><td>
												<fs:label tagclass="lclass control-label" tagid="actiondeletebutton_label" for="actiondeletebutton">Delete</fs:label>
											</td></tr></table>
									</div>
								</div>								
								<div class="row row-height">
									<div class="col-md-11 pull-right text-right" style="margin-right: 5px;">
										<input type="button" id="previewbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('previewbutton','Preview')}"/>
										&nbsp;&nbsp;
										<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save')}"/>
										&nbsp;&nbsp;
										<input type="button" id="saveasbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('saveasbutton','Save As')}" />
										&nbsp;&nbsp;
										<input type="button" id="generatebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('generatebutton','Generate')}" />
										&nbsp;&nbsp;
										<input type="button" id="cancelbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('cancelbutton','Cancel')}"/>
									</div>
								</div>
							</div>
						</form>
					</td>
				</tr>
				<tr class="rclass" id="keyrow">
					<td style="vertical-align:top; padding-bottom:3px;" id="keycolumn1">
						<table class="header-table-class header-left-table">
							<tr class="topic-row">
								<td class="topic-column" nowrap>
									<fs:label tagclass="lclass" tagid="programkeyproperties_label">Key Section</fs:label>
								</td>
								<td class="topic-control-column">									
									<a href="javascript:void(0)" onclick="deleteSelectedKey()" title="Delete Selected Key" style="float:right;"><em class="fa fa-minus-circle fa-class" aria-hidden="true"></em></a>
									<ul id="keyfieldmenulistitem" class="navbar-nav navbar-user" style="float:right; padding-right:20px;">	
										<li class="dropdown user-dropdown" id="keyfieldmenuitem">
											<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-plus-circle fa-class" aria-hidden="true" title="Add New"></em><strong class="caret"></strong></a>														
											<ul class="dropdown-menu">
												<li><a href="javascript:void(0)" onclick="addNewKey()" title="Add New Key"><em class="fa fa-pencil-square-o" aria-hidden="true"></em> New Input</a></li>
												<li><a href="javascript:void(0)" onclick="addNewKeyRow()" title="Add New Row"><em class="fa fa-align-justify" aria-hidden="true"></em> New Row</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
						</table>
					</td>
					<td style="vertical-align:top;">
						<table class="header-table-class header-right-table">
							<tr class="topic-row">
								<td class="topic-column" nowrap><fs:label tagclass="lclass" tagid="attributesproperties_label">Attributes &amp; Properties</fs:label></td>
								<td class="topic-control-column"></td>
								<td class="topic-column" align="right" style="padding-right:15px;"><label class="lclass" id="propertyheaderlabel"></label></td>
							</tr>
						</table>
					</td>					
				</tr>
				<tr class="rclass" id="keyfieldrow">
					<td class="inclass" style="vertical-align:top;" id="keycolumn2">
						<div id="keysectionlayer" class="section-layer-class" style="display:none;">
							<ul id="keysectionlisting" style="margin-left:5px;">
								<!-- temporary key section -->
							</ul>
						</div>
						<div id="keysublistcolumn" class="sublist-layer-class" style="overflow: auto; height:250px;">
							<div id="keysublistinglayer" class="portal-area sublayer-class">
								<ul id="keysubitemlisting" class="sublisting-class">
								</ul>
							</div>		
						</div>						
					</td>
					<td style="vertical-align:top; " rowspan="4">
						<table style="height:100%; width:100%;"><tr><td>
						<div id="property_layer_control">
							<div id="field_layer_control" class="row portal-area sub-entry-layer control-property">
								<div id="field_layer_panel">
									<div id="field_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldid_label" >ID</fs:label>
											</div>
											<div class="col-md-6 col-height">
												<input class="form-control input-md" id="fieldid" size="10" />
											</div>
											<div class="col-md-2 col-height text-right" style="margin-right: 5px;">
												<input type="button" id="updatefieldbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatefieldbutton','Update')}"/>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldname_label" >Name</fs:label>
											</div>
											<div class="col-md-6 col-height">
												<input class="form-control input-md" id="fieldname" size="10" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldlabel_label" >Label</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="fieldlabel" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldtype_label" >Type</fs:label>
											</div>
											<div class="col-md-5 col-height">
												<select class="form-control input-md" id="fieldtype">
													<option value="text">Text</option>
													<option value="radio">Radio Box</option>
													<option value="checkbox">Check Box</option>
													<option value="select">Select Box</option>
													<option value="list">List Box</option>
													<option value="image">Image File</option>
													<option value="hidden">Hidden</option>
												</select>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fielddatatype_label" >Data Type</fs:label>
											</div>
											<div class="col-md-5 col-height">
												<select class="form-control input-md" id="fielddatatype">
													<option value="string">String</option>
													<option value="int">Integer</option>
													<option value="decimal">Decimal</option>
													<option value="date">Date</option>
													<option value="time">Time</option>
												</select>
											</div>
										</div>
										<div class="row row-height" id="valuelayer">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldvalue_label" >Value</fs:label>
											</div>
											<div class="col-md-5 col-height">
												<input class="form-control input-md" id="fieldvalue"/>
											</div>
										</div>
										<div class="row row-height" id="optionvaluelayer">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldvalueoption_label" >Option Value</fs:label>
											</div>
											<div class="col-md-9 col-height">
													<table id="optiontable" width="100%" border="1">
														<thead>
															<tr>
																<th style="text-align:center;"><a href="javascript:void(0)" onclick="copyOption('optiontablebody')" style="float:left; padding-right:2px;"class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-files-o fa-class" aria-hidden="true" title="Copy Option"></em></a><fs:label tagclass="lclass" tagid="fieldvalueoption_headerlabel" >Value</fs:label></th>
																<th style="text-align:center;"><fs:label tagclass="lclass" tagid="fieldtextoption_headerlabel" >Text</fs:label></th>
																<th style="text-align:center;"><fs:label tagclass="lclass" tagid="fieldcontroloption_headerlabel" >Control</fs:label><a href="javascript:void(0)" onclick="pasteOption('optiontablebody')" style="float:right; padding-right:2px;"class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-clipboard fa-class" aria-hidden="true" title="Paste Option"></em></a></th>
																<th style="text-align:center; width:80px;">
																	<a href="javascript:void(0)" onclick="insertNewOption('optiontablebody')" style="padding-right:2px;"class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-plus-circle fa-class" aria-hidden="true" title="Add New Option"></em></a>
																</th>
															</tr>
														</thead>
														<tbody id="optiontablebody">
														</tbody>
													</table>
											</div>
										</div>
										<div class="row row-height" id="fieldtaglayer">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldtag_label" >Tag</fs:label>
											</div>
											<div class="col-md-5 col-height">
												<select class="form-control input-md" id="fieldtag">
													<option value=""></option>
													<option value="int">Integer</option>
													<option value="money">Decimal</option>
													<option value="date">Date</option>
													<option value="time">Time</option>
													<option value="year">Year</option>
													<option value="month">Month</option>
													<option value="hour">Hour</option>
													<option value="minute">Minute</option>
												</select>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldpicture_label" >Picture</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<input class="form-control input-md" id="fieldpicture" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-5 col-height radio my-radio currenting-field">
													<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="fieldcurrenting"/>
													</td><td>
														<fs:label tagclass="lclass control-label" tagid="fieldcurrenting_label" for="fieldcurrenting">Currenting</fs:label>
													</td></tr></table>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right decimal-field">
													<fs:label tagclass="lclass" tagid="fielddecimal_label" >Decimal</fs:label>
											</div>
											<div class="col-md-3 col-height decimal-field">
													<fs:int tagclass="form-control input-md" name="fielddecimal" tagid="fielddecimal"></fs:int>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right decimal-field">
													<fs:label tagclass="lclass" tagid="fielddecimalformat_label" >Decimal Format</fs:label>
											</div>
											<div class="col-md-5 col-height decimal-field">
													<input type="text" class="form-control input-md" id="fielddecimalformat"/>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right decimal-field">
													<fs:label tagclass="lclass" tagid="fieldprecision_label" >Precision</fs:label>
											</div>
											<div class="col-md-3 col-height decimal-field">
													<fs:int tagclass="form-control input-md" name="fieldprecision" tagid="fieldprecision"></fs:int>
											</div>
										</div>
										<div id="keyablecontrollayer" class="row row-height" style="display:none;">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-5 col-height radio my-radio currenting-field">
													<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="fieldkeyable"/>
													</td><td>
														<fs:label tagclass="lclass control-label" tagid="fieldkeyable_label" for="fieldkeyable">Keyable</fs:label>
													</td></tr></table>
											</div>
										</div>
										<div id="columnscontrollayer" class="row row-height control-property">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldcols_label" >Columns</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<input class="form-control input-md" id="fieldcols" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldwidth_label" >Width</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<fs:int tagclass="form-control input-md" name="fieldwidth" tagid="fieldwidth"></fs:int>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldheight_label" >Height</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<fs:int tagclass="form-control input-md" name="fieldheight" tagid="fieldheight"></fs:int>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldstyle_label" >Style</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="fieldstyle" />
											</div>
										</div>
										<div id="fieldcontrollayer" class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="fieldcontrol_label" >Control</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="fieldcontrol" />
											</div>
										</div>
										<div class="row row-height" id="readonly_layer">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="fieldshowing"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="fieldshowing_label" for="fieldshowing">Showing</fs:label>
												</td></tr></table>
											</div>
											<div class="col-md-3 col-height radio my-radio readonly-field">
													<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="fieldreadonly"/>
													</td><td>
														<fs:label tagclass="lclass control-label" tagid="fieldreadonly_label" for="fieldreadonly">Read Only</fs:label>
													</td></tr></table>
											</div>
										</div>
										<div id="validate_layer_control" class="row portal-area sub-entry-layer control-property">
											<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="fieldvalidatepropertylegend_label" >Validate Properties</fs:label></legend></fieldset>
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
												</div>
												<div class="col-md-3 col-height radio my-radio">
													<table><tr><td>
															<input type="checkbox" class="form-control input-md" id="fieldrequired"/>
														</td><td>
															<fs:label tagclass="lclass" tagid="fieldrequired_label" for="fieldrequired">Required</fs:label>
													</td></tr></table>
												</div>
												<div class="col-md-3 col-height radio my-radio">
													<table><tr><td>
															<input type="checkbox" class="form-control input-sd" id="fieldcompleted"/>
														</td><td>
															<fs:label tagclass="lclass" tagid="fieldcompleted_label" for="fieldcompleted">Completed</fs:label>
													</td></tr></table>
												</div>
											</div>
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right decimal-field">
														<fs:label tagclass="lclass" tagid="fieldmsg_label" >Message</fs:label>
												</div>
												<div class="col-md-8 col-height decimal-field">
														<input type="text" class="form-control input-md" id="fieldmsg"/>
												</div>
											</div>
										</div>
										<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="filterpropertylegend_label" >Filter Properties</fs:label></legend></fieldset>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
													<input type="checkbox" class="form-control input-md" id="fieldfiltering"/>
												</td><td>
													<fs:label tagclass="lclass" tagid="fieldfiltering_label" for="fieldfiltering">Filtering</fs:label>
												</td></tr></table>
											</div>											
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-sd" id="fieldlisting"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="fieldlisting_label" for="fieldlisting">Listing</fs:label>
												</td></tr></table>
											</div>
										</div>
										<div id="condition_row" class="row row-height">
											<div class="col-md-3 col-height col-label text-right condition-field">
												<fs:label tagclass="lclass" tagid="fieldcondition_label" >Condition</fs:label>
											</div>
											<div class="col-md-5 col-height condition-field">
												<select class="form-control input-md" id="fieldcondition">
													<option value=""></option>
													<option value="=">Equals(=)</option>
													<option value="!=">Not Equals(!=)</option>
													<option value=">">Greater Than</option>
													<option value="<">Lesser Than</option>
													<option value=">=">Greater Than Equals(&gt;=)</option>
													<option value="<=">Lesser Than Equals(&lt;=)</option>
													<option value="LIKE">Like</option>
													<option value="between">Between</option>
												</select>										
											</div>
										</div>
										<div id="ranged_row" class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="rangelabel_label" >Range Label</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="rangelabel" />									
											</div>
										</div>

										<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="fieldlabelpropertylegend_label" >Label Properties</fs:label></legend></fieldset>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-8 col-height radio my-radio currenting-field">
													<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="fieldheaderlabel"/>
													</td><td>
														<fs:label tagclass="lclass control-label" tagid="fieldheaderlabel_label" for="fieldheaderlabel">Header Label</fs:label>
													</td></tr></table>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right decimal-field">
													<fs:label tagclass="lclass" tagid="placement_label" >Label Placement</fs:label>
											</div>
											<div class="col-md-3 col-height decimal-field">
													<select class="form-control input-md" id="fieldplacement">
														<option value=""></option>
														<option value="prefix">Prefix</option>
														<option value="suffix">Suffix</option>
													</select>
											</div>
										</div>

									</div>
								</div>

								<div id="lookup_layer_control" class="row portal-area sub-entry-layer">
									<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="fieldlookuppropertylegend_label" >Lookup Properties</fs:label></legend></fieldset>
									<div id="lookuptabs">	
										<input type="hidden" id="lookuptype" value="service"/>
										<ul class="nav nav-tabs">
											<li id="lookupserviceitem" class="active">
												<a id="lookupservicelinker" href="#lookupservicelayer" data-toggle="tab" onclick="lookupServiceClick()"><fs:label tagclass="lclass" tagid="lookupservicelinker_label" >Lookup Service</fs:label></a>
											</li>
											<li id="lookuptableitem">
												<a id="lookuptablelinker" href="#lookuptablelayer" data-toggle="tab" onclick="lookupTableClick()"><fs:label tagclass="lclass" tagid="lookuptablelinker_label" >Lookup Table</fs:label></a>
											</li>
											<li id="lookupiniitem">
												<a id="lookupinilinker" href="#lookupinilayer" data-toggle="tab" onclick="lookupIniClick()"><fs:label tagclass="lclass" tagid="lookupinilinker_label" >Lookup INI</fs:label></a>
											</li>
											<li id="lookupcustomtem">
												<a id="lookupcustomlinker" href="#lookupcustomlayer" data-toggle="tab" onclick="lookupCustomClick()"><fs:label tagclass="lclass" tagid="lookupcustomlinker_label" >Lookup Custom</fs:label></a>
											</li>
										</ul>
									</div>
									<div id="lookup_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="lookupfield"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="lookupfield_label" for="lookupfield">Lookup</fs:label>
												</td></tr></table>
											</div>
											<div class="col-md-5 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-sd" id="lookupfieldmultiple"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="lookupfieldmultiple_label" for="lookupfieldmultiple">Multiple Selection</fs:label>
												</td></tr></table>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-8 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="lookupmapping"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="lookupmapping_label" for="lookupmapping">Mapping Lookup</fs:label>
												</td></tr></table>
											</div>
										</div>
										<div id="lookupservicelayer" class="tab-pane">
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="lookupurl_label" >Service URL</fs:label>
												</div>
												<div class="col-md-8 col-height">
													<input class="form-control input-md" id="lookupurl" />
												</div>
											</div>
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="lookupcategory_label" >Category</fs:label>
												</div>
												<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookupcategory" />
												</div>
											</div>
											<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
													</div>
													<div class="col-md-8 col-height radio my-radio">
														<table><tr><td>
																<input type="checkbox" class="form-control input-md" id="lookupgetting"/>
															</td><td>
																<fs:label tagclass="lclass" tagid="lookupgetting_label" for="lookupgetting">Getting Lookup</fs:label>
														</td></tr></table>
													</div>
											</div>

											<div id="lookupservicekeyfieldlayer">
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="lookupkeyfield_label" >Field Key</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookupkeyfield" />
													</div>
												</div>
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="lookupvaluefield_label" >Description</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookupvaluefield" />
													</div>
												</div>
											</div>
										</div>

										<div id="lookuptablelayer" class="tab-pane">
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="lookuptable_label" >Table Name</fs:label>
												</div>
												<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookuptablename" />
												</div>
											</div>
											<div id="lookuptablekeyfieldlayer">
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="lookuptablekeyfield_label" >Field Key</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookuptablekeyfield" />
													</div>
												</div>
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="lookuptablevaluefield_label" >Description</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookuptablevaluefield" />
													</div>
												</div>
											</div>
										</div>

										<div id="lookupinilayer" class="tab-pane">
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="lookupinifile_label" >File Name</fs:label>
												</div>
												<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookupinifile" />
												</div>
											</div>
											<div id="lookupinisectionlayer">
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="lookupinisection_label" >Section Name</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookupinisection" />
													</div>
												</div>
											</div>
										</div>

										<div id="lookupcustomlayer" class="tab-pane">
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="lookupcustomfile_label" >File Name</fs:label>
												</div>
												<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookupcustomfile" />
												</div>
											</div>
											<div id="lookupcustomsectionlayer">
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="lookupcustomsection_label" >Section Name</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="lookupcustomsection" />
													</div>
												</div>
											</div>
										</div>

										<div class="row row-height" id="lookupmappingrow">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-9 col-height">
													<table id="lookupoptiontable" width="100%" border="1">
														<thead>
															<tr>
																<th style="text-align:center;"><fs:label tagclass="lclass" tagid="lookupfieldoption_headerlabel" >Name</fs:label></th>
																<th style="text-align:center;"><fs:label tagclass="lclass" tagid="lookupfieldtextoption_headerlabel" >Text</fs:label></th>
																<th style="text-align:center;"><fs:label tagclass="lclass" tagid="lookupfieldcontroloption_headerlabel" >Control</fs:label></th>
																<th style="text-align:center; width:80px;">
																	<a href="javascript:void(0)" onclick="insertNewOption('lookupoptiontablebody')" style="padding-right:2px;"class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-plus-circle fa-class" aria-hidden="true" title="Add New Option"></em></a>
																</th>
															</tr>
														</thead>
														<tbody id="lookupoptiontablebody">
														</tbody>
													</table>
											</div>
										</div>

									</div>
								</div>
							</div>
							
							<div id="section_layer_control" class="row portal-area sub-entry-layer control-property">
								<div id="section_layer" class="row portal-area sub-entry-layer">
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="sectionlabel_label" >Label</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="sectionlabel" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="sectionid_label" >ID</fs:label>
										</div>
										<div class="col-md-6 col-height">
											<input class="form-control input-md" id="sectionid" size="10" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="sectionstyle_label" >Style</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="sectionstyle" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="sectionincreaseindent_label" >Indent</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<select id="sectionincreaseindent">
												<option value=""></option>
												<option value="margin-left: 25px;">Margin Left 25</option>
												<option value="margin-left: 50px;">Margin Left 50</option>
												<option value="margin-left: 75px;">Margin Left 75</option>
												<option value="margin-left: 100px;">Margin Left 100</option>
												<option value="margin-left: 125px;">Margin Left 125</option>
												<option value="margin-left: 150px;">Margin Left 150</option>
												<option value="margin-left: 175px;">Margin Left 175</option>
												<option value="margin-left: 200px;">Margin Left 200</option>
											</select>
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
										</div>
										<div class="col-md-8 col-height">
												<table><tr><td>
													<input type="checkbox" class="form-control input-md" id="sectiondesign" />
												</td><td style="padding-top:5px; padding-left: 2px;">
													<fs:label tagclass="lclass" tagid="sectiondesign_label" for="sectiondesign">Display Label Only On Design Time</fs:label>
												</td></tr></table>
										</div>
									</div>
								</div>
								<div class="row row-height sub-entry-layer">
									<div class="col-md-10 pull-right text-right" style="margin-right: 5px;">
										<input type="button" id="updatesectionbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatesectionbutton','Update')}"/>
									</div>
								</div>
							</div>

							<div id="part_layer_control" class="row portal-area sub-entry-layer control-property">
								<div id="part_layer" class="row portal-area sub-entry-layer">
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="partlabel_label" >Label</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="partlabel" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="partid_label" >ID</fs:label>
										</div>
										<div class="col-md-6 col-height">
											<input class="form-control input-md" id="partid" size="10" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="parttitle_label">Title</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="parttitle" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="partstyle_label" >Style</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="partstyle" />
										</div>
									</div>
								</div>
								<div class="row row-height sub-entry-layer">
									<div class="col-md-10 pull-right text-right" style="margin-right: 5px;">
										<input type="button" id="updatepartbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatepartbutton','Update')}"/>
									</div>
								</div>
							</div>
							
							<div id="subpart_layer_control" class="row portal-area sub-entry-layer control-property">
								<div id="subpart_layer" class="row portal-area sub-entry-layer">
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="subpartlabel_label" >Label</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="subpartlabel" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="subpartid_label" >ID</fs:label>
										</div>
										<div class="col-md-6 col-height">
											<input class="form-control input-md" id="subpartid" size="10" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="subpartstyle_label" >Style</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="subpartstyle" />
										</div>
									</div>
								</div>
								<div class="row row-height sub-entry-layer">
									<div class="col-md-10 pull-right text-right" style="margin-right: 5px;">
										<input type="button" id="updatesubpartbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatesubpartbutton','Update')}"/>
									</div>
								</div>
							</div>

							<div id="label_layer_control" class="row portal-area sub-entry-layer control-property">
								<div id="label_layer" class="row portal-area sub-entry-layer">
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="labellabel_label" >Label</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="labellabel" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="labelid_label" >ID</fs:label>
										</div>
										<div class="col-md-6 col-height">
											<input class="form-control input-md" id="labelid" size="10" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="labelstyle_label" >Style</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="labelstyle" />
										</div>
									</div>
								</div>
								<div class="row row-height sub-entry-layer">
									<div class="col-md-10 pull-right text-right" style="margin-right: 5px;">
										<input type="button" id="updatelabelbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatelabelbutton','Update')}"/>
									</div>
								</div>
							</div>

							<div id="image_layer_control" class="row portal-area sub-entry-layer control-property">
								<div id="image_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="image_label" >Image</fs:label>
											</div>
											<form id="imageform" name="imageform">
												<div class="col-md-8 col-height" id="imageinputlayer">
													<input type="file" class="form-control input-md" id="fieldimage" name="fieldimage"/>
												</div>
											</form>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="imagewidth_label" >Width</fs:label>
											</div>
											<div class="col-md-5 col-height">
												<fs:int tagclass="form-control input-md" name="imagewidth" tagid="imagewidth"></fs:int>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="imageheight_label" >Height</fs:label>
											</div>
											<div class="col-md-5 col-height">
												<fs:int tagclass="form-control input-md" name="imageheight" tagid="imageheight"></fs:int>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="imagestyle_label" >Style</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="imagestyle" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right"></div>
											<div class="col-md-7 col-height">
												<img id="fieldimagedisplay" src="../images/apps/default_image.png" width="200px" height="200px" alt="Application"/>
											</div>
										</div>
								</div>
								<div class="row row-height sub-entry-layer">
									<div class="col-md-10 pull-right text-right" style="margin-right: 5px;">
										<input type="button" id="updateimagebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('updateimagebutton','Update')}"/>
									</div>
								</div>
							</div>

							<div id="function_layer_control" class="row portal-area sub-entry-layer control-property">
								<div id="function_layer_panel">
									<div id="function_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="functionfieldid_label" >ID</fs:label>
											</div>
											<div class="col-md-6 col-height">
												<input class="form-control input-md" id="functionfieldid" size="10" />
											</div>
											<div class="col-md-2 col-height text-right" style="margin-right: 5px;">
												<input type="button" id="functionupdatefieldbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('functionupdatefieldbutton','Update')}"/>
											</div>
										</div>
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="functionfieldname_label" >Name</fs:label>
											</div>
											<div class="col-md-6 col-height">
												<input class="form-control input-md" id="functionfieldname" size="10" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="functionfieldlabel_label" >Label</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="functionfieldlabel" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="functionfieldstyle_label" >Style</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="functionfieldstyle" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="functionfieldwidth_label" >Width</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<fs:int tagclass="form-control input-md" name="functionfieldwidth" tagid="functionfieldwidth"></fs:int>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="functionfieldheight_label" >Height</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<fs:int tagclass="form-control input-md" name="functionfieldheight" tagid="functionfieldheight"></fs:int>
											</div>
										</div>
										<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="functonfilterpropertylegend_label" >Filter Properties</fs:label></legend></fieldset>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="functionfieldshowing"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="functionfieldshowing_label" for="functionfieldshowing">Showing</fs:label>
												</td></tr></table>
											</div>
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-sd" id="functionfieldlisting"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="functionfieldlisting_label" for="functionfieldlisting">Listing</fs:label>
												</td></tr></table>
											</div>
										</div>
										<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="functionpropertylegend_label" >Function Properties</fs:label></legend></fieldset>
										<div id="functioncondition_row" class="row row-height">
											<div class="col-md-3 col-height col-label text-right condition-field">
												<fs:label tagclass="lclass" tagid="functionfieldcondition_label" >Functon</fs:label>
											</div>
											<div class="col-md-5 col-height condition-field">
												<select class="form-control input-md" id="functionfieldcondition">
													<option value=""></option>
													<option value="NotNullCalculator">Not Null</option>
													<option value="EqualCalculator">Equals(=)</option>
													<option value="NotEqualCalculator">Not Equals(!=)</option>
													<option value="GreaterThanCalculator">Greater Than(&gt;)</option>
													<option value="LesserThanCalculator">Lesser Than(&lt;)</option>
													<option value="GreaterThanOrEqualCalculator">Greater Than Equals(&gt;=)</option>
													<option value="LesserThanOrEqualCalculator">Lesser Than Equals(&lt;=)</option>
													<option value="InsetCalculator">Inset</option>
												</select>										
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-8 col-height radio my-radio currenting-field">
													<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="functionfieldinteractive"/>
													</td><td>
														<fs:label tagclass="lclass control-label" tagid="functionfieldinteractive_label" for="functionfieldinteractive">Interactive</fs:label>
													</td></tr></table>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-8 col-height">
												<fs:label tagclass="lclass" tagid="functionfieldvalueoption_label" >Parameters</fs:label>
											</div>
										</div>
										<div class="row row-height" id="functionoptionvaluelayer">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-8 col-height">
													<table id="functionoptiontable" width="100%" border="1">
														<tbody id="functionoptiontablebody">
														</tbody>
													</table>
											</div>
										</div>
										<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="functionlabelpropertylegend_label" >Label Properties</fs:label></legend></fieldset>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-8 col-height radio my-radio currenting-field">
													<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="functionfieldheaderlabel"/>
													</td><td>
														<fs:label tagclass="lclass control-label" tagid="functionfieldheaderlabel_label" for="functionfieldheaderlabel">Header Label</fs:label>
													</td></tr></table>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right decimal-field">
													<fs:label tagclass="lclass" tagid="functionplacement_label" >Label Placement</fs:label>
											</div>
											<div class="col-md-3 col-height decimal-field">
													<select class="form-control input-md" id="functionfieldplacement">
														<option value=""></option>
														<option value="prefix">Prefix</option>
														<option value="suffix">Suffix</option>
													</select>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="column_layer_control" class="row portal-area sub-entry-layer control-property">
								<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="columnpropertylegend_label" >Column Properties</fs:label></legend></fieldset>
								<div id="column_layer" class="row portal-area sub-entry-layer">
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="columnalign_label" >Alignment</fs:label>
										</div>
										<div class="col-md-3 col-height">
											<select class="form-control input-md" id="columnalign">
												<option value=""></option>
												<option value="left">Left</option>
												<option value="center">Center</option>
												<option value="right">Right</option>
											</select>
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="fieldcolumnstyle_label" >Style</fs:label>
										</div>
										<div class="col-md-8 col-height">
											<input class="form-control input-md" id="fieldcolumnstyle" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="fieldcolspan_label" >Column Span</fs:label>
										</div>
										<div class="col-md-3 col-height">
											<input class="form-control input-md" id="fieldcolspan" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="fieldrowspan_label" >Row Span</fs:label>
										</div>
										<div class="col-md-3 col-height">
											<input class="form-control input-md" id="fieldrowspan" />
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="fieldcolumns_label" >Columns</fs:label>
										</div>
										<div class="col-md-3 col-height">
											<select class="form-control input-md" id="fieldcolumns">
													<option value=""></option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
											</select>
										</div>
										<div class="col-md-5 col-height text-right" style="margin-right: 5px;">
											<input type="button" id="updatefieldbutton2" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatefieldbutton2','Update')}"/>
										</div>
									</div>
								</div>
							</div>

							<div id="display_layer_control" class="row portal-area sub-entry-layer control-property">
								<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="displaypropertylegend_label" >Display Properties</fs:label></legend></fieldset>
								<div id="display_layer" class="row portal-area sub-entry-layer">
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
										</div>
										<div class="col-md-3 col-height radio my-radio">
											<table><tr><td>
													<input type="checkbox" class="form-control input-md" id="displayfield"/>
												</td><td>
													<fs:label tagclass="lclass" tagid="displayfield_label" for="displayfield">Displaying</fs:label>
											</td></tr></table>
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right decimal-field">
												<fs:label tagclass="lclass" tagid="displayplacement_label" >Placement</fs:label>
										</div>
										<div class="col-md-3 col-height decimal-field">
												<select class="form-control input-md" id="displayplacement">
													<option value=""></option>
													<option value="prefix">Prefix</option>
													<option value="suffix">Suffix</option>
												</select>
										</div>
									</div>
								</div>
							</div>

						</div>
						</td></tr></table>
					</td>
				</tr>
				<tr class="rclass" id="inputrow">
					<td style="vertical-align:top;">
						<table class="header-table-class header-left-table">
							<tr class="topic-row">
								<td class="topic-column" nowrap>
									<fs:label tagclass="lclass" tagid="programsectionproperties_label">Input Section</fs:label>
								</td>
								<td class="topic-control-column">
									<a style="float:right;" href="javascript:void(0)" onclick="deleteSelectedSection()" title="Delete Selected Section"><em class="fa fa-minus-circle fa-class" aria-hidden="true"></em></a>
									<ul id="sectionmenulistitem" class="navbar-nav navbar-user" style="float:right; padding-right: 20px;">	
										<li class="dropdown user-dropdown" id="sectionmenuitem">
											<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-plus-circle fa-class" aria-hidden="true" title="Add New"></em><strong class="caret"></strong></a>
											<ul class="dropdown-menu">
												<li><a href="javascript:void(0)" onclick="addNewSection()" title="Add New Section"><em class="fa fa-cube" aria-hidden="true"></em> New Section</a></li>
												<li><a href="javascript:void(0)" onclick="addNewPart()" title="Add New Part"><em class="fa fa-cubes" aria-hidden="true"></em> New Part</a></li>
												<li><a href="javascript:void(0)" onclick="addNewSubPart()" title="Add New Sub Part"><em class="fa fa-clone" aria-hidden="true"></em> New Sub Part</a></li>
												<li><a href="javascript:void(0)" onclick="addNewHorizon()" title="Add New Horizontal Line"><em class="fa fa-header" aria-hidden="true"></em> New Horizontal Line</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="rclass" id="inputfieldrow"> 
					<td class="inclass" style="vertical-align:top;">
						<table style="width:99.4%">
							<tr>
								<td style="vertical-align:top;" rowspan="3">
									<div id="sectionlayer" class="section-layer-class">
										<ul id="sectionlisting" style="margin-left:5px;">
										</ul>
									</div>
								</td>
							</tr>
							<tr>
								<td style="vertical-align:top;">
									<table id="fieldstable" class="header-table-class header-left-table sub-header-class" style="width:98%;">
										<tr class="topic-row">
											<td class="topic-column" nowrap>
												<fs:label tagclass="lclass" tagid="fieldsproperties_label">Fields</fs:label>
											</td>
											<td class="topic-control-column">
												<a style="float:right;" href="javascript:void(0)" onclick="deleteSelectedField()" title="Delete Selected Field"><em class="fa fa-minus-circle fa-class" aria-hidden="true"></em></a>
												<ul id="fieldmenulistitem" class="navbar-nav navbar-user" style="float:right; padding-right:20px;">	
													<li class="dropdown user-dropdown" id="fieldmenuitem">
														<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-plus-circle fa-class" aria-hidden="true" title="Add New"></em><strong class="caret"></strong></a>														
														<ul class="dropdown-menu">
															<li><a href="javascript:void(0)" onclick="addNewInput()" title="Add New Input"><em class="fa fa-pencil-square-o" aria-hidden="true"></em> New Input</a></li>
															<li><a href="javascript:void(0)" onclick="addNewLabel()" title="Add New Label"><em class="fa fa-square-o" aria-hidden="true"></em> New Label</a></li>
															<li><a href="javascript:void(0)" onclick="addNewImage()" title="Add New Image"><em class="fa fa-picture-o" aria-hidden="true"></em> New Image</a></li>
															<li><a href="javascript:void(0)" onclick="addNewRow()" title="Add New Row"><em class="fa fa-align-justify" aria-hidden="true"></em> New Row</a></li>
															<!--<li><a href="javascript:void(0)" onclick="addNewFunctionField()" title="Add New Function Field"><em class="fa fa-cogs" aria-hidden="true"></em> New Function Field</a></li>-->
														</ul>
													</li>
												</ul>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style="vertical-align:top; width: 40%;">
									<div id="sublistcolumn" class="sublist-layer-class">
										<div id="sublistinglayer" class="portal-area sublayer-class">
											<ul id="subitemlisting" class="sublisting-class">
											</ul>
										</div>		
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td style="min-height:1500px;"></td></tr>
			</table>
			<div id="fielddialoglayer">
				<div id="fielddialoglayercontrol">				
						<div id="new_field_layer_panel">
							<div id="new_field_layer" class="row portal-area sub-entry-layer">
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldid_label" >ID</fs:label>
									</div>
									<div class="col-md-6 col-height">
										<input class="form-control input-md" id="newfieldid" size="10" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldname_label" required="true">Name</fs:label>
									</div>
									<div class="col-md-6 col-height">
										<input class="form-control input-md" id="newfieldname" size="10" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldlabel_label" >Label</fs:label>
									</div>
									<div class="col-md-8 col-height">
										<input class="form-control input-md" id="newfieldlabel" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldtype_label" >Type</fs:label>
									</div>
									<div class="col-md-5 col-height">
										<select class="form-control input-md" id="newfieldtype">
											<option value="text">Text</option>
											<option value="radio">Radio Box</option>
											<option value="checkbox">Check Box</option>
											<option value="select">Select Box</option>
											<option value="list">List Box</option>
											<option value="image">Image File</option>
											<option value="hidden">Hidden</option>
										</select>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfielddatatype_label" >Data Type</fs:label>
									</div>
									<div class="col-md-5 col-height">
										<select class="form-control input-md" id="newfielddatatype">
											<option value="string">String</option>
											<option value="int">Integer</option>
											<option value="decimal">Decimal</option>
											<option value="date">Date</option>
											<option value="time">Time</option>
										</select>
									</div>
								</div>
								<div class="row row-height" id="newvaluelayer">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldvalue_label" >Value</fs:label>
									</div>
									<div class="col-md-5 col-height">
										<input class="form-control input-md" id="newfieldvalue"/>
									</div>
								</div>
								<div class="row row-height" id="newoptionvaluelayer">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldvalueoption_label" >Option Value</fs:label>
									</div>
									<div class="col-md-9 col-height">
											<table id="newoptiontable" width="100%" border="1">
												<thead>
													<tr>
														<th style="text-align:center;"><a href="javascript:void(0)" onclick="copyOption('newoptiontablebody')" style="float:left; padding-right:2px;"class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-files-o fa-class" aria-hidden="true" title="Copy Option"></em></a><fs:label tagclass="lclass" tagid="newfieldvalueoption_headerlabel" >Value</fs:label></th>
														<th style="text-align:center;"><fs:label tagclass="lclass" tagid="newfieldtextoption_headerlabel" >Text</fs:label></th>
														<th style="text-align:center;"><fs:label tagclass="lclass" tagid="newfieldcontroloption_headerlabel" >Control</fs:label><a href="javascript:void(0)" onclick="pasteOption('newoptiontablebody')" style="float:right; padding-right:2px;"class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-clipboard fa-class" aria-hidden="true" title="Paste Option"></em></a></th>
														<th style="text-align:center; width:80px;"><a href="javascript:void(0)" onclick="insertNewOption('newoptiontablebody')" style="padding-right:2px;"class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-plus-circle fa-class" aria-hidden="true" title="Add New Option"></em></a></th>
													</tr>
												</thead>
												<tbody id="newoptiontablebody">
												</tbody>
											</table>
									</div>
								</div>
								<div class="row row-height" id="newfieldtaglayer">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldtag_label" >Tag</fs:label>
									</div>
									<div class="col-md-5 col-height">
										<select class="form-control input-md" id="newfieldtag">
											<option value=""></option>
											<option value="int">Integer</option>
											<option value="money">Decimal</option>
											<option value="date">Date</option>
											<option value="time">Time</option>
											<option value="year">Year</option>
											<option value="month">Month</option>
											<option value="hour">Hour</option>
											<option value="minute">Minute</option>
										</select>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldpicture_label" >Picture</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<input class="form-control input-md" id="newfieldpicture" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-4 col-height radio my-radio currenting-field">
											<table><tr><td>
												<input type="checkbox" class="form-control input-md" id="newfieldcurrenting"/>
											</td><td>
												<fs:label tagclass="lclass control-label" tagid="newfieldcurrenting_label" for="newfieldcurrenting">Currenting</fs:label>
											</td></tr></table>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right decimal-field">
											<fs:label tagclass="lclass" tagid="newfielddecimal_label" >Decimal</fs:label>
									</div>
									<div class="col-md-3 col-height decimal-field">
											<fs:int tagclass="form-control input-md" name="newfielddecimal" tagid="newfielddecimal"></fs:int>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right decimal-field">
											<fs:label tagclass="lclass" tagid="newfielddecimalformat_label" >Decimal Format</fs:label>
									</div>
									<div class="col-md-5 col-height decimal-field">
											<input type="text" class="form-control input-md" id="newfielddecimalformat"/>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right decimal-field">
											<fs:label tagclass="lclass" tagid="newfieldprecision_label" >Precision</fs:label>
									</div>
									<div class="col-md-3 col-height decimal-field">
											<fs:int tagclass="form-control input-md" name="newfieldprecision" tagid="newfieldprecision"></fs:int>
									</div>
								</div>								
								<div id="newkeyablecontrollayer" class="row row-height" style="display:none;">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-5 col-height radio my-radio currenting-field">
											<table><tr><td>
												<input type="checkbox" class="form-control input-md" id="newfieldkeyable"/>
											</td><td>
												<fs:label tagclass="lclass control-label" tagid="newfieldkeyable_label" for="newfieldkeyable">Keyable</fs:label>
											</td></tr></table>
									</div>
								</div>
								<div id="newcolumnscontrollayer" class="row row-height control-property">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldcols_label" >Columns</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<input class="form-control input-md" id="newfieldcols" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldwidth_label" >Width</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<fs:int tagclass="form-control input-md" name="newfieldwidth" tagid="newfieldwidth"></fs:int>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldheight_label" >Height</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<fs:int tagclass="form-control input-md" name="newfieldheight" tagid="newfieldheight"></fs:int>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldstyle_label" >Style</fs:label>
									</div>
									<div class="col-md-8 col-height">
										<input class="form-control input-md" id="newfieldstyle" />
									</div>
								</div>
								<div id="newfield_layer_control" class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfieldcontrol_label" >Control</fs:label>
									</div>
									<div class="col-md-8 col-height">
										<input class="form-control input-md" id="newfieldcontrol" />
									</div>
								</div>
								<div class="row row-height" id="newreadonly_layer">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-3 col-height radio my-radio">
										<table><tr><td>
											<input type="checkbox" class="form-control input-md" id="newfieldshowing"/>
										</td><td>
											<fs:label tagclass="lclass" tagid="newfieldshowing_label" for="newfieldshowing">Showing</fs:label>
										</td></tr></table>
									</div>
									<div class="col-md-3 col-height radio my-radio currenting-field">
											<table><tr><td>
												<input type="checkbox" class="form-control input-md" id="newfieldreadonly"/>
											</td><td>
												<fs:label tagclass="lclass control-label" tagid="newfieldreadonly_label" for="newfieldreadonly">Read Only</fs:label>
											</td></tr></table>
									</div>
								</div>
								<div id="newvalidate_layer_control" class="row portal-area sub-entry-layer control-property">
									<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newvalidatepropertylegend_label" >Validate Properties</fs:label></legend></fieldset>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right">
										</div>
										<div class="col-md-3 col-height radio my-radio">
											<table><tr><td>
													<input type="checkbox" class="form-control input-md" id="newfieldrequired"/>
												</td><td>
													<fs:label tagclass="lclass" tagid="newfieldrequired_label" for="newfieldrequired">Required</fs:label>
											</td></tr></table>
										</div>
										<div class="col-md-3 col-height radio my-radio">
											<table><tr><td>
													<input type="checkbox" class="form-control input-sd" id="newfieldcompleted"/>
												</td><td>
													<fs:label tagclass="lclass" tagid="newfieldcompleted_label" for="newfieldcompleted">Completed</fs:label>
											</td></tr></table>
										</div>
									</div>
									<div class="row row-height">
										<div class="col-md-3 col-height col-label text-right decimal-field">
												<fs:label tagclass="lclass" tagid="newfieldmsg_label" >Message</fs:label>
										</div>
										<div class="col-md-8 col-height decimal-field">
												<input type="text" class="form-control input-md" id="newfieldmsg"/>
										</div>
									</div>
								</div>
								<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newfilterpropertylegend_label" >Filter Properties</fs:label></legend></fieldset>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-3 col-height radio my-radio">
										<table><tr><td>
											<input type="checkbox" class="form-control input-md" id="newfieldfiltering"/>
										</td><td>
											<fs:label tagclass="lclass" tagid="newfieldfiltering_label" for="newfieldfiltering">Filtering</fs:label>
										</td></tr></table>
									</div>
									<div class="col-md-3 col-height radio my-radio">
										<table><tr><td>
											<input type="checkbox" class="form-control input-sd" id="newfieldlisting"/>
										</td><td>
											<fs:label tagclass="lclass" tagid="newfieldlisting_label" for="newfieldlisting">Listing</fs:label>
										</td></tr></table>
									</div>
								</div>
								<div id="newcondition_row" class="row row-height">
									<div class="col-md-3 col-height col-label text-right condition-field">
										<fs:label tagclass="lclass" tagid="newfieldcondition_label" >Condition</fs:label>
									</div>
									<div class="col-md-5 col-height condition-field">
										<select class="form-control input-md" id="newfieldcondition">
											<option value=""></option>
											<option value="=">Equals(=)</option>
											<option value="!=">Not Equals(!=)</option>
											<option value=">">Greater Than</option>
											<option value="<">Lesser Than</option>
											<option value=">=">Greater Than Equals(&gt;=)</option>
											<option value="<=">Lesser Than Equals(&lt;=)</option>
											<option value="LIKE">Like</option>
											<option value="between">Between</option>
										</select>										
									</div>
								</div>
								<div id="newranged_row" class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newrangelabel_label" >Range Label</fs:label>
									</div>
									<div class="col-md-8 col-height">
										<input class="form-control input-md" id="newrangelabel" />									
									</div>
								</div>

								<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newlabelpropertylegend_label" >Label Properties</fs:label></legend></fieldset>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-8 col-height radio my-radio">
											<table><tr><td>
												<input type="checkbox" class="form-control input-md" id="newfieldheaderlabel"/>
											</td><td>
												<fs:label tagclass="lclass control-label" tagid="newfieldheaderlabel_label" for="newfieldheaderlabel">Header Label</fs:label>
											</td></tr></table>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
											<fs:label tagclass="lclass" tagid="newplacement_label" >Label Placement</fs:label>
									</div>
									<div class="col-md-3 col-height decimal-field">
											<select class="form-control input-md" id="newfieldplacement">
												<option value=""></option>
												<option value="prefix">Prefix</option>
												<option value="suffix">Suffix</option>
											</select>
									</div>
								</div>

								<div id="newlookup_layer_control" class="row portal-area sub-entry-layer control-property">
									<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newlookuppropertylegend_label" >Lookup Properties</fs:label></legend></fieldset>
									<div id="newlookuptabs">
										<input type="hidden" id="newlookuptype" value="service"/>
										<ul class="nav nav-tabs">
											<li id="newlookupserviceitem" class="active">
												<a id="newlookupservicelinker" href="#newlookupservicelayer" data-toggle="tab" onclick="newLookupServiceClick()"><fs:label tagclass="lclass" tagid="newlookupservicelinker_label" >Lookup Service</fs:label></a>
											</li>
											<li id="newlookuptableitem">
												<a id="newlookuptablelinker" href="#newlookuptablelayer" data-toggle="tab" onclick="newLookupTableClick()"><fs:label tagclass="lclass" tagid="newlookuptablelinker_label" >Lookup Table</fs:label></a>
											</li>
											<li id="newlookupiniitem">
												<a id="newlookupinilinker" href="#newlookupinilayer" data-toggle="tab" onclick="newLookupIniClick()"><fs:label tagclass="lclass" tagid="newlookupinilinker_label" >Lookup INI</fs:label></a>
											</li>
											<li id="newlookupcustomitem">
												<a id="newlookupcustomlinker" href="#newlookupcustomlayer" data-toggle="tab" onclick="newLookupCustomClick()"><fs:label tagclass="lclass" tagid="newlookupcustomlinker_label" >Lookup Custom</fs:label></a>
											</li>
										</ul>
									</div>
									<div id="newlookup_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="newlookupfield"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="newlookupfield_label" for="newlookupfield">Lookup</fs:label>
												</td></tr></table>
											</div>
											<div class="col-md-5 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-sd" id="newlookupfieldmultiple"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="newlookupfieldmultiple_label" for="newlookupfieldmultiple">Multiple Selection</fs:label>
												</td></tr></table>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-8 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="newlookupmapping"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="newlookupmapping_label" for="newlookupmapping">Mapping Lookup</fs:label>
												</td></tr></table>
											</div>
										</div>
										<div id="newlookupservicelayer">
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="newlookupurl_label" >Service URL</fs:label>
												</div>
												<div class="col-md-8 col-height">
													<input class="form-control input-md" id="newlookupurl" />
												</div>
											</div>
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="newlookupcategory_label" >Category</fs:label>
												</div>
												<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookupcategory" />
												</div>
											</div>
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
												</div>
												<div class="col-md-8 col-height radio my-radio">
													<table><tr><td>
															<input type="checkbox" class="form-control input-md" id="newlookupgetting"/>
														</td><td>
															<fs:label tagclass="lclass" tagid="newlookupgetting_label" for="newlookupgetting">Getting Lookup</fs:label>
													</td></tr></table>
												</div>
											</div>

											<div id="newlookupservicekeyfieldlayer">
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="newlookupkeyfield_label" >Field Key</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookupkeyfield" />
													</div>
												</div>
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="newlookupvaluefield_label" >Description</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookupvaluefield" />
													</div>
												</div>
											</div>
										</div>

										<div id="newlookuptablelayer">
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="newlookuptable_label" >Table Name</fs:label>
												</div>
												<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookuptablename" />
												</div>
											</div>
											<div id="newlookuptablekeyfieldlayer">
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="newlookuptablekeyfield_label" >Field Key</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookuptablekeyfield" />
													</div>
												</div>
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="newlookuptablevaluefield_label" >Description</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookuptablevaluefield" />
													</div>
												</div>
											</div>
										</div>

										<div id="newlookupinilayer" class="tab-pane">
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="newlookupinifile_label" >File Name</fs:label>
												</div>
												<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookupinifile" />
												</div>
											</div>
											<div id="newlookupinisectionlayer">
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="newlookupinisection_label" >Section Name</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookupinisection" />
													</div>
												</div>
											</div>
										</div>

										<div id="newlookupcustomlayer" class="tab-pane">
											<div class="row row-height">
												<div class="col-md-3 col-height col-label text-right">
													<fs:label tagclass="lclass" tagid="newlookupcustomfile_label" >File Name</fs:label>
												</div>
												<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookupcustomfile" />
												</div>
											</div>
											<div id="newlookupinisectionlayer">
												<div class="row row-height">
													<div class="col-md-3 col-height col-label text-right">
														<fs:label tagclass="lclass" tagid="newlookupcustomsection_label" >Section Name</fs:label>
													</div>
													<div class="col-md-5 col-height">
														<input class="form-control input-md" id="newlookupcustomsection" />
													</div>
												</div>
											</div>
										</div>

										<div class="row row-height" id="newlookupmappingrow">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-9 col-height">
													<table id="newlookupoptiontable" width="100%" border="1">
														<thead>
															<tr>
																<th style="text-align:center;"><fs:label tagclass="lclass" tagid="newlookupfieldoption_headerlabel" >Name</fs:label></th>
																<th style="text-align:center;"><fs:label tagclass="lclass" tagid="newlookupfieldtextoption_headerlabel" >Text</fs:label></th>
																<th style="text-align:center;"><fs:label tagclass="lclass" tagid="newlookupfieldcontroloption_headerlabel" >Control</fs:label></th>
																<th style="text-align:center; width:80px;">
																	<a href="javascript:void(0)" onclick="insertNewOption('newlookupoptiontablebody')" style="padding-right:2px;"class="dropdown-toggle" data-toggle="dropdown"><em class="fa fa-plus-circle fa-class" aria-hidden="true" title="Add New Option"></em></a>
																</th>
															</tr>
														</thead>
														<tbody id="newlookupoptiontablebody">
														</tbody>
													</table>
											</div>
										</div>

									</div>
								</div>

								<div id="newcolumn_layer_control" class="row portal-area sub-entry-layer control-property">
									<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newcolumnpropertylegend_label" >Column Properties</fs:label></legend></fieldset>
									<div id="newcolumn_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newcolumnalign_label" >Alignment</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<select class="form-control input-md" id="newcolumnalign">
													<option value=""></option>
													<option value="left">Left</option>
													<option value="center">Center</option>
													<option value="right">Right</option>
												</select>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfieldcolumnstyle_label" >Style</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="newfieldcolumnstyle" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfieldcolspan_label" >Column Span</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<input class="form-control input-md" id="newfieldcolspan" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfieldrowspan_label" >Row Span</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<input class="form-control input-md" id="newfieldrowspan" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfieldcolumns_label" >Columns</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<select class="form-control input-md" id="newfieldcolumns">
													<option value=""></option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div id="newdisplay_layer_control" class="row portal-area sub-entry-layer control-property">
									<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newdisplaypropertylegend_label" >Display Properties</fs:label></legend></fieldset>
									<div id="newdisplay_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="newdisplayfield"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="newdisplayfield_label" for="newdisplayfield">Displaying</fs:label>
												</td></tr></table>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right decimal-field">
													<fs:label tagclass="lclass" tagid="newdisplayplacement_label" >Placement</fs:label>
											</div>
											<div class="col-md-3 col-height decimal-field">
													<select class="form-control input-md" id="newdisplayplacement">
														<option value=""></option>
														<option value="prefix">Prefix</option>
														<option value="suffix">Suffix</option>
													</select>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>						
				</div>
			</div>
			<div id="sectiondialoglayer">
				<div id="sectiondialoglayercontrol">
						<div id="new_section_layer" class="row portal-area sub-entry-layer">
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newsectionlabel_label" >Label</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newsectionlabel" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newsectionid_label" >ID</fs:label>
								</div>
								<div class="col-md-6 col-height">
									<input class="form-control input-md" id="newsectionid" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newsectionstyle_label" >Style</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newsectionstyle" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newsectionincreaseindent_label" >Indent</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<select id="newsectionincreaseindent">
										<option value=""></option>
										<option value="margin-left: 25px;">Margin Left 25</option>
										<option value="margin-left: 50px;">Margin Left 50</option>
										<option value="margin-left: 75px;">Margin Left 75</option>
										<option value="margin-left: 100px;">Margin Left 100</option>
										<option value="margin-left: 125px;">Margin Left 125</option>
										<option value="margin-left: 150px;">Margin Left 150</option>
										<option value="margin-left: 175px;">Margin Left 175</option>
										<option value="margin-left: 200px;">Margin Left 200</option>
									</select>
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
								</div>
								<div class="col-md-8 col-height">
										<table><tr><td>
											<input type="checkbox" class="form-control input-md" id="newsectiondesign" />
										</td><td style="padding-top:5px; padding-left: 2px;">
											<fs:label tagclass="lclass" tagid="newsectiondesign_label" for="newsectiondesign">Display Label Only On Design Time</fs:label>
										</td></tr></table>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div id="partdialoglayer">
				<div id="partdialoglayercontrol">
						<div id="new_part_layer" class="row portal-area sub-entry-layer">
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newpartlabel_label" >Label</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newpartlabel" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newpartid_label" >ID</fs:label>
								</div>
								<div class="col-md-6 col-height">
									<input class="form-control input-md" id="newpartid" size="10" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newparttitle_label">Title</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newparttitle" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newpartstyle_label" >Style</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newpartstyle" />
								</div>
							</div>
						</div>
				</div>
			</div>
			<div id="subpartdialoglayer">
				<div id="subpartdialoglayercontrol">
						<div id="newsubpart_layer" class="row portal-area sub-entry-layer">
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newsubpartlabel_label" >Label</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newsubpartlabel" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newsubpartid_label" >ID</fs:label>
								</div>
								<div class="col-md-6 col-height">
									<input class="form-control input-md" id="newsubpartid" size="10" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newsubpartstyle_label" >Style</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newsubpartstyle" />
								</div>
							</div>
						</div>
				</div>
			</div>
			<div id="labeldialoglayer">
				<div id="labeldialoglayercontrol">
						<div id="new_label_layer" class="row portal-area sub-entry-layer">
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newlabellabel_label" >Label</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newlabellabel" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newlabelid_label" >ID</fs:label>
								</div>
								<div class="col-md-6 col-height">
									<input class="form-control input-md" id="newlabelid" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newlabelstyle_label" >Style</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newlabelstyle" />
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right decimal-field">
										<fs:label tagclass="lclass" tagid="newlabelplacement_label" >Placement</fs:label>
								</div>
								<div class="col-md-3 col-height decimal-field">
										<select class="form-control input-md" id="newlabelplacement">
											<option value=""></option>
											<option value="prefix">Prefix</option>
											<option value="suffix">Suffix</option>
										</select>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div id="imagedialoglayer">
				<div id="imagedialoglayercontrol">
						<div id="newimage_layer" class="row portal-area sub-entry-layer">
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newimage_label" >Image</fs:label>
								</div>
								<form id="newimageform" name="newimageform">
									<div class="col-md-8 col-height" id="newimageinputlayer">
										<input type="file" class="form-control input-md" id="newfieldimage" name="fieldimage"/>
									</div>
								</form>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newimagewidth_label" >Width</fs:label>
								</div>
								<div class="col-md-3 col-height">
									<fs:int tagclass="form-control input-md" name="imagewidth" tagid="newimagewidth"></fs:int>
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newimageheight_label" >Height</fs:label>
								</div>
								<div class="col-md-3 col-height">
									<fs:int tagclass="form-control input-md" name="imageheight" tagid="newimageheight"></fs:int>
								</div>
							</div>
							<div class="row row-height">
								<div class="col-md-3 col-height col-label text-right">
									<fs:label tagclass="lclass" tagid="newimagestyle_label" >Style</fs:label>
								</div>
								<div class="col-md-8 col-height">
									<input class="form-control input-md" id="newimagestyle" />
								</div>
							</div>
						</div>
						<div id="newimgcolumn_layer_control" class="row portal-area sub-entry-layer">
							<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newimgcolumnpropertylegend_label" >Column Properties</fs:label></legend></fieldset>
							<div id="newimgcolumn_layer" class="row portal-area sub-entry-layer">
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newimgcolumnalign_label" >Alignment</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<select class="form-control input-md" id="newimgcolumnalign">
											<option value=""></option>
											<option value="left">Left</option>
											<option value="center">Center</option>
											<option value="right">Right</option>
										</select>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newimgcolumnstyle_label" >Style</fs:label>
									</div>
									<div class="col-md-8 col-height">
										<input class="form-control input-md" id="newimgcolumnstyle" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newimgcolspan_label" >Column Span</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<input class="form-control input-md" id="newimgcolspan" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newimgrowspan_label" >Row Span</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<input class="form-control input-md" id="newimgrowspan" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newimgcolumns_label" >Columns</fs:label>
									</div>
									<div class="col-md-8 col-height">
										<input class="form-control input-md" id="newimgcolumns" />
									</div>
								</div>
							</div>
						</div>

				</div>
			</div>
			<div id="functionfielddialoglayer">
				<div id="functionfielddialoglayercontrol">				
						<div id="new_function_field_layer_panel">
							<div id="new_function_field_layer" class="row portal-area sub-entry-layer">
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfunctionfieldid_label" >ID</fs:label>
									</div>
									<div class="col-md-6 col-height">
										<input class="form-control input-md" id="newfunctionfieldid" size="10" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfunctionfieldname_label" required="true">Name</fs:label>
									</div>
									<div class="col-md-6 col-height">
										<input class="form-control input-md" id="newfunctionfieldname" size="10" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfunctionfieldlabel_label" >Label</fs:label>
									</div>
									<div class="col-md-8 col-height">
										<input class="form-control input-md" id="newfunctionfieldlabel" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfunctionfieldstyle_label" >Style</fs:label>
									</div>
									<div class="col-md-8 col-height">
										<input class="form-control input-md" id="newfunctionfieldstyle" />
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfunctionfieldwidth_label" >Width</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<fs:int tagclass="form-control input-md" name="newfunctionfieldwidth" tagid="newfunctionfieldwidth"></fs:int>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
										<fs:label tagclass="lclass" tagid="newfunctionfieldheight_label" >Height</fs:label>
									</div>
									<div class="col-md-3 col-height">
										<fs:int tagclass="form-control input-md" name="newfunctionfieldheight" tagid="newfunctionfieldheight"></fs:int>
									</div>
								</div>
								<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newfunctionfilterpropertylegend_label" >Filter Properties</fs:label></legend></fieldset>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-3 col-height radio my-radio">
										<table><tr><td>
											<input type="checkbox" class="form-control input-md" id="newfunctionfieldshowing"/>
										</td><td>
											<fs:label tagclass="lclass" tagid="newfunctionfieldshowing_label" for="newfunctionfieldshowing">Showing</fs:label>
										</td></tr></table>
									</div>
									<div class="col-md-3 col-height radio my-radio">
										<table><tr><td>
											<input type="checkbox" class="form-control input-sd" id="newfunctionfieldlisting"/>
										</td><td>
											<fs:label tagclass="lclass" tagid="newfunctionfieldlisting_label" for="newfunctionfieldlisting">Listing</fs:label>
										</td></tr></table>
									</div>
								</div>
								<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newfunctionpropertylegend_label" >Function Properties</fs:label></legend></fieldset>
								<div id="newfunctioncondition_row" class="row row-height">
									<div class="col-md-3 col-height col-label text-right condition-field">
										<fs:label tagclass="lclass" tagid="newfunctionfieldcondition_label" >Function</fs:label>
									</div>
									<div class="col-md-5 col-height condition-field">
										<select class="form-control input-md" id="newfunctionfieldcondition">
											<option value=""></option>
											<option value="NotNullCalculator">Not Null</option>
											<option value="EqualCalculator">Equals(=)</option>
											<option value="NotEqualCalculator">Not Equals(!=)</option>
											<option value="GreaterThanCalculator">Greater Than(&gt;)</option>
											<option value="LesserThanCalculator">Lesser Than(&lt;)</option>
											<option value="GreaterThanOrEqualCalculator">Greater Than Equals(&gt;=)</option>
											<option value="LesserThanOrEqualCalculator">Lesser Than Equals(&lt;=)</option>
											<option value="InsetCalculator">Inset</option>
										</select>										
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-8 col-height radio my-radio currenting-field">
											<table><tr><td>
												<input type="checkbox" class="form-control input-md" id="newfunctionfieldinteractive"/>
											</td><td>
												<fs:label tagclass="lclass control-label" tagid="newfunctionfieldinteractive_label" for="newfunctionfieldinteractive">Interactive</fs:label>
											</td></tr></table>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-8 col-height">
										<fs:label tagclass="lclass" tagid="newfunctionfieldvalueoption_label" >Parameters</fs:label>
									</div>
								</div>
								<div class="row row-height" id="newfunctionoptionvaluelayer">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-8 col-height">
											<table id="newfunctionoptiontable" width="100%" border="1">
												<tbody id="newfunctionoptiontablebody">
												</tbody>
											</table>
									</div>
								</div>
								<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newfunctionlabelpropertylegend_label" >Label Properties</fs:label></legend></fieldset>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right">
									</div>
									<div class="col-md-8 col-height radio my-radio currenting-field">
											<table><tr><td>
												<input type="checkbox" class="form-control input-md" id="newfunctionfieldheaderlabel"/>
											</td><td>
												<fs:label tagclass="lclass control-label" tagid="newfunctionfieldheaderlabel_label" for="newfunctionfieldheaderlabel">Header Label</fs:label>
											</td></tr></table>
									</div>
								</div>
								<div class="row row-height">
									<div class="col-md-3 col-height col-label text-right decimal-field">
											<fs:label tagclass="lclass" tagid="newfunctionplacement_label" >Label Placement</fs:label>
									</div>
									<div class="col-md-3 col-height decimal-field">
											<select class="form-control input-md" id="newfunctionfieldplacement">
												<option value=""></option>
												<option value="prefix">Prefix</option>
												<option value="suffix">Suffix</option>
											</select>
									</div>
								</div>

								<div id="newfunctioncolumn_layer_control" class="row portal-area sub-entry-layer control-property">
									<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newfunctioncolumnpropertylegend_label" >Column Properties</fs:label></legend></fieldset>
									<div id="newfunctioncolumn_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfunctioncolumnalign_label" >Alignment</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<select class="form-control input-md" id="newfunctioncolumnalign">
													<option value=""></option>
													<option value="left">Left</option>
													<option value="center">Center</option>
													<option value="right">Right</option>
												</select>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfunctionfieldcolumnstyle_label" >Style</fs:label>
											</div>
											<div class="col-md-8 col-height">
												<input class="form-control input-md" id="newfunctionfieldcolumnstyle" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfunctionfieldcolspan_label" >Column Span</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<input class="form-control input-md" id="newfunctionfieldcolspan" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfunctionfieldrowspan_label" >Row Span</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<input class="form-control input-md" id="newfunctionfieldrowspan" />
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
												<fs:label tagclass="lclass" tagid="newfunctionfieldcolumns_label" >Columns</fs:label>
											</div>
											<div class="col-md-3 col-height">
												<select class="form-control input-md" id="newfunctionfieldcolumns">
													<option value=""></option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div id="newfunctiondisplay_layer_control" class="row portal-area sub-entry-layer control-property">
									<fieldset><legend style="margin-bottom:5px;padding-left:10px;"><fs:label tagclass="lclass" tagid="newfunctiondisplaypropertylegend_label" >Display Properties</fs:label></legend></fieldset>
									<div id="newfunctiondisplay_layer" class="row portal-area sub-entry-layer">
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right">
											</div>
											<div class="col-md-3 col-height radio my-radio">
												<table><tr><td>
														<input type="checkbox" class="form-control input-md" id="newfunctiondisplayfield"/>
													</td><td>
														<fs:label tagclass="lclass" tagid="newfunctiondisplayfield_label" for="newfunctiondisplayfield">Displaying</fs:label>
												</td></tr></table>
											</div>
										</div>
										<div class="row row-height">
											<div class="col-md-3 col-height col-label text-right decimal-field">
													<fs:label tagclass="lclass" tagid="newfunctiondisplayplacement_label" >Placement</fs:label>
											</div>
											<div class="col-md-3 col-height decimal-field">
													<select class="form-control input-md" id="newfunctiondisplayplacement">
														<option value=""></option>
														<option value="prefix">Prefix</option>
														<option value="suffix">Suffix</option>
													</select>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>						
				</div>
			</div>
			<div id="previewdialoglayer">
			</div>
	</div>