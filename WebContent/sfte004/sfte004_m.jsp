<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<c:if test="${fsScreen.init('sfte004',pageContext.request, pageContext.response,true)}"></c:if>
<!DOCTYPE html>
<html>
	<head>
		<title>Program Management</title>		
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createImportScripts('sfte004',pageContext.request, pageContext.response)}" escapeXml="false"></c:out>
		<script type="text/javascript" src="../jquery/ui/jquery.ui.sortable.js"></script>
		<link rel="stylesheet" type="text/css" href="sfte004.css?${fsScreen.currentTime()}" />
		<script type="text/javascript" src="sfte004_function.js?${fsScreen.currentTime()}"></script>
		<script type="text/javascript" src="sfte004.js?${fsScreen.currentTime()}"></script>
	</head>
	<body class="portalbody portalbody-off">
		<div id="fsdialoglayer" style="display:none;"><span id="fsmsgbox"></span></div>
		<div id="fsacceptlayer" style="display:none;"><span id="fsacceptbox"></span></div>
		<div id="fswaitlayer" style="display:none; position:absolute; left:1px; top:1px; z-Index:9999;"><img id="waitImage" class="waitimgclass" src="../images/waiting.gif" width="50px" height="50px" alt=""></img></div>	
		<jsp:include page="sfte004_md.jsp"/>
		<div id="sfte002" class="pt-page pt-page-current pt-page-controller">
			<h1 class="page-header-title" title="sfte004">Program Management</h1>
			<div id="searchpanel" class="panel-body">
				<form id="fssearchform" name="fssearchform" method="post">	
					<div class="row filter-layer">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
							<div class="col-md-3 col-height search-group">
								<fs:label tagid="filename_label" tagclass="control-label">Program Configuration</fs:label>
								<fs:select tagclass="form-control input-md" tagid="filename" name="filename" section="PROGFILE_CATEGORY" showing="both"> </fs:select>
							</div>
							<div class="col-md-2 search-group">
								<button type="button" id="explorebutton" class="btn btn-dark btn-sm" style="margin-top: 28px;" title="Explore Program Configuration">${fsLabel.getText('explorebutton','Explore')}</button>
							</div>
							<div class="pull-right">
								<button type="button" id="newbutton" class="btn btn-dark btn-sm" style="margin-top: 28px; margin-right: 2px;" title="Add New Program">${fsLabel.getText('newbutton','New')}</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<hr class="program-horizontal"/>
			<div id="entrypanel" style="display:none;">
					<jsp:include page="sfte004_de.jsp"/>
			</div>
		</div>

		<div id="saveas_dialog" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="saveas_modalheader">${fsLabel.getText('saveas_modalheader','Save As')}</h4>
					</div>
					<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagclass="lclass" tagid="saveasprogid_label" >ID</fs:label>
						</div>
						<div class="col-md-4 col-height">
							<input class="form-control input-md" id="saveasprogid" placeholder="" autocomplete="off" size="10" />
						</div>
					</div>
					<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagclass="lclass" tagid="saveasprogname_label" >Title</fs:label>
						</div>
						<div class="col-md-7 col-height">
							<input class="form-control input-md" id="saveasprogname" placeholder="" autocomplete="off" size="25"/>
						</div>
					</div>
					<div class="row row-heighter center-block" style="display:none;">
						<div class="col-md-3 col-height"></div>
						<div class="col-md-7 col-height">
							<div class="checkbox">
								<input type="checkbox" class="form-control input-md" id="alwaysbox" />
								<fs:label tagclass="lclass" tagid="alwaysbox_label" for="alwaysbox">Save As New Table Name</fs:label>
							</div>								
						</div>
					</div>					
					<div class="row row-heighter modal-footer" style="margin-left:0px; margin-right:0px;">
						<div class="col-md-9 col-height pull-right">
							<input type="button" id="saveas_okbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('saveas_okbutton','OK')}"/>
							<input type="button" id="saveas_cancelbutton" class="btn btn-dark btn-sm" data-dismiss="modal" value="${fsLabel.getText('saveas_cancelbutton','Cancel')}"/>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="preview_dialog" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
			<div id="preview_modal_dialog" class="modal-dialog modal-xl">
				<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="preview_modalheader">${fsLabel.getText('preview_modalheader','Preview')}</h4>
					</div>
					<div class="row row-heighter center-block" style="margin-left:0px; margin-right:0px;">
						<div class="col-md-12 col-height">							
							<iframe id="previewprogramframe" name="previewprogramframe" width="500" height="500" src="preview.html" title=""></iframe>							
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>
