var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
var prgxmldoc;
var currentRowLayer;
var filterCounter = 0;
var sectionCounter = 0;
var rowCounter = 500;
var columnCounter = 500;
var fielddialog;
var sectiondialog;
var partdialog;
var subpartdialog;
var labeldialog;
var imagedialog;
var functionfielddialog;
var preview_window;
var startIndex = 0;
var alreadyGenerated = false;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("sfte004"); }catch(ex) { }
	try { $(window).bind("unload",function() { if(preview_window) preview_window.close(); }); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
	setupComponents();
	fielddialog = $("#fielddialoglayer").dialog({
        autoOpen: false, modal: true, title: "New Field",
        width: 900, height: 700, resizable: true, 
        buttons: [{ text: "OK", click: function() { $(this).dialog("close"); } }]
    });
	sectiondialog = $("#sectiondialoglayer").dialog({
        autoOpen: false, modal: true, title: "New Section",
        width: 700, height: 380, resizable: true, 
        buttons: [{ text: "OK", click: function() { $(this).dialog("close"); } }]
    });
	partdialog = $("#partdialoglayer").dialog({
        autoOpen: false, modal: true, title: "New Part",
        width: 700, height: 380, resizable: true, 
        buttons: [{ text: "OK", click: function() { $(this).dialog("close"); } }]
    });
	subpartdialog = $("#subpartdialoglayer").dialog({
        autoOpen: false, modal: true, title: "New Sub Part",
        width: 700, height: 300, resizable: true, 
        buttons: [{ text: "OK", click: function() { $(this).dialog("close"); } }]
    });
	labeldialog = $("#labeldialoglayer").dialog({
        autoOpen: false, modal: true, title: "New Label",
        width: 700, height: 350, resizable: true, 
        buttons: [{ text: "OK", click: function() { $(this).dialog("close"); } }]
    });
	imagedialog = $("#imagedialoglayer").dialog({
        autoOpen: false, modal: true, title: "New Image",
        width: 800, height: 580, resizable: true, 
        buttons: [{ text: "OK", click: function() { $(this).dialog("close"); } }]
    });
	functionfielddialog = $("#functionfielddialoglayer").dialog({
        autoOpen: false, modal: true, title: "New Function Field",
        width: 900, height: 700, resizable: true, 
        buttons: [{ text: "OK", click: function() { $(this).dialog("close"); } }]
    });
	$("#preview_dialog").on("hidden.bs.modal", function (e) {
		$("#previewprogramframe").attr("src","preview.html");
	});
}
function setupComponents() {
		$("#explorebutton").click(function(evt) { 
			search(); 
			return false;
		});
		$("#newbutton").click(function(evt) { 
			insert(); 
			return false;
		});
		$("#savebutton").click(function() { 
			save();
			return false;
		});
		$("#saveasbutton").click(function() { 
			$("#saveasprogid").val("");
			$("#saveasprogname").val($("#progname").val());
			$("#alwaysbox").attr("checked",true);
			$("#saveas_dialog").modal("show");
			return false;
		});
		$("#saveas_okbutton").click(function() { 
			$("#saveas_dialog").modal("hide");
			saveAs();
			return false;		
		});
		$("#generatebutton").click(function() { 
			generate();
			return false;
		});
		$("#cancelbutton").click(function() { 
			cancel();
			return false;
		});
		$("#previewbutton").click(function() { 
			preview();
			return false;
		});		
		$("#sectionlisting").sortable({
			placeholder: "ui-state-highlight",
			start : function(evt,ui) {
				var $it = $(ui.item);
				startIndex = $it.index();
			},
			stop : function(evt,ui) {
				var $it = $(ui.item);
				var index = $it.index();
				var idx = $it.attr("idx");
				var difIdx = Math.abs(eval(startIndex) - eval(index));
				//console.log("start Idx = "+startIndex+", index="+index+", idx="+idx+", dif="+difIdx);
				if(startIndex==index) return;
				var pnode = $("program",prgxmldoc);
				var fnode = $("screen",pnode).eq(0);
				var source = $("section[idx="+idx+"]",fnode);
				var dest = $("section",fnode).eq(index);
				if(startIndex>index) {
					dest.before(source);
					idx = eval(idx) - difIdx;
				} else {
					dest.after(source);
					idx = eval(idx) + difIdx;
				}
				updateSectionListing(pnode,"screen",$("#sectionlisting"),$("#sublistcolumn"));
				//console.log("click section index = "+idx);
				$("li[idx="+idx+"]",$("#sectionlisting")).addClass("highlight-state").trigger("click");
			}
		});
		$("#sectionlisting li").each(function (index,element) {
			bindingSectionListItem($(this),$("#sectionlisting"),element,function(src,list,sect,subcol) { 
				//console.log("click item : "+src.attr("idx"));
			});
		});

		$("#fieldfiltering").change(function() { 
			$("#fieldcondition").attr("disabled",!$("#fieldfiltering").is(":checked"));
		}).trigger("change");
		$("#fieldlisting").change(function() { 
			$("#fieldlistingalign").attr("disabled",!$("#fieldlisting").is(":checked"));
		}).trigger("change");

		$("#fieldtag").change(function() { 
			var atag = $("#fieldtag").val();
			if(!("money"==atag)) {
				$("#fielddecimal").val("");
			} else {
				var dc = $("#fielddecimal").data("decimal");
				if(dc) $("#fielddecimal").val(dc);
			}
			$("#fielddecimal").attr("disabled",!("money"==atag));
			$("#fieldcurrenting").attr("disabled",!("date"==atag || "year"==atag || "month"==atag || "hour"==atag || "minute"==atag || "time"==atag));
			$("#fielddecimalformat").attr("disabled",!("money"==atag || "int"==atag));
		}).trigger("change");
		$("#fielddatatype").change(function() {
			changeDataType("#fielddatatype","#fieldtag","#fieldtype");
		});
		$("#lookupfield").change(function() { 
			var tick = $("#lookupfield").is(":checked");
			$("#lookupurl").attr("disabled",!tick);
			$("#lookupkeyfield").attr("disabled",!tick);
			$("#lookupvaluefield").attr("disabled",!tick);
			$("#lookupcategory").attr("disabled",!tick);
			$("#lookuptablename").attr("disabled",!tick);
			$("#lookuptablekeyfield").attr("disabled",!tick);
			$("#lookuptablevaluefield").attr("disabled",!tick);
			$("#lookupinifile").attr("disabled",!tick);
			$("#lookupinisection").attr("disabled",!tick);
			$("#lookupcustomfile").attr("disabled",!tick);
			$("#lookupcustomsection").attr("disabled",!tick);
		}).trigger("change");

		$("#newfieldfiltering").change(function() { 
			$("#newfieldcondition").attr("disabled",!$("#newfieldfiltering").is(":checked"));
		}).trigger("change");
		$("#newfieldlisting").change(function() { 
			$("#newfieldlistingalign").attr("disabled",!$("#newfieldlisting").is(":checked"));
		}).trigger("change");

		$("#newfieldtag").change(function() { 
			var atag = $("#newfieldtag").val();
			if(!("money"==atag)) {
				$("#newfielddecimal").val("");
			}
			$("#newfielddecimal").attr("disabled",!("money"==atag));
			$("#newfieldcurrenting").attr("disabled",!("date"==atag || "year"==atag || "month"==atag || "hour"==atag || "minute"==atag || "time"==atag));
			$("#newfielddecimalformat").attr("disabled",!("money"==atag || "int"==atag));
		}).trigger("change");
		$("#newfielddatatype").change(function() {
			changeDataType("#newfielddatatype","#newfieldtag","#newfieldtype");
		});
		$("#newlookupfield").change(function() { 
			var tick = $("#newlookupfield").is(":checked");
			$("#newlookupurl").attr("disabled",!tick);
			$("#newlookupkeyfield").attr("disabled",!tick);
			$("#newlookupvaluefield").attr("disabled",!tick);
			$("#newlookupcategory").attr("disabled",!tick);
			$("#newlookuptablename").attr("disabled",!tick);
			$("#newlookuptablekeyfield").attr("disabled",!tick);
			$("#newlookuptablevaluefield").attr("disabled",!tick);
			$("#newlookupinifile").attr("disabled",!tick);
			$("#newlookupinisection").attr("disabled",!tick);
			$("#newlookupcustomfile").attr("disabled",!tick);
			$("#newlookupcustomsection").attr("disabled",!tick);
		}).trigger("change");
		$("#fieldtype").change(function() { 
			var ftype = $("#fieldtype").val();
			if("select"==ftype || "list"==ftype) {
				$("#valuelayer").hide();
				$("#optionvaluelayer").show();
			} else {
				$("#valuelayer").show();
				$("#optionvaluelayer").hide();
			}
			if("hidden"==ftype) {
				$("#fieldtag").attr("disabled",false);
				$("#fieldtag").val("");
			}
		}).trigger("change");
		$("#newfieldtype").change(function() { 
			var ftype = $("#newfieldtype").val();
			if("select"==ftype || "list"==ftype) {
				$("#newvaluelayer").hide();
				$("#newoptionvaluelayer").show();
			} else {
				$("#newvaluelayer").show();
				$("#newoptionvaluelayer").hide();
			}
			if("hidden"==ftype) {
				$("#newfieldtag").attr("disabled",false);
				$("#newfieldtag").val("");
			}
		}).trigger("change");
		$("#sectionincreaseindent").change(function() { 
			updateMarginLeftStyle("#sectionincreaseindent","#sectionstyle");
		});
		$("#newsectionincreaseindent").change(function() { 
			updateMarginLeftStyle("#newsectionincreaseindent","#newsectionstyle");
		});
		$("#fieldcondition").change(function() { 
			if("between"==$("#fieldcondition").val()) {
				$("#ranged_row").show();
			} else {
				$("#ranged_row").hide();
			}
		});
		$("#newfieldcondition").change(function() { 
			if("between"==$("#newfieldcondition").val()) {
				$("#newranged_row").show();
			} else {
				$("#newranged_row").hide();
			}
		});
		var $fc = $("#functionfieldcondition").empty();
		var $nfc = $("#newfunctionfieldcondition").empty();
		$fc.append($("<option value=''></option>"));
		$nfc.append($("<option value=''></option>"));
		for(var p in calculator_functions) {
			$fc.append($("<option value='"+p+"'>"+calculator_functions[p].text+"</option>"));
			$nfc.append($("<option value='"+p+"'>"+calculator_functions[p].text+"</option>"));
		}
		$("#functionfieldcondition").change(function() { displayCalculatorFunction("functionoptiontablebody",$("#functionfieldcondition").val()); });
		$("#newfunctionfieldcondition").change(function() { displayCalculatorFunction("newfunctionoptiontablebody",$("#newfunctionfieldcondition").val()); });
		$("#lookupmapping").change(function() { 
			if($("#lookupmapping").is(":checked")) {
				$("#lookupmappingrow").show();
				$("#lookupservicekeyfieldlayer").hide();
				$("#lookuptablekeyfieldlayer").hide();
			} else {
				$("#lookupmappingrow").hide();
				$("#lookupservicekeyfieldlayer").show();
				$("#lookuptablekeyfieldlayer").show();
			}
		}).trigger("change");
		$("#newlookupmapping").change(function() { 
			if($("#newlookupmapping").is(":checked")) {
				$("#newlookupmappingrow").show();
				$("#newlookupservicekeyfieldlayer").hide();
				$("#newlookuptablekeyfieldlayer").hide();
			} else {
				$("#newlookupmappingrow").hide();
				$("#newlookupservicekeyfieldlayer").show();
				$("#newlookuptablekeyfieldlayer").show();
			}
		}).trigger("change");
		$("#lookupgetting").change(function() { 
			if($("#lookupgetting").is(":checked")) {
				$("#lookupmapping").attr("checked",true).trigger("change");
			}
		});
		$("#newlookupgetting").change(function() { 
			if($("#newlookupgetting").is(":checked")) {
				$("#newlookupmapping").attr("checked",true).trigger("change");
			}
		});
}
function updateMarginLeftStyle(indent,sectstyle) {
	var found = false;
	var str = ""; 
	var sty = $(sectstyle).val();
	var stys = sty.split(";");
	$(stys).each(function(idx,elm) {
		var ind = elm.indexOf("margin-left");
		if(ind>=0) {
			found = true;
			str = $(indent).val() + str;
		} else {
			if($.trim(elm)!="") str = str + elm+";";
		}
	});
	if(!found) {
		str = $(indent).val() + str;
	}
	$(sectstyle).val(str);
}
function changeDataType(fielddatatype,fieldtag,fieldtype) {
	var afielddatatype = $(fielddatatype);
	var afieldtag = $(fieldtag);
	var afieldtype = $(fieldtype).val();
	var atype = afielddatatype.val();
	if("string"==atype) afieldtag.val("");
	else if("decimal"==atype) afieldtag.val("money");
	else afieldtag.val(atype);
	afieldtag.trigger("change");
	if("date"==atype || "time"==atype) {
		//$("#"+afieldtag.attr("id")+"layer").hide(); 
		if("hidden"==afieldtype) {
			afieldtag.attr("disabled",false);
			afieldtag.val("");
		} else {
			afieldtag.attr("disabled",true);
		}
	} else {
		//$("#"+afieldtag.attr("id")+"layer").show();
		afieldtag.attr("disabled",false);
	}
}
function bindingFilterListItem($li,$list,fn) {
	$li.click(function() {
		$("li",$list).each(function(){
			$(this).removeClass("highlight-state");
		});
		$li.addClass("highlight-state");
		//console.log("click item : "+$li.attr("class"));
		if(fn) fn($li,$list);
	});
}
function bindingSectionListItem($li,$list,sectionElement,$subcol,fn) {
	$li.click(function() {
		$("li",$list).each(function(){
			$(this).removeClass("highlight-state");
		});
		$li.addClass("highlight-state");
		//console.log("click section item : "+$li.attr("class"));
		$subcol.data("li",$li);
		if(fn) fn($li,$list,sectionElement,$subcol);
	});
}
function bindingSubListItem($li,$list,$subcol,fn) {
	$li.click(function() {
		if(!$subcol) $subcol = $("#sublistcolumn");
		$("li",$subcol).each(function(){
			$(this).removeClass("active-state");
		});
		$li.addClass("active-state");
		//console.log("click sub item : "+$li.attr("class"));
		if(fn) fn($li,$list);
	});
}
function bindingRowListLayer($li,$list,rowElement,fn) {
	$li.click(function() {
		//$("li",$list).removeClass("active-state");
		$("div",$list).each(function(){
			$(this).removeClass("active-state");
		});
		$li.addClass("active-state");
		//console.log("click item : "+$li.attr("class"));
		if(fn) fn($li,$list,rowElement);
	});
}
function initialFields() {
	alreadyGenerated = false;
	$("#filterlisting").empty();
	$("#sectionlisting").empty();
	$("#subitemlisting").empty();
	$("#sublistcolumn").empty();
	filterCounter = 0;
	sectionCounter = 0;
	rowCounter = 500;
	columnCounter = 500;
	$(".control-property").hide();
	$("#propertyheaderlabel").html("");
	$("#fieldstable").hide();
}
function clearingFields() {
	fsentryform.reset();
	$("#filename").val("");
	initialFields();
}
function validExplore() {
		if($.trim($("#filename").val())=="") {
			alertmsg("QS0102",$("#filename_label").html()+" is undefined");
			return false;
		}
		return true;
}
function search(aform) {
		if(!validExplore()) return false;
		if(!aform) aform = fssearchform;
		//alert($(aform).serialize());
		alreadyGenerated = false;
		var xmlfile = $("#filename").val();
		//console.log("explore : "+"../program/"+xmlfile);
		startWaiting();
		jQuery.ajax({
			url: "../program/"+xmlfile,
			type: "POST",
			dataType: "xml",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) {
				console.log("error : "+transport.responseText);
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				//alert(transport.responseText);
				searchComplete(transport,data);
			}
		});	
}
function checking(pid) {
	if($.trim(pid)=="") return;
	jQuery.ajax({
		url: "sfte004_ck.jsp",
		type: "POST",
		data: "fsAjax=true&progid="+pid,
		dataType: "json",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) {
			console.log("checking error : "+transport.responseText);
		},
		success: function(data,status,transport){
			if(data["files"]) {
				alreadyGenerated = data["files"]>0;
			}
		}
	});	
}
function searchComplete(xhr,data) {
		stopWaiting();
		initialFields();
		showPageEntry();
		prgxmldoc = data;
		updateToScreen();
		checking($("#progid").val());
		$("#saveasbutton").attr("disabled",false);
		$("#generatebutton").attr("disabled",false);
}
function updateToScreen() {
	var pnode = $("program",prgxmldoc);
	$("#xmlfile").val($("#filename").val());
	$("#progid").val($("progid",pnode).text());
	$("#tablename").val($("tablename",pnode).text());
	$("#progname").val($("caption",pnode).text());
	$("#headline").val($("headline",pnode).text());
	$("#subheadline").val($("subheadline",pnode).text());
	$("#alwaysbox").attr("checked",true);
	var doc = $(prgxmldoc);
	if(doc.find("keys").size()==0) {
		var fnode = doc.find("filters");
		var knode = $("<keys></keys>");
		var snode = $("<section></section>");
		var lnode = $("<label>Keys</label>");
		var tnode = $("<table></table>");
		var rnode = $("<row></row>");
		snode.attr("design",true);
		tnode.append(rnode);
		snode.append(lnode).append(tnode);
		knode.append(snode);
		knode.insertAfter(fnode);
	}	
	//$("#alwaysbox").attr("checked","true"==$("saveasnew",pnode).text());
	$("#actiondeletebutton").attr("checked","true"==$("deletebutton",pnode).text());
	$("#actioneditbutton").attr("checked","true"==$("editbutton",pnode).text());
	$("#actionfindbutton").attr("checked","true"==$("findbutton",pnode).text());
	$("#genexportbutton").attr("checked","true"==$("exportbutton",pnode).text());
	$("#entrypagebox").attr("checked","true"==$("entrypage",pnode).text());
	$("#usingdialogbox").attr("checked","true"==$("usingdialog",pnode).text());
	if(doc.find("deletebutton").size()==0) { $("#actiondeletebutton").attr("checked",true); }
	if(doc.find("editbutton").size()==0) { $("#actioneditbutton").attr("checked",true); }
	if(doc.find("findbutton").size()==0) { $("#actionfindbutton").attr("checked",true); }
	if(doc.find("exportbutton").size()==0) { $("#genexportbutton").attr("checked",true); }
	if(doc.find("entrypage").size()==0) { $("#entrypagebox").attr("checked",false); }
	if(doc.find("usingdialog").size()==0) { $("#usingdialogbox").attr("checked",true); }
	updateSectionListing(pnode,"filters",$("#filtersectionlisting"),$("#filtersublistcolumn"));
	updateSectionListing(pnode,"keys",$("#keysectionlisting"),$("#keysublistcolumn"));
	updateSectionListing(pnode,"screen",$("#sectionlisting"),$("#sublistcolumn"));
	$("li",$("#filtersectionlisting")).eq(0).trigger("click");
	$("li",$("#keysectionlisting")).eq(0).trigger("click");
}
function updateSectionListing(pnode,nodename,$slist,$subcol) {
	if(!$slist) $slist = $("#sectionlisting");
	$slist.empty();
	var snode = $(nodename,pnode).eq(0);
	$("section",snode).each(function(sectionIndex,sectionElement) {
		sectionCounter = sectionIndex;
		var $sec = $(sectionElement);
		$sec.attr("idx",sectionIndex);
		var label = null;
		var $fe = $sec.children().eq(0);
		var tag = $fe.prop("tagName");
		//console.log(sectionIndex+" = "+tag);
		var liclass = "ui-state-default";
		if("part"==tag) {
			label = $("label",$fe).eq(0).text();
			liclass += " part-class";
		} else if("subpart"==tag) {
			label = $("label",$fe).eq(0).text();
			liclass += " subpart-class";
		} else if("horizon"==tag) {
			label = "Horizontal Line";
			liclass += " horizontal-class";
		} else {
			label = $("label",$sec).eq(0).text();
			liclass += " section-class";
		}
		if(label!=null) {
			if($.trim(label)=="") label = " ";
			var $li = $("<li></li>");
			$li.addClass(liclass);
			$li.attr("idx",sectionIndex);
			$li.data("sectiondata",sectionElement);
			$li.html(label);
			$slist.append($li);
			$li.data("nodename",nodename);
			bindingSectionListItem($li,$slist,sectionElement,$subcol,updateSectionToScreen);
		}
	});		
}
function updateSectionToScreen(src,list,sect,$subcol) {
	$("#propertyheaderlabel").html("");
	$(".control-property").hide();
	$("#fieldstable").hide();
	if(!$subcol) $subcol = $("#sublistcolumn");
	$subcol.empty();
	var $li = $(src);
	var $sec = $(sect);
	$subcol.data("sectiondata",$sec);
	if($li.is(".section-class")) {
		var secIdx = $sec.attr("idx");
		var $table = $("table",$sec).eq(0);
		//console.log("table="+$table.attr("class")+", row="+$("row",$table).size());
		//$("row",$table).each(function(rowIndex,rowElement) {
		$table.children().each(function(rowIndex,rowElement) {
			//console.log("rowIndex="+rowIndex+", row : "+rowElement);
			var $subdiv = $("<div></div>");
			$subdiv.attr("id","sublistinglayer"+rowIndex);
			$subdiv.addClass("portal-area sublayer-class");
			$subdiv.data("rowdata",$(rowElement));
			$subdiv.data("sectiondata",$sec);
			$subdiv.attr("idx",rowIndex);
			$subdiv.attr("sidx",secIdx);
			bindingRowListLayer($subdiv,$subcol,rowElement,function(src,list,rowElem) {
				//console.log("current row = "+rowElem);
			});
			$subcol.append($subdiv);
			var $sit = $("<ul></ul>");
			$sit.attr("id","subitemlisting"+rowIndex);
			$sit.addClass("sublisting-class");
			$subdiv.append($sit);
			$sit.sortable({
				placeholder: "ui-state-active",
				start : function(evt,ui) {
					var $it = $(ui.item);
					startIndex = $it.index();
				},
				stop : function(evt,ui) {
					var $it = $(ui.item);
					var index = $it.index();
					var idx = $it.attr("idx");
					var difIdx = Math.abs(eval(startIndex) - eval(index));
					//console.log("start Idx = "+startIndex+", index="+index+", idx="+idx+", dif="+difIdx);
					if(startIndex==index) return;
					var rowNode = $it.data("rowdata");
					if(!rowNode) return;
					var pnode = rowNode; 
					var source = $("col[idx="+idx+"]",pnode);
					var dest = $("col",pnode).eq(index);
					if(startIndex>index) {
						dest.before(source);
						idx = eval(idx) - difIdx;
					} else {
						dest.after(source);
						idx = eval(idx) + difIdx;
					}
					updateSectionToScreen(src,list,sect,$subcol);
					var $div = $("div.sublayer-class",$subcol).eq(rowIndex);
					$div.trigger("click");
					var $ul = $("ul",$div);
					$("li[idx="+idx+"]",$ul).trigger("click");
				}
			});
			//$subdiv.attr("colsize",$("col",$(rowElement)).size());
			//$("col",$(rowElement)).each(function(columnIndex,columnElement) { 
			$(rowElement).children().each(function(columnIndex,columnElement) { 
				//console.log("column = "+columnIndex+" : "+columnElement);
				var $col = $(columnElement);
				$col.attr("idx",columnIndex);
				var cliclass = "ui-state-default";
				var clabel = null;
				var $ce = $col.children().eq(0);
				var ctag = $ce.prop("tagName");
				var ctype = $ce.attr("type");
				var cname = $ce.attr("name");
				//console.log("column = "+columnIndex+" > tag = "+ctag+", type="+ctype);
				if("input"==ctag) {
					var lbltext = $ce.attr("label");
					if(!lbltext) lbltext = "";
					if($col.children().size()>1) {
						var $lbl = $("label",$col).eq(0);
						lbltext = $lbl.text();
					}
					clabel = lbltext+"("+$ce.attr("name")+")";
					cliclass += " input-class";
				} else if("label"==ctag) {
					clabel = $ce.text();
					cliclass += " label-class";									
				} else if("img"==ctag) {
					var imgsrc = $ce.attr("src");
					var idx = imgsrc.lastIndexOf("/");
					if(idx>0) imgsrc = imgsrc.substring(idx+1);
					clabel = imgsrc;
					cliclass += " image-class";
				}
				//console.log("clabel = "+clabel);
				if(clabel) {
					var $cdiv = $("<div></div>");
					$cdiv.addClass("collayer-class");
					if("input"==ctag) {
						if("radio"==ctype) {
							$cdiv.append($("<i class='fa fa-dot-circle-o' aria-hidden='true'></i>"));
							$cdiv.append("&#160;");
						} else if("checkbox"==ctype) {
							$cdiv.append($("<i class='fa fa-check-square-o' aria-hidden='true'></i>"));
							$cdiv.append("&#160;");
						} else {
							if("true"==$ce.attr("calfield")) {
								$cdiv.append($("<i class='fa fa-cogs' aria-hidden='true'></i>"));
							} else {
								$cdiv.append($("<i class='fa fa-pencil-square-o' aria-hidden='true'></i>"));
							}
							$cdiv.append("&#160;");
						}
					} else if("img"==ctag) {
						$cdiv.append($("<i class='fa fa-picture-o' aria-hidden='true'></i>"));
						$cdiv.append("&#160;");
					}
					var $span = $("<span></span>");
					$span.html(clabel);
					$cdiv.append($span);
					var $cli = $("<li></li>");
					$cli.addClass(cliclass);
					$cli.attr("idx",columnIndex);
					$cli.attr("sidx",secIdx);
					$cli.data("rowdata",$(rowElement));
					$cli.data("coldata",$(columnElement));
					//$cli.html(clabel);
					$cli.append($cdiv);
					$sit.append($cli);
					$cli.data("nodename",$li.data("nodename"));
					bindingSubListItem($cli,$sit,$subcol,inputFieldClick);
				}
			});
		});
		$("#fieldstable").show();
		$("#propertyheaderlabel").html("Section");
		$("#section_layer_control").show();
		var sid = $sec.attr("id");
		var design = $sec.attr("design");
		if(!sid) sid = "";
		if(!design) design = "";
		var oldid = sid;
		$("#sectionid").val(sid);
		var $lbl = $("label",$sec).eq(0);
		var label = $lbl.text();	
		$("#sectionlabel").val(label);
		var sstyle = $sec.attr("style");
		if(!sstyle) sstyle = "";
		$("#sectionstyle").val(sstyle);
		$("#sectionincreaseindent").val("");
		$("#sectiondesign").attr("checked","true"==design);
		var btn = $("#updatesectionbutton");
		btn.unbind("click");
		btn.bind("click",function() { 
				sid = $("#sectionid").val();
				if(!validUpdateDuplicateID(sid,oldid)) return;
				if($.trim(sid)!="") { $sec.attr("id",sid); } else { $sec.removeAttr("id"); }
				sstyle = $("#sectionstyle").val();
				if($.trim(sstyle)!="") { $sec.attr("style",sstyle); } else { $sec.removeAttr("style"); }
				design = $("#sectiondesign").is(":checked");
				if(design) { $sec.attr("design",true); } else { $sec.removeAttr("design"); }
				var lbtxt = $("#sectionlabel").val();
				if($.trim(lbtxt)=="") lbtxt = " ";
				$lbl.text(lbtxt);
				$li.html(lbtxt);
				alertmsg("QS0104","Update success");
		});
		$("div",$subcol).eq(0).trigger("click");
	} else if($li.is(".part-class")) {
		//$("#fieldstable").show();
		$("#propertyheaderlabel").html("Part");
		$("#part_layer_control").show();
		var $fe = $sec.children().eq(0);
		var pid = $fe.attr("id");
		if(!pid) pid = "";
		var oldid = pid;
		$lbl = $("label",$fe).eq(0);
		$title = $("title",$fe).eq(0);
		var label = $lbl.text();
		var title = $title.text();
		$("#partid").val(pid);
		$("#partlabel").val(label);
		$("#parttitle").val(title);
		var pstyle = $fe.attr("style");
		if(!pstyle) pstyle = "";
		$("#partstyle").val(pstyle);
		var btn = $("#updatepartbutton");
		btn.unbind("click");
		btn.bind("click",function() { 
				pid = $("#partid").val();
				if(!validUpdateDuplicateID(pid,oldid)) return;
				if($.trim(pid)!="") { $fe.attr("id",pid); } else { $fe.removeAttr("id"); }
				pstyle = $("#partstyle").val();
				if($.trim(pstyle)!="") { $fe.attr("style",pstyle); } else { $fe.removeAttr("style"); }
				$title.text($("#parttitle").val());
				$lbl.text($("#partlabel").val());
				$li.html($("#partlabel").val());
				alertmsg("QS0104","Update success");
		});
	} else if($li.is(".subpart-class")) {
		//$("#fieldstable").show();
		$("#propertyheaderlabel").html("Sub Part");
		$("#subpart_layer_control").show();
		var $fe = $sec.children().eq(0);
		var spid = $fe.attr("id");
		if(!spid) spid = "";
		var oldid = spid;
		$lbl = $("label",$fe).eq(0);
		var label = $lbl.text();	
		$("#subpartid").val(spid);
		$("#subpartlabel").val(label);
		var spstyle = $fe.attr("style");
		if(!spstyle) spstyle = "";
		$("#subpartstyle").val(spstyle);
		var btn = $("#updatesubpartbutton");
		btn.unbind("click");
		btn.bind("click",function() {
				spid = $("#subpartid").val();
				if(!validUpdateDuplicateID(spid,oldid)) return;
				if($.trim(spid)!="") { 	$fe.attr("id",spid); } else { $fe.removeAttr("id"); }
				spstyle = $("#subpartstyle").val();
				if($.trim(spstyle)!="") { $fe.attr("style",spstyle); } else { $fe.removeAttr("style"); }
				$lbl.text($("#subpartlabel").val());
				$li.html($("#subpartlabel").val());
				alertmsg("QS0104","Update success");
		});
	} else if($li.is(".horizontal-class")) {
		$("#propertyheaderlabel").html("Horizontal");
	} 
}
var prog_texts = "<program><progid></progid><tablename></tablename><caption></caption>";
prog_texts += "<headline></headline><subheadline></subheadline><saveasnew>false</saveasnew><deletebutton>true</deletebutton><editbutton>true</editbutton><findbutton>true</findbutton><exportbutton>false</exportbutton><usingdialog>true</usingdialog><entrypage>false</entrypage>";
prog_texts += "<filters><section design='true'><label>Filters</label><table><row></row></table></section></filters>";
prog_texts += "<keys><section design='true'><label>Keys</label><table><row></row></table></section></keys>";
prog_texts += "<screen></screen></program>";
function insert() {
		clearingFields();
		fsentryform.fsDatatype.value = "json";
		fsentryform.fsAction.value = "enter";
		$("input.ikeyclass",fsentryform).editor({edit:true});
		showPageEntry();
		prgxmldoc = $.parseXML(prog_texts);
		updateToScreen();
		$("#saveasbutton").attr("disabled",true);
		$("#generatebutton").attr("disabled",true);
}
function showPageSearch() {
		$("#entrypanel").hide();
		$("#searchpanel").show();
}
function showPageEntry() {
		$("#entrypanel").show();
		$("#searchpanel").show();
}
function prepareScreenToUpdate(aform,data) {
		aform.fsDatatype.value = "text";
		aform.fsAction.value = "edit";
		try {
		} catch(ex) { }
		$("input.ikeyclass",aform).editor({edit:false});
		showPageEntry();
}
function cancel() {
		confirmCancel(function() {
			clearingFields();
			showPageSearch();
		});
}
function validForm() {
	if($.trim($("#progid").val())=="") {
		alertmsg("QS0123",$("#progid_label").text()+" is undefined",null,function() { 
			setTimeout(function() { $("#progid").focus(); }, 500);
		});
		return false;
	}	
	if($.trim($("#progname").val())=="") {
		alertmsg("QS0111",$("#progname_label").text()+" is undefined",null,function() { 
			setTimeout(function() { $("#progname").focus(); }, 500);
		});
		return false;
	}
	/*
	if($.trim($("#tablename").val())=="") {
		alertmsg("QS0110",$("#tablename_label").text()+" is undefined",null,function() { 
			setTimeout(function() { $("#tablename").focus(); }, 500);
		});
		return false;
	}	
	*/
	return true;
}
function updateContents(aform,saveas) {
	if(!aform) aform = fsentryform;
	aform.xmltext.value = "";
	if(!prgxmldoc) return;
	var pid = $("#progid").val();
	var prgname = $("#progname").val();
	if(saveas) {
		pid = $("#saveasprogid").val(); 
		prgname = $("#saveasprogname").val();
	}
	var doc = $(prgxmldoc);	
	doc.find("progid").text(pid);
	doc.find("caption").text(prgname);
	doc.find("tablename").text($("#tablename").val());
	doc.find("headline").text($("#headline").val());
	doc.find("subheadline").text($("#subheadline").val());
	doc.find("saveasnew").text($("#alwaysbox").is(":checked"));
	doc.find("deletebutton").text($("#actiondeletebutton").is(":checked"));
	doc.find("editbutton").text($("#actioneditbutton").is(":checked"));
	doc.find("findbutton").text($("#actionfindbutton").is(":checked"));
	doc.find("exportbutton").text($("#genexportbutton").is(":checked"));
	doc.find("usingdialog").text($("#usingdialogbox").is(":checked"));
	doc.find("entrypage").text($("#entrypagebox").is(":checked"));
	var node = doc.find("saveasnew");
	if(doc.find("deletebutton").size()==0) {
		var btn = $("<deletebutton></deletebutton>");
		btn.text($("#actiondeletebutton").is(":checked"));
		btn.insertAfter(node);
	}
	if(doc.find("editbutton").size()==0) {
		var btn = $("<editbutton></editbutton>");
		btn.text($("#actioneditbutton").is(":checked"));
		btn.insertAfter(node);
	}
	if(doc.find("findbutton").size()==0) {
		var btn = $("<findbutton></findbutton>");
		btn.text($("#actionfindbutton").is(":checked"));
		btn.insertAfter(node);
	}
	if(doc.find("exportbutton").size()==0) {
		var btn = $("<exportbutton></exportbutton>");
		btn.text($("#genexportbutton").is(":checked"));
		btn.insertAfter(node);
	}
	if(doc.find("usingdialog").size()==0) {
		var btn = $("<usingdialog></usingdialog>");
		btn.text($("#usingdialogbox").is(":checked"));
		btn.insertAfter(node);
	}
	if(doc.find("entrypage").size()==0) {
		var btn = $("<entrypage></entrypage>");
		btn.text($("#entrypagebox").is(":checked"));
		btn.insertAfter(node);
	}
	$("input",prgxmldoc).each(function(index,element) { 
		$(this).removeAttr("idx");
	});
	$("section",prgxmldoc).each(function(index,element) { 
		$(this).removeAttr("idx");
	});
	$("col",prgxmldoc).each(function(index,element) { 
		$(this).removeAttr("idx");
	});
	aform.xmltext.value = xmlToString(prgxmldoc);
}
function save(aform) {
		if(!aform) aform = fsentryform;
		updateContents(aform);
		//if(!confirm(aform.xmltext.value)) return false;
		//alert($(aform).serialize());
		if(!validForm()) return false;
		var isInsertMode = aform.fsAction.value=='enter';
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte004_dc.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					successbox(function() { $("#generatebutton").attr("disabled",false); });					
					var json = $.parseJSON($.trim(data));
					addDropDownBox(json);
					if(json["filename"]) {
						$("#filename").val(json["filename"]);
					}
					if($.trim($("#progid").val())=="") {
						if(json["name"]) {
							$("#progid").val(json["name"]);
						}
					}
				}
			});
		});
		return false;
}
function saveAs(aform) {
		if(!aform) aform = fsentryform;
		updateContents(aform,true);
		//alert($(aform).serialize());
		if(!validForm()) return false;
		var isInsertMode = aform.fsAction.value=='enter';
		confirmSaveAs(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte004_sc.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					var json = $.parseJSON($.trim(transport.responseText));
					var progid = "";
					if(json["name"]) progid = "("+json["name"]+")";
					successbox(function() { clearingFields(); },[progid]);					
					showPageSearch();
					addDropDownBox(json);
					if(json["name"]) {
						$("#progid").val(json["name"]);
					}
				}
			});
		});
		return false;
}
function addDropDownBox(json) {
	if(json["filename"] && json["caption"]) {
		var found = false;
		var filename = json["filename"];
		$("option",$("#filename")).each(function(idx,elm) { 
			if($(elm).attr("value")==filename) { found = true; return; }
		});
		if(!found) {
			var opt = $("<option></option>");
			opt.attr("value",filename);
			opt.text(filename+" - "+json["caption"]);
			opt.appendTo($("#filename"));
		}
	}
}
function generate(aform) {
		if(!aform) aform = fsentryform;
		updateContents(aform);
		//alert($(aform).serialize());
		if(!validForm()) return false;
		var isInsertMode = aform.fsAction.value=='enter';
		confirmGenerate(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte004_gc.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					var progid = "";
					var json = $.parseJSON($.trim(data));
					if(json["name"]) progid = "("+json["name"]+")";
					generatebox(function() { clearingFields(); },[progid]);					
					showPageSearch();
					addDropDownBox(json);
					if($.trim($("#progid").val())=="") {
						if(json["name"]) {
							$("#progid").val(json["name"]);
						}
					}
				}
			});
		});
		return false;
}
function preview(aform) {
		if(!aform) aform = fsentryform;
		updateContents(aform);
		//alert($(aform).serialize());
		if(!validForm()) return false;
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte004_pc.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					var json = $.parseJSON($.trim(data));
					if(json["name"]) {
						var progid = json["name"];
						showPreview("../"+progid+"/"+progid+".jsp?clear=true&seed="+Math.random());
					}
				}
			});
		return false;
}
function showPreview(src) {
	var preframe = $("#previewprogramframe");
	preframe.attr("src",src);
	var predialog = $("#preview_dialog");
	var premodialog = $("#preview_modal_dialog");
	var dw = premodialog.width();
	var dh = predialog.height();
	preframe.width(dw-50);
	preframe.height(dh-10);
	predialog.modal("show");	
}
function confirmGenerate(okFn, cancelFn, width, height) {
	if(!confirmDialogBox("QS0007",null,"Do you want to save and generate this program?",okFn,cancelFn,width,height)) return false;
	return true;
}
function confirmDeleteFilter(paras, okFn, cancelFn, width, height) {
	if(!confirmDialogBox("QS0008",paras,"Do you want to delete this filter %s ?",okFn,cancelFn,width,height)) return false;
	return true;
}
function confirmDeleteSection(paras, okFn, cancelFn, width, height) {
	if(!confirmDialogBox("QS0009",paras,"Do you want to delete this section %s ?",okFn,cancelFn,width,height)) return false;
	return true;
}
function confirmSaveAs(okFn, cancelFn, width, height) {
	if(!confirmDialogBox("QS0010",null,"Do you want to save as this transaction ?",okFn,cancelFn,width,height)) return false;
	return true;
}
function confirmDeleteField(paras, okFn, cancelFn, width, height) {
	if(!confirmDialogBox("QS0011",paras,"Do you want to delete this field %s ?",okFn,cancelFn,width,height)) return false;
	return true;
}
function confirmDeleteRow(paras, okFn, cancelFn, width, height) {
	if(!confirmDialogBox("QS0012",paras,"Do you want to delete this row ?",okFn,cancelFn,width,height)) return false;
	return true;
}
function generatebox(callback,paras) {
	alertbox("QS0013",callback,"Save and generate success %s",paras);
}
function addNewFilter() {
	addNewField($("#filtersublistcolumn"),false,true,false,"filters");
}
function deleteSelectedFilter() {
	deleteInputField("#filtersublistcolumn");
}
function addNewKey() {
	addNewField($("#keysublistcolumn"),true,false,false,"keys");
}
function deleteSelectedKey() {
	deleteInputField("#keysublistcolumn");
}
function addNewSection() {
	$("#newsectionid").val("");
	$("#newsectionlabel").val("");
	$("#newsectionstyle").val("");
	$("#newsectionincreaseindent").val("");
	$("#newsectiondesign").attr("checked",true);
	sectiondialog.dialog("option", "buttons", [{
		text: "Cancel",
			click: function() { $(this).dialog("close"); }
		}, {
			text: " OK ",
			click: function() {
				var sid = $("#newsectionid").val();
				if(!validateDuplicateID(sid)) return;
				var label = $("#newsectionlabel").val();
				if($.trim(label)=="") label = " ";
				createNewSection(sid,label,$("#newsectionstyle").val(),$("#newsectiondesign").is(":checked"));
				sectiondialog.dialog("close"); 
			}
    }]);
	sectiondialog.dialog("open");
}
function createNewSection(sid,label,sstyle,design) {
	sectionCounter++;
	var section_texts = "<section></section>";
	var label_texts = "<label></label>";
	var table_texts = "<table></table>";
	var row_texts = "<row></row>";
	var pnode = $("program",prgxmldoc);
	var snode = $("screen",pnode).eq(0);
	var $sect = $(section_texts);
	var $table = $(table_texts);
	var $row = $(row_texts);
	var $label =$(label_texts);
	if($.trim(sid)!="") $sect.attr("id",sid);
	if($.trim(sstyle)!="") $sect.attr("style",sstyle);
	if(design) $sect.attr("design",true);
	$label.append(label);
	$table.append($row);
	$sect.append($label);
	$sect.append($table);
	snode.append($sect);
	$sect.attr("idx",sectionCounter);
	var $div = $("<div></div>");
	$div.addClass("li-layer-class");
	$div.html(label);
	var $li = $("<li></li>");
	//$li.html(label);
	$li.attr("idx",sectionCounter);
	$li.data("sectiondata",$sect);
	$li.append($div);
	$li.addClass("section-class");
	$li.addClass("ui-state-default highlight-state");
	var $slist = $("#sectionlisting");
	$slist.append($li);
	bindingSectionListItem($li,$slist,$sect,$("#sublistcolumn"),updateSectionToScreen);
	$li.trigger("click");
	return true;
}
function deleteSelectedSection() {
	var lilist = $("#sectionlisting li.highlight-state");
	var sz = lilist.size();
	if(sz>0) {
		var $li = lilist.eq(0);
		confirmDeleteSection([$li.text()],function() { 
			var $li = lilist.eq(0);
			var elem = $li.data("sectiondata");
			$(elem).remove();
			$li.remove();
			$(".control-property").hide();
			$("#fieldstable").hide();
			$("#sublistcolumn").empty();
		});
	}
	return false;
}
function createNewPart(pid,label,title,pstyle) {
	sectionCounter++;
	var section_texts = "<section></section>";
    var part_texts = "<part></part>";
	var label_texts = "<label></label>";
	var title_texts = "<title></title>";
	var pnode = $("program",prgxmldoc);
	var snode = $("screen",pnode).eq(0);
	var $sect = $(section_texts);
	var $part = $(part_texts);
	var $label =$(label_texts);
	var $title = $(title_texts);
	if($.trim(pid)!="") $part.attr("id",pid);
	if($.trim(pstyle)!="") $part.attr("style",pstyle);
	$label.append(label);
	$title.append(title);
	$part.append($label);
	$part.append($title);
	$sect.append($part);
	snode.append($sect);
	$sect.attr("idx",sectionCounter);
	var $div = $("<div></div>");
	$div.addClass("li-layer-class");
	$div.html(label);
	var $li = $("<li></li>");
	//$li.html(label);
	$li.attr("idx",sectionCounter);
	$li.data("sectiondata",$sect);
	$li.append($div);
	$li.addClass("part-class");
	$li.addClass("ui-state-default highlight-state");
	var $slist = $("#sectionlisting");
	$slist.append($li);
	bindingSectionListItem($li,$slist,$sect,$("#sublistcolumn"),updateSectionToScreen);
	$li.trigger("click");
	return true;
}
function createNewSubPart(spid,label,spstyle) {
	sectionCounter++;
	var section_texts = "<section></section>";
    var subpart_texts = "<subpart></subpart>";
	var label_texts = "<label></label>";
	var pnode = $("program",prgxmldoc);
	var snode = $("screen",pnode).eq(0);
	var $sect = $(section_texts);
	var $subpart = $(subpart_texts);
	var $label =$(label_texts);
	if($.trim(spid)!="") $subpart.attr("id",spid);
	if($.trim(spstyle)!="") $subpart.attr("style",spstyle);
	$label.append(label);
	$subpart.append($label);
	$sect.append($subpart);
	snode.append($sect);
	$sect.attr("idx",sectionCounter);
	var $div = $("<div></div>");
	$div.addClass("li-layer-class");
	$div.html(label);
	var $li = $("<li></li>");
	//$li.html(label);
	$li.attr("idx",sectionCounter);
	$li.data("sectiondata",$sect);
	$li.append($div);
	$li.addClass("subpart-class");
	$li.addClass("ui-state-default highlight-state");
	var $slist = $("#sectionlisting");
	$slist.append($li);
	bindingSectionListItem($li,$slist,$sect,$("#sublistcolumn"),updateSectionToScreen);
	$li.trigger("click");
	return true;
}
function createNewHorizon(label) {
	sectionCounter++;
	if(!label) label = "Horizontal Line";
	var section_texts = "<section></section>";
    var horizon_texts = "<horizon></horizon>";
	var pnode = $("program",prgxmldoc);
	var snode = $("screen",pnode).eq(0);
	var $sect = $(section_texts);
	var $horizon = $(horizon_texts);
	$sect.append($horizon);
	snode.append($sect);
	$sect.attr("idx",sectionCounter);
	var $div = $("<div></div>");
	$div.addClass("li-layer-class");
	$div.html(label);
	var $li = $("<li></li>");
	//$li.html(label);
	$li.attr("idx",sectionCounter);
	$li.data("sectiondata",$sect);
	$li.append($div);
	$li.addClass("horizontal-class");
	$li.addClass("ui-state-default highlight-state");
	var $slist = $("#sectionlisting");
	$slist.append($li);
	bindingSectionListItem($li,$slist,$sect,$("#sublistcolumn"),updateSectionToScreen);
	$li.trigger("click");
	return true;
}
function addNewInput() {
	addNewField($("#sublistcolumn"),false,false,false,"screen");
}
function addNewField($subcol,keying,filtering,listing,nodename) {
	if(!ensureActiveRow($subcol)) return;
	$("#newreadonly_layer").show();
	$("#newfield_layer_control").show();
	$("#newlookup_layer_control").show();
	$("#newcolumn_layer_control").show();
	$("#newdisplay_layer_control").hide();
	$("#newvalidate_layer_control").show();
	$("#newcolumnscontrollayer").hide();
	$("#newfieldcols").val("");
	$("#newfieldid").val("");
	$("#newfieldname").val("");
	$("#newfieldvalue").val("");
	$("#newfieldstyle").val("");
	$("#newfieldwidth").val("");
	$("#newfieldheight").val("");
	$("#newfieldlabel").val("");
	$("#newfieldpicture").val("");
	$("#newfieldcontrol").val("");
	$("#newfieldkeyable").attr("checked",false);
	if(keying) $("#newfieldkeyable").attr("checked",true);
	$("#newfieldcurrenting").attr("checked",false);
	$("#newfieldreadonly").attr("checked",false);
	$("#newfielddisabled").attr("checked",false);
	$("#newfielddecimal").val("");
	$("#newfieldprecision").val("");
	$("#newfielddecimalformat").val("");
	$("#newfieldrequired").attr("checked",false);
	$("#newfieldcompleted").attr("checked",false);
	$("#newfieldalerted").attr("checked",false);
	$("#newfieldmsg").val("");
	$("#newfieldshowing").attr("checked",true);
	$("#newfieldlisting").attr("checked",false);
	if(listing) $("#newfieldlisting").attr("checked",true);
	$("#newfieldfiltering").attr("checked",false);
	if(filtering) $("#newfieldfiltering").attr("checked",true);
	$("#newfieldcondition").val("");
	$("#newfieldplacement").val("");
	$("#newfieldheaderlabel").attr("checked",false);
	$("#newlookupfield").attr("checked",false).trigger("change");
	$("#newlookupfieldmultiple").attr("checked",false);
	$("#newlookupurl").val("");
	$("#newlookupkeyfield").val("");
	$("#newlookupvaluefield").val("");
	$("#newlookupgetting").attr("checked",false);
	$("#newlookupcategory").val("");
	$("#newlookuptablename").val("");
	$("#newlookuptablekeyfield").val("");
	$("#newlookuptablevaluefield").val("");
	$("#newlookuptype").val("service");
	$("#newlookupservicelinker").trigger("click");
	$("#newlookupinifile").val("");
	$("#newlookupinisection").val("");
	$("#newlookupcustomfile").val("");
	$("#newlookupcustomsection").val("");
	$("#newfieldtype").trigger("change");
	$("#newcolumnalign").val("");
	$("#newfieldcolumnstyle").val("");
	$("#newfieldcolspan").val("");
	$("#newfieldrowspan").val("");
	$("#newfieldcolumns").val("3");
	$("#newrangelabel").val("");
	$("#newfieldlistingalign").val("");
	$("#newoptiontablebody").empty();
	$("#newfieldfiltering").trigger("change");
	$("#newfieldlisting").trigger("change");
	$("#newfieldcondition").trigger("change");
	$("#newlookupoptiontablebody").empty();
	$("#newlookupmapping").attr("checked",false);
	$("#newlookupmapping").trigger("change");
	fielddialog.dialog("option", "title","New Input Field");
	fielddialog.dialog("option", "buttons", [{
		text: "Cancel",
			click: function() { $(this).dialog("close"); }
		}, {
			text: " OK ",
			click: function() { 
				var eid = $("#newfieldid").val();
				if(!validateDuplicateID(eid)) return;
				var ename = $("#newfieldname").val();
				if($.trim(ename)!="") {
					var etype = $("#newfieldtype").val();
					if("radio"==etype || "checkbox"==etype) {
						var evalue = $("#newfieldvalue").val();
						if($.trim(evalue)=="") {
							alertmsg("QS0108","Require value for radio or checkbox type");
							return;
						}
					} else {
						if(!validateDuplicateName(ename,nodename)) return;
					}
					if(etype=="select" || etype=="list") {
						if(!validInputOption("#newoptiontablebody")) {
							alertmsg("QS0112","Duplicate option value");
							return;
						}
					}
					createNewInput($subcol,nodename);
					fielddialog.dialog("close"); 
				}
			}
    }]);
	fielddialog.dialog("open");
}
function deleteSelectedField() {
	deleteInputField("#sublistcolumn");
}
function deleteInputField(subcol) {
	var lilist = $(subcol+" li.active-state");
	var sz = lilist.size();
	if(sz>0) {
		var $li = lilist.eq(0);
		confirmDeleteField([$li.text()],function() { 
			var $li = lilist.eq(0);
			var elem = $li.data("coldata");
			$(elem).remove();
			$li.remove();
			$(".control-property").hide();
		});
	} else {
		var divlist = $(subcol+" div.active-state");
		sz = divlist.size();
		if(sz>0) {
			confirmDeleteRow(null,function() { 
				var $div = divlist.eq(0);
				var elem = $div.data("rowdata");
				$(elem).remove();
				$div.remove();
				$(".control-property").hide();
			});
		}
	}
	return false;
}
function xmlToString(xmldoc) {
    try {
		return (new XMLSerializer()).serializeToString(xmldoc);
	}catch(ex) { 
		try {
			return xmldoc.xml;
		}catch(exc) { }
    } 
    return "";
}
function updateInputOption($e,tablebody) {
	$("#"+tablebody).empty();
	$e.children().each(function(idx,elm) { 
		var $opt = $(elm);
		addNewOption(tablebody,$opt.attr("value"),$opt.text(),$opt.attr("control"));
	});
}
function validInputOption(optable) {
	var validate = true;
	var optvalues = [];
	$(optable).find("tr").each(function(eidx,etr){
		var $ins = $(etr).find("input");
		var $in1 = $ins.eq(0);
		var $in2 = $ins.eq(1);
		var val1 = $in1.val();
		var val2 = $in2.val();
		if($.trim(val1)!="" && $.trim(val2)!="") {
			optvalues.push({value:val1, text:val2});
		}
	});
	for(var i=0;i<optvalues.length;i++) {
		var iv = optvalues[i].value;
		for(var j=i+1;j<optvalues.length;j++) {
			var jv = optvalues[j].value;
			if(iv==jv) {
				validate = false; break;
			}
		}
		if(!validate) break;
	}
	return validate;
}
function updateInputField($e,$lbl,fn) {
	if("true"==$e.attr("calfield")) {
		updateFunctionField($e,$lbl,fn);
		return;
	}
	var eid = $e.attr("id");
	var ename = $e.attr("name");
	var elabel = $e.attr("label");
	var etype = $e.attr("type");
	var edatatype = $e.attr("datatype");
	var etag = $e.attr("tag");
	var evalue = $e.attr("value");
	var eshow = $e.attr("show");
	var efilter = $e.attr("filter");
	var elisting = $e.attr("listing");
	var econdition = $e.attr("condition");
	var ecurrent = $e.attr("current");
	var edecimal = $e.attr("decimal");
	var ereserved = $e.attr("reserved");
	var eplacement = $e.attr("placement");
	var econtrol = $e.attr("control");
	var estyle = $e.attr("style");
	var ewidth = $e.attr("width");
	var eheight = $e.attr("height");
	var elookup = $e.attr("lookup");
	var elookupurl = $e.attr("lookupurl");
	var elookupkeyfield = $e.attr("lookupkeyfield");
	var elookupvaluefield = $e.attr("lookupvaluefield");
	var elookupcategory = $e.attr("lookupcategory");
	var elookupgetting = $e.attr("gettinglookup");
	var elookuptype = $e.attr("lookuptype");
	var elookuptable = $e.attr("lookuptable");	
	var elookupfile = $e.attr("lookupfile");	
	var elookupsection = $e.attr("lookupsection");	
	var ecustomfile = $e.attr("customfile");
	var ecustomsection = $e.attr("customsection");
	var eheading = $e.attr("headerlabel");
	var edisplay = $e.attr("display");
	var edisplayplacement = $e.attr("displayplacement");
	var eprecision = $e.attr("precision");
	var erangelabel = $e.attr("rangelabel");
	var emask = $e.attr("mask");
	var elookupmultiple = $e.attr("multiplelookup");
	var elookupmapping = $e.attr("mappinglookup");
	var erequired = $e.attr("required");
	var ecompleted = $e.attr("completed");
	var ealerted = $e.attr("alerted");
	var emsg = $e.attr("msg");
	var ereadonly = $e.attr("readonly");
	var edisabled = $e.attr("disabled");
	var ekey = $e.attr("key");
	var ecols = $e.attr("columns");
	var epicture = $e.attr("picture");
	var elistalign = $e.attr("listalign");
	if(!ewidth) ewidth = "";
	if(!eheight) eheight = "";
	if(!elabel) elabel = "";
	if(!eid) eid = "";
	if(!ename) ename = "";
	if(!etype || etype=="") etype = "text";
	if(!edatatype || edatatype=="") edatatype = "string";
	if(!etag) etag = "";
	if(!econdition) econdition = "";
	if(!edecimal) edecimal = "";
	if(!eplacement) eplacement = "";
	if(!econtrol) econtrol = "";
	if(!estyle) estyle = "";
	if(!edisplay) edisplay = "";
	if(!edisplayplacement) edisplayplacement = "";
	if(!eprecision) eprecision = "";
	if(!erangelabel) erangelabel = "";
	if(!emask) emask = "";
	if(!elookupmultiple) elookupmultiple = "";
	if(!elookupmapping) elookupmapping = "";
	if(!erequired) erequired = "";
	if(!ecompleted) ecompleted = "";
	if(!ealerted) ealerted = "";
	if(!emsg) emsg = "";
	if(!ereadonly) ereadonly = "";
	if(!edisabled) edisabled = "";
	if(!ekey) ekey = "";
	if(!ecols) ecols = "";
	if(!epicture) epicture = "";
	if(!elistalign) elistalign = "";
	if($lbl) elabel = $lbl.text();
	$("#fieldid").val(eid);
	$("#fieldname").val(ename);
	$("#fieldname").attr("disabled","true"==ereserved);
	$("#fieldlabel").val(elabel);
	$("#fieldwidth").val(ewidth);
	$("#fieldheight").val(eheight);
	$("#fieldtype").val(etype);
	$("#fielddatatype").val(edatatype);
	$("#fielddatatype").attr("disabled","true"==ereserved);
	if(alreadyGenerated) $("#fielddatatype").attr("disabled",true);
	$("#fieldtag").val(etag);
	$("#fieldvalue").val(evalue);
	$("#fieldshowing").attr("checked","true"==eshow);
	$("#fieldfiltering").attr("checked","true"==efilter);
	$("#fieldlisting").attr("checked","true"==elisting);
	$("#fieldlistingalign").val(elistalign);
	$("#fieldheaderlabel").attr("checked","true"==eheading);
	$("#fieldcondition").val(econdition);
	$("#fieldkeyable").attr("checked","true"==ekey);
	$("#fieldcurrenting").attr("checked","true"==ecurrent);
	$("#fieldreadonly").attr("checked","true"==ereadonly || "readonly"==ereadonly);
	$("#fielddisabled").attr("checked","true"==edisabled || "disabled"==edisabled);
	$("#fielddecimal").val(edecimal);
	$("#fielddecimal").data("decimal",edecimal);
	$("#fieldplacement").val(eplacement);
	$("#fieldprecision").val(eprecision);
	$("#fielddecimalformat").val(emask);
	$("#fieldpicture").val(epicture);
	$("#fieldcontrol").val(econtrol);
	$("#fieldstyle").val(estyle);
	$("#lookupfield").attr("checked","true"==elookup);
	$("#lookupfieldmultiple").attr("checked","true"==elookupmultiple);
	$("#fieldrequired").attr("checked","true"==erequired || "required"==erequired);
	$("#fieldcompleted").attr("checked","true"==ecompleted);
	$("#fieldalerted").attr("checked","true"==ealerted);
	$("#fieldmsg").val(emsg);
	$("#fieldcols").val(ecols);	
	if(!elookupurl) elookupurl = "";
	if(!elookupkeyfield) elookupkeyfield = "";
	if(!elookupvaluefield) elookupvaluefield = "";
	if(!elookupcategory) elookupcategory = "";
	$("#lookupurl").val(elookupurl);
	$("#lookupkeyfield").val(elookupkeyfield);
	$("#lookupvaluefield").val(elookupvaluefield);
	$("#lookupcategory").val(elookupcategory);
	$("#lookupgetting").attr("checked","true"==elookupgetting);
	if(!elookuptype) elookuptype = "";
	if(!elookuptable) elookuptable = "";
	$("#lookuptype").val(elookuptype);
	$("#lookuptablename").val(elookuptable);
	$("#lookuptablekeyfield").val(elookupkeyfield);
	$("#lookuptablevaluefield").val(elookupvaluefield);
	if(!elookupfile) elookupfile = "";
	if(!elookupsection) elookupsection = "";
	if(!ecustomfile) ecustomfile = "";
	if(!ecustomsection) ecustomsection = "";
	if("table"==elookuptype) {
		$("#lookuptablelinker").trigger("click");
		$("#lookuptablekeyfield").val(elookupkeyfield);
		$("#lookuptablevaluefield").val(elookupvaluefield);
		$("#lookupkeyfield").val("");
		$("#lookupvaluefield").val("");
	} else if("ini"==elookuptype) {
		$("#lookupinifile").val(elookupfile);
		$("#lookupinisection").val(elookupsection);
		$("#lookupinilinker").trigger("click");
	} else if("custom"==elookuptype) {
		$("#lookupcustomfile").val(ecustomfile);
		$("#lookupcustomsection").val(ecustomsection);
		$("#lookupcustomlinker").trigger("click");
	} else {
		$("#lookupservicelinker").trigger("click");
		$("#lookuptablekeyfield").val("");
		$("#lookuptablevaluefield").val("");
	}
	$("#displayfield").attr("checked","true"==edisplay);
	$("#displayplacement").val(edisplayplacement);	
	$("#lookupfield").trigger("change");
	$("#fieldfiltering").trigger("change");
	$("#fieldlisting").trigger("change");
	$("#fieldtype").trigger("change");
	$("#fieldtag").trigger("change");
	changeDataType("#fielddatatype","#fieldtag","#fieldtype");
	updateInputOption($e,"optiontablebody");
	if($.trim(etag)!="") $("#fieldtag").val(etag);
	if($.trim(erangelabel)!="") $("#rangelabel").val(erangelabel);
	$("#fieldcondition").trigger("change");
	$("#lookupoptiontablebody").empty();
	if("true"==elookupmapping) {
		updateInputOption($e,"lookupoptiontablebody");
	}
	$("#lookupmapping").attr("checked","true"==elookupmapping);
	$("#lookupmapping").trigger("change");
	var oldid = eid;
	var oldname = ename;
	var $btn2 = $("#updatefieldbutton2");
	$btn2.unbind("click");
	$btn2.bind("click",function() { $("#updatefieldbutton").trigger("click"); });
	var $btn = $("#updatefieldbutton");
	$btn.unbind("click");
	$btn.bind("click",function() { 
		ename = $("#fieldname").val();
		if($.trim(ename)=="") {
			alertmsg("QS0105","Field Name can not be blank");
			return;
		}
		etype = $("#fieldtype").val();
		if($.trim(etype)=="") etype = "text";
		if(etype=="radio" || etype=="checkbox") { } else {
			if(!validUpdateDuplicateName(ename,oldname,$e.data("nodename"))) return;
		}
		if(etype=="select" || etype=="list") {
			if(!validInputOption("#optiontablebody")) {
				alertmsg("QS0112","Duplicate option value");
				return;
			}
		}
		eid = $("#fieldid").val();	
		if(!validUpdateDuplicateID(eid,oldid)) return;
		if($.trim(eid)!="") { $e.attr("id",eid); } else { $e.removeAttr("id"); }
		$e.attr("name",ename);
		if($lbl) $lbl.text($("#fieldlabel").val());
		else {
			elabel = $("#fieldlabel").val();
			if(elabel!="") { $e.attr("label",elabel); } else { $e.removeAttr("label"); }
		}
		eplacement = $("#fieldplacement").val();
		if($.trim(eplacement)!="") { $e.attr("placement",eplacement); } else { $e.removeAttr("placement"); }
		eprecision = $("#fieldprecision").val();
		if($.trim(eprecision)!="") { $e.attr("precision",eprecision); } else { $e.removeAttr("precision"); }
		ewidth = $("#fieldwidth").val();
		if($.trim(ewidth)!="") { $e.attr("width",ewidth); } else { $e.removeAttr("width"); }
		eheight = $("#fieldheight").val();
		if($.trim(eheight)!="") { $e.attr("height",eheight); } else { $e.removeAttr("height"); }
		$e.attr("type",etype);
		edatatype = $("#fielddatatype").val();
		if($.trim(edatatype)=="") edatatype = "string";
		$e.attr("datatype",edatatype);
		etag = $("#fieldtag").val();
		if($.trim(etag)!="") { $e.attr("tag",etag); } else { $e.removeAttr("tag"); }
		evalue = $("#fieldvalue").val();
		if($.trim(evalue)!="") { $e.attr("value",evalue); } else { $e.removeAttr("value"); }
		$e.attr("show",$("#fieldshowing").is(":checked"));
		$e.attr("filter",$("#fieldfiltering").is(":checked"));
		$e.attr("listing",$("#fieldlisting").is(":checked"));
		$e.attr("headerlabel",$("#fieldheaderlabel").is(":checked"));
		$e.attr("required",$("#fieldrequired").is(":checked"));
		$e.attr("completed",$("#fieldcompleted").is(":checked"));
		$e.attr("alerted",$("#fieldalerted").is(":checked"));
		elistalign = $("#fieldlistingalign").val();
		if($.trim(elistalign)!="") { $e.attr("listalign",elistalign); } else { $e.removeAttr("listalign"); }
		emsg = $("#fieldmsg").val();
		if($.trim(emsg)!="") { $e.attr("msg",emsg); } else { $e.removeAttr("msg"); }
		econdition = $("#fieldcondition").val();
		if($.trim(econdition)!="") { $e.attr("condition",econdition); } else { $e.removeAttr("condition"); }
		if("date"==etag || "year"==etag || "time"==etag || "month"==etag || "hour"==etag || "minute"==etag) {			
			$e.attr("current",$("#fieldcurrenting").is(":checked"));
		} else { $e.removeAttr("current"); }
		$e.attr("readonly",$("#fieldreadonly").is(":checked"));
		$e.attr("disabled",$("#fielddisabled").is(":checked"));
		$e.attr("key",$("#fieldkeyable").is(":checked"));
		edecimal = $("#fielddecimal").val();
		if($.trim(edecimal)=="") {
			$e.removeAttr("decimal");
		} else {
			if(etag=="money") {
				edecimal = removeComma(edecimal);
				$e.attr("decimal",edecimal);
			}
		}
		$("#fielddecimal").data("decimal",edecimal);
		epicture = $("#fieldpicture").val();
		if($.trim(epicture)!="") { $e.attr("picture",epicture); } else { $e.removeAttr("picture"); }
		econtrol = $("#fieldcontrol").val();
		if($.trim(econtrol)!="") { $e.attr("control",econtrol); } else { $e.removeAttr("control"); }
		estyle = $("#fieldstyle").val();
		if($.trim(estyle)!="") { $e.attr("style",estyle); 	} else { $e.removeAttr("style"); }
		ecols = $("#fieldcols").val();
		if($.trim(ecols)!="") { $e.attr("columns",ecols); 	} else { $e.removeAttr("columns"); }

		$e.attr("lookup",$("#lookupfield").is(":checked"));
		elookupurl = $("#lookupurl").val();
		if($.trim(elookupurl)!="") { $e.attr("lookupurl",elookupurl); } else { $e.removeAttr("lookupurl"); }
		elookupkeyfield = $("#lookupkeyfield").val();
		if($.trim(elookupkeyfield)!="") { $e.attr("lookupkeyfield",elookupkeyfield); } else { $e.removeAttr("lookupkeyfield"); }
		elookupvaluefield = $("#lookupvaluefield").val();
		if($.trim(elookupvaluefield)!="") { $e.attr("lookupvaluefield",elookupvaluefield); } else { $e.removeAttr("lookupvaluefield"); }
		elookupcategory = $("#lookupcategory").val();
		if($.trim(elookupcategory)!="") { $e.attr("lookupcategory",elookupcategory); } else { $e.removeAttr("lookupcategory"); }
		elookuptype = $("#lookuptype").val();
		if($.trim(elookuptype)!="") { $e.attr("lookuptype",elookuptype); } else { $e.removeAttr("lookuptype"); }
		elookuptable = $("#lookuptablename").val();
		if($.trim(elookuptable)!="") { $e.attr("lookuptable",elookuptable); } else { $e.removeAttr("lookuptable"); }
		if("table"==elookuptype) {
			elookupkeyfield = $("#lookuptablekeyfield").val();
			if($.trim(elookupkeyfield)!="") { $e.attr("lookupkeyfield",elookupkeyfield); } else { $e.removeAttr("lookupkeyfield"); }
			elookupvaluefield = $("#lookuptablevaluefield").val();
			if($.trim(elookupvaluefield)!="") { $e.attr("lookupvaluefield",elookupvaluefield); } else { $e.removeAttr("lookupvaluefield"); }
		} else if("ini"==elookuptype) {
			elookupfile = $("#lookupinifile").val();
			if($.trim(elookupfile)!="") { $e.attr("lookupfile",elookupfile); } else { $e.removeAttr("lookupfile"); }
			elookupsection = $("#lookupinisection").val();
			if($.trim(elookupsection)!="") { $e.attr("lookupsection",elookupsection); } else { $e.removeAttr("lookupsection"); }
		} else if("custom"==elookuptype) {
			ecustomfile = $("#lookupcustomfile").val();
			if($.trim(ecustomfile)!="") { $e.attr("customfile",ecustomfile); } else { $e.removeAttr("customfile"); }
			ecustomsection = $("#lookupcustomsection").val();
			if($.trim(ecustomsection)!="") { $e.attr("customsection",ecustomsection); } else { $e.removeAttr("customsection"); }
		}
		$e.attr("display",$("#displayfield").is(":checked"));
		edisplayplacement = $("#displayplacement").val();
		if($.trim(edisplayplacement)!="") { $e.attr("displayplacement",edisplayplacement); } else { $e.removeAttr("displayplacement"); }
		erangelabel = $("#rangelabel").val();
		if($.trim(erangelabel)!="") { $e.attr("rangelabel",erangelabel); } else { $e.removeAttr("rangelabel"); }
		emask = $("#fielddecimalformat").val();
		if($.trim(emask)!="") { $e.attr("mask",emask); } else { $e.removeAttr("mask"); }
		$e.attr("multiplelookup",$("#lookupfieldmultiple").is(":checked"));
		$e.attr("gettinglookup",$("#lookupgetting").is(":checked"));
		$e.attr("mappinglookup",$("#lookupmapping").is(":checked"));

		$e.empty();
		var tbodyid = "optiontablebody";
		if($("#lookupmapping").is(":checked")) tbodyid = "lookupoptiontablebody";
		$("#"+tbodyid).find("tr").each(function(eidx,etr){
			var $ins = $(etr).find("input");
			var $in1 = $ins.eq(0);
			var $in2 = $ins.eq(1);
			var $in3 = $ins.eq(2);
			var val1 = $in1.val();
			var val2 = $in2.val();
			var val3 = $in3.val();
			if($.trim(val1)!="" && $.trim(val2)!="") {
				var $op = $("<option></option>");
				$op.attr("value",val1);
				$op.text(val2);
				$op.attr("control",val3);
				$e.append($op);
			}
		});
		if(fn) fn();
		alertmsg("QS0104","Update success");
	});
}
function filterFieldClick(src,list) {
	var elm = $(src).data("filterdata");
	var $e = $(elm);
	$(".control-property").hide();
	$("#propertyheaderlabel").html("Filter Field");
	$("#fieldcontrollayer").hide();
	$("#readonly_layer").hide();
	$("#columnscontrollayer").show();
	updateInputField($e);
	if("true"==$e.attr("calfield")) {
		$("#propertyheaderlabel").html("Function Field");
		$("#function_layer_control").show();
	} else {
		$("#field_layer_control").show();
	}
	$("#display_layer_control").show();
}
function inputFieldClick(src,list) {
	$(".control-property").hide();
	$("#readonly_layer").show();
	$("#propertyheaderlabel").html("");
	//$("#columnscontrollayer").show(); 
	var $cli = $(src);
	var $col = $cli.data("coldata");
	var $ce = $col.children().eq(0);	
	$ce.data("nodename",$cli.data("nodename"));
	//console.log("field click > coldata = "+$col+" : is input="+$cli.is(".input-class"));
	if($cli.is(".input-class")) {
		var $lbl = null;
		var lbltext = $ce.attr("label");
		if($col.children().size()>1) {
			$lbl = $("label",$col).eq(0);
			lbltext = $lbl.text();
		}		
		$("#propertyheaderlabel").html("Input Field");
		$("#fieldcontrollayer").show();
		var colalign = $col.attr("align");
		var colstyle = $col.attr("style");
		var colspan = $col.attr("colspan");
		var rowspan = $col.attr("rowspan");
		var columns = $col.attr("columns");
		if(!colalign) colalign = "";
		if(!colstyle) colstyle = "";
		if(!colspan) colspan = "";
		if(!rowspan) rowspan = "";
		if(!columns) columns = "";
		$("#columnalign").val(colalign);
		$("#fieldcolumnstyle").val(colstyle);
		$("#fieldcolspan").val(colspan);
		$("#fieldrowspan").val(rowspan);
		$("#fieldcolumns").val(columns);
		//console.log("label : "+lbltext);
		updateInputField($ce,$lbl,function() {
			colalign = $("#columnalign").val();
			colstyle = $("#fieldcolumnstyle").val();
			colspan = $("#fieldcolspan").val();
			rowspan = $("#fieldrowspan").val();
			columns = $("#fieldcolumns").val();
			if(colalign!="") { $col.attr("align",colalign); } else { $col.removeAttr("align"); }
			if($.trim(colstyle)!="") { $col.attr("style",colstyle); } else { $col.removeAttr("style"); }
			if($.trim(colspan)!="") { $col.attr("colspan",colspan); } else { 	$col.removeAttr("colspan"); }
			if($.trim(rowspan)!="") { $col.attr("rowspan",rowspan); } else { $col.removeAttr("rowspan"); }
			if($.trim(columns)!="") { $col.attr("columns",columns); } else { $col.removeAttr("columns"); }
			var clabel = $("#fieldlabel").val()+"("+$("#fieldname").val()+")";
			var $span = $cli.find("span").eq(0);
			if("true"==$ce.attr("calfield")) { 
				clabel = $("#functionfieldlabel").val()+"("+$("#functionfieldname").val()+")";
			}
			$span.html(clabel);
		});
		if("true"==$ce.attr("calfield")) {
			$("#propertyheaderlabel").html("Function Field");
			$("#function_layer_control").show();
		} else {
			$("#field_layer_control").show();
		}
		$("#validate_layer_control").show();
		$("#lookup_layer_control").show();
		$("#column_layer_control").show();
	} else if($cli.is(".label-class")) {
		$("#propertyheaderlabel").html("Label");
		var lid = $ce.attr("id");
		if(!lid) lid = "";
		var oldid = lid;
		var label = $ce.text();
		$("#labelid").val(lid);
		$("#labellabel").val(label);
		var lstyle = $ce.attr("style");
		if(!lstyle) lstyle = "";
		$("#labelstyle").val(lstyle);
		var colalign = $col.attr("align");
		var colstyle = $col.attr("style");
		var colspan = $col.attr("colspan");
		var rowspan = $col.attr("rowspan");
		var columns = $col.attr("columns");
		if(!colalign) colalign = "";
		if(!colstyle) colstyle = "";
		if(!colspan) colspan = "";
		if(!rowspan) rowspan = "";
		if(!columns) columns = "";
		$("#columnalign").val(colalign);
		$("#fieldcolumnstyle").val(colstyle);
		$("#fieldcolspan").val(colspan);
		$("#fieldrowspan").val(rowspan);
		$("#fieldcolumns").val(columns);
		var btn = $("#updatelabelbutton");
		btn.unbind("click");
		btn.bind("click",function() {
				lid = $("#labelid").val();
				if(!validUpdateDuplicateID(lid,oldid)) return;
				if($.trim(lid)!="") { $ce.attr("id",lid); } else { $ce.removeAttr("id"); 	}
				lstyle = $("#labelstyle").val();
				if($.trim(lstyle)!="") { $ce.attr("style",lstyle); } else { $ce.removeAttr("style"); }
				$ce.text($("#labellabel").val());
				$("div",$cli).html($("#labellabel").val());
				colalign = $("#columnalign").val();
				colstyle = $("#fieldcolumnstyle").val();
				colspan = $("#fieldcolspan").val();
				rowspan = $("#fieldrowspan").val();
				columns = $("#fieldcolumns").val();
				if(colalign!="") { $col.attr("align",colalign); } else { $col.removeAttr("align"); }
				if($.trim(colstyle)!="") { $col.attr("style",colstyle); } else { $col.removeAttr("style"); }
				if($.trim(colspan)!="") { $col.attr("colspan",colspan); } else { 	$col.removeAttr("colspan"); }
				if($.trim(rowspan)!="") { $col.attr("rowspan",rowspan); } else { $col.removeAttr("rowspan"); }
				if($.trim(columns)!="") { $col.attr("columns",columns); } else { $col.removeAttr("columns"); }
				alertmsg("QS0104","Update success");
		});
		$("#label_layer_control").show();
		$("#column_layer_control").show();
	} else if($cli.is(".image-class")) {
		$("#propertyheaderlabel").html("Image");
		var colalign = $col.attr("align");
		var colstyle = $col.attr("style");
		var colspan = $col.attr("colspan");
		var rowspan = $col.attr("rowspan");
		var columns = $col.attr("columns");
		if(!colalign) colalign = "";
		if(!colstyle) colstyle = "";
		if(!colspan) colspan = "";
		if(!rowspan) rowspan = "";
		if(!columns) columns = "";
		$("#columnalign").val(colalign);
		$("#fieldcolumnstyle").val(colstyle);
		$("#fieldcolspan").val(colspan);
		$("#fieldrowspan").val(rowspan);
		$("#fieldcolumns").val(columns);
		var imgsrc = $ce.attr("src");	
		$("#fieldimagedisplay").attr("src",imgsrc);
		var imgw = $ce.attr("width");
		var imgh = $ce.attr("height");
		var imgstyle = $ce.attr("style");
		if(!imgw) imgw = "";
		if(!imgh) imgh = "";
		if(!imgstyle) imgstyle = "";
		$("#imagewidth").val(imgw);
		$("#imageheight").val(imgh);
		$("#imagestyle").val(imgstyle);
		if($.trim($("#fieldimage").val())!="") {
			var $input = $("<input type='file' class='form-control input-md' id='fieldimage' name='fieldimage'></input>");
			$("#imageinputlayer").empty().append($input);
		}
		var btn = $("#updateimagebutton");
		btn.unbind("click");
		btn.bind("click",function() { 
				var w = $("#imagewidth").val();
				var h = $("#imageheight").val();
				if(($.trim(w)!="") && ($.trim(h)!="")) {
					$ce.attr("width",w);
					$ce.attr("height",h);
				}
				imgstyle = $("#imagestyle").val();
				if($.trim(imgstyle)!="") { $ce.attr("style",imgstyle); } else { $ce.removeAttr("style"); }
				colalign = $("#columnalign").val();
				colstyle = $("#fieldcolumnstyle").val();
				colspan = $("#fieldcolspan").val();
				rowspan = $("#fieldrowspan").val();
				columns = $("#fieldcolumns").val();
				if(colalign!="") { $col.attr("align",colalign); } else { $col.removeAttr("align"); }
				if($.trim(colstyle)!="") { $col.attr("style",colstyle); } else { $col.removeAttr("style"); }
				if($.trim(colspan)!="") { $col.attr("colspan",colspan); } else { 	$col.removeAttr("colspan"); }
				if($.trim(rowspan)!="") { $col.attr("rowspan",rowspan); } else { $col.removeAttr("rowspan"); }
				if($.trim(columns)!="") { $col.attr("columns",columns); } else { $col.removeAttr("columns"); }
				if($.trim($("#fieldimage").val())!="") {
					uploadNewImage(imageform,function(data) {
						var imgfile = "";
						var json = $.parseJSON($.trim(data));
						if(json["filename"]) {
							imgfile = json["filename"];
							imgsrc = "../resources/apps/"+imgfile;
							$ce.attr("src",imgsrc);
							$("#fieldimagedisplay").attr("src",imgsrc);
						}
						if(imgfile!="") $cli.find("span").eq(0).html(imgfile);
						alertmsg("QS0104","Update success");
						var $input = $("<input type='file' class='form-control input-md' id='fieldimage' name='fieldimage'></input>");
						$("#imageinputlayer").empty().append($input);
					});
				} else {
					alertmsg("QS0104","Update success");
				}
		});
		$("#image_layer_control").show();
		$("#column_layer_control").show();
	} 
}
function addNewPart() {
	$("#newpartid").val("");
	$("#newpartlabel").val("");
	$("#newparttitle").val("");
	$("#newpartstyle").val("");
	partdialog.dialog("option", "buttons", [{
		text: "Cancel",
			click: function() { $(this).dialog("close"); }
		}, {
			text: " OK ",
			click: function() {
				var pid = $("#newpartid").val();
				if(!validateDuplicateID(pid)) return;
				var label = $("#newpartlabel").val();
				var title = $("#newparttitle").val();
				if($.trim(label)!="") {
					createNewPart(pid,label,title,$("#newpartstyle").val());
					partdialog.dialog("close"); 
				}
			}
    }]);
	partdialog.dialog("open");
}
function addNewSubPart() {
	$("#newsubpartid").val("");
	$("#newsubpartlabel").val("");
	$("#newsubpartstyle").val("");
	subpartdialog.dialog("option", "buttons", [{
		text: "Cancel",
			click: function() { $(this).dialog("close"); }
		}, {
			text: " OK ",
			click: function() {
				var spid = $("#newsubpartid").val();
				if(!validateDuplicateID(spid)) return;
				var label = $("#newsubpartlabel").val();
				if($.trim(label)!="") {
					createNewSubPart(spid,label,$("#newsubpartstyle").val());
					subpartdialog.dialog("close"); 
				}
			}
    }]);
	subpartdialog.dialog("open");
}
function addNewHorizon() {
	createNewHorizon("Horizontal Line");
}
function addNewLabel() {
	addNewLabeling($("#sublistcolumn"));
}
function addNewKeyLabel(){
	addNewLabeling($("#keysublistcolumn"));
}
function addNewFilterLabel() {
	addNewLabeling($("#filtersublistcolumn"));
}
function addNewLabeling($subcol) {
	if(!$subcol) $subcol = $("#sublistcolumn");
	if(!ensureActiveRow($subcol)) return;
	$("#newlabelid").val("");
	$("#newlabellabel").val("");
	$("#newlabelstyle").val("");
	$("#newlabelplacement").val("");
	labeldialog.dialog("option", "buttons", [{
		text: "Cancel",
			click: function() { $(this).dialog("close"); }
		}, {
			text: " OK ",
			click: function() {
				var lid = $("#newlabelid").val();
				if(!validateDuplicateID(lid)) return;
				var label = $("#newlabellabel").val();
				var labelplacer = $("#newlabelplacement").val();
				createNewLabel($subcol,lid,label,labelplacer);
				labeldialog.dialog("close"); 
			}
    }]);
	labeldialog.dialog("open");
}
function createNewLabel($subcol,lid,label,labelplacer) {
	columnCounter++;
	if(!$subcol) $subcol = $("#sublistcolumn");
	var $subdiv = $("div.active-state",$subcol).eq(0);
	var $sit = $("ul",$subdiv).eq(0);
	var $row = $subdiv.data("rowdata");
	var $col = $("<col></col>");
	var $label = $("<label></label>");
	if($.trim(lid)!="") $label.attr("id",lid);
	$label.append(label);
	$col.append($label);
	$row.append($col);
	if("prefix"==labelplacer) $col.attr("align","left");
	else if("suffix"==labelplacer) $col.attr("align","right");
	$col.attr("idx",columnCounter);

	var $cdiv = $("<div></div>");
	$cdiv.addClass("collayer-class");
	var $span = $("<span></span>");
	$span.html(label);
	$cdiv.append($span);
	var $cli = $("<li></li>");
	$cli.addClass("ui-state-default label-class");
	$cli.attr("idx",columnCounter);
	$cli.data("rowdata",$row);
	$cli.data("coldata",$col);
	$cli.append($cdiv);
	$sit.append($cli);
	bindingSubListItem($cli,$sit,$subcol,inputFieldClick);
	$cli.trigger("click");
	return true;
}
function addNewInputOption($input,tablebody) {
	$input.empty();
	$("#"+tablebody).find("tr").each(function(eidx,etr){
		var $ins = $(etr).find("input");
		var $in1 = $ins.eq(0);
		var $in2 = $ins.eq(1);
		var $in3 = $ins.eq(2);
		var val1 = $in1.val();
		var val2 = $in2.val();
		var val3 = $in3.val();
		if($.trim(val1)!="" && $.trim(val2)!="") {
			var $op = $("<option></option>");
			$op.attr("value",val1);
			$op.text(val2);
			if($.trim(val3)!="") $op.attr("control",val3);
			$input.append($op);
		}
	});
}
function createNewInput($subcol,nodename) {
	columnCounter++;
	if(!$subcol) $subcol = $("#sublistcolumn");
	var $subdiv = $("div.active-state",$subcol).eq(0);
	var $sit = $("ul",$subdiv).eq(0);
	var $row = $subdiv.data("rowdata");
	var $col = $("<col></col>");
	var $input = $("<input></input>");
	var eid = $("#newfieldid").val();
	var ename = $("#newfieldname").val();
	var elabel = $("#newfieldlabel").val();
	var etype = $("#newfieldtype").val();
	var edatatype = $("#newfielddatatype").val();
	var etag = $("#newfieldtag").val();
	var evalue = $("#newfieldvalue").val();
	var eplacement = $("#newfieldplacement").val();
	var epicture = $("#newfieldpicture").val();
	var econtrol = $("#newfieldcontrol").val();
	var estyle = $("#newfieldstyle").val();
	var ewidth = $("#newfieldwidth").val();
	var eheight = $("#newfieldheight").val();	
	var emsg = $("#newfieldmsg").val();
	if($.trim(eid)!="") $input.attr("id",eid);
	if($.trim(ename)!="") $input.attr("name",ename);
	if($.trim(estyle)!="") $input.attr("style",estyle);
	if(elabel!="") {
		$input.attr("label",elabel);
		if(eplacement!="") {
			$input.attr("placement",eplacement);
		} else {
			$input.removeAttr("placement");
		}
	}
	if($.trim(ewidth)!="") $input.attr("width",ewidth);
	if($.trim(eheight)!="") $input.attr("height",eheight);
	if($.trim(etype)!="") $input.attr("type",etype);
	if($.trim(edatatype)!="") $input.attr("datatype",edatatype);
	if($.trim(etag)!="") $input.attr("tag",etag);
	if($.trim(evalue)!="") $input.attr("value",evalue);
	if($("#newfieldshowing").is(":checked")) {
		$input.attr("show",true);
	} 
	if($("#newfieldlisting").is(":checked")) {
		$input.attr("listing",true);
		var elistalign = $("#newfieldlistingalign").val();
		if($.trim(elistalign)!="") $input.attr("listalign",elistalign);
		else $input.removeAttr("listalign");
	} 
	if($("#newfieldheaderlabel").is(":checked")) {
		$input.attr("headerlabel",true);
	} 
	var efilter = $("#newfieldfiltering").is(":checked");
	if(efilter) {
		$input.attr("filter",efilter);
		var econdition = $("#newfieldcondition").val();
		if($.trim(econdition)!="") $input.attr("condition",econdition);
		else $input.removeAttr("condition");
	} 
	var eprecision = $("#newfieldprecision").val();
	if($.trim(eprecision)!="") $input.attr("precision",eprecision);
	var edecimal = $("#newfielddecimal").val();
	if($.trim(edecimal)!="") $input.attr("decimal",edecimal);
	var emask = $("#newfielddecimalformat").val();
	if($.trim(emask)!="") $input.attr("mask",emask);
	if(!$("#newfieldcurrenting").is(":disabled")) {
		$input.attr("current",$("#newfieldcurrenting").is(":checked"));
	} 
	if($.trim(epicture)!="") $input.attr("picture",epicture);
	if($.trim(econtrol)!="") $input.attr("control",econtrol);		
	$input.attr("readonly",$("#newfieldreadonly").is(":checked"));
	$input.attr("disabled",$("#newfielddisabled").is(":checked"));
	$input.attr("required",$("#newfieldrequired").is(":checked"));
	$input.attr("completed",$("#newfieldcompleted").is(":checked"));
	$input.attr("alerted",$("#newfieldalerted").is(":checked"));
	$input.attr("key",$("#newfieldkeyable").is(":checked"));
	if($.trim(emsg)!="") $input.attr("msg",emsg);
	addNewInputOption($input,"newoptiontablebody");
	var elookup = $("#newlookupfield").is(":checked");
	if(elookup) {
		$input.attr("lookup",elookup);
		var elookupurl = $("#newlookupurl").val();
		if($.trim(elookupurl)!="") $input.attr("lookupurl",elookupurl);
		else $input.removeAttr("lookupurl");
		var elookupkeyfield = $("#newlookupkeyfield").val();
		if($.trim(elookupkeyfield)!="") $input.attr("lookupkeyfield",elookupkeyfield);
		else $input.removeAttr("lookupkeyfield");
		var elookupvaluefield = $("#newlookupvaluefield").val();
		if($.trim(elookupvaluefield)!="") $input.attr("lookupvaluefield",elookupvaluefield);
		else $input.removeAttr("lookupvaluefield");
		if($("#newlookupgetting").is(":checked")) {
			$input.attr("gettinglookup",true);
		}
		var elookupcategory = $("#newlookupcategory").val();
		if($.trim(elookupcategory)!="") $input.attr("lookupcategory",elookupcategory);
		else $input.removeAttr("lookupcategory");
		var elookuptype = $("#newlookuptype").val();
		var elookuptable= $("#newlookuptablename").val();
		if($.trim(elookuptype)!="") $input.attr("lookuptype",elookuptype);
		else $input.removeAttr("lookuptype");
		if($.trim(elookuptable)!="") $input.attr("lookuptable",elookuptable);
		else $input.removeAttr("lookuptable");
		if("table"==elookuptype) {
			var elookupkeyfield = $("#newlookuptablekeyfield").val();
			if($.trim(elookupkeyfield)!="") $input.attr("lookupkeyfield",elookupkeyfield);
			else $input.removeAttr("lookupkeyfield");
			var elookupvaluefield = $("#newlookuptablevaluefield").val();
			if($.trim(elookupvaluefield)!="") $input.attr("lookupvaluefield",elookupvaluefield);
			else $input.removeAttr("lookupvaluefield");
		} else if("ini"==elookuptype) {
			var elookupfile = $("#newlookupinifile").val();
			if($.trim(elookupfile)!="") $input.attr("lookupfile",elookupfile);
			else $input.removeAttr("lookupfile");
			var elookupsection = $("#newlookupinisection").val();
			if($.trim(elookupsection)!="") $input.attr("lookupsection",elookupsection);
			else $input.removeAttr("lookupsection");
		} else if("custom"==elookuptype) {
			var elookupfile = $("#newlookupcustomfile").val();
			if($.trim(elookupfile)!="") $input.attr("customfile",elookupfile);
			else $input.removeAttr("customfile");
			var elookupsection = $("#newlookupcustomsection").val();
			if($.trim(elookupsection)!="") $input.attr("customsection",elookupsection);
			else $input.removeAttr("customsection");
		}
		$input.attr("multiplelookup",$("#newlookupfieldmultiple").is(":checked"));
		if($("#newlookupmapping").is(":checked")) {
			$input.attr("mappinglookup",true);
			addNewInputOption($input,"newlookupoptiontablebody");
		}
	} 
	var ealign = $("#newcolumnalign").val();
	var ecolstyle = $("#newfieldcolumnstyle").val();
	var ecolspan = $("#newfieldcolspan").val();
	var erowspan = $("#newfieldrowspan").val();
	var ecolumns = $("#newfieldcolumns").val();
	if($.trim(ealign)!="") $col.attr("align",ealign);
	if($.trim(ecolstyle)!="") $col.attr("style",ecolstyle);
	if($.trim(ecolspan)!="") $col.attr("colspan",ecolspan);
	if($.trim(erowspan)!="") $col.attr("rowspan",erowspan);
	if($.trim(ecolumns)!="") $col.attr("columns",ecolumns);
	$col.append($input);
	$row.append($col);
	$col.attr("idx",columnCounter);

	var $cdiv = $("<div></div>");
	$cdiv.addClass("collayer-class");
	//console.log("create new input = "+etype);
	if("radio"==etype) {
		$cdiv.append($("<i class='fa fa-dot-circle-o' aria-hidden='true'></i>"));
		$cdiv.append("&#160;");
	} else if("checkbox"==etype) {
		$cdiv.append($("<i class='fa fa-check-square-o' aria-hidden='true'></i>"));
		$cdiv.append("&#160;");
	} else {
		$cdiv.append($("<i class='fa fa-pencil-square-o' aria-hidden='true'></i>"));
		$cdiv.append("&#160;");
	}
	var $span = $("<span></span>");
	$span.html(elabel+"("+ename+")");
	$cdiv.append($span);
	var $cli = $("<li></li>");
	$cli.addClass("ui-state-default input-class");
	$cli.attr("idx",columnCounter);
	$cli.data("rowdata",$row);
	$cli.data("coldata",$col);
	$cli.append($cdiv);
	$sit.append($cli);
	if(nodename) $cli.data("nodename",nodename);
	bindingSubListItem($cli,$sit,$subcol,inputFieldClick);
	$cli.trigger("click");
	return true;
}
function addNewRow() {
	createNewRow("table",$("#sublistcolumn"),$("#sectionlisting"));
}
function addNewFilterRow() {
	createNewRow("table",$("#filtersublistcolumn"),$("#filtersectionlisting"));
}
function addNewKeyRow() {
	createNewRow("table",$("#keysublistcolumn"),$("#keysectionlisting"));
}
function createNewRow(nodename,$subcol,$listing) {
	if(!$listing) $listing = $("#sectionlisting");
	if(!$subcol) $subcol = $("#sublistcolumn");
	var $li = $subcol.data("li");
	var $sec = $subcol.data("sectiondata");
	var secIdx = $sec.attr("idx");
	var $table = $(nodename,$sec).eq(0);
	var $row = $("<row></row>");
	$table.append($row);
	var rowIndex = $("row",$table).size()+1;
	//console.log("add new row index="+rowIndex);
	//alert($li.html());
	var $subdiv = $("<div></div>");
	$subdiv.attr("id","sublistinglayer"+rowIndex);
	$subdiv.addClass("portal-area sublayer-class");
	$subdiv.data("rowdata",$row);
	$subdiv.attr("idx",rowIndex);
	$subdiv.attr("sidx",secIdx);
	bindingRowListLayer($subdiv,$subcol,$row,function(src,list,rowElem) {
		console.log("current row = "+rowElem);
	});
	$subcol.append($subdiv);
	var $sit = $("<ul></ul>");
	$sit.attr("id","subitemlisting"+rowIndex);
	$sit.addClass("sublisting-class");
	$subdiv.append($sit);
	$sit.sortable({
		placeholder: "ui-state-active",
		start : function(evt,ui) {
			var $it = $(ui.item);
			startIndex = $it.index();
		},
		stop : function(evt,ui) {
			var $it = $(ui.item);
			var index = $it.index();
			var idx = $it.attr("idx");
			//var sidx = $it.attr("sidx");
			//console.log("start Idx = "+startIndex+", index="+index+", idx="+idx+", row="+rowIndex);
			if(startIndex==index) return;
			var rowNode = $it.data("rowdata");
			if(!rowNode) return;
			var pnode = rowNode; 
			//console.log("stop item : "+$it.text()+" : index="+index+" idx="+idx);
			var source = $("col[idx="+idx+"]",pnode);
			var dest = $("col",pnode).eq(index);
			if(startIndex>index) {
				dest.before(source);
			} else {
				dest.after(source);
			}
			updateSectionToScreen($li,$listing,$sec,$subcol);
			var $div = $("#sublistinglayer"+(rowIndex-2));
			$div.trigger("click");
			var $ul = $("ul",$div);
			$("li",$ul).eq(0).trigger("click");
		}
	});
	$subdiv.trigger("click");
}
function ensureActiveRow($subcol) {
	if(!$subcol) $subcol = $("#sublistcolumn");
	var sz = $("div.active-state",$subcol).size();
	if(sz<=0) {
		alertmsg("QS0109","Add new row or select active row first!");
		return false;
	}
	return true;
}
function addNewImage() {
	if(!ensureActiveRow($("#sublistcolumn"))) return;
	if($.trim($("#newfieldimage").val())!="") {
		var $input = $("<input type='file' class='form-control input-md' id='newfieldimage' name='fieldimage'></input>");
		$("#newimageinputlayer").empty().append($input);
	}
	$("#newimagewidth").val("400");
	$("#newimageheight").val("400");
	$("#newimgcolumnalign").val("");
	$("#newimgcolumnstyle").val("");
	$("#newimgcolspan").val("");
	$("#newimgrowspan").val("");
	$("#newimgcolumns").val("");
	$("#newimagestyle").val("");
	imagedialog.dialog("option", "buttons", [{
		text: "Cancel",
			click: function() { $(this).dialog("close"); }
		}, {
			text: " OK ",
			click: function() {
				var colalign = $("#newimgcolumnalign").val();
				var colstyle = $("#newimgcolumnstyle").val();
				var colspan = $("#newimgcolspan").val();
				var rowspan = $("#newimgrowspan").val();
				var imgstyle = $("#newimagestyle").val();
				var columns = $("#newimgcolumns").val();
				var w = $("#newimagewidth").val();
				var h = $("#newimageheight").val();
				if(($.trim($("#newfieldimage").val())!="") && ($.trim(w)!="") && ($.trim(h)!="")) {
					uploadNewImage(newimageform,function(data) {
						var json = $.parseJSON($.trim(data));
						if(json["filename"]) {
							createNewImage("../images/apps/"+json["filename"],w,h,colalign,colstyle,colspan,rowspan,imgstyle,columns);
						}
						imagedialog.dialog("close"); 
					});
				}
			}
    }]);
	imagedialog.dialog("open");
}
function createNewImage(imgsrc,w,h,colalign,colstyle,colspan,rowspan,imgstyle,columns) {
	columnCounter++;
	var $subcol = $("#sublistcolumn");
	var $subdiv = $("div.active-state",$subcol).eq(0);
	var $sit = $("ul",$subdiv).eq(0);
	var $row = $subdiv.data("rowdata");
	var $col = $("<col></col>");
	var $img = $("<img></img>");
	$img.attr("src",imgsrc);
	$img.attr("width",w);
	$img.attr("height",h);
	if($.trim(colalign)!="") $col.attr("align",colalign);
	if($.trim(colstyle)!="") $col.attr("style",colstyle);
	if($.trim(colspan)!="") $col.attr("colspan",colspan);
	if($.trim(rowspan)!="") $col.attr("rowspan",rowspan);
	if($.trim(columns)!="") $col.attr("columns",columns);
	if($.trim(imgstyle)!="") $img.attr("style",imgstyle);
	$col.append($img);
	$row.append($col);
	$col.attr("idx",columnCounter);

	var idx = imgsrc.lastIndexOf("/");
	if(idx>0) imgsrc = imgsrc.substring(idx+1);
	var $cdiv = $("<div></div>");
	$cdiv.addClass("collayer-class");
	var $span = $("<span></span>");
	$span.html(imgsrc);
	$cdiv.append($span);
	var $cli = $("<li></li>");
	$cli.addClass("ui-state-default image-class");
	$cli.attr("idx",columnCounter);
	$cli.data("rowdata",$row);
	$cli.data("coldata",$col);
	$cli.append($cdiv);
	$sit.append($cli);
	bindingSubListItem($cli,$sit,$("#sublistcolumn"),inputFieldClick);
	$cli.trigger("click");
	return true;	
}
function uploadNewImage(aform,fn) {
	//if(!confirm($(aform).serialize())) return false;
	startWaiting();	
	var fd = new FormData(aform);
	var xhr = jQuery.ajax({
		url: "sfte004_ic.jsp",
		type: "POST",
		dataType: "html",
		data: fd,
		enctype: "multipart/form-data",
		processData: false, 
		contentType: false, 
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown); 
		},
		success: function(data,status,transport){ 
			stopWaiting();
			if(fn) fn(data);
		}
	});
}
function getElementID(id) {
	if($.trim(id)!="") {
		var isz = $("[id="+id+"]",prgxmldoc).size();
		//console.log("get id ["+id+"] = "+isz);	
		return isz;
	}
	return 0;
}
function validateDuplicateID(id) {	
	var sz = getElementID(id);
	if(sz>0) {
		alertmsg("QS0107","Duplicate ID");
		return false;
	}
	return true;
}
function validUpdateDuplicateID(id,oldid) {	
	//console.log("id = ["+id+"] , old id = ["+oldid+"]");
	if($.trim(id)!=$.trim(oldid)) {
		var sz = getElementID(id);
		if(sz>=1) {
			alertmsg("QS0107","Duplicate ID");
			return false;
		}
	}
	return true;
}
function getElementName(name,nodename) {
	if($.trim(name)!="") {
		if(!nodename) nodename = "screen";
		var node = $(nodename,prgxmldoc).eq(0);
		var isz = $("[name="+name+"]",node).size();
		//console.log("get name ["+name+"] = "+isz);	
		return isz;
	}
	return 0;
}
function validateDuplicateName(name,nodename) {	
	var sz = getElementName(name,nodename);
	if(sz>0) {
		alertmsg("QS0106","Duplicate Name");
		return false;
	}
	return true;
}
function validUpdateDuplicateName(name,oldname,nodename) {	
	//console.log("name = ["+name+"] , old name = ["+oldname+"] node name =["+nodename+"]");
	if($.trim(name)!=$.trim(oldname)) {
		var sz = getElementName(name,nodename);
		if(sz>=1) {
			alertmsg("QS0106","Duplicate Name");
			return false;
		}
	}
	return true;
}
function toggleReservedAndFilter() {
	var $src = $("#togglefilter");
	if($src.is(".left")) {
		$src.removeClass("left").addClass("right");
		$("#filtercolumn1").hide();
		$("#filtercolumn2").hide();
		$("#programcolumn1").attr("colspan",2);
		$("#programcolumn2").attr("colspan",2);
		$src.find(".fa").removeClass("fa-chevron-circle-left").addClass("fa-chevron-circle-right");
	} else {
		$src.removeClass("right").addClass("left");
		$("#filtercolumn1").show();
		$("#filtercolumn2").show();
		$("#programcolumn1").attr("colspan",1);
		$("#programcolumn2").attr("colspan",1);
		$src.find(".fa").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-left");
	}	
}
function insertNewOption(bodyid,val1,val2,val3) {
	addNewOption(bodyid,val1,val2,val3,-1);
}
function addNewOption(bodyid,val1,val2,val3,rowIndex) {
	var $tbody = $("#"+bodyid);
	var $tr = $("<tr></tr>");
	var $td1 = $("<td></td>");
	var $td2 = $("<td></td>");
	var $td3 = $("<td></td>");
	var $td4 = $("<td></td>").addClass("column-action");
	var $in1 = $("<input></input>");
	if(val1) $in1.val(val1);
	var $in2 = $("<input></input>");
	if(val2) $in2.val(val2);
	var $in3 = $("<input></input>");
	if(val3) $in3.val(val3);
	$in1.addClass("option-value form-control");
	$in2.addClass("option-text form-control");
	$in3.addClass("option-text form-control");
	$td1.addClass("column-value");
	$td2.addClass("column-text");
	$td3.addClass("column-control");
	$td1.append($in1);
	$td2.append($in2);
	$td3.append($in3);
	var $dic = $("<i class='fa fa-minus-circle fa-class' aria-hidden='true'></i>");
	var dlink = $("<a></a").attr("href","javascript:void(0)").attr("style","padding-right:3px;").append($dic).click(function() { 
		$tr.remove();
	}).attr("title","Delete Option Value");	
	var $aic = $("<i class='fa fa-plus-circle fa-class' aria-hidden='true'></i>");
	var alink = $("<a></a").attr("href","javascript:void(0)").append($aic).click(function() { 
		addNewOption(bodyid,null,null,null,$tr.index());
	}).attr("title","Add Option Value");
	var $mic1 = $("<i class='fa fa-caret-up'></i>");
	var $mlink1 = $("<a></a").attr("href","javascript:void(0)").addClass("btn").append($mic1).click(function() { 
		var trs = $tbody.find("tr");
		var idx = $tr.index();
		if(idx>0) {
			$tr.insertBefore(trs.eq(idx-1));
		}
	}).attr("title","Move Up");
	var $mic2 = $("<i class='fa fa-caret-down'></i>");
	var $mlink2 = $("<a></a").attr("href","javascript:void(0)").addClass("btn").append($mic2).click(function() { 
		var trs = $tbody.find("tr");
		var idx = $tr.index();
		if(idx<trs.size()) {
			$tr.insertAfter(trs.eq(idx+1));
		}		
	}).attr("title","Move Down");
	var $div = $("<div></div>").addClass("input-group-btn-vertical").append($mlink1).append($mlink2);
	$td4.append(dlink).append(alink).append($div);
	$tr.append($td1).append($td2).append($td3).append($td4);
	if(rowIndex!=null) {
		var trs = $tbody.find("tr");
		if(trs.size()==0) {
			$tbody.append($tr);
		} else {
			if(rowIndex>=0) {
				$tr.insertAfter(trs.eq(rowIndex));
			} else {
				$tr.insertBefore(trs.eq(0));
			}
		}
	} else {
		$tbody.append($tr);
	}
}
var optionsCopied = [];
function copyOption(atablebody) {
	optionsCopied = [];
	$("#"+atablebody).find("tr").each(function(eidx,etr){
		var $ins = $(etr).find("input");
		var $in1 = $ins.eq(0);
		var $in2 = $ins.eq(1);
		var $in3 = $ins.eq(2);
		var val1 = $in1.val();
		var val2 = $in2.val();
		var val3 = $in3.val();
		if($.trim(val1)!="" && $.trim(val2)!="") {
			optionsCopied.push({value:val1, text:val2, control:val3});
		}
	});
}
function pasteOption(atablebody) {
	if(optionsCopied) {
		$(optionsCopied).each(function(index,element) { 
			addNewOption(atablebody,element.value,element.text,element.control);
		});
	}
}
function programCollapseExpand(src) {
	var $src = $(src);
	if($src.is(".down")) {
		$src.removeClass("down").addClass("up");
		$src.find(".fa").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-up");
		$("#filterfieldrow").show();
	} else {
		$src.removeClass("up").addClass("down");
		$src.find(".fa").removeClass("fa-chevron-circle-up").addClass("fa-chevron-circle-down");
		$("#filterfieldrow").hide();
	}		
}
function toggleLookupPanel(showlayer,hidelayer,hidelayer2,hidelayer3) {
	$(showlayer).show();
	$(hidelayer).hide();
	$(hidelayer2).hide();
	$(hidelayer3).hide();
}
function lookupServiceClick() {
	toggleLookupPanel("#lookupservicelayer","#lookuptablelayer","#lookupinilayer","#lookupcustomlayer");
	$("#lookuptype").val("service");
}
function lookupTableClick() {
	toggleLookupPanel("#lookuptablelayer","#lookupservicelayer","#lookupinilayer","#lookupcustomlayer");
	$("#lookuptype").val("table");
}
function lookupIniClick() {
	toggleLookupPanel("#lookupinilayer","#lookuptablelayer","#lookupservicelayer","#lookupcustomlayer");
	$("#lookuptype").val("ini");
}
function lookupCustomClick() {
	toggleLookupPanel("#lookupcustomlayer","#lookupinilayer","#lookuptablelayer","#lookupservicelayer");
	$("#lookuptype").val("custom");
}
function newLookupServiceClick() {
	toggleLookupPanel("#newlookupservicelayer","#newlookuptablelayer","#newlookupinilayer","#newlookupcustomlayer");
	$("#newlookuptype").val("service");
}
function newLookupTableClick() {
	toggleLookupPanel("#newlookuptablelayer","#newlookupservicelayer","#newlookupinilayer","#newlookupcustomlayer");
	$("#newlookuptype").val("table");
}
function newLookupIniClick() {
	toggleLookupPanel("#newlookupinilayer","#newlookuptablelayer","#newlookupservicelayer","#newlookupcustomlayer");
	$("#newlookuptype").val("ini");
}
function newLookupCustomClick() {
	toggleLookupPanel("#newlookupcustomlayer","#newlookupinilayer","#newlookuptablelayer","#newlookupservicelayer");
	$("#newlookuptype").val("custom");
}
function addNewFilterFunctionField() {
	$("#newfunctionfieldcontrollayer").hide();
	$("#newfuctioncolumn_layer_control").hide();
	$("#newfunctiondisplay_layer_control").show();
	$("#newfunctionfieldid").val("");
	$("#newfunctionfieldname").val("");
	$("#newfunctionfieldstyle").val("");
	$("#newfunctionoptiontablebody").empty();
	$("#newfunctionfieldcondition").val("");
	$("#newfunctionfieldcondition").trigger("change");	
	$("#newfunctionfieldshowing").attr("checked",true);
	$("#newfunctionfieldlisting").attr("checked",true);
	$("#newfunctiondisplayfield").attr("checked",false);
	$("#newfunctionfieldinteractive").attr("checked",false);
	functionfielddialog.dialog("option", "buttons", [{
		text: "Cancel",
			click: function() { $(this).dialog("close"); }
		}, {
			text: " OK ",
			click: function() { 
				var eid = $("#newfunctionfieldid").val();
				if(!validateDuplicateID(eid)) return;
				var ename = $("#newfunctionfieldname").val();
				if($.trim(ename)!="") {
					createNewFilterFunctionField();
					functionfielddialog.dialog("close"); 
				}
			}
    }]);
	functionfielddialog.dialog("open");
}
function createNewFilterFunctionField() {
	filterCounter++;
	var $input = $("<input></input>");
	var eid = $("#newfunctionfieldid").val();
	var ename = $("#newfunctionfieldname").val();
	var elabel = $("#newfunctionfieldlabel").val();
	var eplacement = $("#newfunctionfieldplacement").val();
	var estyle = $("#newfunctionfieldstyle").val();
	var edisplayplacement = $("#newfunctiondisplayplacement").val();
	if($.trim(eid)!="") $input.attr("id",eid);
	if($.trim(ename)!="") $input.attr("name",ename);
	if($.trim(estyle)!="") $input.attr("style",estyle);
	if(elabel!="") {
		$input.attr("label",elabel);
		if(eplacement!="") {
			$input.attr("placement",eplacement);
		} else {
			$input.removeAttr("placement");
		}
	} else {
		$input.removeAttr("label");
		$input.removeAttr("placement");
	}
	if($("#newfunctionfieldshowing").is(":checked")) {
		$input.attr("show",true);
	}
	if($("#newfunctionfieldlisting").is(":checked")) {
		$input.attr("listing",true);
	}
	if($("#newfunctionfieldheaderlabel").is(":checked")) {
		$input.attr("headerlabel",true);
	}
	var edisplay = $("#newfunctiondisplayfield").is(":checked");
	if(edisplay) {
		$input.attr("display",true);
	}
	if($.trim(edisplayplacement)!="") {
		$input.attr("displayplacement",edisplayplacement);
	} else {
		$input.removeAttr("displayplacement");
	}
	$input.attr("calfield",true);
	$input.attr("calfunction",$("#newfunctionfieldcondition").val());
	if($("#newfunctionfieldinteractive").is(":checked")) {
		$input.attr("calinteractive",true);
	}
	addNewFunctionFieldOption($input);
	var pnode = $("program",prgxmldoc);
	var fnode = $("filters",pnode).eq(0);
	fnode.append($input);
	$input.attr("idx",filterCounter);
	var $li = $("<li></li>");
	$li.html(elabel+"("+ename+")");
	$li.attr("idx",filterCounter);
	$li.data("filterdata",$input);
	$li.addClass("ui-state-default highlight-state");
	var $flist = $("#filterlisting");
	$flist.append($li);
	bindingFilterListItem($li,$flist,filterFieldClick);
	$li.trigger("click");
	return true;
}
function addNewFunctionFieldOption($input) {
	$input.empty();
	$("#newfunctionoptiontablebody").find("tr").each(function(eidx,etr){
		var $ins = $(etr).find("input");
		var $in1 = $ins.eq(0);
		var $in2 = $ins.eq(1);
		var $in3 = $ins.eq(2);
		var $op = $("<option></option>");
		$op.attr("value",$in1.val());
		$op.text($in2.val());
		$op.attr("control",$in3.val());
		$input.append($op);
	});
}
function addNewFilterFunctionField() {
	addNewFuncField($("#filtersublistcolumn"));
}
function addNewFunctionField() {
	addNewFuncField($("#sublistcolumn"));
}
function addNewFuncField($subcol) {
	if(!ensureActiveRow($subcol)) return;
	$("#newfunctionfieldcontrollayer").show();
	$("#newfunctionlookup_layer_control").show();
	$("#newfunctioncolumn_layer_control").show();
	$("#newfunctiondisplay_layer_control").hide();
	$("#newfunctionfieldid").val("");
	$("#newfunctionfieldname").val("");
	$("#newfunctionfieldvalue").val("");
	$("#newfunctionfieldstyle").val("");
	$("#newfunctionfieldwidth").val("");
	$("#newfunctionfieldheight").val("");
	$("#newfunctionfieldlabel").val("");
	$("#newfunctionfieldcontrol").val("");
	$("#newfunctionfieldcurrenting").attr("checked",false);
	$("#newfunctionfielddecimal").val("");
	$("#newfunctionfieldprecision").val("");
	$("#newfunctionfielddecimalformat").val("");
	$("#newfunctionfieldrequired").attr("checked",false);
	$("#newfunctionfieldcompleted").attr("checked",false);
	$("#newfunctionfieldalerted").attr("checked",false);
	$("#newfunctionfieldshowing").attr("checked",true);
	$("#newfunctionfieldlisting").attr("checked",false);
	$("#newfunctionfieldfiltering").attr("checked",false);
	$("#newfunctionfieldcondition").val("");
	$("#newfunctionfieldplacement").val("");
	$("#newfunctionfieldheaderlabel").attr("checked",false);
	$("#newfunctioncolumnalign").val("");
	$("#newfunctionfieldcolumnstyle").val("");
	$("#newfunctionfieldcolspan").val("");
	$("#newfunctionfieldrowspan").val("");
	$("#newfunctionfieldcolumns").val("");
	$("#newfunctionoptiontablebody").empty();
	$("#newfunctionfieldcondition").trigger("change");
	functionfielddialog.dialog("option", "title","New Function Field");
	functionfielddialog.dialog("option", "buttons", [{
		text: "Cancel",
			click: function() { $(this).dialog("close"); }
		}, {
			text: " OK ",
			click: function() { 
				var eid = $("#newfunctionfieldid").val();
				if(!validateDuplicateID(eid)) return;
				var ename = $("#newfunctionfieldname").val();
				if($.trim(ename)!="") {
					createNewFunctionField($subcol);
					functionfielddialog.dialog("close"); 
				}
			}
    }]);
	functionfielddialog.dialog("open");
}
function createNewFunctionField($subcol) {
	columnCounter++;
	if(!$subcol) $subcol = $("#sublistcolumn");
	var $subdiv = $("div.active-state",$subcol).eq(0);
	var $sit = $("ul",$subdiv).eq(0);
	var $row = $subdiv.data("rowdata");
	var $col = $("<col></col>");
	var $input = $("<input></input>");
	var eid = $("#newfunctionfieldid").val();
	var ename = $("#newfunctionfieldname").val();
	var elabel = $("#newfunctionfieldlabel").val();
	var etype = "text";
	var edatatype = "string";
	var eplacement = $("#newfunctionfieldplacement").val();
	var estyle = $("#newfunctionfieldstyle").val();
	var ewidth = $("#newfunctionfieldwidth").val();
	var eheight = $("#newfunctionfieldheight").val();
	if($.trim(eid)!="") $input.attr("id",eid);
	if($.trim(ename)!="") $input.attr("name",ename);
	if($.trim(estyle)!="") $input.attr("style",estyle);
	if(elabel!="") {
		$input.attr("label",elabel);
		if(eplacement!="") {
			$input.attr("placement",eplacement);
		} else {
			$input.removeAttr("placement");
		}
	}
	if($.trim(ewidth)!="") $input.attr("width",ewidth);
	if($.trim(eheight)!="") $input.attr("height",eheight);
	if($.trim(etype)!="") $input.attr("type",etype);
	if($.trim(edatatype)!="") $input.attr("datatype",edatatype);
	if($("#newfunctionfieldshowing").is(":checked")) {
		$input.attr("show",true);
	} 
	if($("#newfunctionfieldlisting").is(":checked")) {
		$input.attr("listing",true);
	} 
	if($("#newfunctionfieldheaderlabel").is(":checked")) {
		$input.attr("headerlabel",true);
	} 
	var econdition = $("#newfunctionfieldcondition").val();
	if($.trim(econdition)!="") $input.attr("calfunction",econdition);
	else $input.removeAttr("calfunction");
	if($("#newfunctionfieldinteractive").is(":checked")) {
		$input.attr("calinteractive",true);
	} 
	$input.attr("calfield",true);
	$input.attr("display",true);
	addNewFunctionFieldOption($input);
	var ealign = $("#newfunctioncolumnalign").val();
	var ecolstyle = $("#newfunctionfieldcolumnstyle").val();
	var ecolspan = $("#newfunctionfieldcolspan").val();
	var erowspan = $("#newfunctionfieldrowspan").val();
	var ecolumns = $("#newfunctionfieldcolumns").val();
	if($.trim(ealign)!="") $col.attr("align",ealign);
	if($.trim(ecolstyle)!="") $col.attr("style",ecolstyle);
	if($.trim(ecolspan)!="") $col.attr("colspan",ecolspan);
	if($.trim(erowspan)!="") $col.attr("rowspan",erowspan);
	if($.trim(ecolumns)!="") $col.attr("columns",ecolumns);
	$col.append($input);
	$row.append($col);
	$col.attr("idx",columnCounter);

	var $cdiv = $("<div></div>");
	$cdiv.addClass("collayer-class");
	$cdiv.append($("<i class='fa fa-cogs' aria-hidden='true'></i>"));
	$cdiv.append("&#160;");
	var $span = $("<span></span>");
	$span.html(elabel+"("+ename+")");
	$cdiv.append($span);
	var $cli = $("<li></li>");
	$cli.addClass("ui-state-default input-class");
	$cli.attr("idx",columnCounter);
	$cli.data("rowdata",$row);
	$cli.data("coldata",$col);
	$cli.append($cdiv);
	$sit.append($cli);
	bindingSubListItem($cli,$sit,$subcol,inputFieldClick);
	$cli.trigger("click");
	return true;
}
function updateFunctionField($e,$lbl,fn) {
	var eid = $e.attr("id");
	var ename = $e.attr("name");
	var elabel = $e.attr("label");
	var eshow = $e.attr("show");
	var elisting = $e.attr("listing");
	var ereserved = $e.attr("reserved");
	var eplacement = $e.attr("placement");
	var estyle = $e.attr("style");
	var ewidth = $e.attr("width");
	var eheight = $e.attr("height");
	var eheading = $e.attr("headerlabel");
	var edisplay = $e.attr("display");
	var edisplayplacement = $e.attr("displayplacement");
	var econdition = $e.attr("calfunction");
	var einteractive = $e.attr("calinteractive");
	if(!ewidth) ewidth = "";
	if(!eheight) eheight = "";
	if(!elabel) elabel = "";
	if(!eid) eid = "";
	if(!ename) ename = "";
	if(!eplacement) eplacement = "";
	if(!estyle) estyle = "";
	if(!edisplay) edisplay = "";
	if(!edisplayplacement) edisplayplacement = "";
	if(!econdition) econdition = "";
	if(!einteractive) einteractive = "";
	if($lbl) elabel = $lbl.text();
	$("#functionfieldid").val(eid);
	$("#functionfieldname").val(ename);
	$("#functionfieldname").attr("disabled","true"==ereserved);
	$("#functionfieldlabel").val(elabel);
	$("#functionfieldwidth").val(ewidth);
	$("#functionfieldheight").val(eheight);
	$("#functionfieldshowing").attr("checked","true"==eshow);
	$("#functionfieldlisting").attr("checked","true"==elisting);
	$("#functionfieldheaderlabel").attr("checked","true"==eheading);
	$("#functionfieldplacement").val(eplacement);
	$("#functionfieldstyle").val(estyle);
	$("#displayfield").attr("checked","true"==edisplay);
	$("#displayplacement").val(edisplayplacement);	
	$("#functionfieldcondition").val(econdition);
	$("#functionfieldinteractive").attr("checked","true"==einteractive);
	updateFunctionFieldOption($e);
	$("#functionfieldcondition").trigger("change");
	var oldid = eid;
	var oldname = ename;
	var $btn = $("#functionupdatefieldbutton");
	$btn.unbind("click");
	$btn.bind("click",function() { 
		ename = $("#functionfieldname").val();
		if($.trim(ename)=="") {
			alertmsg("QS0105","Field Name can not be blank");
			return;
		}
		eid = $("#functionfieldid").val();	
		if(!validUpdateDuplicateID(eid,oldid)) return;
		if($.trim(eid)!="") { $e.attr("id",eid); } else { $e.removeAttr("id"); }
		$e.attr("name",ename);
		if($lbl) $lbl.text($("#functionfieldlabel").val());
		else {
			elabel = $("#functionfieldlabel").val();
			if(elabel!="") { $e.attr("label",elabel); } else { $e.removeAttr("label"); }
		}
		eplacement = $("#functionfieldplacement").val();
		if($.trim(eplacement)!="") { $e.attr("placement",eplacement); } else { $e.removeAttr("placement"); }
		ewidth = $("#functionfieldwidth").val();
		if($.trim(ewidth)!="") { $e.attr("width",ewidth); } else { $e.removeAttr("width"); }
		eheight = $("#functionfieldheight").val();
		if($.trim(eheight)!="") { $e.attr("height",eheight); } else { $e.removeAttr("height"); }
		$e.attr("type","text");
		$e.attr("datatype","string");
		$e.attr("show",$("#functionfieldshowing").is(":checked"));
		$e.attr("listing",$("#functionfieldlisting").is(":checked"));
		$e.attr("headerlabel",$("#functionfieldheaderlabel").is(":checked"));
		econdition = $("#functionfieldcondition").val();
		if($.trim(econdition)!="") { $e.attr("calfunction",econdition); } else { $e.removeAttr("calfunction"); }
		$e.attr("calinteractive",$("#functionfieldinteractive").is(":checked"));
		estyle = $("#functionfieldstyle").val();
		if($.trim(estyle)!="") { $e.attr("style",estyle); 	} else { $e.removeAttr("style"); }
		
		$e.attr("display",$("#displayfield").is(":checked"));
		edisplayplacement = $("#displayplacement").val();
		if($.trim(edisplayplacement)!="") { $e.attr("displayplacement",edisplayplacement); } else { $e.removeAttr("displayplacement"); }
		$e.empty();
		$("#functionoptiontablebody").find("tr").each(function(eidx,etr){
			var $ins = $(etr).find("input");
			var $in1 = $ins.eq(0);
			var $in2 = $ins.eq(1);
			var $in3 = $ins.eq(2);
			var $op = $("<option></option>");
			$op.attr("value",$in1.val());
			$op.text($in2.val());
			$op.attr("control",$in3.val());
			$e.append($op);
		});
		if(fn) fn();
		alertmsg("QS0104","Update success");
	});
}
function addNewFunctionOption(bodyid,val1,val2,val3) {
	var $tbody = $("#"+bodyid);
	var $tr = $("<tr></tr>");
	var $td1 = $("<td></td>");
	var $td2 = $("<td></td>");
	var $in1 = $("<input type='hidden'></input>");
	var $in2 = $("<input></input>");
	var $in3 = $("<input type='hidden'></input>");
	if(val1) $in1.val(val1); 
	if(val2) $in2.val(val2);
	if(val3) $in3.val(val3);
	$in2.addClass("option-text form-control");
	$td1.addClass("column-value");
	$td2.addClass("column-text");
	$td1.append($in1).append($in1.val());
	$td2.append($in2).append($in3);
	$tr.append($td1).append($td2);
	$tbody.append($tr);
}
function updateFunctionFieldOption($e) {
	$("#functionoptiontablebody").empty();
	$e.children().each(function(idx,elm) { 
		var $opt = $(elm);
		addNewFunctionOption("functionoptiontablebody",$opt.attr("value"),$opt.text(),$opt.attr("control"));
	});
}
function displayCalculatorFunction(bodyid,val) {
	var optAry = [];
	var $tbody = $("#"+bodyid);
	$tbody.find("tr").each(function(eidx,etr){
		var $ins = $(etr).find("input");
		var $in1 = $ins.eq(0);
		var $in2 = $ins.eq(1);
		var $in3 = $ins.eq(2);
		optAry.push({value: $in1.val(), text: $in2.val(), control: $in3.val()});
	});
	$tbody.empty();
	var fary = calculator_functions[val];
	if(fary) {
		var pary = fary.parameters;
		$(pary).each(function(idx,elm) {
			var txt = "";
			var opt = optAry[idx];
			if(opt) txt = opt.text;
			addNewFunctionOption(bodyid,elm.value,txt,""+elm.control);			
		});	
	}
}