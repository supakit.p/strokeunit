<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
boolean fsIsAjax = fsGlobal.isAjax();
int file_count = 0;
try { 
		String progid = request.getParameter("progid");
		if(progid!=null && progid.trim().length()>0) {
			String realpath = request.getServletContext().getRealPath("");
			String apppath = realpath+java.io.File.separator+progid;
			java.io.File apdir = new java.io.File(apppath);
			if(apdir.exists()) {
				java.io.File[] files = apdir.listFiles();
				file_count = files!=null?files.length:0;
			}
		}		
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
%>
{"files":<%=file_count%>}
