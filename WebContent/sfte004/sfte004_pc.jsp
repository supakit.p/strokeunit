<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.kit.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte004',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte004");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
JProgram fsProg = new JProgram();
fsProg.setDefaults("PROMPT");
fsProg.setPreviewing(true);
fsProg.setGenerateLabel(false);
fsProg.setServicePackage("com.fs.dev.prompt.service");
fsProg.setDefaultEntryPage(false);
fsProg.setUsingDialog(true);
try { 
		String fs_xmltext = request.getParameter("xmltext");
		if(fs_xmltext!=null && fs_xmltext.trim().length()>0) {
			//System.out.println(fs_xmltext);
			//check if there are encoding declaration?
			int idx = fs_xmltext.indexOf("?>");
			if(idx>0) {
				fs_xmltext = fs_xmltext.substring(idx+2);
			}			
			fs_xmltext = "<?xml version=\"1.0\" encoding=\"TIS-620\"?>\n"+fs_xmltext;
			System.out.println(fs_xmltext);
			String realpath = request.getServletContext().getRealPath("");
			fsProg.setDirectory(realpath);
			fsProg.loadXML(fs_xmltext);
			if(fsProg.getProgid()!=null && fsProg.getProgid().trim().length()>0) {
				fsProg.setProgid("pre"+fsProg.getProgid());
			} else {
				fsProg.setProgid("preview");
			}
			if(fsProg.getProgid()!=null && fsProg.getProgid().trim().length()>0) {
				String apppath = fsProg.getDirectory()+java.io.File.separator+fsProg.getProgid();
				java.io.File apdir = new java.io.File(apppath);
				if(!apdir.exists()) apdir.mkdirs();
				else {
					java.io.File[] files = apdir.listFiles();
					if(files!=null) {
						for(java.io.File f : files) {
							try {
								if(f.isFile()) f.delete();
							}catch(Exception ex) { }
						}
					}
				}
				if(fsProg.getTableName()==null || fsProg.getTableName().trim().length()<=0) {
					fsProg.setTableName("t"+fsProg.getProgid());
				}
				fsProg.generate();
			}
		}		
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
String afilename = "";
String aname = "";
String atablename = "";
String acaption = "";
if(fsProg.getFileName()!=null) afilename = JSONUtility.parseJSONString(fsProg.getFileName());
if(fsProg.getProgid()!=null) aname = JSONUtility.parseJSONString(fsProg.getProgid());
if(fsProg.getTableName()!=null) atablename =JSONUtility.parseJSONString(fsProg.getTableName());
if(fsProg.getCaption()!=null) acaption =JSONUtility.parseJSONString(fsProg.getCaption());
%>
{"filename":"<%=afilename%>", "name":"<%=aname%>", "table":"<%=atablename%>", "caption":"<%=acaption%>"}
