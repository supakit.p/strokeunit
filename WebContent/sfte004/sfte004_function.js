var calculator_functions = {
	"NotNullCalculator" : { text: "Not Null", parameters: [{control:true, value:"Controller"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"EqualCalculator" : { text: "Equals(=)", parameters: [{control:true, value:"Controller"},{control:false, value:"Value"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"NotEqualCalculator" : { text: "Not Equals(!=)", parameters: [{control:true, value:"Controller"},{control:false, value:"Value"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"GreaterThanCalculator" : { text: "Greater Than(>)", parameters: [{control:true, value:"Controller"},{control:false, value:"Value"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"LesserThanCalculator" : { text: "Lesser Than(<)", parameters: [{control:true, value:"Controller"},{control:false, value:"Value"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"GreaterThanOrEqualCalculator" : { text: "Greater Than Equals(>=)", parameters: [{control:true, value:"Controller"},{control:false, value:"Value"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"LesserThanOrEqualCalculator" : { text: "Lesser Than Equals(<=)", parameters: [{control:true, value:"Controller"},{control:false, value:"Value"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"InsetCalculator" : { text: "In Set", parameters: [{control:true, value:"Controller"},{control:false, value:"Value Set"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	
	"EqualFieldCalculator" : { text: "Field Equals(=)", parameters: [{control:true, value:"Controller"},{control:true, value:"Field"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"NotEqualFieldCalculator" : { text: "Field Not Equals(!=)", parameters: [{control:true, value:"Controller"},{control:true, value:"Field"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"GreaterThanFieldCalculator" : { text: "Field Greater Than(>)", parameters: [{control:true, value:"Controller"},{control:true, value:"Field"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"LesserThanFieldCalculator" : { text: "Field Lesser Than(<)", parameters: [{control:true, value:"Controller"},{control:true, value:"Field"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"GreaterThanOrEqualFieldCalculator" : { text: "Field Greater Than Equals(>=)", parameters: [{control:true, value:"Controller"},{control:true, value:"Field"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"LesserThanOrEqualFieldCalculator" : { text: "Field Lesser Than Equals(<=)", parameters: [{control:true, value:"Controller"},{control:true, value:"Field"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	"InsetFieldCalculator" : { text: "Field In Set", parameters: [{control:true, value:"Controller"},{control:true, value:"Field Set"},{control:false, value:"If True"},{control:false, value:"If False"}] },
	
	"AgeCalculator" : { text: "Age Calculator", parameters: [{control:true, value:"Controller"},{control:false, value:"Year Label"},{control:false, value:"Month Label"},{control:false, value:"Day Label"}] },
	"BMICalculator" : { text: "BMI Calculator", parameters: [{control:true, value:"Weight Controller"},{control:true, value:"Height Controller"}] }
};
