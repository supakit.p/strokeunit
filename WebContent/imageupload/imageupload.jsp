<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<c:if test="${fsScreen.init('imageupload',pageContext.request, pageContext.response,true)}"></c:if>
<%
request.setCharacterEncoding("TIS-620"); //force thai file name
if(!FileUpload.isMultipartContent(request)) {
	fsGlobal.createResponseStatus(out, response, 9001, "Not supported format (multipart need)");
	return;
}
String filename = "";
try {	
	String fs_temppath = request.getServletContext().getRealPath("")+java.io.File.separator+"uploaded"+java.io.File.separator+"temp";
	java.io.File fs_tempdir = new java.io.File(fs_temppath);
	if(!fs_tempdir.exists()) fs_tempdir.mkdirs();
	DefaultFileItemFactory fs_factory = new DefaultFileItemFactory();
	fs_factory.setRepository(fs_tempdir);
	fs_factory.setSizeThreshold(512);
	FileUpload fs_upload = new FileUpload(fs_factory);
	fs_upload.setSizeMax(1000000000L);
	java.util.List<FileItem> fs_items = fs_upload.parseRequest(request);	
	String resources_path = com.fs.dev.TheUtility.getResourcesPath(request);
	String loadpath = resources_path+java.io.File.separator+"progs"+java.io.File.separator;
	java.io.File fdir = new java.io.File(loadpath);
	if(!fdir.exists()) fdir.mkdirs();
	boolean fs_found = false;
	for(java.util.Iterator fs_it = fs_items.iterator();fs_it.hasNext();) {
		FileItem fs_item = (FileItem) fs_it.next();
		if (!fs_item.isFormField()) {
			fs_found = true;			
			Trace.debug("file item : "+fs_item.getName());
			filename = fs_item.getName();
			if(filename!=null && filename.trim().length()>0) {
				int index = fs_item.getName().lastIndexOf("\\");
				if(index<0) index = fs_item.getName().lastIndexOf(java.io.File.separator);
				if(index>0) filename = fs_item.getName().substring(index+1);
				int counter = 1;
				String fullfilename = loadpath + filename;
				java.io.File fs_uploadfile = new java.io.File(fullfilename);
				while(fs_uploadfile.exists()) {
					String afilename = filename;
					String extension = "";
					int idx = filename.lastIndexOf(".");
					if(idx>0) {
						afilename = filename.substring(0,idx);
						extension = filename.substring(idx);
					}
					fullfilename = loadpath + afilename+("_"+counter)+extension;
					Trace.debug("file name = "+fullfilename+" : "+counter);
					fs_uploadfile = new java.io.File(fullfilename);
					counter++;
				}
				fs_item.write(fs_uploadfile);
				Trace.debug("saving as : "+fullfilename);
			}
		}
	}
} catch(Throwable ex) {
	ex.printStackTrace();
	String msg = ex.getMessage()==null?ex.getClass().getName():ex.getMessage();
	fsGlobal.createResponseStatus(out, response, 9002, msg);
	return;
}
%>
{"filename":"<%=filename%>"}
