var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("sfte001"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
		setupComponents();
		setupAlertComponents();
}
function setupComponents() {
		$("#searchbutton").click(function(evt) { 
			//if(!confirm("search")) return false;
			search(); 
			return false;
		});
		$("#insertbutton").click(function(evt) { 
			insert(); 
			return false;
		});
		$("#savebutton").click(function() { 
			save();
			return false;
		});
		$("#cancelbutton").click(function() { 
			cancel();
			return false;
		});
		$("#uploadbutton").click(function() { 
			if(!validForm()) return false;
			$("#uploadiconfile").trigger("click");
		});
		$("#uploadiconfile").change(function() { 
			console.log("iconfile = "+$("#uploadiconfile").val());
			if($.trim($("#uploadiconfile").val())!="") {
				uploadIconFile();
			}
		});
		$("#iconstyleswitcher").styleswitcher({$styleInput: $("#iconstyle")});
}
function clearingFields() {
		clearAlerts();
		fsentryform.reset();
}
function search(aform) {
		if(!aform) aform = fssearchform;
		//if(!confirm($(aform).serialize())) return false;
		startWaiting();
		jQuery.ajax({
			url: "sfte001_c.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				//alert(transport.responseText);
				searchComplete(transport,data);
			}
		});	
}
function searchComplete(xhr,data) {
		stopWaiting();
		$("#listpanel").html(data);
}
function insert() {
		clearingFields();
		fsentryform.fsDatatype.value = "json";
		fsentryform.fsAction.value = "enter";
		$("#iconfileimage").attr("src","../resources/apps/application.png");
		var icons = "fa fa-desktop";
		$("#iconstyle").val(icons);
		$("#iconstyleswitcher").styleupdate(icons);
		$("input.ikeyclass",fsentryform).editor({edit:true});
		showPageEntry();
}
function showPageSearch() {
		$("#entrypanel").hide();
		$("#searchpanel").show();
}
function showPageEntry() {
		$("#entrypanel").show();
		$("#searchpanel").hide();
}
function submitRetrieve(rowIndex) {
		var aform = fslistform;
		aform.fsRowid.value = ""+rowIndex;
		aform.fsDatatype.value = 'json';
		//alert($(aform).serialize());
		startWaiting();
		jQuery.ajax({
			url: "sfte001_dc.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				//alert(transport.responseText);
				prepareScreenToUpdate(fsentryform,data);
			}
		});	
}
function prepareScreenToUpdate(aform,data) {
		aform.fsDatatype.value = "text";
		aform.fsAction.value = "edit";
		try {
			var jsRecord = $.parseJSON(data);
			for(var p in jsRecord) {
				$("#"+p,aform).val(jsRecord[p]);
			}
			var icfile = jsRecord["iconfile"];
			if(icfile && icfile!="") {
				$("#iconfileimage").attr("src","../resources/apps/"+jsRecord["iconfile"]);				
			} else {
				$("#iconfileimage").attr("src","../resources/apps/application.png");				
			}
			var icons = jsRecord["iconstyle"];		
			if(!icons || $.trim(icons)=="") icons = "fa fa-desktop";
			$("#iconstyleswitcher").styleupdate(icons);			
		} catch(ex) { }
		$("input.ikeyclass",aform).editor({edit:false});
		showPageEntry();
}
function cancel() {
		confirmCancel(function() {
			clearingFields();
			showPageSearch();
		});
}
function validForm() {
	clearAlerts();
	var validator = null;
	if($.trim($("#programid").val())=="") {
		$("#programid").parent().addClass("has-error");
		$("#programid_alert").show();
		if(!validator) validator = "programid";
	}
	if($.trim($("#progname").val())=="") {
		$("#progname").parent().addClass("has-error");
		$("#progname_alert").show();
		if(!validator) validator = "progname";
	}
	if($.trim($("#prognameth").val())=="") {
		$("#prognameth").parent().addClass("has-error");
		$("#prognameth_alert").show();
		if(!validator) validator = "prognameth";
	}
	if($.trim($("#shortname").val())=="") {
		$("#shortname").parent().addClass("has-error");
		$("#shortname_alert").show();
		if(!validator) validator = "shortname";
	}
	if($.trim($("#shortnameth").val())=="") {
		$("#shortnameth").parent().addClass("has-error");
		$("#shortnameth_alert").show();
		if(!validator) validator = "shortnameth";
	}
	if(validator) {
		$("#"+validator).focus();
		setTimeout(function() { 
			$("#"+validator).parent().addClass("has-error");
			$("#"+validator+"_alert").show();
		},100);
		return false;
	}
	return true;
}
function save(aform) {
		if(!aform) aform = fsentryform;
		//alert($(aform).serialize());
		if(!validForm()) return false;
		var isInsertMode = aform.fsAction.value=='enter';
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte001_dc.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,				
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					if(isInsertMode) {
						successbox(function() { clearingFields(); });					
					} else {
						showPageSearch();
						fssearchform.fsPage.value = fslistform.fsPage.value;
						search();
					}
				}
			});
		});
		return false;
}
function submitChapter(aform,index) {
		//alert($(aform).serialize());
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte001_c.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
				//alert(transport.responseText);
			}
		});
}
function submitOrder(fsParams) {
		//alert(fsParams);
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte001_cd.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: fsParams,
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
				//alert(transport.responseText);
			}
		});
		return false;
}
function submitDelete(fsParams) {
		//alert("delete : "+fsParams);
		confirmDelete([fsParams[0]],function() {
			deleteRecord(fsParams);
		});
}
function deleteRecord(fsParams) {
		//alert(fsParams);
		startWaiting();
		jQuery.ajax({
			url: "sfte001_dc.jsp",
			type: "POST",
			data: "fsAction=delete&fsDatatype=json&programid="+fsParams[0],
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				showPageSearch();
				fssearchform.fsPage.value = fslistform.fsPage.value;
				search();
			}
		});	
}
function uploadIconFile(aform) {
	if(!aform) aform = uploadform;
	if(!validForm()) return false;
	$("#uploadprogramid").val($("#programid").val());
	startWaiting();	
	var fd = new FormData(aform);
	var xhr = jQuery.ajax({
		url: "iconupload.jsp",
		type: "POST",
		dataType: "html",
		data: fd,
		enctype: "multipart/form-data",
		processData: false, 
		contentType: false, 
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown); 
		},
		success: function(data,status,transport){ 
			console.log("response : "+transport.responseText);
			stopWaiting();
			var json = $.parseJSON($.trim(data));
			if(json && json["filename"]) {
				var imgsrc = "../resources/apps/"+json["filename"];
				$("#iconfileimage").attr("src",imgsrc);
				$("#iconfile").val(json["filename"]);
			}
		}
	});	
}


