<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE001ABean" scope="session" class="com.fs.bean.SFTE001ABean"/>
<jsp:include page="sfte001_md.jsp"/>
	<div id="entrylayer" class="entry-layer">
		<form id="fsentryform" name="fsentryform" method="post" accept-charset="UTF-8">	
			<input type="hidden" name="fsAction" value="enter"/>
			<input type="hidden" name="fsAjax" value="true"/>
			<input type="hidden" name="fsDatatype" value="text"/>
			<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
			<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
			<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>	
			<input type="hidden" name="iconstyle" id="iconstyle"/>
			<div class="row portal-area sub-entry-layer">
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="programid_label" tagclass="control-label" required="true">Program ID</fs:label>
						</div>
						<div class="col-md-2 col-height">
							<input class="form-control input-md ikeyclass alert-input" id="programid" name="programid" placeholder="" autocomplete="off" size="15"/>
							<div id="programid_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('programid_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="progname_label" tagclass="control-label" required="true">Program Name(English)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="progname" name="progname" placeholder="" autocomplete="off" size="50"/>
							<div id="progname_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('progname_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="prognameth_label" tagclass="control-label" required="true">Program Name(Thai)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="prognameth" name="prognameth" placeholder="" autocomplete="off" size="50"/>
							<div id="prognameth_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('prognameth_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="product_label" tagclass="control-label" required="true">Product</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<fs:select name="product" tagclass="form-control input-md" tagid="product" selection="PRODUCT_CATEGORY" > </fs:select>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="progtype_label" tagclass="control-label">Program Type</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<fs:select name="progtype" tagclass="form-control input-md" tagid="progtype" selection="PROGTYPE_CATEGORY" > </fs:select>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="progsystem_label" tagclass="control-label">Program System</fs:label>
						</div>
						<div class="col-md-2 col-height">
							<fs:select name="progsystem" tagclass="form-control input-md" tagid="progsystem" selection="SYSTEM_CATEGORY" > </fs:select>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="description_label" tagclass="control-label">Description</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md" id="description" name="description" placeholder="" autocomplete="off" size="100"/>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="shortname_label" tagclass="control-label" required="true">Short Name(English)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="shortname" name="shortname" placeholder="" autocomplete="off" size="50"/>
							<div id="shortname_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('shortname_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="shortnameth_label" tagclass="control-label" required="true">Short Name(Thai)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="shortnameth" name="shortnameth" placeholder="" autocomplete="off" size="50"/>
							<div id="shortnameth_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('shortnameth_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="parameters_label" tagclass="control-label">Parameters</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md" id="parameters" name="parameters" placeholder="" autocomplete="off" size="50"/>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="progpath_label" tagclass="control-label">Program Path</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md" id="progpath" name="progpath" placeholder="" autocomplete="off" size="100"/>
						</div>
				</div>
				<div class="row row-heighter">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="iconstyle_label" tagclass="control-label" required="false">Icon Style</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<div id="iconstyleswitcher"></div>
						</div>
				</div>				
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="iconfile_label" tagclass="control-label">Icon File</fs:label>
						</div>
						<div class="col-md-3 col-height" id="iconfilelayer">
							<input type="hidden" class="form-control input-md" id="iconfile" name="iconfile" />
							<input type="button" id="uploadbutton" class="btn btn-base btn-sm" value="${fsLabel.getText('uploadbutton','Upload')}"/>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-9 col-height pull-right">
							<img id="iconfileimage" width="128px" height="128px" src="../resources/apps/application.png" alt=""/>
						</div>
				</div>
				<div class="row row-height">
					<div class="col-md-4 pull-right text-right" style="margin-right: 10px;">
						<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save')}"/>
						&nbsp;&nbsp;
						<input type="button" id="cancelbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('cancelbutton','Cancel')}"/>
					</div>
				</div>
			</div>
		</form>
		<div id="uploadlayer" style="display:none;">
			<form id="uploadform" name="uploadform" enctype="multipart/form-data">
				<input type="hidden" id="uploadprogramid" name="programid" />
				<div id="uploadiconlayer"><input type="file" id="uploadiconfile" name="iconfile" /></div>
			</form>
		</div>
	</div>