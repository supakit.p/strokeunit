<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte001',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE001Bean" scope="session" class="com.fs.bean.SFTE001Bean"/>
<jsp:useBean id="fsSFTE001ABean" scope="request" class="com.fs.bean.SFTE001ABean"/>
<jsp:setProperty name="fsSFTE001ABean" property="*"/>
<%
request.setCharacterEncoding("TIS-620"); //force thai file name
if(!FileUpload.isMultipartContent(request)) {
	fsGlobal.createResponseStatus(out, response, 9001, "Not supported format (multipart need)");
	return;
}
try {	
	String fs_temppath = request.getServletContext().getRealPath("")+java.io.File.separator+"uploaded"+java.io.File.separator+"temp";
	java.io.File fs_tempdir = new java.io.File(fs_temppath);
	if(!fs_tempdir.exists()) fs_tempdir.mkdirs();
	DefaultFileItemFactory fs_factory = new DefaultFileItemFactory();
	fs_factory.setRepository(fs_tempdir);
	fs_factory.setSizeThreshold(512);
	FileUpload fs_upload = new FileUpload(fs_factory);
	fs_upload.setSizeMax(1000000000L);
	java.util.List<FileItem> fs_items = fs_upload.parseRequest(request);	
	fsSFTE001ABean.obtain(fs_items);
	fsGlobal.obtain(fs_items);
	/*
	for (int i = 0; i < fs_items.size(); ++i) {
		FileItem fs_item = (FileItem) fs_items.get(i);
		if (fs_item.isFormField()) {
			String fs_key = fs_item.getFieldName();
			if (fs_key.equalsIgnoreCase("programid")) {
				fsSFTE001ABean.setProgramid(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("progname")) {
				fsSFTE001ABean.setProgname(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("progtype")) {
				fsSFTE001ABean.setProgtype(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("system")) {
				fsSFTE001ABean.setSystem(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("description")) {
				fsSFTE001ABean.setDescription(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("parameters")) {
				fsSFTE001ABean.setParameters(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("product")) {
				fsSFTE001ABean.setProduct(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("fsAction")) {
				fsGlobal.setFsAction(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("fsAjax")) {
				fsGlobal.setFsAjax(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("fsDatatype")) {
				fsGlobal.setFsDatatype(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("fsChapter")) {
				fsGlobal.setFsChapter(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("fsLimit")) {
				fsGlobal.setFsLimit(fs_item.getString());
			} else if (fs_key.equalsIgnoreCase("fsPage")) {
				fsGlobal.setFsPage(fs_item.getString());
			}
		}
	}
	*/
	String resources_path = com.fs.dev.TheUtility.getResourcesPath(request);
	String loadpath = resources_path+java.io.File.separator+"apps"+java.io.File.separator;
	java.io.File fdir = new java.io.File(loadpath);
	if(!fdir.exists()) fdir.mkdirs();
	boolean fs_found = false;
	for(java.util.Iterator fs_it = fs_items.iterator();fs_it.hasNext();) {
		FileItem fs_item = (FileItem) fs_it.next();
		if (!fs_item.isFormField()) {
			fs_found = true;			
			Trace.debug("file item : "+fs_item.getName());
			String filename = fs_item.getName();
			if(filename!=null && filename.trim().length()>0) {
				int index = fs_item.getName().lastIndexOf("\\");
				if(index<0) index = fs_item.getName().lastIndexOf(java.io.File.separator);
				if(index>0) filename = fs_item.getName().substring(index+1);
				fsSFTE001ABean.setIconfile(filename);
				loadpath += filename;
				java.io.File fs_uploadfile = new java.io.File(loadpath);
				fs_item.write(fs_uploadfile);
				Trace.debug("saving as : "+loadpath);
			}
		}
	}
} catch(Throwable ex) {
	Trace.error(ex);
	String msg = ex.getMessage()==null?ex.getClass().getName():ex.getMessage();
	fsGlobal.createResponseStatus(out, response, 9002, msg);
	return;
}
Trace.info("=> "+fsGlobal);
Trace.info("=> "+fsSFTE001ABean);
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte001");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte001/sfte001_de.jsp";
try { 
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	if(fsSFTE001ABean.getProduct()==null || fsSFTE001ABean.getProduct().trim().length()<=0) fsSFTE001ABean.setProduct("PROMPT");
	fsSFTE001ABean.obtain(session,request);
	fsSFTE001ABean.transport(fsGlobal);
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsSFTE001ABean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE001ABean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE001ABean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE001ABean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE001ABean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
