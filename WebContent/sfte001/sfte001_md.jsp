<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%
com.fs.dev.TheUtility smutil = new com.fs.dev.TheUtility("AUTH");
java.util.Map fs_productmap = (java.util.Map)session.getAttribute("PRODUCT_CATEGORY");
java.util.Map fs_progtypemap = (java.util.Map)session.getAttribute("PROGTYPE_CATEGORY");
java.util.Map fs_systemmap = (java.util.Map)session.getAttribute("SYSTEM_CATEGORY");
if(fs_progtypemap==null) {
	smutil.setDirectoryFromFolder(request,java.io.File.separator+"xml"+java.io.File.separator);
	smutil.setConfigfile("smart_config.xml");
	fs_progtypemap = smutil.getMapProperties("/root/progtype/row","id","value",true);
	if(fs_progtypemap!=null) session.setAttribute("PROGTYPE_CATEGORY",fs_progtypemap);
	java.util.Map fs_progtypes = new java.util.HashMap();
	fs_progtypes.put("","");
	fs_progtypes.putAll(fs_progtypemap);
	session.setAttribute("PROGTYPE_CATEGORIES",fs_progtypes);
}
if(fs_systemmap==null) {
	smutil.setDirectoryFromFolder(request,java.io.File.separator+"xml"+java.io.File.separator);
	smutil.setConfigfile("smart_config.xml");
	fs_systemmap = smutil.getMapProperties("/root/system/row","id","value",true);
	if(fs_systemmap!=null) session.setAttribute("SYSTEM_CATEGORY",fs_systemmap);
}
if(fs_productmap==null) {
	fs_productmap = smutil.createProduct(fsAccessor,smutil.getSection(),"sfte003");
	if(fs_productmap!=null) session.setAttribute("PRODUCT_CATEGORY",fs_productmap);
}
%>
