<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte001',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%if(!fsGlobal.bufferMode()||fsGlobal.insertMode()) session.removeAttribute("fsSFTE001Bean"); %>
<jsp:useBean id="fsSFTE001Bean" scope="session" class="com.fs.bean.SFTE001Bean"/>
<jsp:setProperty name="fsSFTE001Bean" property="*"/>
<%
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte001");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte001/sfte001_d.jsp";
boolean testing = true;
try { 
	fsSFTE001Bean.obtain(session,request);
	fsSFTE001Bean.transport(fsGlobal);
	java.util.Enumeration fsElements = fsSFTE001Bean.elements();
	if(fsElements!=null) { 
		com.fs.dev.TheUtility smutil = new com.fs.dev.TheUtility(fsGlobal.getFsSection());
		java.util.Map fs_progtypemap = (java.util.Map)session.getAttribute("PROGTYPE_CATEGORY");
		if(fs_progtypemap==null) {
			smutil.setDirectoryFromFolder(request,java.io.File.separator+"xml"+java.io.File.separator);
			smutil.setConfigfile("smart_config.xml");
			fs_progtypemap = smutil.getMapProperties("/root/progtype/row","id","value",true);
			if(fs_progtypemap!=null) session.setAttribute("PROGTYPE_CATEGORY",fs_progtypemap);
		}
		while(fsElements.hasMoreElements()) {
			com.fs.bean.SFTE001ABean fsdSFTE001ABean = (com.fs.bean.SFTE001ABean)fsElements.nextElement();
			if(fsdSFTE001ABean!=null) {
				String desc = (String)fs_progtypemap.get(fsdSFTE001ABean.getProgtype());
				fsdSFTE001ABean.setProgtypedesc(desc);
			}
		}
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsPager.setRows(fsSFTE001Bean.size());
fsGlobal.adjustPage(fsPager);
fsSFTE001Bean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE001Bean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE001Bean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE001Bean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE001Bean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
