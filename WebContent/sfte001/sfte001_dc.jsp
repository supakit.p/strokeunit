<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte001',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE001Bean" scope="session" class="com.fs.bean.SFTE001Bean"/>
<jsp:useBean id="fsSFTE001ABean" scope="request" class="com.fs.bean.SFTE001ABean"/>
<jsp:setProperty name="fsSFTE001ABean" property="*"/>
<%
if(FileUpload.isMultipartContent(request)) {
	RequestDispatcher rd = application.getRequestDispatcher("/sfte001/sfte001_dmc.jsp");
	rd.forward(request, response);
	return;
}
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte001");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte001/sfte001_de.jsp";
try { 
	int fsRowid = fsGlobal.parseRowid();
	if(fsRowid>0) {
		fsSFTE001ABean = (com.fs.bean.SFTE001ABean)fsSFTE001Bean.getBeanAt(fsRowid-1);
		session.setAttribute("fsSFTE001ABean",fsSFTE001ABean);
	}
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	if(fsSFTE001ABean.getProduct()==null || fsSFTE001ABean.getProduct().trim().length()<=0) fsSFTE001ABean.setProduct("PROMPT");
	fsSFTE001ABean.obtain(session,request);
	fsSFTE001ABean.transport(fsGlobal);
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsSFTE001ABean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE001ABean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE001ABean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE001ABean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE001ABean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
