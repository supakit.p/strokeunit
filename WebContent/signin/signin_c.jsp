<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsLogonBean" scope="request" class="com.fs.dev.auth.SignonBean"/>
<jsp:setProperty name="fsLogonBean" property="*"/>
<%
Trace.info("signin "+request.getContextPath()+" : "+request.getQueryString());
java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
if(fs_map!=null) fsLogonBean.obtain(fs_map);
Trace.info("fsLogonBean = "+fsLogonBean);
session.setAttribute("fsLogonBean.user",fsLogonBean.getUser());
session.setAttribute("fsLogonBean.password",fsLogonBean.getPassword());
fsGlobal.setFsVar("fsAddress",request.getRemoteAddr());
fsGlobal.setFsSection("AUTH");
JSONObject result = new JSONObject();
if(fsLogonBean.getUser().trim().equals("")) {
	String fs_msg = "User or Password is undefined";
	fs_msg = ErrorConfig.getError("5001",fs_msg);
	fsGlobal.setFsMessage(fs_msg);
	JSONHeader header = new JSONHeader();
	header.setModel("signin");
	header.setMethod("login");
	header.setErrorflag("Y");
	header.setErrorcode("5001");
	header.setErrordesc(fs_msg);
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
String signon = (String)GlobalVariable.getVariable("VALIDATE_SIGNON");
if(!(signon!=null && signon.equalsIgnoreCase("false"))) {
	int maxfailure = 3;
	String maxfail = (String)GlobalVariable.getVariable("MAXFAIL_SIGNON");
	if((maxfail!=null) && !maxfail.trim().equals("")) {
		try {
			maxfailure = Integer.parseInt(maxfail);
		} catch(Exception ex) { maxfailure = 3; }
	}
	Integer counter = (Integer)session.getAttribute(fsLogonBean.getUser()+"_signoncounter");
	if(counter==null) counter = new Integer(0);
	if(counter.intValue()>=maxfailure) {
		String fs_msg = "Signon failure over "+maxfailure+" times.";
		fs_msg = ErrorConfig.getError("5003",fs_msg,new String[] { ""+maxfailure });
		fsGlobal.setFsMessage(fs_msg);
		JSONHeader header = new JSONHeader();
		header.setModel("signin");
		header.setMethod("login");
		header.setErrorflag("Y");
		header.setErrorcode("5003");
		header.setErrordesc(fs_msg);
		result.put("head",header);
		out.println(result.toJSONString());
		return;		
	}
	try {
		boolean passed = false;
		String fs_errcode = "5001";
		String fs_msg = "User or Password is incorrect";
		fs_msg = ErrorConfig.getError("5001",fs_msg);
		String fs_password = fsLogonBean.getPassword();
		fsGlobal.setFsVar("fsUser",fsLogonBean.getUser());
		fsGlobal.setFsVar("fsBranch",fsLogonBean.getBranch());
		fsGlobal.setFsProg("signin");
		fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
		BeanTransport fsTransport = TheTransportor.transport(fsGlobal,fsLogonBean);
		if(fsTransport!=null) fsGlobal.setSuccess(fsTransport.isSuccess());
		boolean found = false;
		if(fsLogonBean.effectedTransactions()>0) {
			found = true;
			if(fs_password.equals(fsLogonBean.getPassword())) {
				passed = true;
			} else {
				com.fs.dev.auth.PasswordLibrary encp = new com.fs.dev.auth.PasswordLibrary();
				String password = encp.encrypt(fs_password);
				Trace.debug("password encrypt : "+fs_password+" ("+password+") = "+fsLogonBean.getPassword());
				if(password.equals(fsLogonBean.getPassword())) {
					passed = true;
				} else {
					fs_errcode = "5002";
					fs_msg = "User or Password is incorrect";
					fs_msg = ErrorConfig.getError("5002",fs_msg);
				}
			}
			if(passed) {
				if(fsLogonBean.getActiveflag()!=null && !fsLogonBean.getActiveflag().equals("1")) {
					passed = false;
					fs_errcode = "5004";
					fs_msg = "You are not active, please contact administrator";
					fs_msg = ErrorConfig.getError("5004",fs_msg);
					if(fsLogonBean.getStatus().equals("P")) { //Pending
						fs_errcode = "5007";
						fs_msg = ErrorConfig.getError("5007",fs_msg);
					} else if(fsLogonBean.getStatus().equals("C")) { //Closed
						fs_errcode = "5008";
						fs_msg = ErrorConfig.getError("5008",fs_msg);
					}
				}
				if(fsLogonBean.getLockflag()!=null && fsLogonBean.getLockflag().equals("1")) {
					passed = false;
					fs_errcode = "5005";
					fs_msg = "You are locking, please contact administrator";
					fs_msg = ErrorConfig.getError("5005",fs_msg);
					if(fsLogonBean.getFailtime()!=null && fsLogonBean.getFailtime().trim().length()>0) {
						BeanUtility butil = new BeanUtility();
						long fs_failtime = butil.parseBigDecimal(fsLogonBean.getFailtime()).longValue();
						long fs_curtime = System.currentTimeMillis();
						long fs_difftime = fs_curtime - fs_failtime;
						if(fs_difftime<=180000) {
							fs_errcode = "5009";
							fs_msg = "Your User ID has been locked. Please contact administrator or wait and retry again after 3 minute.";
							fs_msg = ErrorConfig.getError("5009",fs_msg);
						} else {
							passed = true;
							com.fs.dev.auth.LockBean fsLockBean = new com.fs.dev.auth.LockBean();
							fsLockBean.setUser(fsLogonBean.getUser());
							fsLockBean.setLock("0");
							GlobalBean fsHandler = (GlobalBean)fsGlobal;
							fsHandler.setFsAction(GlobalBean.UPDATE_MODE);
							TheTransportor.transport(fsHandler,fsLockBean);
						}
					}
				}
			}
		}
		if(!passed) {
			if((counter.intValue()+1)>=maxfailure) {
				com.fs.dev.auth.LockBean fsLockBean = new com.fs.dev.auth.LockBean();
				fsLockBean.setUser(fsLogonBean.getUser());
				fsLockBean.setLock("1");
				GlobalBean fsHandler = (GlobalBean)fsGlobal;
				fsHandler.setFsAction(GlobalBean.UPDATE_MODE);
				TheTransportor.transport(fsHandler,fsLockBean);
			}
			session.setAttribute(fsLogonBean.getUser()+"_signoncounter",new Integer(counter.intValue()+1));	
			fsLogonBean.setPassword(fs_password);
			fsGlobal.setFsMessage(fs_msg);
			Tracker.track(fsGlobal);
			JSONHeader header = new JSONHeader();
			header.setModel("signin");
			header.setMethod("login");
			header.setErrorflag("Y");
			header.setErrorcode(fs_errcode);
			header.setErrordesc(fs_msg);
			result.put("head",header);
			out.println(result.toJSONString());
			return;
		}
		session.removeAttribute(fsLogonBean.getUser()+"_signoncounter"); 
	} catch(Exception ex) {
		fsGlobal.setThrowable(ex);	
		JSONHeader header = new JSONHeader();
		header.setModel("signin");
		header.setMethod("login");
		header.setErrorflag("Y");
		header.setErrorcode("1");
		header.setErrordesc(fsGlobal.getFsMessage());
		result.put("head",header);
		out.println(result.toJSONString());
		return;
	}
}
if((fsLogonBean.getLogondate()==null) || fsLogonBean.getLogondate().equals("")) {
	BeanFormat formater = new BeanFormat();
	java.util.Date date = new java.util.Date();
	fsLogonBean.setLogondate(formater.formatDate(date,"dd/MM/yyyy"));
}
fsAccessor.setFsKey(session.getId());
fsLogonBean.assign(fsAccessor);
Trace.debug(fsLogonBean);
Trace.debug(fsAccessor);
fsGlobal.retain(request);
Tracker.trace(fsGlobal);
final GlobalBean fsAccessHandler = (GlobalBean)fsGlobal;
final com.fs.dev.auth.AccessBean access = new com.fs.dev.auth.AccessBean();
access.setAccessor(fsLogonBean.getUser());
new Thread() {
	public void run() {
		try { access.update(fsAccessHandler); }catch(Exception ex) { }
		this.stop();
	}
}.start();
session.setAttribute("fsLogonBean.securepassword",fsLogonBean.getPassword());
JSONHeader header = new JSONHeader();
header.setModel("signin");
header.setMethod("login");
header.setErrorflag("N");
header.setErrorcode("0");
header.setErrordesc(fsGlobal.getFsMessage());
result.put("head",header);
JSONObject body = new JSONObject();
body.put("userid",fsAccessor.getFsUser());
body.put("username",fsAccessor.getFsUserName());
body.put("usertype",fsAccessor.getFsUsertype());
body.put("department",fsAccessor.getFsDepartment());
body.put("division",fsAccessor.getFsDivision());
body.put("email",fsAccessor.getFsEmail());
body.put("role",fsAccessor.getFsGroups());
body.put("key",fsAccessor.getFsKey());
result.put("body",body);
out.println(result.toJSONString());
%>
