<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.dev.mail.*" %>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsActivateBean" scope="request" class="com.fs.bean.ActivateBean"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsResetPasswordBean" scope="request" class="com.fs.dev.auth.ResetPasswordBean"/>
<jsp:setProperty name="fsResetPasswordBean" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("as001");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
JSONHeader header = new JSONHeader();
header.setModel("as001");
header.setMethod("forgot_password");
try { 
	int fs_action = fsGlobal.parseAction();
	fsResetPasswordBean.forceObtain(fsGlobal);
	String fs_language = fsGlobal.getFsLanguage();
	String fs_lang = request.getParameter("language");
	if(fs_lang!=null && fs_lang.trim().length()>0) fs_language = fs_lang;
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) {
		fsResetPasswordBean.obtain(fs_map);
		if(fs_map.get("language")!=null) fs_language = (String)fs_map.get("language");
	}
	fsGlobal.setFsLanguage(fs_language);
	fsLabel.setLanguage(fs_language);
	if(fsResetPasswordBean.getEmail().trim().length()<=0) throw new BeanException("Email address is invalid",-8892,fs_language);
	BeanFormat format = new BeanFormat();		
	if(fsActivateBean.getActivateuser()==null || fsActivateBean.getActivateuser().trim().length()<=0) {
		fsActivateBean.setActivateuser(request.getRemoteAddr());
	}
	//String main_context = (String)com.fs.bean.util.GlobalVariable.getVariable("MAIN_CONTEXT");
	//if(main_context==null || main_context.trim().length()<=0) main_context = "ezprompt";
	String scheme = request.getScheme();             
	String serverName = request.getServerName(); 
	int serverPort = request.getServerPort(); 
	String aurl = scheme+"://"+serverName+(serverPort==80?"":":"+serverPort)+request.getContextPath();
	Object activateurl = GlobalVariable.getVariable("ACTIVATE_URL");
	if(activateurl!=null && activateurl.toString().trim().length()>0) {
		aurl = activateurl.toString().trim();
	}
	String fs_activatekey = fsActivateBean.createActivateKey();
	//String fs_activatelink = aurl+"/activate/activate.jsp?activatekey="+fs_activatekey;
	String fs_activatelink = aurl+"/rest/control/activate/"+fs_activatekey;
	fsActivateBean.setActivatetimes("1");
	fsActivateBean.setActivateremark(fsResetPasswordBean.getEmail());
	fsActivateBean.setActivatepage(aurl+"/index.jsp");
	fsActivateBean.setActivatelink(fs_activatelink);
	fsActivateBean.setSenddate(format.formatDate(new java.util.Date()));
	fsActivateBean.setSendtime(format.formatTime(new java.sql.Time(System.currentTimeMillis())));
	int rows = fsActivateBean.insert(fsGlobal); 
	if(rows>0) {
		java.util.Map map = null;
		java.sql.Connection connection = null;
		try {
			connection = DBConnection.getConnection(fsGlobal.getFsSection());
			map = (java.util.Map)XMLConfig.create(request).getConfigure("FORGOTPASSWORDMAIL",connection);
		} finally { 
			if(connection!=null) { try { connection.close(); }catch(Exception ex) {} }
		}				
		//java.util.Map map = (java.util.Map)XMLConfig.create(request).getConfigure("FORGOTPASSWORDMAIL");
		Trace.info("FORGOTPASSWORDMAIL = "+map);
		if(map!=null) {
			String fs_email = fsResetPasswordBean.getEmail();
			if(fs_email!=null && fs_email.trim().length()>0) {
				StringBuffer msg = new StringBuffer();
				msg.append("Dear, Requester.<br/>");
				msg.append("To confirm your password to be changed<br/>");
				msg.append("Please click link below or goto the location to activate<br/>");
				msg.append("<a href=\""+fs_activatelink+"\" target=\"_blank\">Confirm Reset Password</a><br/>");
				msg.append("<br/>Go to the location "+fs_activatelink+"<br/>");
				Trace.info("forgot password : "+fs_email+"("+fs_activatekey+")");
				SendMail mailer = new SendMail();
				mailer.obtain(map);
				mailer.setTo(fs_email);
				mailer.setMessage(msg.toString());
				mailer.start();
			}
		}		
		header.composeError("N", "0", "");
		result.put("head",header);
		out.println(result.toJSONString());
		return;
	}	
}catch(Exception ex) { 
	Trace.error(fsGlobal,ex);
	fsGlobal.setThrowable(ex);
	int errorCode = fsGlobal.parseFsErrorcode();
	header.composeError("Y", Integer.toString(errorCode==0?1:errorCode), fsGlobal.getFsMessage());
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
header.composeError("Y", "2", fsLabel.getText("notfound","Not Found"));
result.put("head",header);
out.println(result.toJSONString());
%>
