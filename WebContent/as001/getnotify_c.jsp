<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<c:if test="${fsScreen.config('as001',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("as001");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
JSONHeader header = new JSONHeader();
header.setModel("as001");
header.setMethod("getnotify");
try { 
	fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
	//create bean in order to obtain parameter from request or rest injection
	com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
	fsBean.addSchema("userid",java.sql.Types.VARCHAR,"userid");
	fsBean.addSchema("language",java.sql.Types.VARCHAR,"language");
	fsBean.obtainFrom(request); //assign variable from request
	fsBean.forceObtain(fsGlobal);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsBean.obtain(fs_map); //assign variable from rest
	fsBean.obtain(session,request);
	fsGlobal.setFsLanguage(fsBean.getString("language"));
	final JSONObject body = new JSONObject();
	final String fs_userid = fsBean.getString("userid");
	final String fs_language = fsBean.getString("language");
	final boolean eng = com.fs.dev.TheUtility.isEnglish(fs_language);
	fsLabel.setLanguage(fs_language);
	if(fs_userid !=null && fs_userid.trim().length()>0) {
		body.put("userid",fs_userid);
		com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
			//override method retrieve depend on action from fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
			public int retrieve(java.sql.Connection connection) throws Exception {
				int result = 0;
				String fs_site = null;
				KnSQL knsql = new KnSQL(this);
				knsql.append("select site,cardid,birthday ");
				knsql.append("from tuserinfo ");
				knsql.append("where userid = ?userid and inactive = '0' ");
				knsql.setParameter("userid",fs_userid);
				try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
					if(rs.next()) {
						result++;
						fs_site = rs.getString("site");
					}
				}
				if(result==0) throw new java.sql.SQLException("User not found","userid",-8895);
				if(result>0) {
					int inbox = 0;
					int outbox = 0;
					knsql.clear();
					knsql.append("select count(*) as counter,boxtype ");
					knsql.append("from tuserbox ");
					knsql.append("where userid = ?userid ");
					knsql.append("and readdate is null ");
					knsql.append("group by boxtype ");
					knsql.setParameter("userid",fs_userid);
					try(java.sql.ResultSet rs = knsql.executeQuery(connection)) {
						while(rs.next()) {
							String boxtype = rs.getString("boxtype");
							if("I".equals(boxtype)) {
								inbox = rs.getInt("counter");
							} else if("O".equals(boxtype)) {
								outbox = rs.getInt("counter");
							}
						}
					}
					body.put("badges",outbox);
					body.put("inbox",inbox);
				}
				return result;
			}	
		};
		TheTransportor.transport(fsGlobal,fsExecuter);
		if(fsExecuter.effectedTransactions()>0) {
			header.composeError("N", "0", "");
			result.put("head",header);
			result.put("body",body);
			out.println(result.toJSONString());
			return;
		}
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	int errorCode = fsGlobal.parseFsErrorcode();
	header.composeError("Y", Integer.toString(errorCode==0?1:errorCode), fsGlobal.getFsMessage());
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
header.composeError("Y", "2", fsLabel.getText("notfound","Not Found"));
result.put("head",header);
out.println(result.toJSONString());
%>
