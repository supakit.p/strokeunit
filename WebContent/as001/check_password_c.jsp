<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<c:if test="${fsScreen.config('as001',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE005CBean" scope="request" class="com.fs.bean.SFTE005CBean"/>
<jsp:setProperty name="fsSFTE005CBean" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("as001");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
JSONHeader header = new JSONHeader();
header.setModel("as001");
header.setMethod("check_password");
try { 
	fsGlobal.setFsAction("check");
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	fsSFTE005CBean.forceObtain(fsGlobal);
	fsSFTE005CBean.obtainFrom(request);
	String fs_language = fsGlobal.getFsLanguage();
	String fs_lang = request.getParameter("language");
	if(fs_lang!=null && fs_lang.trim().length()>0) fs_language = fs_lang;
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) {
		fsSFTE005CBean.obtain(fs_map);
		if(fs_map.get("language")!=null) fs_language = (String)fs_map.get("language");
	}
	fsGlobal.setFsLanguage(fs_language);
	fsLabel.setLanguage(fs_language);
	fsSFTE005CBean.obtain(session,request);
	if(fsSFTE005CBean.getUserid()==null || fsSFTE005CBean.getUserid().trim().length()<=0) {
		fsSFTE005CBean.setUserid(fsAccessor.getFsUser());
	}	
	fsSFTE005CBean.setCheckflag("0");
	String fs_userpassword = fsSFTE005CBean.getUserpassword();
	if(fs_userpassword==null || fs_userpassword.trim().length()<=0) {
		throw new BeanException("Password is undefined",-8995,fsGlobal.getFsLanguage());
	}
	fsSFTE005CBean.transport(fsGlobal);
	if(fsSFTE005CBean.effectedTransactions()>0) {
		header.composeError("N", "0", "");
		result.put("head",header);
		out.println(result.toJSONString());
		return;
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	int errorCode = fsGlobal.parseFsErrorcode();
	header.composeError("Y", Integer.toString(errorCode==0?1:errorCode), fsGlobal.getFsMessage());
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
header.composeError("Y", "2", fsLabel.getText("notfound","Not Found"));
result.put("head",header);
out.println(result.toJSONString());
%>
