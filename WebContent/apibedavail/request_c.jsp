<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("BedAvail");
    header.setMethod("request");
    try {
        Trace.info("############### BedAvail/request ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
//        fsBean.addSchema("bedId", java.sql.Types.VARCHAR, "bedId");
//        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
//        final String fs_bedId = fsBean.getString("bedId");
//        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", java.util.Locale.US);
        final String getDate = df.format(new Date());
        if (fs_data != null && fs_data.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;

                    TheStrokeUnit su = new TheStrokeUnit();

                    String bedId = json.getString("bedId");
                    KnSQL knsql = new KnSQL(this);
                    knsql.append("SELECT Mw_BedAvail.BedId ,Mw_BedAvail.BedNo , Mw_BedAvail.IsAvail ");
                    knsql.append("FROM Mw_BedAvail ");
//                    knsql.append("WHERE Mw_BedAvail.IsAvail ='1' AND Mw_BedAvail.IsActive = '1' ");

                    knsql.append("WHERE Mw_BedAvail.IsActive = '1' AND NOT Mw_BedAvail.BedId IN ");
                    knsql.append(" ( SELECT Tw_PatientInfo.BedId ");
                    knsql.append(" FROM Tw_PatientInfo ");
                    knsql.append(" WHERE Tw_PatientInfo.StatusCode IN ('N') AND Tw_PatientInfo.IsActive = '1' AND Tw_PatientInfo.BedId IS NOT NULL ) ");

//                    String fs_bedId = json.getString("bedId");
//                    KnSQL knsql = new KnSQL(this);
//                    knsql.append("SELECT Mw_BedAvail.BedId bedId,Mw_BedAvail.BedNo bedNo ");
//                    knsql.append("FROM Mw_BedAvail ");
//                    knsql.append("WHERE NOT Mw_BedAvail.BedId IN ");
//                    knsql.append("(SELECT Tw_PatientInfo.BedId ");
//                    knsql.append("FROM Tw_PatientInfo ");
//                    knsql.append("LEFT JOIN Tw_Discharge ON Tw_Discharge.DCId = Tw_PatientInfo.DCId ");
//                    knsql.append("WHERE Tw_Discharge.DCDate >= ?getDate ) ");
                    if (bedId != null && bedId.trim().length() > 0) {
                        knsql.append("OR Mw_BedAvail.BedId = ?bedId ");
                        knsql.setParameter("bedId", bedId);
                    }
//                    knsql.setParameter("getDate", getDate);
                    HashMap<String, String> configFieldBA = new HashMap();
                    configFieldBA.put("BedId", "bedId");
                    configFieldBA.put("BedNo", "bedNo");
                    configFieldBA.put("IsAvail", "isAvailable");

                    org.json.JSONArray jsonBedAvail = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldBA).toString());
                    result += jsonBedAvail.length();

                    body.put("listBedAvail", jsonBedAvail);
                    Trace.info("body : " + body);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
