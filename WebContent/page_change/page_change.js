var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("page_change"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
		setupComponents();
		setupAlertComponents();
		$("#userpassword").blur(function() { 
			$("#matchpassword_alert").hide();
		});
		$("#confirmpassword").blur(function() {
			$("#matchpassword_alert").hide();
		});
}
function setupComponents() {
	$("#savebutton").click(function() { 
		save();
		return false;
	});
}
function clearingFields() {
}
function validForm() {
	clearAlerts();
	$("#matchpassword_alert").hide();
	var validator = null;
	if($.trim($("#oldpassword").val())=="") {
		$("#oldpassword").parent().addClass("has-error");
		$("#oldpassword_alert").show();
		if(!validator) validator = "oldpassword";
	}
	if($.trim($("#userpassword").val())=="") {
		$("#userpassword").parent().addClass("has-error");
		$("#userpassword_alert").show();
		if(!validator) validator = "userpassword";
	}
	if($.trim($("#confirmpassword").val())=="") {
		$("#confirmpassword").parent().addClass("has-error");
		$("#confirmpassword_alert").show();
		if(!validator) validator = "confirmpassword";
	}
	if($("#userpassword").val()!=$("#confirmpassword").val()) {
		$("#confirmpassword").parent().addClass("has-error");
		$("#matchpassword_alert").show();
		if(!validator) validator = "matchpassword";
	}	
	if(validator) {
		var matching = validator=="matchpassword";
		if(matching) validator = "confirmpassword";
		$("#"+validator).focus();
		setTimeout(function() { 
			$("#"+validator).parent().addClass("has-error");
			if(!matching) $("#"+validator+"_alert").show();
		},100);
		return false;
	}
	/*
	if($.trim($("#oldpassword").val())=="") {
		alertmsg("QS0116","Old Password is undefined");
		return false;
	}
	if($.trim($("#userpassword").val())=="") {
		alertmsg("QS0117","New Password is undefined");
		return false;
	}
	if($.trim($("#confirmpassword").val())=="") {
		alertmsg("QS0118","Confirm Password is undefined");
		return false;
	}
	if($("#userpassword").val()!=$("#confirmpassword").val()) {
		alertmsg("QS0119","New Password does not match to Confirm Password");
		return false;
	}
	*/
	return true;
}
function save(aform) {
		if(!aform) aform = fsentryform;
		//alert($(aform).serialize());
		if(!validForm()) return false;
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "page_change_de_c.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					successbox(function() { 
						clearingFields(); 
						try {  window.parent.goHome(); } catch(ex) { }
					});					
				}
			});
		});
		return false;
}

