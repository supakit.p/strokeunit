<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE005CBean" scope="session" class="com.fs.bean.SFTE005CBean"/>

	<div id="entrylayer" class="entry-layer">
		<form id="fsentryform" name="fsentryform" method="post" action="page_change_de_c.jsp">	
			<input type="hidden" name="fsAction" value="update"/>
			<input type="hidden" name="fsAjax" value="true"/>
			<input type="hidden" name="fsDatatype" value="json"/>
			<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
			<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
			<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>	
			<input type="hidden" id="userid" name="userid" value="${fsSFTE005CBean.userid}"/>
			<div class="row portal-area sub-entry-layer">
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="oldpassword_label" tagclass="control-label" required="true">Old Password</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<input type="password" class="form-control input-md alert-input" id="oldpassword" name="oldpassword" placeholder="Old Password" autocomplete="off" size="8" />
							<div id="oldpassword_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('oldpassword_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="userpassword_label" tagclass="control-label" required="true">New Password</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<input type="password" class="form-control input-md alert-input" id="userpassword" name="userpassword" placeholder="New Password" autocomplete="off" size="8" data-toggle="tooltip" title="Password can combine with alphabets and numeric sign not over 8 characters"/>
							<div id="userpassword_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('userpassword_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="confirmpassword_label" tagclass="control-label" required="true">Confirm Password</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<input type="password" class="form-control input-md alert-input" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password" autocomplete="off" size="8" data-toggle="tooltip" title="Password can combine with alphabets and numeric sign not over 8 characters"/>
							<div id="confirmpassword_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('confirmpassword_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right"></div>
						<div class="col-md-6 col-height">
							<div id="matchpassword_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('matchpassword_alert','New Password does not match to Confirm Password')}</div>
						</div>
				</div>
				<div class="row row-height">
					<div class="col-md-4 pull-right text-right" style="margin-right: 10px;">
						<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save')}"/>
					</div>
				</div>
			</div>
		</form>
	</div>