var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("sfte003"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
		setupComponents();
		setupAlertComponents();
}
function setupComponents() {
		$("#searchbutton").click(function(evt) { 
			search(); 
			return false;
		});
		$("#insertbutton").click(function(evt) { 
			insert(); 
			return false;
		});
		$("#savebutton").click(function() { 
			save();
			return false;
		});
		$("#cancelbutton").click(function() { 
			cancel();
			return false;
		});
		$("#uploadbutton").click(function() { 
			if(!validForm()) return false;
			$("#uploadiconfile").trigger("click");
		});
		$("#uploadiconfile").change(function() { 
			console.log("iconfile = "+$("#uploadiconfile").val());
			if($.trim($("#uploadiconfile").val())!="") {
				uploadIconFile();
			}
		});
}
function clearingFields() {
		clearAlerts();
		fsentryform.reset();
}
function search(aform) {
		if(!aform) aform = fssearchform;
		startWaiting();
		jQuery.ajax({
			url: "sfte003_c.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				searchComplete(transport,data);
			}
		});	
}
function searchComplete(xhr,data) {
		stopWaiting();
		$("#listpanel").html(data);
}
function insert() {
		clearingFields();
		fsentryform.fsDatatype.value = "json";
		fsentryform.fsAction.value = "enter";
		$("#iconfileimage").attr("src","../images/nomodule.png");
		$("input.ikeyclass",fsentryform).editor({edit:true});
		showPageEntry();
}
function showPageSearch() {
		$("#entrypanel").hide();
		$("#searchpanel").show();
}
function showPageEntry() {
		$("#entrypanel").show();
		$("#searchpanel").hide();
}
function submitRetrieve(rowIndex) {
		var aform = fslistform;
		aform.fsRowid.value = ""+rowIndex;
		aform.fsDatatype.value = 'json';
		//alert($(aform).serialize());
		startWaiting();
		jQuery.ajax({
			url: "sfte003_dc.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				prepareScreenToUpdate(fsentryform,data);
			}
		});	
}
function prepareScreenToUpdate(aform,data) {
		aform.fsDatatype.value = "text";
		aform.fsAction.value = "edit";
		try {
			var jsRecord = $.parseJSON(data);
			for(var p in jsRecord) {
				$("#"+p,aform).val(jsRecord[p]);
			}
			$("#verified").attr("checked","1"==jsRecord["verified"]).val("1");
			$("#centerflag").attr("checked","1"==jsRecord["centerflag"]).val("1");
			$("#iconfileimage").attr("src","../resources/module/"+jsRecord["iconfile"]);
		} catch(ex) { }
		$("input.ikeyclass",aform).editor({edit:false});
		showPageEntry();
}
function cancel() {
		confirmCancel(function() {
			clearingFields();
			showPageSearch();
		});
}
function validForm() {
	clearAlerts();
	var validator = null;
	if($.trim($("#product").val())=="") {
		$("#product").parent().addClass("has-error");
		$("#product_alert").show();
		if(!validator) validator = "product";
	}
	if($.trim($("#nameen").val())=="") {
		$("#nameen").parent().addClass("has-error");
		$("#nameen_alert").show();
		if(!validator) validator = "nameen";
	}
	if($.trim($("#nameth").val())=="") {
		$("#nameth").parent().addClass("has-error");
		$("#nameth_alert").show();
		if(!validator) validator = "nameth";
	}
	if(validator) {
		$("#"+validator).focus();
		setTimeout(function() { 
			$("#"+validator).parent().addClass("has-error");
			$("#"+validator+"_alert").show();
		},100);
		return false;
	}
	return true;
}
function save(aform) {
		if(!aform) aform = fsentryform;
		if(!validForm()) return false;
		var isInsertMode = aform.fsAction.value=='enter';
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte003_dc.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,				
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					if(isInsertMode) {
						successbox(function() { 
							//clearingFields(); 
							showPageSearch();
							fssearchform.fsPage.value = fslistform.fsPage.value;
							search();
						});					
					} else {
						showPageSearch();
						fssearchform.fsPage.value = fslistform.fsPage.value;
						search();
					}
				}
			});
		});
		return false;
}
function submitChapter(aform,index) {
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte003_c.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
			}
		});
}
function submitOrder(fsParams) {
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte003_cd.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: fsParams,
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
			}
		});
		return false;
}
function submitDelete(fsParams) {
		confirmDelete([fsParams[0]],function() {
			deleteRecord(fsParams);
		});
}
function deleteRecord(fsParams) {
		startWaiting();
		jQuery.ajax({
			url: "sfte003_dc.jsp",
			type: "POST",
			data: "fsAction=delete&fsDatatype=json&product="+fsParams[0],
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				showPageSearch();
				fssearchform.fsPage.value = fslistform.fsPage.value;
				search();
			}
		});	
}
function uploadIconFile(aform) {
	if(!aform) aform = uploadform;
	if(!validForm()) return false;
	$("#uploadproduct").val($("#product").val());
	startWaiting();	
	var fd = new FormData(aform);
	var xhr = jQuery.ajax({
		url: "iconupload.jsp",
		type: "POST",
		dataType: "html",
		data: fd,
		enctype: "multipart/form-data",
		processData: false, 
		contentType: false, 
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown); 
		},
		success: function(data,status,transport){ 
			console.log("response : "+transport.responseText);
			stopWaiting();
			var json = $.parseJSON($.trim(data));
			if(json && json["filename"]) {
				var imgsrc = "../resources/module/"+json["filename"];
				$("#iconfileimage").attr("src",imgsrc);
				$("#iconfile").val(json["filename"]);
			}
		}
	});	
}

