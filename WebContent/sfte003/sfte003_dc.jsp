<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte003',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE003Bean" scope="session" class="com.fs.bean.SFTE003Bean"/>
<jsp:useBean id="fsSFTE003ABean" scope="request" class="com.fs.bean.SFTE003ABean"/>
<jsp:setProperty name="fsSFTE003ABean" property="*"/>
<%
if(FileUpload.isMultipartContent(request)) {
	RequestDispatcher rd = application.getRequestDispatcher("/sfte003/sfte003_dmc.jsp");
	rd.forward(request, response);
	return;
}
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte003");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte003/sfte003_de.jsp";
try { 
	int fsRowid = fsGlobal.parseRowid();
	if(fsRowid>0) {
		fsSFTE003ABean = (com.fs.bean.SFTE003ABean)fsSFTE003Bean.getBeanAt(fsRowid-1);
		session.setAttribute("fsSFTE003ABean",fsSFTE003ABean);
	}
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	fsSFTE003ABean.obtain(session,request);
	fsSFTE003ABean.transport(fsGlobal);
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsSFTE003ABean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE003ABean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE003ABean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE003ABean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE003ABean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
