<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE003ABean" scope="session" class="com.fs.bean.SFTE003ABean"/>

	<div id="entrylayer" class="entry-layer">
		<form id="fsentryform" name="fsentryform" method="post" accept-charset="UTF-8">	
			<input type="hidden" name="fsAction" value="enter"/>
			<input type="hidden" name="fsAjax" value="true"/>
			<input type="hidden" name="fsDatatype" value="text"/>
			<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
			<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
			<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>	
			<div class="row portal-area sub-entry-layer">
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="product_label" tagclass="control-label" required="true">Product Code</fs:label>
						</div>
						<div class="col-md-2 col-height">
							<input type="text" class="form-control input-md ikeyclass alert-input" id="product" name="product" placeholder="" autocomplete="off" size="15" picture="(15)X"/>
							<div id="product_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('product_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="nameen_label" tagclass="control-label">Product Name(English)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="nameen" name="nameen" placeholder="" autocomplete="off" size="50"/>
							<div id="nameen_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('nameen_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="nameth_label" tagclass="control-label">Product Name(Thai)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="nameth" name="nameth" placeholder="" autocomplete="off" size="50"/>
							<div id="nameth_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('nameth_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="url_label" tagclass="control-label">URL or Context</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md" id="url" name="url" placeholder="" autocomplete="off" size="80"/>
							<div id="url_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('url_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-heighter">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="seqno_label" tagclass="control-label" required="false">Sequence</fs:label>
						</div>
						<div class="col-md-2 col-height">
							<fs:int tagclass="form-control input-md alert-input sequenceno" id="seqno" name="seqno"> </fs:int>
							<div id="seqno_alert" role="alert" class="has-error seqno-alert" style="display:none;">${fsLabel.getText('seqno_alert','You can not leave this empty')}</div>
						</div>
				</div>				
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">							
						</div>
						<div class="col-md-8 col-height radio my-radio currenting-field">
							<table><tr><td>
								<input type="checkbox" class="form-control input-md" id="centerflag" name="centerflag" value="1"/>
							</td><td>
								<fs:label tagclass="lclass control-label" tagid="centerflag_label" for="centerflag">Reserved for center setting</fs:label>
							</td></tr></table>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">							
						</div>
						<div class="col-md-8 col-height radio my-radio currenting-field">
							<table><tr><td>
								<input type="checkbox" class="form-control input-md" id="verified" name="verified" value="1"/>
							</td><td>
								<fs:label tagclass="lclass control-label" tagid="verified_label" for="verified">Verify Product Permission</fs:label>
							</td></tr></table>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="iconfile_label" tagclass="control-label">Icon File</fs:label>
						</div>
						<div class="col-md-3 col-height" id="iconfilelayer">
							<input type="hidden" class="form-control input-md" id="iconfile" name="iconfile" />
							<input type="button" id="uploadbutton" class="btn btn-base btn-sm" value="${fsLabel.getText('uploadbutton','Upload')}"/>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-9 col-height pull-right">
							<img id="iconfileimage" width="128px" height="128px" src="../images/nomodule.png" alt=""/>
						</div>
				</div>
				<div class="row row-height">
					<div class="col-md-4 pull-right text-right" style="margin-right: 10px;">
						<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save')}"/>
						&nbsp;&nbsp;
						<input type="button" id="cancelbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('cancelbutton','Cancel')}"/>
					</div>
				</div>
			</div>
		</form>
		<div id="uploadlayer" style="display:none;">
			<form id="uploadform" name="uploadform" enctype="multipart/form-data">
				<input type="hidden" id="uploadproduct" name="product" />
				<div id="uploadiconlayer"><input type="file" id="uploadiconfile" name="iconfile" /></div>
			</form>
		</div>
	</div>