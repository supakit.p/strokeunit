<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte003',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE003Bean" scope="session" class="com.fs.bean.SFTE003Bean"/>
<jsp:useBean id="fsSFTE003ABean" scope="request" class="com.fs.bean.SFTE003ABean"/>
<jsp:setProperty name="fsSFTE003ABean" property="*"/>
<%
request.setCharacterEncoding("TIS-620"); //force thai file name
if(!FileUpload.isMultipartContent(request)) {
	fsGlobal.createResponseStatus(out, response, 9001, "Not supported format (multipart need)");
	return;
}
try {	
	String fs_temppath = request.getServletContext().getRealPath("")+java.io.File.separator+"uploaded"+java.io.File.separator+"temp";
	java.io.File fs_tempdir = new java.io.File(fs_temppath);
	if(!fs_tempdir.exists()) fs_tempdir.mkdirs();
	DefaultFileItemFactory fs_factory = new DefaultFileItemFactory();
	fs_factory.setRepository(fs_tempdir);
	fs_factory.setSizeThreshold(512);
	FileUpload fs_upload = new FileUpload(fs_factory);
	fs_upload.setSizeMax(1000000000L);
	java.util.List<FileItem> fs_items = fs_upload.parseRequest(request);	
	fsSFTE003ABean.obtain(fs_items);
	fsGlobal.obtain(fs_items);
	String resources_path = com.fs.dev.TheUtility.getResourcesPath(request);
	String loadpath = resources_path+java.io.File.separator+"module"+java.io.File.separator;
	java.io.File fdir = new java.io.File(loadpath);
	if(!fdir.exists()) fdir.mkdirs();
	boolean fs_found = false;
	for(java.util.Iterator fs_it = fs_items.iterator();fs_it.hasNext();) {
		FileItem fs_item = (FileItem) fs_it.next();
		if (!fs_item.isFormField()) {
			fs_found = true;			
			Trace.debug("file item : "+fs_item.getName());
			String filename = fs_item.getName();
			if(filename!=null && filename.trim().length()>0) {
				int index = fs_item.getName().lastIndexOf("\\");
				if(index<0) index = fs_item.getName().lastIndexOf(java.io.File.separator);
				if(index>0) filename = fs_item.getName().substring(index+1);
				fsSFTE003ABean.setIconfile(filename);
				loadpath += filename;
				java.io.File fs_uploadfile = new java.io.File(loadpath);
				fs_item.write(fs_uploadfile);
				Trace.debug("saving as : "+loadpath);
			}
		}
	}
} catch(Throwable ex) {
	Trace.error(ex);
	String msg = ex.getMessage()==null?ex.getClass().getName():ex.getMessage();
	fsGlobal.createResponseStatus(out, response, 9002, msg);
	return;
}
Trace.info("=> "+fsGlobal);
Trace.info("=> "+fsSFTE003ABean);
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte003");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte003/sfte003_de.jsp";
try { 
	int fsRowid = fsGlobal.parseRowid();
	if(fsRowid>0) {
		fsSFTE003ABean = (com.fs.bean.SFTE003ABean)fsSFTE003Bean.getBeanAt(fsRowid-1);
		session.setAttribute("fsSFTE003ABean",fsSFTE003ABean);
	}
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	fsSFTE003ABean.obtain(session,request);
	fsSFTE003ABean.transport(fsGlobal);
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsSFTE003ABean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE003ABean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE003ABean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE003ABean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE003ABean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
