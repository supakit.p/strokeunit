<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.menu.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSider" scope="request" class="com.fs.bean.ExecuteBean"/>
<%
	boolean fsIsAjax = fsGlobal.isAjax();
	boolean fsIsJSON = fsGlobal.isJson();
	boolean fsIsJSONData = fsGlobal.isJsondata();
	boolean fsIsXML = fsGlobal.isXml();
	boolean fsIsXMLData = fsGlobal.isXmldata();
	fsGlobal.setFsProg("side_menu");
	fsGlobal.setFsSection("AUTH");
	fsGlobal.obtain(session);
	fsGlobal.obtain(fsAccessor);
	String fs_forwarder = "/main/side_menu_view.jsp";
	java.util.Map<String,String> fs_prodmap = new java.util.HashMap<String,String>();
	try { 
		String fs_userid = PageUtility.getParameter(request,"userid");
		if(fs_userid==null || fs_userid.trim().length()<=0) fs_userid = fsAccessor.getFsUser();
		if(fs_userid!=null && fs_userid.trim().length()>0) {
			fsGlobal.setFsAction(GlobalBean.COLLECT_MODE);
			String fs_site = fsAccessor.getFsSite();
			if(fs_site!=null && fs_site.trim().length()>0) {
				String fs_headsite = fsAccessor.getFsHeadSite();
				ExecuteBean fsExecuter = new ExecuteBean();
				KnSQL exsql = fsExecuter.getKnSQL();
				exsql.append("select distinct product from tcompprod where site = ?site ");
				if(fs_headsite!=null && fs_headsite.trim().length()>0) {
					exsql.append("or site = ?headsite ");
					exsql.setParameter("headsite",fs_headsite);
				}
				exsql.setParameter("site",fs_site);
				fsExecuter.transport(fsGlobal);
				java.util.Enumeration ens = fsExecuter.elements();
				if(ens!=null) {
					while(ens.hasMoreElements()) {
						com.fs.bean.EntityBean fsElement = (com.fs.bean.EntityBean)ens.nextElement();
						String product = fsElement.getString("product");
						if(product!=null && product.trim().length()>0) {
							fs_prodmap.put(product,product);
						}
					}
				}
			}
			KnSQL knsql = fsSider.getKnSQL();
			knsql.append("select tprod.url,tprod.verified,tprog.product,tprog.programid,");
			knsql.append("tprog.progname,tprog.prognameth,tprog.iconstyle,");
			knsql.append("tgroup.seqno as grpno,tgroup.groupname,tgroup.nameen,tgroup.nameth,");
			knsql.append("tgroup.iconstyle as groupstyle,tproggrp.seqno as prgno ");
			knsql.append("from tprod,tprog,tproggrp,tusergrp,tgroup ");
			knsql.append("where tusergrp.userid = ?userid ");
			knsql.append("and tusergrp.groupname = tproggrp.groupname ");
			knsql.append("and tproggrp.programid = tprog.programid ");
			knsql.append("and tproggrp.groupname = tgroup.groupname ");
			knsql.append("and tprog.product = tprod.product ");
			knsql.append("order by grpno,groupname,prgno,programid ");
			knsql.setParameter("userid",fs_userid);
			fsSider.transport(fsGlobal);
			if(fsSider.effectedTransactions()>0) {
				fs_forwarder = "/main/side_menu_view.jsp";
			}
		}
	}catch(Exception ex) { 
		Trace.error(fsAccessor,ex);
		fsGlobal.setThrowable(ex);
		if(fsIsAjax) {
			fsGlobal.createResponseStatus(out, response);
			return;
		}
	}
	fsSider.obtain(session,request);
	if(fsIsJSONData) {
		out.print(fsSider.toJSONData("rows"));
		return;
	}
	if(fsIsJSON) {
		out.print(fsSider.toJSON());
		return;
	}
	if(fsIsXMLData) {
		out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
		out.print(fsSider.toXMLDatas());
		return;
	}
	if(fsIsXML) {
		out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
		out.print(fsSider.toXML());
		return;
	}
	boolean fs_thai = fsScreen.isLanguageTH(request, response);
	java.util.Map<String,com.fs.bean.EntityBean> fs_sidermap = new OrderedHashtable<String,com.fs.bean.EntityBean>();
	java.util.Map<String,java.util.List<com.fs.bean.EntityBean>> fs_siderlist = new OrderedHashtable<String,java.util.List<com.fs.bean.EntityBean>>();
	request.setAttribute("fsSiderMap",fs_sidermap);
	request.setAttribute("fsSiderList",fs_siderlist);
	java.util.Enumeration en = fsSider.elements();
	if(en!=null) {
		for(;en.hasMoreElements();) {
			com.fs.bean.EntityBean fsElement = (com.fs.bean.EntityBean)en.nextElement();
			String iconstyle = fsElement.getString("iconstyle");
			String groupstyle = fsElement.getString("groupstyle");
			if(iconstyle==null || iconstyle.trim().length()<=0) { fsElement.setString("iconstyle","fa fa-desktop"); }
			if(groupstyle==null || groupstyle.trim().length()<=0) { fsElement.setString("groupstyle","fa fa-tasks"); }
			String groupname = fsElement.getString("groupname");
			if(fs_sidermap.get(groupname)==null) fs_sidermap.put(groupname,fsElement);
			java.util.List<com.fs.bean.EntityBean> fs_list = (java.util.List<com.fs.bean.EntityBean>)fs_siderlist.get(groupname);
			if(fs_list==null) {
				fs_list = new java.util.ArrayList<com.fs.bean.EntityBean>();
				fs_siderlist.put(groupname,fs_list);
			}
			fs_list.add(fsElement);
		}
	}
	//build menu tree in order to assign permissions
	String menutitle = (String)GlobalVariable.getVariable("MENU_TREE_TITLE");
	if(menutitle == null || menutitle.trim().length() <= 0) menutitle = "Programs Listing";
	MenuTree tree = new MenuTree(menutitle);
	tree.setUser(fsAccessor.getFsUser());
	tree.setSite(fsAccessor.getFsSite());
	fsAccessor.setMenuTree(tree);
	java.util.Map<String,String> fs_permit = new java.util.HashMap<String,String>();
	fs_permit.put("all","true");
	fs_permit.put("insert","true");
	fs_permit.put("update","true");
	fs_permit.put("delete","true");
	fs_permit.put("retrieve","true");
	fs_permit.put("import","true");
	fs_permit.put("export","true");
	fs_permit.put("launch","true");
	java.util.Map<String,String> fs_defer = new java.util.HashMap<String,String>();
	fs_defer.put("pmte039","pmte020");
	String fs_defer_program = (String)GlobalVariable.getVariable("DEFER_PROGRAM");
	if(fs_defer_program!=null && fs_defer_program.trim().length()>0) {
		String[] fs_programs = fs_defer_program.split(",");
		if(fs_programs!=null) {
			for(String prog : fs_programs) {
				if(prog!=null && prog.trim().length()>0) {
					String[] progs = prog.split("=");
					if(progs.length==2) {
						String source = progs[0];
						String target = progs[1];
						fs_defer.put(source,target);
					}
				}
			}
		}
	}
	for(java.util.Iterator<String> it = fs_sidermap.keySet().iterator();it.hasNext();) {
		String groupname = it.next();
		com.fs.bean.EntityBean groupbean = fs_sidermap.get(groupname);
		String description = fs_thai?groupbean.getString("nameth"):groupbean.getString("nameen");
		MenuItemInterface menu = tree.addItem(tree.createMenuItem(description));
		java.util.List<com.fs.bean.EntityBean> fs_list = fs_siderlist.get(groupname);
		if(fs_list!=null) {
			for(com.fs.bean.EntityBean eb : fs_list){ 
				String verify = eb.getString("verified");
				String product = eb.getString("product");
				String programid = eb.getString("programid");
				//System.out.println(product+" = "+programid+" : "+verify);
				String progname = fs_thai?eb.getString("prognameth"):eb.getString("progname");
				MenuItemInterface item = tree.createMenuItem(progname, programid, groupname);
				item.getPermits().putAll(fs_permit);
				menu.addItem(item);
				if("1".equals(verify)) {
					if(fs_prodmap.get(product)==null) {
						item.getPermits().put("launch","false");
					}
				}
				//System.out.println(programid+" = "+item.getPermits());
				String fs_defer_id = fs_defer.get(programid);
				if(fs_defer_id!=null) {
					MenuItemInterface fs_item = tree.createMenuItem(progname, fs_defer_id, groupname);
					fs_item.getPermits().putAll(fs_permit);
					menu.addItem(fs_item);				
					if("1".equals(verify)) {
						if(fs_prodmap.get(product)==null) {
							fs_item.getPermits().put("launch","false");
						}
					}
				}
			}
		}
	}
	tree.addItem(tree.createMenuItem("Change Password", "page_change")).getPermits().putAll(fs_permit);
	tree.addItem(tree.createMenuItem("User Profile", "page_profile")).getPermits().putAll(fs_permit);
	String progs = (String)GlobalVariable.getVariable("ADDITION_APPLICATION");
	if(progs!=null && progs.trim().length()>0) {
		String[] prgs = progs.split(",");
		if(prgs!=null) {
			for(String prg : prgs) {
				tree.addItem(tree.createMenuItem(prg, prg)).getPermits().putAll(fs_permit);			
			}
		}
	}
	System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	fsAccessor.startSaveMenuTree();
	fs_prodmap.clear(); fs_prodmap = null;
	RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
	rd.forward(request, response);
%>
