<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
				<div id="mainmenuitem" class="navbar-header">					
					<div id="mainmenu" class="menu-class" style="display:none;">
						<a href="javascript:void(0)" class="dropdown-toggle active-trigger" data-toggle="dropdown" id="mainmenutrigger" title="Menu"><em class="fa fa-bars"></em></a>
					</div>
				</div>				
				<div id="homemenuitem" class="navbar-header">
					<div id="homelayer" class="home" style="display:none;"><a href="javascript:void(0)" id="homemenutrigger" title="Home" onclick="goHome()"><em class="fa fa-home"></em></a></div>			
				</div>				
				<div id="productmenuitem" class="navbar-header">
					<div id="productlayer" class="product"><a href="home.jsp" id="intromenutrigger"><img id="productimage" src="images/app_logo.png" width="40" height="40" alt="" style="margin-top:-5px;"/></a></div>			
				</div>				
				<ul id="recentmenuitemlist" class="nav navbar-nav navbar-left navbar-user">			
					<li class="dropdown user-dropdown" id="recentmenuitem">
						<a href="javascript:void(0)" id="recent_linker" class="dropdown-toggle" data-toggle="dropdown" title="Advance">
							<span id="programtitle">Car Loan</span>
							<strong class="caret" id="recentcaret" style="display:none;"></strong></a>
						<ul class="dropdown-menu" id="recentmenulist">							
						</ul>
					</li>		
				</ul>
				<ul id="navmenuitem" class="nav navbar-nav navbar-right navbar-user">			
					<li class="dropdown user-dropdown" id="usermenuitem" style="display:none;">
						<a href="javascript:void(0)" id="accessor_linker" class="dropdown-toggle" data-toggle="dropdown" title="My Information"><span id="accessor_label">Administrator</span><strong class="caret"></strong></a>
						<ul class="dropdown-menu">							
							<li><a href="javascript:void(0)" onclick="profileClick()"><em class="fa fa-user"></em><span id="profile_label"> <%=fsLabel.getText("profile_label","Profile")%></span></a></li>
							<%if(com.fs.dev.TheUtility.isSignInAuthenticated(fsAccessor)) {%>
							<li><a href="javascript:void(0)" onclick="changeClick()"><em class="fa fa-lock"></em><span id="changepwd_label"> <%=fsLabel.getText("changepwd_label","Change Password")%></span></a></li>
							<%} %>
							<li class="divider"></li>
							<li><a href="javascript:void(0)" onclick="logOut()"><em class="fa fa-power-off"></em><span id="logout_label"> <%=fsLabel.getText("logout_label","Log Out")%></span></a></li>
						</ul>
					</li>		
					<li class="dropdown user-dropdown language-menu-item" id="languagemenuitem">
						<a href="javascript:void(0)" id="languagemenuitemlink" class="dropdown-toggle" data-toggle="dropdown">	<img id="languageimage" alt="" src="img/lang/<%=fsScreen.getDefaultLanguage(request,response)%>.png" width="20px" title="Language"><strong class="caret"></strong></a>
						<ul class="dropdown-menu">
							<li><a href="javascript:void(0)" onclick="$('#languageimage').attr('src','img/lang/EN.png'); fs_switchLanguage('EN',true);"><img alt="" src="img/lang/EN.png" width="20px" title="English"/><span id="englishlanguage" style="padding-left: 15px;">English</span></a></li>
							<li><a href="javascript:void(0)" onclick="$('#languageimage').attr('src','img/lang/TH.png'); fs_switchLanguage('TH',true);"><img alt="" src="img/lang/TH.png" width="20px" title="Thai"/><span id="thailanguage" style="padding-left: 15px;">Thai</span></a></li>
						</ul>
					</li>
					<li class="dropdown user-dropdown" id="favormenuitem" style="display:none;">
						<a href="javascript:void(0)" id="favormenuitemlink" class="dropdown-toggle" data-toggle="dropdown">
							<em class="fa fa-th"></em>&nbsp;
						</a>
						<ul class="dropdown-menu favor-dropdown-menu">
							<li>
								<div id="favornewitemlayer" class="favor-menu-icon" style="display:none;">
									<a href="javascript:void(0)" id="favorcancelitem" class="favor-cancel-item" title="Close New Favorite"><em class="fa fa-times-circle"></em></a>
									<select id="favorprogitem">
									</select>
									<a href="javascript:void(0)" id="favornewitem" class="favor-new-item" title="Add New Favorite"><em class="fa fa-plus"></em></a>
								</div>
								<div id="favorbarmenu" class="navbox-tiles"></div>
								<div id="favorcoverbarmenu" class="favor-menu-cover" style="display:none;">
								</div>
							</li>
						</ul>
					</li>
				</ul>

				<div class="navbar-header navbar-right">
					<div id="loginlayer" class="my-control-menu login"><a href="javascript:void(0);" id="loginmenutrigger" class="header-label-class header-menu" title="Sign In" onclick="doLogin()"><%=fsLabel.getText("signin_label","Sign In")%></a></div>			
				</div>
