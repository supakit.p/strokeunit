<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Advance</title>		
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createImportScripts('index',pageContext.request,pageContext.response)}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/scripts.jsp"/>
		<link rel="stylesheet" type="text/css" href="../css/program_style.css?${fsScreen.currentTime()}" />
<%
//#(11000) programmer code begin;
//#(11000) programmer code end;
%>
	</head>
	<body class="portalbody portalbody-off">
		<c:out value="${fsScreen.createDialogLayer()}" escapeXml="false"></c:out>
		<c:out value="${fsScreen.createWaitLayer()}" escapeXml="false"></c:out>
		<div id="ptsearchpager" class="pt-page pt-page-current pt-page-controller pmte001-search-pager">
			<div class="header-layer">
				<h1 class="page-header-title">
					<label>Caution</label>
					<div id="fsfontcontrollayer" class="pull-right font-size-layer">
						<a href="javascript:void(0)" class="increase-linker" onclick="increaseFontSize()"><em class="fa fa-plus-square fa-class" aria-hidden="true" title="Increase Font Size"></em></a><a href="javascript:void(0)" class="decrease-linker" onclick="decreaseFontSize()" title="Decrease Font Size"><i class="fa fa-minus-square fa-class" aria-hidden="true"></i></a>
					</div>
				</h1>
			</div>
			<div id="searchpanel" class="panel-body">
<%
//#(12000) programmer code begin;
//#(12000) programmer code end;
%>
				<div id="messagepanellayer" class="message-panel-class">
					${fsLabel.getText("pagenotfound","Page not found")}
				</div>
				<br/>
				<div id="gobacklayer" class="goback-class">
					<a href="javascript:window.history.back();">${fsLabel.getText("goback","Go Back")}</a>
				</div>
<%
//#(13000) programmer code begin;
//#(13000) programmer code end;
%>
			</div>
		</div>
	</body>
</html>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
