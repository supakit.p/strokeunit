<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>

<div id="fspasswordmodaldialog_layer" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
			<div class="modal-header">
				<h4 class="modal-title" id="change_password_title">
					${fsLabel.getText("change_password_title","Change Password")}
				</h4>
			</div>
			<div class="row center-block" style="padding-top:0px;">
				<label id="changepassword_info" class="control-label" style="padding-left:15px;">${fsLabel.getText('changepassword_info','The system force you to change password, please specified your new password and then submit.')}</label>
			</div>
			<div id="fspasswordentrydialoglayer" class="entry-dialog-layer">
			<form id="fspasswordentryform" role="form" data-toggle="validator" name="fspasswordentryform" method="post">
				<input type="hidden" id="fsuserid" name="userid" />
					<div class="row row-height">
						<div class="col-height col-md-5">
							<fs:label tagid="newpassword_label" tagclass="control-label">New Password</fs:label>
							<input type="password" id="fsuserpassword" name="userpassword" class="form-control input-md" />
							<div id="userpassword_alert" role="alert" class="has-error" style="display:none; color:#A94442;">${fsLabel.getText('userpassword_alert','You can not leave this empty')}</div>
						</div>
					</div>				
			</form>	
			</div>		
			<div class="row-heighter modal-footer" >
				<div class="col-md-9 col-height pull-right">
					<input type="button" id="savechangebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savechangebutton','Submit')}"/>
				</div>
			</div>
		</div>
	</div>
</div>
