<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<ul id="menuitemlist" class="nav sidebar-nav" role="menu">
<%
	java.util.Map fsSiderMap = (java.util.Map)request.getAttribute("fsSiderMap");
	java.util.Map fsSiderList = (java.util.Map)request.getAttribute("fsSiderList");
	if(fsSiderMap!=null && fsSiderList!=null) {
		String fsLanguage = fsScreen.getDefaultLanguage(request, response);
		int counter = 0;
		for(java.util.Iterator it = fsSiderMap.keySet().iterator();it.hasNext();) {
			counter++;
			String fsGroup = it.next().toString();
			com.fs.bean.EntityBean fsGroupEntity = (com.fs.bean.EntityBean)fsSiderMap.get(fsGroup);
			java.util.List fsListEntity = (java.util.List)fsSiderList.get(fsGroup);
			String fsGroupStyle = fsGroupEntity.getString("groupstyle");
			String fsGroupName = fsScreen.getEquals("TH",fsLanguage,fsGroupEntity.getString("nameth"),fsGroupEntity.getString("nameen"));
			%>
			<li class="dropdown">
				<a class="<%=fsGroupStyle%> dropdown-toggle" data-toggle="collapse" href="javascript:void(0);#submenu_<%=counter%>"><span>&nbsp;<%=fsGroupName%></span></a>
				<%
				if(fsListEntity.size()>0) {
					%>
					<ul id="submenu_<%=counter %>" class="panel-collapse collapse" role="menu">
					<%
					for(int i=0,isz=fsListEntity.size();i<isz;i++) {
						com.fs.bean.EntityBean fsElement = (com.fs.bean.EntityBean)fsListEntity.get(i);
						if(fsElement!=null) {
							String fsProgName = fsScreen.getEquals("TH",fsLanguage,fsElement.getString("prognameth"),fsElement.getString("progname"));
							String fsIconStyle = fsElement.getString("iconstyle");
							String fsProgid = fsElement.getString("programid");
							String fsUrl = fsElement.getString("url");
							%>
							<li><a href="javascript:void(0)" class="<%=fsIconStyle%>" onclick="open_page('<%=fsProgid%>','<%=fsUrl%>');">&nbsp;<%=fsProgName %></a></li>
							<%							
						}
					}
					%>
					</ul>
					<%
				}
				%>
			</li>
			<%
		}
	}
%>
</ul>
