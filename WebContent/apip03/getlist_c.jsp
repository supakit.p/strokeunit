<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("P03");
    header.setMethod("Get List");
    try {
        Trace.info("############### P03/GetList ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_data = fsBean.getDataValue("data").toString();

        if (fs_data != null && fs_data.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {

                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();
                    String patientId = json.getString("patientId");
                    Trace.info("############ NursingFocus " + patientId + " ############");
/*                    String startDate = json.has("startDate") ? json.getString("startDate") : "";
                    String endDate = json.has("endDate") ? json.getString("endDate") : "";
                    String shiftId = json.has("shiftId") ? json.getString("shiftId") : "";
                    String sortBy = json.has("sortBy") ? json.getString("sortBy") : "";
*/
                    // Tw_FocusNote
                    KnSQL knsql = new KnSQL(this);
                    knsql.append("SELECT Tw_FocusNote.FocusNoteId ,Tw_FocusList.NursingFocusId as ReferFocusId, Mw_MasterConfig.Display , Mw_Focus.FocusName , Tw_FocusList.FocusId , Tw_FocusList.ActiveDate , Tw_FocusNote.ShiftId , Tw_FocusList.CreateDate ");
                    knsql.append(" FROM Tw_FocusList  ");
                    knsql.append(" LEFT JOIN Tw_FocusNote ON Tw_FocusNote.ReferFocusId = Tw_FocusList.NursingFocusId");
                    knsql.append(" LEFT JOIN Mw_Focus ON Mw_Focus.FocusId = Tw_FocusList.FocusId ");
                    knsql.append(" LEFT JOIN Mw_MasterConfig ON Mw_MasterConfig.GroupName = 'FocusNote' AND Mw_MasterConfig.MasterName = 'Shift' AND  Mw_MasterConfig.MasterId = Tw_FocusNote.ShiftId ");
                    knsql.append(" WHERE Tw_FocusList.PatientId = ?patientId AND Tw_FocusNote.ReferFocusId IS NULL ");
                    knsql.append(" UNION ");
                    knsql.append("SELECT Tw_FocusNote.FocusNoteId ,Tw_FocusNote.ReferFocusId , Mw_MasterConfig.Display , Mw_Focus.FocusName , Tw_FocusNote.FocusId  , Tw_FocusNote.ActiveDate , Tw_FocusNote.ShiftId , Tw_FocusNote.CreateDate ");
                    knsql.append(" FROM Tw_FocusNote ");
                    knsql.append(" LEFT JOIN Mw_Focus ON Mw_Focus.FocusId = Tw_FocusNote.FocusId ");
                    knsql.append(" LEFT JOIN Mw_MasterConfig ON Mw_MasterConfig.GroupName = 'FocusNote' AND Mw_MasterConfig.MasterName = 'Shift' AND  Mw_MasterConfig.MasterId = Tw_FocusNote.ShiftId ");
                    knsql.append(" WHERE PatientId = ?patientId ");
                    knsql.append(" ORDER BY ActiveDate , ShiftId , CreateDate ");

/*                    if (!("".equals(startDate) || "".equals(endDate))) {
                        knsql.append(" AND ActiveDate BETWEEN ?startDate AND ?endDate ");
                        knsql.setParameter("startDate", startDate);
                        knsql.setParameter("endDate", endDate);
                    }

                    if (!"".equals(shiftId)) {
                        knsql.append(" AND ShiftId = ?shiftId ");
                        knsql.setParameter("shiftId", shiftId);
                    }

                    if (!"".equals(sortBy)) {
                        knsql.append(" ORDER BY ActiveDate " + sortBy);
                    }
*/
                    knsql.setParameter("patientId", patientId);
                    HashMap<String, String> configField = new HashMap();
                    configField.put("FocusNoteId", "focusNoteId");
                    configField.put("FocusName", "focusName");
                    configField.put("ReferFocusId", "referFocusId");
                    configField.put("FocusId", "focusId");
                    configField.put("ActiveDate", "activeDate");
                    configField.put("ShiftId", "shiftId");
                    configField.put("Display", "shiftName");
                    org.json.JSONArray jsonFocusNote = new org.json.JSONArray(su.requestData(connection, knsql, true, configField).toString());
                    result += jsonFocusNote.length();
                    
                    //get RNP03
                    knsql = new KnSQL(this);
                    knsql.append("SELECT Tw_RNofFrom.InitiateRN ");
                    knsql.append(" FROM Tw_RNofFrom ");
                    knsql.append(" WHERE Tw_RNofFrom.PatientId =?patientId AND Tw_RNofFrom.FormId = 'RNP03' ");
                    knsql.setParameter("patientId", patientId);
                    HashMap<String, String> configFieldRN = new HashMap();
                    configFieldRN.put("InitiateRN", "rn");
                    org.json.JSONObject jsonRN = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldRN).toString());
                    String InitiateRN = "";
                    if (jsonRN.length() > 0) {
                        InitiateRN = jsonRN.getString("rn");
                    }
                    result += jsonRN.length();
//                    body.put("body",jsonFocusNote);
                    body.put("jsonFocusNote", jsonFocusNote);
                    body.put("rn", InitiateRN);
                    
                    Trace.info("body : " + body);
                    return result;

                }
            };

            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
