<%@page import="com.fs.dev.strokeunit.TheStrokeUnitConstant"%>
<%@page import="com.fs.dev.strokeunit.HttpClientServices"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>

<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.http.NameValuePair" %>
<%@ page import="org.apache.http.message.BasicNameValuePair" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.net.URI" %>

<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("P03");
    header.setMethod("GetProgressNote");
    try {
        Trace.info("############### P03/GetProgressNote ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
//        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");

        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = "FocusNote";
        final String fs_data = fsBean.getDataValue("data").toString();
        if ((fs_data != null && fs_data.trim().length() > 0)) {
            final org.json.JSONObject body = new org.json.JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();
                    org.json.JSONObject results = new org.json.JSONObject();
                    final String fs_action = GlobalBean.RETRIEVE_MODE;
                    String focusNoteId = json.has("focusNoteId") ? json.getString("focusNoteId") : "";
                    String focusId = json.has("focusId") ? json.getString("focusId") : "";
                    String activeDate = json.has("activeDate") ? json.getString("activeDate") : "";
                    String shiftId = json.has("shiftId") ? json.getString("shiftId") : "";
                    String referFocusId = json.has("referFocusId") ? json.getString("referFocusId") : "";
                    String patientId = json.has("patientId") ? json.getString("patientId") : "";
                    //check conditional
                    final String multipleFocusValue = su.getSysConfig("FocusNote", "MultipleFocus").getString(TheStrokeUnitConstant.SYS_CONFIG_VALUE);
                    Trace.info("multipleFocusValue : " + multipleFocusValue);
                    org.json.JSONArray assessmentData = new org.json.JSONArray();
                    org.json.JSONArray interventionData = new org.json.JSONArray();
                    org.json.JSONArray evaluationData = new org.json.JSONArray();
                    org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, fs_apiName);
                    org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);
                    KnSQL knsql = new KnSQL(this);
                    String status = "";
                    String statusName = "";
                    if ("0".equals(multipleFocusValue)) {
                        //Tw_FocusNote
                        knsql = new KnSQL(this);
                        knsql.append("SELECT FocusNoteId , PatientId , ReferFocusId , FocusId , ActiveDate , ShiftId  ");
                        knsql.append(" FROM Tw_FocusNote ");
                        knsql.append(" WHERE PatientId=?patientId AND ActiveDate =?activeDate AND ShiftId =?shiftId AND FocusId =?focusId ");
                        knsql.append(" ORDER BY CreateDate DESC ");
                        knsql.setParameter("patientId", patientId);
                        knsql.setParameter("activeDate", activeDate);
                        knsql.setParameter("shiftId", shiftId);
                        knsql.setParameter("FocusId", focusId);
                        HashMap<String, String> configFieldFN = new HashMap();
                        configFieldFN.put("FocusNoteId", "focusNoteId");
                        configFieldFN.put("PatientId", "patientId");
                        configFieldFN.put("ReferFocusId", "referFocusId");
                        configFieldFN.put("FocusId", "focusId");
                        configFieldFN.put("ActiveDate", "activeDate");
                        configFieldFN.put("ShiftId", "shiftId");
                        org.json.JSONObject jsonFocusNote = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldFN).toString());
                        Trace.info("jsonFocusNote : " + jsonFocusNote);
                        if (jsonFocusNote.length() > 0) {
                            if (focusNoteId.length() > 0) {
                                if (focusNoteId.equals(jsonFocusNote.get("focusNoteId"))) {
                                    // if have focusNoteId
                                    // result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                                } else {
                                    throw new RuntimeException("Duplicate data entry.");
                                }
                            } else {
                                if (referFocusId.length() > 0) {
                                    status = "M";
                                    statusName = su.getSysConfig("FocusNote", "MergeAlert").getString(TheStrokeUnitConstant.SYS_CONFIG_VALUE);
                                } else {
                                    status = "R";
                                    statusName = su.getSysConfig("FocusNote", "RetrieveAlert").getString(TheStrokeUnitConstant.SYS_CONFIG_VALUE);
                                }
                                focusNoteId = jsonFocusNote.getString("focusNoteId");
                            }
                        } else {
                            if (focusNoteId.length() <= 0) {
                                System.err.println("get Default");
                                focusNoteId = TheStrokeUnit.Generate.createUUID();
                                json.put("focusNoteId", focusNoteId);
                                //get Default
                                knsql = new KnSQL(this);
                                knsql.append("SELECT 1 ");
                                knsql.append(" FROM Tw_FocusNote ");
                                knsql.append(" WHERE PatientId=?patientId AND FocusId =?focusId ");
                                knsql.setParameter("patientId", patientId);
                                knsql.setParameter("focusId", focusId);
                                try (java.sql.ResultSet rs = knsql.executeQuery(connection)) {
                                    if (!rs.next()) {
                                        if ("1".equals(focusId)) {
                                            // PN0001
                                            knsql = new KnSQL(this);
                                            knsql.append("SELECT Tw_StrokeType.StrokeTypeId  , Tw_StrokeType.Other ");
                                            knsql.append(" FROM Tw_StrokeType ");
                                            knsql.append(" WHERE PatientId = ?patientId ");
                                            knsql.setParameter("patientId", patientId);
                                            HashMap<String, String> configFieldST = new HashMap();
                                            configFieldST.put("StrokeTypeId", "strokeTypeId");
                                            configFieldST.put("Other", "strokeTypeOther");
                                            org.json.JSONArray jsonStrokeType = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldST).toString());
                                            knsql = new KnSQL(this);
                                            knsql.append("SELECT GCSE  , GCSV , GCSM , MotorPowerLU , MotorPowerRU , MotorPowerLB , MotorPowerRB , NIHSS , NIHSSTotal , MRS , ADL ");
                                            knsql.append(" FROM Tw_NSScore ");
                                            knsql.append(" LEFT JOIN Tw_Neuromuscular ON Tw_NSScore.PatientId = Tw_Neuromuscular.PatientId ");
                                            knsql.append(" WHERE Tw_NSScore.PatientId = ?patientId AND Tw_NSScore.FormId = 'PatientInfo' ");
                                            knsql.setParameter("patientId", patientId);
                                            HashMap<String, String> configFieldPT = new HashMap();
                                            configFieldPT.put("GCSE", "eValue");
                                            configFieldPT.put("GCSV", "vValue");
                                            configFieldPT.put("GCSM", "mValue");
                                            configFieldPT.put("MotorPowerLU", "motorPowerLU");
                                            configFieldPT.put("MotorPowerRU", "motorPowerRU");
                                            configFieldPT.put("MotorPowerLB", "motorPowerLB");
                                            configFieldPT.put("MotorPowerRB", "motorPowerRB");
                                            configFieldPT.put("NIHSS", "nihssScore");
                                            configFieldPT.put("NIHSSTotal", "nihssTotal");
                                            configFieldPT.put("MRS", "mrsValue");
                                            configFieldPT.put("ADL", "bathelADLValue");
                                            org.json.JSONObject jsonNeuroSign = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldPT).toString());
                                            if (jsonStrokeType.length() > 0 || jsonNeuroSign.length() > 0) {
                                                org.json.JSONArray assessmentDetail = new org.json.JSONArray();
                                                org.json.JSONObject jsonObjectDetail = new org.json.JSONObject();
                                                org.json.JSONArray jsonArrayProgressDetail = new org.json.JSONArray();
                                                org.json.JSONObject jsonProgressDetail = new org.json.JSONObject();
                                                org.json.JSONObject jsonFocusTime = new org.json.JSONObject();
                                                jsonProgressDetail.put("additional", "S,N");
                                                jsonProgressDetail.put("seq", "1");
                                                jsonProgressDetail.put("strokeType", jsonStrokeType);
                                                jsonProgressDetail.put("neuroSign", jsonNeuroSign);
                                                jsonArrayProgressDetail.put(jsonProgressDetail);
                                                jsonObjectDetail.put("progressNoteId", "PN0001");
                                                jsonObjectDetail.put("progressDetail", jsonArrayProgressDetail);
                                                assessmentDetail.put(jsonObjectDetail);
                                                jsonFocusTime.put("time", "");
                                                jsonFocusTime.put("assessmentDetail", assessmentDetail);
                                                assessmentData.put(jsonFocusTime);
                                            }

                                        } else if ("2".equals(focusId)) {
                                            System.err.println("get Default 2");
                                            // PN0017 , PN0018
                                            knsql = new KnSQL(this);
                                            knsql.append("SELECT Tw_StrokeType.StrokeTypeId  , Tw_StrokeType.Other ");
                                            knsql.append("FROM Tw_StrokeType ");
                                            knsql.append("WHERE PatientId = ?patientId ");
                                            knsql.setParameter("patientId", patientId);
                                            HashMap<String, String> configFieldST = new HashMap();
                                            configFieldST.put("StrokeTypeId", "strokeTypeId");
                                            configFieldST.put("Other", "strokeTypeOther");
                                            org.json.JSONArray jsonStrokeType = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldST).toString());
                                            knsql = new KnSQL(this);
                                            knsql.append("SELECT GCSE  , GCSV , GCSM , MotorPowerLU , MotorPowerRU , MotorPowerLB , MotorPowerRB , NIHSS , NIHSSTotal , MRS , ADL , CTBrainNC ");
                                            knsql.append(" FROM Tw_NSScore ");
                                            knsql.append(" LEFT JOIN Tw_Neuromuscular ON Tw_NSScore.PatientId = Tw_Neuromuscular.PatientId ");
                                            knsql.append(" LEFT JOIN Tw_PresentIllness ON Tw_NSScore.PatientId = Tw_PresentIllness.PatientId ");
                                            knsql.append(" WHERE Tw_NSScore.PatientId = ?patientId AND Tw_NSScore.FormId = 'PatientInfo' ");
                                            knsql.setParameter("patientId", patientId);
                                            HashMap<String, String> configFieldPT = new HashMap();
                                            configFieldPT.put("GCSE", "eValue");
                                            configFieldPT.put("GCSV", "vValue");
                                            configFieldPT.put("GCSM", "mValue");
                                            configFieldPT.put("MotorPowerLU", "motorPowerLU");
                                            configFieldPT.put("MotorPowerRU", "motorPowerRU");
                                            configFieldPT.put("MotorPowerLB", "motorPowerLB");
                                            configFieldPT.put("MotorPowerRB", "motorPowerRB");
                                            configFieldPT.put("NIHSS", "nihssScore");
                                            configFieldPT.put("NIHSSTotal", "nihssTotal");
                                            configFieldPT.put("MRS", "mrsValue");
                                            configFieldPT.put("ADL", "bathelADLValue");
                                            configFieldPT.put("CTBrainNC", "ctBrainNC");
                                            org.json.JSONObject jsonNeuroSign = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldPT).toString());
                                            knsql = new KnSQL(this);
                                            knsql.append("SELECT CTBrainNC ");
                                            knsql.append(" FROM Tw_PresentIllness ");
                                            knsql.append(" WHERE Tw_PresentIllness.PatientId = ?patientId ");
                                            knsql.setParameter("patientId", patientId);
                                            HashMap<String, String> configFieldCT = new HashMap();
                                            configFieldCT.put("CTBrainNC", "ctBrainNC");
                                            org.json.JSONObject jsonCTBrainNC = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldCT).toString());
                                            org.json.JSONArray assessmentDetail = new org.json.JSONArray();
                                            org.json.JSONObject jsonObjectDetail = new org.json.JSONObject();
                                            org.json.JSONArray jsonArrayProgressDetail = new org.json.JSONArray();
                                            org.json.JSONObject jsonProgressDetail = new org.json.JSONObject();
                                            org.json.JSONObject jsonFocusTime = new org.json.JSONObject();
                                            if (jsonCTBrainNC.length() > 0) {
                                                jsonObjectDetail = new org.json.JSONObject();
                                                jsonArrayProgressDetail = new org.json.JSONArray();
                                                jsonProgressDetail = new org.json.JSONObject();
                                                jsonProgressDetail.put("additional", "");
                                                jsonProgressDetail.put("seq", "1");
                                                jsonProgressDetail.put("progressMoreId", "");
                                                jsonProgressDetail.put("progressMoreDescription", jsonCTBrainNC.has("ctBrainNC") ? jsonCTBrainNC.getString("ctBrainNC") : "");
                                                jsonArrayProgressDetail.put(jsonProgressDetail);
                                                jsonObjectDetail.put("progressNoteId", "PN0018");
                                                jsonObjectDetail.put("progressDetail", jsonArrayProgressDetail);
                                                assessmentDetail.put(jsonObjectDetail);
                                            }
                                            if (jsonStrokeType.length() > 0 || jsonNeuroSign.length() > 0) {
                                                jsonObjectDetail = new org.json.JSONObject();
                                                jsonArrayProgressDetail = new org.json.JSONArray();
                                                jsonProgressDetail = new org.json.JSONObject();
                                                jsonProgressDetail.put("additional", "S,N");
                                                jsonProgressDetail.put("seq", "1");
                                                jsonProgressDetail.put("strokeType", jsonStrokeType);
                                                jsonProgressDetail.put("neuroSign", jsonNeuroSign);
                                                jsonArrayProgressDetail.put(jsonProgressDetail);
                                                jsonObjectDetail.put("progressNoteId", "PN0017");
                                                jsonObjectDetail.put("progressDetail", jsonArrayProgressDetail);
                                                assessmentDetail.put(jsonObjectDetail);
                                            }
                                            if (jsonStrokeType.length() > 0 || jsonNeuroSign.length() > 0 || jsonCTBrainNC.length() > 0) {
                                                jsonFocusTime.put("assessmentDetail", assessmentDetail);
                                                jsonFocusTime.put("time", "");
                                                assessmentData.put(jsonFocusTime);
                                            }

                                        } else if ("3".equals(focusId)) {
                                            // PN0028
                                            knsql = new KnSQL(this);
                                            knsql.append("SELECT GCSE  , GCSV , GCSM , MotorPowerLU , MotorPowerRU , MotorPowerLB , MotorPowerRB ");
                                            knsql.append(" FROM Tw_NSScore ");
                                            knsql.append(" LEFT JOIN Tw_Neuromuscular ON Tw_NSScore.PatientId = Tw_Neuromuscular.PatientId ");
                                            knsql.append(" WHERE Tw_NSScore.PatientId = ?patientId AND Tw_NSScore.FormId = 'PatientInfo' ");
                                            knsql.setParameter("patientId", patientId);
                                            HashMap<String, String> configFieldPT = new HashMap();
                                            configFieldPT.put("GCSE", "eValue");
                                            configFieldPT.put("GCSV", "vValue");
                                            configFieldPT.put("GCSM", "mValue");
                                            configFieldPT.put("MotorPowerLU", "motorPowerLU");
                                            configFieldPT.put("MotorPowerRU", "motorPowerRU");
                                            configFieldPT.put("MotorPowerLB", "motorPowerLB");
                                            configFieldPT.put("MotorPowerRB", "motorPowerRB");
                                            org.json.JSONObject jsonNeuroSign = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldPT).toString());
                                            if (jsonNeuroSign.length() > 0) {
                                                org.json.JSONArray assessmentDetail = new org.json.JSONArray();
                                                org.json.JSONObject jsonObjectDetail = new org.json.JSONObject();
                                                org.json.JSONArray jsonArrayProgressDetail = new org.json.JSONArray();
                                                org.json.JSONObject jsonProgressDetail = new org.json.JSONObject();
                                                org.json.JSONObject jsonFocusTime = new org.json.JSONObject();
                                                jsonProgressDetail.put("additional", "N");
                                                jsonProgressDetail.put("seq", "1");
                                                jsonProgressDetail.put("neuroSign", jsonNeuroSign);
                                                jsonArrayProgressDetail.put(jsonProgressDetail);
                                                jsonObjectDetail.put("progressNoteId", "PN0028");
                                                jsonObjectDetail.put("progressDetail", jsonArrayProgressDetail);
                                                assessmentDetail.put(jsonObjectDetail);
                                                jsonFocusTime.put("time", "");
                                                jsonFocusTime.put("assessmentDetail", assessmentDetail);
                                                assessmentData.put(jsonFocusTime);
                                            }
                                        }
                                    }
                                }
                                result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                            }
                        }
                    } else {
                        //multipleFocusValue = 1
                        if (focusNoteId.length() <= 0) {
                            focusNoteId = TheStrokeUnit.Generate.createUUID();
                            json.put("focusNoteId", focusNoteId);
                            result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                        }
                    }
                    results.put("status", status);
                    results.put("statusName", statusName);
                    results.put("focusNoteId", focusNoteId);
                    // get Transaction
                    knsql = new KnSQL(this);
                    System.err.println("Tw_ProgressNote");
                    knsql.append("SELECT PN.ProgressNoteId , PN.FocusTime , PN.FocusNoteId , PN.ProgressType , PN.Additional ");
                    knsql.append(" , PM.Seq PMSeq , PM.ProgressMoreId , PM.ProgressMoreDescription ");
                    knsql.append(" , NS.Seq NSSeq , NS.EValue , NS.VValue , NS.MValue , NS.MotorPowerLU , NS.MotorPowerLB , NS.MotorPowerRU , NS.MotorPowerRB , NS.PupilLtSizeId , NS.PupilLtTypeId , NS.PupilRtSizeId , NS.PupilRtTypeId , NS.NIHSSScore , NS.NIHSSTotal , NS.MRSValue , NS.BathelADLValue ");
                    knsql.append(" , VS.Seq VSSeq , VS.HeartRate , VS.RespiratoryRate , VS.SBP , VS.DBP , VS.OxygenSat , VS.Temperature ");
                    knsql.append(" , AI.Seq AISeq , AI.GroinBit , AI.GroinValueId , AI.BrachealBit , AI.BrachealValueId , AI.WoundClosureId , AI.WoundClosureText , AI.PedisPulseLtId , AI.PedisPulseRtId , AI.HematomaBit , AI.HematomaSize , AI.EcchymosisBit , AI.EcchymosisSize , AI.BleedingBit , AI.BleedingSize ");
                    knsql.append(" FROM Tw_ProgressNote PN ");
                    knsql.append(" LEFT JOIN Tw_ProgressMore PM ON PM.ProgressNoteId = PN.ProgressNoteId AND PM.FocusNoteId = PN.FocusNoteId AND PM.FocusTime = PN.FocusTime ");
                    knsql.append(" LEFT JOIN Tw_NeuroSign NS ON NS.ProgressNoteId = PN.ProgressNoteId AND NS.FocusNoteId = PN.FocusNoteId AND NS.FocusTime = PN.FocusTime ");
                    knsql.append(" LEFT JOIN Tw_VitalSign VS ON VS.FormId = PN.ProgressNoteId AND VS.PatientId = PN.FocusNoteId AND CONVERT(time ,VS.VitalSignTime )  = PN.FocusTime ");
                    knsql.append(" LEFT JOIN Tw_AcuteIschemic AI ON AI.ProgressNoteId = PN.ProgressNoteId AND AI.FocusNoteId = PN.FocusNoteId AND AI.FocusTime = PN.FocusTime ");
                    knsql.append(" WHERE PN.FocusNoteId =?focusNoteId ");
                    knsql.append(" ORDER BY PN.Ordinal , PN.ProgressType , PN.ProgressNoteId , PMSeq , NSSeq , VSSeq , AISeq ");
                    knsql.setParameter("focusNoteId", focusNoteId);
                    HashMap<String, String> configFieldPN = new HashMap();
                    configFieldPN.put("FocusNoteId", "focusNoteId");
                    configFieldPN.put("FocusTime", "focusTime");
                    configFieldPN.put("ProgressNoteId", "progressNoteId");
                    configFieldPN.put("ProgressType", "progressType");
                    configFieldPN.put("Additional", "additional");
                    //Tw_ProgressMore
                    configFieldPN.put("PMSeq", "pmSeq");
                    configFieldPN.put("ProgressMoreId", "progressMoreId");
                    configFieldPN.put("ProgressMoreDescription", "progressMoreDescription");
                    //Tw_NeuroSign
                    configFieldPN.put("NSSeq", "nsSeq");
                    configFieldPN.put("EValue", "eValue");
                    configFieldPN.put("VValue", "vValue");
                    configFieldPN.put("MValue", "mValue");
                    configFieldPN.put("MotorPowerLU", "motorPowerLU");
                    configFieldPN.put("MotorPowerLB", "motorPowerLB");
                    configFieldPN.put("MotorPowerRU", "motorPowerRU");
                    configFieldPN.put("MotorPowerRB", "motorPowerRB");
                    configFieldPN.put("PupilLtSizeId", "pupilLtSizeId");
                    configFieldPN.put("PupilLtTypeId", "pupilLtTypeId");
                    configFieldPN.put("PupilRtSizeId", "pupilRtSizeId");
                    configFieldPN.put("PupilRtTypeId", "pupilRtTypeId");
                    configFieldPN.put("NIHSSScore", "nihssScore");
                    configFieldPN.put("NIHSSTotal", "nihssTotal");
                    configFieldPN.put("MRSValue", "mrsValue");
                    configFieldPN.put("BathelADLValue", "bathelADLValue");
                    //Tw_VitalSign
                    configFieldPN.put("VSSeq", "vsSeq");
                    configFieldPN.put("HeartRate", "heartRate");
                    configFieldPN.put("RespiratoryRate", "respiratoryRate");
                    configFieldPN.put("SBP", "sbp");
                    configFieldPN.put("DBP", "dbp");
                    configFieldPN.put("OxygenSat", "oxygenSat");
                    configFieldPN.put("Temperature", "temperature");
                    //Tw_AcuteIschemic
                    configFieldPN.put("AISeq", "aiSeq");
                    configFieldPN.put("GroinBit", "groinBit");
                    configFieldPN.put("GroinValueId", "groinValueId");
                    configFieldPN.put("BrachealBit", "brachealBit");
                    configFieldPN.put("BrachealValueId", "brachealValueId");
                    configFieldPN.put("WoundClosureId", "woundClosureId");
                    configFieldPN.put("WoundClosureText", "woundClosureText");
                    configFieldPN.put("PedisPulseLtId", "pedisPulseLtId");
                    configFieldPN.put("PedisPulseRtId", "pedisPulseRtId");
                    configFieldPN.put("HematomaBit", "hematomaBit");
                    configFieldPN.put("HematomaSize", "hematomaSize");
                    configFieldPN.put("EcchymosisBit", "ecchymosisBit");
                    configFieldPN.put("EcchymosisSize", "ecchymosisSize");
                    configFieldPN.put("BleedingBit", "bleedingBit");
                    configFieldPN.put("BleedingSize", "bleedingSize");
                    org.json.JSONArray jsonProgressNote = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldPN).toString());
                    Trace.info("jsonProgressNote : " + jsonProgressNote);

                    if (jsonProgressNote.length() > 0) {
                        org.json.JSONArray assessmentDetail = new org.json.JSONArray();
                        org.json.JSONArray interventionDetail = new org.json.JSONArray();
                        org.json.JSONArray evaluationDetail = new org.json.JSONArray();

                        org.json.JSONObject jsonFocusTime = new org.json.JSONObject();
                        org.json.JSONObject jsonObjectDetail = new org.json.JSONObject();
                        org.json.JSONArray jsonArrayProgressDetail = new org.json.JSONArray();
                        org.json.JSONObject jsonProgressDetail = new org.json.JSONObject();
                        org.json.JSONObject jsonAdditional = new org.json.JSONObject();
                        String tempFocusTime = "";
                        String tempProgressType = "";
                        String tempProgressNoteId = "";

                        for (int i = 0; i < jsonProgressNote.length(); i++) {
                            org.json.JSONObject jsonObjectElement = jsonProgressNote.getJSONObject(i);
                            Trace.info("jsonObjectElement : " + i + " " + jsonObjectElement);
                            String focusTime = jsonObjectElement.getString("focusTime");
                            String progressType = jsonObjectElement.getString("progressType");
                            String additional = jsonObjectElement.getString("additional");
                            String progressNoteId = jsonObjectElement.getString("progressNoteId");
                            String pmSeq = jsonObjectElement.getString("pmSeq");
                            String nsSeq = jsonObjectElement.getString("nsSeq");
                            String vsSeq = jsonObjectElement.getString("vsSeq");
                            String aiSeq = jsonObjectElement.getString("aiSeq");

                            if (!progressNoteId.equals(tempProgressNoteId) || !focusTime.equals(tempFocusTime)) {
                                jsonObjectDetail.put("progressDetail", jsonArrayProgressDetail);
                                if (!"".equals(tempProgressNoteId)) {
                                    switch (tempProgressType) {
                                        case "A":
                                            assessmentDetail.put(jsonObjectDetail);
                                            break;
                                        case "I":
                                            interventionDetail.put(jsonObjectDetail);
                                            break;
                                        case "E":
                                            evaluationDetail.put(jsonObjectDetail);
                                            break;
                                    }
                                }
                                jsonArrayProgressDetail = new org.json.JSONArray();
                                jsonObjectDetail = new org.json.JSONObject();
                            }
                            if (!focusTime.equals(tempFocusTime) || !progressType.equals(tempProgressType)) {
                                switch (tempProgressType) {
                                    case "A":
                                        jsonFocusTime.put("assessmentDetail", assessmentDetail);
                                        assessmentData.put(jsonFocusTime);
                                        break;
                                    case "I":
                                        jsonFocusTime.put("interventionDetail", interventionDetail);
                                        interventionData.put(jsonFocusTime);
                                        break;
                                    case "E":
                                        jsonFocusTime.put("evaluationDetail", evaluationDetail);
                                        evaluationData.put(jsonFocusTime);
                                        break;
                                }
                                jsonFocusTime = new org.json.JSONObject();
                                assessmentDetail = new org.json.JSONArray();
                                interventionDetail = new org.json.JSONArray();
                                evaluationDetail = new org.json.JSONArray();
                            }

                            tempFocusTime = focusTime;
                            tempProgressType = progressType;
                            tempProgressNoteId = progressNoteId;
                            jsonFocusTime.put("time", focusTime);
                            //setDetail
                            if (!"".equals(progressNoteId)) {
                                jsonObjectDetail.put("progressNoteId", progressNoteId);
                            }
                            //check Additional
                            if (!"".equals(additional)) {
                                if (additional.contains("S")) {
                                    knsql = new KnSQL(this);
                                    knsql.append("SELECT Tw_FocusStrokeType.StrokeTypeId  , Tw_FocusStrokeType.StrokeTypeOther , Tw_FocusStrokeType.Seq ");
                                    knsql.append("FROM Tw_FocusStrokeType ");
                                    knsql.append("WHERE FocusNoteId = ?focusNoteId AND ProgressNoteId =?progressNoteId AND FocusTime =?focusTime ");
                                    knsql.setParameter("focusNoteId", focusNoteId);
                                    knsql.setParameter("progressNoteId", progressNoteId);
                                    knsql.setParameter("focusTime", focusTime);
                                    HashMap<String, String> configFieldST = new HashMap();
                                    configFieldST.put("StrokeTypeId", "strokeTypeId");
                                    configFieldST.put("StrokeTypeOther", "strokeTypeOther");
                                    configFieldST.put("Seq", "seq");
                                    org.json.JSONArray jsonStrokeType = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldST).toString());
                                    if (jsonStrokeType.length() > 0) {
                                        jsonProgressDetail.put("seq", jsonStrokeType.getJSONObject(0).getString("seq"));
                                        jsonProgressDetail.put("additional", additional);
                                        jsonProgressDetail.put("strokeType", jsonStrokeType);
                                    }
                                }
                                if (additional.contains("N")) {
                                    if (!"".equals(nsSeq)) {
                                        jsonAdditional.put("eValue", jsonObjectElement.get("eValue"));
                                        jsonAdditional.put("vValue", jsonObjectElement.get("vValue"));
                                        jsonAdditional.put("mValue", jsonObjectElement.get("mValue"));
                                        jsonAdditional.put("motorPowerLU", jsonObjectElement.get("motorPowerLU"));
                                        jsonAdditional.put("motorPowerLB", jsonObjectElement.get("motorPowerLB"));
                                        jsonAdditional.put("motorPowerRU", jsonObjectElement.get("motorPowerRU"));
                                        jsonAdditional.put("motorPowerRB", jsonObjectElement.get("motorPowerRB"));
                                        jsonAdditional.put("pupilLtSizeId", jsonObjectElement.get("pupilLtSizeId"));
                                        jsonAdditional.put("pupilLtTypeId", jsonObjectElement.get("pupilLtTypeId"));
                                        jsonAdditional.put("pupilRtSizeId", jsonObjectElement.get("pupilRtSizeId"));
                                        jsonAdditional.put("pupilRtTypeId", jsonObjectElement.get("pupilRtTypeId"));
                                        jsonAdditional.put("nihssScore", jsonObjectElement.get("nihssScore"));
                                        jsonAdditional.put("nihssTotal", jsonObjectElement.get("nihssTotal"));
                                        jsonAdditional.put("mrsValue", jsonObjectElement.get("mrsValue"));
                                        jsonAdditional.put("bathelADLValue", jsonObjectElement.get("bathelADLValue"));
                                        jsonProgressDetail.put("seq", nsSeq);
                                        jsonProgressDetail.put("additional", additional);
                                        jsonProgressDetail.put("neuroSign", jsonAdditional);
                                        jsonArrayProgressDetail.put(jsonProgressDetail);
                                        jsonProgressDetail = new org.json.JSONObject();
                                        jsonAdditional = new org.json.JSONObject();
                                    }
                                }
                                if (additional.contains("A")) {
                                    jsonAdditional.put("groinBit", jsonObjectElement.get("groinBit"));
                                    jsonAdditional.put("groinValueId", jsonObjectElement.get("groinValueId"));
                                    jsonAdditional.put("brachealBit", jsonObjectElement.get("brachealBit"));
                                    jsonAdditional.put("brachealValueId", jsonObjectElement.get("brachealValueId"));
                                    jsonAdditional.put("woundClosureId", jsonObjectElement.get("woundClosureId"));
                                    jsonAdditional.put("woundClosureText", jsonObjectElement.get("woundClosureText"));
                                    jsonAdditional.put("pedisPulseLtId", jsonObjectElement.get("pedisPulseLtId"));
                                    jsonAdditional.put("pedisPulseRtId", jsonObjectElement.get("pedisPulseRtId"));
                                    jsonAdditional.put("hematomaBit", jsonObjectElement.get("hematomaBit"));
                                    jsonAdditional.put("hematomaSize", jsonObjectElement.get("hematomaSize"));
                                    jsonAdditional.put("ecchymosisBit", jsonObjectElement.get("ecchymosisBit"));
                                    jsonAdditional.put("ecchymosisSize", jsonObjectElement.get("ecchymosisSize"));
                                    jsonAdditional.put("bleedingBit", jsonObjectElement.get("bleedingBit"));
                                    jsonAdditional.put("bleedingSize", jsonObjectElement.get("bleedingSize"));
                                    jsonProgressDetail.put("seq", aiSeq);
                                    jsonProgressDetail.put("additional", additional);
                                    jsonProgressDetail.put("acuteIschemic", jsonAdditional);
                                    jsonArrayProgressDetail.put(jsonProgressDetail);
                                    jsonProgressDetail = new org.json.JSONObject();
                                    jsonAdditional = new org.json.JSONObject();
                                }
                                if (additional.contains("V")) {
                                    jsonAdditional.put("heartRate", jsonObjectElement.get("heartRate"));
                                    jsonAdditional.put("respiratoryRate", jsonObjectElement.get("respiratoryRate"));
                                    jsonAdditional.put("sbp", jsonObjectElement.get("sbp"));
                                    jsonAdditional.put("dbp", jsonObjectElement.get("dbp"));
                                    jsonAdditional.put("oxygenSat", jsonObjectElement.get("oxygenSat"));
                                    jsonAdditional.put("temperature", jsonObjectElement.get("temperature"));
                                    jsonProgressDetail.put("seq", vsSeq);
                                    jsonProgressDetail.put("additional", additional);
                                    jsonProgressDetail.put("vitalSign", jsonAdditional);
                                    jsonArrayProgressDetail.put(jsonProgressDetail);
                                    jsonProgressDetail = new org.json.JSONObject();
                                    jsonAdditional = new org.json.JSONObject();
                                }
                            } else {
                                //check at ProgressMore
                                if (!"".equals(pmSeq)) {
                                    jsonProgressDetail.put("seq", pmSeq);
                                    jsonProgressDetail.put("progressMoreId", jsonObjectElement.get("progressMoreId"));
                                    jsonProgressDetail.put("progressMoreDescription", jsonObjectElement.get("progressMoreDescription"));
                                    jsonProgressDetail.put("additional", additional);
                                    jsonArrayProgressDetail.put(jsonProgressDetail);
                                    jsonProgressDetail = new org.json.JSONObject();
                                }
                            }

                        }
                        Trace.info("jsonArrayProgressDetail : " + jsonArrayProgressDetail);
                        if (jsonObjectDetail.length() > 0) {
                            jsonObjectDetail.put("progressDetail", jsonArrayProgressDetail);
                            switch (tempProgressType) {
                                case "A":
                                    assessmentDetail.put(jsonObjectDetail);
                                    break;
                                case "I":
                                    interventionDetail.put(jsonObjectDetail);
                                    break;
                                case "E":
                                    evaluationDetail.put(jsonObjectDetail);
                                    break;
                            }
                        }
                        if (jsonFocusTime.length() > 0) {
                            switch (tempProgressType) {
                                case "A":
                                    jsonFocusTime.put("assessmentDetail", assessmentDetail);
                                    assessmentData.put(jsonFocusTime);
                                    break;
                                case "I":
                                    jsonFocusTime.put("interventionDetail", interventionDetail);
                                    interventionData.put(jsonFocusTime);
                                    break;
                                case "E":
                                    jsonFocusTime.put("evaluationDetail", evaluationDetail);
                                    evaluationData.put(jsonFocusTime);
                                    break;
                            }
                        }
                    }

                    System.err.println("Mw_StrokeType");
                    // get Stroke type
                    knsql = new KnSQL(this);
                    knsql.append("SELECT StrokeTypeId , FocusNoteDisplay , StrokeName , StrokeCategory  ");
                    knsql.append(" FROM Mw_StrokeType ");
                    knsql.append(" WHERE IsActive = '1' ");
                    HashMap<String, String> configFieldST = new HashMap();
                    configFieldST.put("StrokeTypeId", "masterId");
                    configFieldST.put("StrokeCategory", "masterType");
                    configFieldST.put("FocusNoteDisplay", "display");
                    configFieldST.put("StrokeName", "value");
                    org.json.JSONArray jsonMasterStroke = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldST).toString());

                    // get MapProgressNote
                    knsql = new KnSQL(this);
                    knsql.append("SELECT ProgressNoteEffectTo , SeqEffectTo , ProgressNoteId , Seq , MapType , MasterId ");
                    knsql.append(" FROM Mw_MapProgressNote ");
                    knsql.append(" WHERE IsActive = '1' AND FocusId = ?focusId ");
                    knsql.append(" ORDER BY ProgressNoteEffectTo , SeqEffectTo , ProgressNoteId , Seq ");
                    knsql.setParameter("focusId", focusId);
                    HashMap<String, String> configFieldMapProgressNote = new HashMap();
                    configFieldMapProgressNote.put("ProgressNoteEffectTo", "progressNoteEffectTo");
                    configFieldMapProgressNote.put("SeqEffectTo", "seqEffectTo");
                    configFieldMapProgressNote.put("ProgressNoteId", "progressNoteIdAffect");
                    configFieldMapProgressNote.put("Seq", "seqAffect");
                    configFieldMapProgressNote.put("MapType", "mapType");
                    configFieldMapProgressNote.put("MasterId", "masterId");
                    org.json.JSONArray jsonMapProgressNote = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldMapProgressNote).toString());
                    org.json.JSONArray masterMapProgressNote = new org.json.JSONArray();

                    if (jsonMapProgressNote.length() > 0) {
                        org.json.JSONObject mapProgressNote = new org.json.JSONObject();
                        org.json.JSONArray conditionFlag = new org.json.JSONArray();
                        String tempProgressNoteEffectTo = "";
                        String tempSeqEffectTo = "";
                        String tempMapType = "";
                        for (int i = 0; i < jsonMapProgressNote.length(); i++) {
                            org.json.JSONObject jsonObjectElement = jsonMapProgressNote.getJSONObject(i);
                            String progressNoteEffectTo = jsonObjectElement.getString("progressNoteEffectTo");
                            String seqEffectTo = jsonObjectElement.getString("seqEffectTo");
                            String mapType = jsonObjectElement.getString("mapType");
//                            String progressNoteIdAffect = jsonObjectElement.getString("progressNoteIdAffect");
//                            String seqAffect = jsonObjectElement.getString("seqAffect");
                            if (!tempProgressNoteEffectTo.equals(progressNoteEffectTo) || !tempSeqEffectTo.equals(seqEffectTo) || !tempMapType.equals(mapType)) {
                                if (conditionFlag.length() > 0) {
                                    mapProgressNote.put("conditionFlag", conditionFlag);
                                    masterMapProgressNote.put(mapProgressNote);
                                    conditionFlag = new org.json.JSONArray();
                                    mapProgressNote = new org.json.JSONObject();
                                }
                            }
                            tempProgressNoteEffectTo = progressNoteEffectTo;
                            tempSeqEffectTo = seqEffectTo;
                            tempMapType = mapType;
                            mapProgressNote.put("progressNoteEffectTo", progressNoteEffectTo);
                            mapProgressNote.put("seqEffectTo", seqEffectTo);
                            mapProgressNote.put("mapType", mapType);
//                            mapProgressNote.put("progressNoteIdAffect", progressNoteIdAffect);
//                            mapProgressNote.put("seqAffect", seqAffect);
                            //remove for json format
                            jsonObjectElement.remove("progressNoteEffectTo");
                            jsonObjectElement.remove("seqEffectTo");
                            jsonObjectElement.remove("mapType");
                            conditionFlag.put(jsonObjectElement);
                        }
                        if (conditionFlag.length() > 0) {
                            mapProgressNote.put("conditionFlag", conditionFlag);
                            masterMapProgressNote.put(mapProgressNote);
                        }
                    }

                    System.err.println("Mw_MapAdditional");
                    // get Additional
                    knsql = new KnSQL(this);
                    knsql.append("SELECT GroupType , ProgressNoteId , ParamName , LabelValue  ");
                    knsql.append(" FROM Mw_MapAdditional ");
                    knsql.append(" WHERE IsActive = '1' AND FocusId = ?focusId ");
                    knsql.append(" ORDER BY ProgressNoteId , GroupType  ");
                    knsql.setParameter("focusId", focusId);
                    HashMap<String, String> configFieldAdditional = new HashMap();
                    configFieldAdditional.put("GroupType", "groupType");
                    configFieldAdditional.put("ProgressNoteId", "progressNoteId");
                    configFieldAdditional.put("ParamName", "paramName");
                    configFieldAdditional.put("LabelValue", "labelValue");
                    org.json.JSONArray jsonMasterAdditional = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldAdditional).toString());
                    org.json.JSONArray masterAdditional = new org.json.JSONArray();
                    if (jsonMasterAdditional.length() > 0) {
                        org.json.JSONObject groupAdditional = new org.json.JSONObject();
                        String tempProgressNoteIdAdditional = "";
                        String tempGroupTypeAdditional = "";
                        for (int i = 0; i < jsonMasterAdditional.length(); i++) {
                            org.json.JSONObject jsonObjectElement = jsonMasterAdditional.getJSONObject(i);
                            String paramName = jsonObjectElement.getString("paramName");
                            String labelValue = jsonObjectElement.getString("labelValue");
                            String progressNoteIdAdditional = jsonObjectElement.getString("progressNoteId");
                            String groupTypeAdditional = jsonObjectElement.getString("groupType");

                            if (!tempGroupTypeAdditional.equals(groupTypeAdditional) || !tempProgressNoteIdAdditional.equals(progressNoteIdAdditional)) {
                                if (groupAdditional.length() > 0) {
                                    masterAdditional.put(groupAdditional);
                                }
                                groupAdditional = new org.json.JSONObject();
                            }
                            groupAdditional.put("progressNoteId", progressNoteIdAdditional);
                            groupAdditional.put("groupType", groupTypeAdditional);
                            groupAdditional.put(paramName, labelValue);
                            tempGroupTypeAdditional = groupTypeAdditional;
                            tempProgressNoteIdAdditional = progressNoteIdAdditional;
                        }
                        if (groupAdditional.length() > 0) {
                            masterAdditional.put(groupAdditional);
                        }
                    }

                    System.err.println("Mw_ProgressNote");

                    knsql = new KnSQL(this);
                    knsql.append("SELECT ProgressNoteId , Seq , FocusId , ProgressType  , ProgressNoteName  , ProgressNoteUnit  , ParentId  , Additional , AnswerType , AnswerGroup  , ConditionFlag ");
                    knsql.append(" FROM Mw_ProgressNote ");
                    knsql.append(" WHERE FocusId = ?focusId ");
                    knsql.append(" ORDER BY ProgressType , Ordinal , ProgressNoteId , Seq ");
                    knsql.setParameter("focusId", focusId);
                    HashMap<String, String> configFieldP03 = new HashMap();
                    configFieldP03.put("ProgressNoteId", "progressNoteId");
                    configFieldP03.put("Seq", "seq");
                    configFieldP03.put("FocusId", "focusId");
                    configFieldP03.put("ProgressType", "progressType");
                    configFieldP03.put("ProgressNoteName", "progressNoteName");
                    configFieldP03.put("ProgressNoteUnit", "progressNoteUnit");
                    configFieldP03.put("ParentId", "parentId");
                    configFieldP03.put("Additional", "additional");
                    configFieldP03.put("AnswerType", "answerType");
                    configFieldP03.put("AnswerGroup", "answerGroup");
                    configFieldP03.put("ConditionFlag", "conditionFlag");
                    org.json.JSONArray jsonMaster03 = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldP03).toString());
                    result += jsonMaster03.length();
                    org.json.JSONObject progressNote = new org.json.JSONObject();
                    org.json.JSONArray progressDetail = new org.json.JSONArray();
                    org.json.JSONArray jsonArrayParent = new org.json.JSONArray();
                    org.json.JSONObject jsonObjectParent = new org.json.JSONObject();
                    org.json.JSONArray jsonArrayParentDetail = new org.json.JSONArray();
                    org.json.JSONArray progress = new org.json.JSONArray();
                    String tempProgressNoteId = "";
                    String tempProgressType = "";
                    String tempParentId = "";
                    org.json.JSONArray assessment = new org.json.JSONArray();
                    org.json.JSONArray intervention = new org.json.JSONArray();
                    org.json.JSONArray evaluation = new org.json.JSONArray();
                    org.json.JSONArray conditionFlagDetail = new org.json.JSONArray();
                    String conditionFlagGroup = "";
                    org.json.JSONArray conditionFlagGroupDetail = new org.json.JSONArray();
                    String conditionFlag = "";
                    for (int i = 0; i < jsonMaster03.length(); i++) {
                        org.json.JSONObject jsonObjectElement = jsonMaster03.getJSONObject(i);
                        String progressNoteId = jsonObjectElement.getString("progressNoteId");
                        String seq = jsonObjectElement.getString("seq");
                        String progressType = jsonObjectElement.getString("progressType");
                        String additional = jsonObjectElement.getString("additional");
                        String parentId = jsonObjectElement.getString("parentId");
                        conditionFlag = jsonObjectElement.getString("conditionFlag");
                        conditionFlagDetail = new org.json.JSONArray();
                        conditionFlagGroupDetail = new org.json.JSONArray();
                        conditionFlagGroup = "";

                        if (!tempProgressNoteId.equals(progressNoteId)) {
                            if (tempParentId.length() > 0) {
                                jsonObjectParent.put("parentId", tempParentId);
                                progressNote.put("progressDetail", progressDetail);
                                jsonArrayParentDetail.put(progressNote);
                                if (!tempParentId.equals(parentId)) {
                                    jsonObjectParent.put("parentDetail", jsonArrayParentDetail);
                                    jsonArrayParent.put(jsonObjectParent);
                                    jsonArrayParentDetail = new org.json.JSONArray();
                                    jsonObjectParent = new org.json.JSONObject();
                                }
                                progressNote = new org.json.JSONObject();
                                progressDetail = new org.json.JSONArray();
                            } else {
                                progressNote.put("progressDetail", progressDetail);
                                switch (tempProgressType) {
                                    case "A":
                                        assessment.put(progressNote);
                                        break;
                                    case "I":
                                        intervention.put(progressNote);
                                        break;
                                    case "E":
                                        evaluation.put(progressNote);
                                        break;
                                }
                                if (progressDetail.length() > 0) {
                                    progress.put(progressNote);
                                }
                                progressNote = new org.json.JSONObject();
                                progressDetail = new org.json.JSONArray();
                            }
                        }
                        //check Additional
                        if (additional.length() > 0) {
                            if (additional.contains("S")) {
                                jsonObjectElement.put("strokeType", jsonMasterStroke);
                            }
                            for (int j = 0; j < masterAdditional.length(); j++) {
                                org.json.JSONObject jsonObjectAdditionalElement = masterAdditional.getJSONObject(j);
                                String progressNoteIdAdditional = jsonObjectAdditionalElement.getString("progressNoteId");
                                String groupTypeAdditional = jsonObjectAdditionalElement.getString("groupType");
                                if (progressNoteIdAdditional.equals(progressNoteId) && additional.contains(groupTypeAdditional)) {
                                    String groupAdditionalName = "";
                                    switch (groupTypeAdditional) {
                                        case "N":
                                            groupAdditionalName = "neuroSign";
                                            break;
                                        case "A":
                                            groupAdditionalName = "acuteIschemic";
                                            break;
                                        case "V":
                                            groupAdditionalName = "vitalSign";
                                            break;
                                    }
                                    jsonObjectElement.put(groupAdditionalName, jsonObjectAdditionalElement);
                                }
                            }
                        }

                        //check conditionFlag
                        if (conditionFlag.length() > 0) {
                            if ("1".equals(conditionFlag)) {
                                for (int j = 0; j < masterMapProgressNote.length(); j++) {
                                    org.json.JSONObject jsonObjectMapProgressNoteElement = masterMapProgressNote.getJSONObject(j);
                                    String progressNoteEffectTo = jsonObjectMapProgressNoteElement.getString("progressNoteEffectTo");
                                    String seqEffectTo = jsonObjectMapProgressNoteElement.getString("seqEffectTo");
                                    if (progressNoteEffectTo.equals(progressNoteId)) {
                                        if ("".equals(seqEffectTo)) {
                                            conditionFlagGroupDetail = new org.json.JSONArray(jsonObjectMapProgressNoteElement.getString("conditionFlag"));;
                                            conditionFlagGroup = "1";
                                            conditionFlag = "";
                                        } else if (seq.equals(seqEffectTo)) {
                                            conditionFlagDetail = new org.json.JSONArray(jsonObjectMapProgressNoteElement.getString("conditionFlag"));
                                        }
                                    }

                                }
                            } else if ("2".equals(conditionFlag)) {
                                for (int j = 0; j < jsonMapProgressNote.length(); j++) {
                                    org.json.JSONObject jsonObjectMapProgressNoteElement = jsonMapProgressNote.getJSONObject(j);
                                    String progressNoteIdAffect = jsonObjectMapProgressNoteElement.getString("progressNoteIdAffect");
                                    String seqAffect = jsonObjectMapProgressNoteElement.getString("seqAffect");
                                    if (progressNoteIdAffect.equals(progressNoteId)) {
                                        if ("".equals(seqAffect)) {
                                            conditionFlagGroup = "2";
                                            conditionFlag = "";
                                        }
                                    }
                                }
                            }
                        }

                        progressNote.put("progressNoteId", progressNoteId);
//                        progressNote.put("progressType", progressType);
                        progressNote.put("conditionFlagGroup", conditionFlagGroup);
                        progressNote.put("conditionFlagGroupDetail", conditionFlagGroupDetail);
                        jsonObjectElement.put("conditionFlag", conditionFlag);
                        jsonObjectElement.put("conditionFlagDetail", conditionFlagDetail);
                        progressDetail.put(jsonObjectElement);
                        tempProgressNoteId = progressNoteId;
                        tempProgressType = progressType;
                        tempParentId = parentId;
                    }
                    if (progressDetail.length() > 0) {
                        if (tempParentId.length() > 0) {
                            jsonObjectParent.put("parentId", tempParentId);
                            progressNote.put("progressDetail", progressDetail);
                            jsonArrayParentDetail.put(progressNote);
                            jsonObjectParent.put("parentDetail", jsonArrayParentDetail);
                            jsonArrayParent.put(jsonObjectParent);
                        } else {
                            progressNote.put("progressDetail", progressDetail);
                            switch (tempProgressType) {
                                case "A":
                                    assessment.put(progressNote);
                                    break;
                                case "I":
                                    intervention.put(progressNote);
                                    break;
                                case "E":
                                    evaluation.put(progressNote);
                                    break;
                            }
                            progress.put(progressNote);
                        }
                    }

                    for (int i = jsonArrayParent.length(); i > 0; i--) {
                        org.json.JSONObject jsonMainObjectElement = jsonArrayParent.getJSONObject(i - 1);
                        String parentId = jsonMainObjectElement.getString("parentId");
                        org.json.JSONArray parentDetail = jsonMainObjectElement.getJSONArray("parentDetail");
                        if ((i - 2) < 0) {
                            break;
                        }
                        org.json.JSONObject jsonSubObjectElement = jsonArrayParent.getJSONObject(i - 2);
                        org.json.JSONArray subParentDetail = jsonSubObjectElement.getJSONArray("parentDetail");
                        for (int j = 0; j < subParentDetail.length(); j++) {
                            if (parentId.equals(subParentDetail.getJSONObject(j).getString("progressNoteId"))) {
                                jsonArrayParent.getJSONObject(i - 2).getJSONArray("parentDetail").getJSONObject(j).put("child", parentDetail);
                                jsonArrayParent.remove(i - 1);
                            }
                        }
                    }

                    //set child
                    for (int i = 0; i < progress.length(); i++) {
                        org.json.JSONObject jsonObjectElement = progress.getJSONObject(i);
                        String progressNoteId = jsonObjectElement.getString("progressNoteId");
                        for (int j = 0; j < jsonArrayParent.length(); j++) {
                            org.json.JSONObject jsonObjectArrayParentElement = jsonArrayParent.getJSONObject(j);
                            String parentId = jsonObjectArrayParentElement.getString("parentId");
                            if (progressNoteId.equals(parentId)) {
                                progress.getJSONObject(i).put("child", jsonObjectArrayParentElement.getJSONArray("parentDetail"));
                            }
                        }
                    }

                    org.json.JSONObject NursingFocusNote = new org.json.JSONObject();
                    NursingFocusNote.put("assessment", assessment);
                    NursingFocusNote.put("intervention", intervention);
                    NursingFocusNote.put("evaluation", evaluation);
                    //Transaction
                    NursingFocusNote.put("assessmentData", assessmentData);
                    NursingFocusNote.put("interventionData", interventionData);
                    NursingFocusNote.put("evaluationData", evaluationData);
                    results.put("nursingFocusNote", NursingFocusNote);
                    //call my api
                    
//                    URI url = new URI("http://localhost:8080/strokeunit/rest/handler/apiGetMaster/view");
//                    HashMap<String, String> headers = new HashMap();
//                    headers.put(HttpClientServices.CONTENT_TYPE, HttpClientServices.CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED);
//                    List<NameValuePair> urlParameters = new ArrayList<>();
//                    urlParameters.add(new BasicNameValuePair("groupName", "P03Focus" + focusId));
//                    org.json.JSONObject master = HttpClientServices.sendPost(url, headers, urlParameters);
//                    org.json.JSONObject masterHead = new org.json.JSONObject(master.getString("head"));
//                    org.json.JSONObject masterBody = new org.json.JSONObject();
//                    if (!"2".equals(masterHead.getString("errorcode"))) {
//                        masterBody = new org.json.JSONObject(master.getString("body"));
//                    }
                    results.put("master", su.getMaster(connection,"P03Focus" + focusId,null,""));
//                    results.put("master", masterBody);
                    body.put("body", results);
                    Trace.info("body : " + body);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body.get("body"));
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
