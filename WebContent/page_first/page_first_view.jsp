<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsFavorite" scope="request" class="com.fs.bean.ExecuteBean"/>
<link rel="stylesheet" type="text/css" href="page_first/page_first.css?<%=System.currentTimeMillis()%>" />
<link rel="stylesheet" type="text/css" href="css/user_style.css?<%=System.currentTimeMillis()%>" />
<%=fsScreen.createThemeMain()%>
<script type="text/javascript">
var page_first = $.extend({},base,{
	init: function(setting) { 
	}
});
launchApplication(page_first);
</script>
<div id="page_first" class="pt-page">
	<!--<h1 id="page_first_selector" class="font-bold">SELECT MENU</h1>-->
	<div id="page_first_sub" class="panel-body pt-page-body" align="center">
		<div class="favor-navbox-tiles" style="width: 500px; height: 500px; ">
			<c:if test="${fsFavorite.size() > 0}">
				<c:forEach var="fsElement" items="${fsFavorite.elements()}" varStatus="record">	
					<c:if test="${fsElement.getString('iconfile').length() > 0}">
						<a href="javascript:void(0)" class="tile fa-box-title fav-app" pid="${fsElement.getString('programid')}" onclick="open_page('${fsElement.getString('programid')}','${fsElement.getString('url')}')"><div class="icon"><img class="fa fa-app-image" src="resources/apps/${fsElement.getString('iconfile')}" alt=""/></div><span class="title">${fsElement.getString('shortname')}</span></a>		
					</c:if>
					<c:if test="${fsElement.getString('iconfile').length() <= 0}">
						<a href="javascript:void(0)" class="tile fa-box-title fav-app" pid="${fsElement.getString('programid')}" onclick="open_page('${fsElement.getString('programid')}','${fsElement.getString('url')}')"><div class="icon"><img class="fa fa-app-image" src="resources/apps/application.png" alt=""/></div><span class="title">${fsElement.getString('shortname')}</span></a>		
					</c:if>
				</c:forEach>
			</c:if>
		</div>
	</div>
</div>
