<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<link rel="stylesheet" type="text/css" href="page_first/page_first.css" />
<link rel="stylesheet" type="text/css" href="css/user_style.css" />
<%=fsScreen.createThemeMain()%>
<script type="text/javascript">
var page_first = $.extend({},base,{
	init: function(setting) { 
	}
});
launchApplication(page_first);
</script>
