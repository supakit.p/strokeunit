<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsFavorite" scope="request" class="com.fs.bean.ExecuteBean"/>
<%
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("page_first");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/page_first/page_first_default.jsp";
try { 
	String fs_userid = PageUtility.getParameter(request,"userid");
	if(fs_userid!=null && fs_userid.trim().length()>0) {
		boolean eng = com.fs.dev.TheUtility.isEnglish(request);
		KnSQL knsql = fsFavorite.getKnSQL();
		knsql.append("select tprog.programid,tprog.progname,tprog.iconfile,");
		if(eng) {
			knsql.append("tprog.shortname as shortname");
		} else {
			knsql.append("tprog.shortnameth as shortname");			
		}
		knsql.append(",tfavor.seqno,tprod.url ");
		knsql.append("from tfavor ");
		knsql.append("left join tprog ON tfavor.programid = tprog.programid ");
		knsql.append("left join tprod ON tprod.product = tprog.product ");
		knsql.append("where tfavor.userid = ?userid ");
		knsql.append("order by seqno ");
		knsql.setParameter("userid",fs_userid);
		fsGlobal.setFsAction(GlobalBean.COLLECT_MODE);
		fsFavorite.transport(fsGlobal);
		if(fsFavorite.effectedTransactions()>0) {
			fs_forwarder = "/page_first/page_first_view.jsp";
		}
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsFavorite.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsFavorite.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsFavorite.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsFavorite.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsFavorite.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
