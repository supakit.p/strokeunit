<%@page import="javax.ws.rs.BadRequestException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnitConstant"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.http.message.BasicNameValuePair"%>
<%@page import="org.apache.http.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URI"%>
<%@page import="com.fs.dev.strokeunit.HttpClientServices"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.auth.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<%
    com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
    fsBean.addSchema("username", java.sql.Types.VARCHAR, "username");
    fsBean.addSchema("password", java.sql.Types.VARCHAR, "password");
//    fsBean.addSchema("userData", java.sql.Types.VARCHAR, "userData");
    fsBean.obtainFrom(request); //assign variable from request
    fsBean.forceObtain(fsGlobal);
    final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
    if (fs_map != null) {
        fsBean.obtain(fs_map); //assign variable from rest
    }
    fsBean.obtain(session, request);
    final String fs_username = fsBean.getString("username");
    final String fs_password = fsBean.getString("password");
//    final String fs_userData = fsBean.getDataValue("userData").toString();
    final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", java.util.Locale.US);
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
    final JSONObject result = new JSONObject();
    final JSONObject body = new JSONObject();
    try {
        // Do Something
        try (java.sql.Connection connection = ExecuteData.getNewConnection("PROMPT", false)) {
            TheStrokeUnit su = new TheStrokeUnit();
            final String AUTH_AVALIABLE = su.getSysConfig("authSiriraj", "avaliable").getString(TheStrokeUnitConstant.SYS_CONFIG_VALUE);
            if ("1".equals(AUTH_AVALIABLE)) {
                final String APIKEY = su.getSysConfig("authSiriraj", "APIKEY").getString(TheStrokeUnitConstant.SYS_CONFIG_VALUE);
                final String APP_NAME = su.getSysConfig("authSiriraj", "APPNAME").getString(TheStrokeUnitConstant.SYS_CONFIG_VALUE);
                final String URL = su.getSysConfig("authSiriraj", "URL").getString(TheStrokeUnitConstant.SYS_CONFIG_VALUE);
                final String SUB_URL = su.getSysConfig("authSiriraj", "subURL").getString(TheStrokeUnitConstant.SYS_CONFIG_VALUE);
                Date updateDate = new Date();
                URI uri = new URI(URL + SUB_URL);
                HashMap<String, String> headers = new HashMap();
                headers.put(HttpClientServices.CONTENT_TYPE, HttpClientServices.CONTENT_TYPE_APPLICATION_JSON);
                headers.put("APIKEY", APIKEY);
                headers.put("APPNAME", APP_NAME);
                org.json.JSONObject params = new org.json.JSONObject();
                org.json.JSONObject user = new org.json.JSONObject();
                user.put("name", fs_username);
                user.put("pwd", fs_password);
                params.put("user", user);
                try {
                    org.json.JSONObject result_auth = HttpClientServices.sendPost(uri, headers, params);
                    Trace.info("result_auth : " + result_auth);
                    String msg_auth = result_auth.getString("msg");
                    if (msg_auth.equalsIgnoreCase(TheStrokeUnitConstant.SUCCESS)) {
                        org.json.JSONObject userData = result_auth.getJSONObject("UserInfo").getJSONObject("UserData");
                        userData.put("updateDate", df.format(updateDate));
                        org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, "AuthSiriraj");
                        body.put("userData", userData);
                        org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(userData, jsonArrayConfigfield);
                        su.handleParams(connection, GlobalBean.RETRIEVE_MODE, jsonArrayTableListConfig, userData);
                    } else {
                        if (!("admin".equals(fs_username) && "P@ssW0rd".equals(fs_password))) {
                            JSONHeader header = new JSONHeader();
                            header.setModel("auth");
                            header.setMethod("login");
                            header.setErrorflag("Y");
                            header.setErrorcode("5002");
                            header.setErrordesc("User or Password is incorrect");
                            result.put("head", header);
                            response.setStatus(401);
                            out.println(result.toJSONString());
                            return;
                        }
                    }
                } catch (RuntimeException ex) {
                    if (!("admin".equals(fs_username) && "P@ssW0rd".equals(fs_password))) {
                        JSONHeader header = new JSONHeader();
                        header.setModel("auth");
                        header.setMethod("login");
                        header.setErrorflag("Y");
                        header.setErrorcode("500");
                        header.setErrordesc("Connection Timeout");
                        result.put("head", header);
                        response.setStatus(500);
                        out.println(result.toJSONString());
                        return;
                    }
                } 

            }
        } catch (Exception ex) {
            JSONHeader header = new JSONHeader();
            header.setModel("auth");
            header.setMethod("login");
            header.setErrorflag("Y");
            header.setErrorcode("500");
            header.setErrordesc(ex.toString());
            result.put("head", header);
            response.setStatus(500);
            out.println(result.toJSONString());
            return;
        }

    } catch (Exception ex) {
        fsGlobal.setThrowable(ex);
        JSONHeader header = new JSONHeader();
        header.setModel("auth");
        header.setMethod("login");
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    fsAccessor.setFsUser(fs_username);
    fsAccessor.setFsKey(session.getId());
    Trace.debug(fsAccessor);
    fsGlobal.obtain(fsAccessor);
    fsGlobal.retain(request);
    Tracker.trace(fsGlobal);

    JSONHeader header = new JSONHeader();
    header.setModel("logon");
    header.setMethod("login");
    header.setErrorflag("N");
    header.setErrorcode("0");
    header.setErrordesc(fsGlobal.getFsMessage());
    result.put("head", header);
    body.put("key", fsAccessor.getFsKey());
    body.put("userId", fs_username);
    result.put("body", body);
    fsAccessor.startSaveAccessor();
    out.println(result.toJSONString());
%>
