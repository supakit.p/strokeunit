var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("sftq001"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
	setupComponents();
}
function setupComponents() {
		$("#searchbutton").click(function(evt) { 
			search(); 
			return false;
		});
		$("#resetbutton").click(function(evt) { 
			clearingFields(); 
			return false;
		});
}
function clearingFields() {
		fssearchform.reset();
		$("#datatablebody").empty();
		$("#fschapterlayer").empty();
}
function search(aform) {
		if(!aform) aform = fssearchform;
		//alert($(aform).serialize());
		startWaiting();
		jQuery.ajax({
			url: "sftq001_c.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				//alert(transport.responseText);
				searchComplete(transport,data);
			}
		});	
}
function searchComplete(xhr,data) {
		stopWaiting();
		$("#listpanel").html(data);
}
function submitChapter(aform,index) {
		//alert($(aform).serialize());
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sftq001_c.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
				//alert(transport.responseText);
			}
		});
}
function submitOrder(fsParams) {
		//alert(fsParams);
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sftq001_cd.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: fsParams,
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
				//alert(transport.responseText);
			}
		});
		return false;
}
