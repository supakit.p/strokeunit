<%@ page info="SCCS id: $Id$"%>
<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%
	out.println(java.util.Locale.getDefault());	
	String section = request.getParameter("section");
	String db_locale = (String)com.fs.bean.util.GlobalVariable.getVariable(section==null||section.length()<=0?"DB_LOCALE":section.concat("_LOCALE"));
	if(db_locale!=null && db_locale.trim().length()>0) {
		String db_lang = null;
		int index = db_locale.indexOf('_');
		if(index>0) {
			db_lang = db_locale.substring(0,index);
			db_locale = db_locale.substring(index+1);
		}
		Locale[] locales = Locale.getAvailableLocales();
		if(db_lang!=null && db_lang.trim().length()>0) {
			for(int i=0;i<locales.length;i++) {
				if(db_locale.equals(locales[i].getCountry()) && db_lang.equals(locales[i].getLanguage())) {
					Locale.setDefault(locales[i]);
					break;
				}	
			}
		}
	}		
	System.out.println("db locale = ".concat(String.valueOf(java.util.Locale.getDefault())));		
	out.println(java.util.Locale.getDefault());
%>
