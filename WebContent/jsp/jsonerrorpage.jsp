<%@ page info="SCCS id: $Id$"%>
<%@ page isErrorPage="true" %>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%
	String fs_exception = exception!=null && exception.getMessage()!=null && !exception.getMessage().equals("")?exception.getMessage():(exception==null?"":""+exception);
	fs_exception = com.fs.bean.util.JSONUtility.parseJSONString(fs_exception);
	int fs_errorcode = 1;	
	if(exception!=null) {
		exception.printStackTrace();
		if(exception instanceof com.fs.bean.BeanException) {
			fs_errorcode = ((com.fs.bean.BeanException)exception).getErrorCode();
		}
		if(exception instanceof javax.el.ELException) {
			javax.el.ELException ex = (javax.el.ELException)exception;
			if(ex.getCause() instanceof com.fs.bean.BeanException) {
				fs_errorcode = ((com.fs.bean.BeanException)ex.getCause()).getErrorCode();
			}
		}
	}
	if(fs_errorcode==-4011 || fs_errorcode==-4010 || fs_errorcode==-4013 || fs_errorcode==-4014) {
		fs_errorcode = 401; 
	}
%>
{
	"head":{
		"model":"error_handler",
		"errorflag":"Y",
		"errorcode":"<%=fs_errorcode%>",
		"errordesc":"<%=fs_exception%>"		
	}
}
