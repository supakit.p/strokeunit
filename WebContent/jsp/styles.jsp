<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<%=fsScreen.createTheme()%>
<link href="../bootstrap/bootstrap-min.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../jquery/themes/base/jquery.ui.all.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../jquery/themes/base/jquery-ui-1.8.2.custom-min.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../jquery/js/jquery.contextMenu.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../jquery/clockpicker/bootstrap-clockpicker.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/font-awesome.min.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/animations-min.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/custom_form.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/dialog.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/component.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/base_style.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/nav_menu.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/sidebar_menu.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/favor_menu.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<link href="../css/user_style.css?<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]>
	<script src="../bootstrap/html5shiv.js" type="text/javascript"></script>
	<script src="../bootstrap/respond.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="../css/favor_menu_ie.css" />		
<![endif]-->
<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="../bootstrap/style-ie8.css" />
<![endif]-->       
<!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome-ie7.min.css" />
<![endif]-->
