<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<script type="text/javascript" src="../js/modernizr.custom.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/jquery-1.11.1-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/fsutil.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../bootstrap/bootstrap-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../bootstrap/bootbox-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/base.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/fslib-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/jquery-migrate-1.2.1.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/external/jquery.bgiframe-2.1.1.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.core.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.widget.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.menu.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.mouse.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.button.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.draggable-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.position-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.resizable-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.dialog-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.tabs-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.datepicker-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.ui.combobox-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.ui.selectbox-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.ui.navigate-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.ui.editor.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.contextMenu.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.fileupload.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.maskedinput-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.maskMoney-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/clockpicker/bootstrap-clockpicker.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../jquery/js/jquery.ui.waiting.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/navbar.menu.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/sidebar.menu.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/favorbar.menu.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/calendar/calendar.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/date-min.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../js/functions.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="../vue/vue.min.js?<%=System.currentTimeMillis()%>"></script>
<% 
	String fs_deflang = (String)request.getAttribute("default_language");
	if(fs_deflang==null || fs_deflang.trim().length()<=0) {
		fs_deflang = (String)session.getAttribute("default_language");
	}
	if(fs_deflang==null || fs_deflang.trim().length()<=0) fs_deflang = "EN";
%>
<script type="text/javascript">fs_default_language = "<%=fs_deflang%>";</script>
