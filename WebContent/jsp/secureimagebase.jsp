<%@ page info="SCCS id: $Id$" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.misc.*" %>
<%
try {
	SecureImage image = new SecureImage(true); //using mathematical calculation
	image.setEffect(SecureImage.RANDOM_COLOR+SecureImage.RANDOM_LINE+SecureImage.RANDOM_FONT+SecureImage.RANDOM_STYLE);
	//image.setBackgroundEffect(SecureImage.BE_SQUARE+SecureImage.RANDOM_COLOR);
	//image.setBackgroundEffect(SecureImage.BE_OVAL+SecureImage.RANDOM_COLOR);
	image.setBackgroundEffect(SecureImage.RANDOM_COLOR);
	image.setBackgroundColor(java.awt.Color.white);
	image.setFontSize(24);
	image.drawImage(250,100);
	session.setAttribute("fsSecureImage",image.getImageTitle());
	java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
	image.render(bos);
	com.fs.dev.tool.TheImage fsTheImage = new com.fs.dev.tool.TheImage();
	out.print("data:img/jpeg;base64,");
	out.println(fsTheImage.loadImageFromStream(bos.toByteArray()));
} catch(Exception ex) {
	ex.printStackTrace();
}
%>
