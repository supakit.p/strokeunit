<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%fsAccessor.validate(); %>
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<head>
<title>Version Information</title>
<link href="../css/base_style.css" rel="stylesheet" type="text/css"></link>
<link href="../css/user_style.css" rel="stylesheet" type="text/css"></link>
<%=PageUtility.createLinkStyles(fsAccessor)%>
<%
	String progid = PageUtility.getParameter(request,"progid");
	if(progid==null) progid = (String)session.getAttribute("progid"); 
%>
</head>
<body>
	<%if(progid!=null) {%>
	<span style="font-weight:bold; text-align:left;"><%=progid.toUpperCase()%></span>
	<%}%>
	<table id="fstable1" class=""  border ='1' style="width:100%">
		<tbody id="fstablebody1">
			<tr class="rclass"><th>Program</th><th>Version</th></tr>
<%
	if(progid!=null) {
		String realpath = request.getServletContext().getRealPath("");
		String verfile = realpath+java.io.File.separator+progid+java.io.File.separator+progid+".ver";
		java.io.File vfile = new java.io.File(verfile);
		if(!vfile.exists()) {
			verfile = realpath+java.io.File.separator+progid.toUpperCase()+java.io.File.separator+progid+".ver";
			vfile = new java.io.File(verfile);
		}
		if(vfile.exists()) {
			System.out.println("Open version file "+verfile);
			try(java.io.FileInputStream fin = new java.io.FileInputStream(vfile);
				java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(fin))) {
				String line = br.readLine();
				while(line!=null) {
					int idx = line.indexOf("=");
					if(idx>=0) {
						String pid = line.substring(0,idx);
						String vid = line.substring(idx+1);
						out.println("<tr class=\"rclass\">");
						out.println("<td>"+pid+"</td><td align=\"center\">"+vid+"</td>");
						out.println("</tr>");
					}
					line = br.readLine();
				}
			}
		} else {
			String dir = realpath+java.io.File.separator+progid;
			java.io.File[] files = null;
			java.io.File file = new java.io.File(dir);
			if(!file.exists()) {
				dir = realpath+java.io.File.separator+progid.toUpperCase();
				file = new java.io.File(dir);
			}
			System.out.println("Listing file on "+dir);
			if(file.exists()) {
				com.fs.dev.version.VersionListing vr = new com.fs.dev.version.VersionListing();
				vr.setPrinting(false);
				vr.addFolder(dir);
				java.util.List vlist = vr.execute(false);
				if(vlist!=null) {
					for(int i=0,isz=vlist.size();i<isz;i++) {
						String line = (String)vlist.get(i);
						if(line!=null) {
							int idx = line.indexOf('=');
							if(idx>=0) {
								String pid = line.substring(0,idx);
								String vid = line.substring(idx+1);
								out.println("<tr class=\"rclass\">");
								out.println("<td>"+pid+"</td><td align=\"center\">"+vid+"</td>");
								out.println("</tr>");
							}
						}
					}
				}
			} else {
				out.println("<tr class=\"rclass\">");
				out.println("<td colspan=2>Program ID is undefined</d>");	
				out.println("</tr>");
			}
		}
	}
%>
		</tbody>
	</table>
	<br>
	<div style="text-align: center">
		<input type="button" name="closeButton" value="Close" class="buttoncancel" onclick="window.close();"></input>
	</div>	
</body>
</html>
