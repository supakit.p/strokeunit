<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%fsAccessor.validate(); %>
<%
	String action = PageUtility.getParameter(request,"action");
	String key = PageUtility.getParameter(request,"key");
	String value = PageUtility.getParameter(request,"value");
	if(action!=null) {
		if((key!=null) && (value!=null)) {
			GlobalVariable.setVariable(key,value);
		}
		String fs_forwarder = "/jsp/variables.jsp";
		RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
		rd.forward(request,response);
		return;
	}
%>
<!DOCTYPE html>
<html>
<head>
<title>Global Variables</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link href="../css/base_style.css" rel="stylesheet" type="text/css"></link>
<link href="../css/user_style.css" rel="stylesheet" type="text/css"></link>
<%=PageUtility.createLinkStyles(fsAccessor)%>
</head>
<body>
	<form name="detform" method="post" action="variables_c.jsp">
		<input type="hidden" name="action" value="edit"></input>
		<input type="hidden" name="key" value="<%=key%>"></input>
		<table border="1" style="width:100%">
			<tr><th>Key</th><th>Value</th></tr>
			<tr>
				<td><%=key%></td>
				<td><input type="text" name="value" value="<%=value%>" style="width:99%"></input></td>
			</tr>
			<tr>
				<td colspan="2" align="right">
					<input type="submit" name="submit" value="Submit"></input>
					<input type="reset" name="reset" value="Reset"></input>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
