<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.dom.*"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%fsAccessor.validate(); %>
<!DOCTYPE html>
<html>
<head>
<title>Global Variables</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link href="../css/base_style.css" rel="stylesheet" type="text/css"></link>
<link href="../css/user_style.css" rel="stylesheet" type="text/css"></link>
<%=PageUtility.createLinkStyles(fsAccessor)%>
</head>
<body>
	<h1 class="page-header-title" title="global">Global Variable Information</h1>
<table border="1">
	<tr>
		<th>Key</th><th>Value</th><th>Description</th>
	</tr>
<%
	DOMReader dom = new DOMReader();
	String realpath = request.getServletContext().getRealPath("");
	String filename = realpath+java.io.File.separator+"jsp"+java.io.File.separator+"global_descriptor.xml";
	java.io.FileInputStream fin = new java.io.FileInputStream(filename);
	dom.loadXML(fin);
	java.util.Map hat = dom.getRootAttributes();
	int idx = 0;
	for(java.util.Enumeration en = GlobalVariable.variables();en.hasMoreElements();) {
		String key = (String)en.nextElement();	      
		if(key.equals("CONFIG")) continue; //ignore root tag
		String value = "";
		Object object = GlobalVariable.getVariable(key);
		if(object!=null) value = object.toString();
		String desc = (String)hat.get(key);
		if(desc==null) desc = "";
		idx++;
		desc = BeanUtility.nativeToUnicode(desc);
		out.println("<tr>");
		out.println("<td>");
		out.println("<form id=\"detform"+idx+"\" name=\"detform\" method=\"post\" action=\"variables_c.jsp\">");
		out.println("<input type=\"hidden\" name=\"key\" value=\""+key+"\"></input>");
		out.println("<input type=\"hidden\" name=\"value\" value=\""+value+"\"></input>");
		out.println("<a href=\"javascript:void(0);\" ondragstart=\"return false;\" onclick=\"detform"+idx+".submit();\">"+key+"</a>");
		out.println("</form>");
		out.println("</td>");
		out.println("<td>"+value+"&nbsp;</td>");
		out.println("<td>"+desc+"&nbsp;</td>");
		out.println("</tr>");
	}
%>
</table>
</body>
</html>