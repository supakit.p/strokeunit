<%@ page isErrorPage="true" %>
<%@ page import="com.fs.bean.util.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%
	String errorMessage = PageUtility.getParameter(request,"message");
	if((errorMessage==null) || errorMessage.trim().equals("")) errorMessage = "General Protection Error Occured";
	int errorCode = 0;
	if(exception!=null) {
		exception.printStackTrace();
		if(exception instanceof com.fs.bean.BeanException) {
			errorCode = ((com.fs.bean.BeanException)exception).getErrorCode();
		}
		if(exception instanceof javax.el.ELException) {
			javax.el.ELException ex = (javax.el.ELException)exception;
			if(ex.getCause() instanceof com.fs.bean.BeanException) {
				errorCode = ((com.fs.bean.BeanException)ex.getCause()).getErrorCode();
			}
		}
	}
	int statusCode = 400;
	if(errorCode==-4011 || errorCode==-4010 || errorCode==-4013 || errorCode==-4014) {
		statusCode = 401; 
	}
	response.setStatus(statusCode);
%>
<!DOCTYPE html>
<html>
<head>
<title>Error</title>
<link href="../css/base_style.css" rel="stylesheet" type="text/css"></link>
<link href="../css/user_style.css" rel="stylesheet" type="text/css"></link>
<%=PageUtility.createLinkStyles(fsAccessor)%>
<script>
function talkToParent() {
	try {
		window.parent.alertbox(errorlayer.innerText);
	}catch(ex) { }
}
</script>
</head>
<body style="background-color: whitesmoke;" onload="talkToParent()">
<br></br><br></br>
<div style="text-align:center; color:red; font-size: 24px">
<%=errorMessage%>
</div>
<br></br>
<div style="text-align:center">
	<a href="javascript:window.history.back();">Go Back</a>
</div>
<br></br>
<div id="errorlayer" style="text-align:center;">
<%if(exception!=null && exception.getMessage()!=null && !exception.getMessage().equals("")) {%>
<%=BeanUtility.preserveXML(exception.getMessage())%>
<%}else{ String fs_exception = exception==null?"":""+exception;  %>
<%=BeanUtility.preserveXML(fs_exception)%>
<%}%>
</div>
<br></br>
</body>
</html>