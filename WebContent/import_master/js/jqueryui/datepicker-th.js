( function( factory ) {
    if ( typeof define === "function" && define.amd ) {
        // AMD. Register as an anonymous module.
        define( [ "../widgets/datepicker" ], factory );
    } else {
        // Browser globals
        factory( jQuery.datepicker );
    }
}( function( datepicker ) {

    datepicker.regional.th = {
        closeText: "�Դ",
        prevText: "&#xAB;&#xA0;��͹",
        nextText: "�Ѵ�&#xA0;&#xBB;",
        currentText: "�ѹ���",
        monthNames: [ "���Ҥ�","����Ҿѹ��","�չҤ�","����¹","����Ҥ�","�Զع�¹","�á�Ҥ�","�ԧ�Ҥ�","�ѹ��¹","���Ҥ�","��Ȩԡ�¹","�ѹ�Ҥ�" ],
        monthNamesShort: [ "�.�.","�.�.","��.�.","��.�.","�.�.","��.�.","�.�.","�.�.","�.�.","�.�.","�.�.","�.�." ],
        dayNames: [ "�ҷԵ��","�ѹ���","�ѧ���","�ظ","����ʺ��","�ء��","�����" ],
        dayNamesShort: [ "��.","�.","�.","�.","��.","�.","�." ],
        dayNamesMin: [ "��.","�.","�.","�.","��.","�.","�." ],
        weekHeader: "Wk",
        dateFormat: "dd/mm/yy",
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: "" 
    };
    datepicker.setDefaults( datepicker.regional.th );
    
    /*
    datepicker.beforeShow = function() {
        if($(this).val()!=""){  
            var arrayDate=$(this).val().split("-");       
            arrayDate[2]=parseInt(arrayDate[2])-543;  
            $(this).val(arrayDate[0]+"-"+arrayDate[1]+"-"+arrayDate[2]);  
        }  
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);  
    };
        
    datepicker.onChangeMonthYear = function() {
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);        
    };
        
    datepicker.onClose = function() {
        if($(this).val()!="" && $(this).val()==dateBefore){           
            var arrayDate=dateBefore.split("-");  
            arrayDate[2]=parseInt(arrayDate[2])+543;  
            $(this).val(arrayDate[0]+"-"+arrayDate[1]+"-"+arrayDate[2]);      
        }         
    };
        
    datepicker.onSelect = function(dateText, inst) {
        dateBefore=$(this).val();  
        var arrayDate=dateText.split("-");  
        arrayDate[2]=parseInt(arrayDate[2])+543;  
        $(this).val(arrayDate[0]+"-"+arrayDate[1]+"-"+arrayDate[2]);  
    };
    */

    return datepicker.regional.th;

} ) );