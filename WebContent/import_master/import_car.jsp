<%-- 
    Document   : sfte101_stroke_report
    Created on : Sep 24, 2017, 8:34:33 PM
    Author     : Nuttha_Sot
--%>

<%@page contentType="text/html" pageEncoding="TIS-620"%>
<%@page import="com.fs.dev.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=TIS-620">
        <link rel="stylesheet" href="../import_master/js/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../import_master/js/bootstrap/css/jquery-clockpicker.css">
        <script type="text/javascript" src="../import_master/js/jquery.min.js"></script>
        <script type="text/javascript" src="../import_master/js/jqueryui/jquery-ui.js"></script>
        <script type="text/javascript" src="../import_master/js/bootstrap/js/bootstrap-clockpicker.min.js"></script>
        <script  type="text/javascript" src="import_car.js"></script>
        <style>
        </style>
        <script>
            function init() {
                com.fs.dev.carloan.ImportCarComposer.test();
            }
        </script>
        <title>Upload File</title>
    </head>
    <body onload="initialAction();
            init();">
        <form action="UploadFile" method="post" enctype="multipart/form-data">
            Select File : <input type="file" name="filetoupload">
            <br/>
            <input type="submit" value="Upload File">
        </form>
        <div id="importfile">
            <table id="datatable" class="table table-bordered table-hover table-striped tablesorter">
                <thead>
                    <tr class="panel panel-info panel-heading" style="">
                        <th class="text-center">Record Time</th>
                        <th class="text-center">BP (mm.Hg)</th>
                        <th class="text-center">HR (/min)</th>
                        <th class="text-center">RR (/min)</th>
                    </tr>
                </thead>
                <tbody id="dataListTable">

                </tbody>
            </table>	
        </div>
    </body>
</html>