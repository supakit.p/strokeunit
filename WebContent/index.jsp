﻿<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<% 
    String fs_forwarder = "/intro/intro.jsp?home=true";
	//String fs_forwarder = "/main.jsp";
	if(fsAccessor.isValid()) {
		fs_forwarder = "/main.jsp";
	}    
    RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
    rd.forward(request, response);
%>
