<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.dev.mail.*" %>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsResetPasswordBean" scope="request" class="com.fs.dev.auth.ResetPasswordBean"/>
<jsp:setProperty name="fsResetPasswordBean" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("forgot_password");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
JSONHeader header = new JSONHeader();
header.setModel("forgot_password");
header.setMethod("forgot");
try { 
	int fs_action = fsGlobal.parseAction();
	fsResetPasswordBean.forceObtain(fsGlobal);
	String fs_language = fsGlobal.getFsLanguage();
	String fs_lang = request.getParameter("language");
	if(fs_lang!=null && fs_lang.trim().length()>0) fs_language = fs_lang;
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) {
		fsResetPasswordBean.obtain(fs_map);
		if(fs_map.get("language")!=null) fs_language = (String)fs_map.get("language");
	}
	fsGlobal.setFsLanguage(fs_language);
	if(fsResetPasswordBean.getEmail().trim().length()<=0) throw new BeanException("Email address is invalid",-8892);
	fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
	fsResetPasswordBean.obtain(session,request);
	fsResetPasswordBean.transport(fsGlobal);
	System.out.println("effected transaction : "+fsResetPasswordBean.effectedTransactions());
	if(fsResetPasswordBean.effectedTransactions()>0) {
		java.util.Map map = null;
		java.sql.Connection connection = null;
		try {
			connection = DBConnection.getConnection(fsGlobal.getFsSection());
			map = (java.util.Map)XMLConfig.create(request).getConfigure("FORGOTPASSWORDMAIL",connection);
		} finally { 
			if(connection!=null) { try { connection.close(); }catch(Exception ex) {} }
		}		
		Trace.info("FORGOTPASSWORDMAIL = "+map);
		if(map!=null) {
			String fs_email = fsResetPasswordBean.getEmail();
			if(fs_email!=null && fs_email.trim().length()>0) {
				StringBuffer msg = new StringBuffer();
				msg.append("Dear, "+fsResetPasswordBean.getUsername()+"<br>");
				msg.append("Confirm your password was changed<br>");
				msg.append("user = "+fsResetPasswordBean.getUserid()+"<br>");
				msg.append("new password = "+fsResetPasswordBean.getPassword()+"<br>");
				msg.append("       yours sincerely,<br>");
				Trace.info("forgot password : "+fsResetPasswordBean.getUserid()+" ("+fs_email+")");
				SendMail mailer = new SendMail();
				mailer.obtain(map);
				mailer.setTo(fs_email);
				mailer.setMessage(msg.toString());
				mailer.start();
			}
		}
	}
}catch(Exception ex) { 
	Trace.error(fsGlobal,ex);
	fsGlobal.setThrowable(ex);
	header.setErrorflag("Y");
	header.setErrorcode("1");
	header.setErrordesc(fsGlobal.getFsMessage());
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
header.setErrorflag("N");
header.setErrorcode("0");
header.setErrordesc(fsGlobal.getFsMessage());
result.put("head",header);
out.println(result.toJSONString());
%>
