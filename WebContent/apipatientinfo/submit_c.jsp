<%@page import="com.fs.dev.strokeunit.TheStrokeUnitPatientInfo"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>

<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('api',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("PatientInfo");
    header.setMethod("Submit");
    try {
        Trace.info("############### PatientInfo/Submit ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();
        if ((fs_apiName != null && fs_apiName.trim().length() > 0)) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 1;
                    TheStrokeUnitPatientInfo patientInfo = new TheStrokeUnitPatientInfo();
                    org.json.JSONObject jsonResult = patientInfo.submit(connection, json, fs_apiName);
                    int errorCode = jsonResult.getInt("errorCode");
                    if(errorCode != 0){
                        return errorCode;
                    }
                    body.put("body", jsonResult);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            } else if (fsExecuter.effectedTransactions() == -4092) {
                header.setErrorflag("Y");
                header.setErrorcode("4092");
                header.setErrordesc("HN already exists");
                result.put("head", header);
                response.setStatus(409);
                out.println(result.toJSONString());
                return;
            } else if (fsExecuter.effectedTransactions() == -204) {
                header.setErrorflag("Y");
                header.setErrorcode("204");
                header.setErrordesc("BedId may not be null");
                result.put("head", header);
                response.setStatus(204);
                out.println(result.toJSONString());
                return;
            } else if (fsExecuter.effectedTransactions() == -4091) {
                header.setErrorflag("Y");
                header.setErrorcode("4091");
                header.setErrordesc("BedNo. has already used");
                result.put("head", header);
                response.setStatus(409);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
