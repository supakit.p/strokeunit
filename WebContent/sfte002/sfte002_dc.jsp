<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte002',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE002Bean" scope="session" class="com.fs.bean.SFTE002Bean"/>
<jsp:useBean id="fsSFTE002ABean" scope="request" class="com.fs.bean.SFTE002ABean"/>
<jsp:setProperty name="fsSFTE002ABean" property="*"/>
<%
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte002");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte002/sfte002_de.jsp";
try { 
	int fsRowid = fsGlobal.parseRowid();
	if(fsRowid>0) {
		fsSFTE002ABean = (com.fs.bean.SFTE002ABean)fsSFTE002Bean.getBeanAt(fsRowid-1);
		session.setAttribute("fsSFTE002ABean",fsSFTE002ABean);
	}
	int fs_action = fsGlobal.parseAction();
	fsSFTE002ABean.setUserids("");
	fsSFTE002ABean.setProgids("");
	if(fs_action==GlobalBean.ENTER_ACTION || fs_action==GlobalBean.EDIT_ACTION) {
		String[] fs_progids = request.getParameterValues("progid");
		if(fs_progids!=null) fsSFTE002ABean.setProgids(BeanUtility.merge(fs_progids));
	}
	fsGlobal.obtain(fsAccessor);
	fsSFTE002ABean.obtain(session,request);
	fsSFTE002ABean.transport(fsGlobal);
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsSFTE002ABean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE002ABean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE002ABean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE002ABean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE002ABean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
