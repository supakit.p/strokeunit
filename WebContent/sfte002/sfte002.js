var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("sfte002"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
		setupComponents();
		setupAlertComponents();
}
function setupComponents() {
		$("#searchbutton").click(function(evt) { 
			//if(!confirm("search")) return false;
			search(); 
			return false;
		});
		$("#insertbutton").click(function(evt) { 
			insert(); 
			return false;
		});
		$("#savebutton").click(function() { 
			save();
			return false;
		});
		$("#cancelbutton").click(function() { 
			cancel();
			return false;
		});
		$("#adduserbutton").click(function() { 
			addNewUserInfo();
			return false;
		});
		$("#addprogbutton").click(function() { 
			addNewProgramInfo();
			return false;
		});		
		var usercombo = $("#useringroup").combobox({
				beforeLookup : function() { 							
					$("#useringroup").userslookup();				
					return false;
				}
			});
		$("#savebuttondialoginsert").click(function() { 
			save(fsinsertform,true);
			return false;			
		});
		$("#updatebuttondialoginsert").click(function() { 
			save(fsinsertform,true);
			return false;			
		});
		$("#iconstyleswitcher").styleswitcher({$styleInput: $("#iconstyledialog")});
		$(".modal-dialog").draggable();
		$("#progtablebody").sortable({
			stop: function( event, ui ) {
				displaySequenceTableLists("#progtablebody");
			}
		});	
		$("#addsitebutton").click(function() { 
			addNewSiteInfo();
			return false;
		});		
}
function clearingFields() {
	fsentryform.reset();
	$("#usertablebody").empty();
	$("#progtablebody").empty();
	$("#sitetablebody").empty();
	$(".alert-input").parent().removeClass("has-error");
	$(".alert-span").hide();
	clearAlerts();
}
function search(aform) {
		if(!aform) aform = fssearchform;
		//alert($(aform).serialize());
		startWaiting();
		jQuery.ajax({
			url: "sfte002_c.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				//alert(transport.responseText);
				searchComplete(transport,data);
			}
		});	
}
function searchComplete(xhr,data) {
		stopWaiting();
		$("#listpanel").html(data);
}
function insert() {
		clearingFields();
		fsentryform.fsDatatype.value = "json";
		fsentryform.fsAction.value = "enter";
		$("input.ikeyclass",fsentryform).removeAttr("readonly");
		//showPageEntry();
		fsinsertform.reset();
		fsinsertform.fsAction.value = "enter";
		$("#groupnamedialog").removeAttr("readonly");
		$("#privateflagdialog").attr("checked",false).val("1");
		$("#modalheaderedit").hide();
		$("#modalheader").show();
		$("#updatebuttondialoginsert").hide();
		$("#savebuttondialoginsert").show();
		$("#usertypedialogfieldset").attr("disabled",false);
		$("#iconstyleswitcher").styleupdate("fa fa-desktop");
		$("#insert_dialog_layer").modal("show");		
}
function showPageSearch() {
		$("#entrypanel").hide();
		$("#searchpanel").show();
}
function showPageEntry() {
		$("#entrypanel").show();
		$("#searchpanel").hide();
}
function submitRetrieve(rowIndex) {
		var aform = fslistform;
		aform.fsRowid.value = ""+rowIndex;
		aform.fsDatatype.value = 'json';
		//alert($(aform).serialize());
		startWaiting();
		jQuery.ajax({
			url: "sfte002_dc.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				//alert(transport.responseText);
				prepareScreenToUpdate(fsentryform,data);
			}
		});	
		return false;
}
function prepareScreenToUpdate(aform,data) {
	clearAlerts();
	aform.fsDatatype.value = "text";
	aform.fsAction.value = "edit";
	$("#usertablebody").empty();
	$("#progtablebody").empty();
	$("#sitetablebody").empty();
	try {
		var jsRecord = $.parseJSON(data);
		for(var p in jsRecord) {
			$("#"+p,aform).val(jsRecord[p]);
		}
		$("#privateflag").attr("checked","1"==jsRecord["privateflag"]).val("1");
		var icons = jsRecord["iconstyle"];		
		if(!icons || $.trim(icons)=="") icons = "fa fa-desktop";
		var $btn = $("<button style='width:55px;'></button>").click(function() { return false; });
		$btn.append("<i class='"+icons+"' aria-hidden='true'></i>");
		$("#iconstylelayer").empty().append($btn);		
		$("#sequenceno").val(jsRecord["seqno"]);
		//readUserInfo(jsRecord["groupname"]);
		readProgramInfo(jsRecord["groupname"]);
		//readSiteInfo(jsRecord["groupname"]);
	} catch(ex) { }
	//$("input.ikeyclass",aform).editor({edit:false});
	$("input.ikeyclass",aform).attr("readonly",true);	
	showPageEntry();
}
function submitEdit(rowIndex) {
	var aform = fslistform;
	aform.fsRowid.value = ""+rowIndex;
	aform.fsDatatype.value = 'json';
	//alert($(aform).serialize());
	startWaiting();
	jQuery.ajax({
		url: "sfte002_dc.jsp",
		type: "POST",
		data: $(aform).serialize(),
		dataType: "html",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data,status,transport){ 
			stopWaiting();
			fsinsertform.reset();
			prepareScreenToEdit(fsinsertform,data);
			$("#insert_dialog_layer").modal("show");
		}
	});	
	return false;
}
function prepareScreenToEdit(aform,data) {
	clearAlerts();		
	aform.fsAction.value = "update";
	try {
		var jsRecord = $.parseJSON(data);		
		$("#groupnamedialog").val(jsRecord["groupname"]).attr("readonly",true);
		$("#nameendialog").val(jsRecord["nameen"]);
		$("#namethdialog").val(jsRecord["nameth"]);
		$("#privateflagdialog").attr("checked","1"==jsRecord["privateflag"]).val("1");
		$("#usertypedialogfieldset").attr("disabled",true);
		var icons = jsRecord["iconstyle"];		
		if(!icons || $.trim(icons)=="") icons = "fa fa-desktop";
		$("#iconstyleswitcher").styleupdate(icons);
		$("#sequenceno").val(jsRecord["seqno"]);
	} catch(ex) { }
	$("#modalheader").hide();	
	$("#modalheaderedit").show();
	$("#savebuttondialoginsert").hide();
	$("#updatebuttondialoginsert").show();
}
function cancel() {
	confirmCancel(function() {
		clearingFields();
		showPageSearch();
	});
}
function validForm(aform) {
	clearAlerts();
	var validator = null;
	if($.trim($(".groupname",aform).val())=="") {
		$(".groupname",aform).parent().addClass("has-error");
		$(".groupname-alert",aform).show();
		if(!validator) validator = "groupname";
	}
	if($.trim($(".nameen",aform).val())=="") {
		$(".nameen",aform).parent().addClass("has-error");
		$(".nameen-alert",aform).show();
		if(!validator) validator = "nameen";
	}
	if($.trim($(".nameth",aform).val())=="") {
		$(".nameth",aform).parent().addClass("has-error");
		$(".nameth-alert",aform).show();
		if(!validator) validator = "nameth";
	}
	if($.trim($(".sequenceno",aform).val())=="") {
		$(".sequenceno",aform).parent().addClass("has-error");
		$(".sequenceno-alert",aform).show();
		if(!validator) validator = "sequenceno";
	}
	if(validator) {
		$("."+validator,aform).focus();
		setTimeout(function() { 
			$("."+validator,aform).parent().addClass("has-error");
			$("."+validator+"-alert",aform).show();
		},100);
		return false;
	}
	return true;
}
function save(aform,dialog) {
		if(!aform) aform = fsentryform;
		//alert($(aform).serialize());
		if(!validForm(aform)) return false;
		var isInsertMode = aform.fsAction.value=='enter';
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "sfte002_dc.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					if(isInsertMode) {
						if(dialog) { 
							fssearchform.fsPage.value = fslistform.fsPage.value;
							search();							
						} 
						successbox(function() { fsinsertform.reset(); clearingFields(); });					
					} else {
						showPageSearch();
						fssearchform.fsPage.value = fslistform.fsPage.value;
						search();
						$("#insert_dialog_layer").modal("hide");
					}
				}
			});
		});
		return false;
}
function submitChapter(aform,index) {
		//alert($(aform).serialize());
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte002_c.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: $(aform).serialize(),
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
				//alert(transport.responseText);
			}
		});
}
function submitOrder(fsParams) {
		//alert(fsParams);
		startWaiting();
		var xhr = jQuery.ajax({
			url: "sfte002_cd.jsp",
			type: "POST",
			contentType: defaultContentType,
			data: fsParams,
			dataType: "html",
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				$("#listpanel").html(data); 
				//alert(transport.responseText);
			}
		});
		return false;
}
function submitDelete(fsParams) {
		//alert("delete : "+fsParams);
		confirmDelete([fsParams[0]],function() {
			deleteRecord(fsParams);
		});
		return false;
}
function deleteRecord(fsParams) {
		//alert("delete record : "+fsParams);
		startWaiting();
		jQuery.ajax({
			url: "sfte002_dc.jsp",
			type: "POST",
			data: "fsAction=delete&fsDatatype=json&groupname="+fsParams[0],
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				stopWaiting();
				showPageSearch();
				fssearchform.fsPage.value = fslistform.fsPage.value;
				search();
			}
		});	
}
function exploreLayer(src,layer) {
		var $src = $(src);
		if($src.is(".up")) {
			$src.removeClass("up").addClass("down");
			$("#"+layer).hide();
			$src.find(".fa").removeClass("fa-chevron-circle-up").addClass("fa-chevron-circle-down");
		} else {
			$src.removeClass("down").addClass("up");
			$("#"+layer).show();
			$src.find(".fa").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-up");
		}
}
function displaySequenceInfo(tabname) {
		$("tr",$(tabname)).each(function(index,element) { 
			$("td",$(element)).eq(0).html(""+(index+1));
		});		
}
function displayUserInfo(uid,uname,comname,seqno) {
		if(!seqno) seqno = "";
		var $table = $("#usertablebody");
		var $tr = $("<tr></tr>");
		var $td1 = $('<td class="cclass" align="center"></td').append(seqno);
		var $td2 = $('<td class="cclass" align="center"></td>').append(uid);
		var $td3 = $('<td class="cclass">&nbsp;</td>').append(uname);
		var $td4 = $('<td class="cclass">&nbsp;</td>').append(comname);
		var $td5 = $('<td class="cclass" align="center"></td>');
		var $btn = $('<input type="button" class="btn-delete"></input>');
		$btn.attr("title","Delete "+uid);
		$btn.click(function() { 
			confirmRemove([uid],function() { 
				removeUserInGroup(uid,$("#groupname").val(),function() { 
					$tr.remove();
					displaySequenceInfo("#usertablebody");
				});
			});
			return false;
		});
		$td5.append($btn);
		var $uinput = $('<input type="hidden" name="userid" class="useridclass"></input>');
		$uinput.val(uid);
		$td2.append($uinput);
		$tr.append($td1).append($td2).append($td3).append($td4).append($td5);
		$table.append($tr);
}
/*
function displayProgramInfo(pid,pname,seqno) {
		if(!seqno) seqno = "";
		var $table = $("#progtablebody");
		var $tr = $("<tr></tr>");
		var $td1 = $('<td class="cclass" align="center"></td').append(seqno);
		var $td2 = $('<td class="cclass" align="center"></td>').append(pid);
		var $td3 = $('<td class="cclass">&nbsp;</td>').append(pname);
		var $td4 = $('<td class="cclass" align="center"></td>');
		var $btn = $('<input type="button" class="btn-delete"></input>');
		$btn.attr("title","Delete "+pid);
		$btn.click(function() { 
			confirmRemove([pid],function() { 
				removeProgramInGroup(pid,$("#groupname").val(),function() { 
					$tr.remove();
					displaySequenceInfo("#progtablebody");
				});
			});
			return false;
		});
		$td4.append($btn);
		var $uinput = $('<input type="hidden" name="progid" class="progidclass"></input>');
		$uinput.val(pid);
		$td2.append($uinput);
		$tr.append($td1).append($td2).append($td3).append($td4);
		$table.append($tr);
}
*/
function readUserInfo(grpname) { 
		$("#userlayerheader").startWaiting();
		jQuery.ajax({
			url: "sfte002_uc.jsp",
			type: "POST",
			data: "fsAjax=true&fsDatatype=jsondata&groupname="+grpname,
			dataType: "json",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				$("#userlayerheader").stopWaiting();
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				$("#userlayerheader").stopWaiting();
				//alert(transport.responseText);
				if(data["rows"]) {
					$(data.rows).each(function(index,element) { 
						var uid = element["userid"];
						var uname = element["usertname"]+" "+element["usertsurname"];
						displayUserInfo(uid,uname,element["nameth"]);
					});
					displaySequenceInfo("#usertablebody");
				}
			}
		});	
}
function readProgramInfo(grpname) {
		$("#proglayerheader").startWaiting();
		jQuery.ajax({
			url: "sfte002_pc.jsp",
			type: "POST",
			data: "fsAjax=true&fsDatatype=jsondata&groupname="+grpname,
			dataType: "json",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				$("#proglayerheader").stopWaiting();
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				$("#proglayerheader").stopWaiting();
				//alert(transport.responseText);
				if(data["rows"]) {
					$(data.rows).each(function(index,element) { 
						displayProgramInfo(element["programid"],element["progname"]);
					});
					displaySequenceTableLists("#progtablebody");
				}
			}
		});	
}
function readSiteInfo(grpname) { 
	$("#sitelayerheader").startWaiting();
	jQuery.ajax({
		url: "sfte002_site_c.jsp",
		type: "POST",
		data: "fsAjax=true&fsDatatype=jsondata&groupname="+grpname,
		dataType: "json",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) { 
			$("#sitelayerheader").stopWaiting();
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data,status,transport){ 
			$("#sitelayerheader").stopWaiting();
			//alert(transport.responseText);
			if(data["rows"]) {
				$(data.rows).each(function(index,element) { 
					displaySiteInfo(element["site"],element["compname"]);
				});
				displaySequenceInfo("#sitetablebody");
			}
		}
	});	
}
function addNewUserInfo() {
		var gid = $("#groupname").val();
		var uid = $("#useringroup").val();
		if($.trim(uid)=="") return;
		var found = $("input[value='"+uid+"']",$("#usertablebody")).size()>0;
		if(found) {
			alertmsg("QS0121","Duplicate user entry");
			return;
		}
		var idx = $("tr",$("#usertablebody")).size();
		jQuery.ajax({
			url: "sfte002_user_insert_c.jsp",
			type: "POST",
			data: "fsAjax=true&fsDatatype=jsondata&userid="+uid+"&groupname="+gid,
			dataType: "json",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				if(data["rows"]) {
					var element = data.rows[0];
					if(element) {
						var uname = element["usertname"]+" "+element["usertsurname"];
						displayUserInfo(uid,uname,element["nameth"],idx+1);
					}
				} else {
					//displayUserInfo(uid,"",idx+1);
					alertmsg("QS0122","User not found");
				}
			}
		});	
}
function addNewProgramInfo() { 
		var gid = $("#groupname").val();
		var pid = $("#progingroup").val();
		if($.trim(pid)=="") return;
		var found = $("input[value='"+pid+"']",$("#progtablebody")).size()>0;
		if(found) {
			alertbox("Duplicate program entry");
			return;
		}
		var pname = $("option:selected",$("#progingroup")).text();
		var idx = $("tr",$("#progtablebody")).size();		
		jQuery.ajax({
			url: "sfte002_prog_insert_c.jsp",
			type: "POST",
			data: "fsAjax=true&fsDatatype=jsondata&programid="+pid+"&groupname="+gid+"&seqno="+(idx+1),
			dataType: "json",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				displayProgramInfo(pid,pname,idx+1);
			}
		});	
}
function addNewSiteInfo() {
	var gid = $("#groupname").val();
	var sid = $("#siteingroup").val();
	if($.trim(sid)=="") return;
	var found = $("input[value='"+sid+"']",$("#sitetablebody")).size()>0;
	if(found) {
		alertmsg("QS0121","Duplicate company entry");
		return;
	}
	var idx = $("tr",$("#sitetablebody")).size();
	jQuery.ajax({
		url: "sfte002_site_insert_c.jsp",
		type: "POST",
		data: "fsAjax=true&fsDatatype=jsondata&site="+sid+"&groupname="+gid,
		dataType: "json",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data,status,transport){ 
			displaySiteInfo(sid,$("option:selected",$("#siteingroup")).text(),idx+1);
		}
	});	
}
function displaySiteInfo(sid,comname,seqno) {
	if(!seqno) seqno = "";
	var $table = $("#sitetablebody");
	var $tr = $("<tr></tr>");
	var $td1 = $('<td class="cclass siteno-column" align="center"></td').append(seqno);
	var $td2 = $('<td class="cclass sitename-column">&nbsp;</td>').append(comname);
	var $td3 = $('<td class="cclass sitectrl-column" align="center"></td>');
	var $btn = $('<input type="button" class="btn-delete"></input>');
	$btn.attr("title","Delete "+comname);
	$btn.click(function() { 
		confirmRemove([sid],function() { 
			removeSiteInGroup(sid,$("#groupname").val(),function() { 
				$tr.remove();
				displaySequenceInfo("#sitetablebody");
			});
		});
		return false;
	});
	$td3.append($btn);
	var $uinput = $('<input type="hidden" name="groupcomp" class="siteclass"></input>');
	$uinput.val(sid);
	$td2.append($uinput);
	$tr.append($td1).append($td2).append($td3);
	$table.append($tr);
}
function removeUserInGroup(uid,gid,callback) {
	jQuery.ajax({
		url: "sfte002_user_delete_c.jsp",
		type: "POST",
		data: "fsAjax=true&fsDatatype=json&userid="+uid+"&groupname="+gid,
		dataType: "json",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data,status,transport){ 
			if(callback) callback();
		}
	});	
}
function removeProgramInGroup(pid,gid,callback) {
	jQuery.ajax({
		url: "sfte002_prog_delete_c.jsp",
		type: "POST",
		data: "fsAjax=true&fsDatatype=json&programid="+pid+"&groupname="+gid,
		dataType: "json",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data,status,transport){ 
			if(callback) callback();
		}
	});	
}
function removeSiteInGroup(sid,gid,callback) {
	jQuery.ajax({
		url: "sfte002_site_delete_c.jsp",
		type: "POST",
		data: "fsAjax=true&fsDatatype=json&site="+sid+"&groupname="+gid,
		dataType: "json",
		contentType: defaultContentType,
		error : function(transport,status,errorThrown) { 
			submitFailure(transport,status,errorThrown);  
		},
		success: function(data,status,transport){ 
			if(callback) callback();
		}
	});	
}
function displayProgramInfo(pid,pname,seqno) {
	if(!seqno) seqno = "";
	var $table = $("<table class='prog-item-table-class'></table>");
	var $item = $('<li class="prog-item-class ui-state-active"></li>');	
	var $tr = $("<tr></tr>");
	var $td1 = $('<td class="cclass progno-column" align="center"></td').append(seqno);
	var $td2 = $('<td class="cclass progid-column" align="center"></td>').append(pid);
	var $td3 = $('<td class="cclass progname-column">&nbsp;</td>').append(pname);
	var $td4 = $('<td class="cclass progctrl-column" align="center"></td>');
	var $btn = $('<input type="button" class="btn-delete"></input>');
	$btn.attr("title","Delete "+pid);
	$btn.click(function() { 
		confirmRemove([pid],function() { 
			removeProgramInGroup(pid,$("#groupname").val(),function() { 
				$item.remove();
				displaySequenceTableLists("#progtablebody");
			});
		});
		return false;
	});
	$td4.append($btn);
	var $uinput = $('<input type="hidden" name="progid" class="progidclass"></input>');
	$uinput.val(pid);
	$td2.append($uinput);
	$tr.append($td1).append($td2).append($td3).append($td4);
	$table.append($tr);
	$item.append($table);
	$("#progtablebody").append($item);
}
function displaySequenceTableLists(tablelists) {
	$("li",$(tablelists)).each(function(index,element) { 
		var $table = $(this).find("table").eq(0);
		$table.find("tr").eq(0).find("td").eq(0).html(""+(index+1));
	});
}
