<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<%
com.fs.dev.TheUtility smutil = new com.fs.dev.TheUtility("AUTH");
java.util.Map fs_progmap = (java.util.Map)session.getAttribute("PROGRAM_CATEGORY");
if(fs_progmap==null) {
	fs_progmap = smutil.createProgram(fsAccessor,smutil.getSection(),"sfte002");
	if(fs_progmap!=null) session.setAttribute("PROGRAM_CATEGORY",fs_progmap);
}
smutil.createUserTypes(fsAccessor, "sfte002", request, "USERTYPE_CATEGORY");
smutil.removeCategories(request, "ALL_SITES_CATEGORY");
smutil.createAllSites(fsAccessor, "sfte002", request, "ALL_SITES_CATEGORY", com.fs.dev.Content.emptyContent());
%>
