<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE002ABean" scope="session" class="com.fs.bean.SFTE002ABean"/>
<jsp:include page="sfte002_md.jsp"/>
	<div id="entrylayer" class="entry-layer">
		<form id="fsentryform" name="fsentryform" method="post">	
			<input type="hidden" name="fsAction" value="enter"/>
			<input type="hidden" name="fsAjax" value="true"/>
			<input type="hidden" name="fsDatatype" value="text"/>
			<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
			<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
			<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>	
			<div class="row portal-area sub-entry-layer">
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="groupname_label" tagclass="control-label" required="true">Group Name</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<input type="text" class="form-control input-md ikeyclass alert-input groupname" id="groupname" name="groupname" placeholder="" autocomplete="off" size="20" picture="(20)X" />
							<div id="groupname_alert" role="alert" class="has-error groupname-alert" style="display:none;">${fsLabel.getText('groupname_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="nameen_label" tagclass="control-label" required="true">Name(English)</fs:label>
						</div>
						<div class="col-md-6 col-height">
							<input class="form-control input-md alert-input nameen" id="nameen" name="nameen" placeholder="" autocomplete="off" size="50"/>
							<div id="nameen_alert" role="alert" class="has-error nameen-alert" style="display:none;">${fsLabel.getText('nameen_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="nameth_label" tagclass="control-label" required="true">Name(Thai)</fs:label>
						</div>
						<div class="col-md-6 col-height">
							<input class="form-control input-md alert-input nameth" id="nameth" name="nameth" placeholder="" autocomplete="off" size="50"/>
							<div id="nameth_alert" role="alert" class="has-error nameth-alert" style="display:none;">${fsLabel.getText('nameth_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="usertype_label" tagclass="control-label" required="true">User Type</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<fieldset id="usertypefieldset" disabled><legend style="display:none;"></legend>
							<fs:select tagid="usertype" name="usertype" tagclass="form-control input-md ikeyclass alert-input" section="USERTYPE_CATEGORY"> </fs:select>
							</fieldset>
							<div id="usertype_alert" role="alert" class="has-error usertype-alert" style="display:none;">${fsLabel.getText('usertype_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
						</div>
						<div class="col-md-8 col-height radio my-radio currenting-field">
								<table><tr><td>
										<input type="checkbox" class="form-control ikeyclass input-md" id="privateflag" name="privateflag" value="1" />
								</td><td valign="middle">
										<fs:label tagclass="lclass control-label" tagid="privateflag_label" for="privateflag">Reserved for Private Group</fs:label>
								</td></tr></table>
						</div>
				</div>		
				<div class="row row-heighter center-block">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="iconstyle_label" tagclass="control-label" required="false">Icon Style</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<div id="iconstylelayer"></div>
							<input type="hidden" id="iconstyle" name="iconstyle"/>
						</div>
				</div>
				<div class="row row-heighter center-block">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="sequenceno_label" tagclass="control-label" required="false">Sequence</fs:label>
						</div>
						<div class="col-md-2 col-height">
							<fs:int tagclass="form-control input-md alert-input sequenceno" id="sequenceno" name="seqno"> </fs:int>
							<div id="sequenceno_alert" role="alert" class="has-error sequenceno-alert" style="display:none;">${fsLabel.getText('sequenceno_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
					<div class="col-md-4 pull-right text-right" style="margin-right: 10px;">
						<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save')}"/>
						&nbsp;&nbsp;
						<input type="button" id="cancelbutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('cancelbutton','Cancel')}"/>
					</div>
				</div>
			</div>

			<div class="row" style="margin-left:0px; margin-right: 3px;">
			<div id="proglayerheader" class="pt-page-header pt-page-corser" style="margin-top: 5px;"><fs:label tagid="progtrans_label">Program In Group</fs:label><a href="javascript:void(0)" class="pull-right up" onclick="exploreLayer(this,'proglayer')"><em class="fa fa-chevron-circle-up" style="margin-right: 5px;"></em></a></div>
			<div id="proglayer" class="table-responsive portal-area portal-area-layer">
					<div class="row" style="padding-top:3px; padding-bottom:3px;">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="prog_progingroup_label" tagclass="control-label">Program ID</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<fs:select name="progingroup" tagclass="form-control input-sm" tagid="progingroup" selection="PROGRAM_CATEGORY" showing="both"> </fs:select>
						</div>
						<div class="col-md-1 col-height">
							<input type="button" id="addprogbutton" class="btn btn-dark btn-sm" value="+" style="width:30px;" title="Add Program"/>
						</div>
					</div>
					<table id="progtable" class="table table-bordered table-hover table-striped tablesorter">
						<thead id="progtableheader">
							<tr>
								<th width="50px"  class="text-center" style="cursor: default;"><fs:label tagid="prog_seqno_headerlabel" >No.</fs:label></th>
								<th width="100px" class="text-center"><fs:label tagid="prog_progid_headerlabel">Program ID</fs:label></th>
								<th width="200px" class="text-center"><fs:label tagid="prog_progname_headerlabel">Program Name</fs:label></th>
								<th width="50px" class="text-center"><em class="fa fa-bolt" aria-hidden="true"></em></th>
							</tr>
						</thead>
						<tbody id="aprogtablebody">
						</tbody>
					</table>
					<div id="progtablelayer" style="margin-bottom: 50px;">
						<ul id="progtablebody" class="ul-table-listing"> </ul>
					</div>					
			</div>
			</div>
			
		</form>
	</div>