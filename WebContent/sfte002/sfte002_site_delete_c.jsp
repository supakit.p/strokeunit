<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsExecuteBean" scope="page" class="com.fs.bean.ExecuteBean"/>
<%
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("sfte002");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
try { 
	fsGlobal.setFsAction(GlobalBean.DELETE_MODE);
	String fs_groupname = PageUtility.getParameter(request,"groupname");
	String fs_site = PageUtility.getParameter(request,"site");
	if((fs_groupname!=null && fs_groupname.trim().length()>0) && (fs_site!=null && fs_site.trim().length()>0)) {
		KnSQL sql = fsExecuteBean.getKnSQL();
		sql.clear();
		sql.append("delete from tgroupcomp ");
		sql.append("where tgroupcomp.groupname = ?groupname ");
		sql.append("and tgroupcomp.site = ?site ");
		sql.setParameter("groupname",fs_groupname);	
		sql.setParameter("site",fs_site);	
		fsExecuteBean.obtain(session,request);
		fsExecuteBean.transport(fsGlobal);
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
fsExecuteBean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsExecuteBean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsExecuteBean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsExecuteBean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsExecuteBean.toXML());
	return;
}
out.print(fsExecuteBean.toJSONData("rows"));
%>
