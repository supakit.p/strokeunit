<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<c:if test="${fsScreen.init('sfte002',pageContext.request,pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>

<div id="insert_dialog_layer" class="modal fade pt-page pt-page-item" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xm">
		<div class="modal-content portal-area" style="margin-left:15px; padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modalheader">${fsLabel.getText("insert_dialog_title","New Group")}</h4>
				<h4 class="modal-title" id="modalheaderedit" style="display:none;">${fsLabel.getText("edit_dialog_title","Edit Group")}</h4>
			</div>
			<form id="fsinsertform" role="form" data-toggle="validator" name="fsinsertform" method="post">
				<input type="hidden" name="fsAction" value="enter"/>
				<input type="hidden" name="fsAjax" value="true"/>
				<input type="hidden" name="fsDatatype" value="json"/>			
				<div class="row row-heighter center-block" style="margin-top: 15px;">
							<div class="col-md-3 col-height col-label text-right">
								<fs:label tagid="groupnamedialog_label" tagclass="control-label" required="true">Group Name</fs:label>
							</div>
							<div class="col-md-4 col-height">
								<input type="text" class="form-control input-sm ikeyclass alert-input groupname" id="groupnamedialog" name="groupname" placeholder="" autocomplete="off" size="20" picture="(20)X" />
							</div>
				</div>
				<div class="row row-heighter center-block">
							<div class="col-md-3 col-height text-right"></div>
							<div class="col-md-6 col-height">
								<span id="groupnamedialog_alert" role="alert" class="alert-span has-error groupname-alert" style="display:none;">${fsLabel.getText('groupnamedialog_alert','You can not leave this empty')}</span>
							</div>
				</div>
				<div class="row row-heighter center-block">
							<div class="col-md-3 col-height col-label text-right">
								<fs:label tagid="nameendialog_label" tagclass="control-label" required="true">Name(English)</fs:label>
							</div>
							<div class="col-md-8 col-height">
								<input class="form-control input-sm alert-input nameen" id="nameendialog" name="nameen" placeholder="" autocomplete="off" size="50"/>
								<span id="nameendialog_alert" role="alert" class="alert-span has-error nameen-alert" style="display:none;">${fsLabel.getText('nameendialog_alert','You can not leave this empty')}</span>
							</div>
				</div>
				<div class="row row-heighter center-block">
							<div class="col-md-3 col-height col-label text-right">
								<fs:label tagid="namethdialog_label" tagclass="control-label" required="true">Name(Thai)</fs:label>
							</div>
							<div class="col-md-8 col-height">
								<input class="form-control input-sm alert-input nameth" id="namethdialog" name="nameth" placeholder="" autocomplete="off" size="50"/>
								<span id="namethdialog_alert" role="alert" class="alert-span has-error nameth-alert" style="display:none;">${fsLabel.getText('namethdialog_alert','You can not leave this empty')}</span>
							</div>
				</div>
				<div class="row row-heighter center-block">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="usertypedialog_label" tagclass="control-label" required="true">User Type</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<fieldset id="usertypedialogfieldset"><legend style="display:none;"></legend>
							<fs:select tagid="usertypedialog" name="usertype" tagclass="form-control input-md alert-input" section="USERTYPE_CATEGORY"> </fs:select>
							</fieldset>
							<div id="usertypedialog_alert" role="alert" class="has-error usertype-alert" style="display:none;">${fsLabel.getText('usertypedialog_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-heighter center-block">
						<div class="col-md-3 col-height col-label text-right">
						</div>
						<div class="col-md-8 col-height radio my-radio currenting-field">
								<table><tr><td>
										<input type="checkbox" class="form-control input-md" id="privateflagdialog" name="privateflag" value="1" />
								</td><td valign="middle">
										<fs:label tagclass="lclass control-label" tagid="privateflagdialog_label" for="privateflagdialog">Reserved for Private Group</fs:label>
								</td></tr></table>
						</div>
				</div>				
				<div class="row row-heighter center-block">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="iconstyledialog_label" tagclass="control-label" required="false">Icon Style</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<div id="iconstyleswitcher"></div>
							<input type="hidden" id="iconstyledialog" name="iconstyle"/>
						</div>
				</div>
				<div class="row row-heighter center-block">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="sequencenodialog_label" tagclass="control-label" required="false">Sequence</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<fs:int tagclass="form-control input-md alert-input sequenceno" id="sequencenodialog" name="seqno"> </fs:int>
							<div id="sequencenodialog_alert" role="alert" class="has-error sequenceno-alert" style="display:none;">${fsLabel.getText('sequencenodialog_alert','You can not leave this empty')}</div>
						</div>
				</div>
			</form>			
			<div class="row-heighter modal-footer" >
				<div class="col-md-9 col-height pull-right">
					<input type="button" id="savebuttondialoginsert" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebuttondialoginsert','Save')}"/>
					<input type="button" id="updatebuttondialoginsert" class="btn btn-dark btn-sm" value="${fsLabel.getText('updatebuttondialoginsert','Update')}" style="display:none;"/>
					<input type="button" id="cancelbuttondialoginsert" class="btn btn-dark btn-sm" data-dismiss="modal" value="${fsLabel.getText('cancelbuttondialoginsert','Cancel')}"/>
				</div>
			</div>
		</div>
	</div>
</div>
