<%@page import="com.fs.dev.rest.resource.Employees"%>
<%@page import="com.fs.dev.strok.service.Employee"%>
<%@page import="com.fs.dev.strok.service.TheEmployee"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnitUtility"%>
<%@page import="com.fs.dev.strokeunit.HttpClientServices"%>
<%@page import="com.fs.dev.strok.service.Patient"%>
<%@page import="com.fs.dev.strok.service.ThePatient"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnitConstant"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.http.NameValuePair" %>
<%@ page import="org.apache.http.message.BasicNameValuePair" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.net.URI" %>


<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("Header");
    header.setMethod("request");
    try {
        Trace.info("############### Header/request ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();

        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_data = fsBean.getDataValue("data").toString();

        if (fs_data != null && fs_data.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();

                    String patientId = json.getString("patientId");
                    KnSQL knsql = new KnSQL(this);
                    knsql.append("SELECT PatientId , Ward , HN  , AN  , PatientName ,AdmissionDate , Tw_PatientInfo.BedId , BedNo ");
                    knsql.append(" FROM Tw_PatientInfo ");
                    knsql.append(" LEFT JOIN Mw_BedAvail ON Tw_PatientInfo.BedId = Mw_BedAvail.BedId ");
                    knsql.append(" WHERE Tw_PatientInfo.PatientId = ?patientId AND Tw_PatientInfo.IsActive = '1' AND Tw_PatientInfo.StatusCode IN ('N','D') ");
                    knsql.setParameter("patientId", patientId);
                    HashMap<String, String> configFieldPI = new HashMap();
                    configFieldPI.put("PatientId", "patientId");
                    configFieldPI.put("HN", "hn");
                    configFieldPI.put("AN", "an");
                    configFieldPI.put("PatientName", "patientName");
                    configFieldPI.put("AdmissionDate", "admissionDate");
                    configFieldPI.put("BedId", "bedId");
                    configFieldPI.put("BedNo", "bedNo");
                    configFieldPI.put("Ward", "ward");
                    org.json.JSONObject jsonHeader = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldPI).toString());
                    result += jsonHeader.length();
                    
                    // RN
                    knsql = new KnSQL(this);
                    knsql.append("SELECT InitiateRN , FormId , Username , FullName , Job ");
                    knsql.append(" FROM Tw_RNofFrom ");
                    knsql.append(" LEFT JOIN Tw_Authority ON Tw_Authority.SapId = Tw_RNofFrom.InitiateRN ");
                    knsql.append(" WHERE PatientId =?patientId ");
                    knsql.setParameter("patientId", patientId);
                    HashMap<String, String> configFieldRN = new HashMap();
                    configFieldRN.put("InitiateRN", "rn");
                    configFieldRN.put("FormId", "formId");
                    configFieldRN.put("Username", "username");
                    configFieldRN.put("FullName", "fullName");
                    configFieldRN.put("Job", "job");
                    org.json.JSONArray jsonRN = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldRN).toString());
                    jsonHeader.put("initiateRN", jsonRN);
                    
                    // Co-Mobility
                    knsql = new KnSQL(this);
                    knsql.append("SELECT Tw_CoMorbility.CoMobilityId , Tw_CoMorbility.CoMobilityText, Mw_MasterConfig.Display ");
                    knsql.append(" FROM Tw_CoMorbility ");
                    knsql.append(" LEFT JOIN Mw_MasterConfig ON Tw_CoMorbility.CoMobilityId = Mw_MasterConfig.MasterId AND Mw_MasterConfig.GroupName = 'PersonalData' AND Mw_MasterConfig.MasterName = 'CoMobility' ");
                    knsql.append(" WHERE Tw_CoMorbility.PatientId = ?patientId ");
                    knsql.setParameter("patientId", patientId);
                    HashMap<String, String> configFieldCM = new HashMap();
                    configFieldCM.put("CoMobilityId", "coMobilityId");
                    configFieldCM.put("CoMobilityText", "coMobilityText");
                    configFieldCM.put("Display", "display");
                    org.json.JSONArray jsonCM = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldCM).toString());
                    
                    // InitialDiagnosis
                    knsql = new KnSQL(this);
                    knsql.append("SELECT Tw_InitialDiagnosis.InitialDiagnosisId , Tw_InitialDiagnosis.initialDiagnosisText , Mw_MasterConfig.Display ");
                    knsql.append("FROM Tw_InitialDiagnosis ");
                    knsql.append("LEFT JOIN Mw_MasterConfig ON Tw_InitialDiagnosis.InitialDiagnosisId = Mw_MasterConfig.MasterId AND Mw_MasterConfig.GroupName = 'PersonalData' AND Mw_MasterConfig.MasterName = 'InitialDiagnosis' ");
                    knsql.append("WHERE Tw_InitialDiagnosis.PatientId = ?patientId ");
                    knsql.setParameter("patientId", patientId);
                    HashMap<String, String> configFieldID = new HashMap();
                    configFieldID.put("InitialDiagnosisId", "initialDiagnosisId");
                    configFieldID.put("InitialDiagnosisText", "initialDiagnosisText");
                    configFieldID.put("Display", "display");
                    org.json.JSONArray jsonID = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldID).toString());
                    String diagnosis = "";
                    for (int i = 0; i < jsonID.length(); i++) {
                        org.json.JSONObject jsonObjectElement = jsonID.getJSONObject(i);
                        String initialDiagnosisText = "".equals(jsonObjectElement.getString("initialDiagnosisText")) ? "" : " " + jsonObjectElement.getString("initialDiagnosisText");
                        if (!"".equals(diagnosis)) {
                            diagnosis += ", " + jsonObjectElement.getString("display") + initialDiagnosisText;
                        } else {
                            diagnosis = jsonObjectElement.getString("display") + initialDiagnosisText;
                        }
                    }
                    for (int i = 0; i < jsonCM.length(); i++) {
                        org.json.JSONObject jsonObjectElement = jsonCM.getJSONObject(i);
                        String coMobility = "".equals(jsonObjectElement.getString("coMobilityText")) ? "" : " " + jsonObjectElement.getString("coMobilityText");
                        if (!"".equals(diagnosis)) {
                            diagnosis += ", " + jsonObjectElement.getString("display") + coMobility;
                        } else {
                            diagnosis = jsonObjectElement.getString("display") + coMobility;
                        }
                    }
                    jsonHeader.put("diagnosis", diagnosis);
                    body.put("body", jsonHeader);
                    return result;
                }

            };
            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body.get("body"));
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
