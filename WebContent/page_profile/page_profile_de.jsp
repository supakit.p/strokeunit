<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE005PBean" scope="session" class="com.fs.bean.SFTE005PBean"/>

	<div id="entrylayer" class="entry-layer">
		<form id="fsentryform" name="fsentryform" method="post" action="page_profile_de_c.jsp">	
			<input type="hidden" name="fsAction" value="update"/>
			<input type="hidden" name="fsAjax" value="true"/>
			<input type="hidden" name="fsDatatype" value="json"/>
			<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
			<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
			<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>	
			<input type="hidden" id="userid" name="userid" value="${fsSFTE005PBean.userid}"/>
			<div class="row portal-area sub-entry-layer">
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="usertname_label" tagclass="control-label" required="true">First Name(Thai)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="usertname" name="usertname" placeholder="Last Name(Thai)" autocomplete="off" size="50" value="${fsSFTE005PBean.usertname}"/>
							<div id="usertname_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('usertname_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="usertsurname_label" tagclass="control-label" required="true">Last Name(Thai)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="usertsurname" name="usertsurname" placeholder="Last Name(Thai)" autocomplete="off" size="50" value="${fsSFTE005PBean.usertsurname}"/>
							<div id="usertsurname_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('usertsurname_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="userename_label" tagclass="control-label" required="true">First Name(English)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="userename" name="userename" placeholder="First Name(English)" autocomplete="off" size="50" value="${fsSFTE005PBean.userename}"/>
							<div id="userename_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('userename_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="useresurname_label" tagclass="control-label" required="true">Last Name(English)</fs:label>
						</div>
						<div class="col-md-5 col-height">
							<input class="form-control input-md alert-input" id="useresurname" name="useresurname" placeholder="Last Name(English)" autocomplete="off" size="50" value="${fsSFTE005PBean.useresurname}"/>
							<div id="useresurname_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('useresurname_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="email_label" tagclass="control-label">Email</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<input class="form-control input-md alert-input" id="email" name="email" placeholder="Email" autocomplete="off" size="30" value="${fsSFTE005PBean.email}"/>
							<div id="email_alert" role="alert" class="has-error" style="display:none;">${fsLabel.getText('email_alert','You can not leave this empty')}</div>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="mobile_label" tagclass="control-label">Mobile</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<input class="form-control input-md" id="mobile" name="mobile" placeholder="Mobile Phone" autocomplete="off" size="20" value="${fsSFTE005PBean.mobile}"/>
						</div>
				</div>
				<div class="row row-height">
						<div class="col-md-3 col-height col-label text-right">
							<fs:label tagid="lineno_label" tagclass="control-label">Line ID</fs:label>
						</div>
						<div class="col-md-3 col-height">
							<input class="form-control input-md" id="lineno" name="lineno" placeholder="Line ID" autocomplete="off" size="50" value="${fsSFTE005PBean.lineno}"/>
						</div>
				</div>
				<div class="row row-height">
					<div class="col-md-4 pull-right text-right" style="margin-right: 10px;">
						<input type="button" id="savebutton" class="btn btn-dark btn-sm" value="${fsLabel.getText('savebutton','Save')}"/>
					</div>
				</div>
			</div>
		</form>
	</div>