var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("page_profile"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
		setupComponents();
		setupAlertComponents();
}
function setupComponents() {
	$("#savebutton").click(function() { 
		save();
		return false;
	});
}
function clearingFields() {
}
function validForm() {
	clearAlerts();
	var validator = null;
	if($.trim($("#usertname").val())=="") {
		$("#usertname").parent().addClass("has-error");
		$("#usertname_alert").show();
		if(!validator) validator = "usertname";
	}
	if($.trim($("#usertsurname").val())=="") {
		$("#usertsurname").parent().addClass("has-error");
		$("#usertsurname_alert").show();
		if(!validator) validator = "usertsurname";
	}
	if($.trim($("#userename").val())=="") {
		$("#userename").parent().addClass("has-error");
		$("#userename_alert").show();
		if(!validator) validator = "userename";
	}
	if($.trim($("#useresurname").val())=="") {
		$("#useresurname").parent().addClass("has-error");
		$("#useresurname_alert").show();
		if(!validator) validator = "useresurname";
	}
	if($.trim($("#email").val())=="") {
		$("#email").parent().addClass("has-error");
		$("#email_alert").show();
		if(!validator) validator = "email";
	}
	if(validator) {
		$("#"+validator).focus();
		setTimeout(function() { 
			$("#"+validator).parent().addClass("has-error");
			$("#"+validator+"_alert").show();
		},100);
		return false;
	}
	/*
	if($.trim($("#usertname").val())=="") {
		alertmsg("QS0114",$("#usertname_label").html()+" is undefined");
		return false;
	}
	if($.trim($("#usertsurname").val())=="") {
		alertmsg("QS0115",$("#usertsurname_label").html()+" is undefined");
		return false;
	}
	*/
	return true;
}
function save(aform) {
		if(!aform) aform = fsentryform;
		//alert($(aform).serialize());
		if(!validForm()) return false;
		confirmSave(function() {
			startWaiting();
			var xhr = jQuery.ajax({
				url: "page_profile_de_c.jsp",
				type: "POST",
				data: $(aform).serialize(),
				dataType: "html",
				contentType: defaultContentType,
				error : function(transport,status,errorThrown) { 
					submitFailure(transport,status,errorThrown); 
				},
				success: function(data,status,transport){ 
					stopWaiting();
					successbox(function() { clearingFields(); });					
				}
			});
		});
		return false;
}

