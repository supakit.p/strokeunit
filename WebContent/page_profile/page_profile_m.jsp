<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<c:if test="${fsScreen.init('page_profile',pageContext.request, pageContext.response,true)}"></c:if>
<!DOCTYPE html>
<html>
	<head>
		<title>User Information</title>
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createImportScripts('page_profile',pageContext.request,pageContext.response)}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/scripts.jsp"/>
		<link rel="stylesheet" type="text/css" href="../css/program_style.css" />
		<link rel="stylesheet" type="text/css" href="page_profile.css?${fsScreen.currentTime()}" />
		<script  type="text/javascript" src="page_profile.js?${fsScreen.currentTime()}"></script>				
	</head>
	<body class="portalbody portalbody-off">
		<div id="fsdialoglayer" style="display:none;"><span id="fsmsgbox"></span></div>
		<div id="fsacceptlayer" style="display:none;"><span id="fsacceptbox"></span></div>
		<div id="fswaitlayer" style="display:none; position:absolute; left:1px; top:1px; z-Index:9999;"><img id="waitImage" class="waitimgclass" src="../images/waiting.gif" width="50px" height="50px" alt=""></img></div>	
		<div id="page_profile" class="pt-page pt-page-current pt-page-controller">
			<h1 class="page-header-title" title="page_profile">User Information</h1>
			<div id="entrypanel">
					<jsp:include page="page_profile_de.jsp"/>
			</div>
		</div>
	</body>
</html>
