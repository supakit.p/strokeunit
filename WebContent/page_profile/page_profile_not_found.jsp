<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE005Bean" scope="session" class="com.fs.bean.SFTE005Bean"/>
<c:if test="${fsScreen.init('page_profile',pageContext.request, pageContext.response,true)}"></c:if>
<!DOCTYPE html>
<html>
	<head>
		<title>User Information</title>		
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createScripts(pageContext.request, pageContext.response)}" escapeXml="false"></c:out>
		<link rel="stylesheet" type="text/css" href="page_profile.css" />
	</head>
	<body class="portalbody portalbody-off">
		<div id="page_profile" class="pt-page pt-page-current pt-page-controller">
			<h1 class="page-header-title" title="page_profile">User Information</h1>
			<div id="entrypanel" style="text-align:center; font-size:25px;">
					Profile not found
			</div>
		</div>
	</body>
</html>
