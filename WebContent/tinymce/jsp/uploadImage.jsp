<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.*"%> 
<%@ page import="com.fs.dev.TheUtility"%>
<%@ page language="java" import="com.jspsmart.upload.*"%> 
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="smartUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/> 
<%	
	String filename = "";
	String resources_path = TheUtility.getResourcesPath(request);
	String loadpath = resources_path+java.io.File.separator+"docimages"+java.io.File.separator;
	System.out.println("image path : "+loadpath);
	smartUpload.initialize(pageContext);  	
	smartUpload.setTotalMaxFileSize(1000000000);
	smartUpload.upload();
	try { 
		boolean valid = true;
		java.io.File f = new java.io.File(loadpath);
		if(!f.exists()) {
			valid = f.mkdirs();
		}
		if(valid) {
			filename = smartUpload.getFiles().getFile(0).getFileName();
			String extension = "";
			int index = filename.lastIndexOf(".");
			if(index>0) extension = filename.substring(index);
			String site = fsAccessor.getFsSite();
			String naming = com.fs.bean.util.BeanUtility.getNaming(site!=null && site.trim().length()>0?site.toUpperCase()+"_":"DOCIMG_","",true,true,true,"");
			String fullfilename = loadpath+naming+extension;
			System.out.println("upload "+filename+" as "+fullfilename);
			smartUpload.getFiles().getFile(0).saveAs(fullfilename);
			filename = naming+extension;
			//smartUpload.save(loadpath);
		}
	} catch (Exception ex) { 
		ex.printStackTrace();
	}		
%>
{"location":"<%=filename%>"}