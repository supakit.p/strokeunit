<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE006Bean" scope="session" class="com.fs.bean.SFTE006Bean"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<form name="fslistform" id="fslistform" method="post" autocomplete="off">
	<input type="hidden" name="fsAction" value="view"/>
	<input type="hidden" name="fsAjax" value="false"/>
	<input type="hidden" name="fsDatatype" value="text"/>
	<input type="hidden" name="fsRowid" value=""/>
	<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
	<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
	<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>
	<input type="hidden" name="site" />
<%
//#(11000) programmer code begin;
//#(11000) programmer code end;
%>
</form>
<div id="fseffectedtransactions" style="display:none;">${fsSFTE006Bean.effectedTransactions()}</div>
<table id="datatable" class="table table-bordered table-hover table-striped tablesorter">
	<thead>
		<tr>
			<th class="text-center" style="cursor: default;"><fs:label tagid="seqno_headerlabel" >No.</fs:label></th>
			<th class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=shortname&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="shortname_headerlabel">Company Short Name</fs:label></a></th>
<%
			//#(12000) programmer code begin;
%>
			<th class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=nameen&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="nameen_headerlabel">Company Name (English)</fs:label></a></th>
			<th class="text-center"><a href="javascript:void(0)" class="alink-sorter" onclick="submitOrder('fsAjax=true&fsSorter=nameth&fsChapter=${fsPager.chapter}&fsPage=${fsGlobal.fsPage}')"><fs:label tagid="nameth_headerlabel">Company Name (Thai)</fs:label></a></th>
			<th class="text-center"><fs:label tagid="productcontent_headerlabel">Products or Modules</fs:label></th>
<%
			//#(12000) programmer code end;
%>
			<th class="text-center" style="cursor: default;"><i class="fa fa-bolt" aria-hidden="true"></i></th>
<%
			//#(13000) programmer code begin;
			//#(13000) programmer code end;
%>
		</tr>
	</thead>
	<tbody id="datatablebody">							
		<c:choose>
			<c:when test="${fsSFTE006Bean.size() > 0}">
				<c:forEach var="fsElement" items="${fsSFTE006Bean.elements()}" varStatus="records">
					<c:if test="${!fsGlobal.nextChapter(records.count)}">
						<tr>
							<td class="text-center">${records.count}</td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count},'${fsElement.getString('site')}');">${fsElement.getString('shortname')}</a></td>
<%
							//#(14000) programmer code begin;
%>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count},'${fsElement.getString('site')}');">${fsElement.getString('nameen')}</a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count},'${fsElement.getString('site')}');">${fsElement.getString('nameth')}</a></td>
							<td><a href="javascript:void(0)" class="alink-data" onclick="submitRetrieve(${records.count},'${fsElement.getString('site')}');">${fsElement.getString('productcontent')}</a></td>
<%
							//#(14000) programmer code end;
%>
							<td class="text-center">
									<button name="btn-edit" type="button" class="btn-edit" title="${fsLabel.getLabel('btnedit_tooltip')}"
											onclick="submitRetrieve(${records.count},'${fsElement.getString('site')}');"></button>
							</td>
<%
							//#(15000) programmer code begin;
							//#(15000) programmer code end;
%>
						</tr>
					</c:if>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:if test="${!fsScreen.isViewState()}">
					<tr>
						<td class="text-center" colspan="6">
								${fsLabel.getLabel("recordnotfound")}
						</td>
					</tr>
				</c:if>
			</c:otherwise>
		</c:choose>		
	</tbody>
</table>	
<%
//#(16000) programmer code begin;
//#(16000) programmer code end;
%>
<div class="fschaptertablelayer" style="text-align: center;">
<table class="fschaptertable" style="margin: auto;">
	<tr class="fschapterrow"><td class="fschaptercolumn">
	<form name="fschapterform" id="fschapterform" method="post" autocomplete="off">
		<input type="hidden" name="fsAction" value="chapter"/>
		<input type="hidden" name="fsAjax" value="true"/>
		<input type="hidden" name="fsDatatype" value="text"/>
		<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
		<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
		<input type="hidden" name="fsPage" value="${fsGlobal.fsPage}"/>
		<input type="hidden" name="fsSorter" value=""/>
	</form>
	<div id="fschapterlayer">
<c:if test="${fsGlobal.hasPaging(fsPager.rows)}">
	${fsGlobal.createPaging(fsPager.rows)}
</c:if>
	</div>
	</td>
	</tr>
</table>
</div>	
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
