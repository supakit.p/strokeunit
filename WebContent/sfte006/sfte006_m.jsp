<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte006', pageContext.request, pageContext.response, true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE006Bean" scope="session" class="com.fs.bean.SFTE006Bean"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Company Product Setting</title>
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createImportScripts('sfte006',pageContext.request,pageContext.response)}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/scripts.jsp"/>
<%
//#(10500) programmer code begin;
//#(10500) programmer code end;
%>
		<link rel="stylesheet" type="text/css" href="../css/program_style.css?${fsScreen.currentTime()}" />
		<link rel="stylesheet" type="text/css" href="sfte006.css?${fsScreen.currentTime()}" />
		<script  type="text/javascript" src="sfte006.js?${fsScreen.currentTime()}"></script>
		<script  type="text/javascript" src="sfte006_ke.js?${fsScreen.currentTime()}"></script>
<%
//#(11000) programmer code begin;
//#(11000) programmer code end;
%>
	</head>
	<body class="portalbody portalbody-off">
		<c:out value="${fsScreen.createDialogLayer()}" escapeXml="false"></c:out>
		<c:out value="${fsScreen.createWaitLayer()}" escapeXml="false"></c:out>
		<div id="ptsearchpager" class="pt-page pt-page-current pt-page-controller sfte006-search-pager">
			<c:out value="${fsScreen.createHeaderLayer(pageContext.request,'sfte006',fsLabel.getText('caption','Company Product Setting'))}" escapeXml="false"></c:out>
			<div id="searchpanel" class="panel-body">
				<form id="fssearchform" name="fssearchform" method="post">
					<input type="hidden" name="clear" value="true"/>
					<input type="hidden" name="fsAction" value="collect"/>
					<input type="hidden" name="fsAjax" value="true"/>
					<input type="hidden" name="fsDatatype" value="text"/>
					<input type="hidden" name="fsChapter" value="${fsPager.chapter}"/>
					<input type="hidden" name="fsLimit" value="${fsPager.limit}"/>
					<input type="hidden" name="fsPage" value="1"/>
<%
//#(12000) programmer code begin;
//#(12000) programmer code end;
%>
					<jsp:include page="sfte006_me.jsp"/>
<%
//#(13000) programmer code begin;
//#(13000) programmer code end;
%>
				</form>
				<div id="listpanel" class="table-responsive" style="padding-top: 10px;">
					<jsp:include page="sfte006_d.jsp"/>
				</div>
<%
//#(14000) programmer code begin;
//#(14000) programmer code end;
%>
			</div>
<%
//#(15000) programmer code begin;
//#(15000) programmer code end;
%>
		</div>
		<div id="dialogpanel"></div>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
	</body>
</html>
<%
//#(30000) programmer code begin;
//#(30000) programmer code end;
%>
