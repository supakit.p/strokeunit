<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.init('sfte006', pageContext.request, pageContext.response, true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsSFTE006Bean" scope="session" class="com.fs.bean.SFTE006Bean"/>
<%
//#(10000) programmer code begin;
//#(10000) programmer code end;
boolean fsIsAjax = fsGlobal.isAjax();
fsGlobal.setFsProg("sfte006");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
String fs_forwarder = "/sfte006/sfte006_d.jsp";
//#(20000) programmer code begin;
//#(20000) programmer code end;
fsGlobal.attainPage(request,"fsGlobalsfte006");
try { 
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	fsSFTE006Bean.forceObtain(fsGlobal);
	fsSFTE006Bean.obtain(session,request);
	fsGlobal.obtainPage(request);
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
//#(50000) programmer code begin;
//#(50000) programmer code end;
fsPager.setRows(fsSFTE006Bean.size());
fsSFTE006Bean.obtain(session,request);
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
