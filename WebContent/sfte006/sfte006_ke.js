//#(10000) programmer code begin;
//#(10000) programmer code end;
$(function(){
	startEntryPage();
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
	handleControls(fs_radiocontrols,fs_checkboxcontrols,fs_selectcontrols);
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	initialControls(fs_radiocontrols,fs_checkboxcontrols,fs_selectcontrols);
	//#(40000) programmer code begin;
	//#(40000) programmer code end;
});
//#(50000) programmer code begin;
//#(50000) programmer code end;
function validSaveForm(callback) {
	return validRequiredFields(callback,fs_requiredfields);
}
function validSendForm(callback) {
	var valid = validCompletedFields(fs_completedfields);
	if(!valid) return false;
	return validRequiredFields(callback,fs_requiredfields);
}
var fs_radiocontrols = {

	//#(60000) programmer code begin;
	//#(60000) programmer code end;
};
var fs_checkboxcontrols = {

	//#(70000) programmer code begin;
	//#(70000) programmer code end;
};
var fs_selectcontrols = {

	//#(75000) programmer code begin;
	//#(75000) programmer code end;
};
var fs_requiredfields = {
	//#(80000) programmer code begin;
	//#(80000) programmer code end;
};
var fs_completedfields = {

	//#(90000) programmer code begin;
	//#(90000) programmer code end;
};
//#(100000) programmer code begin;
//#(100000) programmer code end;
