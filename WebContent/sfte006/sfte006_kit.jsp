<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>

<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE006" scope="request" class="com.fs.bean.SFTE006Bean"/>
<%
//#(5000) programmer code begin;
//#(5000) programmer code end;
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
	<input type="hidden" id="site" name="site" value="${fsSFTE006.getString('site')}"/>
	<div class="row row-height">
		<div class="col-md-12 col-height">
				<div class="table-layer-class">
					<div class="row row-height">
						<div class="col-height col-md-8">
<fs:label tagid="company_label" tagclass="control-label">Company</fs:label><input type="text" id="company" class="form-control input-md" value="${fsSFTE006.getString('compname')}" disabled/>
						</div>
					</div>
				</div>
		</div>
	</div>	
	<div class="row row-height">
		<div class="col-md-12 col-height">
							<fs:label tagid="product_label" tagclass="control-label">Products or Modules</fs:label>
							<div id="productslayer">
<%
	com.fs.bean.util.BeanUtility butil = new com.fs.bean.util.BeanUtility();
	boolean eng = com.fs.dev.TheUtility.isEnglish(request);
	java.util.List lists = fsSFTE006.list();
	if(lists!=null) {
		String fieldvalue = fsSFTE006.getProducts();
		int isz = lists.size();
		int counter = isz;
		int max = 2;
		int index = 0;
		java.util.Iterator it = lists.iterator();
		do {
			out.println("<div class=\"row row-height\">");
			for(int k=0;k<max;k++) {
				if(it.hasNext()) {
					index++;
					com.fs.bean.EntityData dt = (com.fs.bean.EntityData)it.next();
					if(dt!=null) {
						String verify = dt.getString("verified");
						String prod = dt.getString("product");
						String name = dt.getString("nameth");
						if(eng) name = dt.getString("nameen");
						boolean marked = butil.isInset(prod,fieldvalue);
						String checked = marked?"checked":"";
						String style = "1".equals(verify)?"color:red;":"color:navy;";
						%>
						<div class="col-height col-md-5">
<div class="checkbox"><input type="checkbox" value="<%=prod%>" id="product_<%=index%>" name="product" class="form-control input-md" <%=checked%>/><label for="product_<%=index%>" class="control-label" style="<%=style%>"><%=name%></label></div>
						</div>
						<%											
					}
				}
			}
			out.println("</div>");
			counter -= max;
		}while(counter>0);
	}
%>								
								
							</div>
		</div>
	</div>
<%
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
