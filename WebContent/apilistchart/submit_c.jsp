
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>

<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apiListChart',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("ListChart");
    header.setMethod("Submit");
    try {
        Trace.info("############### ListChart/Submit ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();
        final String fs_action = GlobalBean.RETRIEVE_MODE;
        final String fs_actionDel = GlobalBean.DELETE_MODE;
        final String fs_actionIns = GlobalBean.INSERT_MODE;
        final String fs_fromId = "ListChart";
        final SimpleDateFormat dfTime = new SimpleDateFormat("HH:mm:ss.SSS", java.util.Locale.US);
        if ((fs_apiName != null && fs_apiName.trim().length() > 0)) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                public int retrieve(java.sql.Connection connection) throws Exception {
                    //initial
                    TheStrokeUnit su = new TheStrokeUnit();
                    int result = 0;
//                    String patientId = json.getString("patientId");
                    String chartId = "";
                    String indicateMetricId = "";
                    String vsId = "";
                    String indicatePulseId = "";
                    String pulseDate = json.getString("pulseDate") + " " + json.getString("pulseTime");
                    json.put("chartDate", json.getString("pulseDate"));
                    json.put("pulseTime", json.getString("pulseTime"));
//                    json.put("pulseDate", pulseDate);
                    org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, fs_apiName);
                    if (jsonArrayConfigfield.length() <= 0) {
                        throw new RuntimeException("Not Found ApiName");
                    }
                    json.put("formId", fs_fromId);
                    if (json.has("indicateMetricId") && json.getString("indicateMetricId").trim().length() > 0) {
                        indicateMetricId = json.getString("indicateMetricId");
                    } else {
                        indicateMetricId = TheStrokeUnit.Generate.createUUID();
                        json.put("indicateMetricId", indicateMetricId);
                        body.put("indicateMetricId", indicateMetricId);
                    }
                    if (!(json.has("indicatePulseId") && json.getString("indicatePulseId").trim().length() > 0)) {
                        indicatePulseId = TheStrokeUnit.Generate.createUUID();
                        json.put("indicatePulseId", indicatePulseId);
                        body.put("indicatePulseId", indicatePulseId);
                    }
                    if (!(json.has("vsId") && json.getString("vsId").trim().length() > 0)) {
                        vsId = TheStrokeUnit.Generate.createUUID();
                        json.put("vsId", vsId);
                        body.put("vsId", vsId);
                    }
                    if(!(json.has("chartId") && json.getString("chartId").trim().length() > 0)){
                        chartId = TheStrokeUnit.Generate.createUUID();
                        json.put("chartId", chartId);
                        body.put("chartId", chartId);
                    }

                    org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);

                    org.json.JSONArray tempDelMetricList = new org.json.JSONArray();
                    org.json.JSONArray tempDelProcessList = new org.json.JSONArray();
                    org.json.JSONArray tempDelDietList = new org.json.JSONArray();
                    for (int i = 0; i < jsonArrayTableListConfig.length(); i++) {
                        org.json.JSONObject jsonObjectElement = jsonArrayTableListConfig.getJSONObject(i);
                        if ("Tw_Metric".equals(jsonObjectElement.getString("tableName"))) {
                            tempDelMetricList.put(jsonObjectElement);
                        } else if ("Tw_MetricProcess".equals(jsonObjectElement.getString("tableName"))) {
                            tempDelProcessList.put(jsonObjectElement);
                        } else if ("Tw_MetricDiet".equals(jsonObjectElement.getString("tableName"))) {
                            tempDelDietList.put(jsonObjectElement);
                        }
                    }

                    result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();

                    org.json.JSONArray jsonArrayConfigfieldMetric = su.getConfigfield(connection, fs_apiName, "Tw_Metric");
                    org.json.JSONArray jsonArrayTableListConfigMetric = su.handleConfigAllData(jsonArrayConfigfieldMetric);

                    org.json.JSONArray jsonArrayConfigfieldIndicateMetricProcess = su.getConfigfield(connection, fs_apiName, "Tw_MetricProcess");
                    org.json.JSONArray jsonArrayTableListConfigMetricProcess = su.handleConfigAllData(jsonArrayConfigfieldIndicateMetricProcess);

                    org.json.JSONArray jsonArrayConfigfieldMetricDiet = su.getConfigfield(connection, fs_apiName, "Tw_MetricDiet");
                    org.json.JSONArray jsonArrayTableListConfigMetricDiet = su.handleConfigAllData(jsonArrayConfigfieldMetricDiet);

                    if (json.has("metricList")) {
                        org.json.JSONArray metricList = json.getJSONArray("metricList");
                        // Delete MetricList And ProcessList Before Insert/Update New Data
                        su.handleParams(connection, fs_actionDel, tempDelMetricList, json);
                        su.handleParams(connection, fs_actionDel, tempDelProcessList, json);
                        su.handleParams(connection, fs_actionDel, tempDelDietList, json);

                        for (int i = 0; i < metricList.length(); i++) {
                            org.json.JSONObject metric = metricList.getJSONObject(i);
                            if (metric.has("metricInfo")) {
                                org.json.JSONArray metricInfoList = metric.getJSONArray("metricInfo");
                                for (int j = 0; j < metricInfoList.length(); j++) {
                                    org.json.JSONObject metricInfo = metricInfoList.getJSONObject(j);
                                    String metricId = TheStrokeUnit.Generate.createUUID();
                                    metricInfo.put("metricType", metric.get("metricType"));
                                    metricInfo.put("metricName", metric.get("metricName"));
                                    metricInfo.put("metricId", metricId);
                                    metricInfo.put("indicateMetricId", indicateMetricId);
                                    result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigMetric, metricInfo).getEffectedTransactions();
                                    if (metricInfo.has("metricProcess")) {
                                        org.json.JSONArray processList = metricInfo.getJSONArray("metricProcess");
                                        for (int k = 0; k < processList.length(); k++) {
                                            org.json.JSONObject process = processList.getJSONObject(k);
                                            process.put("metricId", metricId);
                                            process.put("indicateMetricId", indicateMetricId);
                                            result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigMetricProcess, process).getEffectedTransactions();
                                        }
                                    }
                                    if (metricInfo.has("diet")) {
                                        System.err.println("diet jaaaaaaaaaa #############################");
                                        org.json.JSONArray dietList = metricInfo.getJSONArray("diet");
                                        System.err.println("dietList : " + dietList);
                                        for (int k = 0; k < dietList.length(); k++) {
                                            org.json.JSONObject diet = dietList.getJSONObject(k);
                                            System.err.println("diet : " + diet);
                                            diet.put("metricId", metricId);
                                            diet.put("indicateMetricId", indicateMetricId);
                                            if (diet.has("subDiet")) {
                                                org.json.JSONArray subDietList = diet.getJSONArray("subDiet");
                                                System.err.println("subDietList : " + subDietList);
                                                for (int l = 0; l < subDietList.length(); l++) {
                                                    org.json.JSONObject subDiet = subDietList.getJSONObject(l);
                                                    System.err.println("subDiet : " + subDiet);
                                                    diet.put("subDietId", subDiet.getString("subDietId"));
                                                    result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigMetricDiet, diet).getEffectedTransactions();
                                                }
                                            } else {
                                                result += su.handleParams(connection, "insertNoKey", jsonArrayTableListConfigMetricDiet, diet).getEffectedTransactions();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Trace.info("body : " + body);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);

            if (fsExecuter.effectedTransactions()
                    > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
