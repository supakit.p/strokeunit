<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.Duration"%>
<%@page import="java.time.temporal.ChronoUnit"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("ListChart");
    header.setMethod("request");
    try {
        Trace.info("############### ListChart/request ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_data = fsBean.getDataValue("data").toString();
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", java.util.Locale.US);
        final SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.US);
        final SimpleDateFormat dfTime = new SimpleDateFormat("HH:mm:ss.SSS", java.util.Locale.US);
        if (fs_data != null && fs_data.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    String patientId = json.getString("patientId");
                    Calendar calEndDate = Calendar.getInstance();
                    calEndDate.set(Calendar.HOUR_OF_DAY, 23);
                    calEndDate.set(Calendar.MINUTE, 59);
                    calEndDate.set(Calendar.SECOND, 59);
                    Date endDate = json.has("endDate") && json.getString("endDate").trim().length() > 0 ? df.parse(json.getString("endDate")) : calEndDate.getTime();
                    Calendar calStartDate = Calendar.getInstance(java.util.Locale.US);
                    calStartDate.setTime(endDate);
                    calStartDate.add(Calendar.DATE, -6); // Between 7 day
                    calStartDate.set(Calendar.HOUR_OF_DAY, 0);
                    calStartDate.set(Calendar.MINUTE, 0);
                    calStartDate.set(Calendar.SECOND, 0);

                    Date startDate = calStartDate.getTime();

                    TheStrokeUnit su = new TheStrokeUnit();
                    KnSQL knsql = new KnSQL(this);
                    knsql.append("SELECT Tw_ListChart.ChartId , Tw_IndicatePulse.IndicatePulseId ,Tw_IndicatePulse.PatientId ,ChartDate , PulseTime, Tw_IndicatePulse.PainScale ");
                    knsql.append(",Tw_IndicatePulse.Height ,Tw_IndicatePulse.Weight ,Tw_VitalSign.VitalSignId ,Tw_VitalSign.Temperature ,Tw_VitalSign.RespiratoryRate ,Tw_VitalSign.HeartRate ,Tw_VitalSign.SBP ,Tw_VitalSign.DBP ,Tw_VitalSign.OxygenSat ");
                    knsql.append(" FROM Tw_ListChart ");
                    knsql.append(" LEFT JOIN  Tw_IndicatePulse ON Tw_ListChart.ChartId = Tw_IndicatePulse.ChartId ");
                    knsql.append(" LEFT JOIN Tw_VitalSign ON Tw_VitalSign.VitalSignId = Tw_IndicatePulse.VitalSignId ");
                    knsql.append(" WHERE Tw_ListChart.PatientId =?patientId AND (Tw_ListChart.ChartDate BETWEEN ?startDate AND ?endDate) ");
                    knsql.append(" ORDER BY ChartDate ASC , Tw_IndicatePulse.PulseTime ASC");
                    knsql.setParameter("patientId", patientId);
                    knsql.setParameter("startDate", df2.format(startDate));
                    knsql.setParameter("endDate", df.format(endDate));
                    HashMap<String, String> configFieldIP = new HashMap();
                    configFieldIP.put("ChartId", "chartId");
                    configFieldIP.put("IndicatePulseId", "indicatePulseId");
                    configFieldIP.put("PatientId", "patientId");
                    configFieldIP.put("ChartDate", "pulseDate");
                    configFieldIP.put("PulseTime", "pulseTime");
                    configFieldIP.put("PainScale", "painScale");
                    configFieldIP.put("Height", "height");
                    configFieldIP.put("Weight", "weight");
                    configFieldIP.put("VitalSignId", "vsId");
                    configFieldIP.put("Temperature", "vsTemp");
                    configFieldIP.put("RespiratoryRate", "vsRR");
                    configFieldIP.put("HeartRate", "vsHR");
                    configFieldIP.put("SBP", "vsSBP");
                    configFieldIP.put("DBP", "vsDBP");
                    configFieldIP.put("OxygenSat", "vsO2sat");

                    org.json.JSONArray jsonIndicatePulseList = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldIP).toString());
                    result += jsonIndicatePulseList.length();

                    //Check First Time
                    KnSQL knsqlVS = new KnSQL(this);
                    knsqlVS.append(" SELECT Tw_PatientInfo.Weight ,  Tw_PatientInfo.Height");
                    knsqlVS.append(" FROM Tw_PatientInfo ");
                    knsqlVS.append(" WHERE Tw_PatientInfo.patientId =?patientId ");
                    knsqlVS.setParameter("patientId", patientId);
                    HashMap<String, String> configFieldVS = new HashMap();
                    configFieldVS.put("Height", "height");
                    configFieldVS.put("Weight", "weight");
                    org.json.JSONObject jsonVSFirst = new org.json.JSONObject(su.requestData(connection, knsqlVS, false, configFieldVS).toString());
                    System.err.println("jsonVSFirst : " + jsonVSFirst);
                    String height = "";
                    String weight = "";
                    if (jsonVSFirst.length() > 0) {
                        height = jsonVSFirst.getString("height");
                        weight = jsonVSFirst.getString("weight");
                    }
/*
                    Date pulseDateTimeFirstTime;
                    String pulseDateFirstTime = "";
                    String validateCheck = "0";
                    org.json.JSONObject jsonVSFirstTime6 = new org.json.JSONObject();
                    org.json.JSONObject jsonVSFirstTime18 = new org.json.JSONObject();
                    if (jsonVSFirst.length() > 0) {
                        pulseDateTimeFirstTime = df.parse(jsonVSFirst.getString("pulseDate"));
                        pulseDateFirstTime = df2.format(pulseDateTimeFirstTime);

                        //add Defualt   
                        jsonVSFirstTime6.put("indicatePulseId", "");
                        jsonVSFirstTime6.put("patientId", patientId);
                        jsonVSFirstTime6.put("painScale", "");
                        jsonVSFirstTime6.put("vsId", "");
                        jsonVSFirstTime6.put("vsTemp", "");
                        jsonVSFirstTime6.put("vsRR", "");
                        jsonVSFirstTime6.put("vsHR", "");
                        jsonVSFirstTime6.put("vsO2sat", "");
                        jsonVSFirstTime6.put("vsDBP", jsonVSFirst.getString("vsDBP"));
                        jsonVSFirstTime6.put("vsSBP", jsonVSFirst.getString("vsSBP"));
                        jsonVSFirstTime6.put("weight", jsonVSFirst.getString("weight"));
                        jsonVSFirstTime6.put("height", jsonVSFirst.getString("height"));
                        jsonVSFirstTime6.put("pulseDate", pulseDateFirstTime);
                        // time wrong fomat
                        jsonVSFirstTime6.put("pulseTime", "06:00:00.000");

                        //add Defualt   
                        jsonVSFirstTime18.put("indicatePulseId", "");
                        jsonVSFirstTime18.put("patientId", patientId);
                        jsonVSFirstTime18.put("painScale", "");
                        jsonVSFirstTime18.put("vsId", "");
                        jsonVSFirstTime18.put("vsTemp", "");
                        jsonVSFirstTime18.put("vsRR", "");
                        jsonVSFirstTime18.put("vsHR", "");
                        jsonVSFirstTime18.put("vsO2sat", "");
                        jsonVSFirstTime18.put("vsDBP", jsonVSFirst.getString("vsDBP"));
                        jsonVSFirstTime18.put("vsSBP", jsonVSFirst.getString("vsSBP"));
                        jsonVSFirstTime18.put("weight", jsonVSFirst.getString("weight"));
                        jsonVSFirstTime18.put("height", jsonVSFirst.getString("height"));
                        jsonVSFirstTime18.put("pulseDate", pulseDateFirstTime);
                        // time wrong fomat
                        jsonVSFirstTime18.put("pulseTime", "18:00:00.000");
                    }
*/
                    //set Chart No
                    long temp = -1;
                    org.json.JSONArray tmpIndicatePulseList = new org.json.JSONArray();
                    org.json.JSONObject tmpIndicatePulse = new org.json.JSONObject();
                    org.json.JSONArray indicatePulseListTime = new org.json.JSONArray();

                    for (int i = 0; i < jsonIndicatePulseList.length(); i++) {

                        org.json.JSONObject jsonObjectElement = jsonIndicatePulseList.getJSONObject(i);
                        String chartId = jsonObjectElement.getString("chartId");
                        jsonObjectElement.put("height", height);
                        Trace.info("pulseDate + pulseTime" + jsonObjectElement.getString("pulseDate") + " " + jsonObjectElement.getString("pulseTime"));
                        Date pulseDateTime = df.parse(jsonObjectElement.getString("pulseDate") + " " + jsonObjectElement.getString("pulseTime"));
//                        String pulseDate = df2.format(pulseDateTime);
                        String pulseDate = jsonObjectElement.getString("pulseDate");
                        String pulseTime = jsonObjectElement.getString("pulseTime");
                        // time wrong fomat
//                        String pulseTime = dfTime.format(pulseDateTime);
                        jsonObjectElement.put("pulseDate", pulseDate);
                        jsonObjectElement.put("pulseTime", pulseTime);
                        

                        /*
                        //Check First Time
                        System.err.println("pulseDate : " + pulseDate + "pulseDateFirstTime : " + pulseDateFirstTime);
                        if (pulseDate.equals(pulseDateFirstTime)) {
                            if ("06:00:00.000".equals(pulseTime)) {
                                validateCheck = "1";
                            } else if ("18:00:00.000".equals(pulseTime)) {
                                if ("1".equals(validateCheck)) {
                                    validateCheck = "3";
                                } else {
                                    validateCheck = "2";
                                }
                            }
                        }
                        */

                        Calendar calPulseDate = Calendar.getInstance(java.util.Locale.US);
                        calPulseDate.setTime(pulseDateTime);
                        calPulseDate.set(Calendar.SECOND, 1);
                        pulseDateTime = calPulseDate.getTime();
                        long diff = pulseDateTime.getTime() - startDate.getTime();
                        long chartNo = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                        jsonObjectElement.put("chartNo", chartNo);

                        if (temp == -1) {
                            temp = chartNo;
                            tmpIndicatePulse.put("chartNo", chartNo);
                            tmpIndicatePulse.put("chartId", chartId);
                            indicatePulseListTime.put(jsonObjectElement);
                        } else if (temp == chartNo) {
                            indicatePulseListTime.put(jsonObjectElement);
                        } else {
                            /*
                            //Check First Time
                            if (jsonVSFirst.length() > 0) {
                                System.err.println("temp : " + temp + " chartNo : " + chartNo);
                                System.err.println("validateCheck : " + validateCheck);
                                jsonVSFirstTime6.put("chartNo", temp);
                                jsonVSFirstTime18.put("chartNo", temp);
                                if (!"4".equals(validateCheck)) {
                                    if ("1".equals(validateCheck)) {
                                        indicatePulseListTime.put(jsonVSFirstTime18);
                                    } else if ("2".equals(validateCheck)) {
                                        indicatePulseListTime.put(jsonVSFirstTime6);
                                    } else if ("0".equals(validateCheck)) {
                                        indicatePulseListTime.put(jsonVSFirstTime6);
                                        indicatePulseListTime.put(jsonVSFirstTime18);
                                    }
                                    validateCheck = "4";
                                }
                            }
                             */
                            temp = chartNo;
                            tmpIndicatePulse.put("indicatePulseListTime", indicatePulseListTime);
                            tmpIndicatePulseList.put(tmpIndicatePulse);
                            tmpIndicatePulse = new org.json.JSONObject();
                            indicatePulseListTime = new org.json.JSONArray();
                            tmpIndicatePulse.put("chartNo", chartNo);
                            tmpIndicatePulse.put("chartId", chartId);
                            indicatePulseListTime.put(jsonObjectElement);
                        }
                    }
                    /*
                    //Check First Time
                    if (jsonVSFirst.length() > 0) {
                        if (!"4".equals(validateCheck)) {
                            jsonVSFirstTime6.put("chartNo", temp);
                            jsonVSFirstTime18.put("chartNo", temp);
                            if ("1".equals(validateCheck)) {
                                indicatePulseListTime.put(jsonVSFirstTime18);
                            } else if ("2".equals(validateCheck)) {
                                indicatePulseListTime.put(jsonVSFirstTime6);
                            } else if ("0".equals(validateCheck)) {
                                indicatePulseListTime.put(jsonVSFirstTime6);
                                indicatePulseListTime.put(jsonVSFirstTime18);
                            }
                            validateCheck = "4";
                        }
                    }
                     */
                    
                    tmpIndicatePulse.put("indicatePulseListTime", indicatePulseListTime);
                    tmpIndicatePulseList.put(tmpIndicatePulse);

                    knsql = new KnSQL(this);
                    knsql.append("SELECT Tw_IndicateMetric.IndicateMetricId ,Tw_IndicateMetric.PatientId , Tw_ListChart.ChartId ,ChartDate ,Tw_IndicateMetric.RemarkDate ,Tw_IndicateMetric.RemarkId ,Tw_IndicateMetric.Remark  ");
                    knsql.append(" FROM Tw_ListChart ");
                    knsql.append(" LEFT JOIN  Tw_IndicateMetric ON Tw_ListChart.ChartId = Tw_IndicateMetric.ChartId ");
                    knsql.append(" WHERE Tw_ListChart.PatientId = ?patientId AND (Tw_ListChart.ChartDate BETWEEN ?startDate AND ?endDate) ");
                    knsql.append(" ORDER BY Tw_ListChart.ChartDate ASC ");
                    knsql.setParameter("patientId", patientId);
                    knsql.setParameter("startDate", df2.format(startDate));
                    knsql.setParameter("endDate", df2.format(endDate));
                    HashMap<String, String> configFieldIM = new HashMap();
                    configFieldIM.put("IndicateMetricId", "indicateMetricId");
                    configFieldIM.put("ChartId", "chartId");
                    configFieldIM.put("PatientId", "patientId");
                    configFieldIM.put("ChartDate", "metricDate");
                    configFieldIM.put("RemarkDate", "remarkDate");
                    configFieldIM.put("RemarkId", "remarkId");
                    configFieldIM.put("Remark", "remark");

                    org.json.JSONArray jsonIndicateMetricList = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldIM).toString());
                    result += jsonIndicateMetricList.length();
                    org.json.JSONArray indicateMetric = new org.json.JSONArray();
                    for (int i = 0; i < jsonIndicateMetricList.length(); i++) {
                        org.json.JSONObject jsonObjectElement = jsonIndicateMetricList.getJSONObject(i);
                        String indicateMetricId = jsonObjectElement.getString("indicateMetricId");
                        knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_Metric.MetricId , Tw_Metric.MetricType , Tw_Metric.MetricName, Tw_Metric.IsNPO ,Tw_Metric.SeriesId ,Tw_Metric.MetricTime ,Tw_Metric.Value ,Tw_Metric.Unit  ");
                        knsql.append("FROM Tw_Metric ");
                        knsql.append("WHERE Tw_Metric.IndicateMetricId = ?indicateMetricId ");
                        knsql.append("ORDER BY Tw_Metric.MetricType , Tw_Metric.MetricName , Tw_Metric.MetricTime ");
                        knsql.setParameter("indicateMetricId", indicateMetricId);
                        HashMap<String, String> configFieldM = new HashMap();
                        configFieldM.put("MetricId", "metricId");
                        configFieldM.put("MetricType", "metricType");
                        configFieldM.put("MetricName", "metricName");
                        configFieldM.put("IsNPO", "npo");
                        configFieldM.put("SeriesId", "seriesId");
                        configFieldM.put("MetricTime", "metricTime");
                        configFieldM.put("Value", "value");
                        configFieldM.put("Unit", "unit");

                        org.json.JSONArray jsonMetricList = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldM).toString());
                        org.json.JSONArray metricList = new org.json.JSONArray();
                        String tempMetricType = "";
                        String tempMetricName = "";
                        org.json.JSONArray metricInfo = new org.json.JSONArray();
                        org.json.JSONObject metric = new org.json.JSONObject();
                        for (int j = 0; j < jsonMetricList.length(); j++) {
                            org.json.JSONObject jsonObjectElementMetric = jsonMetricList.getJSONObject(j);
                            String metricId = jsonObjectElementMetric.getString("metricId");
                            String metricType = jsonObjectElementMetric.getString("metricType");
                            String metricName = jsonObjectElementMetric.getString("metricName");
                            jsonObjectElementMetric.remove("metricType");
                            jsonObjectElementMetric.remove("metricName");
                            knsql = new KnSQL(this);
                            knsql.append("SELECT Tw_MetricProcess.ProcessStatus ");
                            knsql.append("FROM Tw_MetricProcess ");
                            knsql.append("WHERE Tw_MetricProcess.MetricId = ?metricId ");
                            knsql.setParameter("metricId", metricId);
                            HashMap<String, String> configFieldMP = new HashMap();
                            configFieldMP.put("ProcessStatus", "processStatus");

                            org.json.JSONArray jsonMetricProcess = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldMP).toString());
                            if (jsonMetricProcess.length() > 0) {
                                jsonObjectElementMetric.put("metricProcess", jsonMetricProcess);
                            }
                            knsql = new KnSQL(this);
                            knsql.append("SELECT Tw_MetricDiet.DietId , Tw_MetricDiet.SubDietId ");
                            knsql.append("FROM Tw_MetricDiet ");
                            knsql.append("WHERE Tw_MetricDiet.MetricId = ?metricId ");
                            knsql.setParameter("metricId", metricId);
                            HashMap<String, String> configFieldMD = new HashMap();
                            configFieldMD.put("DietId", "dietId");
                            configFieldMD.put("SubDietId", "subDietId");

                            org.json.JSONArray jsonMetricDietList = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldMD).toString());
                            if (jsonMetricDietList.length() > 0) {
                                String tempDietId = "";
                                org.json.JSONArray dietList = new org.json.JSONArray();
                                org.json.JSONObject diet = new org.json.JSONObject();
                                org.json.JSONArray subDietList = new org.json.JSONArray();
                                org.json.JSONObject subDiet = new org.json.JSONObject();
                                for (int k = 0; k < jsonMetricDietList.length(); k++) {
                                    org.json.JSONObject metricDiet = jsonMetricDietList.getJSONObject(k);
                                    String dietId = metricDiet.getString("dietId");
                                    String subDietId = metricDiet.getString("subDietId");
                                    if (k == 0) {
                                        tempDietId = dietId;
                                        diet.put("dietId", tempDietId);
                                        if (!"".equals(subDietId)) {
                                            subDiet.put("subDietId", subDietId);
                                            subDietList.put(subDiet);
                                        }
                                    } else {
                                        if (tempDietId.equals(dietId)) {
                                            if (!"".equals(subDietId)) {
                                                subDiet = new org.json.JSONObject();
                                                subDiet.put("subDietId", subDietId);
                                                subDietList.put(subDiet);
                                            }
                                        } else {
                                            if (subDietList.length() > 0) {
                                                diet.put("subDiet", subDietList);
                                            }
                                            dietList.put(diet);
                                            tempDietId = dietId;
                                            diet = new org.json.JSONObject();
                                            diet.put("dietId", tempDietId);
                                            subDietList = new org.json.JSONArray();
                                            if (!"".equals(subDietId)) {
                                                subDiet = new org.json.JSONObject();
                                                subDiet.put("subDietId", subDietId);
                                                subDietList.put(subDiet);
                                            }
                                        }
                                    }
                                }
                                if (subDietList.length() > 0) {
                                    diet.put("subDiet", subDietList);
                                }
                                dietList.put(diet);
                                jsonObjectElementMetric.put("diet", dietList);
                            }

                            if ("".equals(tempMetricType) && "".equals(tempMetricName)) {
                                tempMetricType = metricType;
                                tempMetricName = metricName;
                                metricInfo.put(jsonObjectElementMetric);
                            } else if (tempMetricType.equals(metricType)) {
                                if (tempMetricName.equals(metricName)) {
                                    metricInfo.put(jsonObjectElementMetric);
                                } else {
                                    metric.put("metricType", tempMetricType);
                                    metric.put("metricName", tempMetricName);
                                    metric.put("metricInfo", metricInfo);
                                    metricList.put(metric);
                                    metric = new org.json.JSONObject();
                                    metricInfo = new org.json.JSONArray();
                                    metricInfo.put(jsonObjectElementMetric);
                                    tempMetricType = metricType;
                                    tempMetricName = metricName;
                                }
                            } else {
                                metric.put("metricType", tempMetricType);
                                metric.put("metricName", tempMetricName);
                                metric.put("metricInfo", metricInfo);
                                metricList.put(metric);
                                metric = new org.json.JSONObject();
                                metricInfo = new org.json.JSONArray();
                                metricInfo.put(jsonObjectElementMetric);
                                tempMetricType = metricType;
                                tempMetricName = metricName;
                            }
                        }
                        //last for
                        metric.put("metricType", tempMetricType);
                        metric.put("metricName", tempMetricName);
                        metric.put("metricInfo", metricInfo);
                        metricList.put(metric);

                        jsonObjectElement.put("metricList", metricList);

                        //set Chart No
                        Date metricDate = df2.parse(jsonObjectElement.getString("metricDate"));
                        Calendar calmetricDate = Calendar.getInstance(java.util.Locale.US);
                        calmetricDate.setTime(metricDate);
                        calmetricDate.set(Calendar.SECOND, 1);
                        metricDate = calmetricDate.getTime();
                        long diff = metricDate.getTime() - startDate.getTime();
                        long chartNo = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                        jsonObjectElement.put("chartNo", chartNo);

                        indicateMetric.put(jsonObjectElement);
                    }

                    body.put("indicatePulseList", tmpIndicatePulseList);
                    body.put("indicateMetricList", indicateMetric);
//                    body.put("defaultHeigh", height);
//                    body.put("defaultWeigh", weight);
                    Trace.info("body : " + body);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
