<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("ListPatientBed");
    header.setMethod("request");
    try {
        Trace.info("############### ListPatientBed/request ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
//        fsBean.addSchema("bedId", java.sql.Types.VARCHAR, "bedId");
//        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
//        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
//        final String fs_bedId = fsBean.getString("bedId");
//        final String fs_apiName = fsBean.getString("apiName");
//        final String fs_data = fsBean.getDataValue("data").toString();
        if (true) {
            final JSONObject body = new JSONObject();
//            final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", java.util.Locale.US);
//            final String getDate = df.format(new Date());
//            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();

                    KnSQL knsql = new KnSQL(this);
                    knsql.append("SELECT Mw_BedAvail.BedId , Mw_BedAvail.BedNo ,Tw_PatientInfo.PatientId ,Tw_PatientInfo.HN  ,Tw_PatientInfo.AN ,Tw_PatientInfo.PatientName ,Tw_PatientInfo.Age ,Tw_PatientInfo.Gender ,Tw_PatientInfo.AdmissionDate ");
                    knsql.append("FROM Mw_BedAvail ");
                    knsql.append("LEFT JOIN Tw_PatientInfo ON Tw_PatientInfo.BedId = Mw_BedAvail.BedId AND Tw_PatientInfo.StatusCode IN ('N') AND Tw_PatientInfo.IsActive = '1' ");
                    
                    HashMap<String, String> configFieldPB = new HashMap();
                    configFieldPB.put("BedId", "bedId");
                    configFieldPB.put("BedNo", "bedNo");
                    configFieldPB.put("PatientId", "patientId");
                    configFieldPB.put("HN", "hn");
                    configFieldPB.put("AN", "an");
                    configFieldPB.put("PatientName", "patientName");
                    configFieldPB.put("Age", "age");
                    configFieldPB.put("Gender", "gender");
                    configFieldPB.put("AdmissionDate", "admissionDate");

                    org.json.JSONArray jsonListPatientBed = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldPB).toString());
                    result += jsonListPatientBed.length();
                    org.json.JSONArray jsonResult = new org.json.JSONArray();
                    for (int i = 0; i < jsonListPatientBed.length(); i++) {
                        String isAvailable = "Y";
                        org.json.JSONObject patientBed = jsonListPatientBed.getJSONObject(i);
                        org.json.JSONObject temp = new org.json.JSONObject();
                        temp.put("bedId", patientBed.getString("bedId"));
                        temp.put("bedNo", patientBed.getString("bedNo"));
                        patientBed.remove("bedId");
                        patientBed.remove("bedNo");
                        if (patientBed.has("patientId") && patientBed.getString("patientId").trim().length() > 0) {
                            isAvailable = "N";
                            temp.put("bedDetail", patientBed);
                            knsql = new KnSQL(this);
                            knsql.append("SELECT Tw_StrokeType.StrokeTypeId , Mw_StrokeType.StrokeName ,Mw_StrokeType.StrokeDisplay , Tw_StrokeType.Other  ");
                            knsql.append("FROM Tw_StrokeType ");
                            knsql.append("LEFT JOIN Mw_StrokeType ON Mw_StrokeType.StrokeTypeId = Tw_StrokeType.StrokeTypeId ");
                            knsql.append("WHERE PatientId = ?patientId ");
                            knsql.setParameter("patientId", patientBed.getString("patientId"));
                            HashMap<String, String> configFieldST = new HashMap();
                            configFieldST.put("StrokeTypeId", "strokeTypeId");
                            configFieldST.put("StrokeName", "strokeName");
                            configFieldST.put("StrokeDisplay", "strokeDisplay");
                            configFieldST.put("Other", "other");

                            org.json.JSONArray jsonStrokeType = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldST).toString());
                            StringBuffer strokeTypes = new StringBuffer();
                            for (int j = 0; j < jsonStrokeType.length(); j++) {
                                org.json.JSONObject strokeType = jsonStrokeType.getJSONObject(j);
                                if (strokeTypes.toString().trim().length() > 0) {
                                    strokeTypes.append(", ");
                                }
                                strokeTypes.append(strokeType.getString("strokeDisplay"));
                                if (strokeType.has("other")) {
                                    strokeTypes.append(" : ");
                                    strokeTypes.append(strokeType.getString("other"));
                                }
                            }
                            temp.put("strokeType", strokeTypes);
                        }
                        temp.put("isAvailable", isAvailable);
                        jsonResult.put(temp);
                    }

                    body.put("listPatientBed", jsonResult);
                    Trace.info("body : " + body);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
