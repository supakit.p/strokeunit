<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%
//#scrape & keeping say something anyway
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<% session.removeAttribute("fsGlobal"); %>
<jsp:useBean id="fsGlobal" scope="session" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsNavigateBean" scope="page" class="com.fs.bean.NavigateBean"/>
<jsp:setProperty name="fsNavigateBean" property="*"/>
<jsp:useBean id="fsExc005Bean" scope="session" class="com.fs.bean.ExecuteBean"/>
<%
//#import and uses wherever you will go
//#(10000) programmer code begin;
StringBuilder results = new StringBuilder("{}");
//#(10000) programmer code end;
//#if the condition come to me
//#(15000) programmer code begin;
//#(15000) programmer code end;
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsAjax = fsGlobal.isAjax();
fsGlobal.setFsProg("nav005");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
try {
	//#initialize & assigned variables sitting down here
	//#(20000) programmer code begin;
	fsGlobal.setFsAction(GlobalBean.COLLECT_MODE);
	//#(20000) programmer code end;
	//#cut like a knife
	//#(22500) programmer code begin;
	Trace.debug(fsAccessor,fsNavigateBean);
	String fs_currentUserid = fsAccessor.getFsUser();
	String fs_currentUsertype = fsAccessor.getFsVar("fsUsertype");
	int pageNo = fsNavigateBean.getPageNo();
	int resultsPerpage = fsNavigateBean.getResultsPerpage();
	if(fsNavigateBean.isFetching()) {
		String so = fsNavigateBean.getSearchoption();
		if(so==null || so.trim().length()<=0) so = "W";
		String fs_usertype = PageUtility.getParameter(request,"usertype");
		String fs_employeeid = PageUtility.getParameter(request,"employeeid");
		String fs_tname = PageUtility.getParameter(request,"usertname");
		String fs_tsurname = PageUtility.getParameter(request,"usertsurname");
		String fs_ename = PageUtility.getParameter(request,"userename");
		String fs_esurname = PageUtility.getParameter(request,"useresurname");
		String fs_email = PageUtility.getParameter(request,"email");
		String fs_mobile = PageUtility.getParameter(request,"mobile");
		String fs_sites = fsAccessor.getFsSites();
		String filter = "where";
		KnSQL knsql = fsExc005Bean.getKnSQL();
		knsql.clear();
		knsql.append("select tuserinfo.employeeid,tuserinfo.userid,tuserinfo.usertname,tuserinfo.usertsurname,tuserinfo.userename,tuserinfo.useresurname,tuserinfo.deptcode ");
		knsql.append("from tuserinfo ");
		if(fs_usertype!=null && fs_usertype.length()>0) {
			knsql.append("left join tuser on tuserinfo.userid = tuser.userid ");
			knsql.append(filter+" tuser.usertype in (?usertype) ");			
			knsql.setParameter("usertype",fs_usertype.split(","));
			filter = "and";
		}
		if(fs_sites!=null) {
			knsql.append(filter+" ( tuserinfo.site = ?site or tuserinfo.site in (?sites) ) ");
			knsql.setParameter("site",fsAccessor.getFsSite());			
			knsql.setParameter("sites",fs_sites.split(","));
			filter = "and";
		} else {
			knsql.append(filter+" tuserinfo.site = ?site ");
			knsql.setParameter("site",fsAccessor.getFsSite());			
			filter = "and";
		}
		if(fs_employeeid!=null && fs_employeeid.length()>0) {
			if("W".equals(so)) {
				knsql.append(filter+" tuserinfo.employeeid like ?employeeid ");
				knsql.setParameter("employeeid","%"+fs_employeeid+"%");			
			} else if("H".equals(so)) {
				knsql.append(filter+" tuserinfo.employeeid = ?employeeid ");
				knsql.setParameter("employeeid",fs_employeeid);
			} else if("S".equals(so)) {
				knsql.append(filter+" tuserinfo.employeeid like ?employeeid ");
				knsql.setParameter("employeeid","%"+fs_employeeid);
			} else {
				knsql.append(filter+" tuserinfo.employeeid like ?employeeid ");
				knsql.setParameter("employeeid",fs_employeeid+"%");
			}
			filter = "and";
		}
		if(fs_tname!=null && fs_tname.length()>0) {
			knsql.append(filter+" tuserinfo.usertname like ?usertname ");
			knsql.setParameter("usertname","%"+fs_tname+"%");
			filter = "and";
		}
		if(fs_tsurname!=null && fs_tsurname.length()>0) {
			knsql.append(filter+" tuserinfo.usertsurname like ?usertsurname ");
			knsql.setParameter("usertsurname","%"+fs_tsurname+"%");
			filter = "and";
		}
		if(fs_ename!=null && fs_ename.length()>0) {
			knsql.append(filter+" tuserinfo.userename like ?userename ");
			knsql.setParameter("userename","%"+fs_ename+"%");
			filter = "and";
		}
		if(fs_esurname!=null && fs_esurname.length()>0) {
			knsql.append(filter+" tuserinfo.useresurname like ?useresurname ");
			knsql.setParameter("useresurname","%"+fs_esurname+"%");
			filter = "and";
		}
		if(fs_email!=null && fs_email.length()>0) {
			knsql.append(filter+" tuserinfo.email like ?email ");
			knsql.setParameter("email","%"+fs_email+"%");
			filter = "and";
		}
		if(fs_mobile!=null && fs_mobile.length()>0) {
			knsql.append(filter+" tuserinfo.mobile like ?mobile ");
			knsql.setParameter("mobile","%"+fs_mobile+"%");
			filter = "and";
		}
		knsql.append(filter+" tuserinfo.inactive = '0' ");
		knsql.append(" order by userid ");
	//#(22500) programmer code end;
	fsGlobal.obtain(fsAccessor);
	BeanTransport fsTransport = TheTransportor.transport(fsGlobal,fsExc005Bean);
	//#do you remember to obtain the transporter result
	//#(25000) programmer code begin;
	//#(25000) programmer code end;
	if(!fsGlobal.isException()) {
		TheTransportor.handle(fsGlobal,fsExc005Bean);
	}
	if(fsTransport!=null) fsGlobal.setSuccess(fsTransport.isSuccess());
	//#mode handle & other values never be the same again
	//#(30000) programmer code begin;
	} else {
		if(fsNavigateBean.getSortname()!=null && fsNavigateBean.getSortname().trim().length()>0) {
			fsExc005Bean.sort(fsNavigateBean.getSortname(),fsNavigateBean.isInverse());
		}
	}
	String fs_lang = fsGlobal.getFsLanguage();
	if(fs_lang==null) fs_lang = "EN";
	results = new StringBuilder("{");
	if(fsExc005Bean.size()>0) {
		results.append("\"language\":\""+fs_lang+"\",");
		results.append(fsExc005Bean.toJSONArrayWithLimit(pageNo,resultsPerpage,"rows","cell",false,new String[]{"employeeid","usertname","usertsurname","userename","useresurname","userid","deptcode"}));
	}
	results.append("}");
	//#(30000) programmer code end;
} catch(Exception ex) {
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	//#what error found when it over
	//#(32500) programmer code begin;
	//#(32500) programmer code end;
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
//#before handle forward page
//#(35000) programmer code begin;
//#(35000) programmer code end;
%>
<%=results.toString()%>