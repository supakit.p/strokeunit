<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("P02");
    header.setMethod("Submit");
    try {
        Trace.info("############### P02/Submit ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");

        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();

        if (fs_apiName != null && fs_apiName.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            final String fs_action = GlobalBean.RETRIEVE_MODE;
            final String fs_actionIns = GlobalBean.INSERT_MODE;
//            final String fs_actionDel = GlobalBean.DELETE_MODE;
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {

                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();
                    org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, fs_apiName);
                    if (jsonArrayConfigfield.length() <= 0) {
                        throw new RuntimeException("Not Found ApiName");
                    }
                    String patientId = json.has("patientId") ? json.getString("patientId") : "";
                    String focusId = json.has("focusId") ? json.getString("focusId") : "";

                    String nursingFocusId = "";
                    if (json.has("nursingFocusId") && json.getString("nursingFocusId").trim().length() > 0) {
                        nursingFocusId = json.getString("nursingFocusId");
                    } else {
                        nursingFocusId = TheStrokeUnit.Generate.createUUID();
                        json.put("nursingFocusId", nursingFocusId);
                        body.put("nursingFocusId", nursingFocusId);
                    }

                    if ("".equals(patientId) || "".equals(focusId)) {
                        return 0;
                    }
                    Trace.info("############ " + fs_apiName + " " + patientId + " ############");
                    org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);

                    //delte Tw_GoalsList
                    KnSQL knsql = new KnSQL(this);
                    knsql.append("DELETE FROM Tw_GoalsList ");
                    knsql.append(" WHERE NursingFocusId = ?nursingFocusId ");
                    knsql.setParameter("nursingFocusId", json.getString("nursingFocusId"));
                    knsql.executeUpdate(connection);
                    //delte Tw_GoalsOther
                    knsql = new KnSQL(this);
                    knsql.append("DELETE FROM Tw_GoalsOther ");
                    knsql.append(" WHERE NursingFocusId = ?nursingFocusId ");
                    knsql.setParameter("nursingFocusId", json.getString("nursingFocusId"));
                    knsql.executeUpdate(connection);

                    if (json.has("goalsList")) {
                        org.json.JSONArray jsonGoalsList = json.getJSONArray("goalsList");
                        org.json.JSONArray jsonArrayConfigfieldGoalsList = su.getConfigfield(connection, fs_apiName, "Tw_GoalsList");
                        org.json.JSONArray jsonArrayTableListConfigGoalsList = su.handleConfigAllData(jsonArrayConfigfieldGoalsList);
                        for (int i = 0; i < jsonGoalsList.length(); i++) {
                            org.json.JSONObject jsonObjectElement = jsonGoalsList.getJSONObject(i);
                            jsonObjectElement.put("nursingFocusId", nursingFocusId);
                            jsonObjectElement.put("patientId", patientId);
                            jsonObjectElement.put("focusId", focusId);
                            result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigGoalsList, jsonObjectElement).getEffectedTransactions();
                        }
                    }

                    if (json.has("goalsOtherList")) {
                        org.json.JSONArray jsonGoalsOther = json.getJSONArray("goalsOtherList");
                        org.json.JSONArray jsonArrayConfigfieldGoalsOther = su.getConfigfield(connection, fs_apiName, "Tw_GoalsOther");
                        org.json.JSONArray jsonArrayTableListConfigGoalsOther = su.handleConfigAllData(jsonArrayConfigfieldGoalsOther);
                        for (int i = 0; i < jsonGoalsOther.length(); i++) {
                            org.json.JSONObject jsonObjectElement = jsonGoalsOther.getJSONObject(i);
                            jsonObjectElement.put("nursingFocusId", nursingFocusId);
                            jsonObjectElement.put("seq", i + 1);
                            if (!"".equals(jsonObjectElement.getString("goalsOther"))) {
                                result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigGoalsOther, jsonObjectElement).getEffectedTransactions();
                            }
                        }
                    }

                    result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();

                    Trace.info("body : " + body);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);

            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
