<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("P02");
    header.setMethod("request");
    try {
        Trace.info("############### P02/request ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();

        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();

        if (fs_apiName != null && fs_apiName.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {

                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();

                    String patientId = json.getString("patientId");
                    Trace.info("############ NursingFocus " + patientId + " ############");

                    // Tw_FocusList
                    KnSQL knsql = new KnSQL(this);
                    knsql.append("SELECT Tw_FocusList.NursingFocusId , Tw_FocusList.FocusId  , Mw_Focus.FocusName , Tw_FocusList.GoalsOther , Tw_FocusList.ActiveDate , Tw_FocusList.ResolvedDate , Tw_FocusList.TrackFollow ");
                    knsql.append(" FROM Tw_FocusList ");
                    knsql.append(" LEFT JOIN Mw_Focus ON Mw_Focus.FocusId = Tw_FocusList.FocusId ");
                    knsql.append(" LEFT JOIN Tw_PatientInfo ON Tw_PatientInfo.PatientId = Tw_FocusList.PatientId");
                    knsql.append(" WHERE Tw_FocusList.PatientId =?patientId ");
                    knsql.append(" ORDER BY Tw_FocusList.CreateDate ASC , Tw_FocusList.FocusId ASC ");
                    knsql.setParameter("patientId", patientId);
                    HashMap<String, String> configField = new HashMap();
                    configField.put("NursingFocusId", "nursingFocusId");
                    configField.put("FocusId", "focusId");
                    configField.put("FocusName", "focusName");
                    configField.put("GoalsOther", "goalsOther");
                    configField.put("ActiveDate", "activeDate");
                    configField.put("ResolvedDate", "resolvedDate");
                    configField.put("TrackFollow", "trackFollow");
                    org.json.JSONArray jsonFocusList = new org.json.JSONArray(su.requestData(connection, knsql, true, configField).toString());
                    result += jsonFocusList.length();
                    
                    //get RNP02
                    knsql = new KnSQL(this);
                    knsql.append("SELECT Tw_RNofFrom.InitiateRN ");
                    knsql.append(" FROM Tw_RNofFrom ");
                    knsql.append(" WHERE Tw_RNofFrom.PatientId =?patientId AND Tw_RNofFrom.FormId = 'RNP02' ");
                    knsql.setParameter("patientId", patientId);
                    HashMap<String, String> configFieldRN = new HashMap();
                    configFieldRN.put("InitiateRN", "rn");
                    org.json.JSONObject jsonRN = new org.json.JSONObject(su.requestData(connection, knsql, false, configFieldRN).toString());
                    String InitiateRNFocus = "";
                    if (jsonRN.length() > 0) {
                        InitiateRNFocus = jsonRN.getString("rn");
                    }
                    result += jsonRN.length();

                    for (int i = 0; i < jsonFocusList.length(); i++) {
                        org.json.JSONObject jsonObjectElement = jsonFocusList.getJSONObject(i);
                        String nursingFocusId = jsonObjectElement.getString("nursingFocusId");
                        // GoalsList
                        knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_GoalsList.GoalsId , Mw_FocusGoals.GoalsName ");
                        knsql.append(" FROM Tw_GoalsList ");
                        knsql.append(" LEFT JOIN Mw_FocusGoals ON Mw_FocusGoals.GoalsId = Tw_GoalsList.GoalsId ");
                        knsql.append(" WHERE Tw_GoalsList.NursingFocusId = ?nursingFocusId ");
                        knsql.append(" ORDER BY Tw_GoalsList.GoalsId ASC ");
                        knsql.setParameter("nursingFocusId", nursingFocusId);
                        HashMap<String, String> configFieldCM = new HashMap();
                        configFieldCM.put("GoalsId", "goalsId");
                        configFieldCM.put("GoalsName", "goalsName");
                        org.json.JSONArray jsonGoalsList = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldCM).toString());
                        result += jsonGoalsList.length();

                        // GoalsOtherList
                        knsql = new KnSQL(this);
                        knsql.append("SELECT Tw_GoalsOther.GoalsOther ");
                        knsql.append(" FROM Tw_GoalsOther ");
                        knsql.append(" WHERE Tw_GoalsOther.NursingFocusId = ?nursingFocusId ");
                        knsql.append(" ORDER BY Tw_GoalsOther.Seq ASC ");
                        knsql.setParameter("nursingFocusId", nursingFocusId);
                        HashMap<String, String> configFieldGO = new HashMap();
                        configFieldGO.put("GoalsOther", "goalsOther");
                        org.json.JSONArray jsonGoalsOtherList = new org.json.JSONArray(su.requestData(connection, knsql, true, configFieldGO).toString());
                        result += jsonGoalsOtherList.length();

                        jsonFocusList.getJSONObject(i).put("goalsOtherList", jsonGoalsOtherList);
                        jsonFocusList.getJSONObject(i).put("goalsList", jsonGoalsList);
                        jsonFocusList.getJSONObject(i).put("focusNo", i + 1);
                    }
                    body.put("jsonFocusList", jsonFocusList);
                    body.put("rn", InitiateRNFocus);
                    Trace.info("body : " + body);
                    return result;

                }
            };

            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
