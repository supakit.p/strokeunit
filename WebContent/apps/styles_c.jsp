<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
StringBuffer results = new StringBuffer("{}");
boolean fsIsAjax = fsGlobal.isAjax();
fsGlobal.setFsProg("style");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
try { 
	java.util.Map fs_stylemap = (java.util.Map)session.getAttribute("STYLE_CATEGORY");
	if(fs_stylemap==null) {
		com.fs.dev.TheUtility smutil = new com.fs.dev.TheUtility(fsGlobal.getFsSection());
		fs_stylemap = smutil.createStyle(fsAccessor,fsGlobal.getFsSection(),"styles");
		if(fs_stylemap!=null) session.setAttribute("STYLE_CATEGORY",fs_stylemap);	
	}
	java.util.Map jsmap = new java.util.HashMap();
	if(fs_stylemap!=null) jsmap.put("stylecategory",fs_stylemap);	
	results = new StringBuffer(JSONUtility.parseJSON(jsmap,null,true));
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
}
%>
<%=results.toString()%>
