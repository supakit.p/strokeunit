<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%!
//#it's strong enough to break into another control here
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
<%
//#scrape & keeping say something anyway
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<% fsAccessor.validate(); %>
<% session.removeAttribute("fsGlobal"); %>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%if(!fsGlobal.bufferMode()||fsGlobal.insertMode()) session.removeAttribute("fsSFTE102Bean"); %>
<jsp:useBean id="fsSFTE102Bean" scope="session" class="com.fs.bean.SFTE102Bean"/>
<jsp:setProperty name="fsSFTE102Bean" property="*"/>
<%
//#import and uses wherever you will go
//#(10000) programmer code begin;
//#(10000) programmer code end;
//#if the condition come to me
//#(15000) programmer code begin;
//#(15000) programmer code end;
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsXMLData = fsGlobal.isXmldata();
boolean fsIsJSONData = fsGlobal.isJsondata();
fsGlobal.setFsProg("sfte102");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.setFsSection(null);
fsGlobal.obtain(session);
String fs_forwarder = "/sfte102/sfte102_cd.jsp";
try {
	fsSFTE102Bean.obtain(session,request);
	//#initialize & assigned variables sitting down here
	//#(20000) programmer code begin;
	Trace.debug(" Data ================ "+request.getParameter("data"));
	fsGlobal.setFsSection("STROKEMOBILE");
	fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
	//#(20000) programmer code end;
	//#cut like a knife
	//#(22500) programmer code begin;
	//#(22500) programmer code end;
	fsGlobal.obtain(fsAccessor);
	BeanTransport fsTransport = TheTransportor.transport(fsGlobal,fsSFTE102Bean);
	//#do you remember to obtain the transporter result
	//#(25000) programmer code begin;
	//#(25000) programmer code end;
	if(!fsGlobal.isException()) {
            System.err.println("Not ERROR");
		TheTransportor.handle(fsGlobal,fsSFTE102Bean);
	}
	if(fsTransport!=null) fsGlobal.setSuccess(fsTransport.isSuccess());
	//#mode handle & other values never be the same again
	//#(30000) programmer code begin;
	String fs_message = fsGlobal.getFsMessage();
        Trace.debug("fs_message============================="+fs_message);
        if(fsGlobal.isSuccess()) {
            if(fs_message!= null && fs_message.length()>0) {
                out.print(fs_message);
                return;
            }
        }
	//#(30000) programmer code end;
} catch(Throwable ex) {
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	int fs_errorcode = fsGlobal.parseErrorcode();
	String fs_errormessage = fsGlobal.getFsMessage();
	//#what error found when it over
	//#(32500) programmer code begin;
	//#(32500) programmer code end;
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response, fs_errorcode, fs_errormessage);
		return;
	}
}
//#before handle forward page
//#(35000) programmer code begin;
//#(35000) programmer code end;
fsSFTE102Bean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE102Bean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE102Bean.toJSON());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE102Bean.toXML());
	return;
}
//#born to try handle forward page
//#(40000) programmer code begin;
//#(40000) programmer code end;
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request,response);
%>
