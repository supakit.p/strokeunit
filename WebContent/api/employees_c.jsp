<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%!
//#it's strong enough to break into another control here
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
<%
//#scrape & keeping say something anyway
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<%--<% fsAccessor.validate(); %>--%>
<% session.removeAttribute("fsGlobal"); %>
<jsp:useBean id="fsGlobal" scope="session" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%--<%if(!fsGlobal.bufferMode()||fsGlobal.insertMode()) session.removeAttribute("fsSFTE101Bean"); %>--%>
<jsp:useBean id="fsSFTE101Bean" scope="session" class="com.fs.bean.SFTE101Bean"/>
<jsp:setProperty name="fsSFTE101Bean" property="*"/>
<%
//#import and uses wherever you will go
//#(10000) programmer code begin;
//#(10000) programmer code end;
//#if the condition come to me
//#(15000) programmer code begin;
//#(15000) programmer code end;
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsXMLData = fsGlobal.isXmldata();
boolean fsIsJSONData = fsGlobal.isJsondata();
fsGlobal.setFsProg("sfte101");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.setFsSection(null);
fsGlobal.obtain(session);
String fs_forwarder = "/sfte101/sfte101_cd.jsp";
try {
	fsSFTE101Bean.obtain(session,request);
	//#initialize & assigned variables sitting down here
	//#(20000) programmer code begin;
	java.util.Map map = (java.util.Map)request.getAttribute("formParameter");
	Trace.info("map >>>>>>>>> "+map);
	if(map!=null) fsSFTE101Bean.obtain(map);
	System.out.println("\n-------------------------------------------------------------------------------");
	System.out.println("                               Start apiEmployees                              ");
  System.out.println("-------------------------------------------------------------------------------");
  
  java.util.Enumeration qns = request.getParameterNames();
	for(;qns.hasMoreElements();) {
			String key = (String)qns.nextElement();
			Trace.debug("key=========================="+key+" = "+request.getParameter(key));
	}
  
	fsGlobal.setFsSection("PROMPT");
	fsGlobal.setFsAction(GlobalBean.COLLECT_MODE);
	//#(20000) programmer code end;
	//#cut like a knife
	//#(22500) programmer code begin;
	//#(22500) programmer code end;
	fsGlobal.obtain(fsAccessor);
	BeanTransport fsTransport = TheTransportor.transport(fsGlobal,fsSFTE101Bean);
	//#do you remember to obtain the transporter result
	//#(25000) programmer code begin;
	//#(25000) programmer code end;
	if(!fsGlobal.isException()) {
		TheTransportor.handle(fsGlobal,fsSFTE101Bean);
	}
	if(fsTransport!=null) fsGlobal.setSuccess(fsTransport.isSuccess());
	//#mode handle & other values never be the same again
	//#(30000) programmer code begin;
	String fs_message = fsGlobal.getFsMessage();
  Trace.debug("fs_message============================="+fs_message);
  if(fs_message.indexOf("\"error_code\":\"999\"")>0){
    System.out.println(map.get("data"));
  }
  System.out.println("\n-------------------------------------------------------------------------------");
	System.out.println("                                End apiEmployees                               ");
  System.out.println("-------------------------------------------------------------------------------");
  if(fsGlobal.isSuccess()) {
    if(fs_message!= null && fs_message.length()>0) {
      out.print(fs_message);
      return;
    }
  }
	//#(30000) programmer code end;
} catch(Throwable ex) {
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	int fs_errorcode = fsGlobal.parseErrorcode();
	String fs_errormessage = fsGlobal.getFsMessage();
	//#what error found when it over
	//#(32500) programmer code begin;
	//#(32500) programmer code end;
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response, fs_errorcode, fs_errormessage);
		return;
	}
}
//#before handle forward page
//#(35000) programmer code begin;
//#(35000) programmer code end;
fsSFTE101Bean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsSFTE101Bean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsSFTE101Bean.toJSON());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsSFTE101Bean.toXML());
	return;
}
//#born to try handle forward page
//#(40000) programmer code begin;
//#(40000) programmer code end;
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request,response);
%>
