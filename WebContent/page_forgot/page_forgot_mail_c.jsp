<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.dev.mail.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.config('page_forgot',pageContext.request, pageContext.response,true,false)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsResetPasswordBean" scope="request" class="com.fs.dev.auth.ResetPasswordBean"/>
<jsp:setProperty name="fsResetPasswordBean" property="*"/>
<%
fsGlobal.setFsDatatype("text");
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("page_forgot");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
String fs_forwarder = "/page_forgot/page_forgot_de.jsp";
try { 
	int fs_action = fsGlobal.parseAction();
	fsResetPasswordBean.forceObtain(fsGlobal);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsResetPasswordBean.obtain(fs_map);
	String fsSecureImage = (String)session.getAttribute("fsSecureImage");
	if(!fsResetPasswordBean.getSecurecode().equals(fsSecureImage)) throw new BeanException("Secure code is invalid",-8893);
	if(fsResetPasswordBean.getEmail().trim().length()<=0) throw new BeanException("Email address is invalid",-8896);
	fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
	fsResetPasswordBean.obtain(session,request);
	fsResetPasswordBean.transport(fsGlobal);
	System.out.println("effected transaction : "+fsResetPasswordBean.effectedTransactions());
	if(fsResetPasswordBean.effectedTransactions()>0) {
		fs_forwarder = "/page_forgot/page_forgot_success.jsp";
		java.util.Map map = null;
		java.sql.Connection connection = null;
		try {
			connection = DBConnection.getConnection(fsGlobal.getFsSection());
			map = (java.util.Map)XMLConfig.create(request).getConfigure("FORGOTPASSWORDMAIL",connection);
		} finally { 
			if(connection!=null) { try { connection.close(); }catch(Exception ex) {} }
		}		
		Trace.info("FORGOTPASSWORDMAIL = "+map);
		if(map!=null) {
			String fs_email = fsResetPasswordBean.getEmail();
			if(fs_email!=null && fs_email.trim().length()>0) {
				StringBuilder msg = new StringBuilder();
				msg.append("Dear, "+fsResetPasswordBean.getUsername()+"<br>");
				msg.append("Confirm your password was changed<br>");
				msg.append("user = "+fsResetPasswordBean.getUserid()+"<br>");
				msg.append("new password = "+fsResetPasswordBean.getPassword()+"<br>");
				msg.append("       yours sincerely,<br>");
				Trace.info("forgot password : "+fsResetPasswordBean.getUserid()+" ("+fs_email+")");
				SendMail mailer = new SendMail();
				mailer.obtain(map);
				mailer.setTo(fs_email);
				mailer.setMessage(msg.toString());
				mailer.start();
			}
		}
	}
}catch(Exception ex) { 
	Trace.error(fsGlobal,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
	throw ex;
}
fsResetPasswordBean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsResetPasswordBean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsResetPasswordBean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsResetPasswordBean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsResetPasswordBean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
