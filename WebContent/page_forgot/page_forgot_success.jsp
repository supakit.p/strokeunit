<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<c:if test="${fsScreen.config('page_forgot',pageContext.request, pageContext.response,true,false)}"></c:if>
<%
	String scheme = request.getScheme();             
	String serverName = request.getServerName(); 
	int serverPort = request.getServerPort(); 
	String aurl = scheme+"://"+serverName+(serverPort==80?"":":"+serverPort)+request.getContextPath()+"/index.jsp";
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Forgot Password</title>
		<jsp:include page="../jsp/meta.jsp"/>
		<link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<c:out value="${fsScreen.createStyles()}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/styles.jsp"/>
		<c:out value="${fsScreen.createImportScripts('page_forgot',pageContext.request,pageContext.response)}" escapeXml="false"></c:out>
		<jsp:include page="../jsp/scripts.jsp"/>
		<link rel="stylesheet" type="text/css" href="../css/program_style.css" />
		<link rel="stylesheet" type="text/css" href="page_forgot.css?${fsScreen.currentTime()}" />
		<script type="text/javascript">
		function loginLinkClick() {
			try{
				window.parent.logInClick();
				return;
			}catch(ex) {
				window.open('<%=aurl%>','_self');
			}
		}			
		</script>
	</head>
	<body class="portalbody portalbody-off">
		<div class="row portal-area sub-entry-layer" style="padding-top: 10px; padding-left: 5px; padding-bottom:15px;">
		<table style="width:100%;">
			<tbody>
				<tr class="rclass">
					<td height="50">&nbsp;</td>
				</tr>
				<tr class="rclass">
					<td align="center" height="30"><fs:label tagclass="lclass" tagid="resetpwd_label" required="false">Reset password success</fs:label></td>
				</tr>
				<tr>
					<td align="center" height="30"><fs:label tagclass="lclass" tagid="resetmsg_label" required="false">Please verify your email for new password changed</fs:label></td>
				</tr>
				<tr>
					<td align="center" height="30"><a href="#0" onclick="loginLinkClick()" id="loginlink">Log In</a></td>
				</tr>
			</tbody>
		</table>
		</div>			
	</body>
</html>
