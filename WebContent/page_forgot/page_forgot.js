var mouseX = 0;
var mouseY = 0;
var msgdialog;
var acceptdialog;
$(function(){
	$(this).mousedown(function(e) { mouseX = e.pageX; mouseY = e.pageY; });
	msgdialog = createDialog("#fsdialoglayer");	
	acceptdialog = createDialog("#fsacceptlayer");
	try { startApplication("page_forgot"); }catch(ex) { }
	initialApplication();
});
function initialApplication() {
	setupComponents();
	setupAlertComponents();
}
function setupComponents() {
	$("#sendbutton").click(function() { 
		send();
		return false;
	});
	$("#secureimage_ctrl").click(function() { 
		//$("#secureimage").attr("src","../jsp/secureimage.jsp?"+Math.random());
		jQuery.ajax({
			url: "../jsp/secureimagebase.jsp?"+Math.random(),
			type: "POST",
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown);  
			},
			success: function(data,status,transport){ 
				$("#secureimage").attr("src",$.trim(data));
			}
		});					
		return false;
	}).trigger("click");	
}
function clearingFields() {
	fsentryform.reset();
}
function validForm() {
	clearAlerts();
	var validator = null;
	if($.trim($("#email").val())=="") {
		$("#email").parent().addClass("has-error");
		$("#email_alert").show();
		if(!validator) validator = "email";
	}
	if($.trim($("#securecode").val())=="") {
		$("#securecode").parent().addClass("has-error");
		$("#securecode_alert").show();
		if(!validator) validator = "securecode";
	}
	if(validator) {
		$("#"+validator).focus();
		setTimeout(function() { 
			$("#"+validator).parent().addClass("has-error");
			$("#"+validator+"_alert").show();
		},100);
		return false;
	}
	return true;
}
function send(aform) {
	if(!aform) aform = fsentryform;
	//alert($(aform).serialize());
	if(!validForm()) return false;
	confirmSend(function() {		
		fsentryform.fsAjax.value = "false";
		fsentryform.fsDatatype.value = "text";
		fsentryform.submit();
		/*
		startWaiting();
		var xhr = jQuery.ajax({
			url: "page_forgot_de_c.jsp",
			type: "POST",
			data: $(aform).serialize(),
			dataType: "html",
			contentType: defaultContentType,
			error : function(transport,status,errorThrown) { 
				submitFailure(transport,status,errorThrown); 
			},
			success: function(data,status,transport){ 
				stopWaiting();
				sendsucces(function() { 
					clearingFields(); 
					try {  window.parent.doLogin(); } catch(ex) { }
				});					
			}
		});	
		*/
	});
	return false;
}
function sendsucces(callback,params) {
	alertbox("QS0201",callback,null,params);	
}
