<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.dev.mail.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsPager" scope="request" class="com.fs.bean.util.Pager"/>
<c:if test="${fsScreen.config('page_forgot',pageContext.request, pageContext.response,true,false)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<jsp:useBean id="fsResetPasswordBean" scope="request" class="com.fs.dev.auth.ResetPasswordBean"/>
<jsp:setProperty name="fsResetPasswordBean" property="*"/>
<jsp:useBean id="fsActivateBean" scope="request" class="com.fs.bean.ActivateBean"/>
<%
fsGlobal.setFsDatatype("text");
boolean fsIsAjax = fsGlobal.isAjax();
boolean fsIsJSON = fsGlobal.isJson();
boolean fsIsJSONData = fsGlobal.isJsondata();
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsXMLData = fsGlobal.isXmldata();
fsGlobal.setFsProg("page_forgot");
fsGlobal.setFsSection("AUTH");
fsGlobal.obtain(session);
String fs_forwarder = "/page_forgot/page_forgot.jsp";
try { 
	int fs_action = fsGlobal.parseAction();
	fsResetPasswordBean.forceObtain(fsGlobal);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsResetPasswordBean.obtain(fs_map);
	String fsSecureImage = (String)session.getAttribute("fsSecureImage");
	if(!fsResetPasswordBean.getSecurecode().equals(fsSecureImage)) throw new BeanException("Secure code is invalid",-8893);
	if(fsResetPasswordBean.getEmail().trim().length()<=0) throw new BeanException("Email address is invalid",-8896);
	BeanFormat format = new BeanFormat();		
	if(fsActivateBean.getActivateuser()==null || fsActivateBean.getActivateuser().trim().length()<=0) {
		fsActivateBean.setActivateuser(request.getRemoteAddr());
	}
	String scheme = request.getScheme();             
	String serverName = request.getServerName(); 
	int serverPort = request.getServerPort(); 
	String aurl = scheme+"://"+serverName+(serverPort==80?"":":"+serverPort)+request.getContextPath();
	Object activateurl = GlobalVariable.getVariable("ACTIVATE_URL");
	if(activateurl!=null && activateurl.toString().trim().length()>0) {
		aurl = activateurl.toString().trim();
	}
	String fs_activatekey = fsActivateBean.createActivateKey();
	//String fs_activatelink = aurl+"/activate/activate.jsp?activatekey="+fs_activatekey;
	String fs_activatelink = aurl+"/rest/control/activate/"+fs_activatekey;
	fsActivateBean.setActivatetimes("1");
	fsActivateBean.setActivateremark(fsResetPasswordBean.getEmail());
	fsActivateBean.setActivatepage(aurl+"/index.jsp");
	fsActivateBean.setActivatelink(fs_activatelink);
	fsActivateBean.setSenddate(format.formatDate(new java.util.Date()));
	fsActivateBean.setSendtime(format.formatTime(new java.sql.Time(System.currentTimeMillis())));
	int rows = fsActivateBean.insert(fsGlobal); 
	if(rows>0) {
		fs_forwarder = "/page_forgot/page_forgot_ok.jsp";
		java.util.Map map = null;
		java.sql.Connection connection = null;
		try {
			connection = DBConnection.getConnection(fsGlobal.getFsSection());
			map = (java.util.Map)XMLConfig.create(request).getConfigure("FORGOTPASSWORDMAIL",connection);
		} finally { 
			if(connection!=null) { try { connection.close(); }catch(Exception ex) {} }
		}		
		Trace.info("FORGOTPASSWORDMAIL = "+map);
		if(map!=null) {
			String fs_email = fsResetPasswordBean.getEmail();
			if(fs_email!=null && fs_email.trim().length()>0) {
				StringBuilder msg = new StringBuilder();
				msg.append("Dear, Requester.<br/>");
				msg.append("To confirm your password to be changed<br/>");
				msg.append("Please click link below or goto the location to activate<br/>");
				msg.append("<a href=\""+fs_activatelink+"\" target=\"_blank\">Confirm Reset Password</a><br/>");
				msg.append("<br/>Go to the location "+fs_activatelink+"<br/>");
				Trace.info("forgot password : "+fs_email+"("+fs_activatekey+")");
				SendMail mailer = new SendMail();
				mailer.obtain(map);
				mailer.setTo(fs_email);
				mailer.setMessage(msg.toString());
				mailer.start();
			}
		}		
	}
}catch(Exception ex) { 
	Trace.error(fsGlobal,ex);
	fsGlobal.setThrowable(ex);
	if(fsIsAjax) {
		fsGlobal.createResponseStatus(out, response);
		return;
	}
	throw ex;
}
fsResetPasswordBean.obtain(session,request);
if(fsIsJSONData) {
	out.print(fsResetPasswordBean.toJSONData("rows"));
	return;
}
if(fsIsJSON) {
	out.print(fsResetPasswordBean.toJSON());
	return;
}
if(fsIsXMLData) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsResetPasswordBean.toXMLDatas());
	return;
}
if(fsIsXML) {
	out.print("<?xml version=\"1.0\" encoding=\""+GlobalVariable.getEncoding()+"\"?>");
	out.print(fsResetPasswordBean.toXML());
	return;
}
RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
rd.forward(request, response);
%>
