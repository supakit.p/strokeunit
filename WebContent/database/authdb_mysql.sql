-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.10-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for authdb
CREATE DATABASE IF NOT EXISTS `authdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `authdb`;

-- Dumping structure for table authdb.jinout
CREATE TABLE IF NOT EXISTS `jinout` (
  `userid` varchar(60) DEFAULT NULL,
  `site` varchar(50) DEFAULT NULL,
  `logseqno` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `logintime` datetime DEFAULT NULL,
  `logouttime` datetime DEFAULT NULL,
  `address` varchar(30) DEFAULT NULL,
  `session` varchar(50) DEFAULT NULL,
  `kicker` varchar(20) DEFAULT NULL,
  `kicktime` datetime DEFAULT NULL,
  `expiretime` datetime DEFAULT NULL,
  `browseragent` varchar(250) DEFAULT NULL,
  `browsername` varchar(50) DEFAULT NULL,
  `browserversion` varchar(50) DEFAULT NULL,
  `osname` varchar(50) DEFAULT NULL,
  `typename` varchar(50) DEFAULT NULL,
  `devicename` varchar(50) DEFAULT NULL,
  `familyname` varchar(50) DEFAULT NULL,
  `producername` varchar(50) DEFAULT NULL,
  `network` varchar(50) DEFAULT NULL,
  `privateip` varchar(50) DEFAULT NULL,
  UNIQUE KEY `logseqno` (`logseqno`),
  KEY `session` (`session`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COMMENT='table keep journal login/logout';

-- Dumping data for table authdb.jinout: ~0 rows (approximately)
/*!40000 ALTER TABLE `jinout` DISABLE KEYS */;
/*!40000 ALTER TABLE `jinout` ENABLE KEYS */;

-- Dumping structure for table authdb.ladm
CREATE TABLE IF NOT EXISTS `ladm` (
  `keyid` varchar(55) DEFAULT NULL,
  `curtime` bigint(20) DEFAULT NULL,
  `trxtime` bigint(20) unsigned DEFAULT NULL,
  `edittime` datetime DEFAULT NULL,
  `owner` varchar(60) DEFAULT NULL,
  `process` varchar(50) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `seqno` int(11) unsigned DEFAULT NULL,
  `contents` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep log admin';

-- Dumping data for table authdb.ladm: ~0 rows (approximately)
/*!40000 ALTER TABLE `ladm` DISABLE KEYS */;
/*!40000 ALTER TABLE `ladm` ENABLE KEYS */;

-- Dumping structure for table authdb.lms
CREATE TABLE IF NOT EXISTS `lms` (
  `keyid` varchar(50) DEFAULT NULL,
  `curtime` bigint(20) unsigned DEFAULT NULL,
  `trxtime` bigint(20) unsigned DEFAULT NULL,
  `edittime` datetime DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `process` varchar(50) DEFAULT NULL,
  `contents` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep log sql contents';

-- Dumping data for table authdb.lms: ~0 rows (approximately)
/*!40000 ALTER TABLE `lms` DISABLE KEYS */;
/*!40000 ALTER TABLE `lms` ENABLE KEYS */;

-- Dumping structure for table authdb.tactivate
CREATE TABLE IF NOT EXISTS `tactivate` (
  `activatekey` varchar(100) NOT NULL,
  `activateuser` varchar(100) NOT NULL,
  `transtime` bigint(20) DEFAULT NULL,
  `senddate` date DEFAULT NULL,
  `sendtime` time DEFAULT NULL,
  `expiredate` date DEFAULT NULL,
  `activatedate` date DEFAULT NULL,
  `activatetime` time DEFAULT NULL,
  `activatecount` int(11) DEFAULT NULL,
  `activatetimes` int(11) DEFAULT NULL,
  `activatestatus` char(1) DEFAULT NULL,
  `activatecategory` varchar(50) DEFAULT NULL,
  `activatelink` varchar(200) DEFAULT NULL,
  `activatepage` varchar(200) DEFAULT NULL,
  `activateremark` varchar(200) DEFAULT NULL,
  `activateparameter` varchar(200) DEFAULT NULL,
  `activatemessage` varchar(200) DEFAULT NULL,
  `activatecontents` mediumtext,
  PRIMARY KEY (`activatekey`,`activateuser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep activate info';

-- Dumping data for table authdb.tactivate: ~0 rows (approximately)
/*!40000 ALTER TABLE `tactivate` DISABLE KEYS */;
/*!40000 ALTER TABLE `tactivate` ENABLE KEYS */;

-- Dumping structure for table authdb.tactivatehistory
CREATE TABLE IF NOT EXISTS `tactivatehistory` (
  `activatekey` varchar(100) NOT NULL,
  `activateuser` varchar(100) NOT NULL,
  `transtime` bigint(20) DEFAULT NULL,
  `senddate` date DEFAULT NULL,
  `sendtime` time DEFAULT NULL,
  `expiredate` date DEFAULT NULL,
  `activatedate` date DEFAULT NULL,
  `activatetime` time DEFAULT NULL,
  `activatecount` int(11) DEFAULT NULL,
  `activatetimes` int(11) DEFAULT NULL,
  `activatestatus` char(1) DEFAULT NULL,
  `activatecategory` varchar(50) DEFAULT NULL,
  `activatelink` varchar(200) DEFAULT NULL,
  `activatepage` varchar(200) DEFAULT NULL,
  `activateremark` varchar(200) DEFAULT NULL,
  `activateparameter` varchar(200) DEFAULT NULL,
  `activatemessage` varchar(200) DEFAULT NULL,
  `activatecontents` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep activate history';

-- Dumping data for table authdb.tactivatehistory: ~0 rows (approximately)
/*!40000 ALTER TABLE `tactivatehistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `tactivatehistory` ENABLE KEYS */;

-- Dumping structure for table authdb.tcardtype
CREATE TABLE IF NOT EXISTS `tcardtype` (
  `cardtype` varchar(2) NOT NULL,
  `nameen` varchar(100) NOT NULL,
  `nameth` varchar(100) NOT NULL,
  PRIMARY KEY (`cardtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep card type';

-- Dumping data for table authdb.tcardtype: ~4 rows (approximately)
/*!40000 ALTER TABLE `tcardtype` DISABLE KEYS */;
INSERT INTO `tcardtype` (`cardtype`, `nameen`, `nameth`) VALUES
	('C', 'Care Card', 'บัตรประกันสุขภาพ'),
	('I', 'Identity Card', 'บัตรประจำตัวประชาชน'),
	('P', 'Passport No', 'หนังสือเดินทาง'),
	('S', 'Social Card', 'บัตรประกันสังคม');
/*!40000 ALTER TABLE `tcardtype` ENABLE KEYS */;

-- Dumping structure for table authdb.tcomp
CREATE TABLE IF NOT EXISTS `tcomp` (
  `site` varchar(50) NOT NULL,
  `headsite` varchar(50) DEFAULT NULL,
  `shortname` varchar(50) DEFAULT NULL,
  `nameen` varchar(100) DEFAULT NULL,
  `nameth` varchar(100) DEFAULT NULL,
  `addressen` varchar(200) DEFAULT NULL,
  `addressth` varchar(200) DEFAULT NULL,
  `district` varchar(20) DEFAULT NULL COMMENT 'tdistrict.districtcode',
  `districtname` varchar(50) DEFAULT NULL,
  `amphur` varchar(20) DEFAULT NULL COMMENT 'tamphur.amphurcode',
  `amphurname` varchar(50) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL COMMENT 'tprovince.provincecode',
  `provincename` varchar(50) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL COMMENT 'tcountry.countrycode',
  `telno1` varchar(20) DEFAULT NULL,
  `telno2` varchar(20) DEFAULT NULL,
  `faxno1` varchar(20) DEFAULT NULL,
  `faxno2` varchar(20) DEFAULT NULL,
  `email1` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `telext1` varchar(50) DEFAULT NULL,
  `telext2` varchar(50) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `taxid` varchar(20) DEFAULT NULL,
  `logoimage` varchar(100) DEFAULT NULL,
  `bgimage` varchar(100) DEFAULT NULL COMMENT 'keep file as site to under path [web-project]/images/background',
  `bgimagename` varchar(100) DEFAULT NULL COMMENT 'keep original file name from client',
  `probationdays` int(11) DEFAULT NULL,
  `settleday` int(11) DEFAULT NULL COMMENT 'day of settlement',
  `payday` int(11) DEFAULT NULL COMMENT 'day of salary paid',
  `percentrate` decimal(16,2) DEFAULT NULL COMMENT 'percent usage rate',
  `holidaycalendar` varchar(50) DEFAULT NULL COMMENT 'tcompholidaytable.holidayid',
  `holdflag` varchar(1) DEFAULT '0' COMMENT '1=Hold',
  `inactive` varchar(1) DEFAULT '0' COMMENT '1=Inactive',
  `effectdate` date DEFAULT NULL,
  `timezone` varchar(50) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep company profile';

-- Dumping data for table authdb.tcomp: ~0 rows (approximately)
/*!40000 ALTER TABLE `tcomp` DISABLE KEYS */;
INSERT INTO `tcomp` (`site`, `headsite`, `shortname`, `nameen`, `nameth`, `addressen`, `addressth`, `district`, `districtname`, `amphur`, `amphurname`, `province`, `provincename`, `zipcode`, `country`, `telno1`, `telno2`, `faxno1`, `faxno2`, `email1`, `email2`, `telext1`, `telext2`, `website`, `taxid`, `logoimage`, `bgimage`, `bgimagename`, `probationdays`, `settleday`, `payday`, `percentrate`, `holidaycalendar`, `holdflag`, `inactive`, `effectdate`, `timezone`, `editdate`, `edittime`, `edituser`) VALUES
	('FWG', NULL, 'FWG', 'Freewill Solutions Co.,Ltd.', 'ฟรีวิลโซลูชั่น จำกัด', '29 flr. 1168 Lumpini Tower', 'ชั้น 29 เลขที่ 1168 ลุมพินีเทาเวอร์', '102803', 'ทุ่งมหาเมฆ', '1028', 'เขตสาทร', '002', 'กรุงเทพมหานคร', '10120', 'TH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '125365200', NULL, NULL, NULL, 120, 100, 100, 70.00, NULL, '0', '0', '2019-03-20', NULL, NULL, NULL, 'fwgadmin');
/*!40000 ALTER TABLE `tcomp` ENABLE KEYS */;

-- Dumping structure for table authdb.tcompbranch
CREATE TABLE IF NOT EXISTS `tcompbranch` (
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `branch` varchar(50) NOT NULL,
  `nameen` varchar(100) DEFAULT NULL,
  `nameth` varchar(100) DEFAULT NULL,
  `addressen` varchar(200) DEFAULT NULL,
  `addressth` varchar(200) DEFAULT NULL,
  `district` varchar(20) DEFAULT NULL COMMENT 'tdistrict.districtcode',
  `districtname` varchar(50) DEFAULT NULL,
  `amphur` varchar(20) DEFAULT NULL COMMENT 'tamphur.amphurcode',
  `amphurname` varchar(50) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL COMMENT 'tprovince.provincecode',
  `provincename` varchar(50) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL COMMENT 'tcountry.countrycode',
  `telno1` varchar(20) DEFAULT NULL,
  `telno2` varchar(20) DEFAULT NULL,
  `faxno1` varchar(20) DEFAULT NULL,
  `faxno2` varchar(20) DEFAULT NULL,
  `email1` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `bgimage` varchar(100) DEFAULT NULL,
  `latitude` decimal(16,6) DEFAULT NULL,
  `longitude` decimal(16,6) DEFAULT NULL,
  `distances` decimal(16,6) DEFAULT NULL,
  `gpsflag` varchar(1) DEFAULT '0' COMMENT '1=Allow GPS',
  `inactive` varchar(1) DEFAULT '0' COMMENT '1=Inactive',
  `effectdate` date DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`site`,`branch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep company branch';

-- Dumping data for table authdb.tcompbranch: ~2 rows (approximately)
/*!40000 ALTER TABLE `tcompbranch` DISABLE KEYS */;
INSERT INTO `tcompbranch` (`site`, `branch`, `nameen`, `nameth`, `addressen`, `addressth`, `district`, `districtname`, `amphur`, `amphurname`, `province`, `provincename`, `zipcode`, `country`, `telno1`, `telno2`, `faxno1`, `faxno2`, `email1`, `email2`, `bgimage`, `latitude`, `longitude`, `distances`, `gpsflag`, `inactive`, `effectdate`, `editdate`, `edittime`, `edituser`) VALUES
	('FWG', '00', 'BKK', 'กรุงเทพ', 'BKK', 'BKK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL, NULL, NULL),
	('FWG', '01', 'KKN', 'ขอนแก่น', 'KKN', 'KKN', '400101', 'ในเมือง', '4001', 'เมืองขอนแก่น', '006', 'ขอนแก่น', '40000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13.827150, 100.569426, 1500.000000, '0', '0', '2018-10-12', '2018-10-12', '09:21:58', 'tso');
/*!40000 ALTER TABLE `tcompbranch` ENABLE KEYS */;

-- Dumping structure for table authdb.tcompconfig
CREATE TABLE IF NOT EXISTS `tcompconfig` (
  `site` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `autoflag` varchar(1) NOT NULL DEFAULT '0' COMMENT '1=Auto Transfer or Batch Transfer',
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `holidayflag` varchar(1) DEFAULT NULL COMMENT '1=Can Advance On Holiday Date',
  `saturdayflag` varchar(1) DEFAULT NULL COMMENT '1=Can Advance On Saturday',
  `sundayflag` varchar(1) DEFAULT NULL COMMENT '1=Can Advance On Sunday',
  `mobile` varchar(50) DEFAULT NULL COMMENT 'Notice mobile number separated by comma',
  `remarks` varchar(200) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep company configuration';

-- Dumping data for table authdb.tcompconfig: ~0 rows (approximately)
/*!40000 ALTER TABLE `tcompconfig` DISABLE KEYS */;
INSERT INTO `tcompconfig` (`site`, `category`, `autoflag`, `starttime`, `endtime`, `holidayflag`, `saturdayflag`, `sundayflag`, `mobile`, `remarks`, `editdate`, `edittime`, `edituser`) VALUES
	('FWG', 'ADVANCE', '0', '08:00:00', '12:00:00', '1', '1', '1', NULL, 'bank working time', NULL, NULL, NULL);
/*!40000 ALTER TABLE `tcompconfig` ENABLE KEYS */;

-- Dumping structure for table authdb.tcompgrp
CREATE TABLE IF NOT EXISTS `tcompgrp` (
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `headsite` varchar(50) NOT NULL COMMENT 'tcomp.site',
  PRIMARY KEY (`site`,`headsite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep company group';

-- Dumping data for table authdb.tcompgrp: ~0 rows (approximately)
/*!40000 ALTER TABLE `tcompgrp` DISABLE KEYS */;
/*!40000 ALTER TABLE `tcompgrp` ENABLE KEYS */;

-- Dumping structure for table authdb.tcompparty
CREATE TABLE IF NOT EXISTS `tcompparty` (
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `partysite` varchar(50) NOT NULL COMMENT 'tcomp.site',
  PRIMARY KEY (`site`,`partysite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep company partner ship';

-- Dumping data for table authdb.tcompparty: ~0 rows (approximately)
/*!40000 ALTER TABLE `tcompparty` DISABLE KEYS */;
/*!40000 ALTER TABLE `tcompparty` ENABLE KEYS */;

-- Dumping structure for table authdb.tcompprod
CREATE TABLE IF NOT EXISTS `tcompprod` (
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `product` varchar(50) NOT NULL COMMENT 'tprod.product',
  PRIMARY KEY (`site`,`product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep product of company';

-- Dumping data for table authdb.tcompprod: ~0 rows (approximately)
/*!40000 ALTER TABLE `tcompprod` DISABLE KEYS */;
INSERT INTO `tcompprod` (`site`, `product`) VALUES
	('FWG', 'ADVANCE');
/*!40000 ALTER TABLE `tcompprod` ENABLE KEYS */;

-- Dumping structure for table authdb.tconfig
CREATE TABLE IF NOT EXISTS `tconfig` (
  `category` varchar(50) NOT NULL,
  `colname` varchar(50) NOT NULL,
  `colvalue` varchar(200) DEFAULT NULL,
  `seqno` int(11) DEFAULT '0',
  PRIMARY KEY (`category`,`colname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='keep program custom configuration';

-- Dumping data for table authdb.tconfig: ~0 rows (approximately)
/*!40000 ALTER TABLE `tconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `tconfig` ENABLE KEYS */;

-- Dumping structure for table authdb.tcontactperson
CREATE TABLE IF NOT EXISTS `tcontactperson` (
  `personid` bigint(20) NOT NULL AUTO_INCREMENT,
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `persontype` varchar(1) NOT NULL DEFAULT 'C' COMMENT 'C=Company',
  `nameen` varchar(100) DEFAULT NULL,
  `nameth` varchar(100) DEFAULT NULL,
  `positionname` varchar(100) DEFAULT NULL,
  `telno` varchar(20) DEFAULT NULL,
  `telext` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `inactive` varchar(1) DEFAULT '0',
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`personid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep contact person';

-- Dumping data for table authdb.tcontactperson: ~0 rows (approximately)
/*!40000 ALTER TABLE `tcontactperson` DISABLE KEYS */;
/*!40000 ALTER TABLE `tcontactperson` ENABLE KEYS */;

-- Dumping structure for table authdb.tfavor
CREATE TABLE IF NOT EXISTS `tfavor` (
  `userid` varchar(60) NOT NULL COMMENT 'tuser.userid',
  `programid` varchar(20) NOT NULL COMMENT 'tprog.programid',
  `seqno` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`,`programid`,`seqno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user favorite menu';

-- Dumping data for table authdb.tfavor: ~18 rows (approximately)
/*!40000 ALTER TABLE `tfavor` DISABLE KEYS */;
INSERT INTO `tfavor` (`userid`, `programid`, `seqno`) VALUES
	('fwgadmin', 'sfte001', 1),
	('fwgadmin', 'sfte002', 2),
	('fwgadmin', 'sfte003', 3),
	('fwgadmin', 'sfte005', 4),
	('fwgadmin', 'sftq001', 5),
	('fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'sfte005', 1),
	('fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'sfte007', 2),
	('fwgcenter', 'sfte001', 1),
	('fwgcenter', 'sfte002', 2),
	('fwgcenter', 'sfte003', 3),
	('fwgcenter', 'sfte005', 4),
	('fwgcenter', 'sftq001', 5),
	('tester_test', 'sfte001', 1),
	('tso', 'sfte001', 1),
	('tso', 'sfte002', 2),
	('tso', 'sfte003', 3),
	('tso', 'sfte005', 4),
	('tso', 'sftq001', 5);
/*!40000 ALTER TABLE `tfavor` ENABLE KEYS */;

-- Dumping structure for table authdb.tgroup
CREATE TABLE IF NOT EXISTS `tgroup` (
  `groupname` varchar(50) NOT NULL DEFAULT '',
  `supergroup` varchar(50) DEFAULT '',
  `nameen` varchar(100) DEFAULT NULL,
  `nameth` varchar(100) DEFAULT NULL,
  `seqno` int(11) DEFAULT '0',
  `iconstyle` varchar(50) DEFAULT NULL,
  `privateflag` varchar(1) DEFAULT '0' COMMENT '1=Private Group(Center Usage)',
  `usertype` varchar(1) DEFAULT NULL COMMENT 'tusertype.usertype',
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`groupname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table group info';

-- Dumping data for table authdb.tgroup: ~12 rows (approximately)
/*!40000 ALTER TABLE `tgroup` DISABLE KEYS */;
INSERT INTO `tgroup` (`groupname`, `supergroup`, `nameen`, `nameth`, `seqno`, `iconstyle`, `privateflag`, `usertype`, `editdate`, `edittime`, `edituser`) VALUES
	('ADMIN', 'MD', 'Administrator', 'ผู้ดูแลระบบ', 1, NULL, '0', 'A', NULL, NULL, 'fwgcenter'),
	('ADMINENGAGE', 'ADMIN', 'Admin Feed News', 'การจัดการข่าวสาร', 2, NULL, '0', 'A', NULL, NULL, 'fwgcenter'),
	('ADVANCE', 'ADMIN', 'Advance Salary', 'ผู้ดูแลระบบเบิกเงิน', 14, NULL, '0', 'A', NULL, NULL, NULL),
	('CENTER', 'MD', 'Center Administrator', 'ผู้บริหารระบบส่วนกลาง', 5, NULL, '1', 'A', NULL, NULL, 'fwgcenter'),
	('COACH', 'MD', 'Super Coach', 'เจ้าหน้าที่ระดับสูง', 6, NULL, '0', 'C', NULL, NULL, NULL),
	('DIRECTOR', NULL, 'Director', 'ผู้อำนวยการ', 7, NULL, '0', 'D', '2018-10-19', '11:21:50', 'tso'),
	('EMPLOYEE', NULL, 'Employee', 'พนักงาน', 8, NULL, '0', 'E', '2018-10-19', '11:22:14', 'tso'),
	('EXECUTIVE', NULL, 'Executive', 'ผู้บริหาร', 9, NULL, '0', 'X', '2018-10-19', '11:22:53', 'tso'),
	('MANAGER', NULL, 'Manager', 'ผู้จัดการ', 10, NULL, '0', 'M', '2018-10-19', '11:22:32', 'fwgcenter'),
	('OPERATOR', 'ADMIN', 'Operator', 'เจ้าหน้าที่ปฏิบัติการ', 11, NULL, '0', 'O', NULL, NULL, NULL),
	('SUPERVISOR', NULL, 'Supervisor', 'ผู้ควบคุม', 12, NULL, '0', 'S', '2018-10-19', '11:21:26', 'tso'),
	('TESTER', 'ADMIN', 'Tester', 'ผู้ทดสอบ', 13, NULL, '0', 'O', NULL, NULL, NULL);
/*!40000 ALTER TABLE `tgroup` ENABLE KEYS */;

-- Dumping structure for table authdb.tgroupcomp
CREATE TABLE IF NOT EXISTS `tgroupcomp` (
  `groupname` varchar(50) NOT NULL COMMENT 'tgroup.groupname',
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  PRIMARY KEY (`groupname`,`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep company in group permission';

-- Dumping data for table authdb.tgroupcomp: ~0 rows (approximately)
/*!40000 ALTER TABLE `tgroupcomp` DISABLE KEYS */;
/*!40000 ALTER TABLE `tgroupcomp` ENABLE KEYS */;

-- Dumping structure for table authdb.tlevel
CREATE TABLE IF NOT EXISTS `tlevel` (
  `levelid` varchar(50) NOT NULL,
  `level` int(11) DEFAULT '0',
  `nameen` varchar(100) NOT NULL,
  `nameth` varchar(100) NOT NULL,
  `inactive` varchar(1) DEFAULT '0' COMMENT '1=Inactive',
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`levelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep position level';

-- Dumping data for table authdb.tlevel: ~12 rows (approximately)
/*!40000 ALTER TABLE `tlevel` DISABLE KEYS */;
INSERT INTO `tlevel` (`levelid`, `level`, `nameen`, `nameth`, `inactive`, `editdate`, `edittime`, `edituser`) VALUES
	('LV01', 1, 'Level 01', 'ระดับ 01', '0', NULL, NULL, NULL),
	('LV02', 2, 'Level 02', 'ระดับ 02', '0', NULL, NULL, NULL),
	('LV03', 3, 'Level 03', 'ระดับ 03', '0', NULL, NULL, NULL),
	('LV04', 4, 'Level 04', 'ระดับ 04', '0', NULL, NULL, NULL),
	('LV05', 5, 'Level 05', 'ระดับ 05', '0', NULL, NULL, NULL),
	('LV06', 6, 'Level 06', 'ระดับ 06', '0', NULL, NULL, NULL),
	('LV07', 7, 'Level 07', 'ระดับ 07', '0', NULL, NULL, NULL),
	('LV08', 8, 'Level 08', 'ระดับ 08', '0', NULL, NULL, NULL),
	('LV09', 9, 'Level 09', 'ระดับ 09', '0', NULL, NULL, NULL),
	('LV10', 10, 'Level 10', 'ระดับ 10', '0', NULL, NULL, NULL),
	('LV11', 11, 'Level 11', 'ระดับ 11', '0', NULL, NULL, NULL),
	('LV99', 99, 'Not specified', 'ไม่ระบุ', '0', NULL, NULL, NULL);
/*!40000 ALTER TABLE `tlevel` ENABLE KEYS */;

-- Dumping structure for table authdb.tppwd
CREATE TABLE IF NOT EXISTS `tppwd` (
  `userid` varchar(10) NOT NULL DEFAULT '',
  `checkreservepwd` char(1) NOT NULL DEFAULT '0',
  `timenotusedoldpwd` smallint(6) NOT NULL DEFAULT '0',
  `alertbeforeexpire` smallint(6) NOT NULL DEFAULT '0',
  `pwdexpireday` smallint(6) NOT NULL DEFAULT '0',
  `notloginafterday` smallint(6) NOT NULL DEFAULT '0',
  `notchgpwduntilday` smallint(6) NOT NULL DEFAULT '0',
  `minpwdlength` smallint(6) NOT NULL DEFAULT '0',
  `alphainpwd` smallint(6) NOT NULL DEFAULT '0',
  `otherinpwd` smallint(6) NOT NULL DEFAULT '0',
  `maxsamechar` smallint(6) NOT NULL DEFAULT '0',
  `mindiffchar` smallint(6) NOT NULL DEFAULT '0',
  `maxarrangechar` smallint(6) NOT NULL DEFAULT '0',
  `loginfailtime` int(11) unsigned DEFAULT NULL,
  `fromip` varchar(15) DEFAULT NULL,
  `toip` varchar(15) DEFAULT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `groupflag` char(50) DEFAULT NULL,
  `maxloginfailtime` smallint(6) DEFAULT NULL,
  `checkdictpwd` smallint(6) DEFAULT NULL,
  `maxpwdlength` smallint(6) DEFAULT NULL,
  `digitinpwd` smallint(6) DEFAULT NULL,
  UNIQUE KEY `idx_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=tis620;

-- Dumping data for table authdb.tppwd: ~0 rows (approximately)
/*!40000 ALTER TABLE `tppwd` DISABLE KEYS */;
/*!40000 ALTER TABLE `tppwd` ENABLE KEYS */;

-- Dumping structure for table authdb.tprod
CREATE TABLE IF NOT EXISTS `tprod` (
  `product` varchar(50) NOT NULL DEFAULT '',
  `nameen` varchar(100) NOT NULL,
  `nameth` varchar(100) NOT NULL,
  `seqno` int(11) DEFAULT '0',
  `serialid` varchar(100) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `capital` varchar(1) DEFAULT NULL,
  `verified` varchar(1) DEFAULT '1' COMMENT '1=Verify Product Access',
  `centerflag` varchar(1) DEFAULT '0',
  `iconfile` varchar(100) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep product or module';

-- Dumping data for table authdb.tprod: ~4 rows (approximately)
/*!40000 ALTER TABLE `tprod` DISABLE KEYS */;
INSERT INTO `tprod` (`product`, `nameen`, `nameth`, `seqno`, `serialid`, `startdate`, `url`, `capital`, `verified`, `centerflag`, `iconfile`, `editdate`, `edittime`, `edituser`) VALUES
	('ENGAGE', 'Engagement', 'Engagement', 20, NULL, NULL, NULL, NULL, '0', '0', 'engage.png', NULL, NULL, NULL),
	('MINOR', 'Minor Asset', 'Minor Asset', 60, NULL, NULL, NULL, NULL, '0', '0', 'minor.png', NULL, NULL, NULL),
	('NOTIFY', 'Notify', 'Notify', 30, NULL, NULL, NULL, NULL, '0', '0', 'notify.png', NULL, NULL, NULL),
	('PROMPT', 'Prompt Module', 'Prompt Module', 99, NULL, NULL, NULL, NULL, '0', '1', 'prompt.png', NULL, NULL, NULL);
/*!40000 ALTER TABLE `tprod` ENABLE KEYS */;

-- Dumping structure for table authdb.tprog
CREATE TABLE IF NOT EXISTS `tprog` (
  `product` varchar(30) NOT NULL DEFAULT '' COMMENT 'tprod.product',
  `programid` varchar(20) NOT NULL,
  `progname` varchar(100) DEFAULT NULL,
  `prognameth` varchar(100) DEFAULT NULL,
  `progtype` varchar(2) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `parameters` varchar(80) DEFAULT NULL,
  `progsystem` varchar(10) DEFAULT NULL,
  `iconfile` varchar(50) DEFAULT NULL,
  `iconstyle` varchar(50) DEFAULT NULL,
  `shortname` varchar(50) DEFAULT NULL,
  `shortnameth` varchar(50) DEFAULT NULL,
  `progpath` varchar(150) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`programid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep program name';

-- Dumping data for table authdb.tprog: ~9 rows (approximately)
/*!40000 ALTER TABLE `tprog` DISABLE KEYS */;
INSERT INTO `tprog` (`product`, `programid`, `progname`, `prognameth`, `progtype`, `description`, `parameters`, `progsystem`, `iconfile`, `iconstyle`, `shortname`, `shortnameth`, `progpath`, `editdate`, `edittime`, `edituser`) VALUES
	('ENGAGE', 'pmte015', 'Feed Setting', 'ตั้งค่าข่าว', 'F', 'Feed Setting', NULL, 'F', 'pmte015.png', NULL, 'Feed', 'Feed', NULL, NULL, NULL, NULL),
	('ENGAGE', 'pmte032', 'Notify Message', 'ข้อความแจ้งประกาศ', 'F', 'Notify Message', NULL, 'F', 'pmte032.png', NULL, 'Notify Message', 'Notify Message', NULL, NULL, NULL, NULL),
	('PROMPT', 'sfte001', 'Program Information', 'ข้อมูลโปรแกรม', 'F', 'Program Information', NULL, 'F', 'sfte001.png', NULL, 'Program', 'โปรแกรม', NULL, NULL, NULL, NULL),
	('PROMPT', 'sfte002', 'Group Information', 'กลุ่มผู้ใช้งาน', 'F', 'Group Information', NULL, 'F', 'sfte002.png', NULL, 'Group', 'กลู่มผู้ใช้', NULL, NULL, NULL, NULL),
	('PROMPT', 'sfte003', 'Product Information', 'ข้อมูลผลิตภัณฑ์', 'F', 'Product Information', NULL, 'F', 'sfte003.png', NULL, 'Product', 'ผลิตภัณฑ์', NULL, NULL, NULL, NULL),
	('PROMPT', 'sfte005', 'User', 'ข้อมูลผู้ใช้', 'F', 'User', NULL, 'F', 'sfte005.png', NULL, 'User', 'ผู้ใช้งาน', NULL, NULL, NULL, NULL),
	('PROMPT', 'sfte006', 'Company Product Setting', 'ตั้งค่าผลิตภัณฑ์ของบริษัท', 'F', 'Company Product Setting', NULL, 'F', 'sfte006.png', NULL, 'Company Product', 'ผลิตภัณฑ์บริษัท', NULL, NULL, NULL, NULL),
	('PROMPT', 'sfte007', 'User Privilege', 'สิทธิผู้ใช้', 'F', 'User Privilege', NULL, 'F', 'sfte007.png', NULL, 'User Privilege', 'สิทธิผู้ใช้', NULL, NULL, NULL, NULL),
	('PROMPT', 'sftq001', 'Tracking', 'การตรวจสอบ', 'F', 'Tracking', NULL, 'F', 'sftq001.png', NULL, 'Tracking', 'ตรวจสอบ', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tprog` ENABLE KEYS */;

-- Dumping structure for table authdb.tprogconfig
CREATE TABLE IF NOT EXISTS `tprogconfig` (
  `progid` varchar(20) NOT NULL COMMENT 'tprog.programid',
  `progname` varchar(100) NOT NULL,
  `tablename` varchar(20) DEFAULT NULL,
  `xmlfile` varchar(50) DEFAULT NULL,
  `xmltext` longtext,
  PRIMARY KEY (`progid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='program configuration table';

-- Dumping data for table authdb.tprogconfig: ~0 rows (approximately)
/*!40000 ALTER TABLE `tprogconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `tprogconfig` ENABLE KEYS */;

-- Dumping structure for table authdb.tproggrp
CREATE TABLE IF NOT EXISTS `tproggrp` (
  `groupname` varchar(50) NOT NULL COMMENT 'tgroup.groupname',
  `programid` varchar(20) NOT NULL COMMENT 'tprog.programid',
  `seqno` int(11) DEFAULT '0',
  PRIMARY KEY (`groupname`,`programid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep program by group';

-- Dumping data for table authdb.tproggrp: ~20 rows (approximately)
/*!40000 ALTER TABLE `tproggrp` DISABLE KEYS */;
INSERT INTO `tproggrp` (`groupname`, `programid`, `seqno`) VALUES
	('ADMIN', 'sfte001', 1),
	('ADMIN', 'sfte002', 2),
	('ADMIN', 'sfte003', 3),
	('ADMIN', 'sfte005', 4),
	('ADMIN', 'sfte006', 5),
	('ADMIN', 'sfte007', 6),
	('ADMIN', 'sftq001', 7),
	('ADMINENGAGE', 'pmte015', 1),
	('ADMINENGAGE', 'pmte032', 2),
	('CENTER', 'sfte001', 1),
	('CENTER', 'sfte002', 2),
	('CENTER', 'sfte003', 3),
	('CENTER', 'sfte005', 4),
	('CENTER', 'sfte006', 6),
	('CENTER', 'sfte007', 7),
	('CENTER', 'sftq001', 5),
	('TESTER', 'sfte001', 1),
	('TESTER', 'sfte002', 2),
	('TESTER', 'sfte003', 3),
	('TESTER', 'sfte005', 4);
/*!40000 ALTER TABLE `tproggrp` ENABLE KEYS */;

-- Dumping structure for table authdb.trole
CREATE TABLE IF NOT EXISTS `trole` (
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `roleid` varchar(50) NOT NULL,
  `nameen` varchar(100) NOT NULL,
  `nameth` varchar(100) NOT NULL,
  `headroleid` varchar(50) DEFAULT NULL,
  `inactive` varchar(1) DEFAULT '0' COMMENT '1=Inactive',
  `actionflag` varchar(1) DEFAULT NULL COMMENT '1=Action Role (User Role)',
  `approveflag` varchar(1) DEFAULT NULL COMMENT '1=Approve Role',
  `effectdate` date DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`site`,`roleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep role name';

-- Dumping data for table authdb.trole: ~8 rows (approximately)
/*!40000 ALTER TABLE `trole` DISABLE KEYS */;
INSERT INTO `trole` (`site`, `roleid`, `nameen`, `nameth`, `headroleid`, `inactive`, `actionflag`, `approveflag`, `effectdate`, `editdate`, `edittime`, `edituser`) VALUES
	('SRI', 'R01', 'Programmer', 'โปรแกรมเมอร์', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL),
	('SRI', 'R02', 'System Engineer', 'วิศวกรระบบ', NULL, '0', NULL, '1', NULL, NULL, NULL, NULL),
	('SRI', 'R03', 'System Analysis', 'นักวิเคราะห์ระบบ', NULL, '0', NULL, '1', NULL, NULL, NULL, NULL),
	('SRI', 'R04', 'System Software', 'นักพัฒนาซอฟแวร์', NULL, '0', NULL, '1', NULL, NULL, NULL, NULL),
	('SRI', 'R05', 'Designer', 'นักออกแบบ', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL),
	('SRI', 'R06', 'Architecture', 'สถาปัตยกรรมระบบ', NULL, '0', NULL, '1', NULL, NULL, NULL, NULL),
	('SRI', 'R07', 'Tester', 'ผู้ทดสอบ', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL),
	('SRI', 'R08', 'Consultant', 'ที่ปรึกษา', NULL, '0', NULL, '1', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `trole` ENABLE KEYS */;

-- Dumping structure for table authdb.trpwd
CREATE TABLE IF NOT EXISTS `trpwd` (
  `reservepwd` char(8) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_reservepwd` (`reservepwd`)
) ENGINE=InnoDB DEFAULT CHARSET=tis620;

-- Dumping data for table authdb.trpwd: ~0 rows (approximately)
/*!40000 ALTER TABLE `trpwd` DISABLE KEYS */;
/*!40000 ALTER TABLE `trpwd` ENABLE KEYS */;

-- Dumping structure for table authdb.tstyle
CREATE TABLE IF NOT EXISTS `tstyle` (
  `styleid` varchar(50) NOT NULL,
  `styletext` varchar(50) NOT NULL,
  PRIMARY KEY (`styleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep icon style from awesome font';

-- Dumping data for table authdb.tstyle: ~100 rows (approximately)
/*!40000 ALTER TABLE `tstyle` DISABLE KEYS */;
INSERT INTO `tstyle` (`styleid`, `styletext`) VALUES
	('fa fa-align-justify', 'fa fa-align-justify'),
	('fa fa-archive', 'fa fa-archive'),
	('fa fa-balance-scale', 'fa fa-balance-scale'),
	('fa fa-bank', 'fa fa-bank'),
	('fa fa-bar-chart', 'fa fa-bar-chart'),
	('fa fa-barcode', 'fa fa-barcode'),
	('fa fa-bell-o', 'fa fa-bell-o'),
	('fa fa-bitcoin', 'fa fa-bitcoin'),
	('fa fa-bold', 'fa fa-bold'),
	('fa fa-bolt', 'fa fa-bolt'),
	('fa fa-book', 'fa fa-book'),
	('fa fa-bookmark-o', 'fa fa-bookmark-o'),
	('fa fa-briefcase', 'fa fa-briefcase'),
	('fa fa-building-o', 'fa fa-building-o'),
	('fa fa-bullhorn', 'fa fa-bullhorn'),
	('fa fa-bullseye', 'fa fa-bullseye'),
	('fa fa-calculator', 'fa fa-calculator'),
	('fa fa-calendar', 'fa fa-calendar'),
	('fa fa-calendar-check-o', 'fa fa-calendar-check-o'),
	('fa fa-calendar-minus-o', 'fa fa-calendar-minus-o'),
	('fa fa-calendar-o', 'fa fa-calendar-o'),
	('fa fa-calendar-plus-o', 'fa fa-calendar-plus-o'),
	('fa fa-calendar-times-o', 'fa fa-calendar-times-o'),
	('fa fa-cart-plus', 'fa fa-cart-plus'),
	('fa fa-chain', 'fa fa-chain'),
	('fa fa-chain-broken', 'fa fa-chain-broken'),
	('fa fa-chevron-circle-up', 'fa fa-chevron-circle-up'),
	('fa fa-clipboard', 'fa fa-clipboard'),
	('fa fa-clone', 'fa fa-clone'),
	('fa fa-cloud-download', 'fa fa-cloud-download'),
	('fa fa-cloud-upload', 'fa fa-cloud-upload'),
	('fa fa-cog', 'fa fa-cog'),
	('fa fa-cogs', 'fa fa-cogs'),
	('fa fa-columns', 'fa fa-columns'),
	('fa fa-comment-o', 'fa fa-comment-o'),
	('fa fa-commenting-o', 'fa fa-commenting-o'),
	('fa fa-comments-o', 'fa fa-comments-o'),
	('fa fa-compass', 'fa fa-compass'),
	('fa fa-copy', 'fa fa-copy'),
	('fa fa-credit-card', 'fa fa-credit-card'),
	('fa fa-cube', 'fa fa-cube'),
	('fa fa-cubes', 'fa fa-cubes'),
	('fa fa-cut', 'fa fa-cut'),
	('fa fa-dashboard', 'fa fa-dashboard'),
	('fa fa-database', 'fa fa-database'),
	('fa fa-dedent', 'fa fa-dedent'),
	('fa fa-desktop', 'fa fa-desktop'),
	('fa fa-edit', 'fa fa-edit'),
	('fa fa-envelope-o', 'fa fa-envelope-o'),
	('fa fa-eraser', 'fa fa-eraser'),
	('fa fa-exchange', 'fa fa-exchange'),
	('fa fa-external-link', 'fa fa-external-link'),
	('fa fa-eye', 'fa fa-eye'),
	('fa fa-file-o', 'fa fa-file-o'),
	('fa fa-file-text-o', 'fa fa-file-text-o'),
	('fa fa-files-o', 'fa fa-files-o'),
	('fa fa-font', 'fa fa-font'),
	('fa fa-gears', 'fa fa-gears'),
	('fa fa-gift', 'fa fa-gift'),
	('fa fa-globe', 'fa fa-globe'),
	('fa fa-h-square', 'fa fa-h-square'),
	('fa fa-header', 'fa fa-header'),
	('fa fa-history', 'fa fa-history'),
	('fa fa-home', 'fa fa-home'),
	('fa fa-image', 'fa fa-image'),
	('fa fa-inbox', 'fa fa-inbox'),
	('fa fa-laptop', 'fa fa-laptop'),
	('fa fa-line-chart', 'fa fa-line-chart'),
	('fa fa-link', 'fa fa-link'),
	('fa fa-list', 'fa fa-list'),
	('fa fa-list-alt', 'fa fa-list-alt'),
	('fa fa-minus-circle', 'fa fa-minus-circle'),
	('fa fa-money', 'fa fa-money'),
	('fa fa-newspaper-o', 'fa fa-newspaper-o'),
	('fa fa-paperclip', 'fa fa-paperclip'),
	('fa fa-paste', 'fa fa-paste'),
	('fa fa-paw', 'fa fa-paw'),
	('fa fa-pencil-square-o', 'fa fa-pencil-square-o'),
	('fa fa-pie-chart', 'fa fa-pie-chart'),
	('fa fa-plus-circle', 'fa fa-plus-circle'),
	('fa fa-print', 'fa fa-print'),
	('fa fa-puzzle-piece', 'fa fa-puzzle-piece'),
	('fa fa-qrcode', 'fa fa-qrcode'),
	('fa fa-server', 'fa fa-server'),
	('fa fa-share-alt', 'fa fa-share-alt'),
	('fa fa-sitemap', 'fa fa-sitemap'),
	('fa fa-square-o', 'fa fa-square-o'),
	('fa fa-star-o', 'fa fa-star-o'),
	('fa fa-table', 'fa fa-table'),
	('fa fa-tablet', 'fa fa-tablet'),
	('fa fa-tags', 'fa fa-tags'),
	('fa fa-tasks', 'fa fa-tasks'),
	('fa fa-th', 'fa fa-th'),
	('fa fa-th-large', 'fa fa-th-large'),
	('fa fa-th-list', 'fa fa-th-list'),
	('fa fa-ticket', 'fa fa-ticket'),
	('fa fa-trash-o', 'fa fa-trash-o'),
	('fa fa-undo', 'fa fa-undo'),
	('fa fa-user', 'fa fa-user'),
	('fa fa-users', 'fa fa-users');
/*!40000 ALTER TABLE `tstyle` ENABLE KEYS */;

-- Dumping structure for table authdb.tul
CREATE TABLE IF NOT EXISTS `tul` (
  `seqno` bigint(15) NOT NULL DEFAULT '0',
  `curtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` varchar(60) DEFAULT NULL,
  `site` varchar(50) DEFAULT NULL,
  `progid` varchar(25) DEFAULT NULL,
  `action` varchar(50) DEFAULT '',
  `remark` varchar(200) DEFAULT '',
  `token` varchar(200) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user tracking';

-- Dumping data for table authdb.tul: ~0 rows (approximately)
/*!40000 ALTER TABLE `tul` DISABLE KEYS */;
/*!40000 ALTER TABLE `tul` ENABLE KEYS */;

-- Dumping structure for table authdb.tupwd
CREATE TABLE IF NOT EXISTS `tupwd` (
  `serverdatetime` datetime DEFAULT NULL,
  `systemdate` date NOT NULL DEFAULT '0000-00-00',
  `userid` char(8) NOT NULL DEFAULT '',
  `userpassword` char(16) NOT NULL DEFAULT '',
  `edituserid` char(8) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=tis620;

-- Dumping data for table authdb.tupwd: ~4 rows (approximately)
/*!40000 ALTER TABLE `tupwd` DISABLE KEYS */;
/*!40000 ALTER TABLE `tupwd` ENABLE KEYS */;

-- Dumping structure for table authdb.tuser
CREATE TABLE IF NOT EXISTS `tuser` (
  `userid` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `pinid` varchar(50) DEFAULT NULL,
  `pincode` varchar(100) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `status` varchar(1) DEFAULT 'A' COMMENT 'A=Activate, P=Pending,C=Closed (tuserstatus.userstatus)',
  `userpassword` varchar(100) DEFAULT NULL,
  `passwordexpiredate` date DEFAULT NULL,
  `showphoto` varchar(1) DEFAULT NULL,
  `adminflag` varchar(1) DEFAULT '0',
  `theme` varchar(20) DEFAULT NULL,
  `firstpage` varchar(100) DEFAULT NULL,
  `loginfailtimes` tinyint(3) unsigned DEFAULT '0',
  `lockflag` varchar(1) DEFAULT '0' COMMENT '1=Lock',
  `usertype` varchar(1) DEFAULT NULL,
  `iconfile` varchar(100) DEFAULT NULL,
  `accessdate` date DEFAULT NULL,
  `accesstime` time DEFAULT NULL,
  `accesshits` bigint(20) DEFAULT '0',
  `failtime` bigint(20) DEFAULT NULL,
  `siteflag` varchar(1) DEFAULT '0' COMMENT '1=Access All Site',
  `branchflag` varchar(1) DEFAULT '0' COMMENT '1=Access All Branch',
  `approveflag` varchar(1) DEFAULT '0' COMMENT '1=Approver',
  `changeflag` varchar(1) DEFAULT '0' COMMENT '1=Force change password',
  `mistakens` tinyint(4) DEFAULT '0',
  `mistakentime` bigint(20) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`),
  KEY `pinid_pincode` (`pinid`,`pincode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user info';

-- Dumping data for table authdb.tuser: ~16 rows (approximately)
/*!40000 ALTER TABLE `tuser` DISABLE KEYS */;
INSERT INTO `tuser` (`userid`, `username`, `site`, `pinid`, `pincode`, `startdate`, `enddate`, `status`, `userpassword`, `passwordexpiredate`, `showphoto`, `adminflag`, `theme`, `firstpage`, `loginfailtimes`, `lockflag`, `usertype`, `iconfile`, `accessdate`, `accesstime`, `accesshits`, `failtime`, `siteflag`, `branchflag`, `approveflag`, `changeflag`, `mistakens`, `mistakentime`, `editdate`, `edittime`, `edituser`) VALUES
	('cu-001', 'cu-001', 'FWG', '', '112233', '2018-10-10', NULL, 'A', '$2a$10$hh3QO6BMu/x5ZTWCtJN.NenY38y3uMwpCuJKRzP3fjjWRDKYmh5Be', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'E', NULL, '2019-05-26', '15:03:22', NULL, NULL, '0', '0', '1', '0', 0, NULL, '2019-05-26', '20:22:58', 'tso'),
	('cu-002', 'cu-002', 'FWG', '', '123456', '2018-10-10', NULL, 'A', 'password', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'Z', NULL, '2019-07-13', '16:31:29', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL),
	('cu-003', 'cu-003', 'FWG', '', NULL, '2018-10-10', NULL, 'A', 'password', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'Z', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	('cu-004', 'cuagain', 'FWG', '', 'EF22772542890E11', '2018-10-10', NULL, 'A', '$2a$10$hh3QO6BMu/x5ZTWCtJN.NenY38y3uMwpCuJKRzP3fjjWRDKYmh5Be', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'Z', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL),
	('deen_hippunk', 'deen_hippunk', 'FWG', NULL, 'BF96F1838D4AC613', NULL, NULL, 'A', 'password', NULL, NULL, '0', NULL, NULL, 0, '0', 'E', NULL, NULL, NULL, 0, NULL, '0', '0', '0', '0', 0, NULL, '2019-07-17', '09:17:46', 'tso'),
	('fwgadmin', 'fwgadmin@freewill.com', 'FWG', '', NULL, '2019-01-01', NULL, 'A', 'adminis', NULL, NULL, '1', NULL, NULL, 0, '0', 'A', NULL, '2019-05-05', '20:56:10', 451, NULL, '1', '1', '0', '0', 0, NULL, '2019-02-20', '04:43:58', 'fwgadmin'),
	('fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'FWG', NULL, NULL, '2019-12-26', NULL, 'A', '$2a$10$hh3QO6BMu/x5ZTWCtJN.NenY38y3uMwpCuJKRzP3fjjWRDKYmh5Be', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'A', NULL, '2020-01-03', '12:00:55', 9, 0, '0', '0', '0', '0', 0, 0, '2019-12-26', '11:00:20', 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com'),
	('fwgcenter', 'fwgcenter@freewill.com', 'FWG', '', NULL, '2019-01-01', NULL, 'A', 'adminis', NULL, NULL, '1', NULL, NULL, 0, '0', 'A', NULL, '2019-05-02', '16:48:32', 42, NULL, '1', '1', '1', '0', 0, NULL, NULL, NULL, NULL),
	('nuruddin_man', 'deenhippunk@gmail.com', 'FWG', '', NULL, '2018-10-10', NULL, 'A', 'password', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'A', NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', 0, NULL, '2019-07-16', '12:58:24', 'tso'),
	('smt', 'smt@freewill.com', 'FWG', '', NULL, '2019-01-01', NULL, 'A', 'password', NULL, NULL, '0', NULL, NULL, 0, '0', 'C', NULL, NULL, NULL, 0, NULL, '0', '0', '0', '0', 0, NULL, NULL, NULL, NULL),
	('tas', 'tas@freewill.com', 'FWG', '', NULL, '2019-01-01', NULL, 'A', '$2a$10$gHRm.oTkJtm/eZnzkpqV6OA/WOj8ZTvBPYq4q64HTzltxuVBThU/G', '2019-11-05', '0', '1', 'greenzone.css', NULL, 0, '0', 'C', NULL, '2020-01-02', '09:35:16', 1, NULL, '0', '0', '1', '0', 0, 0, NULL, NULL, NULL),
	('tch', 'tch@freewill.com', 'FWG', '', NULL, '2019-01-01', NULL, 'A', '$2a$10$gHRm.oTkJtm/eZnzkpqV6OtDXg66iokggYaHYaruKOI7kRKgBJFue', '2046-07-27', NULL, '0', NULL, NULL, 0, '0', 'A', NULL, '2020-01-02', '09:34:59', 8, NULL, '1', '1', '0', '0', 0, 0, '2019-02-13', '13:32:27', 'tso'),
	('tester', 'tester@freewill.com', 'FWG', '', NULL, '2019-01-01', NULL, 'A', '$2a$10$KWTzGDY0L6ysGFx.r0SOcuw6hTNmTZ7D1CuyWOyeuks380WdRjZ3y', '2020-05-14', NULL, '0', NULL, NULL, 0, '0', 'O', NULL, '2020-01-17', '12:28:34', 4913, NULL, '0', '0', '0', '0', 0, 0, NULL, NULL, NULL),
	('tester_test', 'tester_test@secfwgfreewillsolutions.onmicrosoft.com', 'FWG', NULL, NULL, NULL, NULL, 'A', 'tester_test', NULL, '1', '0', NULL, NULL, 0, '0', 'A', NULL, '2020-01-17', '11:23:24', 40, NULL, '1', '1', '1', '0', 0, 0, NULL, NULL, NULL),
	('tso', 'tso@freewill.com', 'FWG', '', NULL, '2019-01-01', NULL, 'A', '$2a$10$hh3QO6BMu/x5ZTWCtJN.NeGa.A1uTWLT3OYFRNqvDQ8GwhYOIkfIu', '2045-11-03', '1', '1', 'greenzone.css', NULL, 0, '0', 'A', '', '2020-01-20', '09:01:40', 4570, 1559639792439, '1', '1', '1', '0', 0, 0, NULL, NULL, NULL),
	('ttso', 'ttso@freewill.com', 'FWG', '', NULL, '2019-01-01', NULL, 'A', 'ttso', '2046-03-07', NULL, NULL, NULL, NULL, 0, '0', 'E', NULL, '2019-04-30', '13:49:46', 831, 0, '1', '1', '0', '0', 0, NULL, '2019-01-31', '18:06:52', 'tso');
/*!40000 ALTER TABLE `tuser` ENABLE KEYS */;

-- Dumping structure for table authdb.tuseraddress
CREATE TABLE IF NOT EXISTS `tuseraddress` (
  `userid` varchar(60) NOT NULL,
  `raddress1` varchar(100) DEFAULT NULL,
  `raddress2` varchar(100) DEFAULT NULL,
  `rdistrict` varchar(10) DEFAULT NULL COMMENT 'tdistrict.districtcode',
  `rdistrictname` varchar(50) DEFAULT NULL,
  `ramphur` varchar(10) DEFAULT NULL COMMENT 'tamphur.amphurcode',
  `ramphurname` varchar(50) DEFAULT NULL,
  `rprovince` varchar(10) DEFAULT NULL COMMENT 'tprovince.provincecode',
  `rprovincename` varchar(50) DEFAULT NULL,
  `rzipcode` varchar(10) DEFAULT NULL,
  `rcountry` varchar(10) DEFAULT NULL COMMENT 'tcountry.countrycode',
  `rtelno` varchar(20) DEFAULT NULL,
  `sameflag` varchar(1) DEFAULT NULL COMMENT '1=Same as current address',
  `caddress1` varchar(100) DEFAULT NULL,
  `caddress2` varchar(100) DEFAULT NULL,
  `cdistrict` varchar(10) DEFAULT NULL COMMENT 'tdistrict.districtcode',
  `cdistrictname` varchar(50) DEFAULT NULL,
  `camphur` varchar(10) DEFAULT NULL COMMENT 'tamphur.amphurcode',
  `camphurname` varchar(50) DEFAULT NULL,
  `cprovince` varchar(10) DEFAULT NULL COMMENT 'tprovince.provincecode',
  `cprovincename` varchar(50) DEFAULT NULL,
  `czipcode` varchar(10) DEFAULT NULL,
  `ccountry` varchar(10) DEFAULT NULL COMMENT 'tcountry.countrycode',
  `ctelno` varchar(20) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user address';

-- Dumping data for table authdb.tuseraddress: ~0 rows (approximately)
/*!40000 ALTER TABLE `tuseraddress` DISABLE KEYS */;
/*!40000 ALTER TABLE `tuseraddress` ENABLE KEYS */;

-- Dumping structure for table authdb.tuserbranch
CREATE TABLE IF NOT EXISTS `tuserbranch` (
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `branch` varchar(20) NOT NULL COMMENT 'tcompbranch.branch',
  `userid` varchar(60) NOT NULL COMMENT 'tuser.userid',
  PRIMARY KEY (`site`,`branch`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user access comp branchs';

-- Dumping data for table authdb.tuserbranch: ~0 rows (approximately)
/*!40000 ALTER TABLE `tuserbranch` DISABLE KEYS */;
/*!40000 ALTER TABLE `tuserbranch` ENABLE KEYS */;

-- Dumping structure for table authdb.tusercard
CREATE TABLE IF NOT EXISTS `tusercard` (
  `userid` varchar(60) NOT NULL COMMENT 'tuser.userid',
  `cardid` varchar(50) NOT NULL,
  `cardtype` varchar(2) NOT NULL COMMENT 'tcardtype.cardtype',
  `issuedate` date DEFAULT NULL,
  `expiredate` date DEFAULT NULL,
  `issuer` varchar(150) DEFAULT NULL,
  `cardimage` varchar(100) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userid`,`cardid`,`cardtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user card';

-- Dumping data for table authdb.tusercard: ~0 rows (approximately)
/*!40000 ALTER TABLE `tusercard` DISABLE KEYS */;
/*!40000 ALTER TABLE `tusercard` ENABLE KEYS */;

-- Dumping structure for table authdb.tusercomp
CREATE TABLE IF NOT EXISTS `tusercomp` (
  `site` varchar(50) NOT NULL COMMENT 'tcomp.site',
  `userid` varchar(60) NOT NULL COMMENT 'tuser.userid',
  PRIMARY KEY (`site`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user access comp info';

-- Dumping data for table authdb.tusercomp: ~0 rows (approximately)
/*!40000 ALTER TABLE `tusercomp` DISABLE KEYS */;
/*!40000 ALTER TABLE `tusercomp` ENABLE KEYS */;

-- Dumping structure for table authdb.tusergrp
CREATE TABLE IF NOT EXISTS `tusergrp` (
  `userid` varchar(60) NOT NULL DEFAULT '' COMMENT 'tuser.userid',
  `groupname` varchar(50) NOT NULL DEFAULT '' COMMENT 'tgroup.groupname',
  `rolename` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userid`,`groupname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user in group';

-- Dumping data for table authdb.tusergrp: ~17 rows (approximately)
/*!40000 ALTER TABLE `tusergrp` DISABLE KEYS */;
INSERT INTO `tusergrp` (`userid`, `groupname`, `rolename`) VALUES
	('deen_hippunk', 'EMPLOYEE', NULL),
	('fwgadmin', 'ADMIN', NULL),
	('fwgadmin', 'ADMINENGAGE', NULL),
	('fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'ADMIN', NULL),
	('fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'TESTER', NULL),
	('fwgcenter', 'CENTER', NULL),
	('nuruddin_man', 'ADMIN', NULL),
	('nuruddin_man', 'EMPLOYEE', NULL),
	('tas', 'EMPLOYEE', NULL),
	('tas', 'TESTER', NULL),
	('tch', 'ADMIN', NULL),
	('tester', 'EMPLOYEE', NULL),
	('tester', 'TESTER', NULL),
	('tester_test', 'ADMIN', NULL),
	('tso', 'ADMIN', NULL),
	('tso', 'ADMINENGAGE', NULL),
	('tso', 'CENTER', NULL);
/*!40000 ALTER TABLE `tusergrp` ENABLE KEYS */;

-- Dumping structure for table authdb.tuserinfo
CREATE TABLE IF NOT EXISTS `tuserinfo` (
  `site` varchar(50) NOT NULL DEFAULT '' COMMENT 'tcomp.site',
  `employeeid` varchar(50) NOT NULL DEFAULT '',
  `userid` varchar(60) DEFAULT NULL COMMENT 'tuser.userid',
  `cardid` varchar(50) DEFAULT NULL,
  `userbranch` varchar(20) DEFAULT NULL COMMENT 'tcompbranch.branch',
  `usertname` varchar(50) DEFAULT NULL,
  `usertsurname` varchar(50) DEFAULT NULL,
  `userename` varchar(50) DEFAULT NULL,
  `useresurname` varchar(50) DEFAULT NULL,
  `effectdate` date DEFAULT NULL,
  `beforedate` date DEFAULT NULL COMMENT 'before effect date',
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `employeecode` varchar(50) DEFAULT NULL,
  `supervisor` varchar(50) DEFAULT NULL,
  `photoimage` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL COMMENT 'F=Female,M=Male(tgender.genderid)',
  `lineno` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `cardissuedate` date DEFAULT NULL,
  `cardexpiredate` date DEFAULT NULL,
  `passportno` varchar(50) DEFAULT NULL,
  `socialcardid` varchar(50) DEFAULT NULL,
  `carecardid` varchar(50) DEFAULT NULL,
  `cardimage` varchar(100) DEFAULT NULL,
  `passportimage` varchar(100) DEFAULT NULL,
  `socialcardimage` varchar(100) DEFAULT NULL,
  `carecardimage` varchar(100) DEFAULT NULL,
  `taxid` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `officetelno` varchar(50) DEFAULT NULL,
  `officetelext` varchar(50) DEFAULT NULL,
  `raddress1` varchar(100) DEFAULT NULL,
  `raddress2` varchar(100) DEFAULT NULL,
  `rdistrict` varchar(10) DEFAULT NULL COMMENT 'tdistrict.districtcode',
  `rdistrictname` varchar(50) DEFAULT NULL,
  `ramphur` varchar(10) DEFAULT NULL COMMENT 'tamphur.amphurcode',
  `ramphurname` varchar(50) DEFAULT NULL,
  `rprovince` varchar(10) DEFAULT NULL COMMENT 'tprovince.provincecode',
  `rprovincename` varchar(50) DEFAULT NULL,
  `rzipcode` varchar(10) DEFAULT NULL,
  `rcountry` varchar(10) DEFAULT NULL COMMENT 'tcountry.countrycode',
  `sameflag` varchar(1) DEFAULT NULL COMMENT '1=Same as current address',
  `caddress1` varchar(100) DEFAULT NULL,
  `caddress2` varchar(100) DEFAULT NULL,
  `cdistrict` varchar(10) DEFAULT NULL COMMENT 'tdistrict.districtcode',
  `cdistrictname` varchar(50) DEFAULT NULL,
  `camphur` varchar(10) DEFAULT NULL COMMENT 'tamphur.amphurcode',
  `camphurname` varchar(50) DEFAULT NULL,
  `cprovince` varchar(10) DEFAULT NULL COMMENT 'tprovince.provincecode',
  `cprovincename` varchar(50) DEFAULT NULL,
  `czipcode` varchar(10) DEFAULT NULL,
  `ccountry` varchar(10) DEFAULT NULL COMMENT 'tcountry.countrycode',
  `rtelno` varchar(20) DEFAULT NULL,
  `ctelno` varchar(20) DEFAULT NULL,
  `deptcode` varchar(50) DEFAULT NULL COMMENT 'tdepartment.deptcode',
  `divcode` varchar(50) DEFAULT NULL COMMENT 'tdivision.divcode',
  `nationcode` varchar(50) DEFAULT NULL COMMENT 'tnation.nationcode',
  `racecode` varchar(50) DEFAULT NULL COMMENT 'trace.racecode',
  `religioncode` varchar(50) DEFAULT NULL COMMENT 'treligion.religioncode',
  `marrycode` varchar(50) DEFAULT NULL COMMENT 'tmarry.marrycode',
  `titlecode` varchar(50) DEFAULT NULL COMMENT 'ttitle.titlecode',
  `employid` varchar(50) DEFAULT NULL COMMENT 'temploy.employid',
  `employtype` varchar(50) DEFAULT NULL COMMENT 'monthly/daily/hourly (tworktype.worktype)',
  `employstate` varchar(50) DEFAULT NULL COMMENT 'fulltime/parttime (tworkstate.workstate)',
  `employstatus` varchar(1) DEFAULT 'A' COMMENT 'A=Active,T=Terminated(tworkstatus.workstatus)',
  `positionid` varchar(50) DEFAULT NULL COMMENT 'tposition.positionid',
  `positionname` varchar(100) DEFAULT NULL,
  `positionlevel` int(11) DEFAULT NULL,
  `levelid` varchar(50) DEFAULT NULL COMMENT 'tlevel.id',
  `workcalendar` varchar(50) DEFAULT NULL COMMENT 'ttimeschedule.scheduleid',
  `holidaycalendar` varchar(50) DEFAULT NULL COMMENT 'tholidaytable.holidayid',
  `timeshiftgroup` varchar(50) DEFAULT NULL COMMENT 'ttimegroup.groupid',
  `hiredate` date DEFAULT NULL,
  `probationdate` date DEFAULT NULL,
  `placementdate` date DEFAULT NULL,
  `workdate` date DEFAULT NULL,
  `outdate` date DEFAULT NULL,
  `workinglife` varchar(150) DEFAULT NULL,
  `salary` decimal(20,6) DEFAULT '0.000000',
  `deductamt` decimal(20,6) DEFAULT '0.000000',
  `otheramt` decimal(20,6) DEFAULT '0.000000',
  `holdflag` varchar(1) DEFAULT '0' COMMENT '1=Hold',
  `inactive` varchar(1) DEFAULT '0' COMMENT '1=Inactive',
  `signimage` varchar(200) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`site`,`employeeid`),
  UNIQUE KEY `cardid` (`cardid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user info (employee info)';

-- Dumping data for table authdb.tuserinfo: ~13 rows (approximately)
/*!40000 ALTER TABLE `tuserinfo` DISABLE KEYS */;
INSERT INTO `tuserinfo` (`site`, `employeeid`, `userid`, `cardid`, `userbranch`, `usertname`, `usertsurname`, `userename`, `useresurname`, `effectdate`, `beforedate`, `startdate`, `enddate`, `employeecode`, `supervisor`, `photoimage`, `email`, `gender`, `lineno`, `mobile`, `birthday`, `cardissuedate`, `cardexpiredate`, `passportno`, `socialcardid`, `carecardid`, `cardimage`, `passportimage`, `socialcardimage`, `carecardimage`, `taxid`, `telephone`, `officetelno`, `officetelext`, `raddress1`, `raddress2`, `rdistrict`, `rdistrictname`, `ramphur`, `ramphurname`, `rprovince`, `rprovincename`, `rzipcode`, `rcountry`, `sameflag`, `caddress1`, `caddress2`, `cdistrict`, `cdistrictname`, `camphur`, `camphurname`, `cprovince`, `cprovincename`, `czipcode`, `ccountry`, `rtelno`, `ctelno`, `deptcode`, `divcode`, `nationcode`, `racecode`, `religioncode`, `marrycode`, `titlecode`, `employid`, `employtype`, `employstate`, `employstatus`, `positionid`, `positionname`, `positionlevel`, `levelid`, `workcalendar`, `holidaycalendar`, `timeshiftgroup`, `hiredate`, `probationdate`, `placementdate`, `workdate`, `outdate`, `workinglife`, `salary`, `deductamt`, `otheramt`, `holdflag`, `inactive`, `signimage`, `editdate`, `edittime`, `edituser`, `remarks`) VALUES
	('FWG', '1332', 'nuruddin_man', '1959900190511', '00001', 'คิง', 'คอง', 'king', 'kong', NULL, NULL, '2018-10-10', NULL, '1332', '0008', NULL, 'deenhippunk@gmail.com', 'M', NULL, '0955941678', '1995-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00001', '00001', NULL, NULL, NULL, NULL, 'MR', NULL, NULL, NULL, NULL, 'P03', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 75000.000000, 15000.000000, 15000.000000, '0', '0', NULL, '2019-07-16', '12:58:24', 'tso', 'sfte007'),
	('FWG', '1333', 'deen_hippunk', '1959900190509', '00001', 'ดิง', 'ดอง', 'ding', 'dong', NULL, NULL, '2018-10-10', NULL, '1333', '0008', NULL, 'deenhippunk@gmail.com', 'M', NULL, '0955941678', '1995-05-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00001', '00001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55000.000000, 10000.000000, 0.000000, '0', '0', NULL, '2019-07-17', '09:17:46', 'tso', 'avse011'),
	('FWG', 'c001', 'cu-001', '1959900190512', '001', 'ac', 'dc', 'labu', 'masoh', NULL, NULL, '2018-10-10', NULL, '1334', '0008', NULL, 'tassun_oro@hotmail.com', 'M', NULL, '0955941678', '1996-05-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00001', '00001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 95000.000000, 15000.000000, 0.000000, '0', '0', NULL, '2019-05-26', '20:22:58', 'tso', 'pmte031'),
	('FWG', 'c002', 'cu-002', '1959900190510', '001', 'qw', 'er', 'ty', 'ui', NULL, NULL, '2018-10-10', NULL, '1332', '0008', NULL, 'deenhippunk@gmail.com', 'M', NULL, '0859800937', '1997-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00001', '00001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23000.000000, 750.000000, 0.000000, '0', '0', NULL, NULL, NULL, NULL, NULL),
	('FWG', 'c003', 'cu-003', '1067403867881', '002', 'as', 'df', 'gh', 'jk', NULL, NULL, '2018-10-10', NULL, '1335', '0008', NULL, 'deenhippunk@gmail.com', 'M', NULL, '0955941678', '2000-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00001', '00001', NULL, NULL, NULL, NULL, 'MR', NULL, 'MT', NULL, NULL, '00010', NULL, 0, NULL, NULL, NULL, NULL, '2000-07-01', NULL, NULL, NULL, NULL, NULL, 120000.000000, 0.000000, 0.000000, '1', '0', NULL, '2019-07-22', '13:22:19', 'tso', NULL),
	('FWG', 'c004', 'cu-004', '1234567890123', '003', 'อนันดา', 'เปี่ยมสุข', 'Ananda', 'Piemsuk', NULL, NULL, '2018-10-10', NULL, '1336', '0008', NULL, 'deenhippunk@gmail.com', 'M', NULL, '0955941678', '1995-05-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '021563488', '1254', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00001', '00001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35000.000000, 10000.000000, 0.000000, '0', '0', NULL, NULL, NULL, NULL, NULL),
	('FWG', 'fwgadmin', 'fwgadmin', '1959900190515', '00', 'FWG', 'Administrator', 'FWG', 'Administrator', NULL, NULL, NULL, NULL, 'fwgadmin', 'qq@qq.com', 'photo_fwg_fwgadmin.jpg', 'fwgadmin@freewillsolutions.com', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '020344299', '4299', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'E01', 'MT', 'FT', 'A', 'P05', NULL, NULL, '201902150437258694966632376255373498767358471350', '2018102609263845655668552397321224622740191047477', '2019011512164332481107699830132506934903689939267', 'DAY', NULL, NULL, NULL, NULL, NULL, NULL, 54000.000000, 15000.000000, 0.000000, '0', '0', NULL, '2019-02-20', '04:43:58', 'fwgadmin', 'pmte027'),
	('FWG', 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', NULL, NULL, 'Asset', 'Manager', 'Asset', 'Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000000, 0.000000, 0.000000, '0', '0', NULL, '2019-12-26', '11:00:20', 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'sfte007'),
	('FWG', 'smt', 'smt', '1959900190517', '00', 'สุเมธ', 'เตชาพิสุทธิ์', 'Sumeth', 'Tachapisut', NULL, NULL, NULL, NULL, 'smt', 'veera', 'photo_fwg_smt.png', 'sumeth_tac@freewillsolutions.com', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'care_fwg_smt.png', NULL, NULL, '020344299', '4299', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, 'LV04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58000.000000, 12000.000000, 0.000000, '0', '0', NULL, NULL, NULL, NULL, NULL),
	('FWG', 'tas', 'tas', '3730500569462', '00', 'ทัศสรร', 'โอรส', 'Tassan', 'Oros', '2019-05-07', NULL, NULL, NULL, 'tas', 'smt', 'photo_fwg_tas.png', 'tassan_oro@freewillsolutions.com', 'M', NULL, '029631168', '2019-05-07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'care_fwg_tas.png', NULL, NULL, '020344299', '4299', '209/98', NULL, '120601', 'ปากเกร็ด', '1206', 'ปากเกร็ด', '024', 'นนทบุรี', '11120', NULL, '1', '209/98', NULL, '120601', 'ปากเกร็ด', '1206', 'ปากเกร็ด', '024', 'นนทบุรี', '11120', NULL, NULL, NULL, '004', NULL, 'TH', 'TH', 'ISM', 'M', 'MR', 'EP01', 'MT', 'FT', 'A', 'P03', NULL, 0, 'LV05', '2018102609263845655668552397321224622740191047477', '2019011512164332481107699830132506934903689939267', 'DAY', '2000-11-01', '2001-03-01', '2001-03-02', '2000-11-01', NULL, NULL, 79000.000000, 16000.000000, 0.000000, '0', '0', NULL, '2019-05-07', '14:19:17', 'tso', 'pmte027'),
	('FWG', 'tch', 'tch', '3730500569463', '00', 'Touch', 'Oros', 'Touch', 'Oros', '2019-03-01', NULL, NULL, NULL, 'tch', 'smt', 'photo_fwg_tch.jpg', 'tassun_oro@hotmail.com', 'M', NULL, '0859800936', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '020344299', '4299', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '003', 'D02', NULL, NULL, NULL, NULL, '01', 'EP01', 'MT', 'FT', 'A', 'P03', NULL, 0, 'LV03', '2018102609263845655668552397321224622740191047477', '2019011512164332481107699830132506934903689939267', 'DAY', '2019-04-01', '2000-04-01', '2000-04-01', '2000-04-01', NULL, NULL, 115000.000000, 15000.000000, 0.000000, '0', '0', NULL, '2019-02-13', '13:32:27', 'tso', 'pmte027'),
	('FWG', 'tester', 'tester', '3730500569467', '00', 'Tester', 'Test', 'Tester', 'Test', NULL, NULL, NULL, NULL, 'tester', 'tso', 'photo_fwg_tester.png', 'tester@freewillsolutions.com', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'care_fwg_tester.png', NULL, NULL, '020344299', '4299', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '003', 'D02', 'TH', 'TH', 'BDH', 'M', 'MR', 'E01', 'MT', 'FT', 'A', 'P03', NULL, 5, NULL, '2018102609263845655668552397321224622740191047477', '2019011512164332481107699830132506934903689939267', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 120000.000000, 25000.000000, 0.000000, '0', '0', NULL, NULL, NULL, NULL, NULL),
	('FWG', 'tester_test', 'tester_test', '1455842882641', '00', 'เทสเตอร์', 'เทส', 'เทสเตอร์', 'เทส', NULL, NULL, NULL, NULL, 'tester_test', 'tso', NULL, 'tester_test@gmail.com', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000000, 0.000000, 0.000000, '0', '0', NULL, NULL, NULL, NULL, NULL),
	('FWG', 'tso', 'tso', '3730500569469', '00', 'ทัศสรร', 'โอรส', 'Tassun', 'Oros', NULL, NULL, NULL, NULL, 'tso', 'smt', 'photo_fwg_tso.png', 'tassunoros@gmail.com', 'M', 'tassun_oro', '0955941678', '2000-09-08', '2017-09-08', '2022-09-08', NULL, NULL, NULL, NULL, NULL, NULL, 'care_fwg_tso.png', NULL, NULL, '020344299', '4299', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '003', 'D02', 'TH', 'TH', 'BDH', 'M', 'MR', 'E01', 'MT', 'FT', 'A', 'P03', NULL, 9, 'LV05', '2018102609423945665279385721042184165803579235447', '2019011512164332481107699830132506934903689939267', 'DAY', '2000-11-05', '2001-02-28', '2000-11-05', '2000-11-05', NULL, NULL, 88000.000000, 16000.000000, 0.000000, '0', '0', NULL, NULL, NULL, NULL, NULL),
	('FWG', 'ttso', 'ttso', '3730500569465', '00', 'Tassuno', 'Oros', 'Tassuno', 'Oros', '2019-01-25', NULL, NULL, NULL, 'ttso', 'tso', 'photo_fwg_ttso.png', 'tassun_oro@hotmail.com', 'M', NULL, '0891240081', '2018-10-16', NULL, NULL, '11111', '11111', NULL, NULL, NULL, NULL, 'care_fwg_ttso.png', '11111', '029631168', '020344299', '1143', '209/98', NULL, '120601', 'ปากเกร็ด', '1206', 'ปากเกร็ด', '024', 'นนทบุรี', '11120', NULL, '1', '209/98', NULL, '120601', 'ปากเกร็ด', '1206', 'ปากเกร็ด', '024', 'นนทบุรี', '11120', NULL, NULL, NULL, '003', 'D02', 'TH', 'TH', 'BDH', 'M', 'MR', 'E01', 'MT', 'FT', 'A', 'P03', NULL, 0, 'LV01', '2018102609263845655668552397321224622740191047477', '2019011512164332481107699830132506934903689939267', 'DAY', NULL, '2001-02-06', '2001-02-07', '2000-11-06', '2019-01-15', '18 Years, 2 Months, 9 Days', 80000.000000, 10000.000000, 0.000000, '0', '0', NULL, '2019-01-31', '18:06:52', 'tso', 'pmte027');
/*!40000 ALTER TABLE `tuserinfo` ENABLE KEYS */;

-- Dumping structure for table authdb.tuserinfohistory
CREATE TABLE IF NOT EXISTS `tuserinfohistory` (
  `site` varchar(50) NOT NULL DEFAULT '' COMMENT 'tcomp.site',
  `employeeid` varchar(50) NOT NULL DEFAULT '',
  `userid` varchar(60) DEFAULT NULL COMMENT 'tuser.userid',
  `cardid` varchar(50) DEFAULT NULL,
  `userbranch` varchar(20) DEFAULT NULL,
  `usertname` varchar(50) DEFAULT NULL,
  `usertsurname` varchar(50) DEFAULT NULL,
  `userename` varchar(50) DEFAULT NULL,
  `useresurname` varchar(50) DEFAULT NULL,
  `effectdate` date DEFAULT NULL,
  `beforedate` date DEFAULT NULL COMMENT 'before effect date',
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `employeecode` varchar(50) DEFAULT NULL,
  `supervisor` varchar(50) DEFAULT NULL,
  `photoimage` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL COMMENT 'F=Female,M=Male(tgender.genderid)',
  `lineno` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `cardissuedate` date DEFAULT NULL,
  `cardexpiredate` date DEFAULT NULL,
  `passportno` varchar(50) DEFAULT NULL,
  `socialcardid` varchar(50) DEFAULT NULL,
  `carecardid` varchar(50) DEFAULT NULL,
  `cardimage` varchar(100) DEFAULT NULL,
  `passportimage` varchar(100) DEFAULT NULL,
  `socialcardimage` varchar(100) DEFAULT NULL,
  `carecardimage` varchar(100) DEFAULT NULL,
  `taxid` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `officetelno` varchar(50) DEFAULT NULL,
  `officetelext` varchar(50) DEFAULT NULL,
  `raddress1` varchar(100) DEFAULT NULL,
  `raddress2` varchar(100) DEFAULT NULL,
  `rdistrict` varchar(10) DEFAULT NULL COMMENT 'tdistrict.districtcode',
  `rdistrictname` varchar(50) DEFAULT NULL,
  `ramphur` varchar(10) DEFAULT NULL COMMENT 'tamphur.amphurcode',
  `ramphurname` varchar(50) DEFAULT NULL,
  `rprovince` varchar(10) DEFAULT NULL COMMENT 'tprovince.provincecode',
  `rprovincename` varchar(50) DEFAULT NULL,
  `rzipcode` varchar(10) DEFAULT NULL,
  `rcountry` varchar(10) DEFAULT NULL COMMENT 'tcountry.countrycode',
  `sameflag` varchar(1) DEFAULT NULL COMMENT '1=Same as current address',
  `caddress1` varchar(100) DEFAULT NULL,
  `caddress2` varchar(100) DEFAULT NULL,
  `cdistrict` varchar(10) DEFAULT NULL COMMENT 'tdistrict.districtcode',
  `cdistrictname` varchar(50) DEFAULT NULL,
  `camphur` varchar(10) DEFAULT NULL COMMENT 'tamphur.amphurcode',
  `camphurname` varchar(50) DEFAULT NULL,
  `cprovince` varchar(10) DEFAULT NULL COMMENT 'tprovince.provincecode',
  `cprovincename` varchar(50) DEFAULT NULL,
  `czipcode` varchar(10) DEFAULT NULL,
  `ccountry` varchar(10) DEFAULT NULL COMMENT 'tcountry.countrycode',
  `rtelno` varchar(20) DEFAULT NULL,
  `ctelno` varchar(20) DEFAULT NULL,
  `deptcode` varchar(50) DEFAULT NULL COMMENT 'tdepartment.deptcode',
  `divcode` varchar(50) DEFAULT NULL COMMENT 'tdivision.divcode',
  `nationcode` varchar(50) DEFAULT NULL COMMENT 'tnation.nationcode',
  `racecode` varchar(50) DEFAULT NULL COMMENT 'trace.racecode',
  `religioncode` varchar(50) DEFAULT NULL COMMENT 'treligion.religioncode',
  `marrycode` varchar(50) DEFAULT NULL COMMENT 'tmarry.marrycode',
  `titlecode` varchar(50) DEFAULT NULL COMMENT 'ttitle.titlecode',
  `employid` varchar(50) DEFAULT NULL COMMENT 'temploy.employid',
  `employtype` varchar(50) DEFAULT NULL COMMENT 'monthly/daily/hourly (tworktype.worktype)',
  `employstate` varchar(50) DEFAULT NULL COMMENT 'fulltime/parttime (tworkstate.workstate)',
  `employstatus` varchar(1) DEFAULT 'A' COMMENT 'A=Active,T=Terminated(tworkstatus.workstatus)',
  `positionid` varchar(50) DEFAULT NULL COMMENT 'tposition.positionid',
  `positionname` varchar(100) DEFAULT NULL,
  `positionlevel` int(11) DEFAULT NULL,
  `levelid` varchar(50) DEFAULT NULL COMMENT 'tlevel.id',
  `workcalendar` varchar(50) DEFAULT NULL COMMENT 'ttimeschedule.scheduleid',
  `holidaycalendar` varchar(50) DEFAULT NULL COMMENT 'tholidaytable.holidaycode',
  `timeshiftgroup` varchar(50) DEFAULT NULL COMMENT 'ttimegroup.groupid',
  `hiredate` date DEFAULT NULL,
  `probationdate` date DEFAULT NULL,
  `placementdate` date DEFAULT NULL,
  `workdate` date DEFAULT NULL,
  `outdate` date DEFAULT NULL,
  `workinglife` varchar(150) DEFAULT NULL,
  `salary` decimal(20,6) DEFAULT NULL,
  `deductamt` decimal(20,6) DEFAULT NULL,
  `otheramt` decimal(20,6) DEFAULT NULL,
  `holdflag` varchar(1) DEFAULT '0' COMMENT '1=Hold',
  `inactive` varchar(1) DEFAULT '0' COMMENT '1=Inactive',
  `signimage` varchar(200) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user info (employee info)';

-- Dumping data for table authdb.tuserinfohistory: ~0 rows (approximately)
/*!40000 ALTER TABLE `tuserinfohistory` DISABLE KEYS */;
INSERT INTO `tuserinfohistory` (`site`, `employeeid`, `userid`, `cardid`, `userbranch`, `usertname`, `usertsurname`, `userename`, `useresurname`, `effectdate`, `beforedate`, `startdate`, `enddate`, `employeecode`, `supervisor`, `photoimage`, `email`, `gender`, `lineno`, `mobile`, `birthday`, `cardissuedate`, `cardexpiredate`, `passportno`, `socialcardid`, `carecardid`, `cardimage`, `passportimage`, `socialcardimage`, `carecardimage`, `taxid`, `telephone`, `officetelno`, `officetelext`, `raddress1`, `raddress2`, `rdistrict`, `rdistrictname`, `ramphur`, `ramphurname`, `rprovince`, `rprovincename`, `rzipcode`, `rcountry`, `sameflag`, `caddress1`, `caddress2`, `cdistrict`, `cdistrictname`, `camphur`, `camphurname`, `cprovince`, `cprovincename`, `czipcode`, `ccountry`, `rtelno`, `ctelno`, `deptcode`, `divcode`, `nationcode`, `racecode`, `religioncode`, `marrycode`, `titlecode`, `employid`, `employtype`, `employstate`, `employstatus`, `positionid`, `positionname`, `positionlevel`, `levelid`, `workcalendar`, `holidaycalendar`, `timeshiftgroup`, `hiredate`, `probationdate`, `placementdate`, `workdate`, `outdate`, `workinglife`, `salary`, `deductamt`, `otheramt`, `holdflag`, `inactive`, `signimage`, `editdate`, `edittime`, `edituser`, `remarks`) VALUES
	('FWG', 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', NULL, NULL, 'Asset', 'Manager', 'Asset', 'Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000000, 0.000000, 0.000000, '0', '0', NULL, '2019-12-26', '11:00:20', 'fwgasset@secfwgfreewillsolutions.onmicrosoft.com', 'sfte007');
/*!40000 ALTER TABLE `tuserinfohistory` ENABLE KEYS */;

-- Dumping structure for table authdb.tuserleave
CREATE TABLE IF NOT EXISTS `tuserleave` (
  `site` varchar(50) NOT NULL,
  `userid` varchar(50) NOT NULL,
  `startdate` date NOT NULL,
  `starttime` time NOT NULL,
  `enddate` date NOT NULL,
  `endtime` time NOT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`site`,`userid`,`startdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user leave date';

-- Dumping data for table authdb.tuserleave: ~0 rows (approximately)
/*!40000 ALTER TABLE `tuserleave` DISABLE KEYS */;
/*!40000 ALTER TABLE `tuserleave` ENABLE KEYS */;

-- Dumping structure for table authdb.tusername
CREATE TABLE IF NOT EXISTS `tusername` (
  `userid` varchar(60) NOT NULL,
  `usertname` varchar(50) DEFAULT NULL,
  `usertsurname` varchar(50) DEFAULT NULL,
  `userename` varchar(50) DEFAULT NULL,
  `useresurname` varchar(50) DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `edituser` varchar(50) DEFAULT NULL,
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user name';

-- Dumping data for table authdb.tusername: ~0 rows (approximately)
/*!40000 ALTER TABLE `tusername` DISABLE KEYS */;
/*!40000 ALTER TABLE `tusername` ENABLE KEYS */;

-- Dumping structure for table authdb.tuserrole
CREATE TABLE IF NOT EXISTS `tuserrole` (
  `userid` varchar(50) NOT NULL COMMENT 'tuser.userid',
  `roleid` varchar(50) NOT NULL COMMENT 'trole.roleid',
  PRIMARY KEY (`userid`,`roleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user in roles';

-- Dumping data for table authdb.tuserrole: ~0 rows (approximately)
/*!40000 ALTER TABLE `tuserrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `tuserrole` ENABLE KEYS */;

-- Dumping structure for table authdb.tusersession
CREATE TABLE IF NOT EXISTS `tusersession` (
  `sessionid` varchar(50) NOT NULL,
  `category` varchar(15) NOT NULL,
  `userid` varchar(60) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `createdate` date DEFAULT NULL,
  `createtime` time DEFAULT NULL,
  `editdate` date DEFAULT NULL,
  `edittime` time DEFAULT NULL,
  `sessioncontents` text,
  PRIMARY KEY (`sessionid`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user session logon';

-- Dumping data for table authdb.tusersession: ~0 rows (approximately)
/*!40000 ALTER TABLE `tusersession` DISABLE KEYS */;
/*!40000 ALTER TABLE `tusersession` ENABLE KEYS */;

-- Dumping structure for table authdb.tuserstatus
CREATE TABLE IF NOT EXISTS `tuserstatus` (
  `userstatus` varchar(1) NOT NULL,
  `nameen` varchar(50) NOT NULL,
  `nameth` varchar(50) NOT NULL,
  PRIMARY KEY (`userstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user status access';

-- Dumping data for table authdb.tuserstatus: ~3 rows (approximately)
/*!40000 ALTER TABLE `tuserstatus` DISABLE KEYS */;
INSERT INTO `tuserstatus` (`userstatus`, `nameen`, `nameth`) VALUES
	('A', 'Active', 'ใช้งาน'),
	('C', 'Closed', 'ปิดการใช้งาน'),
	('P', 'Pending', 'ระงับการใช้งาน');
/*!40000 ALTER TABLE `tuserstatus` ENABLE KEYS */;

-- Dumping structure for table authdb.tusertype
CREATE TABLE IF NOT EXISTS `tusertype` (
  `usertype` varchar(1) NOT NULL,
  `nameen` varchar(50) NOT NULL,
  `nameth` varchar(50) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usertype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keep user ENGINE and level';

-- Dumping data for table authdb.tusertype: ~12 rows (approximately)
/*!40000 ALTER TABLE `tusertype` DISABLE KEYS */;
INSERT INTO `tusertype` (`usertype`, `nameen`, `nameth`, `level`) VALUES
	('A', 'Admin', 'เจ้าหน้าที่บริหาร', 30),
	('C', 'Super Coach', 'เจ้าหน้าที่ระดับสูง', 50),
	('D', 'Director', 'ผู้อำนวยการ', 70),
	('E', 'Employee', 'พนักงาน', 10),
	('M', 'Manager', 'ผู้จัดการ', 40),
	('O', 'Operator', 'เจ้าหน้าที่ปฏิบัติการ', 15),
	('P', 'President', 'ประธาน', 90),
	('S', 'Supervisor', 'ผู้ควบคุมดูแล', 20),
	('T', 'Assistance Manager', 'ผู้ช่วยผู้จัดการ', 35),
	('V', 'Vice President', 'รองประธาน', 80),
	('X', 'Executive', 'ผู้บริหาร', 60),
	('Z', 'Client', 'ลูกค้า', 5);
/*!40000 ALTER TABLE `tusertype` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
