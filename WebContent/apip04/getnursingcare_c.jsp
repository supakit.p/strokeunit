<%@page import="java.util.Calendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("P04");
    header.setMethod("request");
    try {
        Trace.info("############### P04/request ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();

        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");
        fsBean.addSchema("language", java.sql.Types.VARCHAR, "language");
        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.US);
        final String fs_language = fsBean.getString("language");
        if (fs_apiName != null && fs_apiName.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {

                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();

                    String patientId = json.getString("patientId");
                    Trace.info("############ P04/request " + patientId + " ############");
                    int pageIndex = json.has("pageIndex") ? "".equals(json.getString("pageIndex")) ? 0 : json.getInt("pageIndex") : 0;
//                    Calendar calEndDate = Calendar.getInstance(java.util.Locale.US);
//                    calEndDate.add(Calendar.DATE, -5 * pageIndex);
//                    Date endDate = calEndDate.getTime();
//                    Calendar calStartDate = Calendar.getInstance(java.util.Locale.US);
//                    calStartDate.add(Calendar.DATE, (-5 * (pageIndex + 1) + 1));
//                    Date startDate = calStartDate.getTime();
                    //////////////////////////////////////// NursingCareData ////////////////////////////////////////
                    KnSQL knsql = new KnSQL(this);
                    knsql.append("SELECT NursingActivityId , PatientId  , CONVERT(date,ActivityDate) ActivityDate, ShiftId , EmpId , FullName , TubeDesc , O2TherapyDesc, Remark ");
                    knsql.append(" FROM Tw_NursingActivity ");
                    knsql.append(" LEFT JOIN Tw_Authority ON Tw_NursingActivity.EmpId = Tw_Authority.SapId ");
//                    knsql.append(" WHERE PatientId =?patientId AND ActivityDate BETWEEN ?startDate AND ?endDate ");
                    knsql.append(" WHERE PatientId =?patientId ");
                    knsql.append(" ORDER BY ActivityDate ASC , ShiftId ASC ");
                    knsql.setParameter("patientId", patientId);
//                    knsql.setParameter("startDate", df.format(startDate));
//                    knsql.setParameter("endDate", df.format(endDate));
                    HashMap<String, String> configField = new HashMap();
                    configField.put("NursingActivityId", "nursingActId");
                    configField.put("PatientId", "patientId");
                    configField.put("ActivityDate", "activityDate");
                    configField.put("ShiftId", "shiftId");
                    configField.put("EmpId", "empId");
                    configField.put("FullName", "empName");
                    configField.put("TubeDesc", "tubeDesc");
                    configField.put("O2TherapyDesc", "o2TherapyDesc");
                    configField.put("Remark", "remark");
                    org.json.JSONArray jsonNursingCareData = new org.json.JSONArray(su.requestData(connection, knsql, true, configField).toString());
                    result += jsonNursingCareData.length();

                    //Get Count 
//                    knsql = new KnSQL(this);
//                    knsql.append("SELECT Count(ActivityDate) Count ");
//                    knsql.append(" FROM Tw_NursingActivity ");
//                    knsql.append(" WHERE PatientId =?patientId ");
//                    knsql.append(" Group BY ActivityDate ");
//                    knsql.setParameter("patientId", patientId);
//                    configField = new HashMap();
//                    configField.put("Count", "count");
//                    org.json.JSONArray jsonCount = new org.json.JSONArray(su.requestData(connection, knsql, true, configField).toString());
//                    int count = 0;
//                    for(int i = 0 ; i < jsonCount.length() ; i++){
//                        org.json.JSONObject jsonObjectElement = jsonCount.getJSONObject(i);
//                        count += jsonObjectElement.getInt("count");
//                    }
//                    
//                    body.put("paging", count%5 != 0 ? count/5 + 1 : count/5 );
                    System.err.println("jsonNursingCareData : " + jsonNursingCareData);
                    org.json.JSONArray nursingCareData = new org.json.JSONArray();
                    org.json.JSONArray nursingCareShift = new org.json.JSONArray();
                    org.json.JSONObject jsonObjectNursingCareData = new org.json.JSONObject();
                    String tempActivityDate = "";
                    for (int i = 0; i < jsonNursingCareData.length(); i++) {
                        org.json.JSONObject jsonObjectElement = jsonNursingCareData.getJSONObject(i);
                        String nursingActId = jsonObjectElement.getString("nursingActId");
                        String activityDate = jsonObjectElement.getString("activityDate");

                        if (!activityDate.equals(tempActivityDate)) {
                            tempActivityDate = activityDate;
                            if (jsonObjectNursingCareData.length() > 0) {
                                jsonObjectNursingCareData.put("nursingCareShift", nursingCareShift);
                                nursingCareData.put(jsonObjectNursingCareData);
                                nursingCareShift = new org.json.JSONArray();
                                jsonObjectNursingCareData = new org.json.JSONObject();
                            }
                        }
                        jsonObjectNursingCareData.put("activityDate", activityDate);
                        jsonObjectElement.remove("activityDate");
                        //FallRiskFactor
                        knsql = new KnSQL(this);
                        knsql.append("SELECT FallId ");
                        knsql.append(" FROM Tw_FallRiskFactor ");
                        knsql.append(" WHERE NursingActivityId =?nursingActId ");
                        knsql.setParameter("nursingActId", nursingActId);
                        configField = new HashMap();
                        configField.put("FallId", "fallId");
                        org.json.JSONArray jsonFallRiskFactor = new org.json.JSONArray(su.requestData(connection, knsql, true, configField).toString());
                        //NursingCare
                        knsql = new KnSQL(this);
                        knsql.append("SELECT NursingCareId , Seq  , GroupName , Description , DescriptionId ");
                        knsql.append(" FROM Tw_NursingCare ");
                        knsql.append(" WHERE NursingActivityId =?nursingActId ");
                        knsql.append(" ORDER BY NursingCareId , Seq ");
                        knsql.setParameter("nursingActId", nursingActId);
                        configField = new HashMap();
                        configField.put("NursingCareId", "nursingCareId");
                        configField.put("Seq", "seq");
                        configField.put("GroupName", "groupName");
                        configField.put("DescriptionId", "nursingDesciptionId");
                        configField.put("Description", "nursingDescription");
                        org.json.JSONArray jsonNursingCare = new org.json.JSONArray(su.requestData(connection, knsql, true, configField).toString());
                        org.json.JSONArray nursingCareDetail = new org.json.JSONArray();
                        org.json.JSONArray description = new org.json.JSONArray();
                        org.json.JSONObject jsonObjectNursingCareDetail = new org.json.JSONObject();
                        String tempNursingCareId = "";
                        String seq = "";
                        String tempSeq = "";
                        for (int j = 0; j < jsonNursingCare.length(); j++) {
                            org.json.JSONObject jsonObjectElementNursingCare = jsonNursingCare.getJSONObject(j);
                            String nursingCareId = jsonObjectElementNursingCare.getString("nursingCareId");
                            String groupName = jsonObjectElementNursingCare.getString("groupName");
                            seq = jsonObjectElementNursingCare.getString("seq");
                            if (!tempNursingCareId.equals(nursingCareId)) {
                                tempNursingCareId = nursingCareId;
                                if (jsonObjectNursingCareDetail.length() > 0) {
                                    if (!"".equals(tempSeq)) {
                                        jsonObjectNursingCareDetail.put("description", description);
                                    } else {
                                        jsonObjectNursingCareDetail.put("description", new org.json.JSONArray());
                                    }
                                    nursingCareDetail.put(jsonObjectNursingCareDetail);
                                    jsonObjectNursingCareDetail = new org.json.JSONObject();
                                    description = new org.json.JSONArray();
                                }
                            }
                            tempSeq = seq;
                            jsonObjectNursingCareDetail.put("nursingCareId", nursingCareId);
                            jsonObjectNursingCareDetail.put("groupName", groupName);
                            description.put(jsonObjectElementNursingCare);
                        }
                        if (jsonObjectNursingCareDetail.length() > 0) {
                            if (!"".equals(tempSeq)) {
                                jsonObjectNursingCareDetail.put("description", description);
                            } else {
                                jsonObjectNursingCareDetail.put("description", new org.json.JSONArray());
                            }
                            nursingCareDetail.put(jsonObjectNursingCareDetail);
                        }

                        jsonObjectElement.put("fallrisk", jsonFallRiskFactor);
                        jsonObjectElement.put("nursingCareDetail", nursingCareDetail);
                        nursingCareShift.put(jsonObjectElement);

                    }
                    if (jsonObjectNursingCareData.length() > 0) {
                        jsonObjectNursingCareData.put("nursingCareShift", nursingCareShift);
                        nursingCareData.put(jsonObjectNursingCareData);
                    }
                    // cal page
                    int dateShow = 5;
                    int nursingCareDataLength = nursingCareData.length();
                    int startPage = nursingCareDataLength - (dateShow * (pageIndex + 1));
                    int endPage = nursingCareDataLength - (dateShow * pageIndex);
                    body.put("pageIndex", pageIndex);
                    body.put("totalDay", nursingCareDataLength);
                    startPage = startPage < 0 ? 0 : startPage;
                    org.json.JSONArray nursingCareDataResult = new org.json.JSONArray();
                    for (int i = startPage; i < endPage; i++) {
                        nursingCareDataResult.put(nursingCareData.getJSONObject(i));
                    }

                    //////////////////////////////////////// NursingCareData ////////////////////////////////////////
                    //////////////////////////////////////// NursingSpecialCase ////////////////////////////////////////
                    knsql = new KnSQL(this);
                    knsql.append("SELECT NursingCareId , FieldDescription ");
                    knsql.append(" FROM Tw_NursingSpecialCase ");
                    knsql.append(" WHERE PatientId =?patientId ");
                    knsql.setParameter("patientId", patientId);
                    configField = new HashMap();
                    configField.put("NursingCareId", "nursingSpecialId");
                    configField.put("FieldDescription", "fieldDescription");
                    org.json.JSONArray jsonNursingSpecialCare = new org.json.JSONArray(su.requestData(connection, knsql, true, configField).toString());
                    //////////////////////////////////////// NursingSpecialCase ////////////////////////////////////////
                    //////////////////////////////////////// NursingCare ////////////////////////////////////////
                    knsql = new KnSQL(this);
                    knsql.append("SELECT NursingCareId , GroupName  , DescriptionName , UnitDescription , MoreDescriptionFlag , AnswerType , AnswerGroup, IsOther , Condition ");
                    knsql.append(" FROM Mw_NursingCare ");
                    knsql.append(" WHERE IsActive = '1' ");
                    knsql.append(" ORDER BY OrdinalGroup ASC , Ordinal ASC ");
                    configField = new HashMap();
                    configField.put("NursingCareId", "nursingCareId");
                    configField.put("GroupName", "groupName");
                    configField.put("DescriptionName", "descriptionName");
                    configField.put("UnitDescription", "unitDescription");
                    configField.put("MoreDescriptionFlag", "moreDescriptionFlag");
                    configField.put("AnswerType", "answerType");
                    configField.put("AnswerGroup", "answerGroup");
                    configField.put("IsOther", "isOther");
                    configField.put("Condition", "condition");
                    org.json.JSONArray jsonNursingCare = new org.json.JSONArray(su.requestData(connection, knsql, true, configField).toString());
                    result += jsonNursingCare.length();
                    org.json.JSONArray nursingCare = new org.json.JSONArray();
                    org.json.JSONArray nursingCareDetail = new org.json.JSONArray();
                    org.json.JSONObject jsonObjectNursingCareDetail = new org.json.JSONObject();
                    String tempGroupName = "";
                    for (int i = 0; i < jsonNursingCare.length(); i++) {
                        org.json.JSONObject jsonObjectElement = jsonNursingCare.getJSONObject(i);
                        String groupName = jsonObjectElement.getString("groupName");
                        if (!tempGroupName.equals(groupName)) {
                            tempGroupName = groupName;
                            if (jsonObjectNursingCareDetail.length() > 0) {
                                jsonObjectNursingCareDetail.put("nursingCareDetail", nursingCareDetail);
                                nursingCare.put(jsonObjectNursingCareDetail);
                                nursingCareDetail = new org.json.JSONArray();
                                jsonObjectNursingCareDetail = new org.json.JSONObject();
                            }
                        }
                        jsonObjectNursingCareDetail.put("groupName", groupName);
                        nursingCareDetail.put(jsonObjectElement);
                    }
                    if (jsonObjectNursingCareDetail.length() > 0) {
                        jsonObjectNursingCareDetail.put("nursingCareDetail", nursingCareDetail);
                        nursingCare.put(jsonObjectNursingCareDetail);
                    }
                    //////////////////////////////////////// NursingCare ////////////////////////////////////////
                    body.put("nursingCareData", nursingCareDataResult);
                    body.put("nursingCare", nursingCare);
                    body.put("nursingSpecial", jsonNursingSpecialCare);
                    body.put("master", su.getMasterArray(connection, "NursingCare", null, fs_language));
                    Trace.info("body : " + body);
                    return result;

                }
            };

            TheTransportor.transport(fsGlobal, fsExecuter);
            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
