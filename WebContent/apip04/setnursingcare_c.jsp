<%@page import="com.fs.dev.strokeunit.TheStrokeUnitEmployee"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.fs.dev.strokeunit.TheStrokeUnit"%>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<%--<c:if test="${fsScreen.config('apilistyear',pageContext.request, pageContext.response,true)}"></c:if>--%>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
    JSONObject result = new JSONObject();
    fsGlobal.setFsProg("api");
    fsGlobal.setFsSection("PROMPT");
//    fsGlobal.obtain(session);
//    fsGlobal.obtain(fsAccessor);
    JSONHeader header = new JSONHeader();
    header.setModel("P04");
    header.setMethod("Submit");
    try {
        Trace.info("############### P04/Submit ###############");
        fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
        //create bean in order to obtain parameter from request or rest injection
        com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
        fsBean.addSchema("apiName", java.sql.Types.VARCHAR, "apiName");
        fsBean.addSchema("data", java.sql.Types.VARCHAR, "data");

        fsBean.obtainFrom(request); //assign variable from request
        fsBean.forceObtain(fsGlobal);
        final java.util.Map fs_map = (java.util.Map) request.getAttribute("formParameter");
        if (fs_map != null) {
            fsBean.obtain(fs_map); //assign variable from rest
        }
        fsBean.obtain(session, request);
        Trace.info("##fs_map : " + fs_map);
        final String fs_apiName = fsBean.getString("apiName");
        final String fs_data = fsBean.getDataValue("data").toString();

        if (fs_data != null && fs_data.trim().length() > 0) {
            final JSONObject body = new JSONObject();
            final org.json.JSONObject json = new org.json.JSONObject(fs_data);
            final String fs_action = GlobalBean.RETRIEVE_MODE;
            final String fs_actionIns = GlobalBean.INSERT_MODE;
            final String fs_actionInsNoKey = TheStrokeUnit.INSERT_MODE_NO_KEY;
            com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {

                public int retrieve(java.sql.Connection connection) throws Exception {
                    int result = 0;
                    TheStrokeUnit su = new TheStrokeUnit();
                    TheStrokeUnitEmployee suEmp = new TheStrokeUnitEmployee();
                    String patientId = json.getString("patientId");
                    Trace.info("############ P04/Submit " + patientId + " ############");
                    String activityDate = json.getString("activityDate");
                    String groupName = json.has("groupName") ? json.getString("groupName") : "";
                    org.json.JSONArray nursingCareShift = new org.json.JSONArray(json.getString("nursingCareShift"));
                    org.json.JSONArray nursingSpecial = new org.json.JSONArray(json.has("nursingSpecial") ? json.getString("nursingSpecial") : "[]");
                    if (nursingSpecial.length() > 0) {
                        //Delete Data Tw_NursingSpecialCare
                        KnSQL knsql = new KnSQL(this);
                        knsql.append("DELETE FROM Tw_NursingSpecialCase WHERE PatientId =?patientId ");
                        knsql.setParameter("patientId", patientId);
                        knsql.executeUpdate(connection);
                        org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, "P04NursingSpecialCare", "Tw_NursingSpecialCase");
                        org.json.JSONArray jsonArrayTableConfig = su.handleConfigAllData(jsonArrayConfigfield);
                        for (int i = 0; i < nursingSpecial.length(); i++) {
                            org.json.JSONObject jsonObjectElement = nursingSpecial.getJSONObject(i);
                            jsonObjectElement.put("patientId", patientId);
                            result += su.handleParams(connection, fs_action, jsonArrayTableConfig, jsonObjectElement).getEffectedTransactions();
                        }
                    }
                    for (int i = 0; i < nursingCareShift.length(); i++) {
                        org.json.JSONObject jsonObjectElement = nursingCareShift.getJSONObject(i);
                        String nursingActId = jsonObjectElement.getString("nursingActId");
                        String shiftId = jsonObjectElement.getString("shiftId");
                        jsonObjectElement.put("groupName", groupName);
                        jsonObjectElement.put("activityDate", activityDate);
                        jsonObjectElement.put("patientId", patientId);
                        json.put("shiftId", shiftId);
                        if ("".equals(nursingActId)) {
                            nursingActId = TheStrokeUnit.Generate.createUUID();
                            nursingCareShift.getJSONObject(i).put("nursingActId", nursingActId);
                            json.put("nursingActId", nursingActId);
                            org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, "P04NursingActivity");
                            org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(json, jsonArrayConfigfield);
                            result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, json).getEffectedTransactions();
                            body.put("patientId", patientId);
                            body.put("activityDate", activityDate);
                            body.put("nursingCareShift", nursingCareShift);
                        } else {
                            org.json.JSONArray jsonArrayConfigfield = su.getConfigfield(connection, "P04NursingActivity");
                            org.json.JSONArray jsonArrayTableListConfig = su.handleConfigData(jsonObjectElement, jsonArrayConfigfield);
                            if ("MoreInformation".equals(groupName)) {
                                String empId = jsonObjectElement.getString("empId");
                                if (!"".equals(empId)) {
                                    suEmp.getEmployeeBySapId(connection, empId);
                                }
                                //Delete Data Tw_FallRiskFactor
                                KnSQL knsql = new KnSQL(this);
                                knsql.append("DELETE FROM Tw_FallRiskFactor WHERE NursingActivityId =?nursingActId ");
                                knsql.setParameter("nursingActId", nursingActId);
                                knsql.executeUpdate(connection);
                                org.json.JSONArray fallrisk = new org.json.JSONArray(jsonObjectElement.getString("fallrisk"));
                                org.json.JSONArray jsonArrayConfigfieldFallrisk = su.getConfigfield(connection, "P04NursingCareFall", "Tw_FallRiskFactor");
                                org.json.JSONArray jsonArrayTableListConfigFallrisk = su.handleConfigAllData(jsonArrayConfigfieldFallrisk);
                                for (int j = 0; j < fallrisk.length(); j++) {
                                    org.json.JSONObject jsonObjectElementFallrisk = fallrisk.getJSONObject(j);
                                    jsonObjectElementFallrisk.put("nursingActId", nursingActId);
                                    result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigFallrisk, jsonObjectElementFallrisk).getEffectedTransactions();
                                }
                                result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, jsonObjectElement).getEffectedTransactions();
                            } else {
                                //Delete Data Tw_NursingCare
                                KnSQL knsql = new KnSQL(this);
                                knsql.append("DELETE FROM Tw_NursingCare WHERE NursingActivityId =?nursingActId AND GroupName =?groupName ");
                                knsql.setParameter("groupName", groupName);
                                knsql.setParameter("nursingActId", nursingActId);
                                knsql.executeUpdate(connection);
                                org.json.JSONArray nursingCareDetail = new org.json.JSONArray(jsonObjectElement.getString("nursingCareDetail"));
                                org.json.JSONArray jsonArrayConfigfieldNursingCareDetail = su.getConfigfield(connection, "P04NursingCare", "Tw_NursingCare");
                                org.json.JSONArray jsonArrayTableListConfigNursingCareDetail = su.handleConfigAllData(jsonArrayConfigfieldNursingCareDetail);
                                for (int j = 0; j < nursingCareDetail.length(); j++) {
                                    org.json.JSONObject jsonObjectElementNursingCareDetail = nursingCareDetail.getJSONObject(j);
                                    String nursingCareId = jsonObjectElementNursingCareDetail.getString("nursingCareId");
                                    org.json.JSONArray description = new org.json.JSONArray(jsonObjectElementNursingCareDetail.getString("description"));
                                    if (description.length() > 0) {
                                        for (int k = 0; k < description.length(); k++) {
                                            org.json.JSONObject jsonObjectElementDescription = description.getJSONObject(k);
                                            jsonObjectElementDescription.put("nursingActId", nursingActId);
                                            jsonObjectElementDescription.put("groupName", groupName);
                                            jsonObjectElementDescription.put("nursingCareId", nursingCareId);
                                            result += su.handleParams(connection, fs_actionIns, jsonArrayTableListConfigNursingCareDetail, jsonObjectElementDescription).getEffectedTransactions();
                                        }
                                    } else {
                                        jsonObjectElementNursingCareDetail.put("nursingActId", nursingActId);
                                        jsonObjectElementNursingCareDetail.put("groupName", groupName);
                                        result += su.handleParams(connection, fs_actionInsNoKey, jsonArrayTableListConfigNursingCareDetail, jsonObjectElementNursingCareDetail).getEffectedTransactions();
                                    }

                                }
                                result += su.handleParams(connection, fs_action, jsonArrayTableListConfig, jsonObjectElement).getEffectedTransactions();
                            }
                        }

                    }
                    Trace.info("body : " + body);
                    return result;
                }
            };
            TheTransportor.transport(fsGlobal, fsExecuter);

            if (fsExecuter.effectedTransactions() > 0) {
                header.setErrorflag("N");
                header.setErrorcode("0");
                result.put("head", header);
                result.put("body", body);
                out.println(result.toJSONString());
                return;
            }
        }
    } catch (Exception ex) {
        Trace.error(fsAccessor, ex);
        fsGlobal.setThrowable(ex);
        header.setErrorflag("Y");
        header.setErrorcode("1");
        header.setErrordesc(fsGlobal.getFsMessage());
        result.put("head", header);
        out.println(result.toJSONString());
        return;
    }
    header.setErrorflag("Y");
    header.setErrorcode("2");
    header.setErrordesc(fsLabel.getText("notfound", "Not Found"));
    result.put("head", header);
    out.println(result.toJSONString());
%>
