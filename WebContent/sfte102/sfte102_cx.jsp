<?xml version="1.0" encoding="UTF-8"?>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/xmlerrorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%!
//#it's strong enough to break into another module here
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
<%
//#scrape & keeping say something anyway
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<% fsAccessor.validate(); %>
<% session.removeAttribute("fsGlobal"); %>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%if(!fsGlobal.bufferMode()||fsGlobal.insertMode()) session.removeAttribute("fsSFTE102Bean"); %>
<jsp:useBean id="fsSFTE102Bean" scope="session" class="com.fs.bean.SFTE102Bean"/>
<jsp:setProperty name="fsSFTE102Bean" property="*"/>
<%
//#import and uses wherever you will go
//#(10000) programmer code begin;
//#(10000) programmer code end;
//#if the condition come to me
//#(15000) programmer code begin;
//#(15000) programmer code end;
String fs_type = "result";
String fs_code = "";
StringBuffer fs_result = new StringBuffer();
boolean fsIsXML = fsGlobal.isXml();
fsGlobal.setFsProg("sfte102");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.setFsSection(null);
fsGlobal.obtain(session);
try {
	fsSFTE102Bean.obtain(session,request);
	//#initialize & assigned variables sitting down here
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	BeanTransport fsTransport = TheTransportor.transport(fsGlobal,fsSFTE102Bean);
	//#do you remember to obtain the transporter result
	//#(25000) programmer code begin;
	//#(25000) programmer code end;
	if(!fsGlobal.isException()) {
		TheTransportor.handle(fsGlobal,fsSFTE102Bean);
	}
	if(fsTransport!=null) fsGlobal.setSuccess(fsTransport.isSuccess());
	//#white love story handle process
	//#(25000) programmer code begin;
	//#(25000) programmer code end;
	fsSFTE102Bean.obtain(session,request);
	if(fsIsXML && GlobalBean.retrieveMode(fs_action)) {
		out.println("<root type=\"result\">");
		out.println(fsSFTE102Bean.toXMLData());
		out.println("</root>");
		return;
	}
	//#mode handle & other values never be the same again
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
} catch(Throwable ex) {
	fsGlobal.setThrowable(ex);
	Trace.error(fsAccessor,ex);
	//#what error found when it over
	//#(32500) programmer code begin;
	//#(32500) programmer code end;
}
//#before handle forward page
//#(35000) programmer code begin;
//#(35000) programmer code end;
if(fsGlobal.isException()) {
	fs_type = "error";
	fs_result = new StringBuffer();
	fs_result.append("<body>");
	fs_result.append(fsGlobal.getFsMessage());
	fs_result.append("</body>");
}
//#born to try handle forward page
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
<message type="<%=fs_type%>" code="<%=fs_code%>">
<%=fs_result.toString()%>
</message>
