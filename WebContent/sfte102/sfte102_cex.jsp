<?xml version="1.0" encoding="UTF-8"?>
<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/xmlerrorpage.jsp"%>
<%@ page contentType="application/vnd.ms-excel"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.dev.exim.*"%>
<%!
//#it's strong enough to break into another part here
//#(60000) programmer code begin;
//#(60000) programmer code end;
%>
<%
//#scrape & keeping say something anyway
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<% fsAccessor.validate(); %>
<% session.removeAttribute("fsGlobal"); %>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<% session.removeAttribute("fsSFTE102Bean"); %>
<jsp:useBean id="fsSFTE102Bean" scope="session" class="com.fs.bean.SFTE102Bean"/>
<jsp:setProperty name="fsSFTE102Bean" property="*"/>
<jsp:useBean id="fsSFTE102Bean" scope="session" class="com.fs.bean.SFTE102Bean"/>
<%
//#import and uses wherever you will go
//#(10000) programmer code begin;
//#(10000) programmer code end;
//#if the condition come to me
//#(15000) programmer code begin;
//#(15000) programmer code end;
String fs_type = "result";
String fs_code = "";
StringBuffer fs_result = new StringBuffer();
boolean fsIsXML = fsGlobal.isXml();
fsGlobal.setFsProg("sfte102");
fsGlobal.isAllowable(fsAccessor);
fsGlobal.setFsSection(null);
try {
	fsSFTE102Bean.obtain(session,request);
	//#initialize & assigned variables sitting down here
	//#(20000) programmer code begin;
	//#(20000) programmer code end;
	int fs_action = fsGlobal.parseAction();
	fsGlobal.obtain(fsAccessor);
	String[] fs_fields = new String[] {"api","client_time","stroke_mobile_id","device_id","sap_no","flag_data","hn","citizen_id","name","age"};
	ExcelWriter fs_writer = new ExcelWriter();
	fs_writer.addHeader("api","API :");
	fs_writer.addHeader("client_time","client time :");
	fs_writer.addHeader("stroke_mobile_id","stroke mobile id :");
	fs_writer.addHeader("device_id","device id :");
	fs_writer.addHeader("sap_no","sap no :");
	fs_writer.addHeader("flag_data","Flag data:");
	fs_writer.addHeader("hn","HN:");
	fs_writer.addHeader("citizen_id","citizen id:");
	fs_writer.addHeader("name","name:");
	fs_writer.addHeader("age","age:");
	//#color of the wind
	//#(22500) programmer code begin;
	//#(22500) programmer code end;
	PageUtility.createExportSchema(fs_writer,fsGlobal,request,fs_fields);
	//#handle array fields
	//#(25000) programmer code begin;
	//#(25000) programmer code end;
	if(fsIsXML) {
		out.println("<root>");
		out.println(fsSFTE102Bean.toXMLData(BeanUtility.NATIVE,fs_fields));
		out.println("</root>");
	} else {
		if(fsSFTE102Bean.size()>0) {
			fs_writer.execute(request,response,"sfte102",fsSFTE102Bean.iteratorElements());
		} else {
			fs_writer.process(request,response,"sfte102","Records not found");
		}
	}
	//#mode handle & other values never be the same again
	//#(30000) programmer code begin;
	//#(30000) programmer code end;
	return;
} catch(Throwable ex) {
	fsGlobal.setThrowable(ex);
	Trace.error(fsAccessor,ex);
}
//#before handle forward page
//#(35000) programmer code begin;
//#(35000) programmer code end;
if(fsGlobal.isException()) {
	fs_type = "error";
	fs_result.append("<body>");
	fs_result.append(fsGlobal.getFsMessage());
	fs_result.append("</body>");
}
//#born to try handle forward page
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
<message type="<%=fs_type%>" code="<%=fs_code%>">
<%=fs_result.toString()%>
</message>
