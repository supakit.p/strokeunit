<%
//#(90050) programmer code begin;
//#(90050) programmer code end;
%>
<fs:ctrlform name="tform" action="sfte102_c.jsp">
<table cellspacing="0" id="fstable1" class="tclass ui-widget-content">
<tbody id="fstablebody1">
	<tr>
		<td>
			<fs:hidden name="fsAction" tagclass="iclass" ><jsp:getProperty name="fsGlobal" property="fsAction"/></fs:hidden>
			<fs:hidden name="fsAjax" tagclass="iclass" >true</fs:hidden>
			<fs:hidden name="fsJson" tagclass="iclass" >false</fs:hidden>
			<fs:hidden name="fsLoading" tagclass="iclass" >true</fs:hidden>
			<fs:hidden name="fsDatatype" tagclass="iclass" ></fs:hidden>
		</td>
	</tr>
<%
	//#(90100) programmer code begin;
	//#(90100) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="api_label" required="false">API :</fs:label></td>
		<td class="inclass in1">
			<fs:text name="api" tagclass="iclass" tagid="api" ><jsp:getProperty name="fsSFTE102Bean" property="api"/></fs:text>
		</td>
	</tr>
<%
	//#(90200) programmer code begin;
	//#(90200) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="client_time_label" required="false">client time :</fs:label></td>
		<td class="inclass in1">
			<fs:text name="client_time" tagclass="iclass" tagid="client_time" ><jsp:getProperty name="fsSFTE102Bean" property="client_time"/></fs:text>
		</td>
	</tr>
<%
	//#(90300) programmer code begin;
	//#(90300) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="stroke_mobile_id_label" required="false">stroke mobile id :</fs:label></td>
		<td class="inclass in1">
			<fs:text name="stroke_mobile_id" tagclass="iclass" tagid="stroke_mobile_id" ><jsp:getProperty name="fsSFTE102Bean" property="stroke_mobile_id"/></fs:text>
		</td>
	</tr>
<%
	//#(90400) programmer code begin;
	//#(90400) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="device_id_label" required="false">device id :</fs:label></td>
		<td class="inclass in1">
			<fs:text name="device_id" tagclass="iclass" tagid="device_id" ><jsp:getProperty name="fsSFTE102Bean" property="device_id"/></fs:text>
		</td>
	</tr>
<%
	//#(90500) programmer code begin;
	//#(90500) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="sap_no_label" required="false">sap no :</fs:label></td>
		<td class="inclass in1">
			<fs:text name="sap_no" tagclass="iclass" tagid="sap_no" ><jsp:getProperty name="fsSFTE102Bean" property="sap_no"/></fs:text>
		</td>
	</tr>
<%
	//#(90600) programmer code begin;
	//#(90600) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="flag_data_label" required="false">Flag data:</fs:label></td>
		<td class="inclass in1">
			<fs:text name="flag_data" tagclass="iclass" tagid="flag_data" ><jsp:getProperty name="fsSFTE102Bean" property="flag_data"/></fs:text>
		</td>
	</tr>
<%
	//#(90700) programmer code begin;
	//#(90700) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="hn_label" required="false">HN:</fs:label></td>
		<td class="inclass in1">
			<fs:text name="hn" tagclass="iclass" tagid="hn" ><jsp:getProperty name="fsSFTE102Bean" property="hn"/></fs:text>
		</td>
	</tr>
<%
	//#(90800) programmer code begin;
	//#(90800) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="citizen_id_label" required="false">citizen id:</fs:label></td>
		<td class="inclass in1">
			<fs:text name="citizen_id" tagclass="iclass" tagid="citizen_id" ><jsp:getProperty name="fsSFTE102Bean" property="citizen_id"/></fs:text>
		</td>
	</tr>
<%
	//#(90900) programmer code begin;
	//#(90900) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="name_label" required="false">name:</fs:label></td>
		<td class="inclass in1">
			<fs:text name="name" tagclass="iclass" tagid="name" ><jsp:getProperty name="fsSFTE102Bean" property="name"/></fs:text>
		</td>
	</tr>
<%
	//#(91000) programmer code begin;
	//#(91000) programmer code end;
%>
	<tr class="rclass">
		<td class="lbclass lb1"><fs:label tagclass="lclass" tagid="age_label" required="false">age:</fs:label></td>
		<td class="inclass in1">
			<fs:text name="age" tagclass="iclass" tagid="age" ><jsp:getProperty name="fsSFTE102Bean" property="age"/></fs:text>
		</td>
	</tr>
<%
	//#add new rows & columns here
	//#(35000) programmer code begin;
	//#(35000) programmer code end;
%>
</tbody>
</table>
</fs:ctrlform>
<%
//#(67500) programmer code begin;
//#(67500) programmer code end;
%>
