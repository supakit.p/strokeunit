<%@ page info="SCCS id: $Id$ [FS-20161104-102000]"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.*"%>
<%@ page import="com.fs.dev.active.*"%>
<%@ page import="com.fs.bean.gener.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<%session.setAttribute("progname","patient");%>
<%session.setAttribute("progid","sfte102");%>
<%session.setAttribute("progrelease","$Release$");%>
<%session.setAttribute("progversion","$Revision$");%>
<%if(request.getParameter("clear")!=null) {
	session.removeAttribute("fsGlobal"); 
	session.removeAttribute("fsSFTE102Bean");
}%>
<%!
//#it's strong enough to break into another method here
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE102Bean" scope="session" class="com.fs.bean.SFTE102Bean"/>
<%
//#initialize & assignment always and forever
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<% fsAccessor.validate(); %>
<% fsSFTE102Bean.obtain(session,request); %>
<html>
<head>
<title>patient</title>
<jsp:include page="../jsp/meta.jsp"/>
<%
//#import style with it own
//#(12500) programmer code begin;
//#(12500) programmer code end;
%>
<jsp:include page="../jsp/tfsstyles.jsp"/>
<link href="../css/fsstyle.css" rel="stylesheet" type="text/css" />
<link href="./css/sfte040.css" rel="stylesheet" type="text/css" />
<%=PageUtility.createLinkStyles(fsAccessor)%>
<%
//#always be my styles
//#(15000) programmer code begin;
//#(15000) programmer code end;
%>
<%=PageUtility.createScripts(request,response,true)%>
<script type="text/javascript" src="../jquery/jquery-1.5.2.js"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="../jquery/ui/jquery.ui.tabs.js"></script>
<fs:importJS>Calendar,DebugDIV</fs:importJS>
<fs:genscript>lookup,util</fs:genscript>
<fs:taborder>
</fs:taborder>
<fs:validmap>
</fs:validmap>
<fs:fnmap>
</fs:fnmap>
<fs:savecmsg>Save Transaction?</fs:savecmsg>
<%
//#always be my scripting
//#(16000) programmer code begin;
//#(16000) programmer code end;
%>
<%=PageUtility.createScripts(request,response,false)%>
<script language="JavaScript" src="sfte102.js"></script>
</head>
<fs:ctrlbody>
<%=PageUtility.createApplication(request,response)%>
<%
//#values assigned & display run to me
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
<%=PageUtility.createExceptionMessage(fsGlobal,true)%>
<%if(PageUtility.includeHeader(request)) {%>
<%@ include file = "/jsp/header.jsp"%>
<%}%>
<%=PageUtility.createExceptionMessage(fsGlobal,false)%>
<%
//#wind of change can lose yourself
//#(45000) programmer code begin;
//#(45000) programmer code end;
%>
<%=PageUtility.createDialogLayer()%>
<%=PageUtility.createWaitLayer()%>
<%=PageUtility.createExportLayer()%>
<div id="workpanel">
	<div id="workbook">
<%if(PageUtility.displayTablet(request)) {%>
		<ul id="workbooklists">
			<li><a href="#tabs_2" id="htabs_2" class="tabitemsclass allowdisabled"><fs:label tagclass="lclass" tagid="entry_label">Entry</fs:label></a></li>
<%
//#declare tabs
//#(52500) programmer code begin;
//#(52500) programmer code end;
%>
		</ul>
<%}%>
<%if(PageUtility.displayPager(request,"listing")) {%>
		<div id="tabs_1" class="tabsclass">
			<div id="retrievepanel" class="retrievepanelclass">
<%@ include file="sfte102_cm.jsp"%>
			</div>
		</div>
<%}%>
<%if(PageUtility.displayPager(request,"entry")) {%>
		<div id="tabs_2" class="tabsclass">
			<div id="entrypanel" class="entrypanelclass">
<%
//#any one of us
//#(53500) programmer code begin;
//#(53500) programmer code end;
%>
<%
//#this masquerade
//#(53550) programmer code begin;
//#(53550) programmer code end;
%>
			</div>
		</div>
<%}%>
<%
//#addon tabs note book
//#(54000) programmer code begin;
//#(54000) programmer code end;
%>
	</div>
</div>
<%
//#smooth operator
//#(55000) programmer code begin;
//#(55000) programmer code end;
%>
<%if(PageUtility.includeFooter(request)) {%>
<%@ include file = "/jsp/footer.jsp"%>
<%}%>
<%
//#script and function every time you go away
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<%=PageUtility.createAlertMessage(fsGlobal)%>
<%
//#25 minutes to late
//#(65000) programmer code begin;
//#(65000) programmer code end;
%>
</fs:ctrlbody>
</html>
