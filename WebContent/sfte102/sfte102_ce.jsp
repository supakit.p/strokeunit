<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.*"%>
<%@ page import="com.fs.dev.active.*"%>
<%@ page import="com.fs.bean.gener.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="com.fs.bean.*" %>
<%@ taglib uri="/WEB-INF/taglibs-formcontrol.tld" prefix="fs"%>
<%if(request.getParameter("clear")!=null) {
	session.removeAttribute("fsGlobal");
	session.removeAttribute("fsSFTE102Bean");
}%>
<%!
//#do you remember
//#(5000) programmer code begin;
//#(5000) programmer code end;
%>
<% com.fs.bean.util.PageUtility.initialPage(request,response); %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:useBean id="fsSFTE102Bean" scope="session" class="com.fs.bean.SFTE102Bean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%!
//#the one that got away
//#(10000) programmer code begin;
//#(10000) programmer code end;
%>
<% fsAccessor.validate(); %>
<% fsSFTE102Bean.obtain(session,request); %>
<%
//#addon include file here
//#(30000) programmer code begin;
//#(30000) programmer code end;
%>
<%@ include file="sfte102_cm.jsp"%>
<%
//#already taken
//#(20000) programmer code begin;
//#(20000) programmer code end;
boolean fsIsXML = fsGlobal.isXml();
boolean fsIsJSON = fsGlobal.isJson();
int fs_chapter = fsGlobal.parseFsChapter();
int fs_pageno = fsGlobal.parseFsPage();
fsSFTE102Bean.obtain(session,request);
//#i like it like that
//#(30000) programmer code begin;
//#(30000) programmer code end;
//#coming around again
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
