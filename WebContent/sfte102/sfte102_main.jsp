<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/errorpage.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="com.fs.dev.servlet.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<% com.fs.bean.util.PageUtility.initialPage(request,response);%>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:setProperty name="fsAccessor" property="*"/>
<jsp:useBean id="fsPermission" scope="page" class="com.fs.bean.util.PermissionBean"/>
<jsp:setProperty name="fsPermission" property="*"/>
<%
	String fs_pager = PageUtility.createParameters("&",request,response);
	String fs_target = request.getParameter("target");
	TheLauncher.launchApplication(session,"sfte102",fs_target!=null,request);
	fsPermission.setProgram("sfte102");
	fsAccessor.addPermission(fsPermission);
	String fs_lang = PageUtility.getDefaultLanguage(request);
	//#get the party started
	//#(5000) programmer code begin;
	//#(5000) programmer code end;
	if(PageUtility.dispatchRequest(request)) {
		String fs_forwarder = "/sfte102/sfte102.jsp";
		RequestDispatcher rd = application.getRequestDispatcher(fs_forwarder);
		rd.forward(request,response);
		return;
	}
	//#i like it like that
	//#(7000) programmer code begin;
	//#(7000) programmer code end;
%>
<html>
<head>
<title>patient</title>
<jsp:include page="../jsp/meta.jsp"/>
<%=PageUtility.createMainScript(fsAccessor)%>
<script language="JavaScript">
try {
	var msgInfo = new MessageInfo();
	msgInfo.lang = '<%=fs_lang%>';
	msgInfo.sendType= '<%=(String)GlobalVariable.getVariable("SEND_MAIL")%>';
	msgInfo.from = '<%=(String)GlobalVariable.getVariable("FROM")%>';
	msgInfo.itMail = '<%=(String)GlobalVariable.getVariable("IT_MAIL")%>';
	msgInfo.freewillMail = '<%=(String)GlobalVariable.getVariable("FREEWILL_MAIL")%>';
	msgInfo.subject = '<%=(String)GlobalVariable.getVariable("SUBJECT")%>';
}catch(ex) { }
//#script and function definition anything for you
//#(10000) programmer code begin;
//#(10000) programmer code end;
</script>
</head>
<%
//#you'll be in my heart
//#(20000) programmer code begin;
//#(20000) programmer code end;
%>
<frameset rows="0,*" id="theframe" frameborder="NO" border="0" framespacing="0">
  <frame src="sfte102_h.html" name="headframe"></frame>
<%
//#remember me this way
//#(30000) programmer code begin;
//#(30000) programmer code end;
%>
  <frame src="sfte102.jsp?clear=true<%=fs_pager%>" name="mainframe"></frame>
<%
//#nothing gonna change my love for you
//#(40000) programmer code begin;
//#(40000) programmer code end;
%>
</frameset>
<%
//#i wanna be with you
//#(50000) programmer code begin;
//#(50000) programmer code end;
%>
<body>
</body>
</html>
