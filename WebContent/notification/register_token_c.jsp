<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<c:if test="${fsScreen.config('notify',pageContext.request, pageContext.response,true)}"></c:if>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("notify");
fsGlobal.setFsSection("PROMPT");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
JSONHeader header = new JSONHeader();
header.setModel("notify");
header.setMethod("register_token");
try { 
	fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
	//create bean in order to obtain parameter from request or rest injection
	com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
	fsBean.addSchema("site",java.sql.Types.VARCHAR,"site");
	fsBean.addSchema("userid",java.sql.Types.VARCHAR,"userid");
	fsBean.addSchema("deviceid",java.sql.Types.VARCHAR,"deviceid");
	fsBean.addSchema("os",java.sql.Types.VARCHAR,"os");
	fsBean.addSchema("token",java.sql.Types.VARCHAR,"token");
	fsBean.addSchema("cardid",java.sql.Types.VARCHAR,"cardid");
	fsBean.addSchema("birthday",java.sql.Types.DATE,"birthday");
	fsBean.addSchema("language",java.sql.Types.VARCHAR,"language");
	fsBean.obtainFrom(request); //assign variable from request
	fsBean.forceObtain(fsGlobal);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsBean.obtain(fs_map); //assign variable from rest
	fsBean.obtain(session,request);
	String fs_language = fsBean.getString("language");
	fsLabel.setLanguage(fs_language);
	final String fs_site = fsBean.getString("site");
	final String fs_userid = fsBean.getString("userid");
	final String fs_deviceid = fsBean.getString("deviceid");
	final String fs_os = fsBean.getString("os");
	final String fs_token = fsBean.getString("token");
	final String fs_cardid = fsBean.getString("cardid");
	final java.sql.Date fs_birthday = fsBean.getFieldByName("birthday").asDate();
	if(fs_deviceid==null || fs_deviceid.trim().length()<=0) throw new java.sql.SQLException("Unspecified device id","deviceid",-11101);
	if(fs_token==null || fs_token.trim().length()<=0) throw new java.sql.SQLException("Unspecified device token","devicetoken",-11102);
	if(fs_os==null || fs_os.trim().length()<=0) throw new java.sql.SQLException("Unspecified OS","os",-11101);
	if((fs_deviceid !=null && fs_deviceid.trim().length()>0) && (fs_token !=null && fs_token.trim().length()>0)) {
		com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
			//override method update depend on action from fsGlobal.setFsAction(GlobalBean.UPDATE_MODE);
			public int update(java.sql.Connection connection) throws Exception {
				int result = 0;
				String site = null;
				String userid = null;
				KnSQL knsql = new KnSQL(this);
				if(fs_userid !=null && fs_userid.trim().length()>0) {
					knsql.append("select userid,site from tuser ");
					knsql.append("where userid = ?userid ");
					if(fs_site!=null && fs_site.trim().length()>0) {
						knsql.append("and site = ?site ");
						knsql.setParameter("site",fs_site);
					}
					knsql.setParameter("userid",fs_userid);
					java.sql.ResultSet rs = knsql.executeQuery(connection);
					if(rs.next()) {
						result++;
						site = rs.getString("site");
						userid = rs.getString("userid");
					}
					close(rs);
				} else if(fs_cardid !=null && fs_cardid.trim().length()>0) {
					knsql.append("select userid,site from tuserinfo ");
					knsql.append(" where cardid = ?cardid and inactive = '0' ");
					if(fs_birthday!=null) {
						knsql.append("and birthday = ?birthday ");
						knsql.setParameter("birthday",fs_birthday);
					}
					knsql.setParameter("cardid",fs_cardid);
					java.sql.ResultSet rs = knsql.executeQuery(connection);
					if(rs.next()) {
						result++;
						site = rs.getString("site");
						userid = rs.getString("userid");
					}
					close(rs);
				}
				if(result<=0) throw new java.sql.SQLException("User does not existed","userid",-8895);
				boolean found = false;
				int id = 0;
				String uid = null;
				String token = null;
				knsql.clear();
				knsql.append("delete from tuserdeviceinfo ");
				knsql.append("where site = ?site and user_id = ?userid ");
				knsql.setParameter("site",site);
				knsql.setParameter("userid",userid);
				knsql.executeUpdate(connection);
				knsql.clear();
				knsql.append("select * from tuserdeviceinfo ");
				knsql.append("where site = ?site and device_id = ?deviceid ");
				knsql.setParameter("site",site);
				knsql.setParameter("deviceid",fs_deviceid);
				java.sql.ResultSet rs = knsql.executeQuery(connection);
				if(rs.next()) {
					found = true;
					id = rs.getInt("id");
					uid = rs.getString("user_id");
					token = rs.getString("device_token");
				}
				close(rs);
				if(found) {
					//verify same token, difference user
					if(fs_token.equals(token) && !userid.equals(uid)) {
						throw new java.sql.SQLException("Duplicate usage token","devicetoken",-11103);
					}
					if(fs_token.equals(token) && userid.equals(uid)) {
						//verify same token and same user, then set active
						knsql.clear();
						knsql.append("update tuserdeviceinfo set active=1 where id = ?id ");
						knsql.setParameter("id",id);
						knsql.executeUpdate(connection);
					} else if(!fs_token.equals(token) && userid.equals(uid)) {
						//verify difference token but same user, then update new token
						knsql.clear();
						knsql.append("update tuserdeviceinfo set active=1, device_token = ?devicetoken where id = ?id ");
						knsql.setParameter("id",id);
						knsql.setParameter("devicetoken",fs_token);
						knsql.executeUpdate(connection);						
					} else if(fs_token.equals(token) && !userid.equals(uid)) {
						//verify same token but difference user, then update new user
						knsql.clear();
						knsql.append("update tuserdeviceinfo set active=1, user_id = ?userid where id = ?id ");
						knsql.setParameter("id",id);
						knsql.setParameter("userid",userid);
						knsql.executeUpdate(connection);												
					} else if(!fs_token.equals(token) && !userid.equals(uid)) {
						//verify difference token and difference user then update new token and new user
						knsql.clear();
						knsql.append("update tuserdeviceinfo set active=1, device_token = ?devicetoken, user_id = ?userid where id = ?id ");
						knsql.setParameter("id",id);
						knsql.setParameter("devicetoken",fs_token);
						knsql.setParameter("userid",userid);
						knsql.executeUpdate(connection);						
					}
				} else {
					String osname = "";
					if("I".equalsIgnoreCase(fs_os)) osname = "iOS";
					else if("A".equalsIgnoreCase(fs_os)) osname = "android";
					//register new record
					TUserDeviceInfoData data = new TUserDeviceInfoData();
					data.init(global);
					data.setRelatable(this);
					data.setSite(site);
					data.setUserid(userid);
					data.setDeviceid(fs_deviceid);
					data.setDevicetoken(fs_token);
					data.setOs(osname);
					data.setActive(1);
					data.setCreateddate(new java.sql.Date(System.currentTimeMillis()));
					data.setCreatedtime(new java.sql.Time(System.currentTimeMillis()));
					data.insert(connection);
				}
				return result;
			}	
		};
		TheTransportor.transport(fsGlobal,fsExecuter);
		if(fsExecuter.effectedTransactions()>0) {
			header.composeError("N", "0", "");
			result.put("head",header);
			out.println(result.toJSONString());
			return;
		}
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	int errorCode = fsGlobal.parseFsErrorcode();
	header.composeError("Y", Integer.toString(errorCode==0?1:errorCode), fsGlobal.getFsMessage());
	result.put("head",header);
	out.println(result.toJSONString());
	return;
}
header.composeError("Y", "2", fsLabel.getText("notfound","Not Found"));
result.put("head",header);
out.println(result.toJSONString());
%>
