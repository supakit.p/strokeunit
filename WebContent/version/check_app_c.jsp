<%@ page info="SCCS id: $Id$"%>
<%@ page errorPage="/jsp/jsonerrorpage.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fs.bean.*" %>
<%@ page import="com.fs.bean.ctrl.*"%>
<%@ page import="com.fs.bean.util.*"%>
<%@ page import="com.fs.bean.misc.*"%>
<%@ page import="org.json.simple.*" %>
<jsp:useBean id="fsAccessor" scope="session" class="com.fs.bean.util.AccessorBean"/>
<jsp:useBean id="fsScreen" scope="request" class="com.fs.dev.library.ScreenUtility"/>
<jsp:useBean id="fsLabel" scope="request" class="com.fs.bean.util.LabelConfig"/>
<jsp:useBean id="fsGlobal" scope="request" class="com.fs.bean.util.GlobalBean"/>
<jsp:setProperty name="fsGlobal" property="*"/>
<%
JSONObject result = new JSONObject();
fsGlobal.setFsProg("version");
fsGlobal.setFsSection("PROMPT");
fsGlobal.obtain(session);
fsGlobal.obtain(fsAccessor);
JSONHeader header = new JSONHeader();
header.setModel("version");
header.setMethod("check_app");
final JSONObject body = new JSONObject();
try { 
	fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
	//create bean in order to obtain parameter from request or rest injection
	com.fs.bean.CustomBean fsBean = new com.fs.bean.CustomBean();
	fsBean.addSchema("version",java.sql.Types.VARCHAR,"username");
	fsBean.addSchema("type",java.sql.Types.VARCHAR,"type");
	fsBean.addSchema("language",java.sql.Types.VARCHAR,"language");
	fsBean.obtainFrom(request); //assign variable from request
	fsBean.forceObtain(fsGlobal);
	java.util.Map fs_map = (java.util.Map)request.getAttribute("formParameter");
	if(fs_map!=null) fsBean.obtain(fs_map); //assign variable from rest
	fsBean.obtain(session,request);
	fsGlobal.setFsLanguage(fsBean.getString("language"));
	final String fs_language = fsBean.getString("language");
	fsLabel.setLanguage(fs_language);
	final String fs_version = fsBean.getFieldByName("version").asString();
	final String fs_type = fsBean.getFieldByName("type").asString();
	Trace.info(">>> check_app : "+fsBean);
	if(fs_version !=null && fs_version.trim().length()>0) {
		body.put("version",fs_version);
		com.fs.bean.ExecuteData fsExecuter = new com.fs.bean.ExecuteData() {
			//override method retrieve depend on action from fsGlobal.setFsAction(GlobalBean.RETRIEVE_MODE);
			public int retrieve(java.sql.Connection connection) throws Exception {
				int result = 0;
				BeanUtility butil = new BeanUtility();
				String fs_appversion = null;
				java.sql.Date effectdate = null;
				KnSQL knsql = new KnSQL(this);
				knsql.append("select appversion,effectdate ");
				knsql.append("from tversion ");
				knsql.append("where appcode = 'ADVANCE' ");
				knsql.append("and apptype = ?apptype ");
				knsql.setParameter("apptype",fs_type==null?"I":fs_type);
				java.sql.ResultSet rs = knsql.executeQuery(connection);
				if(rs.next()) {
					result++;
					fs_appversion = rs.getString("appversion");
					effectdate = rs.getDate("effectdate");
				}
				close(rs);
				if(result>0) {
					if(fs_appversion!=null) {
						boolean valid = false;
						body.put("newversion",fs_appversion);
						if(effectdate!=null) {
							java.sql.Date curdate = new java.sql.Date(System.currentTimeMillis());
							valid = butil.compareDate(effectdate,curdate)>0;
						}
						if(!valid && fs_appversion.compareTo(fs_version)>0) {
							throw new java.sql.SQLException("Please install and update new version","appversion",-8881);
						}
					}
				}
				return result;
			}	
		};
		TheTransportor.transport(fsGlobal,fsExecuter);
		if(fsExecuter.effectedTransactions()>0) {
			header.composeError("N", "0", "");
			result.put("head",header);
			result.put("body",body);
			out.println(result.toJSONString());
			return;
		}
	}
}catch(Exception ex) { 
	Trace.error(fsAccessor,ex);
	fsGlobal.setThrowable(ex);
	int errorCode = fsGlobal.parseFsErrorcode();
	header.composeError("Y", Integer.toString(errorCode==0?1:errorCode), fsGlobal.getFsMessage());
	result.put("head",header);
	result.put("body",body);
	out.println(result.toJSONString());
	return;
}
header.composeError("Y", "2", fsLabel.getText("notfound","Not Found"));
result.put("head",header);
result.put("body",body);
out.println(result.toJSONString());
%>
